<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Start Solar Lead/Appointment Detail</title>
    
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css" />

<style type="text/css">
	/* Client-specific Styles */
	/*#outlook a {padding:0;}*/ /* Force Outlook to provide a "view in browser" menu link. */
	body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;font-family: 'Lato', sans-serif;}
	.ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
	.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.*/
	#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
	img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
	a img {border:none;text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
	.image_fix {display:block;}
	p {margin: 0px 0px !important;}
	table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; overflow-wrap: break-word; word-wrap: break-word; -ms-word-break: break-all; word-break: break-all; word-break: break-word; -ms-hyphens: auto; -moz-hyphens: auto; -webkit-hyphens: auto; hyphens: auto; }
	table td {border-collapse: collapse;}
	
	/* ---- Styles for iPad ---- */
	@media only screen and (max-width: 640px) {
		a[href^="tel"], a[href^="sms"] {text-decoration: none;pointer-events: none;cursor: default;}
		.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;pointer-events: auto;cursor: default;}
		table[class=devicewidth] {width: 100%!important;text-align:center!important;}
		td[class=devicewidth] {width: 100%!important;text-align:center!important;}
		table[class=devicewidthinner] {width: 92%!important;text-align:center!important;}
		td[class=devicewidthinner] {width: 92%!important;text-align:center!important;}
		table[class="fullwidth"]{width:100%!important;}
		table[class=centered] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=centered] {width: 100%!important; text-align: center!important; clear: both; }
		td[class="logo"]{width:100%!important; float:left; text-align: center; margin-bottom:20px;}
		td[class="fullwidth"]{width:100%!important; float: left;}
		img[class="fullwidth"]{width:100%!important;}
		td[class="aligncenter"]{width:100%!important; text-align:center!important;}
		table[class="aligncenter"]{width:100%!important; text-align:center!important;}
	}
	
	/* ---- Styles for iPhone ---- */
	@media only screen and (max-width: 480px) {
		a[href^="tel"], a[href^="sms"] {text-decoration: none;color: #ffffff;pointer-events: none;cursor: default;}
		.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {text-decoration: default;color: #ffffff !important;pointer-events: auto;cursor: default;}
		table[class=devicewidth] {width: 100%!important;text-align:center!important;}
		td[class=devicewidth] {width: 100%!important;text-align:center!important;}
		table[class=devicewidthinner] {width: 92%!important;text-align:center!important;}
		td[class=devicewidthinner] {width: 92%!important;text-align:center!important;}
		table[class="fullwidth"]{width:100%!important;}
		td[class="fullwidth"]{width:100%!important; float: left;}
		table[class=centered] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=centered] {width: 100%!important; text-align: center!important; clear: both; }
		td[class="logo"]{width:100%!important; float:left; text-align:center;margin-bottom:20px;}
		img[class="fullwidth"]{width:100%!important;}
		td[class="aligncenter"]{width:100%!important; text-align:center!important;}
		table[class="aligncenter"]{width:100%!important; text-align:center!important;}
	}
</style>

</head>

<body>

<!-- header begin -->
<table width="100%" bgcolor="#c4e1f4" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="border-bottom-width: 5px; border-bottom-style: solid; border-bottom-color: rgb(42, 122, 175);" class="">
	<tbody>
		<tr>
			<td width="100%">
				<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
					<tbody>
						<!-- Top Spacing begin -->
                        <tr>
                        <td width="100%" height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">&nbsp;</td>
                        </tr>
                        <!-- Top Spacing end-->
						
                        <!-- logo begin -->
                        <tr>
                            <td width="100%">
                                <table width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidthinner">
                                    <tbody>
                                        <tr>
                                            <td width="100%" valign="middle">
                                                <!-- Logo begin -->
                                                <table width="580" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullwidth">
                                                    <tbody>
                                                        <tr>
                                                            <td width="100%" valign="middle" align="left" style="text-align: center;" class="aligncenter">
                                                                <a href="http://www.startsolar.com.au" target="_blank" title="Start Solar Pty Ltd">
                                                                	<img width="100" src="http://lms.startsolar.com.au/img/logostartsolar.png" border="0" title="Start Solar Pty Ltd" alt="Start Solar Pty Ltd" />
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- /Logo end -->
                                                
												<!-- **** Mobile spacing Begin **** -->
                                                <table width="1" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tbody>
                                                        <tr>
                                                        	<td width="100%" height="15" style="font-size: 0;line-height: 0;border-collapse: collapse;">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- **** Mobile spacing end **** -->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- logo end -->                        
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<!-- header end -->


<!-- content begin -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" class="">
    <tbody>
        <tr>
            <td width="100%">
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                    <tbody>
                        <!-- Top Spacing -->
                        <tr>
                        	<td width="100%" height="20" style="font-size:0px; line-height:0px; mso-line-height-rule: exactly;">&nbsp;</td>
                        </tr>
                        <!-- Top Spacing -->
                        <tr>
                            <td width="100%">
                                <table width="580" cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="devicewidthinner">
                                    <tbody>
                                    	<tr>
                                        	<td style="font-family: 'Lato', sans-serif; font-size: 16px; font-weight: 600; color: #333333; text-align: left;" class="">Dear LMS team,</td>
                                        </tr>
                                        <!-- Top Spacing -->
                                        <tr>
                                            <td width="100%" height="16" style="font-size:0px; line-height:0px; mso-line-height-rule: exactly;">&nbsp;</td>
                                        </tr>
                                        <!-- Top Spacing -->
                                        <tr>
                                            <td style="font-family: 'Lato', sans-serif; font-size: 16px; font-weight: 400; color: #333; text-align: left; line-height: 24px;" class="">Below is the query submitted by the user: 
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <!-- Top Spacing -->
                        <tr>
                        	<td width="100%" height="20" style="font-size:0px; line-height:0px; mso-line-height-rule: exactly;">&nbsp;</td>
                        </tr>
                        <!-- Top Spacing -->
                        
                        <tr>
                            <td width="100%">
                                <table width="580" cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="devicewidthinner">
                                    <tbody>
									    <tr>
                                            <td style="font-family: 'Lato', sans-serif; font-size: 16px; font-weight: 400; color: #333; text-align: left; line-height: 24px;" class=""><b style="font-weight:600">Ticket number:</b> <?php echo $info['ticket_no']; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: 'Lato', sans-serif; font-size: 16px; font-weight: 400; color: #333; text-align: left; line-height: 24px;" class=""><b style="font-weight:600">Name:</b> <?php echo $info['alias_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: 'Lato', sans-serif; font-size: 16px; font-weight: 400; color: #333; text-align: left; line-height: 24px;" class=""><b style="font-weight:600">Email:</b> <a href="mailto:<?php echo $info['email_id']; ?>"><?php echo $info['email_id']; ?></a></td>
                                        </tr>
                                        
                                        <tr>
                                            <td width="100%" height="10" style="font-size:0px; line-height:0px; mso-line-height-rule: exactly;">&nbsp;</td>
                                        </tr>
                                        <!-- Top Spacing -->
                                        <tr>
                                            <td style="font-family: 'Lato', sans-serif; font-size: 16px; font-weight: 400; color: #333; text-align: left; line-height: 24px;" class=""><b style="font-weight:600">Remarks:</b><?php echo $info['feedback']; ?> </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<!-- content end -->

</body>

</html>
