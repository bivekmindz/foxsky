<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


//$route['default_controller'] = 'webapp/webapp/landing';

$route['admin']		 		 = 'admin/login';
$route['rate/(.*)']='webapp/rate/index';
//$route['search'] 			 = 'webapp/webapp/search/';
$route['search'] 			 = 'webapp/Search/index';
$route['search1'] 			 = 'webapp/webapp1/search/';
$route['searchnorecord'] 	 = 'webapp/webapp/searchnorecord';
$route['shipping'] 			 = 'webapp/shipping';
$route['return_policy'] 	 = 'webapp/return_policy';
$route['privacy_policy'] 	 = 'webapp/privacy_policy';
$route['fbregister'] 	 	 = 'webapp/fblogin/fbregister';
$route['gmailregister'] 	 = 'webapp/google_login/gmailregister';

$route['register'] 	 		 = 'webapp/registerlogin/register';
$route['login'] 	 		 = 'webapp/memberlogin/memlogin';
$route['vendor-login'] 	 	 = 'manufacturer/login';
$route['myaccount'] 	 	 = 'webapp/memberlogin/myaccount';
$route['track-order(.*)'] 	 = 'webapp/memberlogin/track_order/$1';
$route['all-order'] 	 	 = 'webapp/memberlogin/allorder';
$route['checkout'] 	 	 	 = 'webapp/checkout/index';
$route['logout'] 	 		 = 'webapp/memberlogin/memberLogout';
$route['termsandcondition']  = 'webapp/terms';
$route['cart'] 				 = 'webapp/cart';
$route['allcategory']        = 'webapp/webapp/morecategory';
$route['electronictoys']     = 'webapp/webapp/electronictoys';
$route['softtoys']       	 = 'webapp/webapp/softtoys';
$route['educationaltoys']    = 'webapp/webapp/educationaltoys';
$route['dolltoys']		     = 'webapp/webapp/dolltoys';
$route['strollersandWalkers']= 'webapp/webapp/strollertoys';
$route['rideons']			 = 'webapp/webapp/rideontoys';
$route['outdoorsprots']		 = 'webapp/webapp/outdoortoys';
$route['diecastvehicles']	 = 'webapp/webapp/diecasttoys';
$route['babyandtoddlertoys'] = 'webapp/webapp/babytoys';
$route['musicaltoys']		 = 'webapp/webapp/musicaltoys';
$route['indoorgames']		 = 'webapp/webapp/indoortoys';
$route['kidroomdecor']		 = 'webapp/webapp/kidroom';
$route['puzzlesandcubes']	 = 'webapp/webapp/puzzeltoys';
$route['actiontoysandfigures'] = 'webapp/webapp/actiontoys';
$route['activitysets'] 		 = 'webapp/webapp/activitytoys';
$route['schoolsupplies'] 	 = 'webapp/webapp/schooltoys';
$route['aboutus'] 	         = 'webapp/webapp/aboutus';
$route['support'] 	         = 'webapp/webapp/support';
$route['useraggree'] 	         = 'webapp/webapp/useraggree';
$route['privacy'] 	         = 'webapp/webapp/privacy';
$route['terms'] 	         = 'webapp/webapp/terms';
$route['career'] 	         = 'webapp/webapp/career';
$route['returnpolicy'] 	         = 'webapp/webapp/returnpolicy';

$route['careers'] 	         = 'webapp/webapp/careers';
$route['faq'] 	             = 'webapp/webapp/faq';
$route['startbusiness'] 	 = 'webapp/webapp/startbusiness';
$route['creditpurchase'] 	 = 'webapp/webapp/creditpurchase';
$route['success/(.*)'] 		 = 'webapp/success/$1';
$route['cancel/(.*)'] 		 = 'webapp/cancel/$1';
$route['combo/(.*)'] 	 	= 'webapp/combo/combolisting/$1';
$route['comboproduct/(.*)'] 	 = 'webapp/combo/productdetail/$1';
$route['category/(.*)'] 	 = 'webapp/webproductlisting/productlisting/$1';
// $route['product/(.*)']	 	 = 'webapp/webproductlisting/productdetail/$1/$2';

$route['404_override'] 		 = 'webapp/hel/';
$route['shopotox'] 	         = 'webapp/shopotox/manageSessionTable';
$route['template'] 	         = 'vendor/ckeditor/index';
$route['contactus'] 	     = 'webapp/contactus';
$route['maincategory/(.*)']  = 'webapp/catproductlisting/productlisting/$1';
$route['allbrands']        = 'webapp/webapp/all_brands';
$route['error'] 			 = 'webapp/notfound';
$route['stationery']        = 'webapp/webapp/stationarycat';
$route['track'] 	 	 = 'webapp/memberlogin/tracking';
$route['feedback']		 = 'webapp/webapp/feedback';

$route['recipe'] 	 	 = 'webapp/recipe/recipelist';
$route['recipedetail/(.*)'] 	 	 = 'webapp/recipe/recipedetail/$1';
$route['permission_error'] 	 	 = 'admin/login/permission_error';

//Mi
$route['default_controller'] = 'webapp/webapp/index';
$route['home']               = 'webapp/webapp/index';
$route['product/(.*)']	 	 = 'webapp/productdetails/index';
$route['productspecification/(.*)']	 	 = 'webapp/productdetails/productspecification';
$route['productaccessories/(.*)']	 	 = 'webapp/productdetails/productaccessories';
$route['productreview/(.*)']	 	 = 'webapp/productdetails/productreview';
$route['review/(.*)'] 		 = 'webapp/review/index';

$route['addreview'] 		 = 'webapp/review/addreview';
$route['addcomments'] 		 = 'webapp/review/addcomments';
$route['ajax']  = 'webapp/review/likes';


$route['productfcode/(.*)']	 	 = 'webapp/productdetails/productfcode';
$route['productdetails/(.*)']	 	 = 'webapp/productdetails/productcart';
$route['productaccessoriesdetails/(.*)']	 	 = 'webapp/productdetails/productaccessoriesdetails';
$route['orderinformation']               = 'webapp/cart/orderinformation';
$route['updateshipaddre']               = 'webapp/cart/updateshipaddre';
$route['ccrequesthander']               = 'webapp/ccavenue/ccrequesthander';
$route['codpayment']               = 'webapp/ccavenue/codpayment';
$route['myaccount']	 	 = 'webapp/myaccount/index';
$route['myorder']	 	 = 'webapp/myaccount/myorder';
$route['orderdetails/(.*)']	 	 = 'webapp/myaccount/orderdetails/(.*)';
$route['myaddress']	 	 = 'webapp/myaccount/myaddress';
$route['myprofile']	 	 = 'webapp/myaccount/myprofile';
$route['security']	 	 = 'webapp/myaccount/security';
$route['myreview']	 	 = 'webapp/myaccount/myreview';
$route['editreview/(.*)']	 	 = 'webapp/myaccount/editreview/(.*)';
$route['editreviewupdate']	 	 = 'webapp/myaccount/editreviewupdate';

$route['allproduct']	 	 = 'webapp/allproduct/index';
$route['category/(.*)']	 	 = 'webapp/allproduct/category';
