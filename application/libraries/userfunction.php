<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
class userfunction {
	public function paramiter($parameter = array()) {
		$qtag = null;
		$size = sizeof($parameter);
		if ($size > 0) {
			for ($i = 1; $i <= $size; $i++) {
				$qtag[] = '?';
			}
			return implode(',', $qtag);
		} else {
			return false;
		}
	}
	public function loginAdminvalidation($redirect = NULL) {
		$CI = &get_instance();
		if ($CI->session->userdata('bizzadmin')->LoginID != true) {
			if ($redirect != NULL) {
				redirect($redirect);
			} else {
				redirect('admin');
			}
		}
	}
	public function loginAdminvalidationvendor($redirect = NULL) {
		$CI = &get_instance();
		if ($CI->session->userdata('bizzmanufac')->id != true) {
			if ($redirect != NULL) {
				redirect($redirect);
			} else {
				redirect('vendor-login');
			}
		}
	}
	public function pageaccess($callcontroller) {
		$CI = &get_instance();
		if (in_array(base_url($callcontroller), $CI->session->userdata('url'))) {

			return true;
		} else {
			redirect('user');
		}
	}
	public function UserValidation($redirect = NULL){
		$CI = &get_instance();
		if ($CI->session->userdata('bizzgainmember')->id=='') {
			if ($redirect != NULL) {
				redirect($redirect);
			} else {
				$CI->session->set_flashdata('item','We are a wholesale platform, In order to proceed further, request to log in to your account');
				redirect('login');
			}
		}
	}
         public function get_current_total_amt($ordid)
	{ 
		$CI = &get_instance();
       $parameter= array('act_mode'=>'get_current_total','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $data = $CI->supper_admin->call_procedure('proc_orderflow',$parameter);
        foreach ($data as $key => $value) {
        	$amt=$value->base_prize * $value->ProQty;
        	$vattax=number_format(($amt * $value->protaxvat_p)/100,1,".","");
        	$cst_tax=number_format(($amt * $value->protaxcst_p)/100,1,".","");
        	$entry_tax=number_format(($amt * $value->protaxentry_p)/100,1,".","");
        	$total_amts+=$amt+$vattax+$cst_tax+$entry_tax;
        }
        $total_amt=number_format($total_amts,1,".","");
        return $total_amt;
	}

	public function get_man_current_total_amt($ordid,$manlogid)
	{ 
		$CI = &get_instance();
       $parameter= array('act_mode'=>'get_current_total','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>$manlogid,'order_qty'=>'','oldstatus'=>'');
        $data = $CI->supper_admin->call_procedure('proc_manorderflow',$parameter);
        foreach ($data as $key => $value) {
        	$amt=$value->base_prize * $value->ProQty;
        	$vattax=number_format(($amt * $value->protaxvat_p)/100,1,".","");
        	$cst_tax=number_format(($amt * $value->protaxcst_p)/100,1,".","");
        	$entry_tax=number_format(($amt * $value->protaxentry_p)/100,1,".","");
        	$total_amts+=$amt+$vattax+$cst_tax+$entry_tax;
        }
        $total_amt=number_format($total_amts,1,".","");
        return $total_amt;
	}



	public function get_current_total_amt_partialpay($ordid)
	{ 
		$CI = &get_instance();
       $parameter= array('act_mode'=>'get_current_total_partpay','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $data = $CI->supper_admin->call_procedure('proc_orderflow',$parameter);
        foreach ($data as $key => $value) {
        	$amt=$value->base_prize * $value->ProQty;
        	$vattax=number_format(($amt * $value->protaxvat_p)/100,1,".","");
        	$cst_tax=number_format(($amt * $value->protaxcst_p)/100,1,".","");
        	$entry_tax=number_format(($amt * $value->protaxentry_p)/100,1,".","");
        	$total_amts+=$amt+$vattax+$cst_tax+$entry_tax;
        }
        $total_amt=number_format($total_amts,1,".","");
        return $total_amt;
	}


	public function get_current_total_amt_returnord($ordid,$ordcase)
	{ 
		$CI = &get_instance();
       $parameter= array('act_mode'=>'get_current_total_return','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>$ordcase.'%');
        $data = $CI->supper_admin->call_procedure('proc_orderflow',$parameter);
        foreach ($data as $key => $value) {
        	$amt=$value->base_prize * $value->ProQty;
        	$vattax=number_format(($amt * $value->protaxvat_p)/100,1,".","");
        	$cst_tax=number_format(($amt * $value->protaxcst_p)/100,1,".","");
        	$entry_tax=number_format(($amt * $value->protaxentry_p)/100,1,".","");
        	$total_amts+=$amt+$vattax+$cst_tax+$entry_tax;
        }
        $total_amt=number_format($total_amts,1,".","");
        return $total_amt;
	}

	public function get_current_total_amt_cancelord($ordid)
	{ 
		$CI = &get_instance();
       $parameter= array('act_mode'=>'get_current_total_cancel','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $data = $CI->supper_admin->call_procedure('proc_orderflow',$parameter);
        foreach ($data as $key => $value) {
        	$amt=$value->base_prize * $value->ProQty;
        	$vattax=number_format(($amt * $value->protaxvat_p)/100,1,".","");
        	$cst_tax=number_format(($amt * $value->protaxcst_p)/100,1,".","");
        	$entry_tax=number_format(($amt * $value->protaxentry_p)/100,1,".","");
        	$total_amts+=$amt+$vattax+$cst_tax+$entry_tax;
        }
        $total_amt=number_format($total_amts,1,".","");
        return $total_amt;
	}

}
?>