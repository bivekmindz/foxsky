<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/libraries/REST_Controller.php';
class Discount extends REST_Controller{

  function discounton_post(){
    // $data = json_decode(file_get_contents("php://input"), true);
    $parameter=array(
    	              'act_mode'=>'viewmode',
    	              'discounton'=>$this->post('discounton'),
    	              'textinput'=>$this->post('textinput')
    	            );
    $data = $this->model_api->call_procedure('proc_Discounton', $parameter);
    if(!empty($data)){
      $data   = $this->send_json($data);  
      $this->response($data, 202); 
    }
    else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    } 
  }
  public function username_post(){
    $parameter=array('username'=>$this->post('username'));
   $data=$this->model_api->call_procedure('proc_User',$parameter);
    if(!empty($data)){
      $data   = $this->send_json($data);  
      $this->response($data, 202); 
    }
    else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    } 
  }
  function discountpost_post(){
  	//insert data using API ///////////////////
  	$parameter = array(
            'couponname' =>$this->post('couponname'),
            'couponcode' =>$this->post('couponcode'),
            'discamount' =>$this->post('discamount'),
            'disctype'   =>$this->post('disctype'),
            'coupontype' =>$this->post('coupontype'),
            'discon'     =>$this->post('discon'),
            'discountid' =>$this->post('discountid'),
            'startdate'  =>$this->post('startdate'),
            'enddate'    =>$this->post('enddate'),
            'shopamount' =>$this->post('shopamount'),
            'usertype'   =>$this->post('usertype'),
            'usespercoupon'=>$this->post('usespercoupon'),
            'vendorid'   =>$this->post('vendorid'),
            'userid'     =>$this->post('userid'),
            'status'     =>$this->post('status'),
            'reusable'   =>$this->post('reusable'),
            'freeship'   =>$this->post('freeship'),
            'CreatedBy'  =>$this->session->userdata('royzezadmin')->LoginID
            );
    $data = $this->model_api->call_procedureRow('proc_Coupon', $parameter);
    if(!empty($data)){
      $data   = $this->send_json($data);  
      $this->response($data, 202); 
    }
    else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    } 
  }
  function discountlist_get(){
    $parameter=array('action'=>'viewall','rowid'=>'0');
    $data = $this->model_api->call_procedure('proc_DiscountList',$parameter);
    if(!empty($data)){
      $data   = $this->send_json($data);  
      $this->response($data, 202); 
    }
    else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }
  function couponlist(){
    $parameter=array('act_mode'=>'view','couponid'=>$this->get('cid'));
    $data=$this->model_api->call_procedureRow('proc_Couponlist',$parameter);
    if(!empty($data)){
      $data   = $this->send_json($data);  
      $this->response($data, 202); 
    }
    else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }
  function calculation($totalamt,$DiscountType,$disamt){
      if($DiscountType=='fixed')
        $discamt=$disamt;
      else
        $discamt=($totalamt*$disamt)/100;
      return $discamt;
  }
  function applycoupon_post(){
     $prameter=array('promocode'=>$this->post('promocode'),'user_id'=>$this->post('userid'));
     $data=$this->model_api->call_procedureRow('proc_ApplyCoupon',$prameter);
    if(!empty($data)){
    $array=array('discountOn'=>$data->DiscountOn,'cop_id'=>$data->copid,'purchaseamt'=>$data->PurchaseAmt,'discounttype'=>$data->DiscountType,'discountamt'=>$data->DiscountAmt,'usertype'=>$data->UserType,'applyondiscounted'=>$data->ApplyOnDiscounted,'coupontypeid'=>$data->CouponTypeId,'DiscountOnMaxAmt'=>$data->DiscountOnMaxAmt,'MaxPurchaseAmt'=>$data->MaxPurchaseAmt);
      $data=(json_decode(json_encode($array)));
      $data   = $this->send_json($data);  
      $this->response($data, 202); 
    }
    else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  
}

function applycoupon_checkdiscountid_post(){
  $paramiter=array('cop_id'=>$this->post('cop_id'),'discounton_id'=>$this->post('discounton_id'),'discount_on'=>$this->post('discount_on'),'brand_id'=>$this->post('brand_id'),'vendor_id'=>$this->post('vendor_id'));
  $discountedcatid=$this->model_api->call_procedureRow('proc_ApplyCoupon_chkdsicountinid',$paramiter);
  if(!empty($discountedcatid)){
    $array=array('discountedid'=>$discountedcatid->copid);
  }else{
    $array=array('discountedid'=>'');
  }
  $data   =(json_decode(json_encode($array)));
  $data   = $this->send_json($data);  
  $this->response($data, 202); 
}

function applycuponvalid_post()
{

  $parameter = array('p_userid'=>$this->post('p_userid'),'couponcode'=>$this->post('couponcode'));
  //$this->response($parameter, 202); exit;
  
 
 if(!empty($parameter))
 {
    $data_c = $this->model_api->call_procedureRow('proc_couponvalidapply',$parameter);
    $data   = $this->send_json($data_c);  
    $this->response($data, 202);
 }else{
    $data = '';
    $this->response($data, 202);
 }
  
  
}

function specificcategorymaster_post()
{
    $parameter = array('actmode'=>$this->post('actmode'), 'id'=> $this->post('id')); 

    //$this->response($parameter, 202);  

    $data =  $this->model_api->call_procedureRow('proc_categorydiscount',$parameter);
    if(!empty($data))
    {
       
       $data1   = $this->send_json($data);
       $this->response($data1, 202);  
    
    }else{

      $data = '';
      $this->response($data, 202);  
    }
    
}

}
?>