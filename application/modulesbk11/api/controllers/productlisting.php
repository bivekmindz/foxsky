<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
 class Productlisting extends REST_controller{


function categoryName_get(){
 $parameter = array('act_mode'=>'categoryn','row_id' =>$this->get('catid'),'pvalue'=>'');
 $data = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);
 if(!empty($data)){
 $this->response($data, 202); 
 }
else{
 $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }
}

function groupbrandname_get(){
 $parameter = array('act_mode'=>'brandgr','row_id' =>'','pvalue'=>$this->get('brandid'));
 $data = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);	
 if(!empty($data)){
  $this->response($data, 202); 
 }
 else{
  $this->response("Something Went Wrong", 400);
 }
}

function categoryCount_get(){
 $parameter = array('act_mode'=>'cpcount','row_id' =>$this->get('catid'),'pvalue'=>'');

 $data = $this->model_api->call_procedure('proc_productmasterdata', $parameter);
//p($this->response($data, 202));exit;	
	if(!empty($data)){
	$this->response($data, 202); 
	}
	else{
	$this->response("Something Went Wrong", 400);
	}
 
}

function categoryCount2_get(){
 $parameter = array('act_mode'=>'cpcount','row_id' =>$this->get('catid'),'pvalue'=>'');

 $data = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);
//p($this->response($data, 202));exit;  
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400);
  }
 
}
function filtercategoryCount2_get(){
 $parameter = array('act_mode'=>'searchcpcount','row_id' =>$this->get('catid'),'pvalue'=>'');

 $data = $this->model_api->call_procedure('proc_productmasterdata', $parameter);
//p($this->response($data, 202));exit;  
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400);
  }
 
}
function wishlistCount_get(){
 $parameter = array('CatId' => $this->get('uid'),'promapId' =>'','procityid' =>'','act_mode' => 'wish_count');
 $data = $this->model_api->call_procedureRow('proc_relatedProducts', $parameter); 
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400);
  }
 
}

function parentcatId_get(){
 $parameter = array('act_mode'=>'parentid','row_id' =>$this->get('catid'),'pvalue'=>'');
 $data = $this->model_api->call_procedure('proc_productmasterdata', $parameter);
 if(!empty($data)){
  $this->response($data, 202); 
 }
 else{
   $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
 }

  }
function categoryBrand_get(){
 $parameter = array('act_mode'=>'brandview','row_id' =>$this->get('catid'),'pvalue'=>'');
 $data = $this->model_api->call_procedure('proc_productmasterdata', $parameter);
 if(!empty($data)){
  $this->response($data, 202); 
 }
 else{
   $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
 }	
}

function categoryBrandes_post(){

  $parameter = array('act_mode'=>$this->post('act_mode'),'row_id'=>$this->post('row_id'),'pvalue'=>$this->post('pvalue'));
 //$this->response($parameter, 202);exit;
 $data = $this->model_api->call_procedure('proc_productmasterdata', $parameter);
 if(!empty($data)){
  $this->response($data, 202); 
 }
 else{
   $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
 }  
}

function categoryPrice_get(){
 $parameter = array('act_mode'=>'priceview','row_id' =>$this->get('catid'),'pvalue'=>'');
 $data = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);
 if(!empty($data)){
  $this->response($data, 202); 
 }
 else{
   $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
 }
}

function getPdetail_get(){
$parameter = array( 'proId' => $this->get('name'),
                    'procityid' => $this->get('cityidd') 
                     );
//pend( $this->response($parameter, 202));
$data = $this->model_api->call_procedureRow('proc_GetProductDetails', $parameter);
//pend( $this->response($data, 202));
 if(!empty($data)){
  $this->response($data, 202); 
}
else{
 $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}

function productfeatures_post(){
  $parameter = array('act_mode'=>'featureview','row_id' => $this->post('proId'),'');
  $data = $this->model_api->call_procedure('proc_productmasterdata',$parameter);
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }

  }


 function enforminsert_post(){
  $parameter=array('act_mode'   =>$this->post('act_mode'),
                   'row_id'     =>$this->post('row_id'),
                   'p_userid'   =>$this->post('p_userid'),
                   'p_email'    =>$this->post('p_email'),
                   'p_mobilenum'=>$this->post('p_mobilenum'),
                   'p_mas'      =>$this->post('p_mas'));
  
  $data = $this->model_api->call_procedure('proc_enquiry',$parameter);
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }

  } 


function productCatName_get(){
  $parameter = array('act_mode'=>'productcatname','row_id' =>$this->get('pid'),'pvalue'=>'');
  $data = $this->model_api->call_procedure('proc_productmasterdata', $parameter);
   if(!empty($data)){
     $this->response($data, 202); 
}
else{
    $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  }

function getsimilarProduct_get(){
$parameter = array('CatId' => $this->get('catId'),'promapId' => $this->get('promapId'),'procityid' => $this->get('cityidd'),'act_mode' => 'relpro');
//p($this->response($parameter ,200));exit;
$data      = $this->model_api->call_procedure('proc_relatedProducts', $parameter);
//p($this->response($data ,200));exit;
if(!empty($data)){
$this->response($data ,200);
}else{
$this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}

function getviewProduct_get(){
$parameter = array('CatId' => $this->get('catId'),'promapId' => $this->get('promapId'),'procityid' => $this->get('cityidd'),'act_mode' => 'proupdate');

$data      = $this->model_api->call_procedure('proc_relatedProducts', $parameter);
if(!empty($data)){
$this->response($data ,200);
}else{
$this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}


function proAvailCheck_get(){
$parameter = array( 'proId' => $this->get('name'),
                    'procityid' => $this->get('cityidd')
                    );
$data = $this->model_api->call_procedureRow('proc_GetProductDetails', $parameter);
if(!empty($data)){
$this->response($data , 202); 
}
else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
}


function procompare_get(){

$parameter = array( 
                  'act_mode' => 'viewcomparepro',
                  'row_id' => $this->get('cid'),
                  'pvalue' => $this->get('cmlid')
                  );
$data = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);

if(!empty($data)){
$this->response($data , 202); 
}
else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
}

function procheckqty_post(){
$parameter = array( 
                  'act_mode' => 'qtycheck',
                  'row_id' => $this->post('proid'),
                  'pvalue' => $this->post('cityidd')
                  );
$result = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);
if(!empty($result)){
$this->response($result , 202); 
}
else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
}

function procheckqtycombo_post(){
$parameter = array( 
                  'act_mode' => 'qtycheckcombo',
                  'row_id' => $this->post('proid'),
                  'pvalue' => $this->post('cityidd')
                  );
$result = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);
if(!empty($result)){
$this->response($result , 202); 
}
else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
}

function categoryPricesearch_get(){
 //$parameter = array('act_mode'=>'priceviewsearch','row_id' =>$this->get('catid'),'pvalue'=>$this->get('token'));
$parameter = array('act_mode'=>'priceviewsearch','row_id' =>$this->get('catid'),'pvalue'=>str_replace("+", " ", $this->get('token')));
$data = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);
 if(!empty($data)){
  $this->response($data, 202); 
 }
 else{
   $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
 }
}

function advancesearch_post(){
 $parameter = array( 'mysearchval' =>$this->post('getval'),'act_mode'=>$this->post('act_mode'));
 $data = $this->model_api->call_procedure('proc_advancesearch2', $parameter);

if(!empty($data)){
  $this->response($data, 202); 
}
else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}



 
}
/*check wish list*/
function wishlist_check()
{
  $parameter=array('act_mode'=>$this->get('act_mode'),'userid'=>$this->get('userid'),'proid'=>$this->get('proid'),'c_id'=>$this->get('cityid'));
 $data=$this->model_api->call_procedureRow('proc_Addtowishlist',$parameter);
  if(!empty($data)){
   //$data   = $this->send_json($data);  
   $this->response($data, 202); 
 }
 else{
   $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
 }
}
function addedwishlist_get(){
 $parameter=array('act_mode'=>$this->get('act_mode'),'userid'=>$this->get('userid'),'proid'=>$this->get('proid'),'c_id'=>$this->get('cityid'));
 $data=$this->model_api->call_procedureRow('proc_Addtowishlist',$parameter);
  if(!empty($data)){
   //$data   = $this->send_json($data);  
   $this->response($data, 202); 
 }
 else{
  $data ='no data';
   $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
 }
}
function sendwishlists_get(){
 $parameter = array('act_mode'=>$this->get('act_mode'),'u_Id'=>$this->get('userid'),'p_Id'=>'','c_id'=>$this->get('cityid'));
 $data = $this->model_api->call_procedure('proc_Addtowishlist', $parameter);  
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400);
  }
 
}

function updatewishlist_post(){
 $parameter = array('act_mode'=>'uptodate_wish','u_Id'=>$this->post('userid'),'p_Id'=>'','c_id'=>$this->post('cityid'));
 $data = $this->model_api->call_procedure('proc_Addtowishlist', $parameter);
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400);
  }
 
}

function wishlist_get(){
 $parameter=array('act_mode'=>'viewwishlist','u_Id'=>$this->get('user_id'),'p_Id'=>$this->get('usercityid'),'c_id'=>'');
 //$this->response($parameter, 202);exit;
 $data=$this->model_api->call_procedure('proc_Addtowishlist',$parameter); 
  if(!empty($data)){
   $this->response($data, 202);
  }else{
    $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }
}

public function prolist_post(){
      $parameter     =array(
                          'proName'=>$this->post('proName'),
                          'category_id'=>$this->post('category_id'),
                          'barand_id'=>$this->post('barand_id'),
                          'size'=>$this->post('size'),
                          'color'=>$this->post('color'),
                          'minprice'=>$this->post('minprice'),
                          'maxprice'=>$this->post('maxprice'),
                          'mindiscount'=>$this->post('mindiscount'),
                          'pageorder'=>$this->post('pageorder'),
                          'pid'=>$this->post('pid'),
                          'perpage'=>$this->post('perpage'),
                          'features'=>$this->post('features'),
                          'procityidd'=>$this->post('procityidd') 
                          );
      //p($this->response($parameter, 202));exit();
      $data = $this->model_api->call_procedure('proc_Productlistnew',$parameter);
        if(!empty($data)){
            $this->response($data, 202);
            //p($this->response($data, 202));
        }else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
   }
function removewishlist_get(){
        $parameter = array( 'act_mode'=>'removeproduct',
                            'u_Id'  =>$this->get('wish_id'),
                            'p_Id'  => '',
                            'c_id'  => '' 
                          );
        $data=$this->model_api->call_procedureRow('proc_Addtowishlist', $parameter);
        if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
             $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
    }
function deletewishlist_get(){
        $parameter = array( 'act_mode'=>'deleteproduct',
                            'u_Id'  => $this->get('uid'),
                            'p_Id'  => $this->get('wish_id'),
                            'c_id'  => '' 
                          );
        
        $data=$this->model_api->call_procedureRow('proc_Addtowishlist', $parameter);
        if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
             $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
    }

function ordermail_get(){
$parameter=array('act_mode'=>'maildetaila','row_id'=>$this->get('orderid'),'vusername'=>'','vpassword'=>'','vstatus'=>'');
$data = $this->model_api->call_procedure('proc_memberlogin', $parameter);
 if(!empty($data)){
  $this->response($data, 202); 
}
else{
 $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}      

  function get_bredcum_get()
  {
      $parameter = array('CatId' => $this->get('catid'),'promapId' =>'','procityid' =>'','act_mode' => $this->get('act'));
      pend($this->response($parameter,200));

    // p($this->response($parameter ,200));exit;
      $data      = $this->model_api->call_procedure('proc_relatedProducts', $parameter);
      if(!empty($data)){
      $this->response($data ,200);
      }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }
  
  //----------------- start maincatdata------------------
function newcategoryCount2_get(){
 $parameter = array('act_mode'=>'cpcount','row_id' =>$this->get('catid'),'pvalue'=>'');

 $data = $this->model_api->call_procedure('proc_productmasterdata', $parameter);
//p($this->response($data, 202));exit;  
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400);
  }
 
}
//----------------- end maincatdata ---------------

function autoadsearch_post(){
  
  $parameter = array(
  'act_mode'=>$this->input->post('act_mode'),
  'token'=>$this->input->post('token'), 
  'proName'=>$this->input->post('proName'),
  'category_id'=>$this->input->post('category_id'),
  'barand_id'=>$this->input->post('barand_id'),
  'size'=>$this->input->post('size'),
  'color'=>$this->input->post('color'),
  'minprice'=>$this->input->post('minprice'),
  'maxprice'=>$this->input->post('maxprice'),
  'mindiscount'=>$this->input->post('mindiscount'),
  'pageorder'=>$this->input->post('pageorder'),
  'pid'=>$this->input->post('pid'),
  'perpage'=>$this->input->post('perpage'),
  'features'=>$this->input->post('features'),
  'procityidd'=>$this->input->post('procityidd')
  
  );
 //$parameter = array( 'mysearchval' =>$this->post('getval'),'act_mode'=>$this->post('act_mode'));
  //$this->response($parameter, 202);exit();
 $data = $this->model_api->call_procedure('proc_searchfilter', $parameter);

if(!empty($data)){
  $this->response($data, 202); 
}
else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}


 
}

  public function add_catdetails_post()
  {

    $parameter = array( 
      'act_mode'=>'addtocart_tbl',
      'u_Id'=> $this->post('userid'),
      'p_Id'=> $this->post('prodmap_id'),
      'qty'=>$this->post('qty'),
      'size_att'=>$this->post('size'),
      'color_att'=>$this->post('color'),
      'catimgthree'=>$this->post('guest_uniquekey'),''
      );
      //$this->response($parameter ,200);exit(); 
    $data=$this->model_api->call_procedureRow('proc_catimg', $parameter);
  }
  public function add_catdetailsapp_post()
  {
    $parameter = array( 
      'act_mode'=>'addtocartapp',
      'u_Id'=> $this->post('userid'),
      'p_Id'=> $this->post('prodmap_id'),
      'qty'=>$this->post('qty'),
      'size_att'=>$this->post('size'),
      'color_att'=>$this->post('color'),
      'catimgthree'=>'android',''
      );
    $data=$this->model_api->call_procedureRow('proc_catimg', $parameter);
    if($data->response==0) {      
      $this->response($data, 202);
    } else {
      $this->response($data, 202);
    }
  }
  public function cartlistingapp_post()
  {
    $parameter = array( 
      'act_mode'=>'cartlistingapp','row_id'=> $this->post('userid'),'','','','','',''
      );
    $data=$this->model_api->call_procedure('proc_catimg', $parameter);
    if(!empty($data)) {
      $this->response($data, 202); 
    } else {
       $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }
  public function update_catdetails_get()
  {
     $parameter = array( 'act_mode'=>'addtocart_tblupdate','u_Id'  => $this->get('userid'),'p_Id'  => $this->get('prodmap_id'),
    'qty'  =>$this->get('qty'));
     $data=$this->model_api->call_procedureRow('proc_Addtowishlist', $parameter);
  }
  public function del_catdetails_get()
  {
   $parameter = array( 'act_mode'=>'addtocart_tbldel','u_Id'  => $this->get('userid'),'p_Id'  => $this->get('prodmap_id'),'qty'  =>$this->get('qty'));
        
      $data=$this->model_api->call_procedureRow('proc_Addtowishlist', $parameter);
  }
  public function cartcount_get()
  {
    $parameter = array( 'act_mode'=>'count_cart','u_Id'  => $this->get('userid'),'p_Id'  =>'','qty'  =>'');
    $data=$this->model_api->call_procedureRow('proc_Addtowishlist', $parameter);
    if(!empty($data)){
          $this->response($data, 202); 
        }
        else{
             $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

  public function loadcartdtls_get()
  {
    $parameter = array( 'act_mode'=>'cart_details','u_Id'=> $this->get('userid'),'','');
    $data=$this->model_api->call_procedure('proc_Addtowishlist', $parameter);
    if(!empty($data)){
          $this->response($data, 202); 
        }
        else{
             $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

public function repeatorder_get()
  {
    $parameter = array( 'act_mode'=>'repeat_order','u_Id'=> $this->get('reordid'),'','');
    $data=$this->model_api->call_procedure('proc_Addtowishlist', $parameter);
    if(!empty($data)){
          $this->response($data, 202); 
        }
        else{
             $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

public function orderismailsend_get(){
$parameter=array('act_mode'=>$this->get('modename'),'row_id'=>$this->get('orderid'),'vusername'=>'','vpassword'=>'','vstatus'=>'');
$data = $this->model_api->call_procedure('proc_memberlogin', $parameter);
 if(!empty($data)){
  $this->response($data, 202); 
}
else{
 $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}  

public function comboPrice_get(){
 $parameter = array('act_mode'=>'pricecomboview','row_id' =>'','pvalue'=>'');
 $data = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);
 if(!empty($data)){
  $this->response($data, 202); 
 }
 else{
   $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
 }
}

public function getsimilarcomboProduct_get(){
$parameter = array(

        'act_mode'  =>  'relatedpro',
        'p_name'  =>  '',
        'p_qty'  =>  '',
        'p_start'  =>  '',
        'p_end'  =>  '',
        'p_ingridients'  => '' ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  '',
        'p_comboid'   => '',
        'p_cat_id'    =>  $this->get('catId'),
        'p_sub_catid' => $this->get('cityidd'),
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
     
      );


  
$data      = $this->model_api->call_procedure('proc_saveCombo', $parameter);
if(!empty($data)){
$this->response($data ,200);
}else{
$this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}

public function getviewProductcombo_get(){
$parameter = array(

        'act_mode'  =>  'mostviewpro',
        'p_name'  =>  '',
        'p_qty'  =>  '',
        'p_start'  =>  '',
        'p_end'  =>  '',
        'p_ingridients'  => str_replace('~', ',', $this->get('promapId')) ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  '',
        'p_comboid'   => '',
        'p_cat_id'    =>  $this->get('catId'),
        'p_sub_catid' => $this->get('cityidd'),
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
     
      );
//$this->response($parameter ,200);exit;

  
$data      = $this->model_api->call_procedure('proc_saveCombo', $parameter);
if(!empty($data)){
$this->response($data ,200);
}else{
$this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}

public function fetchtoyproducts_get(){
    if($this->get('brand')!=''){
      $brand      = $this->get('show_brand');
    }else{
      $brand     = 0;
    }
    if($this->get('price')!=''){
      $price       = explode('-',$this->get('price'));
      $minprice    = $price[0];
      $maxprice    = $price[1];
    }else{
      $minprice    = 0;
      $maxprice    = 0;
    }
    if($this->get('discount')!=''){
      $discount    = $this->get('discount');
    }else{
      $discount    = 0;
    }
    if($this->get('color')!=''){
      $color    = $this->get('color');
    }else{
      $color    = 0;
    }
    if($this->get('size')!=''){
      $size    = $this->get('size');
    }else{
      $size    = 0;
    }
    if($this->get('porder')!=''){
      $pageorder      = $this->get('porder');
    }else{
      $pageorder     = 0;
    }
    if($this->get('offset')!=''){
      $offset = $this->get('offset');
    }else{
      $offset = 0;
    }
    $parameter = array(
            'act_mode'  =>  'fetchproducts',
            'p_name'  =>  $this->get('type'),
            'p_qty'  =>  $brand,
            'p_start'  =>  $minprice,
            'p_end'  =>  $maxprice,
            'p_ingridients'  => $discount,
            'p_about'  =>  $color,
            'p_totalqty'  =>  '',
            'p_comboActPrice'  =>  '',
            'p_disPer'  =>  '',
            'p_comb_sellingprice'  =>  '',
            'p_totalsaving'  =>  '',
            'p_combo_img'   =>  $size,
            'p_comboid'   => $pageorder,
            'p_cat_id'    =>  '',
            'p_sub_catid' => '',
            'p_product_id'    =>  '',
            'p_product_qty'    =>  '',
            'p_pro_mrp'    =>  '',
            'p_pro_seling'    =>  '',
            'p_pro_combo_price'    =>  $offset         
          );
    $data = $this->model_api->call_procedure('proc_saveCombo', $parameter);
    if(!empty($data)){
      $this->response($data ,200);
    }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
}


/*public function loadcartdtlsapp_get()
  {
    $parameter = array( 'act_mode'=>'cart_detailsapp','u_Id'=> $this->get('userid'),'','');
    $record=$this->model_api->call_procedure('proc_Addtowishlist', $parameter);
    //print_r($record);exit;
    $cartdata=array();
    foreach ($record as $key => $value) {
    $parameterv = array( 'proId' => $value->promapid,
                    'procityid' => $this->get('cityidd')
                    );
    $data['prodetails'] = $this->model_api->call_procedureRow('proc_GetProductDetails', $parameterv);
    $data['cartdetails']=$value;
    array_push($cartdata, $data);
    }

    if(!empty($cartdata)){
          $this->response($cartdata, 202); 
        }
        else{
             $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }*/


  public function loadcartdtlsapp_get()
  {
    $parameter = array( 'act_mode'=>'cart_detailsapp','u_Id'=> $this->get('userid'),'','');
    $record=$this->model_api->call_procedure('proc_Addtowishlist', $parameter);

    $parameteruser = array( 'act_mode'=>'cart_appmemberdetail','u_Id'=> $this->get('userid'),'','');
    $userdata=$this->model_api->call_procedureRow('proc_Addtowishlist',$parameteruser);

    //p($record);exit;
    $cartdata=array();
    foreach ($record as $key => $value) {
    $parameterv = array( 'proId' => $value->promapid,
                    'procityid' => $this->get('cityidd')
                    );
    $data['prodetails'] = $this->model_api->call_procedureRow('proc_GetProductDetails', $parameterv);
    
    $parametertax=array('act_mode'=>'frontgetvalue',
                        'row_id'=>'', // brandid
                        'taxvalue'=>'',
                        'countryid'=>'',
                        'stateid'=>$this->get('stateid'), //stateid
                        'baseprice'=>'',
                        'localvat'=>$data['prodetails']->catid, // categoryid
                        'vatcat'=>'',
                        'vatnoncat'=>'',
                        'octorytax'=>''
                        );
    $protaxdata=$this->model_api->call_procedure('proc_tax',$parametertax);

    foreach ($protaxdata as $k => $valtax) {
      if($userdata->tinid==$valtax->taxidd){
        $tintaxpercent=$valtax->taxprice;
      }
      if($userdata->cstid==$valtax->taxidd){
        $csttaxpercent=$valtax->taxprice;
      }
      if($valtax->taxidd=='5'){
        $entrytaxpercent=$valtax->taxprice;
      }
    }

    $data['taxpercentages']=array('promapid' => $value->promapid,'tintaxpercent' => $tintaxpercent,'csttaxpercent' => $csttaxpercent,'entrytaxpercent' => $entrytaxpercent);
    $data['cartdetails']=$value;
    array_push($cartdata, $data);
    }

    if(!empty($cartdata)){
          $this->response($cartdata, 202); 
        }
        else{
             $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

  public function appcompareproduct_get()
  {
    $ids   = trim($this->get('idss'),",");
    $comid = explode(",",$ids);
    $record['procomp']=array();
    $record['viewattdata']=array();
    $cityidd=$this->get('cmid');
    for ($i=0; $i < count($comid); $i++) { 

      $parameter = array( 
                  'act_mode' => 'viewcomparepro',
                  'row_id' => $comid[$i],
                  'pvalue' => $cityidd
                  );
      $data['viewprocompare'] = $this->model_api->call_procedureRow('proc_productmasterdata', $parameter);

      array_push($record['procomp'], $data['viewprocompare']);
      
    }
    foreach ($record['procomp'] as $key => $value) {
      
      $catid[]= $value->CatId;
      $proid[]= $value->prodtmappid;
    }

    $finalcatid=implode($catid, ',');
    $parameter            =array('act_mode'=>'catatt','row_id'=>'','catid'=>$finalcatid);
    $record['attname'] = $this->model_api->call_procedure('proc_product',$parameter); 

    $finalproid=implode($proid, ',');
    foreach ($record['attname'] as $key => $value) {
      
     // $parameter1            =array('act_mode'=>'compcatatt','row_id'=>$value->id,'catid'=>$finalproid);
      $parameter1=array('act_mode'=>'compcatatt','row_id'=>$value->id,'p_userid'=>'','p_email'=>$finalproid,'p_mobilenum'=>'','p_mas'=>'');
      $data['attrvalue'] = $this->model_api->call_procedure('proc_enquiry',$parameter1);
      array_push($record['viewattdata'], $data['attrvalue']);

    }

    if(!empty($record['procomp'])){
      $this->response($record, 202); 
    }
    else{
         $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  function enforminsertapp_post(){
  $parameter=array('act_mode'   =>'insert',
                   'row_id'     =>$this->post('proid'),
                   'p_userid'   =>$this->post('userid'),
                   'p_email'    =>'',
                   'p_mobilenum'=>'',
                   'p_mas'      =>$this->post('message'));
  
  $data = $this->model_api->call_procedureRow('proc_enquiry',$parameter);
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }

  }


  function appsubcatfilter_get(){
  $cid=$this->get('catid');
  //$parametercat = array('act_mode'=>'cpcount','row_id' =>$this->get('catid'),'pvalue'=>'');
  //$data['subcategory'] = $this->model_api->call_procedure('proc_productmasterdata', $parametercat);
  $parameterpar = array('act_mode'=>'parentid','row_id' =>$cid,'pvalue'=>'');
  $data['categorylist'] = $this->model_api->call_procedure('proc_productmasterdata', $parameterpar);
  $parameterbrand = array('act_mode'=>'brandview','row_id' =>$cid,'pvalue'=>'');
  $data['brandlist'] = $this->model_api->call_procedure('proc_productmasterdata', $parameterbrand);
  $parameterpri = array('act_mode'=>'priceview','row_id' =>$cid,'pvalue'=>'');
  $data['pricelist'] = $this->model_api->call_procedureRow('proc_productmasterdata', $parameterpri);
  $parameterdis = array('act_mode'=>'discount','','');
  $data['fildiscount'] = $this->model_api->call_procedure('proc_productmasterdata',$parameterdis);
  $parametercolor            = array('act_mode'=>'catatt','row_id'=>'','catid'=>$cid);
  $data['coloratt']     = $this->model_api->call_procedure('proc_product',$parametercolor);

  foreach($data['coloratt'] as $k=>$v){
  $excelHeadArr[]    = $v->attname;
  $excelHeadArrcvc[] = $v->id;
  $allArr[]          = $v->attname;
  $grouparr[$k]      = $v->id;
  }
  foreach ($excelHeadArrcvc as $key => $value) {
    $parameteratt = array('act_mode'=>'attvalue','row_id'=>$value,'pvalue'=>$cid);
    $record['attData'][$grouparr[$key]] =$this->model_api->call_procedure('proc_productmasterdata',$parameteratt);
    $j         = 0;
  }
  $c = 0;
  foreach($record['attData'] as $k=>$v){ 
   foreach($v as $kk=>$vv){
   if($k == $vv->attid){
    $mainArr[$allArr[$c]][] = array('attid'=>$vv->attmapid,'attname'=>$vv->attvalue); 
     }
  }
    $c++;
  }
  $data['attrMapData'] = $mainArr;

  $data['sortby']= array('new'=>4,'discount'=>3,'low_price'=>2,'high_price'=>1);
  
  //p($data);
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }

  }

  function appmaincatfilter_get(){
  $parametercat = array('act_mode'=>'cpcount','row_id' =>$this->get('catid'),'pvalue'=>'');
  $data['categorylist'] = $this->model_api->call_procedure('proc_productmasterdata', $parametercat);
  foreach($data['categorylist'] as $key=>$cfilter1){
    $pcatcid[]=$cfilter1->childid;
  }

  $cid=implode(',',$pcatcid);
  $parameterbrand = array('act_mode'=>'brandview','row_id' =>$cid,'pvalue'=>'');
  $data['brandlist'] = $this->model_api->call_procedure('proc_productmasterdata', $parameterbrand);
  $parameterpri = array('act_mode'=>'priceview','row_id' =>$cid,'pvalue'=>'');
  $data['pricelist'] = $this->model_api->call_procedureRow('proc_productmasterdata', $parameterpri);
  $parameterdis           = array('act_mode'=>'discount','','');
  $data['fildiscount'] = $this->model_api->call_procedure('proc_productmasterdata',$parameterdis);
  $parametercolor            = array('act_mode'=>'catatt','row_id'=>'','catid'=>$cid);
  $data['coloratt']     = $this->model_api->call_procedure('proc_product',$parametercolor);
  
  foreach($data['coloratt'] as $k=>$v){
    $excelHeadArr[]    = $v->attname;
    $excelHeadArrcvc[] = $v->id;
    $allArr[]          = $v->attname;
    $grouparr[$k]      = $v->id;
  }
  foreach ($excelHeadArrcvc as $key => $value) {
    $parameteratt = array('act_mode'=>'pcatattvalue','row_id'=>$value,'pvalue'=>$cid);
    $record['attData'][$grouparr[$key]] =$this->model_api->call_procedure('proc_productmasterdata',$parameteratt);
    $j         = 0;
  }
  $c = 0;
  foreach($record['attData'] as $k=>$v){ 
   foreach($v as $kk=>$vv){
   if($k == $vv->attid){
    //$mainArr[$allArr[$c]][$vv->attmapid] = $vv->attvalue;
    $mainArr[$allArr[$c]][] = array('attid'=>$vv->attmapid,'attname'=>$vv->attvalue);
   }
  }
    $c++;
  }
  $data['attrMapData'] = $mainArr;
  $data['sortby']= array('new'=>4,'discount'=>3,'low_price'=>2,'high_price'=>1);
 
  if(!empty($data)){
  $this->response($data, 202); 
  }
  else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }

  }

  public function del_appcatdetails_get()
  {
   $parameter = array( 'act_mode'=>'addtocart_tbldel','u_Id'  => $this->get('userid'),'p_Id'  => $this->get('prodmap_id'),'qty'  =>$this->get('qty'));
        
      $data=$this->model_api->call_procedureRow('proc_Addtowishlist', $parameter);

  if(!empty($data)){
  $this->response(array("status"=>"cart product deleted successfully"), 202); 
  }
  else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }
  }

  public function apprepeatorder_post()
  {

    $parameter = array( 'act_mode'=>'repeat_order','u_Id'=> $this->post('orderid'),'','');
    $record=$this->model_api->call_procedure('proc_Addtowishlist', $parameter);

    foreach ($record as $key => $valuecart) {

    $parameter = array( 
      'act_mode'=>'appaddtocart_tbl',
      'u_Id'=> $this->post('userid'),
      'p_Id'=> $valuecart->ProId,
      'qty'=>$valuecart->OrdQty,
      'size_att'=>$valuecart->OrdSize,
      'color_att'=>$valuecart->OrdColor,'',''
      );
    $data=$this->model_api->call_procedureRow('proc_catimg', $parameter);
    
    }

    if(!empty($record) && !empty($this->post('orderid')) && !empty($this->post('userid'))){
    $this->response(array("status"=>"Product add in cart successfully"), 202); 
    }
    else{
    $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  }


  public function maincatprolistapp_post(){
      
      $oldcid=$this->post('category_id');
      $catfilter = maincatList2((int)$oldcid);

      foreach ($catfilter as $key => $value) {
        $valuenew = (object) $value;
        $pcatcid[]=$valuenew->childid;
      }
      $cid=implode(',',$pcatcid);
      //p($cid);
      //exit;

      $parameter     =array(
                          'proName'=>0,
                          'category_id'=>$cid,
                          'barand_id'=>$this->post('barand_id'),
                          'size'=>$this->post('size'),
                          'color'=>$this->post('color'),
                          'minprice'=>$this->post('minprice'),
                          'maxprice'=>$this->post('maxprice'),
                          'mindiscount'=>$this->post('mindiscount'),
                          'pageorder'=>$this->post('pageorder'),
                          'pid'=>$this->post('pid'),
                          'perpage'=>6,
                          'features'=>0,
                          'procityidd'=>$this->post('procityidd') 
                          );
      //p($this->response($parameter, 202));exit();
      $data = $this->model_api->call_procedure('proc_Productlistnew',$parameter);
        if(!empty($data)){
            $this->response($data, 202);
            //p($this->response($data, 202));
        }else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
   }

  /* function appproductfeatures_get(){
    $parameter = array('act_mode'=>'featureview','row_id' => $this->get('proId'),'');
    $data = $this->model_api->call_procedure('proc_productmasterdata',$parameter);

    $FeatureName= array();
    $fullfeatuesdata = array();
    $featuesdata['body'] = array();
    $featuesdata['head'] = array();
    foreach ($data as $key => $value) {
      if(!empty($value->rightvalue)){
        array_push($FeatureName, $value->FeatureName);
      }
    }
    $actualFeatureName=array_unique($FeatureName);
    foreach ($actualFeatureName as $key => $value) {
      array_push($featuesdata['head'], $value);
      foreach ($data as $key => $val) {
        if($value==$val->FeatureName){
          if(!empty($val->rightvalue)){
            $fval=(array) $val;
            array_push($featuesdata['body'], $fval);
          }
        }
      }
      array_push($fullfeatuesdata, $featuesdata);
    }
    
   if(!empty($data)){
    $this->response($fullfeatuesdata, 202); 
    }
    else{
    $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  } */

  function appproductfeatures_get(){
    $parameter = array('act_mode'=>'featureview','row_id' => $this->get('proId'),'');
    $data = $this->model_api->call_procedure('proc_productmasterdata',$parameter);

    $FeatureName= array();
    $fullfeatuesdata = array();
    $featuesdata['head']=array();
    $featuesdata['body'] = array();
    foreach ($data as $key => $value) {
      if(!empty($value->rightvalue)){
        array_push($FeatureName, $value->FeatureName);
      }
    }
    
    $actualFeatureName=array_unique($FeatureName);
    
    foreach ($actualFeatureName as $key => $value) {
      $pp=array('name'=>$value);
      array_push($featuesdata['head'], $pp);
    }

    foreach ($actualFeatureName as $key => $value) {
      foreach ($data as $key => $val) {
        if($value==$val->FeatureName){
          if(!empty($val->rightvalue)){
            $fval=(array) $val;
            array_push($featuesdata['body'], $fval);
          }
        }
      }
    }
      array_push($fullfeatuesdata, $featuesdata);
    
   if(!empty($data)){
    $this->response($fullfeatuesdata, 202); 
    }
    else{
    $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  } 

}//end class
?>