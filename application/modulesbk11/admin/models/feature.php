<?php
class feature extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_feature($catid)
    {
    	  $query = $this->db->query("select flm.* from tbl_featureleftmaster as flm inner join tbl_categorymaster as c on flm.Catid=c.catid
           where c.catid=$catid");
        $data = $query->result();
        return $data;
    }

    public function get_right_feature($catid)
    {
    	 $query = $this->db->query("select frm.* from tbl_featurerightmaster as frm inner join tbl_featureleftmaster as flm on frm.FeatureLeftValueId=flm.FeatureLeftID where frm.FeatureLeftValueId=$catid");
        $data = $query->result();
        return $data;

    }

       public function get_left_right_feature($catid)
    {
          $query = $this->db->query("select flm.FeatureLeftID,flm.LeftValue,frm.RightFeatureID,frm.RightValue from tbl_featureleftmaster as flm inner join tbl_categorymaster as c on flm.Catid=c.catid
            inner join  tbl_featurerightmaster as frm on frm.FeatureLeftValueId=flm.FeatureLeftID
            
           where c.catid=$catid");
        $data = $query->result();
        return $data;
    }

  } ?>