<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">
         
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                    
                        <h2>View Vendors</h2>
                       
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>In this Section Admin can add Vendors details and view the Vendors list.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                            <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/vendor/addvendor"><button>ADD Vendor</button></a>
                                </div>
                                 <form method="post" action="">
                                  <input class="xls_download" type="submit" value="Download Excel" name="newsexcel">
                                </form> 
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <form method="post" action="">
                                 
                                <input type="submit"  name="submit" value="Delete">
                                <input type="submit"  name="submitstatus" value="Status"> 
                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span></th>
                                            <th>S:NO</th>
                                            <th>Vendor Code</th>
                                            <th>Vendor Name</th>
                                            <th>Vendor Email</th>
                                            <th>Vendor Contact Number</th>
                                            <th>Vendor Password</th>
                                            <th>Shop/Company Name</th>
                                            <th>Created On</th>
                                            <th>Action</th>


                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                    if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                       foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><input type='checkbox' name='attdelete[]' class='chkApp' value='<?=$value->id?>'> </td> 
                                           
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->vendorcode ;?></td>
                                            <td><?php echo $value->firstname.$value->lastname ;?></td>
                                            <td><?php echo $value->compemailid ;?></td>
                                            <td><?php echo $value->contactnumber ;?></td>
                                            <td><?php echo base64_decode($value->password);?></td>
                                            <td><?php echo $value->companyname ;?></td>
                                            <td><?php echo $value->createdon;?></td>
                                            <input type='hidden' name='attstatu[<?php echo $value->id;?>]'  value='<?=$value->rstatus?>'>
                                            <td><a href="<?php echo base_url()?>admin/vendor/vendorupdate/<?php echo $value->id?>" ><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo base_url()?>admin/vendor/vendordelete/<?php echo $value->id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/vendor/vendorstatus/<?php echo $value->id.'/'.$value->rstatus; ?>"><?php if($value->rstatus=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                 </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
    </script>