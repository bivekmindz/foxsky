<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">
         
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                    
                        <h2>View Consumers</h2>
                       
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view the Consumers list.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                               <!--    <form method="get" action="<?php echo base_url();?>admin/vendor/viewvendorretailsearch">
                                  <input style="padding: 5px 10px;font-size: 13px; margin-right: 10px;" size="40px" type="text" name="searchboxretail" id="searchboxretail" class="searchboxretail" placeholder="Search by Name, Email" value="<?php echo $_GET['searchboxretail']; ?>" >  
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Search" name="Searchrtl">
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" onclick='location.href="<?php echo base_url('admin/vendor/viewvendorretail'); ?>"' type="button" value="Reset" name="resetrtl">
                                </form>  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <form method="post" action="">
                                 
                               <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th>S:NO</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact Number</th>
                                            <th>Password</th>
                                            <th>Address</th>
                                           <th>Created On</th>
                                            <th>Action</th>


                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                    if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                       foreach ($vieww as $key => $value) { #p($value);
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->Name ;?></td>
                                            <td><?php echo $value->emailid ;?></td>
                                            <td><?php echo $value->phone ;?></td>
                                            <td><?php echo base64_decode($value->password);?></td>
                                            <td><?php echo $value->shipaddress;?></td>
                                           <td><?php echo $value->createdon;?></td>
                                            <td>
                                            <a href="<?php echo base_url()?>admin/vendor/vendordeleteretail/<?php echo $value->id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/vendor/vendorstatusretail/<?php echo $value->id.'/'.$value->status; ?>"><?php if($value->status=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                              <!--   <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div> -->
                                 </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>