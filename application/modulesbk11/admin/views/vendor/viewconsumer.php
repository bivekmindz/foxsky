<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">
         
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                    
                        <h2>View Consumers</h2>
                       
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>In this Section Admin can view the Consumers list.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                              <form method="post" action="">
                                  <input class="xls_download" type="submit" value="Download Excel" name="newsexcel">
                                </form> 
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S:NO</th>
                                            <th>Vendor Type</th>
                                            <th>Consumer Code</th>
                                            <th>Consumer Name</th>
                                            <th>Consumer Email</th>
                                            <th>Consumer Contact No.</th>
                                            <th>Consumer State</th>
                                            <th>Consumer City</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                    if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                         foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->vendortype;?></td>
                                            <td><?php echo $value->vendorcode;?></td>
                                            <td><?php echo $value->firstname.' '.$value->lastname;?></td>
                                            <td><?php echo $value->compemailid;?></td>
                                            <td><?php echo $value->contactnumber;?></td>
                                            <td><?php echo $value->statename;?></td>
                                            <td><?php echo $value->cityname;?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                 <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

  