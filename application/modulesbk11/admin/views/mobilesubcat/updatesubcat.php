<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Update Sub Category Stationary </h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add, edit and delete slider banners.</p>
                        </div>
                    </div>
                    <form method="post" action="" enctype="multipart/form-data">
                    <div class="page_box">
                    <div class='flashmsg'><?php echo $this->session->flashdata('messag'); ?></div>
                  

         <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text"> Category Type <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="type_id" id="type_id" onchange="get_main_cat();">
                <option value="">Select Category</option>
                <?php foreach ($cattype as $key => $value) {
                 ?>
                  <option value="<?php echo $value->type_id ?>" <?php if($view->ms_type_id==$value->type_id){echo 'selected';}?>><?php echo $value->type_title ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>

     <div class="sep_box">
      <div class="col-lg-6"  id="get_type_id">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Main Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="ms_parant_id" id="mcatidd" onclick="childcategory();" >
                <option value="">Select Category</option>
                <?php foreach ($parentCatdata as $key => $value) {
                 ?>
                  <option value="<?php echo $value->catid ?>" <?php if($view->ms_parent_id==$value->catid){echo 'selected';}?>><?php echo $value->catname ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>
        <div class="col-lg-6" id="childcat">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="ms_catid" id="catidd" >
                <option value="">Select Category</option>
                <?php 
                foreach ($childCatdata as $key => $value) {
                    ?>

                     <option value="<?php echo $value->catid;?>" <?php if($value->catid==$view->ms_cat_id){echo 'selected';}?>><?php echo $value->catname;?></option>
                
                <?php
            }
                ?>
                </select></div>
            </div>
        </div>
        </div>
          
        </div>
                            <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">sub category Image <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="imgslide" name="imgslide" type="file" /></div>
                                        <img height="60px" width="60px" src="<?php echo base_url();?>assets/mobilebanners/<?php echo $view->ms_img;?>">
                                    </div>
                                </div>
                            </div>
                        </div>   
                        
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    </form>



                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                         <!--        <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S:NO</th>
                                            <th>Mobile Slider Image</th>
                                          <th>Cat Type</th>
                                          <th>Parrent Cat </th>
                                          <th>Sub Cat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   <?php $i=0; foreach ($viewsub_catapp as $key => $value) { $i++; ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><img height="60px" width="60px" src="<?php echo base_url();?>assets/mobilebanners/<?php echo $value->ms_img;?>"></td>
                                            <td><?php echo $value->cattype;?></td>
                                            <td><?php echo $value->pcatname;?></td>
                                            <td><?php echo $value->scatname;?></td>
                                            
                                            
                                            <td><a href="<?php echo base_url()?>admin/addmobsubcat/updatestationarysubcat/<?php echo $value->ms_id?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/addmobsubcat/deletemobileslider/<?php echo $value->ms_id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/addmobsubcat/statusmobileslider/<?php echo $value->ms_id.'/'.$value->ms_status; ?>"><?php if($value->b_img_status=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                    <?php } ?> 
                                    </tbody>
                                </table>
                                 -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

<script>
function childcategory(){
    var catid=$('#mcatidd').val();
    if(mcatidd==0){
        $('#childcat').hide();
    }else{
        $('#childcat').show();
    }
     
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/catvalue',
        type: 'POST',
        //dataType: 'json',
        data: {'catid': catid},
        success: function(data){
             
            var  option_brand = '<option value="">Select Category</option>';
            $('#catidd').empty();
            $("#catidd").append(option_brand+data);
        }
    });
    
   }

   function get_main_cat(){
    var type_id=$('#type_id').val();
    //alert(type_id); return false;
    if(type_id==0){
        $('#get_type_id').hide();
    }else{
        $('#get_type_id').show();
    }
     
    $.ajax({
        url: '<?php echo base_url()?>admin/addmobsubcat/get_main_cat',
        type: 'POST',
        //dataType: 'json',
        data: {'type_id': type_id},
        success: function(data){
             //alert(data);return false;
            var  option_brand = '<option value="">Select Category</option>';
            $('#mcatidd').empty();
            $("#mcatidd").append(option_brand+data);
        }
    });
    
   }


</script>
