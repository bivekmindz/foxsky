<section class="main_container">
    <div id="overlay" class="web_dialog_overlay"></div>

    <?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10">
        <div class="row">
          <form action="" method="post" enctype="multipart/form-data" >
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>View Discount</h2>
                    </div>
                    <div class="page_box">
        <?php $arr = explode(',',$discount->products); ?>
        <div class="gridview">
            <table width="90%">
                <tr>
                    <td>Discount Name</td>
                    <td><?php echo $discount->t_disc_name; ?></td>
                    <td>Discount Type</td>
                    <td><?php echo $discount->DiscountType; ?></td>
                </tr>
                <tr>
                    <td>Between</td>
                    <td colspan="2">
                        <?php echo $discount->StartDate.' To '.$discount->EndDate;?>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Purchase Amount</td>
                    <td><?php echo $discount->PurchaseAmt?$discount->PurchaseAmt:'0';?></td>
                    <td>Discount Amount</td>
                    <td><?php echo $discount->DiscountAmt;?></td>
                </tr>
                <tr>
                    <td>Discount On</td>
                    <td><?php echo $discount->DiscountOn;?></td>
                    <td>City</td>
                    <td><?php //echo $discount->LocationId==0?'All':$discount->LocationId;
                      if( $discount->LocationId==1){
                        echo "Noida";
                      }
else if( $discount->LocationId==2){
                        echo "New Delhi";
                      }
else if( $discount->LocationId==3){
                        echo "Gurgoan";
                      }
else {
                        echo "All City";
                      }


                    ?></td>
                </tr>
                <tr>
                    <!-- <td>Re-Usable</td>
                    <td><?php echo $discount->Resuable;?></td> -->
                    <td>Status</td>
                    <td><?php echo $discount->DiscStatus==I?'In-Active':'Active';?></td>
                </tr>
                <!-- <tr>
                    <td>Products</td>
                    <td>
                        <?php foreach($arr as $k => $v) {
                            $abc = explode('|', $v);
                            echo $abc[0].' - '.$abc[1].'</br>';
                        }?>
                    </td>
                    <td></td>
                    <td></td>
                </tr> -->
            </table>
        </div>
    </div>
</section>