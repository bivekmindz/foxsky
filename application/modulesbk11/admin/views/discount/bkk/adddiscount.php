
<script type="text/javascript" src="<?php echo admin_url(); ?>js/discount.js"></script> 
<style> 
    .coupon{display: none;} 
    .username{display: none;} 
</style> 
<section class="main_container"> 
    <script> 
        $(function () { 
            $(".datepicker").datepicker({ 
                altField: "#alternate", 
                altFormat: "DD, d MM, yy", 
                dateFormat: "yy-mm-dd" 
            }); 
        }); 
    </script> 

    <div id="overlay" class="web_dialog_overlay"></div> 

    <div class="container">
    
        <div class="messagepop pop"> 
            <div id="discounted"></div>

<h6>Apply discount on above</h6> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="radio" name="status" value="1"> Yes
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="radio" name="status" checked="checked" value="2">No
&nbsp;&nbsp;
<input type="button" id="Cancel" name="Cancel" value="X-Close" onclick="return closepopup();" style="padding:5px 10px ; margin:10px 20px ;"> 
             <!-- 
            <input type="button" name="Reset" value="Reset" onclick="return updatediscount();"> 
            OR 
            <input type="button" name="Cancel" value="Cancel" onclick="return closepopup();"> 
            OR 
            <input type="button" name="Aditional" value="Additional" onclick="return aditional();">  -->
        </div> 

        <div class="heading"> 
            <h1>Discount</h1> 
        </div> 
        <div class="gridview"> 
            <table width="90%"> 
                <tr> 
                    <td width="12%">Discount</td> 
                    <td> 
                        <input type="radio" name="disType" value="fixed" onchange="addDiscountAttr($(this));"> Flat discount 
                    </td> 
                    <td> 
                        <input type="radio" name="disType" value="percentage" onchange="addDiscountAttr($(this));"> percentage discount 
                    </td> 
                </tr> 


               <!--  <tr> 
                    <td width="12%">Purchase Amount</td> 
                    <td colspan='2'> 
                        <input type="text" name="disPurchaseAmt" value=""> 
                    </td> 
                    
                </tr>  -->



                <tr> 
                    <td colspan="3" id="Attr"></td> 
                </tr> 
                <tr> 
                    <td colspan="3"> 
                        <input type="button" id="contact" name="save" value="Save" onclick="save_discount();"> 
                    </td> 
                </tr> 
            </table> 
        </div> 
    </div> 
</section> 
<script> 
var i=1;
    var baseurl = "<?php echo base_url(); ?>"; 
    function addDiscountAttr(elem) { 
        
        
        
        $.ajax({ 
            url: baseurl + "admin/discount/discountAttr", 
            type: "POST", 
            data: {'disType': elem.val()}, 
            success: function (data) { 
                //alert(data); 
                $('#Attr').html(data); 
            } 
        }); 
    } 

    function getList(elem) { 
        console.log(elem.val());

        $('#subAttributes').html(''); 
        $('#subCategories').html(''); 
        $('#products').html(''); 
       
        $('#discountTable').html(''); 
        $.ajax({ 
            url: baseurl + "admin/discount/getList", 
            type: "POST", 
            data: {'type' : elem.val()}, 
            success: function (data) { 
                 
                if (elem.val() == 'product') { 
                    $('#products').html(data); 
                } else {

                    $('#discountTable').html(data); 
                } 
            } 
        }); 
    } 

    function get_sub_cat(elem) { 
      
        var arr = new Array(); 
        var type = $('input[name=type]', elem.closest('tr').parent().parent()).val(); 
            
        $('.brandsArr', elem.closest('tr').parent().parent()).each(function () { 
            if ($(this).prop('checked')) { 
                arr.push($(this).val()); 
            } 
        }); 
        if (arr.length > 0) { 
            $.ajax({ 
                url: baseurl + "admin/discount/getSubList", 
                type: "POST", 
                data: {'type': type, 'catId': arr}, 
                success: function (data) { 
                        
                    if (type == 'categories') { 
                        $('#subAttributes').html(data); 
                    } else if (type == 'subcategory') { 
                        $('#subCategories').html(data); 
                    } else { 
                        $('#products').html(data); 
                    } 
                } 
            }); 
        } else { 
            alert('Select ' + type + ' First'); 
            return false; 
        } 
    } 

    function generateCode(elem) { 
        if (elem.prop('checked')) { 
            $.ajax({ 
                url: baseurl + "admin/discount/get_unique_code", 
                type: "POST", 
                data: {}, 
                success: function (data) { 
                    $('input:text[name=coupon_code]').val(data); 
                } 
            }); 
        } else { 
            $('input:text[name=coupon_code]').val(''); 
        } 
    } 

// code are  change  for approve discount product 

    function save_discount(){ 

        //var disPurchaseAmt = $('input[name=disPurchaseAmt]').val(); 
        var disPurchaseAmt = '0'; 

        //alert(disPurchaseAmt);return false;
        var baseurl         = "<?php echo base_url(); ?>"; 
        var disType         = $('input:radio[name=disType]:checked').val(); 
        var discount        = $('input[name=discount]').val(); 
        var purchaseVal     = $('input[name=min_purchase]').val(); 
        var dateFrom        = $('input[name=from]').val(); 
        var dateTo          = $('input[name=to]').val(); 
        var discountOn      = $('input:radio[name=apply]:checked').val(); 
        var coupon_code     = $('input[name=coupon_code]').val(); 
        var numberOfUses    = $('input[name=CouponUser]').val(); 
        var reusable        = $('input:checkbox[name=reusable]').prop('checked') ? 'yes' : 'no'; 
        var usableLimit     = $('input[name=usableLimit]').val(); 
        var discountName    = $('input[name=discountname]').val(); 
        var brandCatId      = new Array(); 
        var sThisVal        = new Array(); 
        var prod_cart       = $("#sel_pro_cart").val();
        var prod_cart_amt   = $("#cart_price").val();




        $('input:checkbox[name=user_select]').each(function () {
           //sThisVal = (this.checked ? $(this).val() : "");
           if(this.checked){
                sThisVal.push($(this).val());
            }
        });

        $('.brandsArr').each(function () { 
            if ($(this).prop('checked')) { 
                brandCatId.push($(this).val()); 
            } 
        }); 
        var productId = new Array(); 
        var users = ''; 
        $('#users option:selected').each(function () { 
            users = users + '|' + $(this).val(); 
        }); 

        if (!disType) { 
            alert('Select discount type'); 
            $('input:radio[name=disType]').focus(); 
            return false; 
        } 
        if (!discount) { 
            alert('Enter discount value'); 
            $('input[name=discount]').focus(); 
            return false; 
        } 
        if (!discountOn) { 
            alert('select brands, category or products'); 
            $('input:radio[name=apply]').focus(); 
            return false; 
        } 
        if (!discountName) { 
            alert('Enter Discount Name'); 
            $('input:text[name=discountname]').focus(); 
            return false; 
        } 
        var error = false; 
        var productIdDiscounted = new Array(); 
        $("input:checkbox[name=products]:checked").each(function () { 
            var ProId = $(this).val(); 
            var ProDis = $('input[name=CurrDiscount]', $(this).parent()).val();  //  get  product discount value , // current discount value discount
      
            if (ProDis > 0 && ProDis != discount) { 
                error = true; 
                productIdDiscounted.push(ProId); 
                 productId.push(ProId);  // added by  testing
            } else { 
                productId.push(ProId);
            } 
        }); 
        productId = $.unique(productId.sort()).sort(); 
        productIdDiscounted = $.unique(productIdDiscounted.sort()).sort(); 
        //alert(productId); return false;

        var arr = new Array(); 
        var p_status= $("input:radio[name=status]:checked").val();

        arr = {'coupon': coupon_code, 'purAmt': purchaseVal, 'numUses': numberOfUses, 'reusable': reusable, 'disType': disType, 'disVal': discount, 'disOn': discountOn, 'startDate': dateFrom, 'endDate': dateTo, 'users': users, 'products': productId, 'brandCatId': brandCatId, 'usableLimit': usableLimit, 'discname': discountName, 'prodCart':prod_cart, 'prodCartAmt':prod_cart_amt, 'userIds':sThisVal, 'disPurchaseAmt':disPurchaseAmt}; 
		//alert(arr);
        if(i==1){                    
                    $.ajax({ 
                        url: baseurl + "admin/discount/getdiscountproductname", 
                        type: "POST", 
                        data: { 'data': arr }, 
                       // dataType: "json",
                        success: function (data) {
                                //alert(data);

                                    if(($.trim(data)).length>0){
                                        $('#discounted').html(data);                                        
                                        openpopup($('#contact')); 
    									return false; 
                                        i++;         
                                    }else{
                                        $('input[name=status]').each(function () {
                                            if(this.value==1){
                                                $(this).prop('checked', true); // Checks it
                                            }else{
                                                $(this).prop('checked', false); // Checks it
                                            }
                                        });
                                        //alert('click');
                                         p_status = 1;
                                        $('#Cancel').click();
                                        $("#Cancel").trigger('click');
                                        document.getElementById("Cancel").click();
                                       
                                    }
                                 }, 
                        //traditional: true               
                        });
        i++;
        //return false;
        }
		i = 0;
		
        var discount = $('input[name=discount]').val(); 
               // var baseurl ="<?php echo base_url(); ?>"; 
                var disType = $('input:radio[name=disType]:checked').val(); 
                var productId = new Array(); 
                $("input:checkbox[name=disProducts]:checked").each(function () { 
                    var ProId = $(this).val(); 
                    productId.push(ProId); 
                }); 
                var brandCatId = new Array(); 
                $('.brandsArr').each(function () { 
                    if ($(this).prop('checked')) { 
                        brandCatId.push($(this).val()); 
                    } 
                });
        // var p1= $('input[name=status]:checked').val()


        //alert(p_status);        
        if(parseInt(p_status)==2){
	       productId=0;
        }
        if(parseInt(p_status)==1){
               //console.log(productId);
               //console.log(arr);
                $.ajax({ 
                        url: baseurl + "admin/discount/addparovedisc", 
                        type: "POST", 
                        data: {'data': arr,"productId":productId}, 
                        success: function (data) {
                            alert("Saved successfully.");
                            //console.log(data);
           					var rdirUrl = "<?php echo base_url(); ?>" + "admin/discount/discountlist"; 
        					window.location = rdirUrl;
							return false;
		                }
                }); 
        }
        // end comented code 
    } 

    function updatediscount() { 

        var discount = $('input[name=discount]').val(); 
        var baseurl ="<?php echo base_url(); ?>"; 
        var disType = $('input:radio[name=disType]:checked').val(); 
        var productId = new Array(); 
        $("input:checkbox[name=disProducts]:checked").each(function () { 
            var ProId = $(this).val(); 
            productId.push(ProId); 
        }); 
        var brandCatId = new Array(); 
        $('.brandsArr').each(function () { 
            if ($(this).prop('checked')) { 
                brandCatId.push($(this).val()); 
            } 
        }); 
     //   console.log(brandCatId);
        console.log(productId);
    //    console.log(brandCatId);
       // return false;
        if (productId.length > 0) { 
            $.ajax({ 
                url: baseurl + "admin/discount/save_discount", 
                type: "POST", 
                data: {'discount':discount, 'type':disType, 'products': productId, 'brandCatId': brandCatId}, 
                success: function (data) { 
                   // alert(data); 
                    alert('Update Successfully'); 
                    closepopup(); 
                    //Reload page 
                   // window.location.href = baseurl + "admin/discount/discountlist"; 
                    //location.reload(); 
                } 
            }); 
        } 
    } 

    function openpopup(elem) { 
        //alert(elem);
        if (elem.hasClass('selected')) { 
            deselect($(this)); 
        } else { 
            elem.addClass('selected'); 
            $('.pop').slideFadeToggle(); 
            $("#overlay").show(); 
            $('.messagepop').fadeIn(300); 
        } 
        //return false; 
    } 

    function closepopup() { 
		var rdirUrl = "<?php echo base_url(); ?>" + "admin/discount/discount";
	
        var p_status= $("input:radio[name=status]:checked").val();
		if(p_status == '2'){
			window.location = rdirUrl;
			return false;
		}
		
        /// uncheck products .
        $('input[name=disProducts]').each(function(){
             if( this.checked ) {
                  //alert( 'checked: ' + this.name + ' ' + this.value );
             } else {
                  var parVal = this.value;
                  $('input[name=products]').each(function(){
                    if(parVal == this.value){
                        //alert( this.value );
                        $(this).prop('checked', false);
                        //$('.prettycheckbox input').prop('checked', true);
                    }
                    
                  });   
             }
        });
        /// uncheck products .


        $('.pop').slideFadeToggle(function () { 
            $('#contact').removeClass('selected'); 
            $("#overlay").hide(); 
        }); 
        return false; 
    } 

    $.fn.slideFadeToggle = function (easing, callback) { 
        return this.animate({opacity: 'toggle', height: 'toggle'}, 'fast', easing, callback); 
    }; 


 
   

    function isNum(evt) { 
        var charCode = (evt.which) ? evt.which : event.keyCode 
        if (charCode > 31 && (charCode < 48 || charCode > 57)) { 
            alert('Please Enter only Numeric key') 
            return false; 
        } else { 
            return true; 
        } 
    } 
</script> 


<style> 
    a.selected { 
        background-color:#1F75CC; 
        color:white; 
        z-index:100; 
    } 

    .messagepop { 
        background-color:#FFFFFF; 
        border:2px solid #999999; 
        cursor:default; 
        display:none; 
        margin-top: 100px; 
        margin-left: 200px; 
        margin-right: 200px; 
        position:absolute; 
        text-align:center; 
        width:394px; 
        height: 350px; 
        z-index:102; 
        padding: 25px 25px 20px; 
        overflow:scroll; 
    } 

    label { 
        display: block; 
        margin-bottom: 3px; 
        padding-left: 15px; 
        text-indent: -15px; 
    } 

    .messagepop p, .messagepop.div { 
        border-bottom: 0px solid #EFEFEF; 
        margin: 8px 0; 
        padding-bottom: 8px; 
    } 

    .web_dialog_overlay { 
        position: fixed; 
        top: 0; 
        right: 0; 
        bottom: 0; 
        left: 0; 
        height: 100%; 
        width: 100%; 
        margin: 0; 
        padding: 0; 
        background-color: #585858; 
        opacity: .80; 
        filter: alpha(opacity=15); 
        -moz-opacity: .15; 
        z-index: 101; 
        display: none; 
    } 
</style>