<?php $this->load->view('helper/nav')?> 
<style type="text/css">
.download-left{
    width:150px;
    float: left;
    height: auto;
    margin-left: 10px;

}
.download-left input{
    width: 100%;
    text-align: center;
}
.download-right{
    width:150px;
    float: right;
    height: auto;
        margin-right: 10px;
    
}
.download-right input{
    width: 100%;
    text-align: center;
}
.delete-left{
    float: left;
    width: 24px;
    height: auto;
}
.delete-right{
    float: right;
    
    padding: 0;
    width: 46px;
}
.grid_tbl td {
    border-bottom: 1px solid #d8e8f5;
    border-right: 1px solid #d8e8f5;
    color: #676767;
    font-size: 13px;
    padding: 6px 4px;
}
.page_box form select{
    background: #fff none repeat scroll 0 0;
    border: 1px solid gray;
    color: black;
    padding: 9px 0 !important;
    width: 100% !important;
}


</style>

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
          <!-- <form action="" method="post" enctype="multipart/form-data" > -->
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Discount List</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can View the  Discount List.</p>
                        </div>
                    </div>

                           
                    <div class="page_box">
                         <!-- <form method="post" action="<?php echo base_url();?>admin/discount/search">
                            <div class="col-lg-3" id="input_div">
                            <div class="tbl_input">
                            <input type="text" name="search_term" placeholder ="Coupon,Disc Name, Disc Wise, Valid From, Valid to" value="" id="search_term" class="valid input_cls">
                            </div>
                            </div>
                            <div class="col-lg-3">
                            <div class="submit_tbl">
                            <input id="" type="submit" name="submit" value="Search" class="btn_button sub_btn">
                            <input type="button" onclick='location.href="<?php echo base_url('admin/discount/all_discount_list'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                            </div>
                            </div>
                             <div class="col-lg-3"><select id="filter_discount" name="filter_discount">
                                <option value="all_list">All</option>
                                <option value="brand">Brand</option>
                                <option value="product">Product</option>
                                <option value="category">category </option>
                            </select>
                        </div>
                        </form> -->
                     <form method="post" action="">
                                 <div class="download-left"> <!--<input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel"> -->
                                </div></form>
                      <form method="post" action="<?php echo base_url().'admin/discount/product_discount'; ?>">
                                 <div class="download-right"> <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Add Discount" name="newsexcel">
                                </div></form>           
                <!-- <table id="filterTable" width="100%" class="grid_tbl">
                    <tr>
                        <td width="22%"><input type="text" name="coupon_code" value="" placeholder="Enter Coupon Code/Name/Type" style="width:97%; padding:8px 2px; border:1px solid #ccc;"></td>
                        <td width="22%"><input type="text" name="start_date" class="datepicker" value="" placeholder="Enter start date" style="width:97%; padding:8px 2px; border:1px solid #ccc;" readonly="true"></td>
                        <td width="22%"><input type="text" name="end_date" class="datepicker" value="" placeholder="Enter end date" style="width:97%; padding:8px 2px; border:1px solid #ccc;" readonly="true"></td>
                        <td width="22%">
                            <select name="Status" style="width:100%;">
                                <option value="">Select Status</option>
                                <option value="A">Active</option>
                                <option value="I">In-Active</option>
                            </select>
                        </td>
                        <td><input type="button" name="filter" value="Filter" onclick="get_filtered_discount();"></td>
                    </tr>
                </table> -->
                <style type="text/css">
                select {
    background: #5285b1 none repeat scroll 0 0;
    border: medium none;
    color: #cee0f1;
    padding: 9px 0 !important;
    width: 100% !important;
}
th:nth-child(n+4) {
    padding: 0 !important;
}

#title_option{
    padding-top:15px;
}
#search_filter_length label select{
    width:inherit!important;
        padding: 5px 5px!important;
}

                </style>


            
            
         <div id="title_option" class="col-lg-12">
           <!--  <table id="all_list" class="grid_tbl"> -->
            <table id="search_filter" class="grid_tbl">
                <thead>
                    <tr>
                        <th scope="col">So.No.</th>
                        <th scope="col">Discount Name</th>
                        <th scope="col">Coupon Code</th>
                        <th scope="col">Coupon User Type</th>
                        <th scope="col">Discount On</th>
                        <th scope="col">Discount Wise</th>
                       <!-- <th scope="col" ><select id="discount_on" onchange="get_table($(this).val());">
                          <option value="none">Discount On</option>
                          <option value="brands">Brands</option>
                          <option value="category">Category</option>
                          <option value="products">Product</option>
                        </select> </th> -->
                         
                        <th scope="col">Discount Type</th>
                       <th scope="col">Discount Value</th>
                       <th scope="col">Fixed Discount Price</th> 
                       <th scope="col">Purchase Amount (Min)</th> 
                       <th scope="col">Purchase Amount (Max)</th> 
                        <th scope="col">Valid From</th>
                        <th scope="col">Valid to</th>
                        <th scope="col">From Time</th>
                        <th scope="col">To Time</th>
                        <th scope="col">Reusable Limit</th>
                        <th scope="col">Updated By</th>
                        <th scope="col">Updated Date</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">

                <?php
                    $i = 1;
                    foreach ($discount_detail AS $list) {
                ?>
                <tr>
                    <td data-rel="so.no"><?php echo $i; ?></td>
                    <td data-rel="so.no"><?php echo ucwords(strtolower($list->discount_name)); ?></td>
                    <td data-rel="so.no"><?php echo $list->coupon; ?></td>
                    <td data-rel="so.no"><?php if($list->coupon_user_type=='retail'){ echo 'Consumer'; } ?></td>
                    <td data-rel="so.no"><?php if($list->discount_wise == 'brands') { echo get_brand_names($list->brand_id) ;} else if($list->discount_wise == 'category'){ echo get_category_names($list->category_id);} else if($list->discount_wise == 'products'){ echo get_product_names($list->product_id);} ?></td>
                    <td data-rel="so.no"><?php echo ucfirst(strtolower($list->discount_wise)); ?></td>
                    
                    <td data-rel="so.no"><?php echo $list->discount_type; ?></td>                   
                    <td data-rel="Pi ID"><?php echo $list->discount_value; ?></td>
                    <td data-rel="Pi ID"><?php echo $list->fixed_discount_price; ?></td>
                    <td data-rel="Pi ID"><?php echo $list->purchase_amount; ?></td>
                    <td data-rel="Pi ID"><?php echo $list->purchase_amount_max; ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_from; ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_to; ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_from_time; ?>:00</td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_to_time; ?>:00</td>
                    <td data-rel="Supplier Number"><?php echo $list->reusable_limit; ?></td>
                    <td><?php if(empty($list->modifiedby)){ echo $list->ausername; } else { echo $list->rusername; } ?></td> 
                    <td><?php if(empty($list->modifiedby)){ echo $list->createdon; } else { echo $list->modifiedon; } ?></td>
                  
                <?php 
                       # echo $list->DiscStatus == 'A' ? 'Active' : 'In-Active';

                ?>
        <!-- <a href="<?php echo base_url(); ?>admin/discount/discount_new_activeinactive/<?=$list->Copid?>/<?php print ($list->DiscStatus=='I') ? 'active':'inactive';?>"
          <?php if($list->DiscStatus=='A'){ ?>class="inactive" <?php } else { ?>class="active_button"  <?php } ?>
          > <?php print ($list->DiscStatus=='I') ? 'Inactive':'Active';?></a> -->
                </td>
                <td data-rel="Action">
                                <?php if($list->status == 1) {  ?>
                                <a class="delete-right" title="Delete" href="<?php echo base_url();?>admin/discount/active_inactive/<?=$list->id?>/0" onclick="return confirm('Are you sure you want to Inactive this?');" >Active</a>
                                <?php } else if($list->status == 0) { ?>
                                <div class="delete-right"><a class="delete" title="Delete" href="<?php echo base_url();?>admin/discount/active_inactive/<?=$list->id?>/1" onclick="return confirm('Are you sure you want to Active this?');" >Inactive</a></div>
                                <?php } ?>
                                <div class="delete-left"><a class="delete" title="Edit" href="<?php echo base_url();?>admin/discount/discount_update/<?=$list->id?>/<?php echo $list->discount_wise; ?>"><i class="fa fa-pencil"></i></a>
                                </div>                
                </td>


        </tr>
        <?php $i++;
            } 
        ?>
            </tbody>
    </table>
        <table id="brand" class="grid_tbl">
                <thead>
                    <tr>
                        <th scope="col">So.No.</th>
                        <th scope="col">Discount Name</th>
                        <th scope="col">Coupon Code</th>
                        <th scope="col">Coupon User Type</th>
                        <th scope="col">Discount On</th>
                        <th scope="col">Discount Wise</th>
                       <!-- <th scope="col" ><select id="discount_on" onchange="get_table($(this).val());">
                          <option value="none">Discount On</option>
                          <option value="brands">Brands</option>
                          <option value="category">Category</option>
                          <option value="products">Product</option>
                        </select> </th> -->
                         
                        <th scope="col">Discount Type</th>
                       <th scope="col">Discount Value</th> 
                       <th scope="col">Purchase Amount</th> 
                        <th scope="col">Valid From</th>
                        <th scope="col">Valid to</th>
                        <th scope="col">Reusable Limit</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">

                <?php
                    $i = 1;
                    foreach ($brand_detail AS $list) {
                ?>
                <tr>
                    <td data-rel="so.no"><?php echo $i; ?></td>
                    <td data-rel="so.no"><?php echo ucwords(strtolower($list->discount_name)); ?></td>
                    <td data-rel="so.no"><?php echo $list->coupon; ?></td>
                    <td data-rel="so.no"><?php if($list->coupon_user_type=='retail'){ echo 'Consumer'; } ?></td>
                    <td data-rel="so.no"><?php if($list->discount_wise == 'brands') { echo get_brand_names($list->brand_id) ;} else if($list->discount_wise == 'category'){ echo get_category_names($list->category_id);} else if($list->discount_wise == 'products'){ echo get_product_names($list->product_id);} ?></td>
                    <td data-rel="so.no"><?php echo ucfirst(strtolower($list->discount_wise)); ?></td>
                    
                    
                    <td data-rel="so.no"><?php echo ucwords(strtolower($list->discount_type)); ?></td>
                    
                    
                    <td data-rel="Pi ID"><?php echo ucfirst(strtolower($list->discount_value)); ?></td>
                    <td data-rel="Pi ID"><?php echo ucfirst(strtolower($list->purchase_amount)); ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_from; ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_to; ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->reusable_limit; ?></td>
                            
                <?php 
                       # echo $list->DiscStatus == 'A' ? 'Active' : 'In-Active';

                ?>
        <!-- <a href="<?php echo base_url(); ?>admin/discount/discount_new_activeinactive/<?=$list->Copid?>/<?php print ($list->DiscStatus=='I') ? 'active':'inactive';?>"
          <?php if($list->DiscStatus=='A'){ ?>class="inactive" <?php } else { ?>class="active_button"  <?php } ?>
          > <?php print ($list->DiscStatus=='I') ? 'Inactive':'Active';?></a> -->
                </td>
                <td data-rel="Action">
                                <?php if($list->status == 1) {  ?>
                                <a class="delete-right" title="Delete" href="<?php echo base_url();?>admin/discount/active_inactive/<?=$list->id?>/0" onclick="return confirm('Are you sure you want to Inactive this?');" >Active</a>
                                <?php } else if($list->status == 0) { ?>
                                <div class="delete-right"><a class="delete" title="Delete" href="<?php echo base_url();?>admin/discount/active_inactive/<?=$list->id?>/1" onclick="return confirm('Are you sure you want to Active this?');" >Inactive</a></div>
                                <?php } ?>
                                <div class="delete-left"><a class="delete" title="Edit" href="<?php echo base_url();?>admin/discount/discount_update/<?=$list->id?>/<?php echo $list->discount_wise; ?>"><i class="fa fa-pencil"></i></a>
                                </div>                
                </td>


        </tr>
        <?php $i++;
            } 
        ?>
            </tbody>
    </table>
        <table id="product" class="grid_tbl">
                <thead>
                    <tr>
                        <th scope="col">So.No.</th>
                        <th scope="col">Discount Name</th>
                        <th scope="col">Coupon Code</th>
                        <th scope="col">Coupon User Type</th>
                        <th scope="col">Discount On</th>
                        <th scope="col">Discount Wise</th>
                       <!-- <th scope="col" ><select id="discount_on" onchange="get_table($(this).val());">
                          <option value="none">Discount On</option>
                          <option value="brands">Brands</option>
                          <option value="category">Category</option>
                          <option value="products">Product</option>
                        </select> </th> -->
                         
                        <th scope="col">Discount Type</th>
                       <th scope="col">Discount Value</th> 
                       <th scope="col">Purchase Amount</th> 
                        <th scope="col">Valid From</th>
                        <th scope="col">Valid to</th>
                        <th scope="col">Reusable Limit</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">

                <?php
                    $i = 1;
                    foreach ($product_detail AS $list) {
                ?>
                <tr>
                    <td data-rel="so.no"><?php echo $i; ?></td>
                    <td data-rel="so.no"><?php echo ucwords(strtolower($list->discount_name)); ?></td>
                    <td data-rel="so.no"><?php echo $list->coupon; ?></td>
                    <td data-rel="so.no"><?php if($list->coupon_user_type=='retail'){ echo 'Consumer'; } ?></td>
                    <td data-rel="so.no"><?php if($list->discount_wise == 'brands') { echo get_brand_names($list->brand_id) ;} else if($list->discount_wise == 'category'){ echo get_category_names($list->category_id);} else if($list->discount_wise == 'products'){ echo get_product_names($list->product_id);} ?></td>
                    <td data-rel="so.no"><?php echo ucfirst(strtolower($list->discount_wise)); ?></td>
                    
                    
                    <td data-rel="so.no"><?php echo ucwords(strtolower($list->discount_type)); ?></td>
                    
                    
                    <td data-rel="Pi ID"><?php echo ucfirst(strtolower($list->discount_value)); ?></td>
                    <td data-rel="Pi ID"><?php echo ucfirst(strtolower($list->purchase_amount)); ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_from; ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_to; ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->reusable_limit; ?></td>
                            
                <?php 
                       # echo $list->DiscStatus == 'A' ? 'Active' : 'In-Active';

                ?>
        <!-- <a href="<?php echo base_url(); ?>admin/discount/discount_new_activeinactive/<?=$list->Copid?>/<?php print ($list->DiscStatus=='I') ? 'active':'inactive';?>"
          <?php if($list->DiscStatus=='A'){ ?>class="inactive" <?php } else { ?>class="active_button"  <?php } ?>
          > <?php print ($list->DiscStatus=='I') ? 'Inactive':'Active';?></a> -->
                </td>
                <td data-rel="Action">
                                <?php if($list->status == 1) {  ?>
                                <a class="delete-right" title="Delete" href="<?php echo base_url();?>admin/discount/active_inactive/<?=$list->id?>/0" onclick="return confirm('Are you sure you want to Inactive this?');" >Active</a>
                                <?php } else if($list->status == 0) { ?>
                                <div class="delete-right"><a class="delete" title="Delete" href="<?php echo base_url();?>admin/discount/active_inactive/<?=$list->id?>/1" onclick="return confirm('Are you sure you want to Active this?');" >Inactive</a></div>
                                <?php } ?>
                                <div class="delete-left"><a class="delete" title="Edit" href="<?php echo base_url();?>admin/discount/discount_update/<?=$list->id?>/<?php echo $list->discount_wise; ?>"><i class="fa fa-pencil"></i></a>
                                </div>                
                </td>


        </tr>
        <?php $i++;
            } 
        ?>
            </tbody>
    </table>
        <table id="category" class="grid_tbl">
                <thead>
                    <tr>
                        <th scope="col">So.No.</th>
                        <th scope="col">Discount Name</th>
                        <th scope="col">Coupon Code</th>
                        <th scope="col">Coupon User Type</th>
                        <th scope="col">Discount On</th>
                        <th scope="col">Discount Wise</th>
                       <!-- <th scope="col" ><select id="discount_on" onchange="get_table($(this).val());">
                          <option value="none">Discount On</option>
                          <option value="brands">Brands</option>
                          <option value="category">Category</option>
                          <option value="products">Product</option>
                        </select> </th> -->
                         
                        <th scope="col">Discount Type</th>
                       <th scope="col">Discount Value</th> 
                       <th scope="col">Purchase Amount</th> 
                        <th scope="col">Valid From</th>
                        <th scope="col">Valid to</th>
                        <th scope="col">Reusable Limit</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">

                <?php
                    $i = 1;
                    foreach ($category_detail AS $list) {
                ?>
                <tr>
                    <td data-rel="so.no"><?php echo $i; ?></td>
                    <td data-rel="so.no"><?php echo ucwords(strtolower($list->discount_name)); ?></td>
                    <td data-rel="so.no"><?php echo $list->coupon; ?></td>
                    <td data-rel="so.no"><?php if($list->coupon_user_type=='retail'){ echo 'Consumer'; } ?></td>
                    <td data-rel="so.no"><?php if($list->discount_wise == 'brands') { echo get_brand_names($list->brand_id) ;} else if($list->discount_wise == 'category'){ echo get_category_names($list->category_id);} else if($list->discount_wise == 'products'){ echo get_product_names($list->product_id);} ?></td>
                    <td data-rel="so.no"><?php echo ucfirst(strtolower($list->discount_wise)); ?></td>
                    
                    
                    <td data-rel="so.no"><?php echo ucwords(strtolower($list->discount_type)); ?></td>
                    
                    
                    <td data-rel="Pi ID"><?php echo ucfirst(strtolower($list->discount_value)); ?></td>
                    <td data-rel="Pi ID"><?php echo ucfirst(strtolower($list->purchase_amount)); ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_from; ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->valid_to; ?></td>
                    <td data-rel="Supplier Number"><?php echo $list->reusable_limit; ?></td>
                            
                <?php 
                       # echo $list->DiscStatus == 'A' ? 'Active' : 'In-Active';

                ?>
        <!-- <a href="<?php echo base_url(); ?>admin/discount/discount_new_activeinactive/<?=$list->Copid?>/<?php print ($list->DiscStatus=='I') ? 'active':'inactive';?>"
          <?php if($list->DiscStatus=='A'){ ?>class="inactive" <?php } else { ?>class="active_button"  <?php } ?>
          > <?php print ($list->DiscStatus=='I') ? 'Inactive':'Active';?></a> -->
                </td>
                <td data-rel="Action">
                                <?php if($list->status == 1) {  ?>
                                <a class="delete-right" title="Delete" href="<?php echo base_url();?>admin/discount/active_inactive/<?=$list->id?>/0" onclick="return confirm('Are you sure you want to Inactive this?');" >Active</a>
                                <?php } else if($list->status == 0) { ?>
                                <div class="delete-right"><a class="delete" title="Delete" href="<?php echo base_url();?>admin/discount/active_inactive/<?=$list->id?>/1" onclick="return confirm('Are you sure you want to Active this?');" >Inactive</a></div>
                                <?php } ?>
                                <div class="delete-left"><a class="delete" title="Edit" href="<?php echo base_url();?>admin/discount/discount_update/<?=$list->id?>/<?php echo $list->discount_wise; ?>"><i class="fa fa-pencil"></i></a>
                                </div>                
                </td>


        </tr>
        <?php $i++;
            } 
        ?>
            </tbody>
    </table>

        </div>
        </div>
        </div>
</section>
<script>
    $(document).ready(function(){

        $('#all_list').show();
        $('#brand').hide();
        $('#product').hide();
        $('#category').hide();

       $('#filter_discount').on('change', function(){
          var id = $(this).val();
          if(id == 'brand')
          {
                $('#brand').show();
                $('#all_list').hide();
                $('#product').hide();
                $('#category').hide();
          }
          else if(id == 'category')
          {
                $('#brand').hide();
                $('#all_list').hide();
                $('#product').hide();
                $('#category').show();
          }
          else if( id == 'product')
          {
                $('#brand').hide();
                $('#all_list').hide();
                $('#product').show();
                $('#category').hide();
          }
          else if(id == 'all_list')
          {
                $('#brand').hide();
                $('#all_list').show();
                $('#product').hide();
                $('#category').hide();
          }
               
         
       });
    });
</script>
<script>
    function get_table(value)
    {
        //alert('hello');
        $.ajax({
            url: '<?php echo base_url()."admin/discount/ajax_discount_list" ; ?>',
            type: "POST",
            data: {'type': value },
            success: function (data) {
                //alert(data); return false;
                $('#title_option').html(data);
            }
        });
    }
   
</script>
<script>
    $(function () {
        $(".datepicker").datepicker({
            altField: "#alternate",
            altFormat: "DD, d MM, yy",
            dateFormat: "yy-mm-dd"
        });
    });

    function get_filtered_discount() {
        var baseurl = "<?php echo base_url(); ?>";
        var Coupon = $('input[name=coupon_code]').val();
        var sdt = $('input[name=start_date]').val();
        var edt = $('input[name=end_date]').val();
        var status = $('select[name=Status]').val();
        var filter = new Array();
        filter = {'Coupon': Coupon, 'start': sdt, 'end': edt, 'status': status};
        $.ajax({
            url: baseurl + "admin/discount/filterDiscount",
            type: "POST",
            data: {'data': filter},
            success: function (data) {
                //alert(data); return false;
                $('#tblBody').html(data);
            }
        });
    }

</script>