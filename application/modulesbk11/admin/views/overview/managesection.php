<div class="wrapper">

<?php
      $this->load->view('helper/nav')
?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">
         
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                    
                        <h2>Manage Home Page Section</h2>
                       
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add manage home page section and can delete also.</p>
                        </div>
                    </div>
            
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                            <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/overview/addsection"><button>Add Overview</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>

                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <!-- <th>Section Name</th> -->
                                            <th> Category</th>
                                            <th>Product Name</th>
                                            <!-- <th>Updated by</th> -->
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                         $i=0; 
                                        if(count($recs) > 0)

                                        {
                                         foreach ($recs as $key => $value) {
                                            // p($value);
                                         $i++;
                                         
                                    ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
<!--                                             <td><?php echo $value->sectionname ;?></td>
 -->                                        <td><?php echo $value->CatName ;?></td>    
                                            <td><?php echo $value->ProductName ;?></td>    
                                            <!-- <td><?php echo $value->sortorder ;?></td> -->   
                                            <!-- <td><?php if(empty($value->modifiedby)){ echo $value->u_createdby; } else { echo $value->u_modifiedby; } ?></td>  -->
                                            <td><?php if(empty($value->modifiedby)){ echo $value->createdon; } else { echo $value->modifiedon; } ?></td>   
                                            <td>
                                            <input type='hidden' name='attstatu[<?php echo $value->proid;?>]'  value='<?=$value->sectionstatus?>'>
                                            <a href="<?php echo base_url()?>admin/overview/updatesection/<?php echo $value->did?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/overview/deletesection/<?php echo $value->ProId?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/overview/statussection/<?php echo $value->ProId.'/'.$value->status; ?>"><?php if($value->status=='0') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                            <?php 
                                    } 

                                }//END IF
                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
