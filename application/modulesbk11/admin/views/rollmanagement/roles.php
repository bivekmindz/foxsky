<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Employee list</h2>
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can select employee and profile and can assign roles to them.</p>
                        </div>
                    </div>
        <div class="page_box">
<div class="col-lg-12">
<?php echo validation_errors(); ?>
<form method="post" action="createrole" onsubmit="return validation();">
<table style="border:1px solid #ccc; padding:5px; width:90%;font-size:14px;">
 <thead>
   <th colspan="2" style=" padding:15px;">Create Role</th>
 </thead>
 <tbody>
 <tr>
   <td style="border:1px solid #ccc; padding:15px;">Select Role</td>
   <td style="border:1px solid #ccc; padding:15px;"><select name="role" id="rolename">
   <option value="">Select </option>
          <?php foreach($rolename as $value){?>
          <option value="<?php echo $value->id ?>"><?php echo $value->rolename?></option>
          <?php } ?>
        </select></td>
 </tr>
 <tr>
   <td style="border:1px solid #ccc; padding:15px;">Employee Name</td>
   <td style="border:1px solid #ccc; padding:15px;">
   		<select name="user" id="userid">
         	<option value="">Select Employee</option>
        </select>
    </td>
 </tr>
 <tr>
   <td style="border:1px solid #ccc; padding:15px;">Modules</td>
   <td style="border:1px solid #ccc; padding:15px;">
   	<select name="mainmenu" id="mainmenu" multiple="multiple">
   	<?php
   			foreach($menuaccess as $mainmenu)
   			{
   	 ?>
   	 	<option value="<?php echo $mainmenu->id;  ?>"><?php echo $mainmenu->menuname;  ?></option>
   	 <?php } ?>
   	 </select>
   </td>
 </tr>

<tr><td style="border:1px solid #ccc; padding:15px;">Select Submenu</td>
<td style="border:1px solid #ccc; padding:15px;">

<select id="div1" name="div1[]" multiple="multiple" style="height:150px; width:200px;">
	

</select>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<select id="div2" name="div2[]" multiple="multiple" style="height:150px; width:200px;">
	

</select>
</td>
</tr>

 
<tr><td style="padding:15px;" id="dikhana_hai" class="newdiv newdivsj"> </td>
	<td>
	<input type="button" class="btn_add" id="rightmove" value=">>" />
	
	<input type="button" class="btn_add" id="leftmove" value="<<" />
	</td>

</tr>
<tr>

<td></td><td id="dikhana_hai2" class="newdiv1 newdivssj">
	
	
</td></tr>
 <tr>
   <td colspan="2">
   <div class="submit_tbl" style="padding:8px 10px;">
   <input type="submit" value="Submit" name="addUserMenu" class="btn_button sub_btn" id="buttonshow"  />
   </div></td>
 </tr>
 </tbody>
</table>
</form>
</div>
</div>
</div>

<script>
	
	$(document).on("change","#mainmenu",function(){


			
			var menuid = $(this).val();
			
			

			var baseurl = "<?php  echo base_url(); ?>";

			//fileter = {'menuid' : menuid };
			$.ajax({

				url: baseurl + "admin/user/getsubmenss",
				type: "POST",
				data:{ 'menuid':menuid},
				success : function(data){

					$("#div1").html(data);
					
				}
			});
	});






	$(document).on("change", "#rolename", function(){
		var roleid = $(this).val();
		var baseurl='<?php echo base_url();?>';
		filter = {'roleid': roleid};
        $.ajax({
            url: baseurl + "admin/user/getuser",
            type: "POST",
            data: {'data': filter},
            success: function (data) {
                //alert(data);  return false;
                $('#userid').html(data);
            }
        });
	});
	$(document).on("change", "#module", function(){
		var mainmenu = $(this).val();
		var baseurl='<?php echo base_url();?>';
		$.ajax({
				url: baseurl+'admin/user/getSubMenu/'+mainmenu,
				type:"GET",
			   error: function (data) {},
				success: function (data) {
					//alert(data);
					$("#dikhana_hai").html(data);
				}
			});
	});

	$(document.body).on('click', '#rightmove' ,function(){		
			var foo = [];
			$('#div1 :selected').each(function(i, selected){ 
			  foo[i] = $(this); 
			});			
			$("#div2").append(foo);
	});
	
	$(document.body).on('click', '#leftmove' ,function(){		
			var foo2 = [];
			$('#div2 :selected').each(function(i, selected){ 
			  foo2[i] = $(this); 
			});			
			$("#div1").append(foo2);
	});


	$(document.body).on('click', '#leftmove' ,function(){
			$(".newdiv1 span").each(function(index, element) {        
			   var valid,got=0;
		valid=$(this).find("input").last().val();
		//alert(valid);
		$(".newdiv span").each(function(index, element) {
			//console.log($(this).find("input").last().val(),"==",valid);
			//alert($(this).attr("id"));
			if($(this).attr("id")==valid)
			got=1;
		});
		if(got==0)
		{	
				$(".newdiv").append("<span id='"+$(this).find("input").last().val()+"'>"+$(this).text()+"<input type='hidden' name='menuid[]' value='"+$(this).find("input").first().val()+"'/><input type='hidden' name='submenuid[]' value='"+$(this).find("input").last().val()+"'/><br/></span>");
			$(this).remove();
		}
		else
		{
			$(this).remove();
		}
		});
			});

	$(document.body).on('click','#buttonshow',function(){
				$('.newdiv').html('');
				
	});

	function validation()
	{
		var user=document.getElementById('user');
		var module=document.getElementById('module');
		if(user.value=='')
		{
			user.style.border="1px solid #900";
			return false;
		}
		else
		{
			user.style.border="";
		}
		if(module.value=='')
		{
			module.style.border="1px solid #900";
			return false;
		}
		else
		{
			module.style.border="";
		}
	}
</script>

