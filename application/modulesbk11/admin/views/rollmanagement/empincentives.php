<?php //pend(date('m',strtotime(date("d-m-Y",strtotime("-1 month")))));?>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Employee Incentive</h2>
    </div>
    <div class="page_box">
        <div class="col-lg-12">
            <p>
                Name : <b><?php echo $employee->F_Name.' '.$employee->L_Name;?></b>
                <button onclick="goBack()" style="float:right;">Go Back</button>
                <script>
                function goBack() {
                    window.history.back();
                }
                </script>
            </p>
            <p>Email ID : <b><?php echo $employee->Email;?></b></p>
        </div>
    </div>
    <div class="page_box">
        <div class="sep_box">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-lg-5">
                            <input type="text" class="date form-control" placeholder="From" id="start_date" readonly />
                    </div>
                    <div class="col-lg-5">
                            <input type="text" class="date form-control" placeholder="To" id="end_date" readonly />
                    </div>
                    <div class="col-lg-2">
                            <input type="button" value="Calculate" class="form-control" onclick="return calculateInc();" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="incentiveData"></div>
</section>
</div>
</div>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />
<script type="text/javascript">
    $('.date').datepicker({
    'format': 'yyyy-mm-dd',
    'autoclose': true,
    todayHighlight: true
    });
</script>  
<script type="text/javascript">      
    function calculateInc() {
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        var emp_id = <?php echo $this->uri->segment(4);?>;
        if(start_date=="" || end_date=="") {
            alert("Please select date range!"); return false;
        } else {
            $.ajax({ 
                type:'post',
                url:'<?php echo base_url()?>admin/user/calculate_incentive',
                data:{'start_date':start_date, 'emp_id':emp_id, 'end_date':end_date},
                success:function(result) {
                    $("#incentiveData").html(result);
                }
            });
        }
    }
</script>