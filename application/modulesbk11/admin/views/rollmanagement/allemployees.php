<?php //p($list); exit();  ?>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Employee Incentives (<?php echo date("M-Y",strtotime('-1 month'));?>)</h2>
    </div>
    <div class="page_box">
        <div class="col-lg-12">
            <p> In this section, admin can view employee incentives!</p>
        </div>
    </div>
    <div class="page_box">
        <div class="sep_box">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-lg-5">
                            <input type="text" class="date form-control" placeholder="From" id="start_date" readonly />
                    </div>
                    <div class="col-lg-5">
                            <input type="text" class="date form-control" placeholder="To" id="end_date" readonly />
                    </div>
                    <div class="col-lg-2">
                            <input type="button" value="Calculate" class="form-control" onclick="return calculateInc();" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page_box" id="incentiveData">
        <div class="sep_box">
            <div class="col-lg-12">
            <table id="test" class="grid_tbl">
                <?php echo $this->session->flashdata("message"); ?>
                <thead>
                    <tr>
                        <th scope="col" style="align:left">Retailer Name</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Email</th>
                        <th scope="col">Address</th>
                        <th scope="col">Role</th>
                        <th scope="col">Incentives (In Rs.)</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">
                    <?php foreach ($list as $employeelist) { 
                        $retailers = trim($employeelist->e_retailid,',');
                        $retailer_array = explode(",",$retailers);
                        $saleTotal=0; 
                        $start_date = date("Y-m-d", strtotime('FIRST DAY OF PREVIOUS MONTH'));
                        $dateObj = new DateTime(date('Y-m-d',strtotime('FIRST DAY OF PREVIOUS MONTH')));
                        $dateObj->modify('last day of');
                        $end_date = $dateObj ->format('Y-m-d');
                        //echo $start_date.' '.$end_date;
                        //$start_date=$start_date.' 00:00:00';
                        //$end_date=$end_date.' 00:00:00';
                        foreach ($retailer_array as $value) {  
                            $resp = retailerData($value); 
                            $total_amt = retailerTotalOrderAmt($value,$start_date,$end_date); 
                            $saleTotal+=$total_amt;                            
                        } 
                        $status = retailerIncentiveStatus($employeelist->e_retailid,$start_date,$end_date);
                        $incentive_amt = empIncentive($saleTotal,$employeelist->empSalary);
                        //echo $saleTotal;
                        ?>
                        <tr>
                            <td data-rel="Pi ID" style="align:left" ><?php echo  $employeelist->F_Name.''.$employeelist->L_Name; ?></td>
                            <td data-rel="S.No"><?php echo $employeelist->ContactNo;  ?></td>
                            <td data-rel="Pi Code"><?php echo $employeelist->Email;  ?></td>
                            <td data-rel="Supplier Number"><?php echo $employeelist->Address;  ?></td>
                            <td data-rel="Supplier Number"><?php echo $employeelist->rolename;  ?></td>
                            <td data-rel="Supplier Number"><?php if($incentive_amt=="") echo 0; else echo number_format($incentive_amt,2,".","");?></td>
                            <td data-rel="Supplier Number" align="center" id="status_td_<?php echo $employeelist->EmployeeID;?>">
                                <?php if($incentive_amt!="" || $incentive_amt!=0) { 
                                    if($status=='paid') { echo '<span style="color:green">Paid</span>'; } else { ?>
                                <select id="incentive_status" class="form-control" onchange="return changeIncStatus(this.value,'<?php echo $employeelist->e_retailid;?>','<?php echo $start_date;?>','<?php echo $end_date;?>','<?php echo $employeelist->EmployeeID;?>');">
                                    <option value="pending" <?php if($status=='pending') echo 'selected';?>>Pending</option>
                                    <option value="paid" <?php if($status=='paid') echo 'selected';?>>Paid</option>
                                </select>
                                <?php } } ?>
                            </td>
                            <td data-rel="Supplier Number">
                            <a href="<?php echo base_url(); ?>admin/user/empincentives/<?php echo $employeelist->EmployeeID;  ?>"><i class="fa fa-view"></i> View Incentives</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</section>
</div>
</div>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />
<script type="text/javascript">
    $('.date').datepicker({
    'format': 'yyyy-mm-dd',
    'autoclose': true,
    todayHighlight: true
    });
</script>  
<script type="text/javascript">      
function calculateInc() {
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();
    if(start_date=="" || end_date=="") {
        alert("Please select date range!"); return false;
    } else {
        $.ajax({ 
            type:'post',
            url:'<?php echo base_url()?>admin/user/allempincentives',
            data:{'start_date':start_date, 'end_date':end_date},
            success:function(result) {
                $("#incentiveData").html(result);
            }
        });
    }
} 
</script>  
<script type="text/javascript">  
function changeIncStatus(incentive_status,retailers,start,end,empid) {
    $.ajax({ 
        type:'post',
        url:'<?php echo base_url()?>admin/user/changeincentive_status',
        data:{'incentive_status':incentive_status,'retailers':retailers,'start':start,'end':end,'empid':empid},
        success:function(result) {
            $("#status_td_"+empid).html(result);
        }
    });
}
</script>