
<style>
label {
    
    color: red;
}
</style>
   
 <div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Assign Role</h2>
                        </div>
                        <div class="page_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?> 
                                        <?php
                                          if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message'); 
                                          }
                                        ?>
                                    </div>
                                    <form method="post" action="" name="roleform" id="roleform">
                                    <div class="row">
                                        
                                       <div class="sep_box">
                                            <div class="col-lg-10">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="tbl_text">Select Role <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <div class="tbl_input">
                                                           <select name="role" id="rolename">
  														                <option value="">Select </option>
                                                    <?php foreach($rolename as $value){ ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->rolename; ?></option>
                                                    <?php } ?>
        														</select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </div>    

                                         <div class="sep_box">
                                            <div class="col-lg-10">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="tbl_text">Select Employee <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <div class="tbl_input">
                                                           <select name="user" id="userid">
         														<option value="">Select Employee</option>

                                                                 <?php foreach ($employeelist as $key => $value) { ?>
                                                        <option value="<?php echo $value->EmployeeID; ?>"><?php echo $value->F_Name.' '.$value->L_Name.' ('.$value->Email.')'; ?></option>
                                                    <?php } ?>
        													</select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </div>    

                                         <div class="sep_box">
                                            <div class="col-lg-10">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="tbl_text">Modules <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <div class="tbl_input firstmainmenu">
                                                          	<select name="mainmenu" id="mainmenu">
                                                             <option value="">Select Main Module</option> 
   																<?php
   															foreach($menuaccess as $mainmenu)
   																{
   													 ?>
   												 	<option value="<?php echo $mainmenu->id;  ?>"><?php echo $mainmenu->menuname;  ?></option>
   															 <?php } ?>
   													 </select>
                                                        </div>
                                                        <span class="mainmenu"></span>


                                                        <div class="tbl_input secondmainmenu">
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </div>    

                                         <div class="sep_box">
                                            <div class="col-lg-10">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="tbl_text">Select Submenu <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <div class="tbl_input">
                                                          	<select  id="div1" name="div1[]" multiple="multiple" style="height:150px; width:200px;">
	

															</select>
															<select id="div2" name="div2[]" multiple="multiple" style="height:150px; width:200px;">
	

														</select>
                                                        </div>
                                                         <span class="div2"></span>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </div>    

                                         <div class="sep_box">
                                            <div class="col-lg-10">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="tbl_text"></div>
                                                    </div>
                                                    <div class="col-lg-9">
                                                        <div class="tbl_input">
                                                           <input type="button" class="btn_add" id="rightmove" value=">>" />  
															<input type="button" class="btn_add" id="leftmove" value="<<" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </div>    




                                     

<div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input type="submit" onclick="return validation();" value="Submit" name="addUserMenu" class="btn_button sub_btn" id="buttonshow"  />
                </div>
            </div>
        </div>
        </div>

        </div>



                                        </form>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>  


<style type="text/css">
input[type="text"]{vertical-align:top; width: 90% !important;}
.field_wrapper div{ margin-bottom:10px;}
.add_button{ line-height: 38px; margin-left:10px;}
.remove_button{ line-height: 38px; margin-left:10px;}
</style>


<script src ="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
<script type="text/javascript">
        // Wait for the DOM to be ready
        $(function() {

          $("#roleform").validate({
    // Specify validation rules
    rules: {
      role: "required",
      user: "required",
      mainmenu : "required"
    },
    // Specify validation error messages
    messages: {
     role : "Please select role",
     user : "Please select employee name",
     mainmenu : "Please select mainmenu"
  },

  submitHandler: function(form) {

    var optionss = $('#div2 > option:selected');
         //var options = $('#prisubjects > option:selected');
    if(optionss.length == 0){
      $(".div2").html("<b>Please move sub menu to right box.</b>").css({'color':'rgb(251,91,91)'});
      return false;
    } else{
        form.submit();
    }
  }
});
        });


 </script> 

 <script>

     /*$(function(){
       $('form').submit(function(){
       var options = $('#mainmenu > option:selected');
         //var options = $('#prisubjects > option:selected');
         //alert(options.length);
        if(options.length == 0){
        $(".mainmenu").html("Please select mainmenu").css({'color':'rgb(251,91,91)'});
        return false;

        }
       

      });
      });*/


/*$(function(){
$('form').submit(function(){
var optionss = $('#div2 > option:selected');
         //var options = $('#prisubjects > option:selected');
         //alert(optionss.length); return false;
if(optionss.length == 0){
$(".div2").html("<b>Please move sub menu to right box.</b>").css({'color':'rgb(251,91,91)'});
return false;

}

});
});
*/

	
	$(document).on("change","#mainmenu",function(){


			
			var menuid = $(this).val();
			//alert(menuid);
			

			var baseurl = "<?php  echo base_url(); ?>";

			//fileter = {'menuid' : menuid };
			$.ajax({

				url: baseurl + "admin/rollmaster/getsubmenss",
				type: "POST",
				data:{ 'menuid':menuid},
				success : function(data){
					//alert(data);
					$("#div1").html(data);
					
				}
			});
	});


	$(document.body).on('click', '#rightmove' ,function(){		
			var foo = [];
			$('#div1 :selected').each(function(i, selected){ 
			  foo[i] = $(this); 
			});			
			$("#div2").append(foo);
	});
	
	$(document.body).on('click', '#leftmove' ,function(){		
			var foo2 = [];
			$('#div2 :selected').each(function(i, selected){ 
			  foo2[i] = $(this); 
			});			
			$("#div1").append(foo2);
	});


	$(document.body).on('click', '#leftmove' ,function(){
			$(".newdiv1 span").each(function(index, element) {        
			   var valid,got=0;
		valid=$(this).find("input").last().val();
		//alert(valid);
		$(".newdiv span").each(function(index, element) {
			//console.log($(this).find("input").last().val(),"==",valid);
			//alert($(this).attr("id"));
			if($(this).attr("id")==valid)
			got=1;
		});
		if(got==0)
		{	
				$(".newdiv").append("<span id='"+$(this).find("input").last().val()+"'>"+$(this).text()+"<input type='hidden' name='menuid[]' value='"+$(this).find("input").first().val()+"'/><input type='hidden' name='submenuid[]' value='"+$(this).find("input").last().val()+"'/><br/></span>");
			$(this).remove();
		}
		else
		{
			$(this).remove();
		}
		});
	});

	$(document.body).on('click','#buttonshow',function(){
				$('.newdiv').html('');
				
	});

	function validation()
	{


       
		var role=document.getElementById('rolename');
		var user=document.getElementById('user');
		if(role.value=='')
		{
			role.style.border="1px solid #000";
			//alert('ok');
			document.getElementById('rolemsg').innerHTML='Select Role';
			return false;
		}
		else
		{
			role.style.border="";
		}
		if(user.value=='')
		{
			user.style.border="1px solid #000";
			document.getElementById('empmsg').innerHTML='Emaployee Name';
			return false;
		}
		else
		{	
			user.style.border="";
		}
	}

$('#buttonshow').click(function() {
    $('#div2 option').prop('selected', true);
});
</script>