<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/admin/js/moment.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/admin/js/ion.calendar.js"></script>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Add Incentive Slab</h2>
    </div>
      <div class="page_box">
          <div class="col-lg-12">
              <p> In this section, admin can add incentive slab.</p>
          </div>
      </div>
        <div class="page_box">

<form action="" method="post" id="slabForm">
<div class="form">
  <span style="color:red"><?php echo $this->session->flashdata("message"); ?></span>
                                <div class="sep_box">
                                  <div class="col-lg-12">
                                    <div class="row">
                                    <a href="javascript:void(0);" class="add_row" title="Add row"><i class="fa fa-plus fa-lg" aria-hidden="true"></i> Add Row</a>
                                    <table class="grid_tbl" style="margin-top:25px;">
                                    <thead>
                                        <tr>
                                            <th>Sale Range From</th>
                                            <th>Sale Range To</th>
                                            <th>Salary Range From</th>
                                            <th>Salary Range To</th>
                                            <th>Price Type</th>
                                            <th>Incentive Price</th>
                                        </tr>
                                    </thead>
                                    <tbody class="append"> 
                                    <tr class="append_wrapper">
                                        <td><input type="text" name="range_from[]" id="range_from" /></td>
                                        <td><input type="text" name="range_to[]" id="range_to" /></td> 
                                        <td><input type="text" name="range_salaryFrom[]" id="range_salaryFrom" /></td>
                                        <td><input type="text" name="range_salaryTo[]" id="range_salaryTo" /></td>      
                                        <td><select type="text" name="range_type[]" id="range_type">
                                                <option value="">-- Select Type --</option>
                                                <option value="1">Percentage</option>
                                                <option value="2">Flat</option>
                                            </select>
                                        </td> 
                                        <td><input type="text" name="range_incentive[]" id="range_incentive" /></td>
                                    </tr>                
                                    </tbody>
                                    </table>
                                    </div>
                                  </div>
                                </div> 

                                <div class="sep_box">
                                  <div class="col-lg-12">
                                    <div class="row">
                                      <input type="submit" value="Submit" name="submit" style="float:right;"/>
                                    </div>
                                  </div>
                                </div>

</form> 
</div>
</div>
</div>
<script type="text/javascript">      
    $(document).ready(function(){
      var maxField = 1000; //Input fields increment limitation
      var addButton = $('.add_row'); //Add button selector
      var wrapper = $('.append_wrapper'); //Input field wrapper
      var x = 1; //Initial field counter is 1
     
      $(addButton).click(function(){ 
        if(x < maxField){
            x++; 
            $(".append_wrapper").clone().removeClass("append_wrapper").appendTo(".append");
        }
      });
      $(wrapper).on('click', '.remove_row', function(e){ //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
      });
    });
</script>
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#slabForm").validate({
          // Specify the validation rules
          rules: {
              "range_from[]" : "required",
              "range_to[]" : "required",
              "range_salaryFrom[]" : "required",
              "range_salaryTo[]" : "required",
              "range_type[]" : "required",
              "range_incentive[]" : {required:true, number:true},
          },
          
          messages: {
              "range_from[]" : "This field is required!",
              "range_to[]" : "This field is required!",
              "range_salaryFrom[]" : "This field is required!",
              "range_salaryTo[]" : "This field is required!",
              "range_type[]" : "This field is required!",
              "range_incentive[]" : {required:"This field is required!", number:"Number only!"},
          },
          
          submitHandler: function(form) {
            form.submit();
          }
      });
   });
</script>