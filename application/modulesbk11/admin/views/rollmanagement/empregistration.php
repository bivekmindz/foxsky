<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#mydate" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange : 'c-50:c+1',
      dateFormat: "dd-mm-yy"
    });
    $( "#mydate2" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange : 'c-10:c+10',
      dateFormat: "dd MM yy"
    });
  });
  </script>

  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

<!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            FName      : "required",
            LName      : "required",
            phone      : "required",
            password   : "required",
            email      : "required", 
            gender     : "required",
            dateofbirth : "required",
            dateofjoining : "required",
            pin      : "required", 
            manageraddress     : "required",
        },
        // Specify the validation error messages
        messages: {
            FName  : "First Name is required",
            LName  : "Last Name is required",
            phone  : "Phone No is required",
            password  : "Password is required",
            email : "Email is required",
            gender : "Gender is required",
            dateofbirth  : "Date Of Birth is required",
            dateofjoining : "Date Of Joining is required",
            pin : "Pincode is required",
            manageraddress : "Address is required",
        },
        submitHandler: function(form) {
              //form.submit();
            var email = $('#email_address').val();
            var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if(!emailReg.test(email)) {
              $("#e_error_msg").html('Valid Email is required');
              $("#email_address").css('border','1px solid red');
              return false;
            }else{
              if($('#e_send').val()==1){
                $("#e_error_msg").html('');
                $("#email_address").css('border','1px');
                form.submit();
              } else {
                $("#e_error_msg").html('This email id already registered');
                $("#email_address").css('border','1px solid red');
                return false;
              }
            }  



        }
    });

  });
  
</script>
<div class="wrapper">
<?php 
$this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Add Employee</h2>
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can Add employee .</p>
                        </div>
                    </div>
        <div class="page_box">
<?php //echo form_open('admin/user/employeeregistration')?>
<form action="<?php echo base_url().'admin/user/employeeregistration'; ?>" id="addCont" method="post" enctype="multipart/form-data" >
<div class="form">
<span id="message"></span>
<input type="hidden" name="e_send" id="e_send" value="">
<div class="sep_box">

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">First Name <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="FName" value="" id="FName" class="required" field="First Name" ></span></div>
          </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Last Name <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="LName" value="" id="LName" class="required" field="Last Name" ></span></div>
          </div>
      </div>
    </div>
    </div>
    <div class="sep_box">

<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Father Name</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="FatherName" value="" id="FatherName" field="Father Name" ></span></div>
          </div>
      </div>
    </div>


<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Phone No. <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="phone" value="" id="Name" class="required" field="Phone No." maxlength="10" onkeypress="return isNumberKey(event)" ></span></div>
          </div>
      </div>
    </div>

</div>
<div class="sep_box">



<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Password <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="password" name="password" value="" id="managerpassword" class="required" field="Password" ></span></div>
          </div>
      </div>
    </div>



<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Email (userid) <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="email" value="" id="email_address" class="required" field="email" onblur="return validatesEmail($(this).val())" ></span></div>
            <label id="e_error_msg" style="color: red;"></label>
          </div>
      </div>
    </div>
    </div>

    <div class="sep_box">

<!-- <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Role</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <select name="role" id="role" class="role" onchange="getretail()">
          <?php foreach($rolename as $value){?>
          <option value="<?php echo $value->id ?>"><?php echo $value->rolename?></option>
          <?php } ?>
        </select>
          </div>
          </div>
      </div>
    </div> -->
    <input type="hidden" name="role" id="role" value="0">


    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Gender <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
             <select name="gender" id="" class="required" field="Gender">
          <option value="1">Male</option>
          <option value="2">Female</option>
        </select>
          </div>
          </div>
      </div>
    </div>


    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Date Of Birth <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <input name="dateofbirth" type="text" value="" id="mydate" data-lang="en" data-years="1947-2015" data-format="YYYY-MM-DD" readonly />
          </div>
          </div>
      </div>
    </div>
    </div>
    <div class="sep_box">

    
  
 <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Date Of Joining <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <input name="dateofjoining" type="text" value="" id="mydate2" data-lang="en" data-years="1947-2015" data-format="YYYY-MM-DD" readonly />
          </div>
          </div>
      </div>
    </div>
    
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Pincode <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="pin" id="pin" value="" maxlength="6" minlength="6" onkeypress="return isNumberKey(event)" class="required" field="pincode"/></span>
          </div>
          </div>
      </div>
    </div>
      </div>
      <div class="sep_box">
   
 <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Address <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><textarea name="manageraddress" id="shippingaddress" class="required" field="address"></textarea></span>
          </div>
          </div>
      </div>
    </div>
   
    </div>

<div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input type="submit" name="deliverman" id="deliverman" class="btn_button sub_btn" value="Submit"><!--  onclick="return validatedeliverymanager()"> -->
                </div>
            </div>
        </div>
        </div>

        </div>

<div class="formCol">

</div>
</form>
<?php //echo form_close();?>  </div>

</div>
</div>
<script>

function validatedeliverymanager()
{
  var error = false;
  

    $('.required').each(function(){
        if($(this).val()=='')
        {
          alert($(this).attr('field') + ' is Missing!');
          $(this).css('border-color','red');
          $(this).focus();
          error = true;
          return false;
          }else{
              $(this).css('border-color','green');
              error=false;  
        }
     });
    if(error)
        {
          return false;
        }else{
          return true;
        }

}

function isNumberKey(evt) {
var charCode = (evt.which) ? evt.which : event.keyCode;
if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
  return false;
 } else {
  return true;
 }
}

function validatesEmail(email) {

    var baseurl = "<?php echo base_url()?>";
    $.ajax({
    url:baseurl+"admin/user/checkdublicatemail",
    type: "POST",
    dataType: "json",
    data: { 'emailid':email},
    async:false,
    success:function(data) {
      //alert(data); return false;
        if(data.email==0)
        { 
          $('#e_send').val(1);
          return true;
        } else {
          $('#e_send').val(0);
          return false;
        }
      }  
});
  
}

function validpin(elem){
    if(elem.val()!=''){
        if((elem.val().length)<6){
            alert("PIN should have 6 digits");
            $(elem).focus();
             $(elem).css('border-color','red');
            return false;
        }else{
             $(elem).css('border-color','green');
            return true;
        }
    }
}
</script>


