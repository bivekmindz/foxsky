<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/admin/js/moment.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/admin/js/ion.calendar.js"></script>
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Update Incentive Slab</h2>
    </div>
        <div class="page_box">
<form action="" method="post" id="slabForm">
<div class="form">
<span id="message"></span>

<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Sale Range From</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="range_from" id="range_from" value="<?php echo $view->range_from;?>" /></span>
          </div>
          </div>
      </div>
    </div>
      
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Sale Range To</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="range_to" id="range_to" value="<?php echo $view->range_to;?>" /></span>
          </div>
          </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Salary Range From</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="range_salaryFrom" id="range_salaryFrom" value="<?php echo $view->range_salaryFrom;?>" /></span>
          </div>
          </div>
      </div>
    </div>
      
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Salary Range To</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="range_salaryTo" id="range_salaryTo" value="<?php echo $view->range_salaryTo;?>" /></span>
          </div>
          </div>
      </div>
    </div>

<?php /* ?><div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Price Type</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><select type="text" name="range_type" id="range_type">
                    <option value="">-- Select Type --</option>
                    <option value="1" <?php if($view->range_type==1) echo "selected";?>>Percentage</option>
                    <option value="2" <?php if($view->range_type==2) echo "selected";?>>Flat</option>
                </select>
          </span>
          </div>
          </div>
      </div>
    </div><?php */?>

<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Incentive Price (In <?php if($view->range_type==1) echo "%"; else echo "Flat";?>)</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="range_incentive" id="range_incentive" value="<?php echo $view->range_incentive;?>" /></span>
          </div>
          </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text"></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="submit" value="Submit" name="submit" /></span>
          </div>
          </div>
      </div>
    </div>
  
</form>   
</div>
</div>
</div>
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#slabForm").validate({
          // Specify the validation rules
          rules: {
              range_from : "required",
              range_to : "required",
              range_salaryFrom : "required",
              range_salaryTo : "required",
              range_type : "required",
              range_incentive : {required:true, number:true},
          },
          
          messages: {
              range_from : "This field is required!",
              range_to : "This field is required!",
              range_salaryFrom : "This field is required!",
              range_salaryTo : "This field is required!",
              range_type : "This field is required!",
              range_incentive : {required:"This field is required!", number:"Number only!"},
          },
          
          submitHandler: function(form) {
            form.submit();
          }
      });
   });
</script>