<?php //pend($editlist);?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#mydate" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange : 'c-50:c+1',
      dateFormat: "dd-mm-yy"
    });
    $( "#mydate2" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange : 'c-10:c+10',
      dateFormat: "dd MM yy"
    });
  });
  </script>
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Employee Edit</h2>
    </div>
        <div class="page_box">

<?php echo form_open('admin/user/employeelistedit/'.$editlist->EmployeeID);?>
<div class="form">
<span id="message"></span>
<div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">First Name  <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="FName" value="<?php echo $editlist->F_Name; ?>" id="FName" class="required" field="First Name" ></span>
          </div>
          </div>
      </div>
    </div>
      
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Last Name  <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="LName" value="<?php echo $editlist->L_Name; ?>" id="LName" class="required" field="Last Name" ></span>
          </div>
          </div>
      </div>
    </div>

</div>
<div class="sep_box">

<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Father Name</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="FatherName" value="<?php echo $editlist->FatherName; ?>" id="FatherName" ></span>
          </div>
          </div>
      </div>
    </div>

<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Phone No.  <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="phone" value="<?php echo $editlist->ContactNo; ?>" id="Name" class="required" field="phone" maxlength="10" onkeypress="return isNumberKey(event)" ></span>
          </div>
          </div>
      </div>
    </div>

</div>
<div class="sep_box">

<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Gender  <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span>
              <select name="gender" id="">
          <option value="1" <?php if($editlist->Gender == 1){ echo 'selected'; } ?>>Male</option>
          <option value="2" <?php if($editlist->Gender == 2){ echo 'selected'; } ?> >Female</option>
        </select>

           </span>
          </div>
          </div>
      </div>
    </div>


    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Date Of Birth  <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span>
              <input name="dateofbirth" type="text" value="<?php echo date_format(date_create($editlist->DOB),"d-m-Y"); ?>" id="mydate" data-lang="en" data-years="1947-2015" data-format="YYYY-MM-DD" class="required" field="Date Of Birth" />
           </span>
          </div>
          </div>
      </div>
    </div>

</div>
<div class="sep_box">

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Date Of Joining  <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span>
               <input name="dateofjoining" type="text" value="<?php echo date_format(date_create($editlist->DOJ),"d F Y"); ?>" id="mydate2" data-lang="en" data-years="1947-2015" data-format="YYYY-MM-DD" class="required" field="Date Of Joining" />

           </span>
          </div>
          </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Pincode  <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span>
               <input type="text" name="pin" id="pin" value="<?php echo $editlist->pincode; ?>" maxlength="6" minlenght="6" onblur = "validpin($(this))" onkeypress="return isNumberKey(event)" class="required" field="pincode"/>

           </span>
          </div>
          </div>
      </div>
    </div>

</div>
<div class="sep_box">    

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Address  <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span>
               <textarea name="manageraddress" id="shippingaddress" class="required" field="address" ><?php echo $editlist->Address; ?> </textarea>

           </span>
          </div>
          </div>
      </div>
    </div>

</div>
    
      <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input type="submit" name="deliverman" class="btn_button sub_btn" value="Submit" onclick="return validatedeliverymanager()">
                </div>
            </div>
        </div>
        </div>

        </div>

<?php echo form_close();?>  </div>

</div>
</div>
<script>
function validatedeliverymanager()
{
  var error = false;
  

    $('.required').each(function(){
        if($(this).val()=='')
        {
          alert($(this).attr('field') + ' is Missing!');
          $(this).css('border-color','red');
          $(this).focus();
          error = true;
          return false;
          }else{
              $(this).css('border-color','green');
              error=false;  
        }
     });
    if(error)
        {
          return false;
        }else{
          return true;
        }

}

function isNumberKey(evt) {
var charCode = (evt.which) ? evt.which : event.keyCode;
if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
  return false;
 } else {
  return true;
 }
}

function validatesEmail(email) {

 var emailReg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  if( !emailReg.test(email)) {
    alert("Enter Valid Email");
    //$("#validcheck").html("Enter Valid Email");
    $("#email_address").css('border','1px solid red');
  }
}

/*$(function(){
    $("#demo1").ionCalendar({
        lang: "en",
        years: "1915-1995",
        onClick: function(date){
            $("#result-1").html("onClick:<br/>" + date);
        }
    });


       
       $("#mydate").ionDatePicker();
       $("#mydate2").ionDatePicker();

});*/

 function validpin(elem){
    if(elem.val()!=''){
        if((elem.val().length)<6){
            alert("PIN should have 6 digits");
            $(elem).focus();
             $(elem).css('border-color','red');
            return false;
        }else{
             $(elem).css('border-color','green');
            return true;
        }
    }
}

</script>


