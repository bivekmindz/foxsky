<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage Purchase Order details</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view single Purchase order details according to manufacturer.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div id="div1" style="width:100%;overflow-x:auto;">
                                <form method="post">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <!-- <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span></th> -->
                                          <th>S.No.</th>
                                          <th>Vendor</th>
                                          <th>SKUNumber</th>
                                          <th>Product Image</th>
                                          <th>Product Name</th>
                                          <th>updated purchase Quantity</th>
                                          <th>Total Stock</th>
                                          <th>Size</th>
                                          <th>Color</th>
                                          <th>Product MRP</th>
                                          <th>old Manufacture Price</th>
                                          <th>New Manufacture Price</th>
                                          <th>purchase Quantity</th>
                                          <th>Manufacture Price</th>
                                          <th>Manufacture Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; foreach ($record as $key => $value) {  //p($value);?>
                                        <tr>
                                        
                                            <!-- <td><input type='checkbox' name='proApp[]' class='chkApp' value='<?=$value->po_proid?>'></td> -->
                                            
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo $value->firstname." ".$value->lastname;?></td>
                                            <td><?php echo $value->po_skuno;?></td>
                                            <td><img src="<?php echo $value->po_proimg;?>" style="vertical-align:middle; width:80px;"></td>
                                            <td><?php echo $value->po_proname;?></td>
                                            <td><?php echo $record2[$i]->postock; ?></td>
                                            <td><?php echo $value->pomq; ?></td>
                                            <td><?php echo $value->po_ordsize;?></td>
                                            <td><?php echo $value->po_ordcolor;?></td>
                                            <td><?php echo number_format($value->prodmrp,1,".","");?></td>
                                            <td><?php echo $value->po_finalprice;?></td>
                                            <td><?php echo $record2[$i]->poprice;?></td>
                                            <td><input size="7px" type="text" id="qty<?php echo $record2[$i]->pomapid; ?>" onkeyup="toqtypri(<?=$record2[$i]->pomapid?>,event)" name="qty[<?php echo $record2[$i]->pomapid; ?>]" value="<?php if($record2[$i]->postock==0){ echo $value->rqty; } else { echo $record2[$i]->postock; } ?>"></td>
                                            <td><input size="8px" type="text" id="sinamt<?=$record2[$i]->pomapid?>" onkeyup="toqtypri(<?=$record2[$i]->pomapid?>,event)" name="sinamt[<?=$record2[$i]->pomapid?>]" value="<?php if($record2[$i]->poprice==0){ echo $value->po_finalprice; } else { echo $record2[$i]->poprice; } ?>"></td>
                                            <td><input size="10px" type="text" readonly="" id="sinamttotal<?=$record2[$i]->pomapid?>" name="sinamttotal[<?=$record2[$i]->pomapid?>]" value=""></td>
                                            
                                            
<script type="text/javascript">
    $(document).ready(function() {
            var quan=$("#qty"+<?php echo $record2[$i]->pomapid; ?>).val();
            var pri=$("#sinamt"+<?php echo $record2[$i]->pomapid; ?>).val();
            var Totalpri=pri*quan;
            $("#sinamttotal"+<?php echo $record2[$i]->pomapid; ?>).val(Totalpri.toFixed(1));
            return true;
    
});
</script>                                            

                                            
                                        </tr>
                                        <?php $i++; } ?>
                                        <tr>
                                        <td colspan="2">&nbsp;</td>
                                        
                                       <td><a href="<?php echo base_url();?>admin/order/printpo/<?php echo $record2[0]->poid;?>" target="_blank"><input style="color: #676767;" type="button" value="Print PO" /></a></td>

                                        <td><input type="submit" name="submitpo" id="submitpo" value="Update PO" /></td>
                                        
                                        </tr>

                                        
                                    </tbody>
                                    
                                </table>
                                </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

     <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});

        function toqtypri(proid){
        
            var quan=$("#qty"+proid).val();
            var pri=$("#sinamt"+proid).val();
            var Totalpri=pri*quan;
            $("#sinamttotal"+proid).val(Totalpri.toFixed(1));
            return true;
        }



        function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
}
    </script>