<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Purchase Report</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view All Purchase Report details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <form method="post" action="">
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
                                </form>
                                <div style="width:100%;overflow-x:auto;">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <th>S.No.</th>
                                          <th>Purchase Order number</th>
                                          <th>Supplier Name</th>
                                          <th>Supplier Invoice number</th>
                                          <th>Discount (If any)</th>
                                          <th>Net Amount Exclusive of VAT/CST</th>
                                          <th>VAT/CST @ 2%/5%/12.5%</th>
                                          <th>Total Amount Inclusive of VAT/CST</th>
                                          <th>Date of Purchase</th>
                                          <th>Supplier Invoice Image</th>
                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; 
                                        if(!empty($record))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                        foreach ($viewporeport as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->pono;?></td>
                                            <td><?php echo $value->firstname.' '.$value->lastname;?></td>
                                            <td><?php echo $value->poveninvoiceno;?></td>
                                            <td><?php echo $value->podiscount;?></td>
                                            <td><?php echo $value->ponetamt; ?></td>
                                            <td><?php echo $value->potax;?></td>
                                            <td><?php echo $value->pototalamt;?></td>
                                            <td><?php echo $value->porecreatedon;?></td>
                                            <td>
                                            <?php if($value->poinvoiceimg==''){ ?>
                                            Bill Not found.
                                            <?php } else { ?>
                                            <a href="<?php echo base_url();?>assets/supplierinvoice/<?php echo $value->poinvoiceimg;?>" target="_blank"><img height="50" width="50" src="<?php echo base_url();?>assets/supplierinvoice/<?php echo $value->poinvoiceimg;?>"></a>
                                            <?php } ?>
                                            </td>
                                            
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>