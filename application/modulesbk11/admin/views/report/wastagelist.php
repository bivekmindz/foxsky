<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Wastage Product List</h2>
                    </div>
                    
                    
    <div class="page_box">
    <div class="sep_box1">
    <div class="col-lg-12">
                  <form method="post" action="">
                  <input style="margin: 5px 0px 15px 0px;" class="xls_download" type="submit" value="Download Excel" name="excel">
                </form> 
    <div class="gridview">
    <table id="search_filter" class="grid_tbl">
    <thead>
    <tr>
      <th bgcolor='red'>S.NO.</th>
      <th bgcolor='red'>Product Name</th>
      <th bgcolor='red'>Product SKU</th>
      <th bgcolor='red'>Wastage QTY</th>
      <th bgcolor='red'>Order Number</th>
      <th bgcolor='red'>Wastage Type</th>
      <th bgcolor='red'>Wastage Description</th>
      <th bgcolor='red'>Created BY</th>
      <th bgcolor='red'>Created On</th>
    </tr>
    </thead>
    <tbody>
       <?php 
        $i=1;
        foreach ($list as $value) {
           
         ?>
        <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $value->w_productname;?></td>
        <td><?php echo $value->w_skuno;?></td>
        <td><?php echo $value->w_qty;?></td>
        <td><?php echo $value->w_orderno;?></td> 
        <td><?php echo $value->w_type;?></td>
        <td><?php echo $value->w_desc;?></td>
        <td><?php echo $value->name.' ('.$value->email.')';?></td>   
        <td><?php echo $value->w_createdon;?></td>     
        </tr>      
        <?php $i++;} ?>
        

    </tbody>
   </table>
   
  </div>
  </div>
  </div>
  </div>