     <div class="wrapper">
       <?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Dashboard</h2>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
                    
                    <div class="col-lg-12">
                       <div class="right_main">
                        <div class="col-lg-3">
                            <div class="dash_box dash_box_2 d_box d_box_2 dash_box_3 dash_box_003">
                                <h2>Total Orders of the Day</h2>
                                <div class="dash_b_t dash_b_t1"><?php echo $result['orderCount']->total; ?></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="dash_box dash_box_2 d_box d_box_2 dash_box_3 dash_box_003">
                                 <h2>Total Sales of Day</h2>
                                <div class="dash_b_t dash_b_t1">
                                  <span class="das_i1"><i class="fa fa-inr" aria-hidden="true"></i></span>
                                  <span class="das_i2"><?php if(empty($result['pricofday']->total)){ echo 0; } else { echo $result['pricofday']->total; } ?></span></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="dash_box dash_box_2 d_box d_box_2 dash_box_3 dash_box_003">
                                 <h2>Total Sales Of Month</h2>



                                <div class="dash_b_t dash_b_t1"> <span class="das_i1"><i class="fa fa-inr" aria-hidden="true"></i></span><span class="das_i2"><?php if(empty($result['totalofmonth']->total)){ echo 0; } else { echo $result['totalofmonth']->total; } ?></span>
                            </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-3">
                            <div class="dash_box dash_box_2 d_box d_box_2 dash_box_3 dash_box_003">
                                 <h2>Order Of Month</h2>
                                <div class="dash_b_t dash_b_t1"><?php echo $result['orderofmonth']->total; ?></div>
                            </div>
                        </div>
                        </div> 
                        </div>
                        
                        
                        <div class="col-lg-12">
                        <div class="left_border">
                        <div class="col-lg-4">
                            <div class="dash_box dash_box_2 dash_box_3 dash_box_04">
                                 <h2>Pending Order</h2>
                                <div class="dash_b_t"><?php echo $result['pendingorder']->total; ?></div>
                            </div>
                        </div>
                        
                        <div class="col-lg-4">
                            <div class="dash_box dash_box_2 dash_box_3 dash_box_04">
                                 <h2>Dispatch Order</h2>
                                <div class="dash_b_t"><?php echo $result['dispatchorder']->total; ?></div>
                            </div>
                        </div>
                        
                        <div class="col-lg-4">
                            <div class="dash_box dash_box_2 dash_box_3 dash_box_04">
                                 <h2>Pending Payment</h2>
                                <div class="dash_b_t"><?php echo $result['pendingpaymentss']->total; ?></div>
                            </div>
                        </div>
                        </div>
                        </div>
                      
                        
                      
                        <div class="col-lg-12">
                        <div class="col-lg-2">
                            <div class="dash_box dash_box_1 dash_box_4 dash_box_5">
                               <h2>Total Orders</h2>
                                  <div class="dash_b_t"><?php echo $result['orderCountCod']->total; ?></div>
                           </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="dash_box dash_box_2 dash_box_4 dash_box_5">
                               <h2>Total Vendors</h2>
                                <div class="dash_b_t"><?php echo $result['manuCount']->total; ?></div>
                            </div>
                        </div>
                         <div class="col-lg-2">
                            <div class="dash_box dash_box_1 dash_box_4 dash_box_5">
                               <h2>Total Consumers</h2>
                                <div class="dash_b_t"><?php echo $result['retailCount']->total; ?></div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="dash_box dash_box_2 dash_box_4 dash_box_5">
                                 <h2>Total Brands</h2>
                                <div class="dash_b_t"><?php echo $result['brandCount']->total; ?></div>
                            </div>
                        </div>

                         <div class="col-lg-2">
                            <div class="dash_box dash_box_2 dash_box_4 dash_box_5">
                                 <h2>Product Live</h2>
                                <div class="dash_b_t"><?php echo $result['productlive']->total; ?></div>
                            </div>   
                        </div>
                        <div class="col-lg-2">
                            <div class="dash_box dash_box_2 dash_box_5 dash_box_4">
                                 <h2>Brand Live</h2>
                                <div class="dash_b_t"><?php echo $result['brnadlive']->total; ?></div>
                            </div>
                        </div>
                        </div>
                        <div class="col-lg-12">
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="dash_tbl_d">
                                <h2>Recent Order</h2>
                                <table><tr><th>S.No.</th><th>Order ID</th><th>E-mail Address</th><th>Order Total</th></tr>
                                <?php
                                    $i=0;
                                    foreach($result['ordersTotal'] as $k=>$v){
                                    $i++;
                                ?>   
                                    <tr><td><?=$i?></td><td><a href="javascript:void(0);" style="cursor: text!important;"><?=$v->OrderNumber?></a></td><td><?=$v->Email?></td><td>Rs. <?=$v->TotalAmt?></td></tr>
                                <?php
                                    }
                                    $i=0;
                                ?>   
                                </table>
                                </div>
                            </div>
                                <div class="col-lg-6">
                                <div class="dash_tbl_d">
                                    <h2>Recent Vendors</h2>
                                    <table>
                                    <tr><th>S.No.</th><th>Order ID</th><th>E-mail Address</th><th>Contact no.</th></tr>
                                    <?php
                                        $i=0;
                                        foreach($result['manuTotal'] as $k=>$v){
                                        $i++;
                                    ?>
                                        <tr><td><?=$i?></td>
                                        <td><a href="javascript:void(0);" style="cursor: text!important;"><?=$v->vendorcode?></a></td>
                                        <td><?=$v->compemailid?></td><td><?=$v->contactnumber?></td></tr>
                                    <?php
                                        }
                                        $i=0;
                                    ?>
                                    </table>
                                </div>
                            </div>
                          </div>
                        </div>
                  </div>
                   
                </div>
            </div>
        </div>
    </div>
    </div>