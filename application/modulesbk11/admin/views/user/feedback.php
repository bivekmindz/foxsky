<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Feedback</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>In this Section Admin can view and approve Users Feedback.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <table class="grid_tbl" id="search_filter">
                                    <thead>

                                        <tr>
                                            <th>S.NO</th>
                                            <th>User Name</th>
                                            <th>User Email</th>
                                            <th>User Contact no</th>
                                            <th>Feedback Comment</th>
                                            <th>Created On</th>
                                            <th>Approved by</th>
                                            <th>Approved Date</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; foreach ($viewfeedback as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->f_name ;?></td>
                                            <td><?php echo $value->f_emailid ;?></td>
                                            <td><?php echo $value->f_contactno ;?></td>
                                            <td><?php echo $value->f_comment ;?></td>
                                            <td><?php echo $value->f_createdon ;?></td>
                                            <td><?php echo $value->u_modifiedby; ?></td> 
                                            <td><?php echo $value->f_modifiedon; ?></td>
                                           <td><a href="<?php echo base_url()?>admin/user/feedbackdelete/<?php echo $value->f_id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/user/feedbackstatus/<?php echo $value->f_id.'/'.$value->f_status; ?>"><?php if($value->f_status=='0') { ?>Unapprove<?php }else { ?>Approved<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>