<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Mobile Home </h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add, edit and delete slider banners.</p>
                        </div>
                    </div>
                    <form method="post" action="" enctype="multipart/form-data">
                    <div class="page_box">
                    <div class='flashmsg'><?php echo $this->session->flashdata('messag'); ?></div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Slider Image <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="imgslide" name="imgslide" type="file" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    </form>



                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S:NO</th>
                                            <th>Mobile Slider Image</th>
                                          
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   <?php $i=0; foreach ($viewslide as $key => $value) { $i++; ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><img height="60px" width="60px" src="<?php echo base_url();?>assets/mobilebanners/<?php echo $value->b_img;?>"></td>
                                            
                                            <td><a href="<?php echo base_url()?>admin/add_mob_banner/updatesmobbanner/<?php echo $value->b_id?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/add_mob_banner/deletemobileslider/<?php echo $value->b_id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/add_mob_banner/statusmobileslider/<?php echo $value->b_id.'/'.$value->b_img_status; ?>"><?php if($value->b_img_status=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                    <?php } ?> 
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


