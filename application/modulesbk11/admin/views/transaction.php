<!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#example').DataTable();
        });
    </script>   
    <div class="wrapper">
    <?php  $this->load->view('helper/nav'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Process Payment</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this section, you can see the list of transactions!</p>
                                <p style="color:green;text-align:center;">
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            </div>
                        </div>
                        <form action="" method="get" id="filterForm">
                        <div class="page_box">
                            <div class="sep_box1">
                                <div class="col-lg-10">
                                    <div class="row">
                                      
                                       <div class="col-lg-3">
                                                <select name="pay_type" class="form-control">
                                                    <option value="">-- Pay Type --</option>
                                                   <!--  <option value="Receivables" <?php if($this->input->get('pay_type')=="Receivables") echo "selected";?>>Receivables</option> -->
                                                    <option value="Payables" <?php if($this->input->get('pay_type')=="Payables") echo "selected";?>>Payables</option>
                                                </select>
                                        </div>
                                       
                                      
                                        <div class="col-lg-3">
                                    <input type="text" id="month" placeholder="01-2017" name="month" class="monthPicker form-control" readonly value="<?php echo $_GET['month']; ?>"/>
                                        </div>     

                                        
                                                <div id="cycle" class="col-lg-6"> <div class="row"> <div class="col-lg-7">
            <input type="text" class="form-control" readonly value="<?php $a=$_GET['cycle'];
if($a!=''){
            $b=explode('*', $a);
            echo $b[4].'-'.$b[0].', '.date('d-m-Y',strtotime($b[5]));} ?>"/>
                                        </div> </div> </div>
                                                                          
                                        <!--<div class="col-lg-2">
                                            <div class="submit_tbl">
                                                <input type="submit" value="Search" name="filter" class="btn_button sub_btn" />
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <form method="post">
                                        <?php if($this->input->get('pay_type')!=""){ ?>
                                            
                                    <div class="col-lg-10">Select All <input type="checkbox" id="checkAll"></div>
                                     <div class="col-lg-2"><input type="submit" name="save_data" value="Process" class="form-control" /></div>
                                     <?php } ?>
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Date From & To (Settlement Cycle)</th>
                                                <th bgcolor='red'>Retailer ID</th>
                                                <th bgcolor='red'>Company Name</th>
                    
                                               <!--  <th bgcolor='red'>No. of Stores</th> -->                                               
                                                <!-- <th bgcolor='red'>Amount Receivable</th> -->
                                                <th bgcolor='red'>Amount Payable</th>
                                                <!-- <th bgcolor='red'>Arears</th>
                                                <th bgcolor='red'>Interest Amount</th> -->
                                                <th bgcolor='red'>Net Amount</th>                                              
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php if($this->input->get('pay_type')!="")
                                            {
                                        $i=0;
                                            foreach ($view as $value) { //p($value);
                                            $i++;
                                           
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?>&nbsp;<input type="checkbox"  name="checkvalue[]" value="<?php echo $value->s_admin_id; ?>"></td>
                                            <td><?php $cycle=explode("*",$this->input->get('cycle'));
                                                //echo $cycle_day=$cycle[0];
                                              echo $start_date=date("d-m-Y", strtotime(trim($cycle[1]))).' To ';
                                              echo $end_date=date("d-m-Y", strtotime($cycle[2])).' Pay Date ';
                                              echo $pay_date=date("d-m-Y", strtotime($cycle[3]));?>

                                            </td>                                           
                                           <td><?php echo $value->retailer_codeid; ?></td> 
                                            <td><?php echo $value->company_name; ?></td>
                                            <!-- <td><?php echo $value->storecount; ?></td> -->
                                            <!-- <td><?php if($value->pay_type=='Receivables')
                                                    echo round($value->totalamt,4);
                                                    else
                                                     echo '0';  ?></td> -->
                                       
                                            <td><?php if($value->pay_type=='Receivables')
                                                    echo '0';
                                                    else
                                                     echo '-'.$value->totalamt;  ?></td>
                                           <!--  <td>0</td>
                                            <td>0</td> -->
                                            <td><?php if($value->pay_type=='Receivables')
                                                    echo '+'.round($value->totalamt,4);
                                                    else
                                                     echo '-'.$value->totalamt;  ?></td>
                                            
                                        </tr>
                                    <?php } }?>

                                        </tbody>
                                    </table>
                                </form>
                                </div>
                            </div>
                        </div>

                       
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>

<script type="text/javascript">
function getday()
{
    var dayselect=$('#month').val();
    var day1=dayselect.split('-');
    var filter_date_string='';
     <?php if(isset($_GET['cycle']) && $_GET['cycle'] != '') { ?>
        var filter_date_string = "<?php echo $_GET['cycle'] ?>";
    <?php } ?>

    $.ajax({
    url:'<?php echo base_url()?>admin/receipts/getdays',
    type:'post',
    data:{'month':day1[0],'year':day1[1],'filter_date_string':filter_date_string},
    success: function(data){
        //alert(data);
        $('#cycle').html(data);
      //$('#msg'+ids).html('Successfully Updated!').css('color','green');
    }

  });
}

<?php if(isset($_GET['month']) && $_GET['month'] != '') { ?>
    getday();
<?php } ?>

function schange(ids) {
  var st=$('#status'+ids).val();
  //alert(st);
  $.ajax({
    url:'<?php echo base_url()?>admin/receipts/updatereceipts',
    type:'post',
    data:{'uid':ids,'ust':st},
    success: function(data){
      $('#msg'+ids).html('Successfully Updated!').css('color','green');
    }

  });
}

function showdealdetailstr(id)
{
  
  $('.dealdetailstr').closest('tr').addClass('str');
  $('#sho_'+id).closest('tr').removeClass('str');

}
</script>

<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#filterForm").validate({
          rules: {            
              cycle : "required",
              month : "required",
              year : "required",
              store_id : "required",
              pay_type : "required"             
          },
          messages: {
              cycle : "This field is required!",
              month : "This field is required!",
              year : "This field is required!",
              store_id : "This field is required!",
              pay_type : "This field is required!"               
          },
          submitHandler: function(form) {
            form.submit();
          }
      });
   });
</script>

<style type="text/css">
.deal_detail{
    float:left;
    width:100%;
    height:auto;
    background:#f1f1f1;
    padding:10px 10px;
}
.deal_left{
    float:left;
    width:25%;
}
.d_image{
    float:left;
    width:100%;
    background: #fff;
    padding:6px 6px;
}
.d_image img{
    width:100%;
}
.deal_right{
    float:left;
    width: 75%;
    padding: 0px 20px;
}
.deal_row{
    float:left;
    width:100%;
    padding:7px 0px;
}
.deal_d2
{
    float:left;
    width:50%;
    padding:0px 10px;
}
.deal_d1
{
    float:left;
    width:100%;
    padding:0px 10px;
}
.deal_text
{
    float:left;
    width:100%;
    font-weight:600;
}
.deal_text_b
{
    float:left;
    width:100%;
    font-weight:400;
    padding:8px 0;
}
.deal_text span{
    font-weight:400!important;
    margin-left:15px;
}
.approve_btn
{
    float:right;
    padding:8px 12px;
    background: green;
    color:#fff!important;
}
.issue_btn
{
    float:right;
    padding:8px 12px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.t_area{
    float:left;
    width:60%;
}
.sbt_btn{
    float:left;
    padding:12px 15px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.issuediv{
    display:none;
}

.d_image img {
    width: 200px;
    height: 252px;
    text-align: center;
}

 </style>

  <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>-->
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
<script type="text/javascript">
$(document).ready(function()
{   
    $(".monthPicker").datepicker({
        dateFormat: 'mm-yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('mm-yy', new Date(year, month, 1)));
            getday();
        }
    });

    $(".monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
        //getday();

    });
});

$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
</script>
<!-- 
<label for="month">Month: </label>
<input type="text" id="month" name="month" class="monthPicker" /> -->