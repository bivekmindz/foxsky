<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            cattype         : "required",
            category_name   : "required",
            
        },
        // Specify the validation error messages
        messages: {
            cattype         : "Choose Category type",
            category_name   : "Category Name is required",
            
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
  <script src="<?php echo admin_url();?>js/jscolor.js"></script>
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Edit Category</h2>
    </div>
        <div class="page_box">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/category/viewcategory"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
              //p($vieww);
            ?>
        </div>
        <form id="addCont" action="" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        
        <?php if($vieww->parentid != 0){ ?>
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Parent Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input  type="text" name="parent_category" value="<?php echo $vieww->categoryname ?>" id="parent_category" readonly/></div>
            </div>
        </div>
        </div>
        <?php } ?>
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
               <div class="tbl_text">Child Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input  type="text" name="category_name" value="<?php echo $vieww->catname ?>" id="category_name"/></div>
            </div>
        </div>
        </div>
        
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Category Description</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><textarea name="cat_description" id="cat_description"><?php echo $vieww->catdesc ?></textarea></div>
            </div>
        </div>
        </div>
        <!-- <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Sort Order </div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input type="number" min="1" style="width: 74%;padding: 8px 5px;" class="cat_level" value="<?php echo $vieww->ordersort ?>" name="cat_level" id="cat_level" /></div>
            </div>
        </div>
        </div>
       
        </div> -->
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Meta Tag Title</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input id="meta_title" type="text" value="<?php echo $vieww->metatitle ?>"  name="meta_title"/></div>
            </div>
        </div>
        </div>
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Meta Tag Desc</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input id="meta_desc" type="text" value="<?php echo $vieww->metadesc ?>" name="meta_desc"/></div>
            </div>
        </div>
        </div>
       
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Meta Tag Keywords</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input id="meta_keywords" type="text" value="<?php echo $vieww->metakeywords ?>" name="meta_keywords" /></div>
            </div>
        </div>
        </div>
       <!--  <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Manufacture Display </div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input type="checkbox" name="cbcheck" value="<?php echo $vieww->isvendor ?>" checked id="cbcheck"  /></div>
            </div>
        </div>
        </div>
       
        </div> -->
        <!-- <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Category Url </div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input id="category_url" type="text" value="<?php echo $vieww->url ?>"  name="category_url" value="<?php echo $vieww->isvendor ?>" /></div>
            </div>
        </div>
        </div>
        
       
        </div> -->
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="Submit1" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>


        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
    