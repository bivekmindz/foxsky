 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            cattype         : "required",
            category_name   : "required",
            parent_category : "required",
        },
        // Specify the validation error messages
        messages: {
            cattype         : "Choose Category type",
            category_name   : "Category Name is required",
            parent_category : "Parent Category is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
            
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Category</h2>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
                            <div class="col-lg-12">
                    <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/category/viewcategory"><button>CANCEL</button></a>
           </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
<form id="addCont" action="" method="post" enctype="multipart/form-data" >
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Category Type <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="cattype" id="typeid" onchange="categoryvalue();">
                                                <option value="">Select Type</option>
                                                <option value="parent">Parent Category</option>
                                               <!--  <option value="child">Child Category</option> -->
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="col-lg-6" id="childid" style='display:none'>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Parent Category <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <select name="parent_category" id="parent_category" onclick="viewshort();">
                                        <option value="">Select Parent Category</option>
                                        <?php 
                                         foreach ($parentCatdata as $Catdata){  ?>	 
									     <option value="<?php echo $Catdata->catid.'-'.$Catdata->catlevel; ?>"><?php echo $Catdata->catname; ?></option>	
							            <?php } ?>
                                        </select></div>
                                        <label id="cat_error" class="error"></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                        <div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Category Name <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="category_name" name="category_name" type="text" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Description</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><textarea name="cat_description" id="cat_description"></textarea></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="sep_box">
                           <!--  <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Category Url </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="category_url" type="text" name="category_url" /></div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Meta Title</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="meta_title" type="text"  name="meta_title"/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Meta Tag Description </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="meta_desc" type="text" name="meta_desc"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Meta Tag Keyword</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="meta_keywords" type="text" name="meta_keywords" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="sep_box" id="cat_image1" style="display: none;">
                           <!--  <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Category Logo</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="cat_logo" type="file" name="cat_logo"/></div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Category Banner</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="cat_ban1" type="file" name="cat_ban1" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="sep_box" id="cat_image2" style="display: none;">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Category Banner Second</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="cat_ban2" type="file" name="cat_ban2"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Category Banner Third</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="cat_ban3" type="file" name="cat_ban3" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>  -->
                        <div class="sep_box">
                           <!--  <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Sort Order </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                    <input type="number" min="1"  class="cat_level" name="cat_level" id="cat_level" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Manufacture Display </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input type="checkbox" name="cbcheck" id="cbcheck" checked value="1" /></div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        </div>

  <!--  ---------------------------------------------- Child category  -------------------------------->




  <!--  ---------------------------------------------- child category  -------------------------------->



                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                </form>
                    </div>
                   
                </div>
            </div>
            <!---------------------- view category ---------------------------------------->
            <div class="page_box" id="parentvieww" style='display:none'>
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Order Sort</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <?php foreach ($vie2w as $key => $value) {
                                         ?>
                                            <td><?php  echo $value->catname?></td>
                                            <td><?php  echo $value->ordersort?></td>
                                           <?php } ?>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="page_box" style='display:none' id="childvi" >
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Order Sort</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody id="test">
                                       
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
<!---------------------- view category ---------------------------------------->

        </div>
 </div>
    </div> 
    

    <script type="text/javascript">
    function categoryvalue(){
        var id=$('#typeid').val();
        
        if($.trim(id) == 'parent'){
            $('#parentid').show();
            $('#parentvieww').show();
            $('#childid').hide();
             $('#childvi').hide();
             $('#cat_image1').show();
             $('#cat_image2').show();
        }
        else{
           // alert('hi');
            $('#childid').show();
            $('#parentvieww').hide();
            $('#parentid').hide();
            $('#cat_image1').hide();
            $('#cat_image2').hide();
        }
    }

   function viewshort(){
    var catids=$('#parent_category').val();
    $('#childvi').show();
    //alert(catids);
      $.ajax({
        url: '<?php echo base_url()?>admin/category/catviewlevel',
        type: 'POST',
        //dataType: 'json',
        data: {'catids': catids},
        success: function(data){
            $('#test').empty();
            
            $('#test').html(data);
        }
    });
   }



    </script>