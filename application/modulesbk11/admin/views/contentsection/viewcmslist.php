<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2> CMS Listing</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can manage and edit all the content pages.</p>
                        </div>
                    </div>
                    <div class="page_box">
                    <div class="">  
                           <?php  echo $this->session->flashdata('message'); ?>
                    </div>
                         <div class="sep_box">
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Menu Category</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; foreach ($cms as $key => $value) { $i++; ?>
                                       <tr>

                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->titlename;?>
                                            </td>
                                           <td><?php echo substr($value->description,100);?></td>
                                          <td><a href="<?php echo base_url()?>admin/ckeditor/index/<?php echo $value->id ?>" ><i class="fa fa-pencil"></i></a><!--&nbsp;
                                            <a href="<?php echo base_url()?>admin/ckeditor/delete/<?php echo $value->id ?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>-->&nbsp;
                                            <a href="<?php echo base_url(); ?>admin/ckeditor/statuscms/<?php echo $value->id.'/'.$value->status; ?>"><?php if($value->status=='0') { ?><!--<i class="fa fa-remove"></i>-->Activated<?php }else { ?><!--<i class="fa fa-check"></i>-->Deactivated<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                   </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    