<?php //p($newsview);exit;?>
<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>First Purchase Offer List</h2>
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can view the First Purchase Offer Listing.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1"> 
                            <div class="col-lg-12">
                            <form method="post" action="<?php echo base_url().'admin/wallet/add_first_purchase_offer'; ?>">
                                 <div class="download-right"> <input class="xls_download" type="submit" value="Add First Purchase" name="newsexcel">
                                </div></form>  

                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>

                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th>S.NO.</th>
                                            <th>Customer Name</th>
                                            <!-- <th>Amt Type</th> -->
                                            <th>Cashback Amt</th>
                                            <th>Transfer Cashback Reason</th>
                                            <th>Transfer Date</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                   
                                    foreach ($wallet as $key => $value) { $i++;?>
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->firstname.' '.$value->lastname.' ('.$value->compemailid.')';?></td>
                                            <!-- <td><?php echo ucfirst($value->w_type);?></td> -->
                                            <td><?php echo $value->w_amount;?></td>
                                            <td><?php echo $value->w_reason;?></td>
                                            <td><?php echo $value->w_date;?></td>
                                           
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>