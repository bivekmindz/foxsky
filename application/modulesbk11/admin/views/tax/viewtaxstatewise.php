<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage State Wise Tax</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can add Category and State wise tax values.</p>
                        </div>
                    </div>
                    <div class="page_box">
                           
<!---############################# search section start #####################################################-->
                   <div class="select_main">
                    <form method="get" action='<?php echo base_url('admin/tax/viewtaxstatewise'); ?>' id='searchfrm'>
                        <div class="sep_box padding_box">
                            <div class="col-lg-4">
                            <div class="tbl_input">
                            <select id="tax_searchstate" name="tax_searchstate" required="true">
                            <option value="">Select Search State</option>
                            <?php foreach ($stvieww as $k => $val) { ?>
                                <option value="<?php echo $val->stateid; ?>"><?php echo $val->statename; ?></option>
                            <?php }?>
                            </select>
                            </div>
                            </div>
                            <div class="col-lg-4">
                            <div class="tbl_input">
                            <select id="tax_searchmcat" name="tax_searchmcat" required="true" onchange="childcategory();">
                                <option value="">Select Search Category</option>
                            <?php foreach ($viewwcattid as $key => $value) { ?>
                                <option value="<?php echo $value->catid; ?>"><?php echo $value->catname; ?></option>
                            <?php }?>
                            </select>
                            </div>
                            </div>
                            </div>
                            <div class="sep_box">
                            <div class="col-lg-4">
                            <div class="tbl_input">
                            <select id="tax_searchcat" name="tax_searchcat" required="true" onchange="subchildcategory();">
                            <option value="">Select Search Sub-category</option>
                            </select>
                            </div>
                            </div>
                            <div class="col-lg-4">
                            <div class="tbl_input">
                            <select id="tax_searchchildcat" name="tax_searchchildcat" required="true">
                            <option value="">Select Search Child Sub-category</option>
                            </select>
                            </div>
                            </div>
                            <div class="col-lg-4">
                            <div class="submit_tbl">
                            <input type="button" onclick='location.href="<?php echo base_url('admin/tax/viewtaxstatewise'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                            <input id="validate_frm" type="submit" name="filterby" id="filterby" value="Search" class="btn_button sub_btn">
                            </div>
                            </div>
                            </div>
                            </form>
                            </div> <!--end of select main-->
                          
  <!---############################# search section end #####################################################-->
                            <!-- <div class="sep_box"> -->
                                <!-- <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/tax/addtaxstatewise"><button>Add Tax</button></a>
                                </div> -->
                                <div class="state_main">
                                 <form method="post" action="">
                                 <div class="col-lg-3">
                                    <div class="tbl_input">
                                        <select id="tax_xlsstate" name="tax_xlsstate" required="true">
                                        <option value="">Select State for Excel</option>
                                        <?php foreach ($stvieww as $k => $val) { ?>
                                            <option value="<?php echo $val->stateid; ?>"><?php echo $val->statename; ?></option>
                                        <?php }?>
                                        </select>
                                    </div>
                                 </div>
                                 <div class="col-lg-3">
                                    <div class="submit_tbl">
                                        <input type="submit" value="Download Excel" name="newsexcel" class="btn_button sub_btn xls_download">
                                    </div>
                                 </div>        
                                </form>
                                </div> <!--end of state_main-->

                                <div class="col-lg-12">
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                 <form method="post" action="">
                                <input type="submit" name="submitstatus" value="Status"> 
                                <!-- <input type="submit"  name="updatetax" value="Edit" class="sts_btn">  -->
                                <table class="grid_tbl">
                                    <thead>

                                        <tr>
                                            <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span></th>
                                            <th>S.NO.</th>
                                            <!-- <th>Brand</th> -->
                                            <th>Category</th>
                                            <th>Country</th>
                                            <th>State</th>
                                            <th>Tax Name</th>
                                            <th>Tax Value</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0;
                                    if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          } 
                                        foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr> 
                                            <td><input type='checkbox' name='attstatus[]' class='chkApp' value='<?=$value->taxvid; ?>'> </td> 
                                            <td><?php echo $i ;?></td>
                                            <!-- <td><?php echo $value->brandname ;?></td> -->
                                            <td><?php echo $value->catname ;?></td>
                                            <td><?php echo $value->name ;?></td>
                                            <td><?php echo $value->statename ;?></td>
                                            <td><?php echo $value->taxname ;?></td>
                                            <td><!-- <input type="text" name="taxvalue[<?php echo $value->taxvid;?>]" class="numeric" value="<?php echo $value->taxprice ;?>" size="3" style="border: none;" maxlength="4"> -->
                                            <?php echo $value->taxprice ;?>&nbsp;%
                                            
                                            </td>
                                            <td>
                                            <input type='hidden' name='attstatu[<?php echo $value->taxvid;?>]'  value='<?=$value->tstatus?>'>
                                            <a href="<?php echo base_url(); ?>admin/tax/taxstatusstatewise/<?php echo $value->taxvid.'/'.$value->tstatus; ?>"><?php if($value->tstatus=='0') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
   <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
    </script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(function () {
            $(".numeric").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 46) || specialKeys.indexOf(keyCode) != -1);
                $(".error1").css("display", ret ? "none" : "inline");
                return ret;
            });
            $(".numeric").bind("paste", function (e) {
                return false;
            });
            $(".numeric").bind("drop", function (e) {
                return false;
            });
        });


   function childcategory(){
    var catid=$('#tax_searchmcat').val();
     
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/catvalue',
        type: 'POST',
        //dataType: 'json',
        data: {'catid': catid},
        success: function(data){
             
            var option = '<option value="">Select Search Sub-category</option>'
            $('#tax_searchcat').empty();
            $('#tax_searchcat').append(option+data);
        }
    });
    
   }

   function subchildcategory(){
        var catid=$('#tax_searchcat').val();
            //alert(catid);
            $.ajax({
                url: '<?php echo base_url()?>admin/attribute/subbranddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                      //alert(data);
                      var  option_brand = '<option value="">Select Search Child Sub-category</option>';
                      $('#tax_searchchildcat').empty();
                      $("#tax_searchchildcat").append(option_brand+data);  
                     
                }
            });
    }

    </script>