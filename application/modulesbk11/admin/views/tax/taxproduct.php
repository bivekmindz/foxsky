     <div class="wrapper">
       <?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                   <form method="POST" enctype="multipart/form-data">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Dashboard</h2>
                    </div>
                    <div class="page_box">
                          <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/tax/viewtaxprod">CANCEL</a>
        </div>
                    
             
                     
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Brand</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                         <select name="brandid" id="brandid" onclick="brandcate()" >
                                        <option value="0">Select Brand</option>
                                        <?php foreach ($vieww as $key => $value) {
                                        ?>
                                        <option <?php if ($_POST["brandid"] == $value->id ) echo 'selected' ; ?> value="<?php echo $value->id ?>"><?php echo $value->brandname ?></option>
                                        <?php }?>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6" style='display:none' id="staterid">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Category</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <select id="catid" name="catid" value="catid" >
                                        <option value="">Select Category</option>
                                        </select> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="Submit1" type="submit" name="submitcat" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        </div>
                              <div class="page_box">
                              <h> Upload Tax</h>
                          <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Country</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <select name="countryid" id="countryid" onclick="selectstates();" >
                                        <option value="">Select  Country</option>
                                        <?php foreach ($viewcountry as $value){  ?>  
                                        <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>                  
                                        <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6" style='display:none' id="staterdid">
                            <div class="row">
                            <div class="col-lg-4">
                            <div class="tbl_text">State</div>
                            </div>
                            <div class="col-lg-8">
                            <div class="tbl_input">
                            <select id="stateid" name="stateid" value="stateid" >
                            <option value="">Select State</option>
                            </select> 
                            </div>
                            </div>
                            </div>
                            </div>
                        </div>
                  <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
        <div class="col-lg-4">Select file</div>
        <div class="col-lg-8"><input type="file" name="userfile" id="userfile" /></div>
        </div>
        </div>
        
        </div>
          <div class="sep_box">
          <div class="col-lg-6">
          <div class="col-lg-4"></div>
        <div class="col-lg-8">
        <div class="submit_tbl">
        <input type="submit" name="submit" class="btn_button sub_btn"/></div></div></div>
          </div>
        
                    </div>
                    <div class="page_box">
                    <input type="submit" value="Download" name="Download" class="hbutton" style="float:right;"/>
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr> 
                                            <th bgcolor='red'><input type="checkbox" id="selAll">Select All</th>
                                            <th>Product Name</th>
                                            <th>Product Sku</th>
                                            <th>Selling Price</th>
                                        
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    foreach ($prvieww as $key => $value) {
                                        
                                     ?>
                                        <tr>
                                            <td><input type='checkbox' name='proid[]' class='chkApp' value='<?=$value->proid?>'> </td> 
                                            <td><?php echo $value->proname; ?></td>
                                            <td><?php echo $value->prosku; ?></td>
                                            <td><?php echo $value->prodsellingprice; ?></td>
                                            <input type="hidden" name="procat"  value="<?php echo $value->catid;?>">
                                            <input type="hidden" name="proname[<?php echo $value->proid;?>]"  value="<?php echo $value->proname;?>">
                                            <input type="hidden" name="prosku[<?php echo $value->proid;?>]"  value="<?php echo $value->prosku;?>">
                                            <input type="hidden" name="prosellin[<?php echo $value->proid;?>]"  value="<?php echo $value->prodsellingprice;?>">
                                        </tr>
                                     <?php }?>   
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                                </form>
            </div>
        </div>
    </div>
    </div>
<script type="text/javascript">
    function brandcate(){
     var barid=$('#brandid').val();
     
     $('#staterid').show();
     $.ajax({
        url: '<?php echo base_url()?>admin/product/brandcategory',
        type: 'POST',
        data: {'barid': barid},
        success: function(data){
        var  option_brand = '<option value="">Select Category</option>';
        $('#catid').empty();
        $("#catid").append(option_brand+data);
           
         }
  });
    
   }
   brandcate();


function selectstates(){
     var countryid=$('#countryid').val();
     $('#staterdid').show();
     $.ajax({
        url: '<?php echo base_url()?>admin/geographic/countrystate',
        type: 'POST',
        //dataType: 'json',
        data: {'countryid': countryid},
        success: function(data){
        var  option_brand = '<option value="">Select State</option>';
        $('#stateid').empty();
        $("#stateid").append(option_brand+data);
           
         }
  });
    
   }

    </script>

    <script type="text/javascript">
  $(document).ready(function() {
    $('#selAll').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});


</script>