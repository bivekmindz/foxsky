<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Brand Mapping</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can map specific brand to specific category.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">
                        <!---############################# search section start #####################################################-->
                           <!--  <form method="get" action='<?php echo base_url('admin/category/viewbrandmaster'); ?>' id='searchfrm'>
                            <div class="col-lg-3">
                            <div class="tbl_input">
                            <select id="filter_by" name="filter_by">
                            <option value="">Filter BY</option>
                            <option value="brand_name" <?php echo ($_GET['filter_by']=='brand_name')?'selected':'';?>>Brand Name</option>
                            <option value="categ_name"<?php echo ($_GET['filter_by']=='categ_name')?'selected':'';?>>Category Name</option>
                            </select>
                            </div>
                            </div>
                            <div class="col-lg-3" id="input_div">
                            <div class="tbl_input">
                            <input type="text" name="searchname" value="<?php echo (!empty($_GET['searchname']))?$_GET['searchname']:'';?>" id="searchname" class="valid input_cls">
                            </div>
                            </div>
                            <div class="col-lg-3">
                            <div class="submit_tbl">
                            <input id="validate_frm" type="button" value="Search" class="btn_button sub_btn">
                            <input type="button" onclick='location.href="<?php echo base_url('admin/category/viewbrandmaster'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                            </div>
                            </div>
                            </form> -->
  <!---############################# search section end #####################################################-->
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/category/addbrandmaster"><button>ADD Brand Master</button></a>
                                </div>
                                 <form method="post" action="">
                                  <input class="xls_download" type="submit" value="Download Excel" name="newsexcel">
                                </form>
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                 <form method="post" action="">
                                 
                                <input type="submit"  name="submit" value="Delete">
                                
                                <table class="grid_tbl" id="search_filter">
                                    <thead>

                                        <tr>
                                           <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span>
                                              <!-- / <br>
                                           <span style='border:0px color:blue; cursor:pointer;' id='DeselAll'>Deselect</span> --></th>
                                             <th>S:NO</th>
                                            <th>Brand Name</th>
                                            <th>Category Name</th>
                                            <th>Updated by</th>
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0;
                                         if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*20; 
                                            }
                                          }
                                         foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><input type='checkbox' name='attdelete[]' class='chkApp' value='<?=$value->id?>'> </td> 
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->brandname ;?></td>
                                            <td><?php echo $value->catname ;?></td>
                                            <td><?php echo $value->u_createdby ;?></td>
                                            <td><?php echo $value->bm_createdon ;?></td>                                           
                                           
                                            <td></i>
                                            <a href="<?php echo base_url()?>admin/category/brandmasterdelete/<?php echo $value->id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                           </a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                              <!--   <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div> -->
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
    $('#validate_frm').click(function(){
      var err=0;
     if($('#filter_by').val()=="")
     { 
        err++;
       $('#filter_by').css('border-color','red');
     }else{
       $('#filter_by').css('border-color','');
     }
     if($('#searchname').val().search(/\S/) == -1)
     { 
       err++;
       $('#searchname').css('border-color','red');
     }else{
       $('#searchname').css('border-color','');
     }
     if(err==0)
     {
      $('#searchfrm').submit();
     }
    })
  /*$(document).ready(function(){
    $('#selAll').click(function(){   
      $('.chkApp').each(function() { //loop through each checkbox
          this.checked = true;  //select all checkboxes with class "checkbox1"               
      });
});

  });
$(document).ready(function(){
    $('#DeselAll').click(function(){   
      $('.chkApp').each(function() { //loop through each checkbox
          this.checked = false;  //select all checkboxes with class "checkbox1"               
      });
});

  });*/


</script>

<script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
    </script>