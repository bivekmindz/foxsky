<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Country</h2>
    
                    </div>
                    <div class="page_box">
                    	<div class="col-lg-12">
                    		<p> In this section , admin will be able to add the country names
and can view the country list</p>
                    	</div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">

                                <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/geographic/add_country"><button>ADD COUNTRY</button></a>
                                </div>
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S:NO</th>
                                            <th>Country Name</th>
                                            <th>Country Code</th>
                                            <th>Updated by</th>
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->name ;?></td>
                                            <td><?php echo $value->code ;?></td>
                                            <td><?php if(empty($value->modifiedby)){ echo $value->u_createdby; } else { echo $value->u_modifiedby; } ?></td> 
                                            <td><?php if(empty($value->modifiedby)){ echo $value->createdon; } else { echo $value->modifiedon; } ?></td>
                                            <td><a href="<?php echo base_url()?>admin/geographic/countryupdate/<?php echo $value->conid?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/geographic/countrydelete/<?php echo $value->conid?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/geographic/countrystatus/<?php echo $value->conid.'/'.$value->counstatus; ?>"><?php if($value->counstatus=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>