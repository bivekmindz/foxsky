<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage State</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this section , admin will be able to add the State names
and can view the State list.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">

                                <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/geographic/addstate"><button>ADD STATE</button></a>
                                </div>
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Country Name</th>
                                            <th>State Name</th>
                                            <th>State Code</th>
                                            <th>Updated by</th>
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; foreach ($data as $key => $value) { $i++; ?>
                                       <tr>

                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->name;?></td>
                                            <td><?php echo $value->statename;?></td>
                                            <td><?php echo $value->statecode;?></td>
                                            <td><?php if(empty($value->modifiedby)){ echo $value->u_createdby; } else { echo $value->u_modifiedby; } ?></td> 
                                            <td><?php if(empty($value->modifiedby)){ echo $value->createdon; } else { echo $value->modifiedon; } ?></td>
                                            <td><a href="<?php echo base_url()?>admin/geographic/stateupdate/<?php echo $value->stateid?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/geographic/statedelete/<?php echo $value->stateid?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/geographic/statestatus/<?php echo $value->stateid.'/'.$value->isdeleted; ?>"><?php if($value->isdeleted=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                   </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>