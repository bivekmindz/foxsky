<!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>


   <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('#example').DataTable();
                                    } );
                                </script>    
<style type="text/css">
    

    .gridview{    width: 100%;
    float: left;
    overflow-y: auto;}
</style>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/viewer.css">
   <script src="<?php echo base_url();?>assets/js/viewer.js"></script>
  <script src="<?php echo base_url();?>assets/js/main.js"></script>

    <div class="wrapper">
    <?php  $this->load->view('helper/nav'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Upload Receipts Details</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this section, you can see the list of Receipts!</p>
                                <p style="color:green;text-align:center;">
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            </div>
                        </div>

 <form method="post" action="">
<input style="background:#79400D;border: none;color: #fff;padding: 5px 10px;font-size: 13px; float:right;  margin-bottom: 10px;" type="submit" value="Download Excel" name="receipts">
</form>  


                        <div class="page_box">
                            <div class="col-lg-12">

                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Settlement Cycle</th>
                                                <th bgcolor='red'>Transaction ID</th>
                                                <th bgcolor='red'>Bill UID</th>
                                                <th bgcolor='red'>Bill Image</th>
                                                <th bgcolor='red'>Store ID</th>
                                                <th bgcolor='red'>Store Name</th>
                                                <th bgcolor='red'>Location</th>
                                                <th bgcolor='red'>User ID</th>
                                                <th bgcolor='red'>Date & Time of Upload</th>
                                                <th bgcolor='red'>Date of Purchase</th>
                                              <!--   <th bgcolor='red'>Date & Time of Bill sent to retailer</th> -->
                                                <th bgcolor='red'>Bill No.</th>
                                                <th bgcolor='red'>Bill Amt.</th>
<?php /*if($this->session->userdata('popcoin_login')->s_usertype==2 || $this->session->userdata('popcoin_login')->s_usertype==4){ }else{ */?>
                                                <th bgcolor='red'>Paid By Cash/Card</th>
                                               <!--  <th bgcolor='red'>Paid By Mypopcoins</th> -->
                                                <?php //} ?>
                                                <th bgcolor='red'>Cashback %</th>
                                                <th bgcolor='red'>Cashback Amt.</th>
                                               <th bgcolor='red'>Approved Date & Time</th>
                                               <!--  <th bgcolor='red'>Status</th> -->                                             
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $i=0;
                                        $b_amt=0;
                                            foreach ($view as $value) {
                                            $i++;
                                           
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            
                                            <td><?php echo date("d-m-Y", strtotime($value->from_date)).' To '.date("d-m-Y", strtotime($value->to_date)).' </br>Pay Date </br>'.date("d-m-Y", strtotime($value->pay_date)); ?></td>
                                            <td><?php echo $value->txn_id; ?></td>
                                            <td><?php echo $value->bill_uid; ?></td>
                                            <td>
                                               <ul class="docs-pictures clearfix">
            <li><img width="50px;" height="50px;" src="<?php echo base_url().'public/'.$value->bill_img; ?>"></li></ul>
                                                <?php //echo $value->bill_img; ?>
                                            </td>
                                            <td><?php echo $value->s_storeunid; ?></td> 
                                            <td><?php echo $value->s_name; ?></td>
                                            <td><?php echo $value->location; ?></td>
                                            <td><?php echo $value->t_FName;//$value->user_uid; ?></td> 
                                            <td><?php echo date("d-m-Y h:i:s", strtotime($value->re_createdon)); ?></td>
 
                                            <td><?php echo date("d-m-Y", strtotime($value->date_of_purchase)); ?></td>
                                           <!--  <td><?php echo date("d-m-Y h:i:s", strtotime($value->re_modified)); ?></td> -->
                                            <td><?php echo $value->bill_no; ?></td>
                                            <td><?php /*if($this->session->userdata('popcoin_login')->s_usertype==2 || $this->session->userdata('popcoin_login')->s_usertype==4){ echo $bamt=$value->bill_update_amt; }else{ ?>
                                            <?php echo $bamt=$value->bill_amt; ?>
                                            <?php } ?></td>
                                            <?php if($this->session->userdata('popcoin_login')->s_usertype==2 || $this->session->userdata('popcoin_login')->s_usertype==4){ }else{*/ 
                                            echo $bamt=$value->bill_amt; ?></td>
                                            <td><?php echo $bamt=$value->bill_update_amt; ?></td>
                                            <!-- <td><?php echo $value->popcoins_amt; ?></td> -->
                                            <?php //} ?>
                                           <td><?php
echo $value->mpc_margin. ' %'; ?></td> 
                                            <td><?php //echo $value->cashback_amt;
echo $bamt1=($value->bill_update_amt*$value->mpc_margin)/100; ?></td>
                                            <td><?php echo date("d-m-Y h:i:s", strtotime($value->re_modified)); ?></td>
                                           
                                    
                                    <?php $b_amt +=$bamt1; } ?>

                                        </tbody>
                                    </table>
                                    Total Amount <?php echo ' :'.$b_amt; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>

<script type="text/javascript">
function schange(ids) {
  var st=$('#status'+ids).val();
  //alert(st);
  $.ajax({
    url:'<?php echo base_url()?>admin/receipts/updatereceipts',
    type:'post',
    data:{'uid':ids,'ust':st},
    success: function(data){
      $('#msg'+ids).html('Successfully Updated!').css('color','green');
    }

  });
}

function showdealdetailstr(id)
{
  
  $('.dealdetailstr').closest('tr').addClass('str');
  $('#sho_'+id).closest('tr').removeClass('str');

}
</script>

<style type="text/css">
.deal_detail{
    float:left;
    width:100%;
    height:auto;
    background:#f1f1f1;
    padding:10px 10px;
}
.deal_left{
    float:left;
    width:25%;
}
.d_image{
    float:left;
    width:100%;
    background: #fff;
    padding:6px 6px;
}
.d_image img{
    width:100%;
}
.deal_right{
    float:left;
    width: 75%;
    padding: 0px 20px;
}
.deal_row{
    float:left;
    width:100%;
    padding:7px 0px;
}
.deal_d2
{
    float:left;
    width:50%;
    padding:0px 10px;
}
.deal_d1
{
    float:left;
    width:100%;
    padding:0px 10px;
}
.deal_text
{
    float:left;
    width:100%;
    font-weight:600;
}
.deal_text_b
{
    float:left;
    width:100%;
    font-weight:400;
    padding:8px 0;
}
.deal_text span{
    font-weight:400!important;
    margin-left:15px;
}
.approve_btn
{
    float:right;
    padding:8px 12px;
    background: green;
    color:#fff!important;
}
.issue_btn
{
    float:right;
    padding:8px 12px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.t_area{
    float:left;
    width:60%;
}
.sbt_btn{
    float:left;
    padding:12px 15px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.issuediv{
    display:none;
}

.d_image img {
    width: 200px;
    height: 252px;
    text-align: center;
}

 </style>