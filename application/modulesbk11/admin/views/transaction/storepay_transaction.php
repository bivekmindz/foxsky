<?php //pend($amountdetails);?><!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#example').DataTable();
        });
    </script>   
    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Settle Transaction</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this section, you can see the list of transactions!</p>
                                <p style="color:green;text-align:center;">
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            </div>
                        </div> 
                      
                        
                        <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl">
                                        <thead>
                                            <tr>
                                               
                                               <!--  <th bgcolor='red'>Date From & To (Settlement Cycle)</th>
                                                <th bgcolor='red'>Bill Pay Date</th> -->
                                                <th bgcolor='red'>Retailer ID</th>
                                                <th bgcolor='red'>Company Name</th>
                                                <th bgcolor='red'>Store ID</th> 
                                                <th bgcolor='red'>Store Name</th>                                               
                                                <th bgcolor='red'>Amount Receivable</th>
                                                <th bgcolor='red'>Amount Payable</th>
                                                <th bgcolor='red'>Arears</th>
                                                <th bgcolor='red'>Interest Amount</th>
                                                <th bgcolor='red'>Net Amount</th>
                                               <!--  <th bgcolor='red'>Reference ID</th>
                                                <th bgcolor='red'>Bill Generate Date</th>  -->  
                                                 <th bgcolor='red'>Adjust Balance(+)</th>
                                                <th bgcolor='red'>Action</th>                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php //p($amount);
                                            if($amount['curr_status']==1){ ?>
                                        <tr>
                                       
                                     <!--        <td><?php echo date("d-m-Y", strtotime($amount['from_date'])).' To '.date("d-m-Y", strtotime($amount['to_date'])); ?></td>
                                            <td><?php echo date("d-m-Y", strtotime($amount['pay_date'])); ?></td> -->
                                            <td><?php echo $amount['s_name']; ?></td>
                                            <td><?php echo $amount['company_name']; ?></td>
                                              <td><?php echo $amount['sid']; ?></td>
                                            <td><?php echo $amount['store_count']; ?></td>


                                             <td><?php //p($amount['cc']);

                                      $ab=explode('#', $amount['cc']);
                                      //p($ab);
                                      $rec=0;
                                      $pay=0;
                                      $rec_in=0;
                                      $pay_in=0;
                                      $str=0;
                                      $interest1='';

                                      foreach ($ab as $key => $value) {
                                          //p($value);
                                           $ab1=explode(',', $value);
                                    /*  p($ab1[0]);
                                      p($ab1[1]);
                                      p($ab1[2]);
                                      p($ab1[3]);
                                      p($ab1[4]);
                                      p($ab1[5]);
                                      p($ab1[6]);*/
                                      $tid_amt=$ab1[1];
                                     $cycle_id=$ab1[5];
                                      $mpc_interset=$ab1[7];
                                      $pay_permission=$ab1[8];

                                        $curdate=date('Y-m-d h:i:s');
                                       $pdate=$ab1[0];
                                        
                                        $date1 = new DateTime($pdate);
                                        $date2 = new DateTime($curdate);
                                        $diff = $date2->diff($date1)->format("%a");
                             //echo $diff;
                                        if($curdate<=$pdate)
                                        {
                                            $intamt='0';
                                            $interest1 =$interest1.$intamt.'#';
                                           
                                        }
                                        else
                                        {
                                            $intamt1=($ab1[1]*$mpc_interset)/100;
                                            $intamt=round((($intamt1*$diff)/365),4);
                                            $interest1 =$interest1.$intamt.'#';
                                           
                                        }



                                      if($ab1[2]=='Receivables')
                                      {
                                          /*p($ab1[0]);
                                          p($ab1[1]);
                                          p($ab1[2]);
                                          p($ab1[3]);
                                          p($ab1[4]);
                                          p($ab1[5]);
                                          p($ab1[6]);*/
                                          $newamtrec =$ab1[1];
                                          $newrec_ref =$ab1[4];
                                          $mpc_intersetr=$ab1[7];
                                          

                                $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[2];
                                $pdate=$ab1[0];
                                $date1 = new DateTime($pdate);
                                $date2 = new DateTime($curdate);
                                $diff = $date2->diff($date1)->format("%a");
                     // echo $diff;
                                if($curdate<=$pdate)
                                {
                                    $intamt='0';
                                    $rec_in=$rec_in+$intamt;
                                    $rec =$rec+$ab1[1];
                                }
                                else
                                {
                                    $intamt1=($ab1[1]*$mpc_intersetr)/100;
                                    $intamt=round((($intamt1*$diff)/365),4);

                                    //echo $intamt1;
                                    $rec_in=$rec_in+$intamt;
                                    $rec =$rec+$ab1[1];
                                }
                                      }
                                      if($ab1[2]=='Payables')
                                      {
                                          /*p($ab1[0]);
                                          p($ab1[1]);
                                          p($ab1[2]);
                                          p($ab1[3]);
                                          p($ab1[4]);
                                          p($ab1[5]);
                                          p($ab1[6]);*/
                                           $newamtpay =$ab1[1];
                                           $newpay_ref =$ab1[4];
                                           $mpc_intersetp=$ab1[7];
                                         

                                $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[2];
                                $pdate=$ab1[0];
                                $date1 = new DateTime($pdate);
                                $date2 = new DateTime($curdate);
                                $diff = $date2->diff($date1)->format("%a");
                     
                                if($curdate<=$pdate)
                                {
                                    $intamt='0';
                                    $pay_in=$pay_in+$intamt;
                                    $pay =$pay+$ab1[1];
                                }
                                else
                                {
                                    $intamt1=($ab1[1]*$mpc_intersetp)/100;
                                    $intamt=round((($intamt1*$diff)/365),4);
                                    //echo $intamt;
                                    $pay_in=$pay_in+$intamt;
                                    $pay =$pay+$ab1[1];
                                }


                                      }
                                      }  //echo $interest1.'___';
//echo $str; // store count

$m=date("m", strtotime($amount['t_createdon']));
$y=date("Y", strtotime($amount['t_createdon']));
                                      ?><!--</td>
                                       <td>--><?php if(empty($newamtpay)) $newamtpay='0'; else $newamtpay=$newamtpay;
                                       echo ' <a href="'.base_url('admin/transaction/details_pay').'/'.$amount['retailer_id'].'/'.$newrec_ref.'/Payables/'.$cycle_id.'/'.$m.'/'.$y.'/'.$amount['store_id'].'" target="_blank">+'.number_format($newamtpay,2).'</a>';
                                            //echo $pay; // Payables
                                             ?></td>
                                      <td><?php if(empty($newamtrec)) $newamtrec='0'; else $newamtrec=$newamtrec;
                                      echo ' <a href="'.base_url('admin/transaction/details_rec').'/'.$amount['retailer_id'].'/'.$newrec_ref.'/Receivables/'.$cycle_id.'/'.$m.'/'.$y.'/'.$amount['store_id'].'" target="_blank">-'.number_format($newamtrec,2).'</a>';
                                      //echo $rec;

                                         //echo $newamtrec;

                                       // Receivables?></td>
                                           
                                            <td><?php $ret=$rec+$rec_in;
                                                        $pet=$pay+$pay_in;

                                                        $apa=round($pay-$newamtpay,4);
                                                        //$are=round($rec-$newamtrec,4)-$apa;
                                                        $are=round($apa-($rec-$newamtrec),2);
                                                      
                          //echo ' <a href="'.base_url('admin/transaction/details_areas').'/'.$amount['store_id'].'/'.$are.'" target="_blank">'.$are.'</a>';
                                             echo $are; //Arears ?></td>
                     <td><?php //echo $rec_in.'__'.$pay_in;
                     //echo $int=$pay_in-$rec_in+$interest1; //interest
                     echo $int=number_format($pay_in-$rec_in,2); //interest
                    
                        ?></td>
                                            <td><?php //echo $newamtrec-$newamtpay.'___';
                                            //echo $amt=($newamtrec-$newamtpay)+$are+$int;
                                            echo number_format(($newamtpay-$newamtrec)+$are+$int,2);  
                                            $amt=round(($newamtpay-$newamtrec)+$are+$int,2); 
                                            //echo $amt=$ret-$pet; 
                                            //echo '____'.$int;
                                             //echo '</br>*_****_*'.($amt+$int); 
                                             //echo $tamt= ($amt+$int);  //net amt
                                             ?></td>
                                              <td><?php 
                                              if($remaining_balance->t_sid=='')
                                              {
                                                  $rem_bal=0;
                                                  $rem_bal_id=0;
                                              }
                                              else
                                              {
                                                  if($remaining_balance->rem_bal=='')
                                                  {
                                                      $rem_bal=0;
                                                  }
                                                  else
                                                  {
                                                      $rem_bal=round($remaining_balance->rem_bal,2);
                                                  }
                                                  $rem_bal_id=$remaining_balance->t_sid;
                                              }
                                              echo $rem_bal; ?></td>
                                         <!-- 
                                             <td><?php echo $amount->txn_id; ?></td>
                                                <td><?php echo date("d-m-Y h:i:s", strtotime($amount->t_createdon)); ?></td> -->

                                            <td style="padding: 10px 0px;width: 100%;">


                                                <?php // echo ' <a href="'.base_url('admin/tax/storelistdetails').'/'.$amount->txn_id.'/'.$amount->cycle_id.'/'.$amount->retailer_id.'" target="_blank">View</a>';
                                               // echo $amount->pay_type;

 
  $amount_f=round((str_replace('-', '', $amt))-$rem_bal,2);
                                             

if($pet > $ret)
{
    echo ' Receivables';
}
else if($pet < $ret)
{
    echo ' Payables</br>';
   // echo $amount_f;
if($pay_permission=='2')
{
    ?>
    <form action="<?php echo base_url('payment-process'); ?>" method="post" target="_blank" id="pay<?php echo $amount['t_id']; ?>">
    <input type="text" class="form-control" name="amount" id="payment" value="<?php echo $amount_f; ?>" size="10">
     <input type="hidden" name="arears" id="arears" value="<?php echo $are; ?>">
    <input type="hidden" name="all" value="<?php echo $amount['cc']; ?>">
    <input type="hidden" name="intamt" id="intamt" value="<?php echo $interest1; ?>">
    <input type="hidden" name="pay_amt" id="pay_amt" value="<?php echo $tid_amt; ?>">
    
     <input type="hidden" name="key" value="<?php echo $this->config->item('MERCHANT_KEY') ?>" />
            <input type="hidden" name="txnid" value="<?php echo  $amount['txn_id'].rand(1000,9999); ?>" />
            <input type="hidden" name="firstname" id="studentname" value="<?php echo $amount['company_name']; ?>" />

            <input type="hidden" name="email" id="email" value="<?php echo $amount['email']; ?>" />

            <input type="hidden" name="phone" class="phone" value="<?php echo $amount['contact']; ?>" />


            <input type="hidden" name="productinfo" class="coursename" value="Store Payment" />

            <input type="hidden" name="surl" value="<?php echo $this->config->item('surls') ?>"  />


            <input type="hidden" name="furl" value="<?php echo $this->config->item('furl') ?>" />

            <input type="hidden"  name="service_provider" value="payu_paisa"  />

           <!-- <input type="hidden"  name="service_provider" value=""  /> -->
            <input type="hidden" name="lastname"  value="Online" />

            <input type="hidden" name="curl" value="" />


            <input type="hidden" name="address1" value="<?php echo $amount['t_id']; ?>" />

            <input type="hidden" name="address2" value="" />


            <input type="hidden" name="city" value="" />

            <input type="hidden" name="state" value="" />


            <input type="hidden" name="country" value="" />

            <input type="hidden" name="zipcode" value="" />
            <input type="hidden" name="udf1" value="" />

            <input type="hidden" name="udf2" value="" />

            <input type="hidden" name="udf3" value="" />

            <input type="hidden" name="udf4" value="" />

            <input type="hidden" name="udf5" value="" />

            <input type="hidden" name="pg" value="" />




    <input type="submit" name="save_data" value="Pay" class="form-control1 pay" onclick="return fieldValidate('<?php echo $amount['t_id']; ?>');" id="pay_<?php echo $amount['t_id']; ?>" />
    </form>

  <!--HDFC-->



<?php 
//$url='https://www.mypopcoins.com/partner/admin/transaction/payment_response_status';
$url='https://www.mypopcoins.com/partner/admin/transaction/payment_response_status_check';
$update_status=array('tid'=>$amount['t_id'],'arrears'=>$are,'all'=>$amount['cc'],'intamt'=>$interest1,'sid'=>$ab1[6],'desc'=>$amount['company_name'].''.$amount['txn_id'],'return_url'=>$url,'name'=>$amount['company_name'],'address'=>'Delhi','city'=>'Delhi','state'=>'Delhi','pincode'=>'110096','email'=>$amount['email'],'contact'=>$amount['contact'],'total_amt'=>$tid_amt,'txn_id'=>$amount['txn_id'],'tinterest'=>str_replace('-', '', $int),'rem_bal_id'=>$rem_bal_id,'rem_bal'=>$rem_bal,'storeid'=>$amount['store_id']);
//p($update_status);
$this->session->set_userdata('updatestatus',$update_status);
     //p($this->session->userdata('updatestatus'));

} 

}
else if($ret == $pet)
{
    
     if($ret+$pet=='0')
      {  
                                               
    echo ' Settled';
  }
  else
  {
    echo ' Not Settled';
  }
   
}
?>
                                                

                                            
                                            </td>
                                      
                                           
                                        </tr>
                                        <?php  } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
            
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>


<script type="text/javascript">
function fieldValidate(ids) {
    
    var value=$('#payment').val();
    var intamt=$('#intamt').val();
    var pay_amt=$('#pay_amt').val();
    var arr=$('#arears').val();
     //var to_amt1=(parseFloat(intamt)+parseFloat(pay_amt)+parseFloat(arr)).toFixed(0);
    var to_amt11='<?php echo $amount_f; ?>';
    var to_amt1=parseFloat(to_amt11).toFixed(0);
    var to_amt=(parseInt(((to_amt1*10)/100).toFixed(0))+parseFloat(arr));


    if(value=="") {
        alert("Please input an amount!"); return false;
    } else {

        if(to_amt <= parseFloat(value))
        {
              var r = confirm("Confirmed this amount!");
              if (r == true) 
              {

                     /* $.ajax({
                url:'<?php echo base_url()?>admin/transaction/update_payment_status1',
                type:'post',
                data:{},
                success: function(data){
                 }
                });*/

                submitid="pay"+ids;
                document.getElementById(submitid).submit();
                $("#pay_"+ids).remove();
                 
              } 
              else 
              {
                 return false;
              }
        }
        else
        {
               alert("Please input greater than or equal this ("+to_amt+") amount!"); return false;
        }
       
    }
}
</script>

<script>
function submithdfc(id)
{
  submitid="hdfc"+id;
  //alert(submitid);
  document.getElementById(submitid).submit();
  $("#"+submitid).remove();

  $.ajax({
    url:'<?php echo base_url()?>admin/transaction/update_payment_status1',
    type:'post',
    data:{},
    success: function(data){
     }
    });

}
</script>

<style type="text/css">
.pay{background: #2196F3;
    padding: 5px 10px;
    margin-left: 78px;
    color: #fff;
    border-radius: 5px;
    font-weight: 700;
    border: none;
    width: 33%;
    margin-top: 5px;}

.deal_detail{
    float:left;
    width:100%;
    height:auto;
    background:#f1f1f1;
    padding:10px 10px;
}
.deal_left{
    float:left;
    width:25%;
}
.d_image{
    float:left;
    width:100%;
    background: #fff;
    padding:6px 6px;
}
.d_image img{
    width:100%;
}
.deal_right{
    float:left;
    width: 75%;
    padding: 0px 20px;
}
.deal_row{
    float:left;
    width:100%;
    padding:7px 0px;
}
.deal_d2
{
    float:left;
    width:50%;
    padding:0px 10px;
}
.deal_d1
{
    float:left;
    width:100%;
    padding:0px 10px;
}
.deal_text
{
    float:left;
    width:100%;
    font-weight:600;
}
.deal_text_b
{
    float:left;
    width:100%;
    font-weight:400;
    padding:8px 0;
}
.deal_text span{
    font-weight:400!important;
    margin-left:15px;
}
.approve_btn
{
    float:right;
    padding:8px 12px;
    background: green;
    color:#fff!important;
}
.issue_btn
{
    float:right;
    padding:8px 12px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.t_area{
    float:left;
    width:60%;
}
.sbt_btn{
    float:left;
    padding:12px 15px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.issuediv{
    display:none;
}

.d_image img {
    width: 200px;
    height: 252px;
    text-align: center;
}

 </style>
