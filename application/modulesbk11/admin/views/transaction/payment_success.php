<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" /> 
   <link href="<?php echo base_url() ?>assets/css/payment.css" rel="stylesheet" /> 
   
   

   
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    
    <title></title>
    

</head>
   
    <div class="wrapper mailer_des" >
    <?php  //$this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                 <div class="col-md-10 col-md-offset-1">
<div class="mailer_all">
<div class="right_red">

<div class="retailer pad_top">
<p><span><img src="<?php echo base_url()?>assets/img/mail-1.jpg"></span>Retailer/ Store Name :</p>

<p class="font_m"> <?php if(isset($rs_name)) echo $rs_name;?> </p>

<div class="line_bo"></div>
</div>


<div class="retailer">
<p><span><img src="<?php echo base_url()?>assets/img/mail-2.jpg"></span>Retailer /Store ID :</p>

<p class="font_m"><?php if($usertype==2){ echo $rs_id; } else { echo $rs_id; }?></p>

<div class="line_bo"></div>
</div>


<div class="retailer">
<p><span><img src="<?php echo base_url()?>assets/img/mail-3.jpg"></span>Settlement Cycle:</p>

<p class="font_m"><span><?php if(isset($from_date)) echo $from_date;?></span> To <span><?php if(isset($to_date)) echo $to_date;?></span></p>

<div class="line_bo"></div>
</div>


<div class="retailer">
<p><span><img src="<?php echo base_url()?>assets/img/mail-4.jpg"></span>Amount :</p>

<p class="font_m"><?php if(isset($pay_amt)) echo $pay_amt;?> INR</p>

<div class="line_bo"></div>
</div>


<div class="retailer pad_bot">
<p><span><img src="<?php echo base_url()?>assets/img/mail-5.jpg"></span>Transaction ID :</p>

<p class="font_m"><?php if(isset($transaction_id)) echo $transaction_id;?></p>


</div>


</div>

<div class="logo_sect">
<div class="top_logo">
<div class="pad_lef">
<div class="lgo"><img src="<?php echo base_url()?>assets/img/footer-logo.png"></div>
<div class="text_co"><?php echo date('d-M-Y');?></div>

<div class="line_br"></div>

</div>

</div>

<div class="mid_con">
<p>Thank you <span><img src="<?php echo base_url()?>assets/img/smile.jpg"></span> !!  <br>
We value  your patronage.</p>
</div>

<div class="your">
<p style=" color: #000;padding: 9px 0;">Paid Amount <?php if(isset($pay_amt)) echo $pay_amt;?> INR and Transaction ID <?php if(isset($transaction_id)) echo $transaction_id;?>.</p>
<p>Your Settlement note is available on your Dashboard .</p>
</div>
<div class="bottom">
<p>For any Clarification,kindly contact us at <span>support@mindzshop.com</span></p>

</div>
<div class="lie"></div>

<div class="my_pop">
<span>www.mypopcoins.com</span>
<span style="margin-left: 229px;">T&C Apply</span>

</div>
</div>




</div>

</div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>

 