 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
  </style>

 
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Update Weight</h2>
    </div>
        <div class="page_box">
        
        <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
       
        <form id="addCont" action="" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Title</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input  type="text" name="title"  id="title" value="<?php echo $vieww->title ?>" required /></div>
            </div>
        </div>
        </div>
        </div>
          <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Unit</div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input"><input id="unit" type="text" name="unit" value="<?php echo $vieww->unit ?>"/></div>
                </div>
            </div>
        </div>
        </div>
         <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Value</div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input"><input id="value" type="text" name="value" value="<?php echo $vieww->value ?>"/></div>
                </div>
            </div>
        </div>
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
   