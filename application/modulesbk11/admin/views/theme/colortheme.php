<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

<!-- jQuery Form Validation code -->
<style>
  input.error{border:1px solid red;}
  label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>

<script language='javascript'>

// When the browser is ready...
$(document).ready(function() {

// Setup form validation on the #register-form element
$("#addCont").validate({
// Specify the validation rules
rules: {
  pick_theme_color : "required",
},
// Specify the validation error messages
messages: {
  pick_theme_color : "Attribute name is required",
},
submitHandler: function(form) {
  form.submit();
}
});

});

</script>

<div class="wrapper">
  <?php $this->load->view('helper/nav')?> 
  <div class="col-lg-10 col-lg-push-2">
    <div class="row">
      <div class="page_contant">
        <div class="col-lg-12">
          <div class="page_name">
            <script src="<?php echo admin_url();?>js/jscolor.js"></script>
            <h2>Add Attribute Map</h2>
          </div>
          <div class="page_box">
            <div class="sep_box">
              <div class="col-lg-12">
                <div class='flashmsg'>
                  <?php echo validation_errors(); ?>
                  <?php
                  if($this->session->flashdata('message')){
                    echo $this->session->flashdata('message'); 
                  }
                  ?>
                </div>
              </div>
            </div>
            
            <form id="addCont" action="" method="post" enctype="multipart/form-data" >
              <div class="sep_box">
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-3" >
                      <div class="tbl_text">Choose Theme Color <span style="color:red;font-weight: bold;">*</span></div>
                    </div>
                    <div class="col-lg-7">
                      <div class="tbl_input">
                        <input type="text" class="jscolor" name="pick_theme_color"  id="pick_theme_color" onClick="colorpik()" /></div>
                      </div>
                    </div><!-- color div -->
                  </div>
                  <div class="col-lg-6">
                    <div class="row">
                      <div class="col-lg-3" id="pik_theme_color">
                      </div>
                    </div>  
                  </div>
                </div>
                
                <div class="sep_box">
                  <div class="col-lg-6">
                    <div class="row">
                      <div class="col-lg-4"></div>
                      <div class="col-lg-8">
                        <div class="submit_tbl">
                          <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
  function set_pick_theme(){
    var pick_theme_color=$("#pick_theme_color").val();
    $("#pik_theme_color").css("background-color","#"+pick_theme_color);
    $("#pik_theme_color").css("padding","20px");
  }
  set_pick_theme();
</script>