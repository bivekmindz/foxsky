<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Admin Update Password</h2>
    </div>
        <div class="page_box">
         <div class="passmessg"> <?php echo $this->session->flashdata('message');  ?> </div>
                    <?php echo form_open('admin/login/adminupdatepassword',array('id' => 'batchadd'));?>
                       
                        <span id="passwd_error"></span>
                  <div class="sep_box">
                  <div class="col-lg-6">
                  <div class="row">
                  <div class="col-lg-4">
                  <div class="tbl_text">Old Password</div>
                  </div>
                  <div class="col-lg-8">
                  <div class="tbl_input"> <input type="password" name="old_password" id="old_password" onblur="return checkpasswordexist()"; >
                            <span id="oldpassword" ></span>  </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  
                   <div class="sep_box">
                  <div class="col-lg-6">
                  <div class="row">
                  <div class="col-lg-4">
                  <div class="tbl_text">New Password</div>
                  </div>
                  <div class="col-lg-8">
                  <div class="tbl_input"> <input type="hidden" name="respassword" id="respassword" value="1" >
                            <input type="password" name="new_password" id="new_password" onblur="return minpassvalue();"><span class="passerr" id="err1"></span></div>
                  </div>
                  </div>
                  </div>
                  </div>
                  
                   <div class="sep_box">
                  <div class="col-lg-6">
                  <div class="row">
                  <div class="col-lg-4">
                  <div class="tbl_text">Confirm Password</div>
                  </div>
                  <div class="col-lg-8">
                  <div class="tbl_input"><input type="password" name="confirm_password" id="confirm_password"  ></div>
                  </div>
                  </div>
                  </div>
                  </div>
                  
                   <div class="sep_box">
                  <div class="col-lg-6">
                  <div class="row">
                  <div class="col-lg-4">
                  <div class="tbl_text"></div>
                  </div>
                  <div class="col-lg-8">
                  <div class="submit_tbl"> <input class="btn_button sub_btn submitpassBtn" type="submit" name="submit" value="Save Changes"  onclick="return matchpassword();"></div>
                  </div>
                  </div>
                  </div>
                  </div>
                   
                        
                    </form>
               </div>
               </div></section>
              


<script>
  
function minpassvalue(){
 
  var password    = $("#new_password").val();
  if(password.length < 6) {
          $("#new_password").css('border','1px solid red');
          $("#err1").html("Password Atleast Six Characters");
          return false;
      }
      else {
        $("#new_password").css('border','1px solid green');
        $("#err1").html("");
      }

}
  function matchpassword()
  {
        
      var oldpassword = $("#old_password").val();
      var password    = $("#new_password").val();
      var conformpassword = $("#confirm_password").val();
     
      if(oldpassword=='')
      {
         
        $("#old_password").css('border','1px solid red');
        return false;
      }else if(password=='')
      {
        $("#new_password").css('border','1px solid red');
        return false;
      }else if(conformpassword=='')
      {
         
        $("#confirm_password").css('border','1px solid red');
        return false;
      }else if(password!=conformpassword)
      {
        $("#new_password").css('border','1px solid red');
        $("#confirm_password").css('border','1px solid red');
        return false;
      }

      if(password.length < 6) {
          $("#new_password").css('border','1px solid red');
          $("#err1").html("Password Atleast Six Characters");
          return false;
      }
      else {
        $("#new_password").css('border','1px solid green');
        $("#err1").html("");
      }

  }


function checkpasswordexist()
      {
        var baseurl = "<?php echo  base_url(); ?>";
        var oldpasswods =  $("#old_password").val();
         
        if(oldpasswods !=""){
        $.ajax({
         url:baseurl+'admin/login/adminUniquepassword',
         type:'POST',
         dataType:'JSON',
         data:{'oldpasswod':oldpasswods},
         success: function(data){
           //alert(data); return false;
            if(data.adminemailcount==0){
                $("#old_password").css('border','1px solid red');
                $('.submitpassBtn').prop('disabled', true);
                return false;
            } else {
                $("#old_password").css('border','1px solid green');
                $('.submitpassBtn').prop('disabled', false); 
            }
         }
       });
      } 
    
    }


</script>