<!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>

    <div class="wrapper">
    <?php  $this->load->view('helper/nav'); ?>
        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Transaction Cycle Details</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p>In this section, you can see the list of transaction cycle details!</p>
                                <p style="color:red;text-align:center;">
                                    <?php echo validation_errors(); ?> 
                                   
                                </p>
                                <p style="color:green;text-align:center;">                                   
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            </div>
                        </div>
                          <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <form action="" method="post">
                <div class="col-lg-2"><select name="payname" class="form-control"><option value="">Select Day</option>
                    <option value="monday">Monday</option><option value="tuesday">Tuesday</option><option value="wednesday">Wednesday</option><option value="thursday">Thursday</option><option value="friday">Friday</option><option value="saturday">Saturday</option><option value="sunday">Sunday</option></select>
                    </div>
                    <div class="col-lg-2"><input type="number" name="todate" placeholder="To Day 7" min="1" max="50" class="form-control"></div>
                  
                 <div class="col-lg-3"><input type="number" name="paydate" placeholder="Payment Pay Day 7" min="0" max="30" class="form-control"></div>
                  
                <div class="col-lg-2"><input type="submit" name="paysubmit" class="form-control btn-primary"></div>
            </form>
                                </div></div></div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>                                               
                                                <th bgcolor='red'>Payment Cycle Day</th>
                                                <th bgcolor='red'>To Diff Days</th>
                                                <th bgcolor='red'>Payment Pay Days</th>
                                                <th bgcolor='red'>Status</th>
                                                <th bgcolor='red'>Create Date</th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($vieww as $value) { ?>
                                        <tr>                                       
                                            <td><?php echo ucfirst($value->day_name); ?></td>
                                            <td><?php echo $value->diff_days_to; ?></td>
                                            <td><?php echo $value->pay_day; ?></td>
                                            <td><?php echo $value->cycle_status; ?></td> 
                                            <td><?php echo $value->cycle_createdon; ?></td> 
                                                                                     
                                        </tr>
                                    <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>  
</html>

