<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

<!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            banimg      : "required",
        },
        // Specify the validation error messages
        messages: {
            banimg  : "Banner Image is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>
<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Home Right Banners</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can add, edit and delete Right banners.</p>
                        </div>
                    </div>
                    <form method="post" id="addCont" action="" enctype="multipart/form-data">
                    <div class="page_box">
                    <div class='flashmsg'><?php echo $this->session->flashdata('messag'); ?></div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Banner Image <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="banimg" name="banimg" type="file" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Image Url </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="banurl" name="banurl" type="url" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    </form>



                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S:NO</th>
                                            <th>Slider Image</th>
                                            <th>Image Url</th>
                                            <th>Updated by</th>
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   <?php $i=0; foreach ($viewban as $key => $value) { $i++; ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><img height="60px" width="60px" src="<?php echo base_url();?>assets/webapp/images/<?php echo $value->t_imgname;?>"></td>
                                            <td><?php echo $value->t_path ;?></td>
                                            <td><?php if(empty($value->h_modifiedby)){ echo $value->u_createdby; } else { echo $value->u_modifiedby; } ?></td> 
                                            <td><?php if(empty($value->h_modifiedby)){ echo $value->h_createdon; } else { echo $value->h_modifiedon; } ?></td>
                                            <td><a href="<?php echo base_url()?>admin/bannergallery/updaterightbanner/<?php echo $value->n_imgid?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/bannergallery/deleterightban/<?php echo $value->n_imgid?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/bannergallery/statusrightban/<?php echo $value->id.'/'.$value->status; ?>"><?php if($value->status=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                    <?php } ?> 
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


