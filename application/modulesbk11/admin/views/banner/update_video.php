<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

<!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
    .submit_tbll { width: 100%; float: left; text-align: left; }
    .submit_tbll input { border: none;padding: 9px 14px;  }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            h_video  : "required",
        },
        // Specify the validation error messages
        messages: {
            h_video  : "Video file is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Update Video</h2>
   
    </div>
        <div class="page_box">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/bannergallery/view_video_list"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        <div style="text-align: center;color: red;"><?php echo $this->session->flashdata('er_msg'); ?></div>
        <form method="post" id="addCont" enctype="multipart/form-data">
                    
        <div class="sep_box">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="tbl_text">Update Video File</div>
                    </div>
                    <div class="col-lg-8">
                        <div class="tbl_input"><input id="h_video" name="h_video" type="file" /></div>
                    </div>
                </div>
            </div>
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-12">
                <div class="submit_tbll">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        <div class="sep_box">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                    <span style="font-weight: bold;">Existing Video File : </span><?php echo $view_svideo->v_filename; ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
   