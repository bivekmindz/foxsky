<?php 
$check_permission=checkpermission();
if($check_permission==2){
 redirect(base_url().'permission_error');
} 
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <!-- <link rel='shortcut icon' href="<?php echo base_url();?>assets/fevicon.png"/> -->
    <link href="<?php echo admin_url();?>bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo admin_url();?>fontawsome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo admin_url();?>css/StyleSheet.css" rel="stylesheet" />
    <script type="text/javascript">
       var site_url="<?php echo base_url(); ?>";
    </script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="<?php echo admin_url();?>js/JavaScript.js"></script>
    <!-- for select2 js -->
     <link href="<?php echo base_url(); ?>assets/js/select/dist/css/select2.min.css" rel="stylesheet" />
     <script src="<?php echo base_url(); ?>assets/js/select/dist/js/select2.min.js"></script>
    <!-- for select2 js -->
<!-- datepicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <!--  <link rel="stylesheet" href="/resources/demos/style.css"> -->
    <!-- datepicker -->
    <script type="text/javascript">
    $(document).ready(function () {
			 $(".menu-sm").click(function(){
		  $(".left_nav").toggleClass("active_mm");
        });
		 }); 
    </script>
    
</head>
<body>
    <div class="wrapper">
        <div class="col-lg-12">
            <div class="row">
                <div class="top_header">
                    <div class="col-lg-4">
                     <span class="menu-sm hidden-lg"> <i class="fa fa-align-justify menu-sm-b"></i></span>
                        <?php if($this->session->userdata('bizzadmin')->EmployeeId==9988){ ?>
                          <a href="<?php echo  base_url();?>admin/login/dashboard" class="logo_admin">
                              <img src="<?php echo admin_url();?>images/logo3.png" />
                          </a>
                        <?php } else { ?>
                          <a href="<?php echo  base_url();?>admin/login/emp_dashboard" class="logo_admin">
                              <img src="<?php echo admin_url();?>images/logo3.png" />
                          </a>
                        <?php } ?>
                    </div>
                    <div class="col-lg-8">
                  
                     <a id="logout_btn" class="user" href="<?php echo base_url();?>admin/login/logout" style="color: #444;font-size: 13px;padding: 14px 0px;float: right;text-decoration:none;margin-left:30px;">
                  <?php if(isset($this->session->userdata('admin')->UserName)){ echo 'Logout';}?>
                  <i class="fa fa-user" style="margin-right:5px;"></i>Logout</a>
                        <a href="<?php echo  base_url();?>admin/login/adminupdatepassword" style="color: #444;font-size: 13px;padding: 14px 0px;float: right;text-decoration:none;"><i class="fa fa-key"></i> Change Password</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script> 

    
