<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2"> <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Brand</h2>
   
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p>In this Section Admin can download and upload brand wise excel and upload product image zip folder.</p>
                        </div>
                    </div>
        <div class="page_box">
        <div class="sep_box">
        <div class="col-lg-12">
      
         <div class='flashmsg'>
            <?php
              if($excelErr!=""){
                echo $excelErr; 
              }
            ?>
        </div>
        </div>
        </div>
      
        <div class="sep_box" style="border-bottom:solid 1px #F1EFEF;">
        <div class="col-lg-6">
        <div class="row">
          <center><span style='color:red; padding:10px; '>
            <?php  
              // Get Flash data on view 
              print $this->session->flashdata('message_name');
            ?></span>
          </center>
        <div class="col-lg-4">
                <div class="tbl_text">Upload Image Zip Folder: <span style="color:red;font-weight: bold;">*</span> <span id="imgzipErr" style='color:red;' onclick='$(this).hide();'></span></div>
            </div>
              <div class="col-lg-8">
                <div class="tbl_input">
                 <form name="fileupload" action="" method="post" enctype="multipart/form-data" onSubmit="return hello(99999991);">
        <label>
          <input type="file" name="my_file" id="99999991">
        </label>
       
        <div class="submit_tbl">
                 <input type="submit" name="submitimg2" class="pop_sub btn_button sub_btn" style="float:left;" />
                </div>
        
        </form></div></div>
        </div>
        </div>
     
        </div>
        <form action="" method="post" enctype="multipart/form-data" >
        <!-- <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Brand Name <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select name="brandid" id="brandid">
                <option>Select Brand</option>
                <?php foreach ($vieww as $key => $value) {
                ?>
                 <option value="<?php echo $value->id ?>"><?php echo $value->brandname ?></option>
               <?php } ?>
               </select>
                </div>
            </div>
        </div>
        </div>
        </div> -->
        <div class="sep_box">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Main Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                    <select name="mcatidd" id="mcatidd" onclick="childcategory();" >
                <option value="">Select Category</option>
                <?php foreach ($parentCatdata as $key => $value) {
                 ?>
                  <option value="<?php echo $value->catid ?>"><?php echo $value->catname ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>
        </div>
        <div class="sep_box" style="display:none" id="childcat">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="catidd" id="catidd" onclick="subchildcategory();"  >
                <option value="">Select Category</option></select></div>
            </div>
        </div>
        </div>
        </div>
        <div class="sep_box" style="display:none" id="subchildcat">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Child Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="subcatidd" id="subcatidd">
                <option value="">Select Category</option></select></div>
            </div>
        </div>
        </div>
        </div>
        <div class="sep_box">
          <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text"></div>
            </div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                <input type="submit" name="download" value="Download excel" class="btn_button sub_btn" >
                </div>
            </div>
        </div>
        </div>
        </div>
 
        <div class="sep_box">
        <div class="col-lg-12">
        <h3>Choose Brand and Upload File</h3></div>
         <?php  
      $errMsgg = "";
      foreach ($upload as $key => $value) {
        //p($value);
        $errMsgg .= '<li style="list-style-type:bullet; line-height:25px;">'.$value->Description.'<br>';
      }

      if(!empty($errMsgg)){
        print '<b style="color:red;">Uploading Error: Product already exists:</b><br>';
        print '<p style="border-bottom:1px solid silver; padding-bottom:10px; margin-bottom:20px;">'.$errMsgg.'</p><br>';
      }

    ?>
    
         
  <div class="sep_box">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Main Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                    <select name="mcatiddup" id="mcatiddup" onclick="childcategoryupload();" >
                <option value="">Select Category</option>
                <?php foreach ($parentCatdata as $key => $value) {
                 ?>
                  <option value="<?php echo $value->catid ?>"><?php echo $value->catname ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>
        </div>
        <div class="sep_box" style="display:none" id="childcatup">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="catiddup" id="catiddup" onclick="subchildcategoryupload();"  >
                <option value="">Select Category</option></select></div>
            </div>
        </div>
        </div>
        </div>
        <div class="sep_box" style="display:none" id="subchildcatup">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Child Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="subcatiddup" id="subcatiddup">
                <option value="">Select Category</option></select></div>
            </div>
        </div>
        </div>
        </div>

     
        
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
        <div class="col-lg-4">Select file <span style="color:red;font-weight: bold;">*</span></div>
        <div class="col-lg-8"><input type="file" name="userfile" id="userfile" /></div>
        </div>
        </div>
        
        </div>
          <div class="sep_box">
          <div class="col-lg-6">
          <div class="col-lg-4"></div>
        <div class="col-lg-8">
        <div class="submit_tbl">
        <input type="submit" name="submit" class="btn_button sub_btn"/></div></div></div>
          </div>
            </form>      
                </div>
            </div>
        </div>
    </div>
    </div>

<script type="text/javascript">
//  ---  Upload Image Zip Folder --- //

function hello(fupid){
    var getId = fupid;
    $('#imgzipErr').html('<i class="fa fa-spinner fa-spin"></i>');

    if($("#99999991").val() == ""){
      $("#imgzipErr").show().html('Please select a zip file to upload.')
      return false;
    }
    var data1 = new FormData();
    //alert(data1);
    $.each($('#'+getId)[0].files, function(i, file) {
      data1.append('my_file', file);
    });
    $.ajax({
      url           : "<?php echo base_url() ?>admin/product/img_resixe",
      type          : "POST",
      data          : data1,
      fileElementId : 'my_file',
      enctype       : 'multipart/form-data',
      processData   : false,  // tell jQuery not to process the data
      contentType   : false   // tell jQuery not to set contentType
    }).done(function(data) {
      // console.log(data);        
      // alert(data);
      $("#imgzipErr").html('');
      $("#imgzipErr").show().html(data);
      // alert('Image Succesfully Upload');
    });
    return false;
}


function childcategory(){
    var catid=$('#mcatidd').val();
    if(mcatidd==0){
        $('#childcat').hide();
    }else{
        $('#childcat').show();
    }
     
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/catvalue',
        type: 'POST',
        //dataType: 'json',
        data: {'catid': catid},
        success: function(data){
             
            var  option_brand = '<option value="">Select Category</option>';
            $('#catidd').empty();
            $("#catidd").append(option_brand+data);
        }
    });
    
   }

   function subchildcategory(){
        var catid=$('#catidd').val();
            //alert(catid);

            if(mcatidd==0){
                $('#subchildcat').hide();
            }else{
                $('#subchildcat').show();
            }
             
            $.ajax({
                url: '<?php echo base_url()?>admin/attribute/subbranddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                      //alert(data);
                      var  option_brand = '<option value="">Select Category</option>';
                      $('#subcatidd').empty();
                      $("#subcatidd").append(option_brand+data);  
                     
                }
            });
    }


//uploading product filter

function childcategoryupload(){
    var catid=$('#mcatiddup').val();
    if(mcatiddup==0){
        $('#childcatup').hide();
    }else{
        $('#childcatup').show();
    }
     
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/catvalue',
        type: 'POST',
        //dataType: 'json',
        data: {'catid': catid},
        success: function(data){
             
            var  option_brand = '<option value="">Select Category</option>';
            $('#catiddup').empty();
            $("#catiddup").append(option_brand+data);
        }
    });
    
   }

   function subchildcategoryupload(){
        var catid=$('#catiddup').val();
            //alert(catid);

            if(mcatiddup==0){
                $('#subchildcatup').hide();
            }else{
                $('#subchildcatup').show();
            }
             
            $.ajax({
                url: '<?php echo base_url()?>admin/attribute/subbranddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                      //alert(data);
                      var  option_brand = '<option value="">Select Category</option>';
                      $('#subcatiddup').empty();
                      $("#subcatiddup").append(option_brand+data);  
                     
                }
            });
    }

</script>