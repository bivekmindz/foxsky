 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
	.mt-10{ margin-top:10px;}
	span.full-width{width: 100%;text-align: center;float: left;}
	span.full-width a{ display: inline-block; }
</style>
<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>UnApprove Product</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view the unapprove product list and approve them also.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box1">
<!--############################# search section start #####################################################-->
                            <!-- <form method="get" action='<?php echo base_url('admin/product/unproduct'); ?>' id='searchfrm'>
                            <div class="col-lg-3">
                            <div class="tbl_input">
                            <select id="filter_by" name="filter_by">
                            <option value="">Filter BY</option>
                            <option value="sku_no" <?php echo ($_GET['filter_by']=='sku_no')?'selected':'';?>>SKU Number</option>
                            <option value="product_name" <?php echo ($_GET['filter_by']=='product_name')?'selected':'';?>>Product Name</option>
                            <option value="brand_name" <?php echo ($_GET['filter_by']=='brand_name')?'selected':'';?>>Brand Name</option>
                            <option value="categ_name"<?php echo ($_GET['filter_by']=='categ_name')?'selected':'';?>>Category Name</option>
                            <option value="manufac_name"<?php echo ($_GET['filter_by']=='manufac_name')?'selected':'';?>>User Name</option>
                            </select>
                            </div>
                            </div>
                            <div class="col-lg-3" id="input_div">
                            <div class="tbl_input">
                            <input type="text" name="searchname" value="" id="searchname" class="valid input_cls">
                            </div>
                            </div>
                            <div class="col-lg-3">
                            <div class="submit_tbl">
                            <input id="validate_frm" type="submit" value="Search" class="btn_button sub_btn">
                            <input type="button" onclick='location.href="<?php echo base_url('admin/product/unproduct'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                            </div>
                            </div>
                            </form> -->
  <!---############################# search section end #####################################################-->
                            <div class="col-lg-12">
                             <!--    <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/feature/addcatfeature"><button>ADD Feature Master</button></a>
                                </div> -->
                                <form method="post" action="">
                                  <input class="xls_download" type="submit" value="Download Excel" name="newsexcel">
                                </form>
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                 <form method="post" action="">
                                <input type="submit" class="hbutton trans mrgnt5 xls_download" name="submit" value="Approve Product" >
                                <!-- <input type="submit"  name="submitdelete" value="Delete" style="background: #109a55!important;"> --> 
                                <!-- <input type="submit"  name="submitstatus" value="Status"> -->
                                <table class="grid_tbl" id="search_filter">
                                    <thead>

                                        <tr>
                                          <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span>
                                              <!-- / <br>
                                           <span style='border:0px color:blue; cursor:pointer;' id='DeselAll'>Deselect</span> --></th>
                                            <th>S:NO</th>
                                            <th>Product Image</th>
                                           <!--  <th>User Name</th> -->
                                            <th>Sku Number</th>
                                            <th>Product Name</th>
                                            <th>Category</th>
                                            <!-- <th>Brand</th> -->
                                            <th>Qty</th>
                                            <th>Mrp</th>
                                            <th>Selling Price</th>
                                           <!--  <th>Store Name</th> -->
                                            <th>Created On</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                    if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                       foreach ($vieww as $key => $value) { 
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><input type='checkbox' name='proApp[]' class='chkApp' value='<?=$value->skuno?>'> </td> 
                                            <td><?php echo $i ;?></td>
                                            <td><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value->venproimgpath; ?>" height="75" width="50"></td>
                                          <!--   <td><?php echo $value->firstname ;?></td> -->
                                            <td><?php echo $value->skuno ;?></td>
                                            <td><?php echo $value->productname ;?></td>
                                            <td><?php echo $value->catname ;?></td>
                                            <!-- <td><?php echo $value->brandname ;?></td> -->
                                            <td><?php echo $value->ProductQty ;?></td>
                                            <td><?php echo $value->prodmrp ;?></td>
                                            <td><?php echo $value->prodsellingprice ;?></td>
                                           <!--  <td><?php echo $value->st_store_name ;?></td> -->
                                             <td><?php echo $value->createdOn ;?></td>
                                            <td class="text-center"> <!-- <span class="full-width"><a href="<?php echo base_url()?>admin/product/productdelete/<?php echo $value->skuno?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a></span> -->
                                            <button type="button" class="mt-10" data-toggle="modal" onclick="reject_status(<?php echo $value->menuproductid; ?>);" data-target="#myModal">REJECT</button>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


<!-- add comment section start -->
<div id="myModal" class="modal fade" role="dialog">

    <div class="add_coment_canc">
        <h3>ADD Comment</h3>
        <form>
            <input type="hidden" id="reject_proid">
            <textarea id="reject_proreason"  placeholder="Add Comment Here..." required></textarea>
            <a href="javascript:void(0);" class="comen_su" onclick="reject_pro();">Submit</a>
            <a href="javascript:void(0);" class="comen_ca" onclick="window.open('<?php echo base_url(uri_string()); ?>','_self')">Cancel</a>
        </form>

    </div>
    <div class="overlay-popup"></div>
</div>
<!-- add comment section end -->


 <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});


        $(document).ready(function(){
            $(".status_btn").click(function(){
                $("#unapp-pop").addClass("popup-show");
                $("#unapp-pop").removeClass("popup-show");

            });         
        });

function reject_status(r_id){
    $("#reject_proid").val(r_id);
}

function reject_pro(){
    var reject_proid = $("#reject_proid").val();
    var reject_proreason = $("#reject_proreason").val();
    if(reject_proreason!='' && reject_proid!=''){
        $.ajax({
            url:'<?php echo base_url();?>admin/product/reject_product',
            type:'post',
            data:{'reject_proid':reject_proid,'reject_proreason':reject_proreason},
            success:function(rep)
            {   
                //alert(rep);
                //console.log(rep);
                //return false;
                document.location.href=window.location.href;
            }
        });
    } else{
        alert("Comment is required.");    
        return false;    
    }
}

</script>