
<script type="text/javascript">
$(document).ready(function(){
var maxField = 1000; 
var addButton = $('.add_row'); 
var x = 1; 
var wrapper = $('.append'); 
/************************************************* Add More and remove for URL and Image **************************************************************/
$(addButton).click(function(){
   
    if(x < maxField){
        x++;
        $(".append_wrapper").clone().removeClass("append_wrapper").appendTo(".append");
    }
});
$(wrapper).on('click', '.remove_row', function(e){ //Once remove button is clicked
    if(x>1) {
    e.preventDefault();
    $(this).parent('div').parent('div').remove(); //Remove field html
    x--; //Decrement field counter
}
});
});
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?>
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Add Product</h2>
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add Single Product with details.</p>
                        </div>
                    </div>
          <div class="page_box">
  <form action="" method="post" enctype="multipart/form-data" id="add_prod">
  <div class="form">
  <span id="message"></span>

  <div class="sep_box">

     <!--  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Brand Name <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><select name="brandid" id="brandid" onchange="selectcat();" class="need">
                <option value="">Select Brand</option>
                <?php foreach ($vieww as $key => $value) { ?>
                 <option value="<?php echo $value->id; ?>"><?php echo $value->brandname; ?></option>
               <?php }?>
                
              </select>
             </span></div>
            </div>
        </div>
      </div> -->

      <!-- <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Main Category <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
             <span>
             <select name="catid" id="catid" onchange="selectfeature();selectatt(this.value);" class="need">
                
                <option value="">Select Main Category</option>
                <?php     foreach ($maincat as $key => $value) { ?>
                  <option value="<?php echo $value->catid;  ?>">
                  <?php echo $value->catname;  ?></option>
               <?php } ?>
              </select></div>
            </div>
        </div>
      </div> -->
      </div>
                 
      <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Category <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
             <span>
             <!-- onchange="selectatt(this.value);selectfeature();selectattribute();"  -->
             <select name="catid" id="catid" class="need">
                <option value="">Select Main Category</option>
                <?php     foreach ($maincat as $key => $value) { ?>
                  <option value="<?php echo $value->catid;  ?>">
                  <?php echo $value->catname;  ?></option>
               <?php } ?>
                
              </select></div>
            </div>
        </div>
      </div>
   
       </div>


                 <div class="sep_box" style="display: none;" id="attrnew">
                  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Attribute
         </div></div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><!-- <select name="agename" id="agename" class="need"> -->
                <select name="attname" id="attname">
                <option value="">Select Attribute</option>
                </select></span></div>
            </div></div>
        </div>
    <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Color</div></div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><!-- <select name="colorname" id="colorname" class="need"> -->
                <select name="colorname" id="colorname">
                <option value="">Select Color</option>
                </select>
                </span></div>
            </div></div>
          </div>
       </div>
       
                 <div class="sep_box" style="display: none;" id="attr">
                  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Age
         </div></div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><!-- <select name="agename" id="agename" class="need"> -->
                <select name="agename" id="agename">
                <option value="">Select Age</option>
                </select></span></div>
            </div></div>
        </div>
    <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Color</div></div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><!-- <select name="colorname" id="colorname" class="need"> -->
                <select name="colorname" id="colorname">
                <option value="">Select Color</option>
                </select>
                </span></div>
            </div></div>
          </div>
       </div>

        <div class="sep_box hide" id="attrbute"></div>
        <div id="fee"></div> 


  <div class="sep_box">

      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Product Name <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="pname"  id="pname" class="needed"></span></div>
            </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Article Number <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
             <span><input type="text" name="anumber"  id="anumber" class="needed"></span></div>
            </div>
        </div>
      </div>
      </div>
      <div class="sep_box">

        <div class="col-lg-6">

          <div class="row">
            <div class="col-lg-4">
              <div class="tbl_text">Product SKU <span style="color:red;font-weight: bold;">*</span>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="tbl_input">
               <span><input type="text" name="psku"  id="psku" class="needed"></span></div>
             </div>
           </div>
         </div>
        <div class="col-lg-6">

          <div class="row">
            <div class="col-lg-4">
              <div class="tbl_text">Barcode Number
              </div>
            </div>
            <div class="col-lg-8">
              <div class="tbl_input">
               <span><input type="text" name="barcode"  id="barcode"></span></div>
             </div>
           </div>
         </div>
        </div>

        <div class="sep_box">
          
            <div class="col-lg-6">
              <div class="row">
                <div class="col-lg-4">
                  <div class="tbl_text">Product Description
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="tbl_input">
                    <span><textarea name="pdes" id="pdes"></textarea></span></div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-4">
                <div class="tbl_text">Stock Qty <span style="color:red;font-weight: bold;">*</span>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="tbl_input">
                  <span><input type="number" name="stock"  id="stock" min="1"></span></div>
                </div>
              </div>
            </div>
            </div>

            <div class="sep_box">
              <div class="col-lg-6">
                <div class="row">
                  <div class="col-lg-4">
                    <div class="tbl_text">Purchase Price <span style="color:red;font-weight: bold;">*</span>
                    </div>
                  </div>
                  <div class="col-lg-8">
                    <div class="tbl_input">
                      <span><input type="number" name="pprice"  id="pprice" class="need" onblur="profit_margin();" min="1"></span></div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="tbl_text">Product MRP <span style="color:red;font-weight: bold;">*</span>
                      </div>
                    </div>
                    <div class="col-lg-8">
                      <div class="tbl_input">
                        <span><input type="number" name="pmrp"  id="pmrp" class="need" onblur="profit_margin();" min="1"></span></div>
                      </div>
                    </div>
                  </div>
                </div>

      <div class="sep_box">
       <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Selling Price <span style="color:red;font-weight: bold;">*</span>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="number" name="sprice"  id="sprice" class="need" onblur="profit_margin();" min="1"></span></div>
            </div>
          </div>
        </div>
       <!--  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Citygroup1 % <span style="color:red;font-weight: bold;">*</span>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="number" name="city1"  id="city1" class="need" min="1"></span></div>
            </div>
          </div>
        </div> -->
      </div>

     <!--  <div class="sep_box">
       <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Citygroup2 % <span style="color:red;font-weight: bold;">*</span>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="number" name="city2"  id="city2" class="need" min="1"></span></div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Citygroup3 % <span style="color:red;font-weight: bold;">*</span>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="number" name="city3"  id="city3" class="need" min="1"></span></div>
            </div>
          </div>
        </div>
      </div> -->
      
 <!--      <div class="sep_box"> -->

     
 <!--  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Profit Margin %
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="pmargin"  id="pmargin" readonly></span></div>
            </div>
            </div>
        </div> -->
       <!--  <div class="col-lg-6">
        <div class="row"><div class="col-lg-4">
            <div class="tbl_text">Discount %</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="text" name="discount"  id="discount" readonly ></span></div></div></div>
        </div>

        </div> -->
       <!--  <div class="sep_box">
          <div class="col-lg-3">
            <div class="row"><div class="col-lg-4">
              <div class="tbl_text">Range1 <span style="color:red;font-weight: bold;">*</span></div></div>
              <div class="col-lg-8">
                <div class="tbl_input"><span><input type="number" name="range1[]"  id="range11" class="need" min="1"></span></div></div></div>
              </div>
               <div class="col-lg-3">
            <div class="row"><div class="col-lg-4">
              <div class="tbl_text">To <span style="color:red;font-weight: bold;">*</span></div></div>
              <div class="col-lg-8">
                <div class="tbl_input"><span><input type="number" name="range1[]"  id="range12" class="need" min="1"></span></div></div></div>
              </div>
              <div class="col-lg-6">
                <div class="row">
                  <div class="col-lg-4"><div class="tbl_text">Price1 %  <span style="color:red;font-weight: bold;">*</span></div></div>
                  <div class="col-lg-8">
                  <div class="tbl_input"><span><input type="number" name="price1"  id="price1" class="need" min="1"></span></div></div></div></div>
                  </div> -->
<!-- 
                  <div class="sep_box">
                    <div class="col-lg-3">
                      <div class="row"><div class="col-lg-4">
                        <div class="tbl_text">Range2 <span style="color:red;font-weight: bold;">*</span></div></div>
                        <div class="col-lg-8">
                          <div class="tbl_input"><span><input type="number" name="range2[]"  id="range21" class="need" min="1"></span></div></div></div>
                        </div>
                        <div class="col-lg-3">
                      <div class="row"><div class="col-lg-4">
                        <div class="tbl_text">To <span style="color:red;font-weight: bold;">*</span></div></div>
                        <div class="col-lg-8">
                          <div class="tbl_input"><span><input type="number" name="range2[]"  id="range22" class="need" min="1"></span></div></div></div>
                        </div>
                        <div class="col-lg-6">
                          <div class="row">
                            <div class="col-lg-4"><div class="tbl_text">Price2 % <span style="color:red;font-weight: bold;">*</span></div></div>
                            <div class="col-lg-8"><div class="tbl_input"><span><input type="number" name="price2"  id="price2" class="need" min="1"></span></div></div></div>
                          </div>
                        </div> -->

                      <!--   <div class="sep_box">
                          <div class="col-lg-6">
                            <div class="row"><div class="col-lg-4"><div class="tbl_text">Range3 to more <span style="color:red;font-weight: bold;">*</span></div></div>
                            <div class="col-lg-8"><div class="tbl_input"><span><input type="number" name="range3"  id="range3" class="need" min="1"></span></div>
                          </div></div>
                        </div>
                        <div class="col-lg-6">
                          <div class="row">
                            <div class="col-lg-4"><div class="tbl_text">Price3 % <span style="color:red;font-weight: bold;">*</span></div></div>
                            <div class="col-lg-8">
                              <div class="tbl_input"><span><input type="number" name="price3"  id="price3" class="need" min="1"></span></div></div></div>
                            </div>

                          </div> -->
      <!--   <div class="sep_box">
         <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Height
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
             <span><input type="number" name="hight" id="hight" onblur="set_volumetric_wt()" min="1"></span>
            </div>
            </div>
        </div>
      </div>
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Measuring Unit1
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="munit1"  id="munit1"></span></div>
            </div>
            </div>
        </div>
   
    </div>
      -->
    <!-- <div class="sep_box">
     <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Breadth
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="number" name="breadth"  id="breadth" onblur="set_volumetric_wt()" min="1"></span></div>
            </div>
            </div>
        </div>
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Measuring Unit2
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="munit2"  id="munit2"></span></div>
            </div>
            </div>
        </div>
    
    </div> -->
  <!--     <div class="sep_box">
      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Length
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="number" name="length"  id="length" onblur="set_volumetric_wt()" min="1"></span></div>
            </div>
            </div>
        </div>
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Measuring Unit3
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="munit3"  id="munit3"></span></div>
            </div>
            </div>
        </div>
    
    </div> -->
   <!--    <div class="sep_box">
      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Weight
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="number" name="weight"  id="weight" min="1"></span></div>
            </div>
            </div>
        </div>
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Measuring Unit4
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="munit4"  id="munit4"></span></div>
            </div>
            </div>
        </div>
    
    </div> -->
      <div class="sep_box">
  <!--     <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Volumetric Weight (kg)
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="volweight"  id="volweight" readonly></span></div>
            </div>
            </div>
        </div> -->
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Product Warranty
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="pwarranty"  id="pwarranty"></span></div>
            </div>
            </div>
        </div>
   
    </div>
    <!--   <div class="sep_box">
       <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Meta Title
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="meta_ttl"  id="meta_ttl"></span></div>
            </div>
            </div>
        </div>
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Meta Keyword
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="meta_key"  id="meta_key"></span></div>
            </div>
            </div>
        </div>
    
    </div> -->
  <div class="sep_box">
   <!--  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Meta Discription
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="meta_disc"  id="meta_disc"></span></div>
            </div>
            </div>
        </div> -->                
      
         <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Main Image <span style="color:red;font-weight: bold;">*</span></div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="mainimg"  id="mainimg" class=""></span></div>
            </div></div>
        </div>
         </div>
         <div class="sep_box">
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image1</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="img1"  id="img1"></span></div></div></div>
        </div>
        <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image2</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="img2"  id="img2"></span></div></div>
            </div>
        </div>
    </div>
    <div class="sep_box">
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image3</div></div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="file" name="img3"  id="img3"  ></span></div>
            </div></div>
    </div>  
    <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image4</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="img4"  id="img4"></span></div>
          </div>
          </div>
        </div></div>
 
  <div class="sep_box">
          <div class="col-lg-6">
          <div class="row">
              <div class="col-lg-4"></div>
              <div class="col-lg-8">
                  <div class="submit_tbl">
                      <input type="button" name="proadd" class="btn_button sub_btn" value="Submit" onclick="validate_addpro();">
                  </div>
              </div>
          </div>
          </div>

          </div>


  </form> </div>

</div>
</div>

<script type="text/javascript">
 function selectcat(){
     var barid=$('#brandid').val();
     
     $.ajax({
        url: '<?php echo base_url()?>admin/product/brandcategory',
        type: 'POST',
        data: {'barid': barid},
        success: function(data){
           //alert(data); return false;
        var  option_brand = '<option value="">Select Main Category</option>';
        $('#mcatid').empty();
        $("#mcatid").append(option_brand+data);
                    
         }
  });
    
   } 

function selectfeature()
{
  var catid=$('#catid').val();

if(catid == ''){
return false;
}

$.ajax({
url: '<?php echo base_url(); ?>admin/product/selectfeature',
type: 'POST',
data: {'catid':catid},
success:function(data){
console.log(data);
var data1=JSON.parse(data);
var trHTML = '';
var j=1;

   for (i = 0; i < data1.length; i++) 
   {
    trHTML+="<div class='col-lg-6'>"+
                "<div class='row'>"+
                "<div class='col-lg-4'>"+
                "<div class='tbl_text'>"+data1[i]["LeftValue"]+"</div>"+
                "</div><div class='col-lg-8' style='margin-bottom:2px;'>"+
                " <div class='tbl_input' >"+
              "<span><input type='text'name='leftattribute["+data1[i]["FeatureLeftID"]+"]' id='leftattribute' ></span>"+
                  "</div>"+
                  "</span></div></div></div></div>";
  
   }
  // $('#attrbute').removeClass('hide');

  $('#fee').html(trHTML);
//console.log(data.trim().split('**')[2]);
//console.log(data.trim().split('**')[2].split('&&')[1]);

}
});
}

function selectatt(){
var catid=$('#catid').val();
if(catid == ''){
return false;
}
$.ajax({
url: '<?php echo base_url(); ?>admin/product/selectattribute',
type: 'POST',
data: {'catid':catid},
success:function(data){

//console.log(data);return false;
//console.log(data.trim().split('**')[2]);
//console.log(data.trim().split('**')[2].split('&&')[1]);
if(data.trim()!=""){
  var rep=data.trim().split('**');
  //alert(rep[1]);
  if(rep[1]!='')
  {
    $('#attr').show();
   $('#colorname').html(rep[1]);
  $('#agename').html(rep[0]); 
  }
  else
  {
     $('#attr').hide();

  }
  

  //$('#attrbute').removeClass('hide');

  //$('#attrbute').html(rep[2]);
  //$('#rightfeaturename').html(rep[2].split('&&')[1]);
}
}

});
}

function get_subcat(val)
{
    if(val == ''){
    return false;
    }
    $.ajax({
      url: '<?php echo base_url(); ?>admin/product/get_child_cat/'+val+'/'+$('#brandid').val(),
      type: 'POST',
      success:function(data){
        //alert(data);
        var  option_brand = '<option value="">Select Sub Category</option>';
        $('#catid').empty();
        $("#catid").append(option_brand+data);
                    
      }

    });
}


function profit_margin(ths)
{
  var sp=$('#sprice').val();
  if(sp!="" && $('#pprice').val()!="")
  {
     pm=(parseInt(sp)-parseInt($('#pprice').val()))*100/parseInt($('#pprice').val());
     $('#pmargin').val(pm.toFixed(2));
  }else{
    $('#pmargin').val('');
  }
  if(sp!="" && $('#pmrp').val()!=""){
     dis=(parseInt($('#pmrp').val())-parseInt(sp))*100/parseInt($('#pmrp').val());
     $('#discount').val(dis.toFixed(2));
  }else{
    $('#discount').val(''); 
  }
  
}
function validate_addpro()
{
  var err=0;
  $('.need').each(function(){
    if(this.value=="")
    { 
      err++
      $(this).css('border-color','red');
    }else{
      $(this).css('border-color','');
    }
  })

  $('.needed').each(function(){
    if($(this).val().search(/\S/) == -1)
    {
      err++
      $(this).css('border-color','red');
    }else{
      $(this).css('border-color','');
    }
  })
  if($('#mainimg').val()=="")
  {
    err++
    alert('please Select Main File image')
  }
  if(err==0)
  {
    $('#add_prod').submit();
  }

}

function set_volumetric_wt()
{
  if($('#length').val()!="" && $('#breadth').val()!="" && $('#hight').val()!=""){
     var vol=(parseInt($('#length').val())*parseInt($('#breadth').val())*parseInt($('#hight').val()))/5000;
     $('#volweight').val(vol.toFixed(2));
  }else{
    $('#volweight').val('');
  }
}

function selectattribute()
{
  var catid=$('#catid').val();
if(catid == ''){
return false;
}
$.ajax({
url: '<?php echo base_url(); ?>admin/product/selectatt',
type: 'POST',
data: {'catid':catid},
success:function(data){

console.log(data);return false;
//console.log(data.trim().split('**')[2]);
//console.log(data.trim().split('**')[2].split('&&')[1]);
if(data.trim()!=""){
  var rep=data.trim().split('**');
  //alert(rep[1]);
  if(rep[1]!='')
  {
    $('#attr').show();
   $('#colorname').html(rep[1]);
  $('#agename').html(rep[0]); 
  }
  else
  {
     $('#attr').hide();

  }
}
}
});
}
</script>


