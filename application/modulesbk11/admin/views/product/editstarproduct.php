<?php //p($data); ?>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Edit Product</h2>
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add Single Product with details.</p>
                        </div>
                    </div>
        <div class="page_box">
<form action="" method="post" enctype="multipart/form-data">
<div class="form">
<span id="message"></span>



    <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Image Name</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="file" name="img1" value="" id="img1" class="required" field="name" >
<input type="hidden" name="old_img1" value="<?php echo $image[1]->imgpath; ?>" >
<input type="hidden" name="img1_id" value="<?php echo $image[1]->proimgid; ?>" >
            </span>
            <?php if(!empty($image[1]->imgpath) && $image[1]->imgpath!='na') { ?>
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $image[1]->imgpath; ?>" height="75" width="50">   <?php } ?>

            </div>
          </div>
          </div>
      </div>
  
  </div>



<div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input type="submit" name="proupdate" class="btn_button sub_btn" value="Update">
                </div>
            </div>
        </div>
        </div>

        </div>


</form> </div>

</div>
</div>
</div>

<!-- <script>
function profit_margin()
{
  var sp=$('#sprice').val();
  if(sp!="" && $('#pprice').val()!="")
  {
     pm=(parseInt(sp)-parseInt($('#pprice').val()))*100/parseInt($('#pprice').val());
     $('#pmargin').val(pm.toFixed(2));
  }else{
    $('#pmargin').val('');
  }
  if(sp!="" && $('#pmrp').val()!=""){
     dis=(parseInt($('#pmrp').val())-parseInt(sp))*100/parseInt($('#pmrp').val());
     $('#discount').val(dis.toFixed(2));
  }else{
    $('#discount').val(''); 
  }
  
}
</script> -->