
<script type="text/javascript">
$(document).ready(function(){
var maxField = 1000; 
var addButton = $('.add_row'); 
var x = 1; 
var wrapper = $('.append'); 
/************************************************* Add More and remove for URL and Image **************************************************************/
$(addButton).click(function(){
   
    if(x < maxField){
        x++;
        $(".append_wrapper").clone().removeClass("append_wrapper").appendTo(".append");
    }
});
$(wrapper).on('click', '.remove_row', function(e){ //Once remove button is clicked
    if(x>1) {
    e.preventDefault();
    $(this).parent('div').parent('div').remove(); //Remove field html
    x--; //Decrement field counter
}
});
});
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?>
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Add Attribute</h2>
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add Single Attribute of product  with details.</p>
                        </div>
                    </div>
          <div class="page_box">
  <form action="" method="post" enctype="multipart/form-data" >
  <div class="form">
  <span id="message"></span>

  <div class="sep_box">


      </div>
                 
      <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Product <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
             <span>


             <select name="pid" id="catid" class="need" required>
                <option value="">Select Product</option>
                <?php     foreach ($mainproduct as $key => $value) { ?>
                  <option value="<?php echo $value->proid;  ?>">
                  <?php echo $value->proname;  ?></option>
               <?php } ?>
                
              </select></div>
            </div>
        </div>
      </div>
   
       </div>


     

  <div class="sep_box">

      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Attribute Name <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="aname"  id="aname" class="needed" required></span></div>
            </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Article small description <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
             <span><input type="text" name="anumber"  id="anumber" class="needed" required></span></div>
            </div>
        </div>
      </div>
      </div>
      <div class="sep_box">

        <div class="col-lg-6">

          <div class="row">
            <div class="col-lg-4">
              <div class="tbl_text">Article SKU <span style="color:red;font-weight: bold;">*</span>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="tbl_input">
               <span><input type="text" name="psku"  id="psku" class="needed" required></span></div>
             </div>
           </div>
         </div>
        <div class="col-lg-6">

          <div class="row">
            <div class="col-lg-4">
              <div class="tbl_text">Barcode Number
              </div>
            </div>
            <div class="col-lg-8">
              <div class="tbl_input">
               <span><input type="text" name="barcode"  id="barcode" required></span></div>
             </div>
           </div>
         </div>
        </div>

        <div class="sep_box">
          
            <div class="col-lg-6">
              <div class="row">
                <div class="col-lg-4">
                  <div class="tbl_text">Article Description
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="tbl_input">
                    <span><textarea name="pdes" id="pdes" required></textarea></span></div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-4">
                <div class="tbl_text">Stock Qty <span style="color:red;font-weight: bold;">*</span>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="tbl_input">
                  <span><input type="number" name="stock"  id="stock" min="1" required></span></div>
                </div>
              </div>
            </div>
            </div>

            <div class="sep_box">
              <div class="col-lg-6">
                <div class="row">
                  <div class="col-lg-4">
                    <div class="tbl_text">Purchase Price <span style="color:red;font-weight: bold;">*</span>
                    </div>
                  </div>
                  <div class="col-lg-8">
                    <div class="tbl_input">
                      <span><input type="number" name="pprice"  id="pprice" class="need" required min="1"></span></div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="tbl_text">Article MRP <span style="color:red;font-weight: bold;">*</span>
                      </div>
                    </div>
                    <div class="col-lg-8">
                      <div class="tbl_input">
                        <span><input type="number" name="pmrp"  id="pmrp" class="need" required min="1"></span></div>
                      </div>
                    </div>
                  </div>
                </div>

      <div class="sep_box">
       <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Selling Price <span style="color:red;font-weight: bold;">*</span>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="number" name="sprice"  id="sprice" class="need" required min="1"></span></div>
            </div>
          </div>
        </div>
     
      </div>

     
      <div class="sep_box">
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Article Warranty
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="pwarranty"  id="pwarranty" required></span></div>
            </div>
            </div>
        </div>
   
    </div>
   
  <div class="sep_box">
           
      
         <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Main Image <span style="color:red;font-weight: bold;">*</span></div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="mainimg"  id="mainimg" class="" required></span></div>
            </div></div>
        </div>
         </div>
         <div class="sep_box">
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image1</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="img1"  id="img1"></span></div></div></div>
        </div>
        <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image2</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="img2"  id="img2"></span></div></div>
            </div>
        </div>
    </div>
    <div class="sep_box">
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image3</div></div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="file" name="img3"  id="img3"  ></span></div>
            </div></div>
    </div>  
    <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image4</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="img4"  id="img4"></span></div>
          </div>
          </div>
        </div></div>
 
  <div class="sep_box">
          <div class="col-lg-6">
          <div class="row">
              <div class="col-lg-4"></div>
              <div class="col-lg-8">
                  <div class="submit_tbl">
                      <input type="submit" name="proadd" class="btn_button sub_btn" value="Submit">
                  </div>
              </div>
          </div>
          </div>

          </div>


  </form> </div>

</div>
</div>





