
<script type="text/javascript">
$(document).ready(function(){
var maxField = 1000; 
var addButton = $('.add_row'); 
var x = 1; 
var wrapper = $('.append'); 
/************************************************* Add More and remove for URL and Image **************************************************************/
$(addButton).click(function(){
   
    if(x < maxField){
        x++;
        $(".append_wrapper").clone().removeClass("append_wrapper").appendTo(".append");
    }
});
$(wrapper).on('click', '.remove_row', function(e){ //Once remove button is clicked
    if(x>1) {
    e.preventDefault();
    $(this).parent('div').parent('div').remove(); //Remove field html
    x--; //Decrement field counter
}
});
});
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?>
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Edit Attribute</h2>
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can Edit Single Attribute of product  with details.</p>
                        </div>
                    </div>
          <div class="page_box">
  <form action="" method="post" enctype="multipart/form-data" >
  <div class="form">
  <span id="message"></span>

  <div class="sep_box">

      </div>
                 
      <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Product <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
             <span>


             <select name="pid" id="catid" class="need" required>
                <option value="">Select Product</option>
                <?php     foreach ($mainproduct as $key => $value) { ?>
                  <option value="<?php echo $value->proid;  ?>" <?php if($attproduct->prd_pid==$value->proid) { ?> selected <?php } ?>>
                  <?php echo $value->proname;  ?></option>
               <?php } ?>
                
              </select></div>
            </div>
        </div>
      </div>
   
       </div>


  <div class="sep_box">

      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Attribute Name <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="aname"  id="aname" class="needed" required value="<?php echo $attproduct->proname; ?>"></span></div>
            </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Article small description <span style="color:red;font-weight: bold;">*</span></div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
             <span><input type="text" name="anumber"  id="anumber" class="needed" required value="<?php echo $attproduct->prd_anumber; ?>"></span></div>
            </div>
        </div>
      </div>
      </div>
      <div class="sep_box">

        <div class="col-lg-6">

          <div class="row">
            <div class="col-lg-4">
              <div class="tbl_text">Article SKU <span style="color:red;font-weight: bold;">*</span>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="tbl_input">
               <span><input type="text" name="psku"  id="psku" class="needed" required value="<?php echo $attproduct->prosku; ?>"></span></div>
             </div>
           </div>
         </div>
        <div class="col-lg-6">

          <div class="row">
            <div class="col-lg-4">
              <div class="tbl_text">Barcode Number
              </div>
            </div>
            <div class="col-lg-8">
              <div class="tbl_input">
               <span><input type="text" name="barcode"  id="barcode" required value="<?php echo $attproduct->barcode; ?>"></span></div>
             </div>
           </div>
         </div>
        </div>

        <div class="sep_box">
          
            <div class="col-lg-6">
              <div class="row">
                <div class="col-lg-4">
                  <div class="tbl_text">Article Description
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="tbl_input">
                    <span><textarea name="pdes" id="pdes" required><?php echo $attproduct->description; ?></textarea></span></div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-4">
                <div class="tbl_text">Stock Qty <span style="color:red;font-weight: bold;">*</span>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="tbl_input">
                  <span><input type="number" name="stock"  id="stock" min="1" required value="<?php echo $attproduct->prd_stock; ?>"></span></div>
                </div>
              </div>
            </div>
            </div>

            <div class="sep_box">
              <div class="col-lg-6">
                <div class="row">
                  <div class="col-lg-4">
                    <div class="tbl_text">Purchase Price <span style="color:red;font-weight: bold;">*</span>
                    </div>
                  </div>
                  <div class="col-lg-8">
                    <div class="tbl_input">
                      <span><input type="number" name="pprice"  id="pprice" class="need" required min="1" value="<?php echo $attproduct->prd_pprice; ?>"></span></div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="tbl_text">Article MRP <span style="color:red;font-weight: bold;">*</span>
                      </div>
                    </div>
                    <div class="col-lg-8">
                      <div class="tbl_input">
                        <span><input type="number" name="pmrp"  id="pmrp" class="need" required min="1" value="<?php echo $attproduct->prd_pmrp; ?>"></span></div>
                      </div>
                    </div>
                  </div>
                </div>

      <div class="sep_box">
       <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Selling Price <span style="color:red;font-weight: bold;">*</span>
            </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="number" name="sprice"  id="sprice" class="need" required min="1" value="<?php echo $attproduct->prd_sprice; ?>"></span></div>
            </div>
          </div>
        </div>
     
      </div>

     
      <div class="sep_box">
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4">
            <div class="tbl_text">Article Warranty
  </div>
          </div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="text" name="pwarranty"  id="pwarranty" required value="<?php echo $attproduct->warrenty; ?>"></span></div>
            </div>
            </div>
        </div>
   
    </div>
   
  <div class="sep_box">
           
      
         <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Main Image <span style="color:red;font-weight: bold;">*</span></div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="mainimg"  id="mainimg" class="" ></span>
<input type="hidden" name="old_mainimg" value="<?php echo $attproduct->prd_main_image; ?>" >
<input type="hidden" name="mainimg_id" value="<?php echo $attproduct->proimgid; ?>" >
            </span>

<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $attproduct->prd_main_image; ?>" height="75" width="50">   



            </div>
            </div></div>
        </div>
         </div>
         <div class="sep_box">
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image1</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="img1"  id="img1"></span>


<input type="hidden" name="old_img1" value="<?php echo $attproduct->prd_img1; ?>" >
<input type="hidden" name="img1_id" value="<?php echo $attproduct->proimgid; ?>" >
            </span>
         
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $attproduct->prd_img1; ?>" height="75" width="50">   


            </div></div></div>
        </div>
        <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image2</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="img2"  id="img2"></span>

<input type="hidden" name="old_img2" value="<?php echo $attproduct->prd_img2; ?>" >
<input type="hidden" name="img2_id" value="<?php echo $attproduct->proimgid; ?>" >
            </span>
 <?php if(!empty($attproduct->prd_img2) && $attproduct->prd_img2!='na') { ?>
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $attproduct->prd_img2; ?>" height="75" width="50">   <?php } ?>
            </div></div>
            </div>
        </div>
    </div>
    <div class="sep_box">
  <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image3</div></div>
          <div class="col-lg-8">
            <div class="tbl_input">
              <span><input type="file" name="img3"  id="img3"  ></span>
<input type="hidden" name="old_img3" value="<?php echo $attproduct->prd_img3; ?>" >
<input type="hidden" name="img3_id" value="<?php echo $attproduct->proimgid; ?>" >
            </span>
 <?php if(!empty($attproduct->prd_img3) && $attproduct->prd_img3!='na') { ?>
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $attproduct->prd_img3; ?>" height="75" width="50">   <?php } ?>
            </div>


              </div>
            </div></div>
    </div>  
    <div class="col-lg-6">
        <div class="row">
          <div class="col-lg-4"><div class="tbl_text">Image4</div></div>
          <div class="col-lg-8">
            <div class="tbl_input"><span><input type="file" name="img4"  id="img4"></span>


            
<input type="hidden" name="old_img4" value="<?php echo $attproduct->prd_img4; ?>" >
<input type="hidden" name="img4_id" value="<?php echo $attproduct->proimgid; ?>" >
            </span>
 <?php if(!empty($attproduct->prd_img4) && $attproduct->prd_img4!='na') { ?>
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $attproduct->prd_img4; ?>" height="75" width="50">   <?php } ?>



            </div>
          </div>
          </div>
        </div></div>
 
  <div class="sep_box">
          <div class="col-lg-6">
          <div class="row">
              <div class="col-lg-4"></div>
              <div class="col-lg-8">
                  <div class="submit_tbl">
                      <input type="submit" name="proupdate" class="btn_button sub_btn" value="Submit">
                  </div>
              </div>
          </div>
          </div>

          </div>


  </form> </div>

</div>
</div>





