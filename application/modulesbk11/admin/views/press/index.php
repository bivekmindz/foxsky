<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Press Release</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add press release.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box1">

                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/press/add"><button>ADD Press release</button></a>
                                </div>
                                 
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                 <form method="post" action="">
                             
                                <table class="grid_tbl" id="search_filter">
                                    <thead>

                                        <tr>
                                          
                                            <th>S:NO</th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Url</th>
                                            
                                            
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                    if(!empty($vieww))
                                    {
                                      if($_GET['page'])
                                      {
                                        $page = $_GET['page']-1; 
                                        $i=$page*50; 
                                      }
                                    }
                                    foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                           
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->press_title ;?></td>
                                            <td><?php if(!empty($value->press_images)){ ?><img height="50" width="50" src="<?php echo base_url()?>imagefiles/pressimages/<?php echo $value->press_images;?>"><?php } else { echo "-"; } ?></td>
                                            <td><?php echo $value->press_url;  ?></td>

                                            
                                           <input type='hidden' name='attstatu[<?php echo $value->id;?>]'  value='<?=$value->status?>'>

                                         
                                            <td><a href="<?php echo base_url()?>admin/press/pressupdate/<?php echo $value->id?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/press/pressdelete/<?php echo $value->id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/press/pressstatus/<?php echo $value->id.'/'.$value->status; ?>"><?php if($value->status=='2') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
    $('#validate_frm').click(function(){
     if($('#brand_name').val().search(/\S/) == -1)
     {
       $('#brand_name').css('border-color','red');
     }else{
       $('#searchfrm').submit();
     }
    })

   
 /* $(document).ready(function(){
    $('#selAll').click(function(){   
      $('.chkApp').each(function() { //loop through each checkbox
          this.checked = true;  //select all checkboxes with class "checkbox1"               
      });
});

  });
$(document).ready(function(){
    $('#DeselAll').click(function(){   
      $('.chkApp').each(function() { //loop through each checkbox
          this.checked = false;  //select all checkboxes with class "checkbox1"               
      });
});

  });
*/

</script>

<script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
    </script>
