  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore: [],
        rules: {
            rep_title       : "required",
            rep_ingredients : "required",
            "rep_category[]"    : "required",
            "rep_products[]"    : "required",
        },
        // Specify the validation error messages
        messages: {
            rep_title       : "Recipe Title is required.",
            rep_ingredients : "Recipe Ingredient is required",
            "rep_category[]": "Recipe Category is required",
            "rep_products[]"    : "Recipe Product is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
            
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Update Recipe</h2>
                    </div>
                    <div class="page_box" style="overflow: inherit;">
                        
                    <div class="sep_box">
                        <div class="col-lg-12">
                            <div class='flashmsg'>
                                <?php
                                  if($this->session->flashdata('message')){
                                    echo $this->session->flashdata('message'); 
                                  }
                                ?>
                            </div>
                        </div>
                    </div>

        <form id="addCont" action="" method="post" enctype="multipart/form-data" >
            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Recipe Title <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input id="rep_title" name="rep_title" type="text" value="<?php echo $recipe_detail->r_title; ?>" /></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Recipe Banner <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input id="rep_banner" name="rep_banner" type="file" /></div>
                            <div style="margin-top: 25px"><img height="50px" width="50px" src="<?php echo base_url()?>assets/webapp/recipe_img/<?php echo $recipe_detail->r_banner ;?>"></div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="sep_box">    
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Recipe Ingredients <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><textarea name="rep_ingredients" id="rep_ingredients"><?php echo $recipe_detail->r_ingredient; ?></textarea></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Choose Category <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input">
                                <select name="rep_category[]" id="rep_category" multiple="multiple">
                                <?php 
                                    $catarr=explode(",", $get_category->recipe_cat);
                                    foreach ($all_category as $key => $val) { 
                                        if(!empty($val->catname)){
                                ?>
                                    <option <?php if(in_array($val->catid, $catarr)){ echo "selected"; } ?> value="<?php echo $val->catid; ?>"><?php echo $val->catname; ?></option>
                                <?php } } ?>    
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Add Recipe Products <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input">
                                <select name="rep_products[]" id="rep_products" multiple="multiple">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-8">
                            <div class="submit_tbl">
                                <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div>
</div> 

<script type="text/javascript">
$(document).ready(function() {
   $('#rep_products').multiselect({ 
        enableClickableOptGroups: true,
        enableFiltering: true,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
    });
});

$(document).ready(function() {
   $('#rep_category').multiselect({ 
        enableClickableOptGroups: true,
        enableFiltering: true,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
    });
});

$("#rep_category").change(function(){
   var categoryid = $("#rep_category").val();
    //alert(categoryid);
      $.ajax({
            url: "<?php echo base_url() ?>admin/recipe/getproductsupdate/<?php echo $this->uri->segment(4)?>",
            type:'POST', 
            data:{'categoryid':categoryid},
            success: function(result){
            //alert(result);
              $("#rep_products").empty();
              $("#rep_products").attr("multiple","multiple");
              $("#rep_products").multiselect('destroy');
              $("#rep_products").append(result);
              $('#rep_products').multiselect({ 
                enableClickableOptGroups: true,
                enableFiltering: true,
                includeSelectAllOption: true,
                enableCaseInsensitiveFiltering: true,
              });
            }
      });
});

function recipepro(){
    var categoryid = $("#rep_category").val();
    //alert(categoryid);
      $.ajax({
            url: "<?php echo base_url() ?>admin/recipe/getproductsupdate/<?php echo $this->uri->segment(4)?>",
            type:'POST', 
            data:{'categoryid':categoryid},
            success: function(result){
            //alert(result);
              $("#rep_products").empty();
              $("#rep_products").attr("multiple","multiple");
              $("#rep_products").multiselect('destroy');
              $("#rep_products").append(result);
              $('#rep_products').multiselect({ 
                enableClickableOptGroups: true,
                enableFiltering: true,
                includeSelectAllOption: true,
                enableCaseInsensitiveFiltering: true,
              });
            }
      });
}

recipepro();
</script>