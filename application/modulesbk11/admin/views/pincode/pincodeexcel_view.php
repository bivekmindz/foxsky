<style>
label {
    
    color: red;
}

 </style>
 <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

 <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
    $("#submitproduct").validate({
        // Specify the validation rules
        rules: {
            userfile    : "required"
         },
        // Specify the validation error messages
        messages: {
             userfile   : "Please select file!"
         },
        submitHandler: function(form) {
            form.submit();
        }
    });

 
  });
  
</script>


<div class="wrapper">

<?php  $this->load->view('helper/nav')?> 
   <div class="col-lg-10 col-lg-push-2">
      <div class="row">
         <div class="page_contant">
            <div class="col-lg-12">
               <div class="page_name">
                  <h2>Upload  Bulk Pincode</h2>
               </div>
               <div class="page_box">
                  <div class="col-lg-12">
                     <p>In this Section Admin can download and upload excel .</p>
                  </div>
               </div>
               <div class="page_box">
                  <div class="sep_box">
                     <div class="col-lg-12">
                        <div class='flashmsg'>
                           <?php
                              if($excelErr!=""){
                                echo $excelErr; 
                              }
                              ?>
                        </div>
                     </div>
                  </div>
                 
              <form action="" method="post" id = "downloadexcel" enctype="multipart/form-data" >
                 <div class="sep_box">
                    <div class="col-lg-6">
                       <div class="row">
                           <div class="col-lg-4">Download file <span style="color:red;font-weight: bold;">*</span></div>
                          <div class="col-lg-8">
                             <div class="submit_tbl">
                                <input type="submit" name="download" value="Download excel" class="btn_button sub_btn" >
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </form>

            <form id = "submitproduct" method = "post" action ="" enctype="multipart/form-data">
               <div class="sep_box">
                  <div class="col-lg-12">
                     <h3>Upload Excel File</h3>
                  </div>
                  <?php  
                     $errMsgg = "";
                     foreach ($upload as $key => $value) {
                       //p($value);
                       $errMsgg .= '<li style="list-style-type:bullet; line-height:25px;">'.$value->Description.'<br>';
                     }
                     
                     if(!empty($errMsgg)){
                       print '<b style="color:red;">Uploading Error: Product already exists:</b><br>';
                       print '<p style="border-bottom:1px solid silver; padding-bottom:10px; margin-bottom:20px;">'.$errMsgg.'</p><br>';
                     }
                     
                     ?>
                 
               </div>
               <div class="sep_box">
                  <div class="col-lg-6">
                     <div class="row">
                        <div class="col-lg-4">Select file <span style="color:red;font-weight: bold;">*</span></div>
                        <div class="col-lg-8"> <input  type="file"  name="userfile" id="userfile" /></div>
                     </div>
                  </div>
               </div>
               <div class="sep_box">
                  <div class="col-lg-6">
                     <div class="col-lg-4"></div>
                     <div class="col-lg-8">
                        <div class="submit_tbl">
                           <input type="submit" value = "Submit" name="submit" class="btn_button sub_btn"/>
                        </div>
                     </div>
                  </div>
               </div>
            </form>



               </div>
            </div>
         </div>
      </div>
   </div>
</div>



