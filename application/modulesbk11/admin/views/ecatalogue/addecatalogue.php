 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            ecat         : "required",
        },
        // Specify the validation error messages
        messages: {
            ecat         : "Required",
            
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Products in E-catalogue</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add products in E-catalogue.</p>
                        </div>
                    </div>
                    <div class="page_box">
        <div class="col-lg-12">
          <div class="sep_box">
            <div style="text-align:right;"><a href="<?php echo base_url() ?>admin/ecatalogue/createpdf"><button>E-catalogue List</button></a></div>
          </div>
         <!--   <div style="float: left;width: 100%;margin-left: 14px;"><h4>Create New Catalogue</h4></div> -->
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div>


            <form id="addCont" action="" method="post" enctype="multipart/form-data" >
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Add Products" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <div   class="sep_box">
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Country <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                          <select name="countryid" id="countryid" onchange="selectstates();">
                                            <option value="none">Select Country</option>
                                            <?php foreach ($viewcountry as $key => $value) {
                                                
                                            ?>
                                             <option value="<?php echo $value->conid ?>"><?php echo $value->name ?></option>
                                            <?php } ?>
                                        </select>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">State <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                        <select name="stateid" id="stateid" onchange="statecountry();">
                                            <option value="none">Select State</option>
                                        </select>

                                          </div>
                                    </div>
                                </div>
                            </div> 


                             <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">City <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                       <select name="cityid" id="cityid">
                                            <option value="none">Select City</option>
                                        </select>

                                          </div>
                                    </div>
                                </div>
                            </div> 

                            </div>     

                        <div   class="sep_box">
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Ecatalogue Name <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                          <select name="ecat" id="ecat" onchange="">
                                                <option value="">Select Type</option>
                                                <?php foreach ($ecatlist as $key => $value) { ?>
                                                <option value="<?php echo $value->id;?>"><?php echo $value->ecat_name;?></option>
                                                <?php } ?>
                                            </select></div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Brands <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                  <select name='brandid' class="brandid" id="mcatidd" onchange="barndcategory($(this));" >
                                  <option value="">Select Brand</option>
                                  <?php foreach ($branddata as $key => $value) {
                                  ?>
                                  <option value="<?php echo $value->id ?>"><?php echo $value->brandname; ?></option>
                                  <?php }?>
                                  </select>

                                          </div>
                                    </div>
                                </div>
                            </div> 

                             
                            
                             <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text"><span style="color:red;font-weight: bold;">--</span></div>
                                        <div class="tbl_input">
                                              By Category<input type="radio" name="cars" class="check" checked="checked" value="2"  />
                                              By Sub Category<input type="radio" name="cars" value="3" />
                                            </div>
                                    </div>
                                </div>
                            </div>    

                            </div>  
                            <div class="col-lg-4" id="div1">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="sec_cat" id="sec_cat_single" onchange="selectproductbycat();">
                                              <option value="">Select Type</option>
                                                <?php foreach ($parentcat as $key => $value) { ?>
                                                <option value="<?php echo $value->catid;?>"><?php echo $value->catname;?></option>
                                                <?php } ?>
                                               
                                                
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                            <div id="div2">
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="sec_cat" id="sec_cat" onchange="selectsubcat();">
                                              <option value="">Select Type</option>
                                                <?php foreach ($parentcat as $key => $value) { ?>
                                                <option value="<?php echo $value->catid;?>"><?php echo $value->catname;?></option>
                                                <?php } ?>
                                               
                                                
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Sub Category <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="sec_sub_cat" id="sec_sub_cat" onchange="selectproduct();">
                                                
                                                
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                          </div>
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select All <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <input type="checkbox" id="selectall" name="vehicle" value="Bike"><br>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div id="mainidiv">
                            
                        </div>
                        </div>
                       



                       

                       
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div> 


<script type="text/javascript">
   $(document).ready(function(){
     $("#div1").show();
 $("#div2").hide();
$("input[type=radio]").click(function(){
 if($(this).hasClass("check")){
 $("#div1").show();
 $("#div2").hide();
 }else{
  $("#div2").show();
 $("#div1").hide();
 }
});

   });

   $(document).ready(function(){
$("#selectall").click(function(){
        //alert("just for check");
        if(this.checked){
            $('.checkboxall').each(function(){
                this.checked = true;
            })
        }else{
            $('.checkboxall').each(function(){
                this.checked = false;
            })
        }
    });
});
    function selectsubcat(){
     var parentcat=$('#sec_cat').val();
     $.ajax({
        url: '<?php echo base_url()?>admin/ecatalogue/subcategory',
        type: 'POST',
        data: {'parentcat': parentcat},
        success: function(data){
           //alert(data); //return false;
           var option="<option value=''>Select subcategory</option>";
            $('#sec_sub_cat').empty();
            $("#sec_sub_cat").html(option+data);
           // $('#sec_subcat').multiselect({ 
               // enableClickableOptGroups: true,
               // enableFiltering: true,
              //  includeSelectAllOption: true,

           // });
              
         }
  });
    
   }
   function selectproduct(){
     var parentcat=$('#sec_sub_cat').val();
     
     var cityid=$('#cityid').val();
    // alert(parentcat); return false;
     $.ajax({
        url: '<?php echo base_url()?>admin/ecatalogue/getproduct',
        type: 'POST',
        data: {'parentcat': parentcat,'city':cityid},
        success: function(data){
           //alert(data); return false;
            $('#mainidiv').empty();
            $("#mainidiv").html(data);
            //$('#sec_product').multiselect({ 
                //enableClickableOptGroups: true,
                //enableFiltering: true,
               //includeSelectAllOption: true,

            //});
              
         }
  });
    
   }

   function selectproductbycat(){
     var parentcat=$('#sec_cat_single').val();
     
     var cityid=$('#cityid').val();
    // alert(parentcat); return false;
     $.ajax({
        url: '<?php echo base_url()?>admin/ecatalogue/selectproductbycat',
        type: 'POST',
        data: {'parentcat': parentcat,'city':cityid},
        success: function(data){
           //alert(data); return false;
            $('#mainidiv').empty();
            $("#mainidiv").html(data);
            //$('#sec_product').multiselect({ 
                //enableClickableOptGroups: true,
                //enableFiltering: true,
               //includeSelectAllOption: true,

            //});
              
         }
  });
    
   }
   

    function barndcategory(){
     
     var brand=$('#mcatidd').val();
     var cityid=$('#cityid').val();
    // alert(parentcat); return false;
     $.ajax({
        url: '<?php echo base_url()?>admin/ecatalogue/getproductbybrand',
        type: 'POST',
        data: {'brandid':brand,'city':cityid},
        success: function(data){
           //alert(data); return false;
            $('#mainidiv').empty();
            $("#mainidiv").html(data);
            //$('#sec_product').multiselect({ 
                //enableClickableOptGroups: true,
                //enableFiltering: true,
               //includeSelectAllOption: true,

            //});
              
         }
  });
    
   }
   function selectstates(){
var countryid=$('#countryid').val();
$('#staterid').show();
$.ajax({
 url: '<?php echo base_url()?>admin/geographic/countrystate',
type: 'POST',
data: {'countryid': countryid},
success: function(data){
var  option_brand = '<option value="none">Select State</option>';
$('#stateid').empty();
$("#stateid").append(option_brand+data);
 }
});
}

function statecountry(){
var  stid=$('#stateid').val();
$.ajax({
 url: '<?php echo base_url()?>admin/vendor/citystate',
type: 'POST',
data: {'stid': stid},
success: function(data){
var  option_brand = '<option value="none">Select City</option>';
$('#cityid').empty();
$("#cityid").append(option_brand+data);
 }
});
}

</script>    

    