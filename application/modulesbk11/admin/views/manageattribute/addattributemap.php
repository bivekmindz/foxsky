 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <!-- <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script> -->  
  <script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            attnameid : "required",
            "attname[]"   : "required",
            "attnametext[]" : "required",
        },
        // Specify the validation error messages
        messages: {
            attnameid : "Attribute name is required",
            "attname[]" : "Attribute Value is required",
            "attnametext[]" : "Attribute Text is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    <script src="<?php echo admin_url();?>js/jscolor.js"></script>

    <h2>Add Attribute Map</h2>
    
    </div>
        <div class="page_box">
         <div class="sep_box">
         <div class="col-lg-12">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/attribute/viewattributemap"><button>CANCEL</button></a>
        </div>
        <div class='flashmsg'>
            <?php echo validation_errors(); ?>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div>
        </div>
        <form id="addCont" action="" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Attribute <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="attnameid" id="attnameid" onchange="setColorBox(this.value)">
                <option value="">Select Attribute</option>
                <?php foreach ($vieww as $key => $value) {
               
                 ?>
                 <option value="<?php echo $value->id ?>"><?php echo $value->attname ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>

       <div class="col-lg-6" id="showColorBox" style='display:none'>
            <div class="row">
                <div class="col-lg-3" >
                    <div class="tbl_text">Choose color <span style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-7">
                    <div class="tbl_input">
                    <input type="text" class="jscolor" name="attcolor[]"  id="attcolor" onClick="colorpik()" /></div>
                </div>
        </div><!-- color div -->

</div>

    </div>
    <div class="sep_box">

    <div class="col-lg-12">
 

 <div id="attBox" style='border-bottom:1px solid green; padding-bottom:5px;'>
<div class="col-lg-1">
    <div style='width:30px; float:left; padding:5px; border:1px solid silver; font-weight:bold; cursor:pointer;position:absolute;top:15px;left:0px;' id="addMore">(+)</div>
    </div>
    <div class="sep_box repAttr" style='border-bottom:1px solid silver; padding-bottom:5px;'>
    <div class="col-lg-1"></div>
        
        <div class="col-lg-5">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Attribute Value <span style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input"><input type="text" name="attname[]"  id="attname" class="attname" /></div>
                </div>
            </div>
        </div><!-- attribute div -->

        <div class="col-lg-5">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Attribute Text <span style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-4">
                    <div class="tbl_input"><input id="attnametext" type="text" name="attnametext[]" /></div>
                </div>
            </div>
        </div><!-- text div -->

    </div><!-- end row -->
  </div>
  </div>
  </div>


        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>


   <script type="text/javascript">
   function setColorBox(attid){
     var attid =$('#attnameid option:selected').text();

      if(attid=='Color'){
       $('#attname').val("");
        $('#attnametext').val("");
       $('#showColorBox').show();
       /*var colrvalue=$('.jscolor').val();
       alert(colrvalue);
        $('#attname').val(colrvalue);*/
     }
      else
       {
        $('#attname').val("");
        $('#attnametext').val("");
       $('#showColorBox').hide();
       
       }

    }

    $(document).ready(function(){
       
        $("#addMore").click(function(){
            var remme = "<div style='width:30px; float:left; padding:5px; border:1px solid #FF5200;color:#FF5200;cursor:pointer;' class='remMore' onclick='delme($(this));'>(-)</div>";
            $(".repAttr:last-child").clone(true).appendTo("#attBox");
            //alert($(".repAttr:last-child div.remMore").length);

            if($(".repAttr:last-child div.remMore").length == 0){
                $(".repAttr:last-child").prepend(remme);
                return false;
            }
        }); 
    });  

    function delme(vv){
        $(vv).parent().remove();
        return false;    
    }


   </script>