 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            cattype         : "required",
            category_name   : "required",
            cat_description : "required",
            category_url    : "required",
            meta_title      : "required",
            meta_desc       : "required",
            meta_keywords   : "required",
            cat_level       : "required",
            cbcheck         : "required",
           /* email:{
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            agree: "required" */
        },
        // Specify the validation error messages
        messages: {
            cattype         : "Choose Category type",
            category_name   : "Category Name is required",
            cat_description : "Category Description is required",
            meta_title      : "Meta title is required",
            meta_desc       : "Meta Description is required",
            meta_keywords   : "Meta Keywords is required",
            cat_level       : "Choose Sort Order",
            cbcheck         : "Required",
            /*password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            email: "Please enter a valid email address",
            agree: "Please accept our policy"*/
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
            
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add More Products in Existing Combo</h2>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
                            <div class="col-lg-12">
                    <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/attribute/combolisting"><button>CANCEL</button></a>
           </div>
<form action="" method="post" enctype="multipart/form-data" >
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Brand <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name='brandid' class="brandid" id="brandid" onchange="barndcategory();" >
                                                <option value="">Select Brand</option>
                                                <?php foreach ($branddata as $key => $value) {  ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->brandname; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Main Category <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="mcatidd" id="mcatidd" class="mcatidd" onchange="childcategory();" >
                                                <option value="">Select Main Category</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Sub Category <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="catidd" id="catidd" class="catidd" onchange="get_products();" >
                                                <option value="">Select Sub Category</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Product <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="catproidd" id="catproidd" class="catproidd" onchange="productprice();" >
                                                <option value="">Select Product</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Product MRP (Per Quantity) <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input id="combopromrp" name="combopromrp" id="combopromrp" type="text" class="combopromrp" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Selling Price(Per Quantity)<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input id="combosellpri" name="combosellpri" id="combosellpri" type="text" class="combosellpri" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Quantity <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input id="comboqty" name="comboqty" id="comboqty" type="text" class="comboqty" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Combo Price <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input id="combopri" name="combopri" id="combopri" type="text" class="combopri" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                   
                </div>
                </form>
            </div>
        </div>
    </div>
</div> 
    
<script type="text/javascript">

function barndcategory(){
    
    var brandid = $('#brandid').val();
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/get_maincategoryedit',
        type: 'POST',
        data: {'brandids': brandid},
        success: function(data){
            
            var option='<option value="">Select Main Category</option>';
            $('#mcatidd').empty();
            $('#mcatidd').html(option+data);
        }
    });
    
   }

   function childcategory(){
    var catid = $('#mcatidd').val();
    var brandid = $('#brandid').val();
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/get_subcatedit',
        type: 'POST',
        data: {'catid': catid,'brandid':brandid},
        success: function(data){
               
            var option='<option value="">Select Sub Category</option>';
            $('#catidd').empty();
            $('#catidd').html(option+data);
            
        }
    });
    
   }

 function get_products(elev)
 {
    var catid = $('#catidd').val();
    var brandid = $('#brandid').val();
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/get_productsedit',
        type: 'POST',
        data: {'catid': catid,'brandid':brandid},
        success: function(data){
            var option='<option value="">Select Product</option>';
            $('#catproidd').empty();
            $('#catproidd').html(option+data);
        }
    });
 }  

 function productprice()
 {
    //var productid = $("#productid").val();
     var productid = $('#catproidd').val();
    //alert(productid); return false;
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/productprice',
        type: 'POST',
        dataType: 'json',
        data: {'productid': productid},
        success: function(data){
            //console.log(data);
            //alert(data); return false;
            $('#combopromrp').val(data.productMRP); 
            $('#combosellpri').val(data.FinalPrice); 
            //$("#mrpprice").val(data.productMRP);
            //$("#finalprice").val(data.FinalPrice); 
            
           
        }
    });
 }  

</script>