<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage Return Processing Orders</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view All Return Processing order details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                          <th>S.NO.</th>
                                          <th>Order No.</th>
                                          <th>Customer Name</th>
                                          <th>Email</th>
                                          <th>Mobile</th>
                                          <th>PaymentMode</th>
                                          <th>PaymentStatus</th>
                                          <!-- <th>ShippingStatus</th> -->
                                          <!-- <th>Amount</th> -->
                                          <th>Order Process Date</th>
                                          <!-- <th>Vendor Name</th> -->
                                          <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0;$j=0;
                                        if(!empty($penview))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          
                                        foreach ($penview as $key => $value) { $i++; //p(substr($value->Remark, 0, 17) );?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->OrderNumber;?></td>
                                            <td><?php echo $value->ShippingName;?></td>
                                            <td><?php echo $value->Email;?></td>
                                            <td><?php echo $value->ContactNo;?></td>
                                            <td><?php echo $value->PaymentMode;?></td>
                                            <td><?php echo $value->PaymentStatus;?></td>
                                            <!-- <td><?php echo $value->ShippingStatus;?></td> -->
                                            <!-- <td><?php echo number_format($value->TotalAmt,1,".","");?></td> -->
                                           <!--  <td><?php $ordamts=number_format($current_amt[$j]+number_format($value->ShippingAmt,1,".","")-number_format($value->CouponAmt,1,".","")-number_format($value->usedwalletamt,1,".",""),1,".","");
                                                  if($ordamts==0.0){ 
                                                    echo number_format($current_amt[$j]+number_format($value->ShippingAmt,1,".","")-number_format($value->CouponAmt,1,".",""),1,".","");
                                                  } else {
                                                    //echo $ordamts;
                                                    echo number_format($current_amt[$j]+number_format($value->ShippingAmt,1,".","")-number_format($value->CouponAmt,1,".",""),1,".","");
                                                  }
                                                ?></td> -->
                                            <td><?php echo $value->orderprocessdate;?></td>
<!--                                             <td><?php echo $value->vendorname;?></td>
 -->                                            <td>
                                            <?php if (substr($value->Remark, 0, 17)=="Order Refund Case") { ?>
                                              <a href="<?php echo base_url().'admin/order/ordreturndetailprocess/'.$value->OrderId;?>?ordcase=Order Refund Case"><i class="fa fa-eye"></i></a>  
                                            <?php } else { ?>
                                              <a href="<?php echo base_url().'admin/order/ordreturndetailprocess/'.$value->OrderId;?>?ordcase=Order Replacement Case"><i class="fa fa-eye"></i></a>
                                            <?php } ?>
                                            </td>
                                            
                                        </tr>
                                        <?php $j++; } }else {?>
                                      <tr><td colspan="10">Record Not Found</td></tr>
                                      <?php } ?>
                                    </tbody>
                                    
                                </table>
                               <!--  <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div> -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>