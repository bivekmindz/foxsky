<?php //p($orddetail);exit;?>  
  <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            pp_paymode : "required",
            pp_partamt : "required",
        },
        // Specify the validation error messages
        messages: {
            pp_paymode: "Payment Type is required",
            pp_partamt: "Amount is required",
        },
        submitHandler: function(form) {
            var pp_partamt=parseFloat($('#pp_partamt').val());
            var remordamt=parseFloat($('#remordamt').val());
            if(pp_partamt > remordamt){
                alert('Payment amount is not greater than Remaining amount!');
            } else {
                form.submit();
            }
           
        }
    });

  });
  
  </script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Partial Payment</h2>
                    </div>
                    <div class="page_box">
                      <div class="sep_box">
                        <div class="col-lg-12">
                        <?php foreach ($ordpartpaydetail as $key => $value) {
                          $rempartamt+=$value->pp_ammount;
                        }  
                        if($rempartamt==''){ ?>
                        <h3 style="float:right;width:auto;">Remaining Balance :- Rs. <?php echo number_format($current_amt,1,".",""); ?></h3>
                        <?php } else { ?>
                        <h3 style="float:right;width:auto;">Remaining Balance :- Rs. <?php echo number_format(number_format($current_amt,1,".","")-number_format($rempartamt,1,".",""),1,".",""); ?></h3>
                        <?php } ?>
                        <h3 style="float:left;width:auto;">Order Amount :- Rs. <?php echo number_format($current_amt,1,".",""); ?></h3>
                     <!--  session flash message  -->
                    <div class='flashmsg'>
                        <?php echo validation_errors(); ?> 
                        <?php
                          if($this->session->flashdata('message')){
                            echo $this->session->flashdata('message'); 
                          }
                        ?>
                    </div>
                    </div></div>
                    <form action="" id="addCont" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="remordamt" value='<?php echo number_format(number_format($current_amt,1,".","")-number_format($rempartamt,1,".",""),1,".",""); ?>' >
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Order ID</div>
                                    </div>
                                    <div class="col-lg-8">
                                       <div class="tbl_input">
                                        <input type="text" name="pp_ordid" id="pp_ordid" value="<?php echo $orddetail->OrderNumber;?>" readonly /> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Customer Name</div>
                                    </div>
                                    <div class="col-lg-8">
                                       <div class="tbl_input">
                                        <input type="text" name="pp_cusname" id="pp_cusname" value="<?php echo $orddetail->ShippingName;?>" readonly /> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            </div>
                        
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Payment Type</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                          <select name="pp_paymode" id="pp_paymode" onchange="chmodepay()" >
                                            <option value="">Select Type</option>
                                            <option value="CASH">CASH</option>
                                            <option value="CHEQUE">CHEQUE</option>
                                          </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div id="chequemode" style="display: none;">
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Cheque No.</div>
                                    </div>
                                    <div class="col-lg-8">
                                       <div class="tbl_input">
                                        <input type="text" name="pp_chno" id="pp_chno" /> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Bank Name</div>
                                    </div>
                                    <div class="col-lg-8">
                                       <div class="tbl_input">
                                        <input type="text" name="pp_chbankname" id="pp_chbankname" /> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Account No.</div>
                                    </div>
                                    <div class="col-lg-8">
                                       <div class="tbl_input">
                                        <input type="text" name="pp_chacno" id="pp_chacno" /> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Date of Cheque</div>
                                    </div>
                                    <div class="col-lg-8">
                                       <div class="tbl_input">
                                        <input type="date" name="pp_chdate" id="pp_chdate" /> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div>

                        

        
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Amount</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input type="text" name="pp_partamt" id="pp_partamt" onKeyPress="return isNumberKey(event);"/></div>
                                    </div>
                                </div>
                            </div>

         
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <!-- <input id="Submit1" name="submit" type="submit" value="Submit" class="btn_button sub_btn" onclick="return validate16();" /> -->
                                            <input id="Submit1" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   </form>
                </div>
            </div>
        </div>
    </div>
    </div>

<script type="text/javascript">

function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode;
if (charCode != 46 && charCode > 31 
&& (charCode < 48 || charCode > 57))
return false;

return true;
}


function chmodepay(){
 var pp_paymode=$('#pp_paymode').val();
 if($.trim(pp_paymode)=='CHEQUE'){
    $('#chequemode').css('display','block');
 } else {
    $('#chequemode').css('display','none');
 }
}

</script>
    