<?php //p($newsview);exit;?>
<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>User Order Payment details</h2>
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can view the User Order Payment details.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">

                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                                <?php foreach ($ordpartpaydetail as $key => $value) {
                                      $rempartamt+=$value->pp_ammount;
                                    }  
                                    if($rempartamt==''){ ?>
                                    <h3 style="float:right;width:auto;">Remaining Balance :- Rs. <?php echo number_format($orddetail->TotalAmt,1,".",""); ?></h3>
                                    <?php } else { ?>
                                    <h3 style="float:right;width:auto;">Remaining Balance :- Rs. <?php echo number_format(number_format($orddetail->TotalAmt,1,".","")-number_format($rempartamt,1,".",""),1,".",""); ?></h3>
                                    <?php } ?>
                                    <div style="float:left; width:auto; line-height: 30px;">
                                <p><b>Name :- </b><?php echo $orddetail->ShippingName;?></p>
                                <p><b>Order Number :- </b><?php echo $orddetail->OrderNumber;?></p>
                                <p><b>Order Amount :- Rs. </b><?php echo number_format($orddetail->TotalAmt,1,".","");?></p>
                                </div>
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S.NO.</th>
                                            <th>Payment Type</th>
                                            <th>Paid Amount</th>
                                            <th>Cheque No.</th>
                                            <th>Cheque Account No.</th>
                                            <th>Cheque Bank Name</th>
                                            <th>Cheque Date</th>
                                            <th>Created On</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                    foreach ($ordpartpaydetail as $key => $value) { $i++;?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->pp_paymentmode; ?></td>
                                            <td><?php echo $value->pp_ammount; ?></td>
                                            <?php if(!empty($value->partialpaymapid)){ ?>
                                            <td><?php echo $value->cheque_no; ?></td>
                                            <td><?php echo $value->account_number; ?></td>
                                            <td><?php echo $value->bank_name; ?></td>
                                            <td><?php echo $value->date_ofcheque; ?></td>
                                            <?php } else { ?>
                                            <td> - </td><td> - </td><td> - </td><td> - </td>
                                            <?php } ?>
                                            <td><?php echo $value->pp_createdon; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>