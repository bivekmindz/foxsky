﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <style type="text/css">
        .select2-container {
        box-sizing: border-box;
        display: inline-block;
        margin: 0px 5px 0px 0px;
        position: relative;
        vertical-align: middle;
        float: left;
        }
        .hide {
        display: none !important;
        }
    </style>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <!-- for select2 js -->
     <link href="<?php echo base_url(); ?>assets/js/select/dist/css/select2.min.css" rel="stylesheet" />
     <script src="<?php echo base_url(); ?>assets/js/select/dist/js/select2.min.js"></script>
    <!-- for select2 js -->
    <script type="text/javascript">
       var site_url="<?php echo base_url(); ?>";
    </script>
</head>
<body>
 <button onclick="window.print();" style="background:#0370cc;color:#fff;border:none;padding:4px 7px;cursor:pointer;">Print</button>
    <div id="inv_print"style="float:left;width:800px;height:auto;border:dashed 1px #ccc;padding:10px 10px;font-family:arial, helvetica, sans-serif;">
        <div style="float:left;width:100%;text-align:center;">
            <img src="http://urbanlibas.com/bizzgain/images/logo3.png">
        </div>
        <div style="float:left;width:100%;text-align:center;background:#F1F1F1;padding:8px 0px;font-family:Arial, Helvetica, sans-serif;color: #4FAB29;margin:5px 0px;">
            Your order has been placed
        </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;"><b>Dear</b> <span style="color:#4FAB29;"><?php echo ucwords($record->ShippingName);?></span> </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;">Thanks for placing your order at <b>MindzShop.com</b> The following are your order details: </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;"><b>Order Number:</b> <span style="color:#4FAB29;"><?php echo $record->OrderNumber;?></span> </div>
        <div style="float:left;width:100%;margin-top:5px;font-size:13px;"><b>Shipping Address :</b> <span style="color:#4FAB29;"><?php echo $record->ShippingAddress;?></span> </div>
        <div style="float:left;width:100%;margin-top:5px;font-size:13px;"><b>Payment Method:</b> <span style="color:#4FAB29;"><?php echo $record->PaymentMode;?></span> </div>
        <table style="border-collapse:collapse;margin:0px;padding:0px;float:left;width:100%;font-size:13px;margin:15px 0px;">
		<tr style="background: #058ED0;color: #fff;"><th style="padding:7px 0px;width:50px;">S.No.</th><th style="padding:7px 0px;">Skunumber</th><th style="padding:7px 0px;">Pro. image</th><th style="padding:7px 0px;">Pro. Name</th><th style="padding:7px 0px;">Qty.</th><th style="padding:7px 0px;">Size</th><th style="padding:7px 0px;">Color</th><th style="padding:7px 0px;">VAT/TIN Tax</th><th style="padding:7px 0px;">CST Tax</th><th style="padding:7px 0px;">Entry/Octori Tax</th><th style="padding:7px 0px;">Unit Price</th><th style="padding:7px 0px;">Amount</th><th style="padding:7px 0px;">Remove</th></tr>
    <?php 
    $i=1;
    if(isset($detialrecord) && count($detialrecord)>0){
       foreach ($detialrecord as $value) {
       $row_id=$value->OrderproId;
       $pro_id=$value->ProId;

    ?>
        <tr id="<?php echo $row_id.','.$pro_id; ?>" style="background:#<?php echo ($i%2==0)?'fff':'ccc';?>;">
        <td style="text-align:center;padding:4px 3px;"><?php echo $i++; ?></td>
        <td style="text-align:center;padding:4px 3px;"><?php echo $value->SkuNumber;?></td>
        <td style="text-align:center;padding:4px 3px;"><img src="<?php echo $value->ProImage;?>" style="vertical-align:middle; width:30px;"></td>
        <td style="text-align:center;padding:4px 3px;"><?php echo $value->ProName;?></td>
        <td style="text-align:center;padding:4px 3px;">
                       <input type="hidden" id="stateid" value="<?=$value->State?>">
                       <input type="hidden" id="cstid" value="<?=$value->cstid?>">
                       <input type="hidden" id="tinid" value="<?=$value->tinid?>">
                        <input type="number" ordproid="<?=$value->OrderproId?>" ordid="<?=$value->OrderId?>" catid="<?=$value->CatId?>" brandid="<?=$value->brandId?>" cityid="<?=$value->citygroupid?>" proid="<?=$value->ProId?>" onkeyup="return updateqty($(this))" onchange="return updateqty($(this))" value="<?php echo $value->OrdQty;?>" class="update_product11" name="OrdQty[<?=$value->OrderproId?>]" id="OrdQty<?=$value->OrderproId?>" style="width:50px;"></td>
        <td style="text-align:center;padding:4px 3px;"><?php echo ($value->OrdSize!="")?$value->OrdSize:'--'?></td>
        <td style="text-align:center;padding:4px 3px;"><span style="width:15px;height:15px;background:#<?php echo $value->OrdColor; ?>;display:block;margin:auto;"></span><?php echo $value->color_name; ?></td>
        <td style="text-align:center;padding:4px 3px;"><?php echo number_format(($value->protaxvat),2);?></td>
        <td style="text-align:center;padding:4px 3px;"><?php echo number_format(($value->protaxcst),2);?></td>
        <td style="text-align:center;padding:4px 3px;"><?php echo number_format(($value->protaxentry),2);?></td>
        <td style="text-align:center;padding:4px 3px;"><input type="text" ordproid="<?=$value->OrderproId?>"  ordid="<?=$value->OrderId?>" catid="<?=$value->CatId?>" brandid="<?=$value->brandId?>" cityid="<?=$value->citygroupid?>" proid="<?=$value->ProId?>" onchange="return updatepri($(this))" value="<?php echo $value->VenSellPrice;?>" id="unitprice<?=$value->OrderproId?>"  name="unitprice[<?=$value->OrderproId?>]" style="width:50px;"></td>
        <td style="text-align:center;padding:4px 3px;"><?php echo number_format(($value->VenSellPrice * $value->OrdQty),2);?></td>
        <td style="text-align:center;padding:4px 3px;"><a href="javascript:void(0);" onclick="delete_prod(<?php echo $row_id;?>,<?php echo (($value->TotalAmt) - ($value->VenSellPrice * $value->OrdQty));?>);"style="text-decoration:none;color:#f00;">X</a></td>
        </tr>
<?php }} ?>
<tr><td colspan="9" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"></td>
<td colspan="2" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 0px #ccc;"><b>Sub Total :</b></td>
<td colspan="2" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo number_format($value->subtotal,2) ;?></td>
<tr><td colspan="9" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"></td>
<td colspan="2" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 0px #ccc;"><b>Shipping :</b></td>
<td colspan="2" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo number_format($value->ShippingAmt,2); ?></td>
<tr><td colspan="9" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"></td>
<td colspan="2" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 0px #ccc;"><b>Tax :</b></td>
<td colspan="2" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo number_format($value->Tax,2); ?></td>
<tr><td colspan="9" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"></td>
<td colspan="2" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 0px #ccc;"><b>Final Total :</b></td>
<td colspan="2" style="text-align:right;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo number_format($value->TotalAmt,2); ?></td>

            </table>

        <div style="padding:5px 0px;float:left;width:100%;">
        <button id="add_more" style="background:#0370cc;color:#fff;border:none;padding:4px 7px;cursor:pointer;">Add More Product</button></div>
        <div id="pro_serch_div" class="hide" style="padding:5px 0px;float:left;width:100%;">
            <form id="add_pro_form">
                <input type="hidden" name="city" value="<?php echo $value->BillCity; ?>">
                <input type="hidden" name="state" value="<?php echo $value->BillState; ?>">
                 
                <select name="prod" id="prod" onchange="get_color(this.value);" style="width:120px;padding:3px 4px;float:left;margin-right:5px;">
                <?php if(!empty($product_list))
                { 
                    echo "<option value=''></option>";
                    foreach ($product_list as $value) {
                    echo "<option value='".$value->proid."'>".$value->proname."</option>";
                }

                } ?>

                </select>
                <div id="pro_feature_div" class="hide">
                    <select id="color" name="color" onchange="get_color_age(this.value);" class="hide" style="width:120px;padding:3px 4px;float:left;margin-right:5px;">
                    </select>
                    <select id="pro_size" name="pro_size" class="hide" style="width:120px;padding:3px 4px;float:left;margin-right:5px;">
                    </select>
                    <input id="proqty" name="proqty" style="padding:3px 5px;float:left;width:100px;margin-right:5px;"/>
                </div>
                
                <input type="button" id="add_pro_sub" value="Add" style="background:#0370cc;color:#fff;border:none;padding:5px 7px 4px;cursor:pointer;">
            </form>
        </div>

            <div style="float:left;width:100%;font-size:12px;line-height:20px;padding:5px 0px;">For any queries please call us on <b>+91 0000000000</b> or mail us on <b>support@mindzshop.com</b></div>
            <div style="float:left;width:100%;font-size:13px;line-height:20px;font-weight:bold;padding-top:20px;">
                Regards,<br />
                MindzShop Team
            </div>

        </div>

        
<script type="text/javascript">

$('#add_pro_sub').on("click",function(){
  var err=0;
  if($('#prod').val()=="")
  { 
    err++;
    $('#prod').css("border-color","red");
  }
  if($('#color').attr('class')=="")
  { 
    if($('#color').val()=="")
    {
      err++;
     $('#color').css("border-color","red");
    }else{
      $('#color').css("border-color","");
    }
    
  }
  if($('#pro_size').attr('class')=="")
  { 
    if($('#pro_size').val()=="")
    {
      err++;
     $('#pro_size').css("border-color","red");
    }else{
      $('#pro_size').css("border-color","");
    }
  }
    if($('#proqty').val()=="" || isNaN($('#proqty').val())==true || $('#proqty').val()<1)
    {
      err++;
     $('#proqty').css("border-color","red");
    }else{
      $('#proqty').css("border-color","");
    }
  
  if(err==0)
  { 
    var order_id="<?php echo  $record->OrderId; ?>";
    var formdata=$("#add_pro_form").serialize();
    
    $.ajax({
      type:"post",
      url:site_url+"admin/order/add_product/"+order_id,
      data:formdata,
      success:function(rep)
      {
         //alert(rep);
         window.location.href=window.location.href;
      }

    });
  }
  
  
})

    $('#add_more').on("click",function(){

   //$( this ).text('Remove');
   //$(this).attr("id","remove")
    $('#pro_serch_div').removeClass('hide');
     $("#prod").select2({
      placeholder: "Select Product",
      allowClear: true
    });
  
 });

function get_color(val)
{
   $.ajax({
                type:"post",
                url:site_url+"admin/order/get_color/"+val,
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data.trim());
                    if(data.trim()==0)
                    { 
                      
                      $('#pro_feature_div').removeClass('hide');
                      $('#color').addClass('hide');
                      $('#pro_size').addClass('hide');
                    }else{
                      $('#pro_feature_div').removeClass('hide');
                      $('#color').removeClass('hide');
                      $('#color').html(data);
                      $('#pro_size').addClass('hide');
                      
                      
                    }
                    
                }
        });
}
function get_color_age(val)
{

  $.ajax({
                type:"post",
                url:site_url+"admin/order/get_color_age/"+val.split(',')[1],
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data.trim());
                    if(data.trim()==0)
                    { 
                      $('#pro_size').addClass('hide');
                    }else{
                       $('#pro_size').removeClass('hide');
                       $('#pro_size').html(data.split('#')[1]);
                    }
                    
                }
        });
}
$('.update_product' ).on("keyup",function() {
   var val = this.value; 
   var name = this.name;
   var State_id="<?php echo $record->BillState;?>";
   var city="<?php echo $record->BillCity;?>";
   var order_id="<?php echo  $record->OrderId; ?>";
   var url="<?php echo base_url(); ?>";
   var pro_id = $(this).closest('tr').attr('id').split(',');
      if(val.search(/\S/) == -1 || val==0)
   {
    alert('value can not be blank or Zero');
   }
   else if(isNaN(val)==true)
    {
       alert('Value should be integer ');
       window.location.href=window.location.href;
    }else{
    
        $.ajax({
                type:"post",
                url:url+"admin/order/update_product_val",
                data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id,city_vl:city},
                success:function(data){
                   // alert(data);
                    window.location.href=window.location.href;
                }
        });
   }
   
   
 });
function delete_prod(id,pz)
 {  


   var order_id="<?php echo  $record->OrderId; ?>";
    $.ajax({
                type:"post",
                url:site_url+"admin/order/delete_product/"+id+"/"+pz+"/"+order_id,
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data);
                    window.location.href=window.location.href;
                }
        });
 }
 /*function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
}*/
</script>
<script type="text/javascript">
function updateqty(val){
var qnty       = val.val();
var productid  = val.attr('proid');
var cityid  = val.attr('cityid');
var catid   =val.attr('catid');
var brndid  =val.attr('brandid');
var stateid =$('#stateid').val();
var cstid =$('#cstid').val();
var tinid =$('#tinid').val();
var ordid =val.attr('ordid');
var ordproid =val.attr('ordproid');
var unitprc    = $('#unitprice'+ordproid).val();
//alert(unitprc);
//alert(qnty);return false;
$("#qtymsg").hide();
if($.isNumeric(qnty)==false){
 $("#qtymsg").show();
 $("#qtymsg").html('<span class="qtyinvalid">Invalid quantity entered!</span>');
 return false;  
}
$.ajax({
   url:'<?php echo base_url();?>admin/order/updateorderqty',
   type: 'POST',
   data: {qnty:qnty,proid:productid,cityid:cityid,catid:catid,brndid:brndid,stateid:stateid,cstid:cstid,tinid:tinid,ordid:ordid,ordproid:ordproid,unitprc:unitprc},
   success:function(data){
    console.log(data);
    //alert(data);return false;
    window.location.href=window.location.href;
    return false;
    }
  });
}


function updatepri(val){
var unitprc    = val.val();
var productid  = val.attr('proid');
var cityid  = val.attr('cityid');
var catid   =val.attr('catid');
var brndid  =val.attr('brandid');
var stateid =$('#stateid').val();
var cstid =$('#cstid').val();
var tinid =$('#tinid').val();
var ordid =val.attr('ordid');
var ordproid =val.attr('ordproid');
var qnty      = $('#OrdQty'+ordproid).val();
//alert(qnty);
//alert(unitprc); return false;

$("#qtymsg").hide();
if($.isNumeric(qnty)==false){
 $("#qtymsg").show();
 $("#qtymsg").html('<span class="qtyinvalid">Invalid quantity entered!</span>');
 return false;  
}
$.ajax({
   url:'<?php echo base_url();?>admin/order/updateorderqty',
   type: 'POST',
   data: {qnty:qnty,proid:productid,cityid:cityid,catid:catid,brndid:brndid,stateid:stateid,cstid:cstid,tinid:tinid,ordid:ordid,ordproid:ordproid,unitprc:unitprc},
   success:function(data){
    //console.log(data);
    //alert(data);return false;
    window.location.href=window.location.href;
    return false;
    }
  });

}
</script>
</body>
</html>
