<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage Return Processing Order details</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view single Return Processing order details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                              <!-- <h3 style="float:right;width:auto;">Order Number :- <?php echo $pendetailview[0]->OrderNumber; ?></h3>
                              <div style="float:left;width:auto;">
                                <select style="margin-right: 20px;padding:3px 5px;" id="chstatus" name="chstatus">
                                          <option value="">select status</option>
                                          <option value="return">Return Complete</option>
                                          <option value="return cancel">Return Cancel</option>
                                        </select>
                                        <input type="button" name="submitmanu" id="submitmanu" value="Submit" class="btn_button sub_btn" style="border:none;padding:4px 10px;"/>
                                        <i class="fa fa-spinner fa-spin hide" style="font-size: 21px;"></i>
                              </div> -->
                             <!--  <div style="font-size: 14px;margin: 10px 0px 10px 0px;color: green;" class="col-lg-12"><span style="font-size: 16px;font-weight: bold; color: #383338" >REMARK :- </span><?php echo $pendetailview[0]->Remark; ?></div> -->
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                <form action="" method="post" id="form_id">
                              <h3 style="float:right;width:auto;">Order Number :- <?php echo $pendetailview[0]->OrderNumber; ?></h3>
                              <div style="float:left;width:auto;">
                                <select style="margin-right: 20px;padding:3px 5px;" id="chstatus" name="chstatus">
                                          <option value="">select status</option>
                                          <option value="return ">Return Complete</option>
                                          <option value="return cancel">Return Cancel</option>
                                        </select>
                                        <input type="button" name="submitmanu" id="submitmanu" value="Submit" class="btn_button sub_btn" style="border:none;padding:4px 10px;"/>
                                        <i class="fa fa-spinner fa-spin hide" style="font-size: 21px;"></i>
                              </div> 
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                  <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                        <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span></th> 
                                          <th>S.NO.</th>
                                          <th>Order Status</th>
                                         
                                          <th >Product Image</th>
                                          <th colspan="6">Product Name</th>
                                          <th>Quantity</th>
                                         
                                          <th>MRP</th>
                                          <th>Unit Price</th>
                                          <th colspan="5">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=0; foreach ($pendetailview as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><input type='checkbox' name='proApp' class='chkApp<?=$value->OrderId?>' value='<?=$value->StatusOrderId?>'></td>
                                            <td><?php echo $i;?></td>
                                            <td id="status<?=$value->StatusOrderId?>"><?php echo $value->ShipStatus;?></td>
                                            <!-- <td><?php echo $value->firstname." ".$value->lastname;?></td>
                                            <td><?php echo $value->companyname;?></td>
                                            <td><?php echo $value->SkuNumber;?></td> -->
                                            <td><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value->ProImage;?>" style="vertical-align:middle; width:80px;"></td>
                                            <td colspan="6"><?php echo $value->ProName;?></td>
                                            <td ><?php echo $value->OrdQty;?>
                                            <!-- <td id="qty<?=$value->StatusOrderId?>"><?php echo $value->ProQty;?> -->
                                               <input type="hidden" id="stateid" value="<?=$value->State?>">
                                               <input type="hidden" id="cstid" value="<?=$value->cstid?>">
                                               <input type="hidden" id="tinid" value="<?=$value->tinid?>">
                                            <input type="hidden" ordproid="<?=$value->StatusOrderId?>" ordid="<?=$value->OrderId?>" catid="<?=$value->CatId?>" brandid="<?=$value->brandId?>" cityid="<?=$value->citygroupid?>" proid="<?=$value->ProId?>" value="<?php echo $value->OrdQty;?>" class="update_product11" name="OrdQty" id="qty<?=$value->StatusOrderId?>" style="width:50px;" min="1" oldqy="<?php echo $value->OrdQty;?>" ordstatus="<?=$value->ShipStatus?>" stock="<?php echo $value->pomq; ?>">
                                            </td>
                                           <!--  <td><?php echo $value->OrdColor;?></td> -->
                                           <!--  <td><?php echo $value->OrdSize;?></td>
                                            <td>
                                            <?php if($value->OrdColor=='multicolor'){
                                            $urlmulti=base_url().'images/imgpsh_fullsize.jpg'; 
                                            $OrdColor='#'.$value->OrdColor; echo '<font style="background-image:url('.$urlmulti.');background-size: cover;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br>'.$value->color_name;
                                             } else {  
                                              $OrdColor='#'.$value->OrdColor; echo '<font style="background-color:'.$OrdColor.'";>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br>'.$value->color_name;
                                             } ?>
                                            </td> -->
                                            <?php $amt=number_format($value->base_prize*$value->ProQty,1,".",""); ?>
                                           <!--  <td><?php echo $tin_tax_amt=number_format($amt * $value->protaxvat_p / 100,1,".","");?></td>
                                            <td><?php echo $cst_tax_amt=number_format($amt * $value->protaxcst_p / 100,1,".","");?></td>
                                            <td><?php echo $entry_tax_amt=number_format($amt * $value->protaxentry_p / 100,1,".","");?></td> -->
                                             <?php $tin_tax_amt=number_format($amt * $value->protaxvat_p / 100,1,".","");
                                             $cst_tax_amt=number_format($amt * $value->protaxcst_p / 100,1,".","");
                                             $entry_tax_amt=number_format($amt * $value->protaxentry_p / 100,1,".","");?></td>
                                            <td><?php echo number_format($value->ProPrice,1,".","");?></td>
                                            <td><?php echo number_format($value->base_prize,1,".","");?></td>
                                            <td colspan="5"><?php echo number_format($value->base_prize*$value->ProQty,1,".","");?></td>
                                            <?php 
                                              $t_subtotal[]=number_format($value->base_prize*$value->ProQty,1,".","");
                                              $t_tax[]=number_format($tin_tax_amt+$cst_tax_amt+$entry_tax_amt,1,".","");
                                              $totproprice=$amt+$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
                                             
                                            ?>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                        <?php
                                            $ship=number_format($value->ShippingAmt,1,".","");
                                            $to_subtotal=array_sum($t_subtotal);
                                            $to_tax=array_sum($t_tax);
                                            //$to_coup=number_format(array_sum($t_coup),1,".","");
                                            //$to_wall=number_format(array_sum($t_wall),1,".","");
                                            $to_coup=number_format($value->CouponAmt,1,".","");
                                            $to_wall=number_format($value->usedwalletamt,1,".","");

                                        ?>
                                      <td colspan="8">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Sub Total :</b></td>
                                      <td colspan="4"><?php echo number_format($to_subtotal,1,".","");?></td></tr>
                                     <!--  <tr><td colspan="8">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Shipping :</b></td><td colspan="4"><?php echo $ship;?></td></tr>
                                      <tr><td colspan="8">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Tax :</b></td>
                                      <td colspan="4"><?php echo number_format($to_tax,1,".",""); ?></td></tr>
                                      <?php if(!empty($value->CouponAmt)){ ?>
                                      <tr><td colspan="8">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Coupon Amount (-) :</b></td>
                                      <td colspan="4"><?php echo $to_coup; ?></td></tr>
                                      <?php } ?>
                                      <?php if(!empty($value->usedwalletamt) && $value->usedwalletamt!=0.0){ ?>
                                      <tr><td colspan="8">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Wallet Pay (-) :</b></td>
                                      <td colspan="4"><?php echo $to_wall; ?></td></tr>
                                      <?php } ?> -->
                                      <tr><td colspan="8">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Final Total :</b></td><td colspan="4"><?php $ordamts=number_format(number_format($to_subtotal,1,".","")+$ship+number_format($to_tax,1,".","")-$to_coup-$to_wall,1,".","");
                                      if($ordamts<=0.0){
                                        echo number_format(number_format($to_subtotal,1,".","")+$ship+number_format($to_tax,1,".","")-$to_coup,1,".","");
                                        ?>
                                        <input type="hidden" id="returnordamt" name="returnordamt" value='<?php echo number_format(number_format($to_subtotal,1,".","")+$ship+number_format($to_tax,1,".","")-$to_coup,1,".",""); ?>'>
                                        <?php
                                         } else {
                                        echo $ordamts;
                                        ?>
                                        <input type="hidden" id="returnordamt" name="returnordamt" value="<?php echo $ordamts; ?>">
                                        <?php } ?></td></tr>
                                    </tbody>
                                    
                                </table>
                                </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
      var ordid="<?php echo $pendetailview[0]->OrderId; ?>";
        if(this.checked) { // check select status
            $('.chkApp'+ordid).each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp'+ordid).each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
    
});


        $(document).ready(function(){
 $('#submitmanu').click(function(){
     var ordid="<?php echo $pendetailview[0]->OrderId; ?>";
     var ttl_check=0;
     var ttl_uncheck=0;
    $('.chkApp'+ordid).each(function()
    { 
        ttl_check++;
        if($(this).prop("checked") == false)
        {
           ttl_uncheck++;
        }
        
    })
    if(ttl_check==ttl_uncheck)
    {
        alert('Please check atleast one Product');
    }else if($('#chstatus').val()==""){
      $('#chstatus').css('border-color','red');
         
    }else{
             $('#submitmanu').hide();
            $('.fa-spin').removeClass('hide');  
         $('.chkApp'+ordid).each(function(){ 
                    
                        if($(this).prop("checked") == true)
                        {
                           var id=$(this).attr('value');
                           var current_qty=$('#qty'+id).html();
                           var old_qty=$('#qty'+id).html();
                           var old_stat=$('#status'+id).html();
                           var total_stock='';
                           
                           $.ajax({ 
                            type:'post',
                            url:'<?php echo base_url()?>admin/order/uptodate_order_satatus',
                            data:{'statsid':id,'flag':$('#chstatus').val(),'cur_qty':current_qty,'old_qty':old_qty,'old_stat':old_stat,'total_stock':total_stock},
                            success:function(rep)
                            {
                                //alert(rep);
                                //console.log(rep);
                                //return false;
                               document.location.href=window.location.href;
                            }

                           });
                        }

                    }) 
    }
  
  })

});
    </script>