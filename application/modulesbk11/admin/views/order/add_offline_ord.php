<div class="wrapper">
<?php $this->load->view('helper/nav');?>
<script>
$(document).ready(function(){
  $(".vend_typ_sel").select2({
  placeholder: "Select a Vendor Type",
  allowClear: true
})
  $(".vend_list").select2({
  placeholder: "Select a Vendor List",
  allowClear: true
})
  $(".product_list").select2({
  placeholder: "Select a Product",
  allowClear: true
})
  $(".payment_list").select2({
  placeholder: "Select",
  allowClear: true
})

});
  
</script> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
        
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add offline order</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can make order .</p>
                        </div>
                    </div>
                    

      

   <!--  product deatisl div section start -->
   <div class="page_box">
    <div class="sep_box">
      <div class="col-lg-12">
        
        <input type="hidden" id="ship_adddr_id" name="ship_adddr_id">
          
          <?php
          if($this->session->flashdata('success')){
          echo "<div class='succ_msg'>".$this->session->flashdata('success')."</div>"; 
          }elseif($this->session->flashdata('error')){
            echo "<div class='err_msg'>".$this->session->flashdata('error')."</div>"; 
          }
          ?>
          
        <div style="width:100%;overflow-x:auto;">

          <table class="grid_tbl" id="offline_tbl">
            <thead>
              <tr>
                <th>S.NO.</th>
                <th>SKUNumber</th>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Age</th>
                <th>Color</th>
                <th>VAT/TIN Tax</th>
                <th>CST Tax</th>
                <th>Entry/<br>Octori<br>Tax</th>
                <th>Unit Price</th>
                <th>Amount</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
                             
                </tbody>

              </table>


            </div>
            
          </div>
        </div>
        <div class="sep_box">
      <div class="col-lg-12">
          <div style="width:100%;overflow-x:auto;">

            <table class="grid_tbl" id="price_details">
              <tr>
                <td>Sub total</td><td id="sub">0</td>
                <td>Tax</td><td id="tax" tcst="0" ttin="0" tenrty="0">0</td>
                <td>Shipping Charge</td><td id="charg">0</td>
                <td>Final total</td><td id="final">0</td>
              </tr>
            </table>


            </div>
           
          </div>
        </div>

      </div>
    <!--  product deatisl div section end -->
    <form id="add_pro_form">
                         <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Vendor Type <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="v_type" id="v_type" onchange="get_vendor(this.value);" class="">
                                                <option value="">Select a Vendor Type</option>
                                                <?php foreach ($vendor_type as $value) { 
                                                  echo '<option value="'.$value->vendortype.'">'.ucfirst($value->vendortype).'</option>';
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Vendor List <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="v_list" id="v_list" onchange="open_address(this.value);remove_table_data();" class="">
                                                <option value="">Select a Vendor List</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
    <!--  add more product section -->
                                        
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Product <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="prod" id="prod" onchange="get_color(this.value);get_img(this.value);" class="product_list">
                                                <?php if(!empty($product_list))
                                                { 
                                                echo "<option value=''></option>";
                                                foreach ($product_list as $value) {
                                                echo "<option value='".$value->proid."'>".$value->proname."</option>";
                                                }

                                                } ?>
                       
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text"></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                           <div id="pro_img_data" class="hide"></div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                       
                          

                          <div id="pro_feature_div" class="hide">
                            <select id="color" name="color" onchange="get_color_age(this.value);" class="hide">
                              <option value="">Choose Color</option>
                            </select>
                            <select id="pro_size" name="pro_size" class="hide">
                             <option value="">Choose Age</option>
                            </select>
                            
                            <input type="number" id="proqty" name="proqty" min="1" value="1">
                            <input type="button" id="add_pro_sub" value="Add">
                          
                          </div>
                       

                   
                    
                  <!--  add more product section -->

                  <!-- ------------------------------------payment section start -------------->
                   <div class="sep_box">
                    <div class="col-lg-6">
                      <div class="row">
                        <div class="col-lg-4">
                          <div class="tbl_text">Payment Mode <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                          <div class="tbl_input">
                          <select id="payment_mode" name="payment_mode" onchange="get_cheque_form(this.value);" class="">
                              <option value="">Select</option>
                              <option value="COD">COD</option>
                              <option value="CHEQUE">CHEQUE</option>
                              <option value="CREDIT">CREDIT</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="row">
                        <div class="col-lg-4">
                          <div class="tbl_text">Remarks</div>
                        </div>
                        <div class="col-lg-8">
                          <div class="tbl_input">
                           <textarea name="remrk" id="remrk"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!---------------------------------------------  pament section end -->

            
<!-- ---------------------------------------------------checque div start -->
<div class="col-lg-12 col-md-12 hide" id="cheque_pay">

  <div class="panyment_main">
    <div class="col-lg-6 col-md-6">
      <div class="form-group">
        <label for="usr" style="font-weight:normal;">Cheque No :</label>
        <input type="text" name="chequeno" class="form-control" id="chequeno">
      </div>

      <div class="form-group">
        <label for="usr" style="font-weight:normal;">Bank Name :</label>
        <input type="text" name="bankname" class="form-control" id="bankname">
      </div>


      <div class="form-group">
        <label for="usr" style="font-weight:normal;">Account No :</label>
        <input type="text" name="accno" class="form-control" id="accno">
      </div>

      <div class="form-group">
        <label for="pwd" style="font-weight:normal;">Amount :</label>
        <input type="text" name="chequeamount" class="form-control" id="chequeamount">
      </div>
    </div>


    <div class="col-lg-6 col-md-6">
      <div class="form-group">
        <label for="usr" style="font-weight:normal;">IFSC Code :</label>
        <input type="text" name="ifsccode" class="form-control" id="ifsccode">
      </div>

      <div class="form-group">
        <label for="usr" style="font-weight:normal;">Date of Cheque :</label>
        <input type="date" name="chequedate" class="form-control" id="chequedate">
      </div>

      <div class="form-group">
        <label for="pwd" style="font-weight:normal;">Date of Cheque Collect :</label>
        <input type="date" name="chequecollectdate" class="form-control" id="chequecollectdate">
      </div>

      <div class="form-group">
        <label for="pwd" style="color:#FFFFFF;">.</label><br>
        <input type="hidden" name="ch_copyshipdetid" id="ch_copyshipdetid" value="141">
        <span class="loadingBtn" style="display:none; width: 96%; margin-left: 0;" id="dlbcheque"><i class="fa fa-spinner fa-spin"></i></span>

      </div>

    </div>
  </div> 

</div>
</form> 
<!------------------------------------------------------ cheque div end -->

  <!--  saved address start-->
                    <div class="page_box hide" id="adddetailtab"></div></div>
                     <!-- saved address end -->

<!-- ----------------------- add new address start ------------------------>   
                    <form id="ship_addr_form"><div class="page_box hide" id="ship_addr"></div></form>

                   <!-- ----------------------- add new address end ------------------------>
                   <button id="submitmanu">Add Order</button>
                   <i class="fa fa-spinner fa-spin hide" style="position: absolute;font-size: 21px;margin-left: 10px;"></i>
                </div>
            </div>
            
        </div>
    </div>
    </div>
    <div id="test"></div>
    <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
      var ordid="<?php echo $pendetailview[0]->OrderId; ?>";
        if(this.checked) { // check select status
            $('.chkApp'+ordid).each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp'+ordid).each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});


function get_color(val)
{
  $('#color').val("");
  $('#pro_size').val("");
   $.ajax({
                type:"post",
                url:site_url+"admin/order/get_color/"+val,
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data.trim());
                    if(data.trim()==0)
                    { 
                      
                      $('#pro_feature_div').removeClass('hide');
                      $('#color').addClass('hide');
                      $('#pro_size').addClass('hide');
                    }else{
                      $('#pro_feature_div').removeClass('hide');
                      $('#color').removeClass('hide');
                      $('#color').html(data);
                      $('#pro_size').addClass('hide');
                      
                      
                    }
                    
                }
        });
}

function get_img(val)
{
    $.ajax({
                type:"post",
                url:site_url+"admin/order/get_img/"+val,
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data);
                    if(data==0)
                    { 
                      $('#pro_img_data').addClass('hide');
                    }else{
                      $('#pro_img_data').removeClass('hide');
                      $('#pro_img_data').html(data);

                    }
                    
                }
        });
}

function get_color_age(val)
{

  $.ajax({
                type:"post",
                url:site_url+"admin/order/get_color_age/"+val.split(',')[1],
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data.trim());
                    if(data.trim()==0)
                    { 
                      $('#pro_size').addClass('hide');
                    }else{
                       $('#pro_size').removeClass('hide');
                       $('#pro_size').html(data.split('#')[1]);
                    }
                    
                }
        });
}
$('#add_pro_sub').on("click",function(){
   
  var err=0;
  if($('#v_type').val()=="")
  { 
    err++
    $('#v_type').css('border-color','red');
  }else{
    $('#v_type').css('border-color','');
  }
  if($('#v_list').val()=="")
  { 
    err++
    $('#v_list').css('border-color','red');
  }else{
    $('#v_list').css('border-color','');
  }
  if($('#prod').val()=="")
  { 
    err++;
    $('#prod').css("border-color","red");
  }
  if($('#color').attr('class')=="")
  { 
    if($('#color').val()=="")
    {
      err++;
     $('#color').css("border-color","red");
    }else{
      $('#color').css("border-color","");
    }
    
  }
  if($('#pro_size').attr('class')=="")
  { 
    if($('#pro_size').val()=="")
    {
      err++;
     $('#pro_size').css("border-color","red");
    }else{
      $('#pro_size').css("border-color","");
    }
  }
    if($('#proqty').val()=="" || isNaN($('#proqty').val())==true || $('#proqty').val()<1)
    {
      err++;
     $('#proqty').css("border-color","red");
    }else{
      $('#proqty').css("border-color","");
    }
  
  if(err==0)
  { 
    $("#add_pro_sub").attr("disabled","disabled");    
    $.ajax({
      type:"post",
      url:site_url+"admin/order/get_order_row/",
      data:{'vendoid':$('#v_list').val(),'productid':$('#prod').val(),'colr':$('#color').val(),'age':$('#pro_size').val(),'qty':$('#proqty').val(),'pro_img':$('#pro_img_data').find("img").attr('src'),'proname':$("#prod option:selected").html(),'ttl_row':$('.sku').length,'amount':$('#sub').text()},
      success:function(rep)
      { 
          console.log(rep); 
        //alert(parseFloat(rep.trim().split('**')[6]));
         $('#offline_tbl').append(rep.trim().split('**')[0]);
         $('#sub').html((parseFloat($('#sub').text())+parseFloat(rep.trim().split('**')[4])).toFixed(1));
         var tax=parseFloat(rep.trim().split('**')[1])+parseFloat(rep.trim().split('**')[2])+parseFloat(rep.trim().split('**')[3]);
         $('#tax').html((parseFloat($('#tax').text())+parseFloat(tax)).toFixed(1));
         $('#tax').attr('ttin',(parseFloat($('#tax').attr('ttin'))+parseFloat(rep.trim().split('**')[1])).toFixed(1));
         $('#tax').attr('tcst',(parseFloat($('#tax').attr('tcst'))+parseFloat(rep.trim().split('**')[2])).toFixed(1));
         $('#tax').attr('tenrty',(parseFloat($('#tax').attr('tenrty'))+parseFloat(rep.trim().split('**')[3])).toFixed(1));
         $('#charg').text((parseFloat($('#charg').text())+parseFloat(rep.trim().split('**')[5])).toFixed(1));
         $('#final').html((parseFloat($('#final').text())+parseFloat(rep.trim().split('**')[6])+parseFloat(tax)+parseFloat(rep.trim().split('**')[5])).toFixed(1));
         $("#add_pro_sub").attr("disabled",false);    
      }

    });
  }
  
  
})
function delete_prod(id,proid,qty)
 { 
         $.ajax({
                type:"post",
                url:site_url+"admin/order/delete_product/"+id+"/"+proid+"/"+qty,
                success:function(data){
                  //alert(data);
                    window.location.href=window.location.href;
                }
        });
 }
 function isNumberKey(evt)
 {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
  return true;
 }
 function get_vendor(val)
 {
    $.get(site_url+'admin/order/get_vendor_list',
    {
       v_type:val
    },
     function(data){
      //alert(data);
      $('#v_list').html(data.trim());

    })
        
  }
function remove_table_data()
{
    $("#offline_tbl tbody").empty();
    $("#price_details tbody").html('<tr><td>Sub total</td><td id="sub">0</td><td>Tax</td><td id="tax" tcst="0" ttin="0" tenrty="0">0</td><td>Shipping Charge</td><td id="charg">0</td><td>Final total</td><td id="final">0</td></tr>');
    $('#cheque_pay').addClass('hide');
    $('#payment_mode').val("");
    $('#prod').val("");
    //$("#prod").val(null).trigger("change"); 
    $('#pro_feature_div').addClass('hide');
    $('#color').addClass('hide');
    $('#pro_size').addClass('hide');
}


function open_address(val)
{ 
      $.get(site_url+"admin/order/get_vend_address_details/"+val,
            function(data,status){
            //alert("Data: " + data + "\nStatus: " + status);
            $('#ship_addr').addClass('hide');
            $('#adddetailtab').removeClass('hide');
            $('#adddetailtab').html(data.trim());
        });
    
   
}
function add_back_address(id)
{  
  if(id!=''){
    $.post(site_url+"admin/order/get_ven_account_details/"+id,
            function(data,status){
            //alert("Data: " + data + "\nStatus: " + status);
             $('#adddetailtab').addClass('hide');
             $('#ship_addr').removeClass('hide');
             $('#ship_addr').html(data.trim());
             
        });
    
  }else{
    $('#ship_addr').addClass('hide');
    $('#adddetailtab').removeClass('hide');
  }
  
}
function auto_fill()
{ 
  var err=0;
  if($('#shipadd').val().search(/\S/) == -1)
  { 
    err++
    $('#shipadd').css('border-color','red');
  }else{
    $('#shipadd').css('border-color','');
  }
   if($('#shippin').val()=="")
  { 
    err++
    $('#shippin').css('border-color','red');
  }else if($('#shippin').val().length<6){
    alert('Pincode minimum length is six digits');
    err++
    $('#shippin').css('border-color','red');
  }else{
    $('#shippin').css('border-color','');
  }
  if(err==0)
  {
    $('#billname').val($('#shipname').val());
    $('#billcontact').val($('#shipcontact').val());
    $('#billadd').val($('#shipadd').val());
    $('#billpin').val($('#shippin').val());

  }else{
    return false;
  }
}

function shipdetail_fill(id)
{
  var err=0;

  if($('#shipadd').val().search(/\S/) == -1)
  { 
    err++
    $('#shipadd').css('border-color','red');
  }else{
    $('#shipadd').css('border-color','');
  }
   if($('#shippin').val()=="")
  { 
    err++
    $('#shippin').css('border-color','red');
  }else if($('#shippin').val().length<6){
    alert('Pincode minimum length is six digits');
    err++
    $('#shippin').css('border-color','red');
  }else{
    $('#shippin').css('border-color','');
  }
   if($('#billname').val().search(/\S/) == -1)
  { 
    err++
    $('#billname').css('border-color','red');
  }else{
    $('#billname').css('border-color','');
  }
   if($('#billcontact').val()=="")
  { 
    err++
    $('#billcontact').css('border-color','red');
  }else if($('#billcontact').val().length<10){
    err++
    alert('billcontact minimum length is ten digits');
    $('#billcontact').css('border-color','red');
  }
  else{
    $('#billcontact').css('border-color','');
  }
  if($('#billadd').val().search(/\S/) == -1)
  { 
    err++
    $('#billadd').css('border-color','red');
  }else{
    $('#billadd').css('border-color','');
  }
   if($('#billpin').val()=="")
  { 
    err++
    $('#billpin').css('border-color','red');
  }else if($('#billpin').val().length<6){
    alert('Pincode minimum length is six digits');
    err++
    $('#billpin').css('border-color','red');
  }else{
    $('#billpin').css('border-color','');
  }
  if(err==0)
  {
    //alert($("#ship_addr_form").serialize());return false;
   $.ajax({
    type:"post",
    url:site_url+"admin/order/save_vender_addr/"+id,
    data:$("#ship_addr_form").serialize(),
    success:function(rep)
    { 
      //alert(rep);return false;
        open_address(id);
    }

       })

  }
  
}
function delete_shipp_addr(aid,vid)
{
  $.post(site_url+"admin/order/delshipdetail/"+aid, function(data, status){
        //alert("Data: " + data + "\nStatus: " + status);
        open_address(vid);
    });
}
function editshipdetail(aid,vid)
{
  $.post(site_url+"admin/order/get_vendor_ship_details/"+aid+"/"+vid, function(data, status){
        //alert("Data: " + data + "\nStatus: " + status);
             $('#adddetailtab').addClass('hide');
             $('#ship_addr').removeClass('hide');
             $('#ship_addr').html(data.trim());
    });
}
function shipdetail_update(aid,vid)
{
  var err=0;

  if($('#shipadd').val().search(/\S/) == -1)
  { 
    err++
    $('#shipadd').css('border-color','red');
  }else{
    $('#shipadd').css('border-color','');
  }
   if($('#shippin').val()=="")
  { 
    err++
    $('#shippin').css('border-color','red');
  }else if($('#shippin').val().length<6){
    alert('Pincode minimum length is six digits');
    err++
    $('#shippin').css('border-color','red');
  }else{
    $('#shippin').css('border-color','');
  }
   if($('#billname').val().search(/\S/) == -1)
  { 
    err++
    $('#billname').css('border-color','red');
  }else{
    $('#billname').css('border-color','');
  }
   if($('#billcontact').val()=="")
  { 
    err++
    $('#billcontact').css('border-color','red');
  }else if($('#billcontact').val().length<10){
    err++
    alert('billcontact minimum length is ten digits');
    $('#billcontact').css('border-color','red');
  }
  else{
    $('#billcontact').css('border-color','');
  }
  if($('#billadd').val().search(/\S/) == -1)
  { 
    err++
    $('#billadd').css('border-color','red');
  }else{
    $('#billadd').css('border-color','');
  }
   if($('#billpin').val()=="")
  { 
    err++
    $('#billpin').css('border-color','red');
  }else if($('#billpin').val().length<6){
    alert('Pincode minimum length is six digits');
    err++
    $('#billpin').css('border-color','red');
  }else{
    $('#billpin').css('border-color','');
  }
  if(err==0)
  {
   $.ajax({
    type:"post",
    url:site_url+"admin/order/update_vender_addr/"+aid,
    data:$("#ship_addr_form").serialize(),
    success:function(rep)
    { 
      //alert(rep);return false;
        open_address(vid);
    }

       })

  }
  
}
function assign_val(ths)
{
  if(ths.checked)
    $('#ship_adddr_id').val(ths.value);
  else
    $('#ship_adddr_id').val('');
}

$('#submitmanu').click(function(){
  var err=0;
  if($('#v_type').val()=="")
  { 
    err++
    $('#v_type').css('border-color','red');
  }else{
    $('#v_type').css('border-color','');
  }
  if($('#v_list').val()=="")
  { 
    err++
    $('#v_list').css('border-color','red');
  }else{
    $('#v_list').css('border-color','');
  }
  if($('#prod').val()=="")
  { 
    err++
    $('#prod').css('border-color','red');
  }else{
    $('#prod').css('border-color','');
  }
  if($('#ship_adddr_id').val()=="")
  { 
    err++
    alert('Please Select address');
  }
  if($('#payment_mode').val()=="")
  { 
    err++
    $('#payment_mode').css('border-color','red');
  }else{
    $('#payment_mode').css('border-color','');
  }
  if($('#sub').text()=="0")
  { 
    err++
    alert('First add atlest one product');
  }
  if(err=="0"){
    $("#submitmanu").text('Process..');
     $("#submitmanu").attr("disabled","disabled");
     $('.fa-spin').removeClass('hide');
  var sku_no=[];
  var pro_img=[];
  var pro_name=[];
  var promapid=[];
  var catid=[];
  var qty=[];
  var age=[];
  var color=[];
  var tin=[];
  var tin_p=[];
  var cst=[];
  var cst_p=[];
  var entry=[];
  var entry_p=[];
  var unit_pz=[];
  var total=[];
 $('#offline_tbl tbody tr').each(function() {
  sku_no.push($(this).find(".sku").text());
  pro_img.push($(this).find(".img").attr('src'));
  pro_name.push($(this).find(".pro_name").text());
  promapid.push($(this).find(".pro_name").attr('pid'));
  catid.push($(this).find(".pro_name").attr('catid'));
  qty.push($(this).find(".qty").text());
  age.push($(this).find(".age").text());
  color.push($(this).find(".colr").attr('clr'));
  tin.push($(this).find(".tin").text());
  tin_p.push($(this).find(".tin").attr('tx'));
  cst.push($(this).find(".cst").text());
  cst_p.push($(this).find(".cst").attr('tx'));
  entry.push($(this).find(".entry").text());
  entry_p.push($(this).find(".entry").attr('tx'));
  unit_pz.push($(this).find(".unit").text());
  total.push($(this).find(".total").text());
 })

  $.ajax({
      type:"post",
      url:site_url+"admin/order/add_order_by_admin/",
      data:{'vendoid':$('#v_list').val(),'SKUNumber':sku_no,'pro_color':color,'pro_age':age,'pro_qty':qty,'image':pro_img,'proname':pro_name,'promap_id':promapid,'cat_id':catid,'tin_tax':tin,'tin_taxp':tin_p,'cst_tax':cst,'cst_taxp':cst_p,'entry_tax':entry,'entry_taxp':entry_p,'sale_pz':unit_pz,'total_amt':total,'ship_id':$('#ship_adddr_id').val(),'paymentmode':$('#payment_mode').val(),'cheque_no':$('#chequeno').val(),'ifsc_code':$('#ifsccode').val(),'bank_name':$('#bankname').val(),'cheque_date':$('#chequedate').val(),'acc_no':$('#accno').val(),'chequecollect_date':$('#chequecollectdate').val(),'cheque_amount':$('#chequeamount').val(),'subtotal':$('#sub').text(),'total_tax':$('#tax').text(),'ttin_tax':$('#tax').attr('ttin'),'tcst_tax':$('#tax').attr('tcst'),'tentry_tax':$('#tax').attr('tenrty'),'shipping_charge':$('#charg').text(),'final_total':$('#final').text(),'remark':$('#remrk').val()},
      success:function(rep)
      { 
         //console.log(rep);
         //alert(rep);return false;
         //$('#test').html(rep.trim());
         window.location.href=window.location.href;
      }

    })
}
});
function get_cheque_form(val)
{
   if(val=="CHEQUE")
    $('#cheque_pay').removeClass('hide');
  else
    $('#cheque_pay').addClass('hide');
}
function delete_row(val)
{
  $('#sub').text(parseFloat($('#sub').text()) - parseFloat($(val).closest('tr').find('.total').text()));
$.ajax({
  type:"post",
  url:site_url+"admin/order/get_shipping_charge/"+$('#sub').text(),
  success:function(data){
    //alert(data);return false;
    $('#charg').text(data.trim());
  }

});
 
  $('#tax').attr('ttin',(parseFloat($('#tax').attr('ttin')) - parseFloat($(val).closest('tr').find('.tin').text())).toFixed(1));
  $('#tax').attr('tcst',(parseFloat($('#tax').attr('tcst')) - parseFloat($(val).closest('tr').find('.cst').text())).toFixed(1));
  $('#tax').attr('tenrty',(parseFloat($('#tax').attr('tenrty')) - parseFloat($(val).closest('tr').find('.entry').text())).toFixed(1));
  var tax=parseFloat($(val).closest('tr').find('.tin').text()) + parseFloat($(val).closest('tr').find('.cst').text()) + parseFloat($(val).closest('tr').find('.entry').text());
  $('#tax').text((parseFloat($('#tax').text() - parseFloat(tax)).toFixed(1)));
  $('#final').text((parseFloat($('#final').text()) - parseFloat(parseFloat($(val).closest('tr').find('.total').text()) + parseFloat(tax))).toFixed(1));

  $(val).closest('tr').remove();
   var i=1;
  $('#offline_tbl tbody tr').each(function(){
    $(this).find(".sno").text(i);
    i++
  });

}


</script>

   