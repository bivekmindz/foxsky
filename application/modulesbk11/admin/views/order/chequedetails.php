<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage Cheque Order details</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view single Cheque order details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                              <!-- <h3>Order Number :- <?php echo $pendetailview[0]->OrderNumber; ?></h3> -->
                              <h3 style="float:right;width:auto;">Order Number :- <?php echo $pendetailview[0]->OrderNumber; ?></h3>
                               <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                <form method="post">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <th>S.NO.</th>
                                          <th>Order Status</th>
                                          <th>Vendor</th>
                                          <th>Shop Name</th>
                                          <th>SKUNumber</th>
                                          <th>Product Image</th>
                                          <th>Product Name</th>
                                          <th>Quantity</th>
                                          <th>Age</th>
                                          <th>Color</th>
                                          <th>VAT/TIN Tax</th>
                                          <th>CST Tax</th>
                                          <th>Entry/<br>Octori<br>Tax</th>
                                          <th>MRP</th>
                                          <th>Unit Price</th>
                                          <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=0; foreach ($pendetailview as $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo ucfirst($value->ShipStatus);?></td>
                                            <td><?php echo $value->firstname." ".$value->lastname;?></td>
                                            <td><?php echo $value->companyname;?></td>
                                            <td><?php echo $value->SkuNumber;?></td>
                                            <td><img src="<?php echo $value->ProImage;?>" style="vertical-align:middle; width:80px;"></td>
                                            <td><?php echo $value->ProName;?></td>
                                            <td><?php echo $value->ProQty;?></td>
                                            <td><?php echo $value->OrdSize;?></td>
                                            <td>
                                            <?php if($value->OrdColor=='multicolor'){
                                            $urlmulti=base_url().'images/imgpsh_fullsize.jpg'; 
                                            $OrdColor='#'.$value->OrdColor; echo '<font style="background-image:url('.$urlmulti.');background-size: cover;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br>'.$value->color_name;
                                             } else {  
                                              $OrdColor='#'.$value->OrdColor; echo '<font style="background-color:'.$OrdColor.'";>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br>'.$value->color_name;
                                             } ?>
                                            </td>
                                           <?php $amt=number_format($value->base_prize*$value->ProQty,1,".",""); ?>
                                            <td><?php echo $tin_tax_amt=number_format($amt * $value->protaxvat_p / 100,1,".","");?></td>
                                            <td><?php echo $cst_tax_amt=number_format($amt * $value->protaxcst_p / 100,1,".","");?></td>
                                            <td><?php echo $entry_tax_amt=number_format($amt * $value->protaxentry_p / 100,1,".","");?></td>
                                            <td><?php echo number_format($value->ProPrice,1,".","");?></td>
                                            <td><?php echo number_format($value->base_prize,1,".","");?></td>
                                            <td><?php echo number_format($value->base_prize*$value->ProQty,1,".","");?></td>
                                           <?php 
                                              $t_subtotal[]=number_format($value->base_prize*$value->ProQty,1,".","");
                                              $t_tax[]=number_format($tin_tax_amt+$cst_tax_amt+$entry_tax_amt,1,".","");
                                              $totproprice=$amt+$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
                                              //$t_coup[]=get_partialdiscountpercentamt($totproprice,$value->couponper);
                                              //$t_wall[]=get_partialdiscountpercentamt($totproprice,$value->walletper);
                                            ?>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                        <?php
                                            $ship=number_format($value->ShippingAmt,1,".","");
                                            $to_subtotal=array_sum($t_subtotal);
                                            $to_tax=array_sum($t_tax);
                                            //$to_coup=number_format(array_sum($t_coup),1,".","");
                                            //$to_wall=number_format(array_sum($t_wall),1,".","");
                                            $to_coup=number_format($value->CouponAmt,1,".","");
                                            $to_wall=number_format($value->usedwalletamt,1,".","");

                                        ?>
                              <td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Sub Total :</b></td>
                              <td colspan="2"><?php echo number_format($to_subtotal,1,".","");?></td></tr>
                              <tr><td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Shipping :</b></td>
                              <td colspan="2"><?php echo $ship;?></td></tr>
                              <tr><td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Tax :</b></td>
                              <td colspan="2"><?php echo number_format($to_tax,1,".",""); ?></td></tr>
                              <?php if(!empty($value->CouponAmt)){ ?>
                              <tr><td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Coupon Amount (-) :</b></td>
                              <td colspan="2"><?php echo $to_coup; ?></td></tr>
                              <?php } ?>
                              <?php if(!empty($value->usedwalletamt) && $value->usedwalletamt!=0.0){ ?>
                              <tr><td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Wallet Pay (-) :</b></td>
                              <td colspan="2"><?php echo $to_wall; ?></td></tr>
                              <?php } ?>
                              <tr><td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Final Total :</b></td>
                              <td colspan="2"><?php $ordamts=number_format(number_format($to_subtotal,1,".","")+$ship+number_format($to_tax,1,".","")-$to_coup-$to_wall,1,".","");
                                      if($ordamts==0.0){
                                        echo number_format(number_format($to_subtotal,1,".","")+$ship+number_format($to_tax,1,".","")-$to_coup,1,".",""); }
                                      else {
                                        echo $ordamts;
                                        } ?></td></tr>
                                    </tbody>
                                    
                                </table>
                                </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>