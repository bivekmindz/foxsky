<?php //p($record);
//p($detialrecord[0]->tinvalue);exit;?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo admin_url();?>fontawsome/css/font-awesome.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/webapp/js/ConvertNumberToWord.js" ></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Invoice</title>
<style type="text/css">
body{
	margin:0; 
	padding:0; 
	-webkit-print-color-adjust: exact;
	-moz-print-color-adjust: exact;
	-khtml-print-color-adjust: exact;
	-o-print-color-adjust: exact;
}
.logo_left h3{
	margin:5px 0;
	font-size:14px;
}
.logo_left p{
	margin:5px 0;
}
.container{max-width:900px; margin:auto; border: dashed 1px #ccc; float:left; padding:10px 10px;}
.header{width:100%; height:40px; background:#F1F1F1;}
.wrapper{width:100%; margin:auto;}
.section{width:100%;}
.top_header{width:100%; padding:5px 0px;}
.logo_left{width:100%; float:left;text-align: center;}
.logo_left p{font-size:10px;}
.invoice_right{width:100%; float:right;text-align: center;}
.invoice_right h1{font-size:26px; color:#000; text-align:right; font-family: Arial, Helvetica, sans-serif;}
.top_header_bottom{width:100%; padding:8px 0px;float:left;}
.invoice_to{width:40%; float:left;}
.invoice_to p{font-size:12px; margin-bottom: -14px; margin-left: 6px;line-height: 19px;}
.invoice_to_right{width:60%; float:left; margin-top: 17px;}
.date{float:right; margin-left: 16px; padding: 5px 0px;text-align: center;}
.date img{width:26px;}
.date b{color:#3A3A3A; font-size:12px;}
.date span{
    font-size:12px;
    padding-top:7px;
    display: block;
}
.invoice_no span{
    font-size:12px;
    padding-top:7px;
    display: block;
}
.invoice_no{float:right; padding: 5px 0px;text-align: center;
    margin-left: 10px;}
.invoice_no b{color:#3A3A3A; font-size:12px;}
.payment{width:40%;  background:#058ED0; }
.payment h1{font-size:16px; text-align:left; color:#fff; padding:5px 5px;}
.invoice_too {width: 53%;float: left; margin-top: 6px;}
.invoice_too p{font-size:15px; margin-bottom: -14px; margin-left: 6px;}
.thanku{padding: 15px 8px 5px 0px; font-size:12px; color:#000;}
.invoice_too_right{width:100%; float:right; /*height:74px;*/}
.blue_bg{background:#058ED0;float:left;width:35%;}
.blue_bg2{background:#058ED0;float:right;width:35%;}
.payment-right{width:100%;   float:right; /*margin:70px 75px;*/}
.payment-right h3{  font-size: 12px;
    text-align: left;
    float: right;
    color: #fff;
    padding: 7px 11px 7px 0px;
    margin: 0px;
    width: 73%;
    margin-right: 34px;}
	.payment-right h1{  font-size: 12px;
    text-align: left;
    float: left;
    color: #fff;
    padding: 7px 11px;
    margin: 0px;
    width: 100%;
    margin-left: 0px;}
.signature{float:right;      margin: -39px 11px;}
.signature h1{font-size:12px; color:#000;text-align:center;}
.signature p{font-size:12px; color:#000; text-align:left;margin-top: 52px;}
.footer_head{width:53%; height:30px; background:#058ED0; float:left; }
.footer{width:53%;  background:#F1F1F1; float:left; }
.left-text{float:left; font-size:14px; color:#fff; padding:5px 5px;}
.right-text{width:200px; float:right; font-size:14px; color:#fff; padding:5px 5px;}
.left-text p{color:#000; font-size:10px;margin-left: 1px;margin: 4px 0px;}
.right-text p{color:#000; font-size:13px; }
button {
    background: #1870BB;
    border: none;
    color: #fff;
    padding: 5px 10px;
    font-size: 13px;
    position: absolute;
    left: 921px;
    top: 0;
}
.date i{color:#3A3A3A; font-size: 15px; padding: 0px 13px;}
.invoice_no i{color:#3A3A3A; font-size: 15px; padding: 0px 13px;}
.top_header_bottom1{width:100%;}
.bank_d{
    float: right;
    font-size: 12px;
    margin: 17px;
}
.bank_d p{
    foat:left;width:100%;margin:4px 0px;
}
.bank_d p b{
    float:left;
    margin-right:8px;
}
</style>

</head>

<body>
<button id="prinvoice">invoice print</button>
<div class="container">

<div class="wrapper">
<div class="section">
<div class="top_header">
<div class="invoice_right">Retail/Tax Invoice</div>
<div class="logo_left"><h3>Good Trade Tech Solutions Pvt. Ltd.</h3><p> 43, MM, 2nd floor, Rani Jhansi Road, New Delhi-55
<br>
<i class="fa fa-phone" aria-hidden="true"></i> 011 - 43543769 &nbsp;&nbsp;&nbsp;
<i class="fa fa-envelope" aria-hidden="true"></i> sales@MindzShop.com &nbsp;&nbsp;&nbsp;<i class="fa fa-globe" aria-hidden="true"></i> www.MindzShop.com  
</p></div><!--end of invoice-->
</div>
<!--end of logo-->
<!-- end of top header -->

<div class="top_header_bottom">
<?php //p($record);?>
<div class="invoice_to">
<p>BILL TO :</p>
<p><b>SHOP NAME : <?php echo ucwords($record->companyname);?></b></p>
<p><b><?php echo ucwords($record->BillName);?></b></p>
<p><?php echo ucwords($record->ShippingAddress);?> </p>
<p><b>PAN : </b> <?php echo (!empty($record->pan_no))?$record->pan_no:'NA';?></p>
<p><b>VAT / CST / TIN No. : </b> <?php echo (!empty($record->tin_no))?$record->tin_no:'NA';?></p>
</div><!-- end of invoice to -->

<div class="invoice_to_right">
<div style="float:right;">
<span class="invoice_no"><i class="fa fa-inr" aria-hidden="true"></i><br>
<b>Payment Method</b><br /><span><?php echo $record->PaymentMode; ?></span></span>
<span class="date"><i class="fa fa-calendar" aria-hidden="true"></i><br>
<b>DATE</b><br /><span><?php echo explode(' ',$record->created)[0];?><br /><?php echo explode(' ',$record->created)[1];?></span></span>

<span class="invoice_no"><i class="fa fa-file-text-o" aria-hidden="true"></i><br>
<b>ORDER ID</b><br /><span><?php echo $record->OrderNumber;?></span></span>

</div>

</div><!-- end of invoice right -->
</div><!-- end of header-bottom -->

<div class="top_header_bottom">
<table style="border-collapse:collapse;margin:0px; margin-top:15px;float:left;width:100%;font-size:13px;">
<tbody>
<tr style="background: #058ED0;color: #fff;">
<th style="font-size: 12px;">S.No.</th>
<th style="padding:5px 5px;width:300px; text-align:left;width:300px;font-size: 12px;">Product Name</th>
<th style=" font-size: 12px;">Sku</th>
<th style=" font-size: 12px;">Quantity</th>
<th style="font-size: 12px;">MRP</th>
<th style="font-size: 12px;">Discount&nbsp;%</th>
<th style="font-size: 12px;">Tax&nbsp;%</th>
<th style="font-size: 12px;">Unit&nbsp;Price</th>
<th style="font-size: 12px;">Total</th></tr>
<?php 
    $i=1;
    if(isset($detialrecord) && count($detialrecord)>0){
       foreach ($detialrecord as $value) {
       $row_id=$value->id;
       //$pro_id=$value->ProId;
        $t_subtotal[]=number_format($value->unit_pz*$value->pro_qty,1,".","");
        $t_tax[]=number_format($value->tax,1,".","");
     //$i++;
    ?>
<!--<tr style="background: #<?php echo ($i%2==0)?'dedede':'ccc';?>;color: #000;">-->
<tr>

<td style=" text-align:center;font-size:12px;"><?php echo $i++;?>.</td>
<td style="padding:3px 5px;width:300px; text-align:left;font-size:12px;"><?php echo $value->product_name;?></td>
<td style=" text-align:center;font-size:12px;"><?php echo $value->sku_no;?></td>
<td style=" text-align:center;font-size:12px;"><?php echo $value->pro_qty;?></td>
<td style=" text-align:center;font-size:12px;"><?php echo number_format($value->mrp,1,".","");?></td>
<td style=" text-align:center;font-size:12px;"><?php echo number_format(($value->mrp-$value->unit_pz)*100/$value->mrp,1,".","");?></td>
<td style=" text-align:center;font-size:12px;">
<?php

 $tax_per= ($value->tax * 100)/($value->unit_pz * $value->pro_qty); 

 if($tax_per< 7 and $tax_per> 4){

    echo 5;
 }
 if($tax_per< 15 and $tax_per > 8){

    echo 12.5;
 }

 if($tax_per<4 and $tax_per>1){

    echo 2;
 }

if($tax_per<1 ){

    echo 0;
}


 

 ?></td>

<td style=" text-align:center;font-size:12px;">Rs. <?php echo number_format($value->unit_pz,1,".","");?></td>
<td style=" padding: 1px 0px 1px 40px;text-align: left; font-size: 12px; width:81px;">Rs. <?php echo number_format($value->unit_pz * $value->pro_qty,1,".","");?></td>


</tr>

<?php $totordqty+=$value->pro_qty;?>


<?php }} ?>
</tbody>
</table>
</div>

<div class="top_header_bottom1">
<div style="width:100%; float:right;">
<!--<div class="payment"><h1>Company Statutory Details</h1></div>
<!--end of invoice to -->

<div class="invoice_too_right">
<table style="border-collapse:collapse;margin:0px;float:right;width:350px;font-size:13px;">
<tbody>
<tr style="color: #000;"><td style="padding: 1px 30px;text-align: right;font-size: 12px;">SUB TOTAL</td><td style="padding:1px 0px 1px 40px; text-align: left;width:81px;">Rs. <?php echo number_format($to_subtotal=array_sum($t_subtotal),1,".","");?></td></tr>

<tr style="color: #000;"><td style="padding: 1px 53px;text-align: right;font-size: 12px;">TAX</td><td style="padding:1px 0px 1px 40px; text-align: left;">Rs. <?php echo number_format($to_tax=array_sum($t_tax),1,".","");?></td></tr>

<tr style="color: #000;"><td style="padding: 1px 39px;text-align: right;font-size: 12px;">SHIPPING</td><td style="padding:1px 0px 1px 40px; text-align: left;">Rs. <?php echo number_format($record->shipping_amt,1,".","");?></td></tr>

<?php if(!empty($record->CouponAmt)){ ?><tr style="color: #000;"><td style="padding: 1px 39px;text-align: right;font-size: 12px;">COUPON DISCOUNT(-)</td><td style="padding:1px 0px 1px 40px; text-align: left;">Rs. <?php echo number_format($record->CouponAmt,1,".","");?></td></tr> <?php } ?>

<?php if(!empty($record->usedwalletamt)){ ?><tr style="color: #000;"><td style="padding: 1px 39px;text-align: right;font-size: 12px;">WALLET PAY(-)</td><td style="padding:1px 0px 1px 40px; text-align: left;">Rs. <?php echo number_format($record->usedwalletamt,1,".","");?></td></tr> <?php } ?>


</tbody>
</table>

</div><!-- end of invoice right -->
<?php
if(empty($record->CouponAmt) && empty($record->usedwalletamt)){
    $amo_round= number_format((number_format($to_subtotal,1,".","")+number_format($to_tax,1,".","")+number_format($record->shipping_amt,1,".","")),1,".","");
} else {
    $amo_round= number_format((number_format($to_subtotal,1,".","")+number_format($to_tax,1,".","")+number_format($record->shipping_amt,1,".","")-(number_format($record->CouponAmt,1,".","")+number_format($record->usedwalletamt,1,".",""))),1,".","");
}
$final_total_round=ceil($amo_round).'.00';
?>
<input type="hidden" id="td_amt" value="<?php echo $final_total_round; ?>" />
<div style="padding: 0px 0px 8px 0px;font-size: 12px;color: #000;">Total Order Quantity : <?php echo $totordqty; ?></div>
<div class="payment-right">
<div class="blue_bg">
<h1>Company Statutory Details</h1></div>
<div class="blue_bg2">
<h3><span style="text-align:left; padding: 0 13px;font-size:16px;">Final Total</span> 
<span style="float:right;color:#fff;font-size:16px;margin-right: -7px;">Rs. <?php echo $final_total_round ?></span></h3></div>
</div>
<div class="invoice_too">

<div class="thanku"><b>CIN :</b> AAE-5709 <br>
<b>PAN :</b> AAOFG6971C<br> 
<b>VAT / CST / TIN No. :</b> 07367134910<br>
</div>
<div class="thanku">Rupees in Words : <span id="p_inwordamt"></span></div>
</div>
<div class="bank_d">
   <!-- <p><b>Bank Account details :</b> Good Trade Tech Solutions Pvt Ltd.</p>-->
   <p> <b>Current A/C No. :</b> 9160200347688891
</p>
    <p><b>Bank :</b> Axis Bank Limited</p>
   <!-- <p><b>Address :</b> Dr Gopal Das Bhawan, 28, Barakhamba Road,<br> New Delhi-110001</p>-->
    <p><b>IFSC Code:</b> UTIB0001146

    </p>
</div>
</div> <!--paymentright-->




</div><!-- end of section -->

<div class="footer_head">
<div class="left-text">TERMS</div>

</div>
<div style="width:100%; float:left;">
<div class="footer">
<div class="left-text"><p>              
1. Goods once sold will not be returned or exchange in any condition.<br>                    
2. Interest @ 18%p.a. will be charged if amount not paid in due date.<br>                    
3. All Disputes Subject to Delhi jurisdiction.<br>                   
4. Warranty: standard as per the Manufacturer.                    
</p></div>

</div>
<div class="signature">
<h1>For Good Trade Tech Solutions Pvt. Ltd.</h1>
<p>Authorized Signatory</p>
</div>
</div><!--end of footer -->

</div><!--end of wrapper -->
</div><!--end of container -->

<script type="text/javascript">
$(document).ready(function() {
    $("#prinvoice").click(function() {
        $("#prinvoice").css('display', 'none');
        window.print();
    });
});


    $(document).ready(function () {
        inWords('td_amt', 'p_inwordamt');
    });

</script>

</body>
</html>

