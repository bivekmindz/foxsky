<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Offline Order details</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view single offline order details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                              <h3>Order Number :- <?php echo $details[0]->OrderNumber; ?></h3>
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                <form method="post">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <th>S.NO.</th>
                                          <th>Order Status</th>
                                          <th>Vendor</th>
                                          <th>Shop Name</th>
                                          <th>SKUNumber</th>
                                          <th>Product Image</th>
                                          <th>Product Name</th>
                                          <th>Quantity</th>
                                          <th>Age</th>
                                          <th>Color</th>
                                          <th>VAT/TIN Tax</th>
                                          <th>CST Tax</th>
                                          <th>Entry/<br>Octori<br>Tax</th>
                                          <th>MRP</th>
                                          <th>Unit Price</th>
                                          <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=0; foreach ($details as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->ShipStatus;?></td>
                                            <td><?php echo $value->firstname." ".$value->lastname;?></td>
                                            <td><?php echo $value->companyname;?></td>
                                            <td><?php echo $value->SkuNumber;?></td>
                                            <td><img src="<?php echo $value->ProImage;?>" style="vertical-align:middle; width:80px;"></td>
                                            <td><?php echo $value->ProName;?></td>
                                            <td><?php echo $value->ProQty;?></td>
                                            <td><?php echo $value->OrdSize;?></td>
                                            <td>
                                            <?php if($value->OrdColor=='multicolor'){
                                            $urlmulti=base_url().'images/imgpsh_fullsize.jpg'; 
                                            $OrdColor='#'.$value->OrdColor; echo '<font style="background-image:url('.$urlmulti.');background-size: cover;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br>'.$value->color_name;
                                             } else {  
                                              $OrdColor='#'.$value->OrdColor; echo '<font style="background-color:'.$OrdColor.'";>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br>'.$value->color_name;
                                             } ?>
                                            </td>
                                            <?php $amt=number_format($value->base_prize*$value->ProQty,1,".",""); ?>
                                            <td><?php echo $tin_tax_amt=number_format($amt * $value->protaxvat_p / 100,1,".","");?></td>
                                            <td><?php echo $cst_tax_amt=number_format($amt * $value->protaxcst_p / 100,1,".","");?></td>
                                            <td><?php echo $entry_tax_amt=number_format($amt * $value->protaxentry_p / 100,1,".","");?></td>
                                            <td><?php echo number_format($value->ProPrice,1,".","");?></td>
                                            <td><?php echo number_format($value->base_prize,1,".","");?></td>
                                            <td><?php echo number_format($value->base_prize*$value->ProQty,1,".","");?></td>
                                            <?php 
                                              $t_subtotal[]=number_format($value->base_prize*$value->ProQty,1,".","");
                                              $t_tax[]=number_format($tin_tax_amt+$cst_tax_amt+$entry_tax_amt,1,".","");
                                            ?>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                        <?php
                                            $ship=number_format($value->ShippingAmt,1,".","");
                                            $to_subtotal=array_sum($t_subtotal);
                                            $to_tax=array_sum($t_tax);

                                        ?>
                                <td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Sub Total :</b></td>
                                <td colspan="2"><?php echo number_format($to_subtotal,1,".","");?></td></tr>
                                <tr><td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Shipping :</b></td>
                                <td colspan="2"><?php echo $ship;?></td></tr>
                                <tr><td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Tax :</b></td>
                                <td colspan="2"><?php echo number_format($to_tax,1,".",""); ?></td></tr>
                                <tr><td colspan="12">&nbsp;</td><td colspan="2" style="border-right:solid 0px #ccc;"><b>Final Total :</b></td>
                                <td colspan="2"><?php echo number_format(number_format($to_subtotal,1,".","")+number_format($to_tax,1,",","")+$ship,1,".",""); ?></td></tr>
                                    </tbody>
                                    
                                </table>
                                </form>
                                <?php if(!empty($details[0]->userremark)){ ?>
                                <div><p style="font-size: 15px;padding: 6px 0px 6px 0px;"><span style="font-weight: bold;">Order Remark :- </span><span style="color: #186BB2;font-weight: bold;"><?php echo $details[0]->userremark; ?></span></p></div>
                                <?php } ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    