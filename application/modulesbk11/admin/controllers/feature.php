<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Feature extends MX_Controller{

//............. Constuct class ............... //
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();

  }

//............. Add Feature Master ............... //
public function addfeature(){
  	$this->userfunction->loginAdminvalidation();
  	if($this->input->post('submit')){
  	 $featname         = $this->input->post('featname');
  	 $userid           = $this->session->userdata('bizzadmin')->LoginID;
  	 $parameter        = array('act_mode'=>'featcheck',  'row_id'=>'', 'feaid'=>'',  'feavalue'=>$featname,  'catid'=>'');
  	 $record['record'] = $this->supper_admin->call_procedureRow('proc_feature',$parameter);
    
    if($record['record']->attcount>0){
     $this->session->set_flashdata("message", "Feature Already Exists");
     redirect("admin/feature/addfeature");
    }
  	else{
  	  $parameter        = array('act_mode'=>'feainsert', 'row_id'=>$userid,  'feaid'=>'',  'feavalue'=>$featname,  'catid'=>'');
      $record['record'] = $this->supper_admin->call_procedureRow('proc_feature',$parameter);
  	  $this->session->set_flashdata("message", "Your information was successfully Saved.");
  	  redirect("admin/feature/viewfeature");
  	}
  }

 


  	$this->load->view('helper/header');
  	$this->load->view('feature/addfeature');

  }  

//............. View Feature Master ............... //
 public function viewfeature(){
  	$this->userfunction->loginAdminvalidation();
  	//----------------------multiple delete -------------------------------//
  	if($this->input->post('submit')){
  	 foreach ($this->input->post( 'attdelete') as $key => $value) {
  	  $parameter         = array('act_mode'=>'delete','row_id'=>$value,'feaid'=>'','feavalue'=>'','catid'=>'');
  	  $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);
  	}
      $this->session->set_flashdata("message", "Your information was successfully delete.");
      redirect("admin/feature/viewfeature");
  	}
  	//----------------------------end delete ---------------------------------//
   //--------------------------multiple ststus ------------------------------//
  	if($this->input->post('submitstatus')){
     foreach($this->input->post( 'attdelete') as $key => $value){
      $status            = $this->input->post('attstatu')[$value];
      $userid           = $this->session->userdata('bizzadmin')->LoginID;
      $act_mode          = $status == 'A' ? 'activefea':'inactivefea';
  	  $parameter2        = array('act_mode'=>$act_mode, 'row_id'=>$value, 'feaid'=>$userid ,'feavalue'=>'' ,'catid'=>'' );
      $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature', $parameter2);
     }
  	  $this->session->set_flashdata("message", "Your Status was successfully Updated.");
  	  redirect("admin/feature/viewfeature");
  	}

  	$parameter           = array('act_mode'=>'view', 'row_id'=>'', 'feaid'=>'', 'feavalue'=>$featname, 'catid'=>'' );
  	$responce['vieww']   = $this->supper_admin->call_procedure('proc_feature',$parameter);

    //----------------  Download Newsletter Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Feature Name');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Feature Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->FeatureName);
           
           
            }
          }

          $filename='Feature.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Newsletter Excel ------------------------// 

  	$this->load->view('helper/header');
  	$this->load->view('feature/featureview',$responce);

  } 

//............. Feature Delete ............... //
 public function featuredelete($id){
   $parameter         = array('act_mode'=>'delete','row_id'=>$id,'feaid'=>'','feavalue'=>'','catid'=>'');
   $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);
   $this->session->set_flashdata("message", "Your information was successfully delete.");
   redirect("admin/feature/viewfeature"); 	
 
  }

//............. Feature Status ............... //
 public function featurestatus ($id){
   $rowid             = $this->uri->segment(4);
   $status            = $this->uri->segment(5);
   $act_mode          = $status == 'A'?'activefea':'inactivefea';
   $userid           = $this->session->userdata('bizzadmin')->LoginID;
   $parameter2        = array('act_mode'=>$act_mode,'row_id'=>$rowid,'feaid'=>$userid,'feavalue'=>'','catid'=>'');
   $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter2);
   $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
   redirect(base_url().'admin/feature/viewfeature');

}   

//............. Feature Update ............... //
public function featureupdate($id){
  if($this->input->post('submit')){
    $featname         = $this->input->post('featname');
    $userid           = $this->session->userdata('bizzadmin')->LoginID;
    $parameter        = array('act_mode'=>'update','row_id'=>$id,'feaid'=>$userid,'feavalue'=>$featname,'catid'=>'');
    $record['record'] = $this->supper_admin->call_procedureRow('proc_feature',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully Updated.');
    redirect(base_url().'admin/feature/viewfeature');
   }	
  $parameter          = array('act_mode'=>'viewid','row_id'=>$id,'feaid'=>'','feavalue'=>'','catid'=>'');
  $responce['vieww']  = $this->supper_admin->call_procedureRow('proc_feature',$parameter);	
  $this->load->view('helper/header');
  $this->load->view('feature/editfeature',$responce);	

}

//............. Add Feature Left ............... //
public function addfeatureleft(){
  $this->userfunction->loginAdminvalidation();
 if($this->input->post('submit')){
  foreach ($this->input->post('leftvalue') as $key => $value) {
   $userid          = $this->session->userdata('bizzadmin')->LoginID; 
   $parameter       = array('act_mode'=>'featcheck','row_id'=>'','feaid'=>$this->input->post('featid'),'feavalue'=>$value,'catid'=>'');
   $record['record']= $this->supper_admin->call_procedureRow('proc_feature',$parameter);
    if($record['record']->attcount>0){
     $this->session->set_flashdata("message", "Feature Already Exists");
     redirect("admin/feature/addfeature");
    }
    else{
      $parameter       = array('act_mode'=>'insertleft','row_id'=>$userid,'feaid'=>$this->input->post('featid'),'feavalue'=>$value,'catid'=>'');
      $record['record']= $this->supper_admin->call_procedureRow('proc_feature',$parameter);
    }
  }
  $this->session->set_flashdata("message", "Your information was successfully Saved.");
  redirect("admin/feature/viewfeaturemaster");
   
 } 

 $parameter         = array('act_mode'=>'featleftview','row_id'=>'','feaid'=>'','feavalue'=>'','catid'=>'');
 $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter); 
 $parameterr        = array('act_mode'=>'catview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
 $responce['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr);  
 $this->load->view('helper/header');
 $this->load->view('feature/addfeatureleft',$responce);  

}

//............. View Feature Master ............... //
 public function viewfeaturemaster(){
    $this->userfunction->loginAdminvalidation();
    //----------------------multiple delete -------------------------------//
    if($this->input->post('submit')){
     foreach ($this->input->post( 'attdelete') as $key => $value) {
      $parameter         = array('act_mode'=>'leftdelete','row_id'=>$value,'feaid'=>'','feavalue'=>'','catid'=>'');
      $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);
    }
      $this->session->set_flashdata("message", "Your information was successfully delete.");
      redirect("admin/feature/viewfeaturemaster");
    }
   //--------------------------multiple ststus ------------------------------//
    if($this->input->post('submitstatus')){
     foreach ($this->input->post( 'attdelete') as $key => $value) {
      $status            = $this->input->post('attstatu')[$value];
      $userid           = $this->session->userdata('bizzadmin')->LoginID;
      $act_mode          = $status == 'A'?'activeleft':'inactiveleft';
      $parameter2        = array('act_mode'=>$act_mode,'row_id'=>$value,'feaid'=>$userid,'feavalue'=>'','catid'=>'');
      $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter2);
     }
      $this->session->set_flashdata("message", "Your Status was successfully Updated.");
      redirect("admin/feature/viewfeaturemaster");
    }

    //-------------------------------end ----------------------------------------//
    $parameter           = array('act_mode'=>'leftview','row_id'=>'','feaid'=>'','feavalue'=>'','catid'=>'');
    $responce['vieww']   = $this->supper_admin->call_procedure('proc_feature',$parameter);

    //----------------  Download Newsletter Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Feature Type','Left Feature Name');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Feature Left Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->FeatureName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->LeftValue);
           
            }
          }

          $filename='Feature Left.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Newsletter Excel ------------------------// 

    $this->load->view('helper/header');
    $this->load->view('feature/viewfeaturemaster',$responce);

  } 

 //............. Feature Master Delete ............... // 
 public function featuremasterdelete($id){
   $parameter         = array('act_mode'=>'leftdelete','row_id'=>$id,'feaid'=>'','feavalue'=>'','catid'=>'');
   $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);
   $this->session->set_flashdata("message", "Your information was successfully delete.");
   redirect("admin/feature/viewfeaturemaster");   
 
  }

//............. Feature Master Status ............... //
 public function featuremasterstatus ($id){
   $rowid             = $this->uri->segment(4);
   $status            = $this->uri->segment(5);
   $userid           = $this->session->userdata('bizzadmin')->LoginID;
   $act_mode          = $status == 'A'?'activeleft':'inactiveleft';
   $parameter2        = array('act_mode'=>$act_mode,'row_id'=>$rowid,'feaid'=>$userid,'feavalue'=>'','catid'=>'');
   $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter2);
   $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
   redirect(base_url().'admin/feature/viewfeaturemaster');

}  

//............. Feature Master Update ............... //
public function featuremasterupdate($id){
 if($this->input->post('submit')){
  $featname         = $this->input->post('featname');
  $userid           = $this->session->userdata('bizzadmin')->LoginID;
  $parameter        = array('act_mode'=>'leftupdate','row_id'=>$id,'feaid'=>$userid,'feavalue'=>$featname,'catid'=>'');
  $record['record'] = $this->supper_admin->call_procedureRow('proc_feature',$parameter);
  $this->session->set_flashdata('message', 'Your information was successfully Updated.');
  redirect(base_url().'admin/feature/viewfeaturemaster');
 }  
  $parameter          = array('act_mode'=>'leftviewId','row_id'=>$id,'feaid'=>'','feavalue'=>'','catid'=>'');
  $responce['vieww']  = $this->supper_admin->call_procedureRow('proc_feature',$parameter); 
  $this->load->view('helper/header');
  $this->load->view('feature/editleft',$responce);
}
//--------------------------------------end featuremap ---------------------------------------------------//

//............. Add Category Feature  ............... //
public function addcatfeature(){
  $this->userfunction->loginAdminvalidation();
  if($this->input->post('submit')){
    $userid           = $this->session->userdata('bizzadmin')->LoginID;
    foreach ($this->input->post('feavalueid') as $key => $value){
      $parameter         = array('act_mode'=>'catfeatucheck','row_id'=>$value,'feaid'=>$this->input->post('featid'),'feavalue'=>'','catid'=>$this->input->post('catidd'));
      $response['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);
     if($record['record']->attcount>0){
      $this->session->set_flashdata("message", "Category Already Exists");
      redirect("admin/feature/addcatfeature");
     }
     else{
      $parameter         = array('act_mode'=>'insertcatf','row_id'=>$value,'feaid'=>$this->input->post('featid'),'feavalue'=>$userid,'catid'=>$this->input->post('subcatidd'));
      $response['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);
     }
    }//foreah

   $this->session->set_flashdata("message", "Your information was successfully saved.");
   redirect("admin/feature/featureviewcat"); 
  }	

  $parameter         = array('act_mode'=>'featleftview','row_id'=>'','feaid'=>'','feavalue'=>'','catid'=>'');
  $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);
   
  $parameterr                = array('act_mode'=>'parecatview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $responce['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 	
  $this->load->view('helper/header');
  $this->load->view('feature/addfeaturecate',$responce);	
}


public function catvalue(){
  $catidd                    = $this->input->post('catid');
  $parameterr                = array('act_mode'=>'childcatview','row_id'=>$catidd,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $responce['childCatdata']  = $this->supper_admin->call_procedure('proc_category',$parameterr);  
  

  $str               = '';

  foreach($responce['childCatdata'] as $k=>$v){   
   # $str .= "<option value=".$v->id.">".$v->attvalue."</option>";
   $str .= '<option value="'.$v->catid.'">'.$v->catname.'</option>';
  }
  echo $str;

}

//............. Feature Value ............... //
public function featurevalue(){
  $featid            = $this->input->post('featid');
  $parameter         = array('act_mode'=>'catfeatu','row_id'=>$featid,'feaid'=>'','feavalue'=>'','catid'=>'');
  $response['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter); 

  $str = '';

  foreach($response['vieww'] as $k=>$v){   
   # $str .= "<option value=".$v->id.">".$v->attvalue."</option>";
    if(!empty($v->LeftValue)){
      $str .= '<option value="'.$v->FeatureLeftID.'">'.$v->LeftValue.' </option>';
    }
  }
  echo $str;

}

//............. Feature View Category ............... //
public function featureviewcat(){
  $this->userfunction->loginAdminvalidation();
  $parameter         = array('act_mode'=>'vcf','row_id'=>'','feaid'=>'','feavalue'=>'','catid'=>'');
  $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter); 	

  //----------------  Download  Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Feature Name','Feature Left Value','Category Name');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Feature Category Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->FeatureName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->LeftValue);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->catname);
           
            }
          }

          $filename='Feature Category.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------//

      //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/feature/featureviewcat?";
      $config['total_rows']       = count($responce['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

      $parameter         = array('act_mode'=>'vcf','row_id'=>$page,'feaid'=>$second,'feavalue'=>'','catid'=>'');
      #$responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);  

    //----------------  end pagination ------------------------// 

  $this->load->view('helper/header');
  $this->load->view('feature/viewfaeturecat',$responce);		

}

//............. Feature Delete ............... //
public function featurecdelete($id){
  $parameter         = array('act_mode'=>'deletefc','row_id'=>$id,'feaid'=>'','feavalue'=>'','catid'=>'');
  $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter); 
  redirect("admin/feature/featureviewcat"); 	
}
//-------------------------------------end ----------------------------------------------------------------//


//............. Add Feature Right ............... //
public function addfeatureright(){
  $this->userfunction->loginAdminvalidation();
 if($this->input->post('submit')){

  //pend($_POST);
  // foreach ($this->input->post('rightvalue') as $key => $value) {

   $userid          = $this->session->userdata('bizzadmin')->LoginID; 
   $parameter       = array('act_mode'=>'featcheck','row_id'=>'','feaid'=>$this->input->post('featid'),'feavalue'=>$value,'catid'=>'');
   $record['record']= $this->supper_admin->call_procedureRow('proc_feature',$parameter);
    if($record['record']->attcount>0){
     $this->session->set_flashdata("message", "Feature Already Exists");
     redirect("admin/feature/addfeature");
    }
   else{
 foreach ($this->input->post('rightvalue') as $key => $value) {

   $param = array(
     'act_mode' => 'insertright',
    'row_id' => $this->input->post('pro_cat'),
    'cat_id' =>$this->input->post('sec_cat'),
    'name' => $value,
    'sec_order' => '',
    'media_type' => '',
    'img_name' => '',
    'img_url' => '',
    'img_head' => '',
    'imgshead' => ''
  );
 //  pend($param);
   $responce['abc'] = $this->supper_admin->call_procedure('proc_overview',$param);
   }  
    $this->session->set_flashdata("message", "Your information was successfully Saved.");
  redirect("admin/feature/viewfeaturemasterright");
 

 }
  }
 
   
 
 $parameter          = array('act_mode'=>'featleftview','row_id'=>'','feaid'=>'','feavalue'=>'','catid'=>'');
 $responce['vieww']  = $this->supper_admin->call_procedure('proc_feature',$parameter); 
 $parameterr         = array('act_mode'=>'catview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
 
 $responce['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr);  




 $this->load->view('helper/header');
 $this->load->view('feature/addfeatureright',$responce);  

}

  public function featureLeft()
  {
      $param = array('act_mode' => 'getLeft',
                'row_id' => '0',
                'cat_id' => $this->input->post('ids'),
                'name' => '',
                'sec_order' => '0',
                'media_type' => '',
                'img_name' => '',
                'img_url' => '',
                'img_head' => '',
                'imgshead' => '');
  
      $data['getProductName'] = $this->supper_admin->call_procedure('proc_overview', $param);
      $proData = '<option value="">Select Type</option>';

      foreach ($data['getProductName'] as $key => $value) {
          $proData .= '<option value="'.$value->FeatureLeftID.'">'.$value->LeftValue.'</option>';
      }
      echo $proData;
  } 


  public function featureRight()
  {    
      $param = array('act_mode' => 'getRight',
                    'row_id' => '0',
                    'cat_id' => $this->input->post('ids'),
                    'name' => '',
                    'sec_order' => '0',
                    'media_type' => '',
                    'img_name' => '',
                    'img_url' => '',
                    'img_head' => '',
                    'imgshead' => '');
                  
      $data['getProductName'] = $this->supper_admin->call_procedure('proc_overview', $param);
      
      $proData = '<option value="">Select Type</option>';
      foreach ($data['getProductName'] as $key => $value) {
          $proData .= '<option value="'.$value->RightFeatureID.'">'.$value->RightValue.'</option>';
      }
      echo $proData;

  } 


//............. Feature Master Right ............... //
 public function viewfeaturemasterright(){
    $this->userfunction->loginAdminvalidation();
    //----------------------multiple delete -------------------------------//
    if($this->input->post('submit')){
     
    foreach($this->input->post( 'attdelete') as $key => $value){
      $parameter         = array('act_mode'=>'rightdelete','row_id'=>$value,'feaid'=>'','feavalue'=>'','catid'=>'');
      $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);
    }
      $this->session->set_flashdata("message", "Your information was successfully delete.");
      redirect("admin/feature/viewfeaturemasterright");
    }

   //--------------------------multiple ststus ------------------------------//
    if($this->input->post('submitstatus')){
     foreach ($this->input->post( 'attdelete') as $key => $value) {
      $status            = $this->input->post('attstatu')[$value];

      $act_mode          = $status == 'A'?'activeright':'inactiveright';
      $parameter2        = array('act_mode'=>$act_mode,'row_id'=>$value,'feaid'=>'','feavalue'=>'','catid'=>'');
      $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter2);
  }
      $this->session->set_flashdata("message", "Your Status was successfully Updated.");
      redirect("admin/feature/viewfeaturemasterright");
    }

    //-------------------------------end ----------------------------------------//
    $parameter         = array('act_mode'=>'rightview','row_id'=>'','feaid'=>'','feavalue'=>'','catid'=>'');
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);


      //----------------  Download  Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Feature Type','Left Feature Name','Right Feature Name');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Feature Right Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->FeatureName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->LeftValue);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->RightValue);
           
            }
          }

          $filename='Feature Right.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------// 
    
    $this->load->view('helper/header');
    $this->load->view('feature/viewfeaturemasterright',$responce);

  }

//............. Feature Master Delete Right ............... //
 public function featuremasterdeleteright($id){
   $parameter       = array('act_mode'=>'rightdelete','row_id'=>$id,'feaid'=>'','feavalue'=>'','catid'=>'');
   $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter);
   $this->session->set_flashdata("message", "Your information was successfully delete.");
   redirect("admin/feature/viewfeaturemasterright");   
 
  }

//............. Feature Master Right Status ............... //
 public function featuremasterstatusright ($id){
   $rowid             = $this->uri->segment(4);
   $status            = $this->uri->segment(5);
   $act_mode          = $status == 'A'?'activeright':'inactiveright';
   $parameter2        = array('act_mode'=>$act_mode,'row_id'=>$rowid,'feaid'=>'','feavalue'=>'','catid'=>'');
   $responce['vieww'] = $this->supper_admin->call_procedure('proc_feature',$parameter2);
   $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
   redirect(base_url().'admin/feature/viewfeaturemasterright');

}  

//............. Feature Master Update Right ............... //
public function featuremasterupdateright($id){
 if($this->input->post('submit')){
  $featname         = $this->input->post('featname');
  $parameter        = array('act_mode'=>'rightupdate','row_id'=>$id,'feaid'=>'','feavalue'=>$featname,'catid'=>'');
  $record['record'] = $this->supper_admin->call_procedureRow('proc_feature',$parameter);
  $this->session->set_flashdata('message', 'Your information was successfully Updated.');
  redirect(base_url() . 'admin/feature/viewfeaturemasterright');
 }

  $parameter         = array('act_mode'=>'rightviewId','row_id'=>$id,'feaid'=>'','feavalue'=>'','catid'=>'');
  $responce['vieww'] = $this->supper_admin->call_procedureRow('proc_feature',$parameter);
  $this->load->view('helper/header');
  $this->load->view('feature/editright',$responce);
  
}

//--------------------------------------End Right feature ---------------------------------------------------//

}


?>