<?php 
if (!defined('BASEPATH')) 
    exit('No direct script access allowed'); 

class Discount extends MX_Controller { 

    public function __construct() { 
       
        
        $this->load->model("supper_admin"); 
        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel_IOFactory');
        $this->userfunction->loginAdminvalidation();

    } 

    public function index() { 

        $this->userfunction->loginAdminvalidation();
        if ($this->input->post('submit')) { 
            alert("here");exit;
            $this->form_validation->set_rules('discname', 'Discount Name', 'trim|required|xss_clean'); 
            $this->form_validation->set_rules('offertype', 'Choose Disocunt Or Coupon', 'trim|required|xss_clean'); 
 
            //$this->form_validation->set_rules('disctype','Discount Type','trim|required|xss_clean'); 
            if ($this->form_validation->run() != false) { 
                $xml = null; 
                if ($this->input->post('discountid') != '') { 
                    $xml.='<NewDataSet>'; 
                    foreach ($this->input->post('discountid') AS $list) { 
                        $xml.='<tblcouponmap>'; 
                        $xml.='<CatIdBrandIdProId>' . $list . '</CatIdBrandIdProId>'; 
                        $xml.='<UserId>' . $this->input->post('username') . '</UserId>'; 
                        $xml.='<Discounton>' . $this->input->post('discon') . '</Discounton>'; 
                        $xml.='</tblcouponmap>'; 
                    } 
                    $xml.='</NewDataSet>'; 
                } 
                $parameter = array('DiscountName' => $this->input->post('discname'), 
                    'OfferType' => $this->input->post('offertype'), 
                    'CouponCode' => $this->input->post('couponcode'), 
                    'PurchaseAmt' => $this->input->post('shopamount'), 
                    'NumOfUses' => $this->input->post('usespercoupon'), 
                    'UserType' => $this->input->post('usertype'), 
                    'CouponTypeId' => $this->input->post('coupontype'), 
                    'UserId' => $this->input->post('userid'), 
                    'Resuable' => $this->input->post('reusable'), 
                    'LocationId' => $this->input->post('location'), 
                    'FreeShipping' => $this->input->post('freeship'), 
                    'DiscountType' => $this->input->post('disctype'), 
                    'DiscountAmt' => $this->input->post('discamount'), 
                    'DiscountOn' => $this->input->post('discon'), 
                    'StartDate' => $this->input->post('startdate'), 
                    'EndDate' => $this->input->post('enddate'), 
                    'VenderId' => $this->input->post('vendername'), 
                    'CreatedBy' => 1, //$this->input->post(1), 
                    'DiscStatus' => $this->input->post('status'), 
                    'xml' => $xml 
                ); 
               
                //$path  = api_url().'discount/discountpost/format/json/'; 
                //$data  = curlpost($parameter,$path); 
                $data = $this->supper_admin->call_procedureRow('proc_Discounton', $parameter); 
                if ($data->lastid > 0) { 
                    $this->session->set_flashdata("message", "Successfully inserted"); 
                    redirect('admin/discount/discountlist'); 
                } else 
                    echo $data; 
            } 
        } 
        $this->load->view('helper/header'); 
        $this->load->view('discount/adddiscount'); 
        $this->load->view('helper/footer'); 
    } 

    public function discountAttr() { 
        
        $disType = $_POST['disType']; 
        $parameter = array(); 
        $data['users'] = $this->supper_admin->call_procedure('proc_getAllUsers', $parameter); 
        
        //p($data['users']); exit();
        //echo $disType; exit(); 
        if ($disType == 'percentage') { 
            $this->load->view('discount/discountAttr', $data); 
        } else if ($disType == 'fixed') { 
            $this->load->view('discount/flatAttr', $data); 
        } 
    } 




    public function cart() { 

        $this->userfunction->loginAdminvalidation();
        $this->load->view('helper/header'); 
        $this->load->view('discount/cartdiscount'); 
        //$this->load->view('helper/footer'); 
    } 

    public function get_unique_code() { 
       
        $code = 'shop'; 
        $num = rand(111, 999); 
        $string = "cou"; 
        $str = str_shuffle($num . $string); 
        $uniqueCode = $code . $str; 
        $parameter = array('p_code' => $uniqueCode); 
        $uCode = $this->supper_admin->call_procedureRow('proc_getUniqueCode', $parameter); 
        if (empty($ucode)) { 
            echo strtoupper($uniqueCode); 
        } else { 
            echo base_url() . 'admin/discount/get_unique_code'; 
        } 
    }

    //fn added by manishkr 18 july
      public function get_unique_code_bizz() { 
       
        $code = 'hmt'; 
        $num = rand(111, 999); 
        $string = "cou"; 
        $str = str_shuffle($num . $string); 
        $uniqueCode = $code . $str; 
        $parameter = array('p_code' => $uniqueCode); 
        $uCode = $this->supper_admin->call_procedureRow('proc_getUniqueCode', $parameter); 
        if (empty($ucode)) { 
            echo strtoupper($uniqueCode); 
        } else { 
            echo base_url() . 'admin/discount/get_unique_code_bizz'; 
        } 
    }


  /*  public function saveCartDisc() { 
        $parameter = array(
                            'p_offer' => 'cart', 
                            'p_amt' => $_POST['disAmt'], 
                            'p_type' => $_POST['disTyp'], 
                            'p_pamt' => $_POST['purAmt'], 
                            'p_start' => $_POST['startDate'], 
                            'p_end' => $_POST['endDate'], 
                            'p_coupon' => $_POST['coupon'], 
                            'p_limit' => $_POST['limit'], 
                            'p_reuse' => $_POST['reuse'], 
                            'p_usableLimit' => $_POST['usableLimit'], 
                            'p_disc_name' => $_POST['discname'],
                            'p_categorycart_discount' => $_POST['categorydiscountid'],
                            'p_discountvalues'  => $_POST['discountonvalue'],
                            'p_brandproductdiscunt' => $_POST['discountbrandproduct'],
                            'p_productid'           =>  $_POST['productid']
                        ); 
        //p($parameter); exit; 
        
        $data['disc'] = $this->supper_admin->call_procedureRow('proc_insertDisc', $parameter); 
    }   */

    public function discountlist() 
    { 
        $this->userfunction->loginAdminvalidation();
        $this->load->view('helper/header'); 
        //$path  = api_url().'discount/discountlist/format/json/'; 
        $data['result'] = $this->supper_admin->call_procedure('proc_DiscountList'); //curlget($path); 
        //p($data['result']);exit; 

        //----------------  Download Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Discount Name','Discount Type','Coupon Code','Discount ON','Start Date','End Date','Discount','Purchase Amount');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Discount Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($data['result'] as $key => $value) {
             
            $newvar = $j+$key;

                       if ($value->DiscountType == 'percentage')
                            $type = '%';
                        else if ($value->DiscountType == 'fixed')
                            $type = 'INR';
                        else
                            $type = '';

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->t_disc_name);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->DiscountName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->CouponCode);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->DiscountOn);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->StartDate);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->EndDate);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->DiscountAmt. '('.$type.')');
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->PurchaseAmt);
            }
          }

          $filename='Discount.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------// 

        $this->load->view('discount/discountlist', $data); 

        //$this->load->view('helper/footer'); 
        //print_r($data['result']); 
     } 


    public function view() {
        $this->userfunction->loginAdminvalidation(); 
        $disId = $this->uri->segment(4); 
        $err = false; 
        if($disId == '' || $disId <= 0) { 
            $err = true; 
        } 
        $parameter = array('id' => $disId); 
        #print 'hi';exit;
        $data['discount'] = $this->supper_admin->call_procedureRow('proc_getDiscountDetails', $parameter); 
        if(count($data) > 0 && !$err) { 
            $this->load->view('helper/header'); 
            $this->load->view('discount/viewdiscount', $data); 
            //$this->load->view('helper/footer'); 
        } else { 
            redirect(base_url().'admin/discount/discountlist'); 
        } 
        
    }

    function discount_new_activeinactive(){
        $rowid = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        
        $act_mode = ($status == "inactive")?"A":"I";

        $param = array('act_mode'=>$act_mode,'row_id'=>$rowid);
        #p($param);exit;
        $result['data'] = $this->supper_admin->call_procedureRow('proc_discountstatus', $param);
        redirect("admin/discount/discountlist");
    }

    // fn added by manishkr
  

    function discount_delete()
    {
       $rowid =  $this->uri->segment(4);
       $param = array('act_mode'=>'delete','row_id'=>$rowid);
       $result['data'] = $this->supper_admin->call_procedureRow('proc_discountstatus', $param);
       redirect("admin/discount/discountlist");

    }

    public function filterDiscount() { 
        //p($_POST); exit();
        foreach ($_POST as $val) { 
            $parameter = array('coupon' => $val['Coupon'], 'startDate' => $val['start'], 'endDate' => $val['end'], 'status' => $val['status']); 
            $data['discount'] = $this->supper_admin->call_procedure('proc_getFilterDiscountList', $parameter); 
            //p($data['discount']); exit();
        } 
        $this->load->view('discount/disprolistingfilter', $data); 
    } 

    public function discnew(){
    $parameter = array('act_mode'=>'all','id'=>"0");
    $data['result']=$this->supper_admin->call_procedure('proc_getcoupan_notapprove',$parameter);
//p($data['result']);
//exit;
  $this->load->view('helper/header'); 
  $this->load->view('discount/newdiscapprove',$data); 
    $this->load->view('helper/footer'); 

}

    public function get_sub_category(){
        
        $parent_id = $_POST['parent_id'];
        if($parent_id!="none")
        {

        }

        $parameter = array('act_mode'=>'get_sub_category','id'=>$parent_id); 
        $sub_cat = $this->supper_admin->call_procedure('Proc_GetCatgories', $parameter); 
        $html = "<select id='sub_category' name = 'sub_category[]' multiple>";
                  //<option value='none'>Select any Sub Category </option>";
        foreach($sub_cat as $key => $value)
        {
            $html .= "<option value = '".$value->childid."'>".$value->catname."</option>";
        }
        $html .= "</select>";
        echo $html; die;
    }

    public function hm_get_sub_category(){
        
        $parent_id = $_POST['parent_id'];
        if($parent_id!="none")
        {

        }

        $parameter = array('act_mode'=>'hm_get_sub_category','id'=>$parent_id); 
        $sub_cat = $this->supper_admin->call_procedure('Proc_GetCatgories', $parameter); 
        $html = "<option value='none'>Select any Sub Category</option>";
                  //<option value='none'>Select any Sub Category </option>";
        foreach($sub_cat as $key => $value)
        {
            $html .= "<option value = '".$value->childid."'>".$value->catname."</option>";
        }
        echo $html; die;
    }

    public function hm_get_subchild_category(){
        
        $parent_id = $_POST['parent_id'];
        if($parent_id!="none")
        {

        }

        $parameter = array('act_mode'=>'hm_get_subchild_category','id'=>$parent_id); 
        $sub_cat = $this->supper_admin->call_procedure('Proc_GetCatgories', $parameter); 
        $html = "<select id='sub_category' name = 'sub_category[]'>";
                  //<option value='none'>Select any Sub Category </option>";
        foreach($sub_cat as $key => $value)
        {
            $html .= "<option value = '".$value->childid."'>".$value->catname."</option>";
        }
        $html .= "</select>";
        echo $html; die;
    }
    public function get_sub_category_edit()
    {
        $parent_id = $_POST['parent_id'];
        if($parent_id!="none")
        {

        }

        $parameter = array('act_mode'=>'get_sub_category','id'=>$parent_id); 
        $sub_cat = $this->supper_admin->call_procedure('Proc_GetCatgories', $parameter); 
        $html = "<select id='sub_category' name = 'sub_category[]' multiple>";
                  //<option value='none'>Select any Sub Category </option>";
        foreach($sub_cat as $key => $value)
        {
            $html .= "<option value = '".$value->childid."'>".$value->catname."</option>";
        }
        $html .= "</select>";
        echo $html; die;   
    }
    public function getList() { 
       
       $type = $_POST['type']; 
        
        //echo $type; exit; 

        $data['type'] = $type; 
        if ($type == 'brands') { 
        
            $parameter = array(); 
            $brands = $this->supper_admin->call_procedure('Proc_GetBrandforexcel', $parameter); 
           // p(count($brands)); 
        } else if ($type == 'categories') { 
        
            $parameter = array('act_mode'=>'maincategory','id'=>''); 
            $categories = $this->supper_admin->call_procedure('Proc_GetCatgories', $parameter); 
            //p($categories); exit;
        } else if ($type == 'product') { 
        
            $parameter = array('act_mode' => 'all', 'p_brandCatId' => 0, 'p_status' => 0); 
            $products = $this->supper_admin->call_procedure('proc_GetProductsByBrandCategory', $parameter); 
            //p($products); die;

        } else if ($type == 'all-product') { 
            $data['list'] = array(); 
        } 

       if (isset($categories)) { 
            foreach ($categories as $key => $val) { 
                $data['list'][] = array('id' => $val->CatId, 'name' => $val->CatName); 
                
            } 
        } 
         //$this->load->view('discount/listingview', $data); 
        if (isset($brands)) { 
            foreach ($brands as $key => $val) { 
                $data['list'][] = array('id' => $val->id, 'name' => $val->brandname); 
            }
            
        } 

        if (isset($products)) { 
            foreach ($products as $key => $val) { 
                //p($val); 
                $data['list'][] = array('id' => $val->proid, 'name' => $val->proname, 'discount' => $val->discount); 
                //p($data['list']); exit;
            } 
        } 
        if ($type == 'product') { 
            $data['products'] = $products; 
            $this->load->view('discount/productlisting', $data); 
        }
        else if($type == 'categories'){
            $this->load->view('discount/category_listing_view', $data);    
        } 
        else if($type == 'brands') { 
            $this->load->view('discount/listingview', $data); 
        }
    }

   /* public function getSubList() { 
        $type = $_POST['type']; 
        
         $catId = $_POST['catId'];
         
        if($type=='categories')
        {
            
            $catId =  $catId[0]['id'];
            
        }else{

        if(isset($_POST['catId'])) { 
            $catId = '^(' . implode('|', $_POST['catId']) . ')$'; 
        } else { 
            $catId = ''; 
        } 
     }   

        if ($type == 'categories' || $type == 'subcategory') { 

                $parameter = array('act_mode'=>"subcategory",'id' => $catId);
               
                
                $subcat =   $this->supper_admin->call_procedure('Proc_GetCatgories',$parameter);

             
            //$parameter = array('act_mode'=>"category",'id' => $catId);
            
            //$parameter = array('p_parentId' => $catId, 'p_catlevel' => 0); 
            //$subcat = $this->supper_admin->call_procedure('proc_GetSubCategory', $parameter); 
            
            //$subcat = $this->supper_admin->call_procedure('Proc_GetCatgories', $parameter); 

            

            foreach ($subcat as $key => $val) { 
            
                //$Catlevel = $val->Catlevel; 
                $data['list'][] = array('CatId' => $val->CatId, 'CatName' => $val->CatName); 
            } 
             $this->load->view('discount/subcategoryview',$data);   

             //$this->load->view('discount/listingview', $data); 
           /* if (!isset($Catlevel)) { 
                $data['type'] = 'products'; 
            } else if ($Catlevel == 1) { 
                $data['type'] = 'subcategory'; 
            } else if ($Catlevel == 2) { 
                $data['type'] = 'all'; 
            } */
            /*if (isset($Catlevel)) { 
                $this->load->view('discount/listingview', $data); 
            } else { 
                $data['list'] = array(); 
                $parameter = array('act_mode' => 'allByVendor', 'p_brandCatId' => $catId, 'p_status' => 0); 
                $data['products'] = $this->supper_admin->call_procedure('proc_GetProductsByBrandCategory', $parameter); 
                $this->load->view('discount/productlisting', $data); 
            } 
        } else { 
            if ($type == 'all') { 
                $parameter = array('act_mode' => 'categories', 'p_brandCatId' => $catId, 'p_status' => 0); 
            } else { 
                $parameter = array('act_mode' => 'brands', 'p_brandCatId' => $catId, 'p_status' => 0); 
                //p($parameter); exit;
            } 
            $data['products'] = $this->supper_admin->call_procedure('proc_GetProductsByBrandCategory', $parameter); 
            $this->load->view('discount/productlisting', $data); 
        } 
    } */



    public function register() { 
        $this->userfunction->loginAdminvalidation();
        //$parameter = array('act_mode' => 'all'); 
        //$data['cities'] = $this->supper_admin->call_procedure('proc_cities', $parameter); 
        //p($data);exit; 
        //, $data
        $this->load->view('helper/header'); 
        $this->load->view('discount/registerDiscount'); 
        //$this->load->view('helper/footer'); 
    } 
    
    // new fn added by manishkr
    public function cart_discount() {
        $this->userfunction->loginAdminvalidation();
        //$parameter = array('act_mode' => 'all'); 
        //$data['cities'] = $this->supper_admin->call_procedure('proc_cities', $parameter); 
        //p($data);exit; 
        //, $data
        $this->load->view('helper/header'); 
        $this->load->view('discount/cart_discount'); 
        //$this->load->view('helper/footer'); 

    }

    public function product_discount() // It includes Brand, Category and Product discount
    {
         
        $this->userfunction->loginAdminvalidation();
        
        $this->load->view('helper/header'); 
        $this->load->view('discount/product_discount'); 
    }

    public function discount_update()
    {
            $rowid =  $this->uri->segment(4);
            $prod_type = trim($this->uri->segment(5));

            $param = array('act_mode' => 'view_all_id',
                                'discount_name' => '',
                                'discount_value' => '', 
                                'discount_type' => '', 
                                'purchase_amount' => '', 
                                'is_coupon' => 0, 
                                'coupon' => '', 
                                'is_reusable' => $rowid, 
                                'reusable_limit' => '', 
                                'valid_from' => '', 
                                'valid_to' => '', 
                                'coupon_user_type' => '',
                                'row_id' => '',
                                'brand_list' => '',
                                'product_type' => '',
                                'parent_id' => ''
                                );
           $result['detail'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $param);
           $result['product_type'] = $prod_type;
           if($prod_type == 'products') { 
                $parameter = array('act_mode' => 'all', 'p_brandCatId' => 0, 'p_status' => 0); 
                $result['all_products'] = $this->supper_admin->call_procedure('proc_GetProductsByBrandCategory', $parameter); 
                //p($result['all_products']); die;
            }
           else if($prod_type == 'brands'){
               $parameter = array(); 
                $result['all_brands'] = $this->supper_admin->call_procedure('Proc_GetBrandforexcel', $parameter);
                //p($result['all_brands']); die;
           }
           else if($prod_type == 'category') {
                $parameter = array('act_mode'=>'hm_get_main_parent','id'=>$result['detail']->parent_id); 
                $result['hm_main_parent'] = $this->supper_admin->call_procedureRow('Proc_GetCatgories', $parameter);

                $parameter = array('act_mode'=>'maincategory','id'=>''); 
                $result['p_all_category'] = $this->supper_admin->call_procedure('Proc_GetCatgories', $parameter);

                $parameter = array('act_mode'=>'hm_get_sub_category','id'=>$result['hm_main_parent']->parentid); 
                $result['all_category'] = $this->supper_admin->call_procedure('Proc_GetCatgories', $parameter); 

                $parameter = array('act_mode'=>'hm_get_subchild_category','id'=>$result['detail']->parent_id);
                $result['sub_category'] = $this->supper_admin->call_procedure('Proc_GetCatgories', $parameter);
                //p($result['sub_category']); die;  
                
                //p($result['sub_category']); die;
                //p($result['all_category']); die;
            }
           //p($products); die;
           $this->load->view('helper/header'); 
           $this->load->view('discount/product_discount_edit', $result); 
    }

    public function active_inactive()
    {
        $loginid=$this->session->userdata('bizzadmin')->LoginID;
        $rowid =  $this->uri->segment(4);
        //$prod_type = $this->uri->segment(5);
        $status = $this->uri->segment(5);
        $param = array('act_mode' => 'all_active_inactive',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => $status, 
                            'coupon' => '', 
                            'is_reusable' => $rowid, 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => $loginid,
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id' =>'',
                            );


        $result['data'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $param);
        redirect("admin/discount/all_discount_list");  
    }

    public function updateProductDiscount($type)
    {
        $loginid=$this->session->userdata('bizzadmin')->LoginID;
        $parameter = array();
        if($type == 'brands')
        {
            $parameter = array('act_mode' => 'update_product_discount',
                    'discount_name' => $_POST['discount_name'],
                    'discount_value' => $_POST['discount_value'], 
                    'discount_type' => $_POST['discount_type'], 
                    'purchase_amount' => $_POST['purchase_amount'],
                    'is_coupon' => $_POST['is_coupon'], 
                    'coupon' => $_POST['coupon'], 
                    'is_reusable' => $_POST['is_reusable'], 
                    'reusable_limit' => $_POST['reusable_limit'], 
                    'valid_from' => $_POST['valid_from'], 
                    'valid_to' => $_POST['valid_to'], 
                    'coupon_user_type' => $_POST['coupon_user_type'],
                    'row_id' => $_POST['unique_id'],
                    'brand_list' => $_POST['brand_list'],
                    'product_type' => 'brands',
                    'parent_id' => $_POST['purchase_amount_max'],
                    'param1' => $_POST['fixed_discount_price'],
                    'param2' => $loginid,
                    'param3' => $_POST['valid_from_time'],
                    'param4' => $_POST['valid_to_time']
                    );
             //p($parameter); die;
        }
        else if($type == 'categories')
        {
            $parameter = array(
                    'act_mode' => 'update_product_discount',
                    'discount_name' => $_POST['discount_name'],
                    'discount_value' => $_POST['discount_value'], 
                    'discount_type' => $_POST['discount_type'], 
                    'purchase_amount' => $_POST['purchase_amount'],
                    'is_coupon' => $_POST['is_coupon'], 
                    'coupon' => $_POST['coupon'], 
                    'is_reusable' => $_POST['is_reusable'], 
                    'reusable_limit' => $_POST['reusable_limit'], 
                    'valid_from' => $_POST['valid_from'], 
                    'valid_to' => $_POST['valid_to'], 
                    'coupon_user_type' => $_POST['coupon_user_type'],
                    'row_id' => $_POST['unique_id'],
                    'brand_list' => $_POST['category_list'],
                    'product_type' => 'category',
                    'parent_id'    => $_POST['parent_id'],
                    'param1' => $_POST['fixed_discount_price'],
                    'param2' => $_POST['purchase_amount_max'].','.$loginid,
                    'param3' => $_POST['valid_from_time'],
                    'param4' => $_POST['valid_to_time']
                    );
        }
        else if($type == 'product')
        {
               //serialized product map id and product id
               $p_list_str = $_POST['product_list'];
               $str_to_array = explode(',', $p_list_str);
               //p($str_to_array); die;
               $ser_array = array();

               $k = 0;
               $j = 0;
               for ($k = 0; $k<count($str_to_array)/2; $k++)
               {
                    $ser_array[$k] = array(
                     'promapid' =>  $str_to_array[$j++],  
                     'proid'    =>  $str_to_array[$j++]
                    );
               } 

               //----------------------


               $parameter = array(
                    'act_mode' => 'update_product_discount',
                    'discount_name' => $_POST['discount_name'],
                    'discount_value' => $_POST['discount_value'], 
                    'discount_type' => $_POST['discount_type'], 
                    'purchase_amount' => $_POST['purchase_amount'],
                    'is_coupon' => $_POST['is_coupon'], 
                    'coupon' => $_POST['coupon'], 
                    'is_reusable' => $_POST['is_reusable'], 
                    'reusable_limit' => $_POST['reusable_limit'], 
                    'valid_from' => $_POST['valid_from'], 
                    'valid_to' => $_POST['valid_to'], 
                    'coupon_user_type' => $_POST['coupon_user_type'],
                    'row_id' => $_POST['unique_id'],
                    'brand_list' => serialize($ser_array),
                    'product_type' => 'products',
                    'parent_id'    =>$_POST['purchase_amount_max'],
                    'param1' => $_POST['fixed_discount_price'],
                    'param2' => $loginid,
                    'param3' => $_POST['valid_from_time'],
                    'param4' => $_POST['valid_to_time']
                    );
        }
       
        $data['stored_flag'] = $this->supper_admin->call_procedureRow('proc_discountcart_sec', $parameter);    
    }

    public function saveProductDiscount($type) 
    {
       $loginid=$this->session->userdata('bizzadmin')->LoginID;
        $parameter = array();
        if($type == 'brands')
        {
            $parameter = array('act_mode' => 'insert_product_discount',
                    'discount_name' => $_POST['discount_name'],
                    'discount_value' => $_POST['discount_value'], 
                    'discount_type' => $_POST['discount_type'], 
                    'purchase_amount' => $_POST['purchase_amount'],
                    'is_coupon' => $_POST['is_coupon'], 
                    'coupon' => $_POST['coupon'], 
                    'is_reusable' => $_POST['is_reusable'], 
                    'reusable_limit' => $_POST['reusable_limit'], 
                    'valid_from' => $_POST['valid_from'], 
                    'valid_to' => $_POST['valid_to'], 
                    'coupon_user_type' => $_POST['coupon_user_type'],
                    'row_id' => '',
                    'brand_list' => $_POST['brand_list'],
                    'product_type' => 'brands',
                    'parent_id'    => $_POST['purchase_amount_max'],
                    'param1' => $_POST['fixed_discount_price'],
                    'param2' => $loginid,
                    'param3' => $_POST['valid_from_time'],
                    'param4' => $_POST['valid_to_time']
                    );
        }
        else if($type == 'categories')
        {
            $parameter = array(
                    'act_mode' => 'insert_product_discount',
                    'discount_name' => $_POST['discount_name'],
                    'discount_value' => $_POST['discount_value'], 
                    'discount_type' => $_POST['discount_type'], 
                    'purchase_amount' => $_POST['purchase_amount'],
                    'is_coupon' => $_POST['is_coupon'], 
                    'coupon' => $_POST['coupon'], 
                    'is_reusable' => $_POST['is_reusable'], 
                    'reusable_limit' => $_POST['reusable_limit'], 
                    'valid_from' => $_POST['valid_from'], 
                    'valid_to' => $_POST['valid_to'], 
                    'coupon_user_type' => $_POST['coupon_user_type'],
                    'row_id' =>  $_POST['parent_id'],
                    'brand_list' => $_POST['category_list'],
                    'product_type' => 'category',
                    'parent_id'    => $_POST['purchase_amount_max'],
                    'param1' => $_POST['fixed_discount_price'],
                    'param2' => $loginid,
                    'param3' => $_POST['valid_from_time'],
                    'param4' => $_POST['valid_to_time']
                    );
        }
        else if($type == 'product')
        {
               //serialized product map id and product id
               $p_list_str = $_POST['product_list'];
               $str_to_array = explode(',', $p_list_str);
               //p($str_to_array); die;
               $ser_array = array();

               $k = 0;
               $j = 0;
               for ($k = 0; $k<count($str_to_array)/2; $k++)
               {
                    $ser_array[$k] = array(
                     'promapid' =>  $str_to_array[$j++],  
                     'proid'    =>  $str_to_array[$j++]
                    );
               } 

               //----------------------


               $parameter = array(
                    'act_mode' => 'insert_product_discount',
                    'discount_name' => $_POST['discount_name'],
                    'discount_value' => $_POST['discount_value'], 
                    'discount_type' => $_POST['discount_type'], 
                    'purchase_amount' => $_POST['purchase_amount'],
                    'is_coupon' => $_POST['is_coupon'], 
                    'coupon' => $_POST['coupon'], 
                    'is_reusable' => $_POST['is_reusable'], 
                    'reusable_limit' => $_POST['reusable_limit'], 
                    'valid_from' => $_POST['valid_from'], 
                    'valid_to' => $_POST['valid_to'], 
                    'coupon_user_type' => $_POST['coupon_user_type'],
                    'row_id' => '',
                    'brand_list' => serialize($ser_array),
                    'product_type' => 'products',
                    'parent_id'     => $_POST['purchase_amount_max'],
                    'param1' => $_POST['fixed_discount_price'],
                    'param2' => $loginid,
                    'param3' => $_POST['valid_from_time'],
                    'param4' => $_POST['valid_to_time']
                    );


        }
       
        $data['stored_flag'] = $this->supper_admin->call_procedureRow('proc_discountcart_sec', $parameter); 
    }
    public function product_discount_list($prod_type) {

        $parameter = array('act_mode' => 'view_product_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => $prod_type,
                            'parent_id' => '',    
                            ); 

         $data['view_type'] = 'brands';
         $data['brands_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);

         //p($data['brands_detail']); die;
         
        $this->load->view('helper/header'); 
        $this->load->view('discount/product_discount_list', $data); 
    
    }
    public function cart_discount_list() // listing all coupoun cart 
     {
         $parameter = array('act_mode' => 'view_cart_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id'   => ''
                            ); 
         $data['cart_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);
         $this->load->view('helper/header'); 
        $this->load->view('discount/cart_discount_list', $data); 
     }
     
     public function all_discount_list() {
       $parameter = array('act_mode' => 'get_all_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => 'all',
                            'parent_id' => ''    
                            ); 

        // $data['view_type'] = 'brands';
         $data['discount_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);
         $parameter = array('act_mode' => 'get_all_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => 'brands',
                            'parent_id' => ''    
                            ); 

         //$data['view_type'] = 'brands';
         $data['brand_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);

         $parameter = array('act_mode' => 'get_all_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => 'category',
                            'parent_id' => ''    
                            ); 

        //$data['view_type'] = 'brands';
         $data['category_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);
         $parameter = array('act_mode' => 'get_all_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => 'products',
                            'parent_id' => ''    
                            ); 

        // $data['view_type'] = 'brands';
         $data['product_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);

        
        $this->load->view('helper/header');  
        $this->load->view('discount/all_discount_list',$data); 
    }
    public function cart_search()
    {
        $search_term = trim($this->input->post('search_term'));

        $parameter = array('act_mode' => 'get_search_cart_data',
                            'discount_name' => $search_term,
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id'   => ''
                            ); 
        
         $data['cart_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);
        
         $this->load->view('helper/header'); 
        $this->load->view('discount/cart_discount_list', $data); 

    }
   public function search()
    {
      
         $search_term = trim($this->input->post('search_term'));
         $parameter = array('act_mode' => 'get_search_data',
                            'discount_name' => $search_term,
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id' => ''    
                            ); 

         $data['view_type'] = 'brands';
         $data['discount_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);

         $parameter = array('act_mode' => 'get_all_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => 'brands',
                            'parent_id' => ''    
                            ); 

         //$data['view_type'] = 'brands';
         $data['brand_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);

         $parameter = array('act_mode' => 'get_all_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => 'category',
                            'parent_id' => ''    
                            ); 

        //$data['view_type'] = 'brands';
         $data['category_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);
         $parameter = array('act_mode' => 'get_all_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => 'products',
                            'parent_id' => ''    
                            ); 

        // $data['view_type'] = 'brands';
         $data['product_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);
         
         $this->load->view('helper/header');  
         $this->load->view('discount/all_discount_list',$data); 
    }
    public function ajax_discount_list() //ajax fn that update brands,category, product accordingly
    {
        $prod_type = $_POST['type'];

        $parameter = array('act_mode' => 'view_product_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => $prod_type,
                            'parent_id'   => '',    
                            ); 

            $data['type_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);
            


            //p($data['type_detail']); die;
            if($prod_type == 'brands')
            {
                echo $this->load->view('discount/tbl_brand_discount',$data, true);
                die;
            }
            else if($prod_type == 'products')
            {
                //print_r($data); die;
                echo $this->load->view('discount/tbl_product_discount',$data, true);
                die;
            }
            else if($prod_type == 'category')
            {
                echo $this->load->view('discount/tbl_category_discount',$data, true);
                die;
            }
            die;

        }

     function cart_discount_edit() // fn that populate edit page through url
     {
        $rowid =  $this->uri->segment(4);
        $param = array('act_mode' => 'view_cart_discount_id',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => 0, 
                            'coupon' => '', 
                            'is_reusable' => $rowid, 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id'   => ''
                            );

       $result['detail'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $param);
     
       $this->load->view('helper/header'); 
       $this->load->view('discount/cart_discount_edit', $result); 
         
     }

     function cart_discount_active()
    {
       $rowid =  $this->uri->segment(4);
       $loginid=$this->session->userdata('bizzadmin')->LoginID;
        $param = array('act_mode' => 'active_inactive',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => 1, 
                            'coupon' => '', 
                            'is_reusable' => $rowid, 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => $loginid,
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id'    => ''
                            );

       $result['data'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $param);
       redirect("admin/discount/cart_discount_list");   
    }

    function cart_discount_inactive()
    {
       $rowid =  $this->uri->segment(4);
       $loginid=$this->session->userdata('bizzadmin')->LoginID;
        $param = array('act_mode' => 'active_inactive',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => 0, 
                            'coupon' => '', 
                            'is_reusable' => $rowid, 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => $loginid,
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id'   => ''
                            );

       $result['data'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $param);
       redirect("admin/discount/cart_discount_list");   
    }

     public function editCartDisc() { // ajax fn to update record

        $loginid=$this->session->userdata('bizzadmin')->LoginID;
        $time=$_POST['valid_from_time'].','.$_POST['valid_to_time'].','.$loginid;
                
                $parameter = array('act_mode' => 'update_cart_disc',
                            'discount_name' => $_POST['discount_name'],
                            'discount_value' => $_POST['discount_value'], 
                            'discount_type' => $_POST['discount_type'], 
                            'purchase_amount' => $_POST['purchase_amount'], 
                            'is_coupon' => $_POST['is_coupon'], 
                            'coupon' => $_POST['coupon'], 
                            'is_reusable' => $_POST['is_reusable'], 
                            'reusable_limit' => $_POST['reusable_limit'], 
                            'valid_from' => $_POST['valid_from'], 
                            'valid_to' => $_POST['valid_to'], 
                            'coupon_user_type' => $_POST['coupon_user_type'],
                            'row_id' => $_POST['cart_discount_id'],
                            'brand_list' => $time,
                            'product_type' => $_POST['purchase_amount_max'],
                            'parent_id'  => $_POST['fixed_discount_price']
                        );
            
             $data['stored_flag'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $parameter);     

     }

     public function saveCartDisc(){ // cart discount add
      $loginid=$this->session->userdata('bizzadmin')->LoginID;
      $time=$_POST['valid_from_time'].','.$_POST['valid_to_time'];
        $parameter = array('act_mode' => 'insert_cart_discount',
                            'discount_name' => $_POST['discount_name'],
                            'discount_value' => $_POST['discount_value'], 
                            'discount_type' => $_POST['discount_type'], 
                            'purchase_amount' => $_POST['purchase_amount'], 
                            'is_coupon' => $_POST['is_coupon'], 
                            'coupon' => $_POST['coupon'], 
                            'is_reusable' => $_POST['is_reusable'], 
                            'reusable_limit' => $_POST['reusable_limit'], 
                            'valid_from' => $_POST['valid_from'], 
                            'valid_to' => $_POST['valid_to'], 
                            'coupon_user_type' => $_POST['coupon_user_type'],
                            'row_id' => $loginid,
                            'brand_list' => $time,
                            'product_type' => $_POST['purchase_amount_max'],
                            'parent_id'    => $_POST['fixed_discount_price']
                            ); 
        $data['stored_flag'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $parameter); 
     }

/*
   public function saveRegisterDisc() { 


            //if(is_array($_POST['userid'])){
           //$userId =implode(",",$_POST['userid']); 
    //}
   // else{
        
    //$userId = '0';
    //}


        if(is_array($_POST['userid'])){
            
        $p_offer='user';
        $userId =implode(",",$_POST['userid']);
        }
        else {

        $p_offer='register'; 
        $userId = '0';
        }

        if($_POST['reuse']){ 
            $_POST['reuse']; 
        }else{ 
            $_POST['reuse']=0; 
        } 
        if($_POST['reuse'] and $_POST['usableLimit']==''){ 
            $_POST['usableLimit']=100000; 
        } 
        $parameter = array('p_offer' => $p_offer, 'p_amt' => $_POST['disAmt'], 'p_type' => $_POST['disTyp'], 'p_pamt' => $_POST['purAmt'], 'p_start' => $_POST['startDate'], 'p_end' => $_POST['endDate'], 'p_coupon' => $_POST['coupon'], 'p_limit' => $_POST['limit'], 'p_reuse' => $_POST['reuse'], 'p_usableLimit' => $_POST['usableLimit'], 'p_disc_name' => $_POST['discname'], 'p_regFrom' => $_POST['regFrom'], 'regTo' => $_POST['regTo'], 'region' => $_POST['region'],'userids'=>$userId); 
        //p($parameter);    exit;
        $data['disc'] = $this->supper_admin->call_procedureRow('proc_insertRegDisc', $parameter); 

        
        //p($data['disc']); exit();




        //if(is_array($_POST['userid'])){
        /*foreach ($_POST['userid'] as $key  ) {

        $parameter1 = array('p_userid' =>$userId ,'p_reusibility'=>$_POST['reuse'],'p_discountid'=>$data['disc']->id );
            $data['disc2'] = $this->supper_admin->call_procedureRow('proc_user_coupan', $parameter1); 
     }*/

    //}

 
   
    public function getalluser(){
        $parameter = array();
        $data['userlist']=$this->supper_admin->call_procedure('proc_get_alluser',$parameter);
     //   $this->load->view('helper/header');
        $this->load->view('discount/userlist',$data);
      //  $this->load->view('helper/footer');

    }

public function addparovedisc(){

//p($_POST); exit;sds

if(is_array($_POST['productId'])){
foreach ($_POST['productId'] as $key ) { 
                $p_resetproductdiscid .=$key.','; 
             } 
}
else{
    $p_resetproductdiscid="0";

}
foreach ($_POST['data']['brandCatId'] as $key ) { 
                $cat_brandid .=$key.','; 
             } 

//in p_discnam varchar(50) ,in p_disctype varchar(20), p_fromdat varchar(20), p_todate varchar(20) ,p_reussable int(5), p_brand varchar(20) , p_category varchar(20) ,p_product varchar(20) , p_resetproductdiscid varchar(20)
foreach ($_POST['data']['products'] as $key ) { 
                $p_product .= $key.','; 
             } 



if(is_array($_POST['data']['userIds'])){
        foreach ($_POST['data']['userIds'] as $key ) { 
                $userId .=$key.','; 
        } 
}
else{
    $userId = '0';
}


$discpurchase = '0';
$parameter_new = array('p_discnam' =>$_POST['data']['discname'],
                     'coupon' =>$_POST['data']['coupon'],
                     'p_disctype'=>$_POST['data']['disType'],
                     'p_fromdat'=>$_POST['data']['startDate'],
                     'p_todate'=>$_POST['data']['endDate'],
                     'p_reussable'=>$_POST['data']['disVal'],
                     'p_brand'=>$_POST['data']['disOn'],
                     'p_category'=>$cat_brandid,
                     'p_product'=>$p_product,
                     'p_resetproductdiscid'=>$p_resetproductdiscid,
                     'p_prod_cart'=>'',
                     'p_prod_cart_amt'=>'',
                     'p_user_ids'=>$userId,
                     //'disPurchaseAmt'=>$_POST['data']['disPurchaseAmt']
                     'disPurchaseAmt'=>$discpurchase
                     );

    $this->supper_admin->call_procedureRow('proc_savedisc_approve',$parameter_new);

    //p($parameter_new);exit;

}

public function getdiscountproductname(){
       
        $data['proArr'] = array();         
        if (isset($_POST)) {
       
          foreach ($_POST['data']['products'] as $val) {                
              
              $parameter = array('proValue' => $val,'startdate'=>$_POST['data']['startDate'],'enddate'=>$_POST['data']['endDate'],'prodisOn' => $_POST['data']['disOn']);       
              
                  $savedData = $this->supper_admin->call_procedureRow('proc_checkdiscount', $parameter);
                   if (!empty($savedData)) { 
                    $parameter1 = array('proValue' => $val); 
                    $product = $this->supper_admin->call_procedureRow('proc_discGetProductDetails', $parameter1); //proc_GetProductDetails
                    
                if (!empty($product)) { 
                    $data['proArr'][] = array('ProId' => $product->prodtmappid, 'ProName' => $product->ProName, 'ProductDiscount' => $product->ProductDiscount, 'ProductDiscountType'=>$product->DiscountType); 
                } 
            } 
           } 
       
            if(count($data['proArr']) > 0){                
       
                $this->load->view('disprolisting', $data);
            }else{
       
                $data = count($data['proArr']);
                $this->load->view('disprolisting', $data);                
            }
        }  
}
public function getsubcategorylist()
{
    $subcateid = $_POST['maincatid'];

    $parameter = array('act_mode'=>"subcategory",'id' => $subcateid);
    $data['list'] =   $this->supper_admin->call_procedure('Proc_GetCatgories',$parameter);
    
    //p($data['list']); exit;
    $this->load->view('discount/subcategoryview',$data);   

}


public function getthirdlevelcategorylist()
{
   $childsubcateid  = $_POST['subcatid'];

    $parameter = array('act_mode'=>"childsubcategory",'id' => $childsubcateid);
    $data['list'] =   $this->supper_admin->call_procedure('Proc_GetCatgories',$parameter);
    //$parameter1 = array('act_mode'=>"childcategoryproduct",'id' => $childsubcateid);
     //$data['product'] =   $this->supper_admin->call_procedure('Proc_GetCatgories',$parameter1);
        //shradha.agrawal20@gmail.com
    $this->load->view('discount/childcategorysubview',$data);   
} 

public function first_discount_list() // listing all coupoun cart 
     {
         $parameter = array('act_mode' => 'view_first_discount',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => '', 
                            'coupon' => '', 
                            'is_reusable' => '', 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id'   => ''
                            ); 
         $data['cart_detail'] = $this->supper_admin->call_procedure('proc_discount_cart', $parameter);
         $this->load->view('helper/header'); 
        $this->load->view('discount/first_discount_list', $data); 
     }

public function first_discount() {
        $this->userfunction->loginAdminvalidation();
     
        $this->load->view('helper/header'); 
        $this->load->view('discount/first_discount'); 
       
}

public function savefirstDisc(){ // cart discount add
      $loginid=$this->session->userdata('bizzadmin')->LoginID;
      $time=$_POST['valid_from_time'].','.$_POST['valid_to_time'];
        $parameter = array('act_mode' => 'insert_first_discount',
                            'discount_name' => $_POST['discount_name'],
                            'discount_value' => $_POST['discount_value'], 
                            'discount_type' => $_POST['discount_type'], 
                            'purchase_amount' => $_POST['purchase_amount'], 
                            'is_coupon' => 0,#$_POST['is_coupon'], 
                            'coupon' => $_POST['coupon'], 
                            'is_reusable' => 0,#$_POST['is_reusable'], 
                            'reusable_limit' => 0,#$_POST['reusable_limit'], 
                            'valid_from' => $_POST['valid_from'], 
                            'valid_to' => $_POST['valid_to'], 
                            'coupon_user_type' => 'retail',#$_POST['coupon_user_type'],
                            'row_id' => $loginid,
                            'brand_list' => $time,
                            'product_type' => $_POST['purchase_amount_max'],
                            'parent_id'    => $_POST['fixed_discount_price']
                            ); 
        //p($parameter);
        $data['stored_flag'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $parameter); 
}

    public function first_active_inactive()
    {
        $loginid=$this->session->userdata('bizzadmin')->LoginID;
        $rowid =  $this->uri->segment(4);
        //$prod_type = $this->uri->segment(5);
        $status = $this->uri->segment(5);
        $param = array('act_mode' => 'all_active_inactive',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => $status, 
                            'coupon' => '', 
                            'is_reusable' => $rowid, 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => $loginid,
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id' =>'',
                            );


        $result['data'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $param);
        redirect("admin/discount/first_discount_list");  
    }

    function first_discount_edit() // fn that populate edit page through url
     {
        $rowid =  $this->uri->segment(4);
        $param = array('act_mode' => 'view_first_discount_id',
                            'discount_name' => '',
                            'discount_value' => '', 
                            'discount_type' => '', 
                            'purchase_amount' => '', 
                            'is_coupon' => 0, 
                            'coupon' => '', 
                            'is_reusable' => $rowid, 
                            'reusable_limit' => '', 
                            'valid_from' => '', 
                            'valid_to' => '', 
                            'coupon_user_type' => '',
                            'row_id' => '',
                            'brand_list' => '',
                            'product_type' => '',
                            'parent_id'   => ''
                            );

       $result['detail'] = $this->supper_admin->call_procedureRow('proc_discount_cart', $param);
     
       $this->load->view('helper/header'); 
       $this->load->view('discount/first_discount_edit', $result); 
         
     }

} 
?>