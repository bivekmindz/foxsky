<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Recipe extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('upload');
    $this->userfunction->loginAdminvalidation();
    
  }
  
  public function addrecipe(){

     if($this->input->post('submit')){
        $userid = $this->session->userdata('bizzadmin')->LoginID;
        $imgslide          = str_replace(' ', '', $_FILES['rep_banner']['name']);
          
       $field_name_img    = 'rep_banner';
       $img_file          = $this->image_upload($field_name_img);

       $allowedExts       = array("gif","jpeg","jpg","png");
       $temp              = explode(".",$imgslide);
       $extension         = end($temp);
       //echo $extension;exit();
       if (in_array($extension, $allowedExts)){

        $parameter=array('act_mode' => 'addrecipe',
                         'param2' => $this->input->post('rep_title'),
                         'param3' => time().$imgslide,
                         'param4' => '',
                         'param5' => '',
                         'param6' => '',
                         'param7' => $this->input->post('rep_ingredients'),
                         'param8' => '',
                         'param9' => '',
                         'param10' => $userid);
        $recipedata = $this->supper_admin->call_procedureRow('proc_recipe',$parameter);
        
        foreach ($this->input->post('rep_products') as $key => $value) {
          $parameter_pro=array('act_mode' => 'addrecipe_products',
                         'param2' => $recipedata->lastid,
                         'param3' => $value,
                         'param4' => '',
                         'param5' => '',
                         'param6' => '',
                         'param7' => '',
                         'param8' => '',
                         'param9' => '',
                         'param10' => $userid);
        $recipe_pro = $this->supper_admin->call_procedureRow('proc_recipe',$parameter_pro);
        }

        $this->session->set_flashdata('message', 'Recipe Added Successfully.');
        redirect('admin/recipe/addrecipesteps/'.$recipedata->lastid);

        } else {

        $this->session->set_flashdata('message', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
        redirect('admin/recipe/addrecipe');    
            
        }


     }


    $parameterc=array('act_mode' => 'get_last_level_category',
                     'param2' => '',
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '');
    $record['all_category'] = $this->supper_admin->call_procedure('proc_recipe',$parameterc);

    $this->load->view('helper/header');
    $this->load->view('recipe/addrecipe',$record);
  }


  public function getproducts(){
    $categoryid = implode(",",$this->input->post('categoryid'));
    $parameter=array('act_mode' => 'get_all_products',
                     'param2' => $categoryid,
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '');
    $all_products = $this->supper_admin->call_procedure('proc_recipe',$parameter);

    foreach ($all_products as $key => $value) {
        echo "<option value='".$value->prodtmappid."'>".$value->ProductName."</option>";
    }
  }

  public function getproductsupdate(){
    $categoryid = implode(",",$this->input->post('categoryid'));
    $rowid=$this->uri->segment(4);

    $parampro=array('act_mode' => 'getrecipeprodetail',
                    'param2' => $rowid,
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '');
    $recipe_pro_detail = $this->supper_admin->call_procedure('proc_recipe',$parampro);
    $recipepro=array();
    foreach ($recipe_pro_detail as $key => $value) {
        array_push($recipepro, $value->rp_productmapid);
    }
    $parameter=array('act_mode' => 'get_all_products',
                     'param2' => $categoryid,
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '');
    $all_products = $this->supper_admin->call_procedure('proc_recipe',$parameter);

    foreach ($all_products as $key => $value) {
        if(in_array($value->prodtmappid, $recipepro)){
            echo "<option selected value='".$value->prodtmappid."'>".$value->ProductName."</option>";
        } else{
            echo "<option value='".$value->prodtmappid."'>".$value->ProductName."</option>";
        }
    }
  }

  public function addrecipesteps($id){

    if($this->input->post('submit')){
        $userid = $this->session->userdata('bizzadmin')->LoginID;
        $imgslide          = str_replace(' ', '', $_FILES['rep_image']['name']);
          
       $field_name_img    = 'rep_image';
       $img_file          = $this->image_upload($field_name_img);

       $allowedExts       = array("gif","jpeg","jpg","png");
       $temp              = explode(".",$imgslide);
       $extension         = end($temp);
       //echo $extension;exit();
       if (in_array($extension, $allowedExts)){

        $parameter=array('act_mode' => 'addrecipe_steps',
                         'param2' => $id,
                         'param3' => $this->input->post('rep_step_title'),
                         'param4' => time().$imgslide,
                         'param5' => '',
                         'param6' => '',
                         'param7' => $this->input->post('rep_desc'),
                         'param8' => '',
                         'param9' => '',
                         'param10' => $userid);
        $recipedata = $this->supper_admin->call_procedureRow('proc_recipe',$parameter);

        $this->session->set_flashdata('message', 'Recipe steps Added Successfully.');
        redirect('admin/recipe/addrecipesteps/'.$id);
        }
         else{        
            $this->session->set_flashdata('message', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            redirect('admin/recipe/addrecipesteps/'.$id);
         }
    }    

    $this->load->view('helper/header');
    $this->load->view('recipe/addrecipestep',$record);
  }

  public function recipe_list(){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    if($this->input->post('submit')){
     foreach ($this->input->post( 'recipedelete') as $key => $value) {
        $paramdel=array('act_mode' => 'delete_recipe',
                     'param2' => $value,
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => $userid);
        $delrecipe = $this->supper_admin->call_procedureRow('proc_recipe',$paramdel);
     }
     $this->session->set_flashdata("message", "Information was successfully deleted.");
     redirect("admin/recipe/recipe_list");
    }

    $param=array('act_mode' => 'get_recipe_list',
                 'param2' => '',
                 'param3' => '',
                 'param4' => '',
                 'param5' => '',
                 'param6' => '',
                 'param7' => '',
                 'param8' => '',
                 'param9' => '',
                 'param10' => '');
    $recipedata = $this->supper_admin->call_procedure('proc_recipe',$param);
   
    //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/recipe/recipe_list?";
     $config['total_rows']       = count($recipedata);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $record["links"]  = explode('&nbsp;',$str_links );
     $parameter=array('act_mode' => 'get_recipe_list',
                     'param2' => '',
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => $page,
                     'param10' => $second);
    $record['recipelist'] = $this->supper_admin->call_procedure('proc_recipe',$parameter);

    $this->load->view('helper/header');
    $this->load->view('recipe/recipe_list',$record);
  }

  public function deleterecipe($id){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $paramdel=array('act_mode' => 'delete_recipe',
                    'param2' => $id,
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => $userid);
    $delrecipe = $this->supper_admin->call_procedureRow('proc_recipe',$paramdel);
    $this->session->set_flashdata("message", "Information was successfully deleted.");
    redirect("admin/recipe/recipe_list");
  }

  public function statusrecipe(){

    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $ch_status      = $status==0?1:0;
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $paramstatus=array('act_mode' => 'status_recipe',
                    'param2' => $rowid,
                    'param3' => $ch_status,
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => $userid);
    $st_recipe = $this->supper_admin->call_procedureRow('proc_recipe',$paramstatus);
    $this->session->set_flashdata("message", "Status was successfully updated.");
    redirect("admin/recipe/recipe_list");
  }

  public function updaterecipe($id){
    $param=array('act_mode' => 'getrecipedetail',
                    'param2' => $id,
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '');
    $record['recipe_detail'] = $this->supper_admin->call_procedureRow('proc_recipe',$param);

    $parampro=array('act_mode' => 'getrecipeprodetail',
                    'param2' => $id,
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '');
    $record['recipe_pro_detail'] = $this->supper_admin->call_procedure('proc_recipe',$parampro);

    $parameterc=array('act_mode' => 'get_last_level_category',
                     'param2' => '',
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '');
    $record['all_category'] = $this->supper_admin->call_procedure('proc_recipe',$parameterc);

    $parameter=array('act_mode' => 'get_recipe_category',
                     'param2' => $id,
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '');
    $record['get_category'] = $this->supper_admin->call_procedureRow('proc_recipe',$parameter);

    if($this->input->post('submit')){
        $userid = $this->session->userdata('bizzadmin')->LoginID;
        $imgslide          = str_replace(' ', '', $_FILES['rep_banner']['name']);
        if(empty($imgslide)){
            $recipeimg=$record['recipe_detail']->r_banner;
        } else {
           $field_name_img    = 'rep_banner';
           $img_file          = $this->image_upload($field_name_img);
           $recipeimg=time().$imgslide;

        }
       
        $allowedExts       = array("gif","jpeg","jpg","png");
        $temp              = explode(".",$recipeimg);
        $extension         = end($temp);
          
        if (in_array($extension, $allowedExts)){

        $paramupr=array('act_mode' => 'update_recipe',
                         'param2' => $id,
                         'param3' => $this->input->post('rep_title'),
                         'param4' => $recipeimg,
                         'param5' => '',
                         'param6' => '',
                         'param7' => $this->input->post('rep_ingredients'),
                         'param8' => '',
                         'param9' => '',
                         'param10' => $userid);
        $update_recipe_detail = $this->supper_admin->call_procedure('proc_recipe',$paramupr);

        $paramproup=array('act_mode' => 'update_recipe_products',
                         'param2' => $id,
                         'param3' => '',
                         'param4' => '',
                         'param5' => '',
                         'param6' => '',
                         'param7' => '',
                         'param8' => '',
                         'param9' => '',
                         'param10' => $userid);
        $update_recipe_pro = $this->supper_admin->call_procedure('proc_recipe',$paramproup);

        foreach ($this->input->post('rep_products') as $key => $value) {
            $parameter_pro=array('act_mode' => 'addrecipe_products',
                                 'param2' => $id,
                                 'param3' => $value,
                                 'param4' => '',
                                 'param5' => '',
                                 'param6' => '',
                                 'param7' => '',
                                 'param8' => '',
                                 'param9' => '',
                                 'param10' => $userid);
            $recipe_pro = $this->supper_admin->call_procedureRow('proc_recipe',$parameter_pro);
        }
        $this->session->set_flashdata("message", "Information was successfully updated.");
        redirect("admin/recipe/recipe_list");

        } else {

        $this->session->set_flashdata('message', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
        redirect('admin/recipe/updaterecipe');    

        }


    }

    $this->load->view('helper/header');
    $this->load->view('recipe/updaterecipe',$record);
  }

  public function recipe_steps_list($id){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    if($this->input->post('submit')){
     foreach ($this->input->post( 'recipestepdelete') as $key => $value) {
        $paramdel=array('act_mode' => 'delete_recipe_step',
                     'param2' => $value,
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => $userid);
        $delrecipe = $this->supper_admin->call_procedureRow('proc_recipe',$paramdel);
     }
     $this->session->set_flashdata("message", "Information was successfully deleted.");
     redirect("admin/recipe/recipe_steps_list/".$id);
    }

    $param=array('act_mode' => 'getrecipe_step_list',
                 'param2' => $id,
                 'param3' => '',
                 'param4' => '',
                 'param5' => '',
                 'param6' => '',
                 'param7' => '',
                 'param8' => '',
                 'param9' => '',
                 'param10' => '');
    $record['recipestepdata'] = $this->supper_admin->call_procedure('proc_recipe',$param);
   
    $this->load->view('helper/header');
    $this->load->view('recipe/recipe_steps_list',$record);
  }

  public function deleterecipestep($id){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $paramdel=array('act_mode' => 'delete_recipe_step',
                    'param2' => $id,
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => $userid);
    $delrecipe = $this->supper_admin->call_procedureRow('proc_recipe',$paramdel);
    $this->session->set_flashdata("message", "Information was successfully deleted.");
    redirect("admin/recipe/recipe_steps_list/".$this->uri->segment(5));
  }

  public function statusrecipestep(){

    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $ch_status      = $status==0?1:0;
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $paramstatus=array('act_mode' => 'status_recipe_steps',
                    'param2' => $rowid,
                    'param3' => $ch_status,
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => $userid);
    $st_recipe = $this->supper_admin->call_procedureRow('proc_recipe',$paramstatus);
    $this->session->set_flashdata("message", "Status was successfully updated.");
    redirect("admin/recipe/recipe_steps_list/".$this->uri->segment(6));
  }

  public function updaterecipestep($id){
    $paramstatus=array('act_mode' => 'recipe_steps_detail',
                    'param2' => $id,
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '');
    $record['step_recipe'] = $this->supper_admin->call_procedureRow('proc_recipe',$paramstatus);

    if($this->input->post('submit')){
        $userid = $this->session->userdata('bizzadmin')->LoginID;
        $imgslide          = str_replace(' ', '', $_FILES['rep_image']['name']);
        if(empty($imgslide)){
            $recipeimg=$record['step_recipe']->rd_image;
        } else {
           $field_name_img    = 'rep_image';
           $img_file          = $this->image_upload($field_name_img);
           $recipeimg=time().$imgslide;

        }
       
        $allowedExts       = array("gif","jpeg","jpg","png");
        $temp              = explode(".",$recipeimg);
        $extension         = end($temp);
          
        if (in_array($extension, $allowedExts)){

        $parameter=array('act_mode' => 'update_recipe_steps',
                         'param2' => $id,
                         'param3' => $this->input->post('rep_step_title'),
                         'param4' => $recipeimg,
                         'param5' => '',
                         'param6' => '',
                         'param7' => $this->input->post('rep_desc'),
                         'param8' => '',
                         'param9' => '',
                         'param10' => $userid);
        //p($parameter);exit();
        $recipe_step_update = $this->supper_admin->call_procedureRow('proc_recipe',$parameter);

        $this->session->set_flashdata('message', 'Information was successfully updated.');
        redirect("admin/recipe/recipe_steps_list/".$record['step_recipe']->rd_rid);
        }
         else{        
            $this->session->set_flashdata('message', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            redirect('admin/recipe/updaterecipestep/'.$id);
         }
    }

    $this->load->view('helper/header');
    $this->load->view('recipe/updaterecipestep',$record);
  }

  public function image_upload($field_name){

    $config['upload_path']   = './assets/webapp/recipe_img/';
    $config['allowed_types'] = 'jpg|jpeg|gif|png';
    $config['max_size']      = '10000000';
    $config['file_name']     = time().str_replace(' ', '', $_FILES[$field_name]['name']);
    
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload($field_name)){
        $data['error']  = array('error' => $this->upload->display_errors());
    } else {
        $data['name']   = array('upload_data' => $this->upload->data());
    }
    return $data;
  }
  
}//end class
?>