<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Sectionmanager extends MX_Controller{

//............. Default Construct function ............... //
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->userfunction->loginAdminvalidation();
    
  }// end function.

  
  public function section(){
    
     $param = array('act_mode' => 'getspecification',
                    'row_id' => '',
                    'sec_cat' => '',
                    'pro_cat' => '',
                    'sectionhead' => '',
                    'sectionsubhead' => '',
                    'bannerimg' => '',
                    'userid' => '');

    $record['recs']  = $this->supper_admin->call_procedure('proc_specifications',$param);


    $this->load->view('helper/header');
    $this->load->view('sectionmanager/managesection',$record);
  
  }
  
  public function addsection(){

    $this->userfunction->loginAdminvalidation();

    $parameter = array('act_mode'=>'viewparentcat','row_id'=>'0','catparentid'=>'0','cname'=>'0','cdesc'=>'0','caturl'=>'0','catmeta'=>'0',
      'metadesc'=>'0','metakeyword'=>'0','catsort' =>'0','catdisplay'=>'0','catlevel'=>'0');
    $record['parentcat']  = $this->supper_admin->call_procedure('proc_category',$parameter);

    if($this->input->post('submit')){
    
      $sec_cat = $this->input->post('sec_cat');
      $pro_cat = $this->input->post('pro_cat');

      $sectionhead = $this->input->post('sectionhead');
      $sectionsubhead = $this->input->post('sectionsubhead');
if($_FILES['slideimg']['name']!='') {
      $slideimg = time().str_replace(' ','',$_FILES['slideimg']['name']);
      move_uploaded_file($_FILES["slideimg"]["tmp_name"],"assets/sectionimages/".$slideimg);
 }
 else
 {
  $slideimg ='';
 }
      $userid = $this->session->userdata('bizzadmin')->LoginID;

      $param = array('act_mode' => 'insertspecification',
                    'row_id' => '',
                    'sec_cat' => $sec_cat,
                    'pro_cat' => $pro_cat,
                    'sectionhead' => $sectionhead,
                    'sectionsubhead' => $sectionsubhead,
                    'bannerimg' => $slideimg,
                    'userid' => $userid);

      $lastsecid  = $this->supper_admin->call_procedure('proc_specifications',$param);
             //  echo count($_POST['left_val']);
      for($i=0; $i< count($_POST['specificationsubhead']);$i++){
         
        
         $left_val = $_POST['left_val'][$i];
         $right_val = $_POST['right_val'][$i];
         $value  = $_POST['value'][$i];
         $feahead = $_POST['specificationhead'][$i];
         $feasubhead = $_POST['specificationsubhead'][$i];

         if($_FILES['banimg']['name'][$i]!='') {
 $banimg = time().$i.str_replace(' ', '', $_FILES['banimg']['name'][$i]);
        move_uploaded_file(str_replace(' ', '', $_FILES["banimg"]["tmp_name"][$i]),"assets/sectionimages/".$banimg);
}
else
{
  $banimg = "";
}
        $param = array('act_mode' => 'insertfeature',
                    'row_id' => '',
                    'specification_id' => $lastsecid[0]->id,
                    'left_value_id' => $left_val,
                    'right_value_id' => $right_val,
                    'feature_value' => $value,
                    'feature_banner_image' => $banimg,
                    'feature_heading' => $feahead,
                    'feature_sub_heading' => $feasubhead);
        $record['slidedetail']  = $this->supper_admin->call_procedure('proc_spec_features',$param);

     }

     $this->session->set_flashdata('message', 'Your information was successfully Saved.');
   //  redirect('admin/sectionmanager/addsection');
    }

    $this->load->view('helper/header');
    $this->load->view('sectionmanager/addsectionspec',$record);
  }

  public function productName()
  {
    
  $param = array(
  // 'act_mode'=>'getProductName',
  // 'pro_id'   => $this->input->post('ids'),
  // 'Param2'   => '',
  // 'Param3'   => '',
  // 'Param4'   => '',
  // 'Param5'   => ''
     'act_mode' => 'getProductName',
    'row_id' => '0',
    'cat_id' => $this->input->post('ids'),
    'name' => '',
    'sec_order' => '0',
    'media_type' => '',
    'img_name' => '',
    'img_url' => '',
    'img_head' => '',
    'imgshead' => ''
  );
  // p($param);exit();
  $data['getProductName'] = $this->supper_admin->call_procedure('proc_overview', $param);
  // p($data['getProductName']); exit;
  $proData = '<option value="">Select Type</option>';

  foreach ($data['getProductName'] as $key => $value) {
  $proData .= '<option value="'.$value->proid.'">'.$value->proname.'</option>';
  // p($proData);
  // exit();
  }
  echo $proData;

  } 




  public function featureLeft()
  {
    
  $param = array(
  // 'act_mode'=>'getProductName',
  // 'pro_id'   => $this->input->post('ids'),
  // 'Param2'   => '',
  // 'Param3'   => '',
  // 'Param4'   => '',
  // 'Param5'   => ''
     'act_mode' => 'getLeft',
    'row_id' => '',
    'cat_id' => $this->input->post('ids'),
    'name' => '',
    'sec_order' => '',
    'media_type' => '',
    'img_name' => '',
    'img_url' => '',
    'img_head' => '',
    'imgshead' => ''
  );
  // p($param);exit();
  $data['getProductName'] = $this->supper_admin->call_procedure('proc_overview', $param);
 // p($data['getProductName']); exit;
  $proData = '<option value="">Select Type</option>';

  foreach ($data['getProductName'] as $key => $value) {
  $proData .= '<option value="'.$value->FeatureLeftID.'">'.$value->LeftValue.'</option>';
  // p($proData);
  // exit();
  }
  echo $proData;

  } 


   public function featureRight()
  {
    
  $param = array(
  // 'act_mode'=>'getProductName',
  // 'pro_id'   => $this->input->post('ids'),
  // 'Param2'   => '',
  // 'Param3'   => '',
  // 'Param4'   => '',
  // 'Param5'   => ''
     'act_mode' => 'getRight',
    'row_id' => '',
    'cat_id' => $this->input->post('ids'),
    'name' => '',
    'sec_order' => '',
    'media_type' => '',
    'img_name' => '',
    'img_url' => '',
    'img_head' => '',
    'imgshead' => ''
  );
  
  $data['getProductName'] = $this->supper_admin->call_procedure('proc_overview', $param);
 // p($data['getProductName']); exit;
  $proData = '<option value="">Select Type</option>';

  foreach ($data['getProductName'] as $key => $value) {
  $proData .= '<option value="'.$value->RightFeatureID.'">'.$value->RightValue.'</option>';
  // p($proData);
  // exit();
  }
  echo $proData;

  } 



//............. Manage Section Master ............... //
  public function managesection(){

    $this->userfunction->loginAdminvalidation();
    $this->load->view('helper/header');

    $parameter     = array('row_id'=>'','catparentid'=>'','actmode'=>'listselection');
    $data['recs']  = $this->supper_admin->call_procedure('proc_sectionmanager',$parameter);
    
    //p($data['recs']); exit;
    $this->load->view('sectionmanager/managesection',$data);
  
  }//end function.


  public function deletesection($id){
    $this->userfunction->loginAdminvalidation();
    $parameterimg     = array('row_id'=>$id,'catparentid'=>'','actmode'=>'viewsectionimg');
    $dataimg  = $this->supper_admin->call_procedure('proc_sectionmanager',$parameterimg);
    foreach ($dataimg as $key => $value) {
        unlink($_SERVER["DOCUMENT_ROOT"]."/bizzgain.com/assets/sectionimages/".$value->imgname);
    }

    $parameter     = array('row_id'=>$id,'catparentid'=>'','actmode'=>'deletespecification');
    $data  = $this->supper_admin->call_procedure('proc_sectionmanager',$parameter);
    redirect('admin/sectionmanager/section');

  }


  public function deletesectionbanner($id){
    $this->userfunction->loginAdminvalidation();
   
    $parameter     = array('row_id'=>$id,'catparentid'=>'','actmode'=>'deletesctionbann');
    $data  = $this->supper_admin->call_procedure('proc_sectionmanager',$parameter);
    redirect('admin/sectionmanager/section');

  }


  public function subcategory(){
    $parentcatid=$this->input->post('parentcat');
    //echo $parentcatid;exit();
    $parameter= array('act_mode'=>'viewsubcatdata','row_id'=>$parentcatid,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort' =>'','catdisplay'=>'','catlevel'=>'');
    $record['subcat']  = $this->supper_admin->call_procedure('proc_category',$parameter);
    $str = '';
    foreach($record['subcat'] as $k=>$v){
      
        $str .= "<option value=".$v->catid.">".$v->catname."</option>";
   
    }
    echo $str;
  
  }

  public function hmsubcategory(){
    $parentcatid=$this->input->post('parentcat');
    $sec_catid=$this->input->post('sec_catid');
    //echo $parentcatid;exit();
    $parameter= array('act_mode'=>'hmviewsubcatdata','row_id'=>$parentcatid,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort' =>'','catdisplay'=>'','catlevel'=>'');
    $record['subcat']  = $this->supper_admin->call_procedure('proc_category',$parameter);
    $str = '';
    foreach($record['subcat'] as $k=>$v){
        if(!empty($sec_catid)){
            if($v->catid==$sec_catid){
                $str .= "<option selected value=".$v->catid.">".$v->catname."</option>";
            } else {
                $str .= "<option value=".$v->catid.">".$v->catname."</option>";
            }
        } else {
            $str .= "<option value=".$v->catid.">".$v->catname."</option>";
        }
      
   
    }
    echo $str;
  
  }


public function changecategory(){

    //$chsku=array('CHICCO-159','LO-150','LO-151','LO-152','LO-153','LO-154','MAT-DFR37','MAT-P6873','MS1021B','MS1021C','MS1021E','MS1021F','MS1021G','MS1021H','MS1021J','MS1100A','MS1100B','MS1200A','MS1700L','MS1700M','MS2300L','MS2300M','MS2521A','VTA022');

$chsku=array('CT-109A');
    
    foreach ($chsku as $key => $value) {
        $parameter=array('act_mode'=>'chcatapppro','Ordid'=>'','coments'=>$value,'empid'=>'');
        $record['subcat']  = $this->supper_admin->call_procedure('proc_Commnents',$parameter);
    }
        echo 'success';
  }  

  public function updatesection($id){

    $parameter = array('act_mode'=>'viewparentcat','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort' =>'','catdisplay'=>'','catlevel'=>'');
    $record['parentcat']  = $this->supper_admin->call_procedure('proc_category',$parameter);

    $param = array('act_mode'   => 'get_specificationmaster',
                   'row_id'     => $id,
                   'cat_id'     => '',
                   'name'       => '',
                   'sec_order'  => '',
                   'media_type' => '',
                   'img_name'   => '',
                   'img_url'    => '',
                   'img_head'   => '',
                   'imgshead'   => '');
     $record['section_detail']  = $this->supper_admin->call_procedureRow('proc_sectiondetails',$param);
   

     $paramc = array('act_mode'   => 'get_section_cat',
                   'row_id'     => $id,
                   'cat_id'     => '',
                   'name'       => '',
                   'sec_order'  => '',
                   'media_type' => '',
                   'img_name'   => '',
                   'img_url'    => '',
                   'img_head'   => '',
                   'imgshead'   => '');
     $record['section_cat']  = $this->supper_admin->call_procedureRow('proc_sectiondetails',$paramc);



     $paramcp = array('act_mode'   => 'get_section_prod',
                   'row_id'     => $id,
                   'cat_id'     => $record['section_detail']->product_id,
                   'name'       => '',
                   'sec_order'  => '',
                   'media_type' => '',
                   'img_name'   => '',
                   'img_url'    => '',
                   'img_head'   => '',
                   'imgshead'   => '');
     $record['prod_cat']  = $this->supper_admin->call_procedureRow('proc_sectiondetails',$paramcp);

 $paramcpb = array('act_mode'   => 'get_section_bottomsection',
                   'row_id'     => $id,
                   'cat_id'     => '',
                   'name'       => '',
                   'sec_order'  => '',
                   'media_type' => '',
                   'img_name'   => '',
                   'img_url'    => '',
                   'img_head'   => '',
                   'imgshead'   => '');
     $record['prod_bottomsection']  = $this->supper_admin->call_procedure('proc_sectiondetails',$paramcpb);



     $paramcs = array('act_mode'   => 'get_section_banner',
                   'row_id'     => $id,
                   'cat_id'     => '',
                   'name'       => '',
                   'sec_order'  => '',
                   'media_type' => '',
                   'img_name'   => '',
                   'img_url'    => '',
                   'img_head'   => '',
                   'imgshead'   => '');
     $record['section_banners']  = $this->supper_admin->call_procedure('proc_sectiondetails',$paramcs);
     
     if($this->input->post('submit')){

      


       $sec_cat = $this->input->post('sec_cat');
      $pro_cat = $this->input->post('pro_cat');

      $sectionhead = $this->input->post('sectionhead');
      $sectionsubhead = $this->input->post('sectionsubhead');
if($_FILES['slideimg']['name']!='')
{      $slideimg = time().str_replace(' ','',$_FILES['slideimg']['name']);
      move_uploaded_file($_FILES["slideimg"]["tmp_name"],"assets/sectionimages/".$slideimg);
} else {

  $slideimg = $this->input->post('slideimgtext');
}

 $param = array('act_mode' => 'updatespecification',
                    'row_id' => $id,
                    'sec_cat' => $sec_cat,
                    'pro_cat' => $pro_cat,
                    'sectionhead' => $sectionhead,
                    'sectionsubhead' => $sectionsubhead,
                    'bannerimg' => $slideimg,
                    'userid' => $userid);

      $lastsecid  = $this->supper_admin->call_procedure('proc_specifications',$param);

               
      for($i=0; $i< count($_POST['specificationsubheadn']);$i++){
         if($_FILES['banimgn']['name'][$i]!='') {
         $banimg = time().$i.str_replace(' ', '', $_FILES['banimgn']['name'][$i]);
             move_uploaded_file(str_replace(' ', '', $_FILES["banimgn"]["tmp_name"][$i]),"assets/sectionimages/".$banimg);

} else {  $banimg =$_POST['banimgnval'][$i] ; }
         //echo $left_val."--".$value 
         $left_val = $_POST['left_valn'][$i];
         $right_val = $_POST['right_valn'][$i];
         $value  = $_POST['valuen'][$i];
         $feahead = $_POST['specificationheadn'][$i];
         $feasubhead = $_POST['specificationsubheadn'][$i];
   $featid = $_POST['featid'][$i];
    
        $param = array('act_mode' => 'updatefeature',
                    'row_id' => $featid,
                    'specification_id' => $lastsecid[0]->id,
                    'left_value_id' => $left_val,
                    'right_value_id' => $right_val,
                    'feature_value' => $value,
                    'feature_banner_image' => $banimg,
                    'feature_heading' => $feahead,
                    'feature_sub_heading' => $feasubhead);
   //     p( $param );

        $record['slidedetail']  = $this->supper_admin->call_procedure('proc_spec_features',$param);

     }


      for($i=0; $i< count($_POST['specificationhead']);$i++){
         
         $banimg = time().$i.str_replace(' ', '', $_FILES['banimg']['name'][$i]);
         $left_val = $_POST['left_val'][$i];
         $right_val = $_POST['right_val'][$i];
         $value  = $_POST['value'][$i];
         $feahead = $_POST['specificationhead'][$i];
         $feasubhead = $_POST['specificationsubhead'][$i];

        move_uploaded_file(str_replace(' ', '', $_FILES["banimg"]["tmp_name"][$i]),"assets/sectionimages/".$banimg);

        $param = array('act_mode' => 'insertfeature',
                    'row_id' => '',
                    'specification_id' => $lastsecid[0]->id,
                    'left_value_id' => $left_val,
                    'right_value_id' => $right_val,
                    'feature_value' => $value,
                    'feature_banner_image' => $banimg,
                    'feature_heading' => $feahead,
                    'feature_sub_heading' => $feasubhead);
        $record['slidedetail']  = $this->supper_admin->call_procedure('proc_spec_features',$param);

     }

     $this->session->set_flashdata('message', 'Your information was successfully Saved.');
     redirect('admin/sectionmanager/section');


     }
      

  $param = array('act_mode' => 'getLeft',
                'row_id' => '0',
                'cat_id' => $record['section_detail']->category_id,
                'name' => '',
                'sec_order' => '0',
                'media_type' => '',
                'img_name' => '',
                'img_url' => '',
                'img_head' => '',
                'imgshead' => '');
  
      $record['getProductName'] = $this->supper_admin->call_procedure('proc_overview', $param);

 $param = array('act_mode' => 'getRight',
                    'row_id' => '0',
                    'cat_id' =>  $record['section_detail']->category_id,
                    'name' => '',
                    'sec_order' => '0',
                    'media_type' => '',
                    'img_name' => '',
                    'img_url' => '',
                    'img_head' => '',
                    'imgshead' => '');
                  
      $record['getProductrightName'] = $this->supper_admin->call_procedure('proc_overview', $param);


    $this->load->view('helper/header');
    $this->load->view('sectionmanager/updatesection',$record);
  }

  public function statussection(){

    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $ch_status      = $status=='A'?'D':'A';
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $param = array('act_mode' => 'home_specification_active_inactive',
                   'row_id' => $rowid,
                   'cat_id' => '',
                   'name' => $ch_status,
                   'sec_order' => '',
                   'media_type' => '',
                   'img_name' => '',
                   'img_url' => '',
                   'img_head' => '',
                   'imgshead' => $userid);

    $lastsecid = $this->supper_admin->call_procedure('proc_sectiondetails',$param);
    $this->session->set_flashdata('message', 'Status was successfully updated.');
    redirect('admin/sectionmanager/section');
  }


} 
?>