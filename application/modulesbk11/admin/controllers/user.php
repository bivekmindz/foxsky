<?php
class User extends MX_Controller
{
	public function __construct()
	{
		$this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
	}
	function userlisting(){
    $this->userfunction->loginAdminvalidation();
    
          	$user_listing=api_url().'userapi/listing/id/0/format/json/';
            #$response['userlisting'] = json_decode(file_get_contents($user_listing));
            $response['userlisting'] = curlget($user_listing); 
            if(!empty($response['userlisting'])){
               $data['success'] = "success" ;
               json_encode($data);
            }else{
              $data['fail'] ="something went wrong";
              echo json_encode($data);
            }
           $this->load->view('helper/header');
           $this->load->view("user/userlisting",$response); 
           $this->load->view('helper/footer');
  }

public function excel_index(){
  $this->userfunction->loginAdminvalidation();
  if($this->input->post('download')){

   $brandid                  = $this->input->post('brandid');
   $parameter                = array(
    'act_mode'=>'bcategory',
    'row_id'=>$brandid,
    ''
    );
   $responce['category']     = $this->supper_admin->call_procedure('proc_product',$parameter); 
   //p($responce['category']);exit;
   $parameter                = array(
    'act_mode'=>'bmcategory',
    'row_id'=>$brandid,
    ''
    );
   $responce['Maincategory'] = $this->supper_admin->call_procedure('proc_product',$parameter);
   //p($responce['Maincategory']);exit;
   $allArr               = array();
   $templevel            = 0;  
   $newkey               = 0;
   $grouparr[$templevel] = "";
   foreach ($responce['category'] as $key => $value) {
      $catid[]    = $value->catid;
      $catname[]  = $value->catname;
   }//foreach
   $finalcatid   = implode($catid, ',');
   $finalcatname = implode($catname, ',');
  //-----------------------------------end category --------------------------------------//

  //---------------------------------main category ---------------------------------------//
    foreach ($responce['Maincategory'] as $key => $val) {
      $mcatid[]     = $val->catid;
      $mcatname1[]  = $val->catname;
    }//foreach

    $mcatname       = array_unique($mcatname1);
    $finalmcatid    = implode(array_unique($mcatid), ',');
    $finalmcatname  = implode(array_unique($mcatname1), ',');
  //--------------------------------end category ------------------------------------------//

   // p($finalcatname);

   //-----------------------------MANUFACTURECODE----------------------------------//
  $parameter            =array('act_mode'=>'vendcode',
    'row_id'=>'',
    'catid'=>''
    );
  $responce['vendcodee']= $this->supper_admin->call_procedure('proc_product',$parameter); 
  foreach ($responce['vendcodee'] as $key => $value) {
   $manufvalue[]=$value->vendorcode;
   }
   $vendorname=implode($manufvalue, ',');
   //------------------------------END CODE-----------------------------------------//
  
  //------------------------------FEATURE-------------------------------------------//
 $featurevaluee1[]    = 'ManufactureCode';
 $featurevaluee1[]    = 'categories'; 
 $featurevaluee1[]    = 'maincategories'; 
 //p($finalcatid);
 //$parameter           =array('act_mode'=>'feature','row_id'=>'','catid'=>$finalcatid);
 //$responce['fetname'] = $this->supper_admin->call_procedure('proc_productmasterexcel',$parameter); 
$parameter           =array(
  'act_mode'=>'feature',
  'row_id'=>'',
  'pvalue'=>$finalcatid
  );
 $responce['fetname'] = $this->supper_admin->call_procedure('proc_productmasterexcel',$parameter);
//p($responce['fetname']);
 foreach ($responce['fetname'] as $key => $value) {
   $featurevaluee[]   = $value->LeftValue;
   $featurevaluee1[]  = $value->LeftValue;
   $featureleftid[]   = $value->FeatureLeftID;
   $groupfeature[$key]= $value->LeftValue;
 }//foreach
 $featurevalue        = array_unique($featurevaluee);
 $newfeatureleftid    = array_unique($featureleftid);

 foreach ($newfeatureleftid as $ke => $value) {
    $parameter        = array(
      'act_mode'=>'getleftfromright',
      'row_id'=>$value,
      'feaid'=>'',
      '',
      ''
      );
    $data['featureData'][$groupfeature[$ke]]  = $this->supper_admin->call_procedure('proc_feature',$parameter); 
    $j = 0;
 }

  foreach ($data['featureData'] as $k=>$v){
    foreach ($v as $kk=>$vv) {
      if($k == $vv->LeftValue){
        $newfeatureData[$k]   .= $vv->RightValue.', ';
        $newfeatureData2[$k][] = $vv->RightValue;
      }//if
    }//foreach

    $finalfetureData[$k]  = substr($newfeatureData[$k],0,-2);  
    $finalfetureData2[$k] = $newfeatureData2[$k];  
  }//foreach
//-------------------------------END ---------------------------------------------//  

//----------------------------------city group ------------------------------------//
  $parameter            = array(
    'act_mode'=>'citymasterview',
    'row_id'=>'',
    'counname'=>'',
    'coucode'=>'',
    'commid'=>''
    );
  $responce['cityview'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

  foreach ($responce['cityview'] as $key => $cityval) {  
    $citygroup[]        = $cityval->mastname;
  }//foreach
//-----------------------------------end -------------------------------------------//   

//------------------------------ ATTRIBUTE------------------------------------------//   
 $parameter             = array('act_mode'=>'catatt',
  'row_id'=>'',
  'catid'=>$finalcatid
  );
 $responce['attname']   = $this->supper_admin->call_procedure('proc_product',$parameter); 

foreach($responce['attname'] as $k=>$v){
    $oldexcelHeadArr[]  = $v->attname;
    $oldexcelHeadArr1[] = $v->attname;
    $excelHeadArrcvc[]  = $v->id;
    $allArr[]           = $v->attname;
    $grouparr[$k]       = $v->attname;
}
//p($featurevalue);
$excelHeadArr  = array_merge($featurevalue,$oldexcelHeadArr);
$excelHeadArrqq1 = array_merge($featurevaluee1,$oldexcelHeadArr1);
$excelHeadArr1 = array_unique($excelHeadArrqq1);

//-------------------------------------- END -----------------------------------------//
//------------------------------------ATTRIBUTE VALUE----------------------------------//
foreach ($excelHeadArrcvc as $key => $value) {
    $parameter    = array('act_mode'=>'attvalue',
      'row_id'=>$value,
      'catid'=>''
      );
    $data['attData'][$grouparr[$key]] = $this->supper_admin->call_procedure('proc_product',$parameter);
    $j            = 0;
 }

foreach ($data['attData'] as $k=>$v) {
  foreach ($v as $kk=>$vv) {
    if($k == $vv->attname){
      $attributeData[$k]    .= $vv->attmapname.', ';
      $attributeData2[$k][] = $vv->attmapname;
    }
  }//foreach

  $oldfinalAttrData[$k]  = substr($attributeData[$k],0,-2);
  $oldfinalAttrData2[$k] = $attributeData2[$k];

}
  
  $oldfinalAttrData['categories']       = $finalcatname;
  $oldfinalAttrData2['categories']      = $catname;
  $oldfinalAttrData['maincategories']   = $finalmcatname;  
  $oldfinalAttrData2['maincategories']  = $mcatname;
  $oldfinalAttrData2['ManufactureCode'] = $manufvalue;
  $oldfinalAttrData['ManufactureCode']  = $vendorname;
  $finalAttrData  = array_merge($oldfinalAttrData,$finalfetureData);
  $finalAttrData2 = $oldfinalAttrData2;
  //$finalAttrData2 = array_merge($oldfinalAttrData2,$finalfetureData2);
   //p($finalAttrData);
   //p($oldfinalAttrData2);exit;
  

//--------------------------------------------END ATTRIBUTE VALUE -------------------------------//
$attCount = array();
foreach($finalAttrData as $k=>$v){
 $valueCount   = count(explode(",", $v));
 $attCount[$k] = $valueCount+1;
} 
//p($attCount[$k]);exit;
$objPHPExcel   = new PHPExcel();
//-------------------------------- First defult Sheet -------------------------------------------------//
$objWorkSheet  = $objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(0);
$finalExcelArr1 = array_merge($excelHeadArr1);
//p($finalExcelArr1);exit();
$cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
$j      = 2;
//p($finalAttrData2);exit;
for($i=0;$i<count($finalExcelArr1);$i++){
 $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
 foreach ($finalAttrData2 as $key => $value) {
  foreach ($value as $k => $v) {
    if($key == $finalExcelArr1[$i]){
      $newvar = $j+$k;
      $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
   }
  }
 }  
}
//exit;
$objPHPExcel->getSheetByName('Worksheet')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
//----------------------------------end first defult sheet ---------------------------------------------// 

//---------------------------------state first product sheet--------------------------------------------//
$arrPart2   = array('ProductName','ArticleNumber','ProductSku','BarcodeNumber','ProductDescription','Stock_QTY','PurchasePrice','Mrp','SellingPrice');
$arrPart22  = array('ProfitMargin_percent','Discount_percent','Range1','Range2','Range3','Price1_percent','Price2_percent','Price3_percent','Height','MeasuringUnit1','Breadth','MeasuringUnit2','Length','MeasuringUnit3','Weight','MeasuringUnit4','VolumetricWeight_KG','Productwarranty','MetaTag1','MetaTag2','MetaTag3','MainImage','Image1','Image2','Image3','Image4','maincategories','categories','ManufactureCode');
$finalExcelArr = array_merge($arrPart2,$citygroup,$arrPart22,$excelHeadArr);
$objPHPExcel->setActiveSheetIndex(1);
$totalfinal = count($finalExcelArr);
$totalatt   = count($excelHeadArr);
$totalfeatures = count($featurevalue);
//p($excelHeadArr);
//p($totalatt);exit;
$total      = $totalfinal-$totalatt;
$objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#'.$brandid.'');
//$objPHPExcel->getActiveSheet()->setTitle('VendorProductUploadWorksheet#'.$brandid.'');
$cols       = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
 
//Set border style for active worksheet
$styleArray = array(
      'borders' => array(
          'allborders' => array(
            'style'  => PHPExcel_Style_Border::BORDER_THIN
          )
      )
);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArray);
//Set border style for active worksheet

 for($i=0;$i<count($finalExcelArr);$i++){
  // Add heading column to worksheet.
  $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]); 

  //For freezing top heading row.
  $objPHPExcel->getActiveSheet()->freezePane('A2');

  //Set height for column head.
  $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
  
  //Set width for column head.
  $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

  //protect whole sheet.
  /*$objPHPExcel->getActiveSheet()->getProtection()->setPassword('bizgain');
  $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
  $objPHPExcel->getActiveSheet()->getStyle('A2:'.$cols[$totalfinal].'500')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);*/

  //Set background color for heading column.
  $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
      array(
          'fill' => array(
              'type'  => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => '71B8FF')
          ),
          'font'  => array(
              'bold'  => false,
              'size'  => 15,
          )
      )
  );

for($k=2;$k <1000;$k++){

    //Set height for every single row.
    $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);

    // Formula for calculating discount.
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$k.'', '= ((1-(I'.$k.')/(H'.$k.'))*100)');
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$k.'', '= (((I'.$k.')/(G'.$k.')-1)*100)');
    $objPHPExcel->getActiveSheet()->setCellValue('AC'.$k.'', '= ((U'.$k.'*W'.$k.'*Y'.$k.')/5000)');
   
    //Create select box for Manufacturer.
    $objValidation22 = $objPHPExcel->getActiveSheet()->getCell('AO2')->getDataValidation();
    $objValidation22->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation22->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation22->setAllowBlank(false);
    $objValidation22->setShowInputMessage(true);
    $objValidation22->setShowErrorMessage(true);
    $objValidation22->setShowDropDown(true);
    $objValidation22->setErrorTitle('Input error');
    $objValidation22->setError('Value is not in list.');
    $objValidation22->setPromptTitle('Pick from list');
    $objValidation22->setPrompt('Please pick a value from the drop-down list.');
    $objValidation22->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['ManufactureCode']));
    $objPHPExcel->getActiveSheet()->getCell('AO'.$k)->setDataValidation($objValidation22);

    //Create select box for categories.
    $objValidation2 = $objPHPExcel->getActiveSheet()->getCell('AM2')->getDataValidation();
    $objValidation2->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation2->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation2->setAllowBlank(false);
    $objValidation2->setShowInputMessage(true);
    $objValidation2->setShowErrorMessage(true);
    $objValidation2->setShowDropDown(true);
    $objValidation2->setErrorTitle('Input error');
    $objValidation2->setError('Value is not in list.');
    $objValidation2->setPromptTitle('Pick from list');
    $objValidation2->setPrompt('Please pick a value from the drop-down list.');
    $objValidation2->setFormula1('Worksheet!$'.'C$2:$'.'C$'.($attCount['maincategories']));
    $objPHPExcel->getActiveSheet()->getCell('AM'.$k)->setDataValidation($objValidation2);


    $objValidation2 = $objPHPExcel->getActiveSheet()->getCell('AN2')->getDataValidation();
    $objValidation2->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation2->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation2->setAllowBlank(false);
    $objValidation2->setShowInputMessage(true);
    $objValidation2->setShowErrorMessage(true);
    $objValidation2->setShowDropDown(true);
    $objValidation2->setErrorTitle('Input error');
    $objValidation2->setError('Value is not in list.');
    $objValidation2->setPromptTitle('Pick from list');
    $objValidation2->setPrompt('Please pick a value from the drop-down list.');
    $objValidation2->setFormula1('Worksheet!$'.'B$2:$'.'B$'.($attCount['categories']));
    $objPHPExcel->getActiveSheet()->getCell('AN'.$k)->setDataValidation($objValidation2);

//p(count($finalExcelArr1));
     if($i>=$total+$totalfeatures){
      //p($oldexcelHeadArr1);
      for($h=0; $h <count($finalExcelArr1); $h++) { 
        if($finalExcelArr1[$h] == $finalExcelArr[$i]){
        
          $objValidation = $objPHPExcel->getActiveSheet()->getCell($cols[$i].'2')->getDataValidation();
          $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
          $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
          $objValidation->setAllowBlank(false);
          $objValidation->setShowInputMessage(true);
          $objValidation->setShowErrorMessage(true);
          $objValidation->setShowDropDown(true);
          $objValidation->setErrorTitle('Input error');
          $objValidation->setError('Value is not in list.');
          $objValidation->setPromptTitle('Pick from list');
          $objValidation->setPrompt('Please pick a value from the drop-down list.');
          $objValidation->setFormula1('Worksheet!$'.$cols[$h].'$2:$'.$cols[$h].'$'.$attCount[$finalExcelArr[$i]].'');
          $objPHPExcel->getActiveSheet()->getCell($cols[$i].$k)->setDataValidation($objValidation);
        }
      }//forfro
    } //thfor
  }//secfor
}//first main for
#exit;

//----------------------------------end product sheet --------------------------------------------------//
$filename  = 'vendorexcel.xls';
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
ob_end_clean();
ob_start();
$objWriter->save('php://output');
exit;

}// end final if; 
// --------------------- End Download Excel condition ----------------------


// --------------------- Upload Excel condition ----------------------
if($this->input->post('submit')){
 $barndvalueid= $this->input->post('product_brandid');
 $fileName    = $_FILES['userfile']['tmp_name'];
 $objPHPExcel = PHPExcel_IOFactory::load($fileName);
 $rowsold     = $objPHPExcel->getActiveSheet()->toArray(); 
 $sheetName   = $objPHPExcel->getActiveSheet()->getTitle();
//--------------------------------- attribute value feature value --------------------------------//
 $brandid              = explode('#', $sheetName);
 $parameter            = array('act_mode'=>'bcategory',
  'row_id'=>$brandid[1],
  ''
  );
 $responce['category'] = $this->supper_admin->call_procedure('proc_product',$parameter); 

 foreach ($responce['category'] as $key => $value) {
  $catid[]   = $value->catid;
  $catname[] = $value->catname;
 }
 $finalcatid           = implode($catid, ',');
 $parameter            = array('act_mode'=>'catatt',
  'row_id'=>'',
  'catid'=>$finalcatid
  );
 $responce['attname']  = $this->supper_admin->call_procedure('proc_product',$parameter); 

 foreach ($responce['attname'] as $key => $value) {
  $attname[]  = $value->attname.' varchar(250)';
  $attname2[] = $value->attname;
  $attIds[$value->attname] = $value->id;
}
// --------------- final attribute value --------------------- //
$finalattname  = implode($attname, ',');
$finalattname2 = implode($attname2, ',');

$attsql        = 'CREATE temporary TABLE Attributetable ( id INT NOT NULL AUTO_INCREMENT Primary key ,Dynamic_Attribute VARCHAR(250) NOT NULL , Dynamic_Attributevalue VARCHAR(250) NOT NULL, Attid VARCHAR(250) NOT NULL);';   
$attinsert     = 'insert into Attributetable(Dynamic_Attribute,Dynamic_Attributevalue,Attid)';
// --------------------------- end --------------------------//

//----------------- feature value -----------------------//
$parameter            = array('act_mode'=>'feature','row_id'=>'','catid'=>$finalcatid);
$responce['fetname']  = $this->supper_admin->call_procedure('proc_product',$parameter); 

foreach ($responce['fetname'] as $key => $value) {
   $featurevalue[]  = $value->LeftValue.' varchar(250)';
   $featurevalue2[] = $value->LeftValue;
   $fcatIds[$value->LeftValue] = $value->FeatureID;
 }

$featurevalue_unique        = array_unique($featurevalue2);

$finalfeature  = implode($featurevalue, ',');
$finalfeature2 = implode($featurevalue2, ',');
$featuresql    = 'CREATE temporary TABLE tblfeature ( id INT NOT NULL AUTO_INCREMENT Primary key ,feature_left VARCHAR(250) NOT NULL , feture_rightvalue VARCHAR(250) NOT NULL,featuid VARCHAR(250) NOT NULL);';   
$fearureinsert = 'insert into tblfeature(feature_left,feture_rightvalue,featuid)';
//--------------------------------end feature --------------------------------------//

//-----------------------------city value ---------------------------------------//
$parameter            = array('act_mode'=>'citymasterview','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
$responce['cityview'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
foreach ($responce['cityview'] as $key => $cityval) {
  $citygroup[] = $cityval->mastname;
  $cityid[]    = $cityval->cmasterid;
  $citycatIds[$cityval->mastname] = $cityval->cmasterid;
} 

$citysql    = 'CREATE temporary TABLE tblcitynew ( id INT NOT NULL AUTO_INCREMENT Primary key ,citygid VARCHAR(250) NOT NULL , cityvalues VARCHAR(250) NOT NULL);';   
$cityinsert = 'insert into tblcitynew(citygid,cityvalues)';
//--------------------------------end feature --------------------------------------//

//-----------------------------------end attribute value --------------------------//
$headArr = array();
//-------------- remove empty array ------------------------//
#$data    = array_map('array_filter', $rowsold);
#$rows    = array_filter($data);

#$data    = array_map('array_filter', $rowsold);
#$rows    = array_filter($data);

$countAttribute   = count($attname);//Total dynamic attribute count.
$countFeature     = count($featurevalue_unique);//Total dynamic feature count.

$commattfeatu     = $countAttribute + $countFeature;
$countColumn      = (int) 41 + $commattfeatu; //Total dynamic excel columns.

$staticvaluetotal = $countColumn - $commattfeatu;
$totalfeasture    = $staticvaluetotal + $countFeature;
$totalattvalue    = $totalfeasture + $countAttribute;
//p($staticvaluetotal);//exit;
//p($totalfeasture);
//---- new logic for removing extra columns.
$rowsold_upd = array();
$rowsold_empty = array();

/*foreach($rowsold as $k=>$v){
  #print ' <br>- top - '.gettype($k);
  if($v[0] == ""){
    unset($v);
  }
  $rowsold_upd[$k] = array_splice($v, $countColumn+1);
  #print '<br> - bottom - '.gettype($k);
  $rowsold[$k] = $v;
  if($k == NULL){
    unset($k);
  }
}*/
for($i=0;$i<count($rowsold);$i++){
  if($rowsold[$i][0] == ""){
    $rowsold_empty[$i] = $i;
  }
  if(is_array($rowsold[$i])){
    $rowsold_upd[$i]   = array_splice($rowsold[$i], $countColumn+1);
  }
}
$rowsold_final         = array_diff_key($rowsold, $rowsold_empty);

//---- new logic for removing extra columns.
$data = $rowsold;
$rows = $data;
#p($rowsold_final);
#exit;
//  -----------------end --------------------------------------//
for($i = 0; $i<count($rows); $i++){
 for($j = 0; $j<count($rows[$i]); $j++){}
  $headArr[] = $rows[$i];
}
//p($headArr);
for($k=0;$k<count($headArr);$k++){
 if(count($headArr[$k])>2){
 if(gettype($headArr[$k]) == 'array'){
  if($k == 0){
  // ----------------------- create table------------------ //
  $sql = 'CREATE temporary TABLE ExcelTable (';
  for($c=0;$c<count($headArr[0]); $c++){
    if($headArr[0][$c]!=''){
      if($c == 4){
        $sql .= $headArr[0][$c].' text, ';      
      }else{
        $sql .= $headArr[0][$c].' VARCHAR(250),';
      }
    }
  }//foreach

  $sql   .= ')';
  $newsql = substr_replace($sql ,"",-2);
  $newsql.= ')';

    //------------------------end----------------------------//
   
    //------------------------insert table value -------------//
  $sql3  = 'insert into ExcelTable(';
  for($t=0;$t<count($headArr[0]); $t++) {
    if($headArr[0][$t] != ''){
      $sql3    .= $headArr[0][$t].',';
      $sqldata .= $headArr[0][$t].',';
    }
  }
  $sql3    .= ')';
  $newsql3  = substr_replace($sql3 ,"",-2);
  $newsql3 .= ')';

  //---------------------------end value----------------------//

}//end $k==0;

#if($k > 0){
#p(count($rowsold_final));
#exit;
if(($k > 0) && ($k < count($rowsold_final))){

//-------------------feature value ------------------//
$citysql4 = 'values ';
$uur      = 0;

for($ir=9;$ir<12; $ir++){
 $cityVals[$citygroup[$uur]] = $headArr[$k][$ir];//create attribute values array
 $uur++;
}

$citysql4_4     = '';
foreach($cityVals as $c=>$d){
  $citysql4_4[] = "('".$citycatIds[$c]."','".$d."')";
}
$allcitys     = implode(",", $citysql4_4);
$finalcity    = $citysql4.$allcitys;
$citynewsql5  = $cityinsert;
$citynewsql5 .= $finalcity;
$citynewsql5 .= ";";
//---------------------end ------------------------------//

//-------------------feature value ------------------//
$featuresql4 = 'values ';
$uu          = 0;

foreach ($featurevalue_unique as $key => $featurenevalue) {
  $featurenewdatavalue[]=$featurenevalue;
}
//p($featurenewdatavalue);
for($ir=$staticvaluetotal;$ir<$totalfeasture; $ir++){
 $fetaVals[$featurenewdatavalue[$uu]] = $headArr[$k][$ir];//create attribute values array
 $uu++;
}
//p($fetaVals);exit;
$featusql4_4 = '';
foreach($fetaVals as $c=>$d){
  $featusql4_4[] = "('".$c."','".$d."','".$fcatIds[$c]."')";
}
$allfeaturVals  = implode(",", $featusql4_4);
$finalfetur     = $featuresql4.$allfeaturVals;
$featurnewsql5  = $fearureinsert;
$featurnewsql5 .= $finalfetur;
$featurnewsql5 .= ";";
//---------------------end ------------------------------//

//--------------attribute value -------------------------//

$attsql4 = 'values ';
$u       = 0;
//p($totalfeasture);
//p($totalattvalue);
for($irt = $totalfeasture;$irt<$totalattvalue; $irt++){
 $attVals[$attname2[$u]] = $headArr[$k][$irt];//create attribute values array
 $u++;
}
$attsql4_4 = '';
//p($attVals);exit;
foreach($attVals as $c=>$d){
  $attsql4_4[] = "('".$c."','".$d."','".$attIds[$c]."')";
}

$allAttVals = implode(",", $attsql4_4);
$finalAtt   = $attsql4.$allAttVals;
$attnewsql5 = $attinsert;
$attnewsql5.= $finalAtt;
$attnewsql5.= ";";

//----------------end attribute ----------------------------//
  $sql4 = 'values(';
  for($i=0;$i<count($headArr[$k]); $i++) {
    #if(trim($headArr[$k][$i])!=''){
      if($headArr[$k][0] !=""){
        //if(($i == 2) OR ($i == 4)){// changed : 13 apr 16
        if(($i == 0) OR ($i == 4)){
           $sql4 .= "'".addslashes($headArr[$k][$i])."'".',';
        }
        else if(($i == 14) OR ($i == 15) OR ($i == 16)){
           $sql4 .= "'".str_replace(" ","",$headArr[$k][$i])."'".',';
        }


else if(($i == 12) OR ($i == 13)){ 

  if($headArr[$k][$i] == '#DIV/0!'){
        $sql4 .= "'".str_replace("#DIV/0!",0,$headArr[$k][$i])."'".',';
      } else {
        $sql4 .= "'".$headArr[$k][$i]."'".',';
      }
}

        else{
           $sql4 .= "'".$headArr[$k][$i]."'".',';
        }
      }// if($headArr[$k] !="")
    #}
  }
  $sql4    .= ')';

  $newsql4  = substr_replace($sql4 ,"",-2);
  $newsql4 .= ')';
  $newsql5  = $newsql3;
  $newsql5 .= $newsql4;
  $newsql5 .= ";";
  
  $parameter = array(
    'sqlquery'=>$newsql,
    'sqlquery2'=>$newsql5,
    'sqlattquery'=>$attsql,
    'sqlattquery2'=>$attnewsql5,
    'sqlfeatquery'=>$featuresql,
    'sqlfeatquery2'=>$featurnewsql5 ,
    'sqlcity'=>$citysql,
    'sqlcity2'=>$citynewsql5,
    'brandid'=>$barndvalueid
    );
  //p($parameter);exit;
  $responce['upload'] = $this->supper_admin->call_procedure('proc_bulkuploadtest',$parameter);
  $responce['excelErr'] = 'Product excel successfully uploaded.';
 
 }
} 
}// divif end
}//for end
  //exit();
move_uploaded_file($_FILES['userfile']['tmp_name'], FCPATH.'newproductexcels/'.time().'-'.$barndvalueid.'-'.$_FILES['userfile']['name']);
}//firstsubmit if;

  $parameter           = array(
    'act_mode'=>'brmview',
    'row_id'=>'',
    'brandname'=>'',
    'brandimage'=>'',
    ''
    );
  $responce['vieww']   = $this->supper_admin->call_procedure('proc_brand',$parameter); 
  $this->load->view('helper/header');
  $this->load->view('product/productexcel',$responce);

}
/////// -------------  End Function  ------------ ////////










  public function usernewsletter(){
    
    $this->userfunction->loginAdminvalidation();
    $parameter=array('act_mode'=>'newsdata','useremail'=>'','n_status'=>'','rowid'=>'');
    $record['newsview']=$this->supper_admin->call_procedure('proc_newsletter',$parameter);

     //----------------  Download Newsletter Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Email','Date & Time');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Newsletter Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($record['newsview'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->email);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->createdon);
            }
          }

          $filename='Newsletter Subscribers Listing.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Newsletter Excel ------------------------// 

    
    $this->load->view('helper/header');
    $this->load->view('user/usernewletter',$record);
    
  }

function enquiryview(){
  $parameter=array('act_mode'=>'enview','useremail'=>'','n_status'=>'','rowid'=>'');
  $record['newsview']=$this->supper_admin->call_procedure('proc_newsletter',$parameter);
   //----------------  Download Newsletter Excel ----------------------------//

          if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Name','Contact Number','Email','Product Name','Message','Date');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Enquiry Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($record['newsview'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->firstname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->contactnumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->compemailid);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->ProductName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->en_message);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->created_on);
            }
          }

          $filename='Enquires Listing.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
          }
      //----------------  End Download Newsletter Excel ------------------------//

       //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/user/enquiryview?";
      $config['total_rows']       = count($record['newsview']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $record["links"]  = explode('&nbsp;',$str_links );

     $parameter=array('act_mode'=>'enview','row_id'=>$page,'p_userid'=>$second,'p_email'=>'','p_mobilenum'=>'','p_mas'=>'');
     $record['newsview']=$this->supper_admin->call_procedure('proc_enquiry',$parameter);

    //----------------  end pagination ------------------------//   

  $this->load->view('helper/header');
  $this->load->view('user/enquirydetail',$record);
}





  function userInactive($id){
            $userInactive=api_url().'userapi/InactiveUser/id/'.$id.'/format/json/';
             $response['userInactive'] = curlget($userInactive);
            if(!empty($response['userInactive']) ){
              $data['success'] = "success" ;
               json_encode($data);
              redirect(base_url().'admin/user/userlisting');
            }else{
              $data['fail'] ="something went wrong";
              echo json_encode($data);
            }
    }

    function userActive($id){
             $ActiveUser=api_url().'userapi/ActiveUser/id/'.$id.'/format/json/';
             $response['ActiveUser'] = curlget($ActiveUser);
             
            if(!empty($response['ActiveUser'])){
              $data['success'] = "success" ;
               json_encode($data);
              redirect(base_url().'admin/user/userlisting');
            }else{
              $data['fail'] ="something went wrong";
              echo json_encode($data);
            }

    }
      public function addrole()
    {       
    $this->load->view('helper/header');
    if($this->input->post('addrole')=='Submit')
    {
      $userid = $this->session->userdata('bizzadmin')->LoginID;
      $parameter = array('act_mode'=>'addrole','rolename' =>  $this->input->post('RName'),'Param3'=>'','Param4'=>'','Param5'=>'','Param6'=>$userid);
      //p($parameter);
      $this->supper_admin->call_procedureRow('proc_addrole',$parameter);
      $this->session->set_flashdata('message', 'Your data successfully Inserted!');
      redirect('admin/rollmaster/rolelist');
    }
    $this->load->view('rollmanagement/addrole');
      
  }

  public function employeeregistration()
  {    
      $this->load->view('helper/header');
      $this->load->library('form_validation');
      $password = $this->input->post('password');
      if($this->input->post('deliverman')) {

          $this->form_validation->set_rules('email', 'Email', 'required');
          if($this->form_validation->run() != FALSE)
          {   
              $role_userid = $this->session->userdata('bizzadmin')->LoginID;      
              $datedobcreate=date_create($this->input->post('dateofbirth'));
              $dobformat=date_format($datedobcreate,"Y-m-d");

              $rid=implode(',', $this->input->post('retailernm'));
              $parameter       = array(
              'fm_name'      =>  $this->input->post('FName'),
              'lm_name'      =>  $this->input->post('LName'),
              'fatm_name'    =>  $this->input->post('FatherName'),
              'm_phone'      =>  $this->input->post('phone'),
              'm_password'   =>  trim(md5($this->input->post('password'))),
              'm_email'      =>  $this->input->post('email'),
              'm_gender'     =>  $this->input->post('gender'),
              'm_role'       =>  $this->input->post('role'),
              'm_birthdate'  =>  $dobformat,  
              'm_joiningdate'=>  $this->input->post('dateofjoining'),        
              //'m_city'       =>  $this->input->post('city'),
              'm_city'       =>  $role_userid,
              'm_address'    =>  $this->input->post('manageraddress'),
              'm_pincode'    =>  $this->input->post('pin'),
              'act_mode'     =>  'insert',
              'm_id'         =>  base64_encode($this->input->post('password')),
              'retailid'     =>  $rid
              );

              $data['result'] = $this->supper_admin->call_procedureRow('proc_empreg',$parameter);
              $this->session->set_flashdata('message', 'Your information was successfully Inserted.');
              redirect(base_url().'admin/rollmaster/employeelist');
          }
      }
      $parameter1=array();
      $data['rolename']=$this->supper_admin->call_procedure('proc_getrollname',$parameter1);
      $parameter1=array('act_mode'=>'viewwreta','row_id'=>'','pvalue'=>'');
      $data['retaname']=$this->supper_admin->call_procedure('proc_productmasterdata',$parameter1);
      $this->load->view('rollmanagement/empregistration',$data);
  }

  public function employeelist()
  {  
    $this->load->view('helper/header');
    $parameter = array(
          'roles' => $roles,
          'userid' => $userid,
          'menuid' => $menuid,
          'submenuid' => $submenuid,
          'act_mode' => 'viewemployee',
          'start'=>'',
          'total_rows'=>''
          );    


     $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);

    //----------------  Download Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Employee Name','Phone No.','Email','Address','Role');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Employee Role Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($data['list'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->F_Name.' '.$value->L_Name);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->ContactNo);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->Email);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->Address);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->rolename);
      
            }
          }

          $filename='Employee Role Listing.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Newsletter Excel ------------------------// 

          //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/user/employeelist?";
      $config['total_rows']       = count($data['list']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $data["links"]  = explode('&nbsp;',$str_links );

    $parameter = array(

          'roles' => $roles,
          'userid' => $userid,
          'menuid' => $menuid,
          'submenuid' => $submenuid,
          'act_mode' => 'viewemployee',
          'start'=>'',
          'total_rows'=>''

          );
      


     $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);

    //----------------  end pagination ------------------------//  


    $this->load->view('rollmanagement/employeelist',$data);
    //$this->load->view('helper/footer');   
  }
  public function employeelistedit()
  {    
      $employeeid =   $this->uri->segment(4);
      $this->load->view('helper/header');
      $parameter = array(
        'roles' => '',
        'userid' => $employeeid,
        'menuid' => '',
        'submenuid' => '',
        'act_mode' => 'hm_editemployee',
        'start'=>'',
        'total_rows'=>''
      );
      $data['editlist'] = $this->supper_admin->call_procedureRow('proc_assignrole', $parameter);   
      
      if($this->input->post('deliverman')=='Submit')
      {
          $role_userid = $this->session->userdata('bizzadmin')->LoginID;
          $parameter = array(
          'fm_name' =>  $this->input->post('FName'),
          'lm_name' =>  $this->input->post('LName'),
          'fatm_name' =>  $this->input->post('FatherName'),
          'm_phone' =>  $this->input->post('phone'),
          'm_password' => '',
          'm_email'  => '',
          'm_gender'  =>  $this->input->post('gender'),
          'm_role'  =>  0,
          'm_birthdate' =>  date_format(date_create($this->input->post('dateofbirth')),"Y-m-d"),  
          'm_joiningdate' =>  $this->input->post('dateofjoining'),        
          'm_city'  =>  $role_userid,
          'm_address' =>  $this->input->post('manageraddress'),
          'm_pincode' =>  $this->input->post('pin'),
          'act_mode'  =>  'employeeedit',
          'm_id'    =>  $employeeid,
          'retailid' => ''
          );
          $data['result'] = $this->supper_admin->call_procedureRow('proc_empreg',$parameter);
          $this->session->set_flashdata('message', 'Your data successfully Updated!');
          redirect('admin/rollmaster/employeelist');
      }

      $this->load->view('rollmanagement/employeelistedit',$data);
  }

  public function employeelistdelete()
  {
    $employeeid =   $this->uri->segment(4);   
    
        $parameter = array(
        'fm_name' =>  '',
        'lm_name' =>  '',
        'fatm_name' =>  '',
        'm_phone' =>  '',
        'm_password' => '',
        'm_email'  => '',
        'm_gender'  =>  '',
        'm_role'  =>  '',
        'm_birthdate' =>  '', 
        'm_joiningdate' =>  '',       
        'm_city'  =>  '',
        'm_address' =>  '',
        'm_pincode' =>  '',
        'act_mode'  =>  'deleteedit',
        'm_id'    =>  $employeeid,
        'retailid'=>''
    );      
    $data['result'] = $this->supper_admin->call_procedureRow('proc_empreg',$parameter);
    redirect('admin/user/employeelist');  
  }

  public function createrole() {
    
    $parameter2=array('id' => '','act_mode' => 'mainmenu');
    $data['menuaccess'] = $this->supper_admin->call_procedure('proc_SubMenu', $parameter2);   
      
    //p($data['menuaccess']); exit;



    if ($this->input->post('addUserMenu')){      
        $count = 0;         
        $userid         = $this->input->post('user');//role,user,mainmenu,div1
        //echo "sdfad".$userid; exit();
      //echo $count = sizeof($this->input->post('menuid'));
      $date = date('Y-m-d');
      $this->form_validation->set_rules('user', 'User ', 'trim|required|xss_clean');
      if($this->form_validation->run() != FALSE) {
        
        $roles      = $this->input->post('role');
        $userid         = $this->input->post('user');//role,user,mainmenu,div1
        //$menuid         = $this->input->post('mainmenu');
        //$submenuid      = implode(",",$this->input->post('div2'));
        
        //p($this->input->post('div2')); exit();
        //echo "newmenu".$roles."/"$userid; 
        foreach($this->input->post('div2') as $val){
        
        $getIds = explode("-",$val);

        $parameter = array(

          'roles' => $roles,
          'userid' => $userid,
          'menuid' => $getIds[0],
          'submenuid' => $getIds[1],
           'act_mode' => 'insert',
          'start'=>'',
          'total_rows'=>''

          );

        //p($parameter); exit;
        $this->supper_admin->call_procedureRow('proc_assignrole', $parameter);        
      
        }

        /*for ($i = 0; $i < $count; $i++) {
          $parameter      = array('moduleid' => $menuid[$i], 'leftmenuid' => $submenuid[$i], 'userid' => $userid);
                    
          $result['data'] = $this->supper_admin->call_procedure('proc_user_menu', $parameter);
        }*/     
        
      }
    }
                
        

    $parameter1=array();
    $data['rolename']=$this->supper_admin->call_procedure('proc_getrollname',$parameter1);      
    $this->load->view('helper/header');
    $this->load->view('rollmanagement/roles',$data);
    //$this->load->view('helper/footer');

  }

  public function getuser() {
       //p($_POST); exit;
        foreach ($_POST as $val){           
            $parameter = array('roleid' => trim($val['roleid']));

          

            $data['combo'] = $this->supper_admin->call_procedure('proc_getuser', $parameter);
            //p($data['combo']); exit();
          
        }
        $this->load->view('rollmanagement/getusercombo',$data);
        //p($data['combo']); exit();
        
    }


    public function getsubmenss()
    {
      $parameter2 = array('Id' => $_POST['menuid'][0],'act_mode' => 'submanu');
      
      

      $data['result'] = $this->supper_admin->call_procedure('proc_SubMenu' ,$parameter2);


      

      /*p($data['result']); exit();*/
      
      $res_opt = "";
      foreach($data['result'] as $k=>$v){
        //p($v);
        $res_opt .= "<option value='".$_POST['menuid'][0].'-'.$v->id."'>".$v->menuname."</option>";

      }
       p($res_opt);   


    }
public function checkdublicatemail()
    {

     $emailuniqueid =  $this->input->post('emailid');
     
      $parameter = array
      (
        'fm_name'      =>  '',
        'lm_name'      =>  '',
        'fatm_name'    =>  '',
        'm_phone'      =>   '',
        'm_password'   =>  '',
        'm_email'      =>  $emailuniqueid,
        'm_gender'     =>  '',
        'm_role'       =>  '',
        'm_birthdate'  => '',  
        'm_joiningdate'=> '',        
        'm_city'       =>  '',
        'm_address'    =>  '',
        'm_pincode'    =>  '',
        'act_mode'     =>  'check',
        'm_id'         =>  '0',
        'retailid'     => '',
        
        );

      $data = $this->supper_admin->call_procedureRow('proc_empreg',$parameter);
      echo json_encode( $data);
}

public function Manageshipping()
{
  
  $this->load->view('helper/header');
  $this->load->view('shipping/shippinglist',$data);
     
}

public function addshipping()
{
  
  $this->load->view('helper/header');
  $this->load->view('shipping/addshipping',$data);
     
}

public function businessuserlist()
{
  
  $parameter=array('act_mode'=>'viewstartbus',
                     'row_id'=>'',
                     'b_username'=>'',
                     'b_email'=>'',
                     'b_mobilenum'=>'',
                     'b_pincode'=>'',
                     'b_address'=>'',
                     'b_status'=>''
                    );
  $data['viewbus']=$this->supper_admin->call_procedure('proc_business', $parameter);
  $this->load->view('helper/header');
  $this->load->view('user/businessdata',$data);
     
}

public function cartsdetaillist(){

  $parameter=array('act_mode'=>'viewcartdetails','u_Id'=>'','p_Id'=>'','c_id'=>'');
  $record['viewallcart']=$this->supper_admin->call_procedure('proc_Addtowishlist', $parameter);
  //p($record['viewallcart']);exit;

  //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/user/cartsdetaillist?";
      $config['total_rows']       = count($record['viewallcart']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $record["links"]  = explode('&nbsp;',$str_links );

     //$parameter=array('act_mode'=>'enview','row_id'=>$page,'p_userid'=>$second,'p_email'=>'','p_mobilenum'=>'','p_mas'=>'');
     //$record['newsview']=$this->supper_admin->call_procedure('proc_enquiry',$parameter);

     $parameter=array('act_mode'=>'viewcartdetails','u_Id'=>$page,'p_Id'=>$second,'c_id'=>'');
     $record['viewallcart']=$this->supper_admin->call_procedure('proc_Addtowishlist', $parameter);

    //----------------  end pagination ------------------------//   

  $this->load->view('helper/header');
  $this->load->view('user/userscartdetail',$record);

}

public function solrexcel(){
  $response = $this->supper_admin->call_procedure('proc_for_solr_copy', array());
/*foreach ($response as $key => $value) {
p($value);exit;
}*/

           $finalExcelArr = array('prodtmappid','PCreatedOn','cityid_cityvalue','ProId','prodtmappid','manufactureId','qty','CatName','CatId','BrandName','proQnty','ParentCat','ParentCatId','productMRP','SKUNO','SellingPrice','FinalPrice','ProDiscount','ProductName','image','BrandID','FDisId','qty1','qty2','qty3','price','price2','price3','SizeVal','Color_Val');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Employee Role Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($response as $key => $value) {
             
            $newvar = $j+$key;




            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->id);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->PCreatedOn);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->cityid_cityvalue);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->ProId);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->prodtmappid);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->manufactureId);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->qty);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->CatName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->CatId);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->BrandName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->proQnty);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->ParentCat);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->ParentCatId);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->productMRP);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $value->SKUNO);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->SellingPrice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[16].$newvar, $value->FinalPrice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[17].$newvar, $value->ProDiscount);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[18].$newvar, $value->ProductName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[19].$newvar, $value->image);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[20].$newvar, $value->BrandID);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[21].$newvar, $value->FDisId);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[22].$newvar, $value->qty1);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[23].$newvar, $value->qty2);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[24].$newvar, $value->qty3);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[25].$newvar, $value->price);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[26].$newvar, $value->price2);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[27].$newvar, $value->price3);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[28].$newvar, $value->SizeVal);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[29].$newvar, $value->Color_Val);
            
            }
          }

          $filename='Employee Role Listing.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
}

  public function allemployees()
  {
      $this->load->view('helper/header');
      $parameter = array(
      'roles' => $roles,
      'userid' => $userid,
      'menuid' => $menuid,
      'submenuid' => $submenuid,
      'act_mode' => 'viewsalesemployee',
      'start'=>'',
      'total_rows'=>''
      );    
      $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
      //----------------  start pagination ------------------------// 
      $config['base_url']         = base_url()."admin/user/employeelist?";
      $config['total_rows']       = count($data['list']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

      $this->pagination->initialize($config);
      if($_GET['page']){
      $page         = $_GET['page']-1 ;
      $page         = ($page*50);
      $second       = $config['per_page'];
      }
      else{
      $page         = 0;
      $second       = $config['per_page'];
      }     
      $str_links = $this->pagination->create_links();
      $data["links"]  = explode('&nbsp;',$str_links );
      $parameter = array(
      'roles' => $roles,
      'userid' => $userid,
      'menuid' => $menuid,
      'submenuid' => $submenuid,
      'act_mode' => 'viewsalesemployee',
      'start'=>'',
      'total_rows'=>''
      );     
      $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
      //----------------  end pagination ------------------------//  
      $this->load->view('rollmanagement/allemployees',$data); 
  } 

  public function empincentives($emp_id)
  {
      $this->load->view('helper/header');
      $parameter = array(
          'p_role' => '',
          'p_user' => $emp_id,
          'p_menu' => '',
          'p_submenu' => '',
          'act_mode' => 'fetchemployeedetail',
          'start'=>'',
          'total_rows'=>''
          );    
      $data['employee'] = $this->supper_admin->call_procedureRow('proc_assignrole',$parameter);
      $this->load->view('rollmanagement/empincentives',$data); 
  }

  public function viewincentiveslabs()
  {
      $this->load->view('helper/header');
      $parameter = array(
      'roles' => '',
      'userid' => '',
      'menuid' => '',
      'submenuid' => '',
      'act_mode' => 'viewincentiveslabs',
      'start'=>'',
      'total_rows'=>''
      );    
      $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
      $this->load->view('rollmanagement/viewincentiveslabs',$data); 
  } 

  public function addincentiveslabs()
  {
      if($this->input->post('submit')) 
      {
        $this->form_validation->set_rules('range_from[]','Sale range from','trim|required');
        $this->form_validation->set_rules('range_to[]','Sale range to','trim|required');
        $this->form_validation->set_rules('range_salaryFrom[]','Salary range from','trim|required');
        $this->form_validation->set_rules('range_salaryTo[]','Salary range tos','trim|required');
        $this->form_validation->set_rules('range_type[]','Price type','trim|required');
        $this->form_validation->set_rules('range_incentive[]','Incentive amount','trim|required');
        if($this->form_validation->run() != false) {
            for($i=0; $i<count($this->input->post('range_from')); $i++) {
              $parameter = array(
              'p_role' => $this->input->post('range_salaryFrom')[$i],
              'p_user' => $this->input->post('range_from')[$i],
              'p_menu' => $this->input->post('range_to')[$i],
              'p_submenu' => $this->input->post('range_type')[$i],
              'act_mode' => 'insertincentiveslabs',
              'start'=> $this->input->post('range_salaryTo')[$i],
              'total_rows'=>$this->input->post('range_incentive')[$i]
              );    
              $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter); 
            }                    
            $this->session->set_flashdata('message', 'Your information has been saved successfully!');
            redirect("admin/user/viewincentiveslabs");
        }
      }
      $this->load->view('helper/header');
      $parameter = array(
      'roles' => '',
      'userid' => '',
      'menuid' => '',
      'submenuid' => '',
      'act_mode' => 'viewincentiveslabs',
      'start'=>'',
      'total_rows'=>''
      );    
      $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
      $this->load->view('rollmanagement/addincentiveslabs',$data); 
  } 

  public function deleteincentiveslab($id) {
    $parameter = array(
      'p_role' => $id,
      'p_user' => '',
      'p_menu' => '',
      'p_submenu' => '',
      'act_mode' => 'deleteincentiveslab',
      'start'=>'',
      'total_rows'=>''
      );    
      $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);                   
    $this->session->set_flashdata('message', 'Your record has been deleted successfully!');
    redirect("admin/user/viewincentiveslabs");
  }

  public function editincentiveslab($id)
  {
      if($this->input->post('submit')) 
      {
        $this->form_validation->set_rules('range_from','Sale range from','trim|required');
        $this->form_validation->set_rules('range_to','Sale range to','trim|required');
        $this->form_validation->set_rules('range_salaryFrom','Salary range from','trim|required');
        $this->form_validation->set_rules('range_salaryTo','Salary range to','trim|required');
        $this->form_validation->set_rules('range_incentive','Incentive amount','trim|required');
        if($this->form_validation->run() != false) {
              $parameter = array(
              'p_role' => $id,
              'p_user' => $this->input->post('range_from'),
              'p_menu' => $this->input->post('range_to'),
              'p_submenu' => $this->input->post('range_salaryFrom'),
              'act_mode' => 'updateincentiveslab',
              'start'=>$this->input->post('range_salaryTo'),
              'total_rows'=>$this->input->post('range_incentive')
              );    
              $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);                   
            $this->session->set_flashdata('message', 'Your information has been updated successfully!');
            redirect("admin/user/viewincentiveslabs");
        }
      }
      $this->load->view('helper/header');
      $parameter = array(
          'p_role' => '',
          'p_user' => $id,
          'p_menu' => '',
          'p_submenu' => '',
          'act_mode' => 'fetchslabdetail',
          'start'=>'',
          'total_rows'=>''
          );
      $data['view'] = $this->supper_admin->call_procedureRow('proc_assignrole', $parameter);
      $this->load->view('rollmanagement/editincentiveslab',$data); 
  } 

  public function calculate_incentive()
  {
      $start_date = $_REQUEST['start_date'];
      $end_date = $_REQUEST['end_date'];
      //$start_date = $_REQUEST['start_date'].' 00:00:00';
      //$end_date = $_REQUEST['end_date'].' 00:00:00';
      $emp_id = $_REQUEST['emp_id'];
      $parameter = array(
          'p_role' => '',
          'p_user' => $emp_id,
          'p_menu' => '',
          'p_submenu' => '',
          'act_mode' => 'fetchempretailers',
          'start'=>'',
          'total_rows'=>''
          );    
      $data['retailers'] = $this->supper_admin->call_procedureRow('proc_assignrole',$parameter);
      $retailerids=trim($data['retailers']->e_retailid,',');
      $retailer_array = explode(",",$retailerids);
      echo '<div class="page_box">
              <div class="sep_box">
                <div class="col-lg-12">
                    <table class="grid_tbl">';
                        echo $this->session->flashdata("message");
                        echo '<thead>
                            <tr>
                                <th scope="col" style="align:left">Retailer Name</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Email</th>
                                <th scope="col">Company Name</th>
                                <th scope="col">Total Order Amount (In Rs.)</th>
                            </tr>
                        </thead>
                        <tbody id="tblBody">';
                                    $saleTotal=0; foreach ($retailer_array as $value) {  
                                    $resp = retailerData($value); 
                                    $total_amt = retailerTotalOrderAmt($value,$start_date,$end_date); 
                                    $saleTotal+=$total_amt;
                                    if($value!="") {
                                echo '<tr>
                                    <td data-rel="Pi ID" style="align:left" >'.$resp->firstname.' '.$resp->lastname.'</td>
                                    <td data-rel="S.No">'.$resp->contactnumber.'</td>
                                    <td data-rel="Pi Code">'.$resp->compemailid.'</td>
                                    <td data-rel="Supplier Number">'.$resp->companyname.'</td>
                                    <td data-rel="Supplier Number">'.number_format($total_amt,2,".","").'</td>
                                </tr>';
                                } }
                                echo '<tr>
                                    <td colspan="3"></td>
                                    <td><b>Total Sale : </b></td>
                                    <td><b>'.number_format($saleTotal,2,".","").'</b></td>
                                </tr>';
                                if($saleTotal!=0) { $incentive_amt = empIncentive($saleTotal,$data['retailers']->empSalary);
                                echo '<tr>
                                    <td colspan="3"></td>
                                    <td><b>Incentive Amount : </b></td>
                                    <td><b>'; if($incentive_amt=="") echo 0; else echo number_format($incentive_amt,2,".",""); echo '</b></td>
                                </tr>';
                                }
                        echo '</tbody>
                    </table>
                </div>
            </div>
        </div>';
  } 

  public function allempincentives()
  {
      $parameter = array(
      'roles' => '',
      'userid' => '',
      'menuid' => '',
      'submenuid' => '',
      'act_mode' => 'viewsalesemployee',
      'start'=>'',
      'total_rows'=>''
      );    
      $list = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
      echo '<div class="sep_box">
            <div class="col-lg-12">
            <table id="test" class="grid_tbl">';
                echo $this->session->flashdata("message");
                echo '<thead>
                    <tr>
                        <th scope="col" style="align:left">Retailer Name</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Email</th>
                        <th scope="col">Address</th>
                        <th scope="col">Role</th>
                        <th scope="col">Incentives (In Rs.)</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">';
                        foreach ($list as $employeelist) { 
                        $retailers = trim($employeelist->e_retailid,',');
                        $retailer_array = explode(",",$retailers);
                        $saleTotal=0; 
                        $start_date = $_REQUEST['start_date'];
                        $end_date = $_REQUEST['end_date'];
                        //$start_date = $start_date.' 00:00:00';
                        //$end_date = $end_date.' 00:00:00';
                        foreach ($retailer_array as $value) {  
                            $resp = retailerData($value); 
                            $total_amt = retailerTotalOrderAmt($value,$start_date,$end_date); 
                            $saleTotal+=$total_amt;                            
                        } 
                        $status = retailerIncentiveStatus($employeelist->e_retailid,$start_date,$end_date);
                        $incentive_amt = empIncentive($saleTotal,$employeelist->empSalary);                      
                        echo '<tr>
                            <td data-rel="Pi ID" style="align:left" >'.$employeelist->F_Name.''.$employeelist->L_Name.'</td>
                            <td data-rel="S.No">'.$employeelist->ContactNo.'</td>
                            <td data-rel="Pi Code">'.$employeelist->Email.'</td>
                            <td data-rel="Supplier Number">'.$employeelist->Address.'</td>
                            <td data-rel="Supplier Number">'.$employeelist->rolename.'</td>
                            <td data-rel="Supplier Number">'; if($incentive_amt=="") echo 0; else echo number_format($incentive_amt,2,".",""); echo '</td>
                            <td data-rel="Supplier Number" align="center" id="status_td_'.$employeelist->EmployeeID.'">';
                                if($incentive_amt!="" || $incentive_amt!=0) { if($status=='paid') { echo '<span style="color:green">Paid</span>'; } else { 
                                echo '<select id="incentive_status" class="form-control"'; ?> onchange="return changeIncStatus(this.value,'<?php echo $employeelist->e_retailid;?>','<?php echo $start_date;?>','<?php echo $end_date;?>','<?php echo $employeelist->EmployeeID;?>');"  <?php echo '>
                                    <option value="pending"'; if($status=='pending') echo 'selected'; echo '>Pending</option>
                                    <option value="paid"'; if($status=='paid') echo 'selected'; echo '>Paid</option>
                                </select>';
                                } }
                            echo '</td>
                            <td data-rel="Supplier Number">
                            <a href="'.base_url().'admin/user/empincentives/'.$employeelist->EmployeeID.'"><i class="fa fa-view"></i> View Incentives</a>
                            </td>
                        </tr>';
                        }                         
                echo '</tbody>
            </table>
            </div>
        </div>';
  }

  public function changeincentive_status() {
      $retailers = trim($_REQUEST['retailers'],',');
      $parameter = array(
      'p_role' => $_REQUEST['start'],
      'p_user' => $_REQUEST['empid'],
      'p_menu' => $retailers,
      'p_submenu' => $_REQUEST['end'],
      'act_mode' => 'changeincentivestatus',
      'start'=> $_REQUEST['incentive_status'],
      'total_rows'=>''
      );    
      $resp = $this->supper_admin->call_procedure('proc_assignrole', $parameter);   
      echo '<span style="color:green">Paid</span>';                
  }

  public function removeempretailers($empid) {
     $parameter = array(
        'roles' => '',
        'userid' => $empid,
        'menuid' => '',
        'submenuid' => '',
        'act_mode' => 'editemployee',
        'start'=>'',
        'total_rows'=>''
      );
      $data['editlist'] = $this->supper_admin->call_procedureRow('proc_assignrole', $parameter);   
      $empretial=explode(',',$data['editlist']->e_retailid);
      $newretaills=array_diff($empretial, $_POST['alexistretailer']);
      
      $parameters = array(
        'roles' => '',
        'userid' => $empid,
        'menuid' => implode(',', $newretaills),
        'submenuid' => '',
        'act_mode' => 'remexistretail',
        'start'=>'',
        'total_rows'=>''
      );
      $record = $this->supper_admin->call_procedureRow('proc_assignrole', $parameters);  
  }


  function feedback(){
    $this->userfunction->loginAdminvalidation();
    
    $params = array('act_mode'=>'view_feedback',
                   'row_id'=>'', 
                   'Param3'=>'',
                   'Param4'=>'',
                   'Param5'=>'',
                   'Param6'=>'',
                   'Param7'=>'',
                   'Param8'=>'',
                   'Param9'=>'',
                   'Param10'=>'');
    $response['viewfeedback']=$this->supper_admin->call_procedure('proc_feedback',$params);
    
    $this->load->view('helper/header');
    $this->load->view("user/feedback",$response); 
  }

  function feedbackdelete($id){
    $this->userfunction->loginAdminvalidation();
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $params = array('act_mode'=>'del_feedback',
                   'row_id'=>$id, 
                   'Param3'=>$userid,
                   'Param4'=>'',
                   'Param5'=>'',
                   'Param6'=>'',
                   'Param7'=>'',
                   'Param8'=>'',
                   'Param9'=>'',
                   'Param10'=>'');
    $response['viewfeedback']=$this->supper_admin->call_procedure('proc_feedback',$params);
    $this->session->set_flashdata("message", "Feedback deleted successfully.");
    redirect(base_url().'admin/user/feedback');
  }

  function feedbackstatus($id){
    $this->userfunction->loginAdminvalidation();
    $rowid             = $this->uri->segment(4);
    $status            = $this->uri->segment(5);
    $u_status          = $status == '0'?'1':'0';
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $params = array('act_mode'=>'status_feedback',
                   'row_id'=>$rowid, 
                   'Param3'=>$userid,
                   'Param4'=>$u_status,
                   'Param5'=>'',
                   'Param6'=>'',
                   'Param7'=>'',
                   'Param8'=>'',
                   'Param9'=>'',
                   'Param10'=>'');
    $response['viewfeedback']=$this->supper_admin->call_procedure('proc_feedback',$params);
    $this->session->set_flashdata("message", "Feedback status updated successfully.");
    redirect(base_url().'admin/user/feedback');
  }

}
?>