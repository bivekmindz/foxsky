<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Wallet extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->userfunction->loginAdminvalidation();
    
  }
  
  public function index(){

    $parameter=array('act_mode'=>'walletlist','row_id'=>'','wa_orderid'=>'','wa_userid'=>'','wa_type'=>'','wa_amt'=>'','wa_status'=>'');
    $record['userswallet'] = $this->supper_admin->call_procedure('proc_wallet',$parameter);

    //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/Wallet/index?";
     $config['total_rows']       = count($record['userswallet']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );
     $parameter=array('act_mode'=>'walletlist','row_id'=>$page,'wa_orderid'=>'','wa_userid'=>$second,'wa_type'=>'','wa_amt'=>'','wa_status'=>'');
     $record['userswallet']   = $this->supper_admin->call_procedure('proc_wallet',$parameter);
//p($record['userswallet']);exit;
     //----------------  end pagination setting ------------------------//


    $this->load->view('helper/header');
    $this->load->view('adminwallet/viewwalletlist',$record);
  }

  public function walletdetail($wallid){
    
    $parameter=array('act_mode'=>'getwalletdrcr',
                     'row_id'=>$wallid,
                     'wa_orderid'=>'',
                     'wa_userid'=>'',
                     'wa_type'=>'',
                     'wa_amt'=>'',
                     'wa_status'=>''
                     );
    $record['userswallet'] = $this->supper_admin->call_procedure('proc_wallet',$parameter);
    $parametermandet=array('act_mode'=>'getmandetwallet',
                     'row_id'=>$wallid,
                     'wa_orderid'=>'',
                     'wa_userid'=>'',
                     'wa_type'=>'',
                     'wa_amt'=>'',
                     'wa_status'=>''
                     );
    $record['manufacdetwallet'] = $this->supper_admin->call_procedureRow('proc_wallet',$parametermandet);
    $this->load->view('helper/header');
    $this->load->view('adminwallet/viewwalletdetail',$record);
  }



  public function addwalletmoney(){
    
    if($this->input->post('submit')){



      if($this->input->post('userwalltype')=='credit'){
          $walletamt=$this->input->post('userwallamt');
      } else {
          $parameter=array('act_mode'=>'getwalletdrcr',
                     'row_id'=>$this->input->post('userwallid'),
                     'wa_orderid'=>'',
                     'wa_userid'=>'',
                     'wa_type'=>'',
                     'wa_amt'=>'',
                     'wa_status'=>''
                     );
          $record['userswallet'] = $this->supper_admin->call_procedure('proc_wallet',$parameter);
          $totfixwalletamt=0;
          foreach($record['userswallet'] as $value){ 
            $totfixwalletamt = number_format($totfixwalletamt,1,".","")+number_format($value->w_amount,1,".","");
          }
          
          if($totfixwalletamt<$this->input->post('userwallamt')){
              $this->session->set_flashdata("message", "wallet not updated because your amount is greater than existing Wallet amount.");
              redirect("admin/wallet/addwalletmoney");
          } else {
              $walletamt='-'.$this->input->post('userwallamt');
          }

      }

      $parameter=array('act_mode'=>'addwalletmoney',
                       'row_id'=>'',
                       'wa_orderid'=>$this->input->post('userwallordid'),
                       'wa_userid'=>$this->input->post('userwallid'),
                       'wa_type'=>$this->input->post('userwalltype'),
                       'wa_amt'=>$walletamt,
                       'wa_status'=>''
                       );
      $record['userswallet'] = $this->supper_admin->call_procedure('proc_wallet',$parameter);
      $this->session->set_flashdata("message", "Your information was successfully Saved");
      redirect("admin/wallet/addwalletmoney");
    }

    $parametermandet=array('act_mode'=>'allmanwallet','row_id'=>'','wa_orderid'=>'','wa_userid'=>'','wa_type'=>'','wa_amt'=>'','wa_status'=>'');
    $record['manufacwallet'] = $this->supper_admin->call_procedure('proc_wallet',$parametermandet);
    $this->load->view('helper/header');
    $this->load->view('adminwallet/addwallet',$record);
  }


  public function getmanorders(){
    $manid = $this->input->post('manid');
    $parameter=array('act_mode'=>'allmanwalletorder','row_id'=>$manid,'wa_orderid'=>'','wa_userid'=>'','wa_type'=>'','wa_amt'=>'','wa_status'=>'');
    $record = $this->supper_admin->call_procedure('proc_wallet',$parameter);
    $str = '';
    $str .= '<option value="">Select Order</option>';
    foreach($record as $k=>$v){
      $str .= "<option value=".$v->OrderId.">".$v->OrderNumber."</option>";
    }
      echo $str;
  } 

  public function ordrevisemail($ordid){ 

    $parameter        = array('act_mode'=>'dispatchdetail','row_id'=>'','orderstatus'=>'dispatch','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
    $responce['mailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
    //p($responce['mailview']); exit();

    $bizzgainemail="support@mindzshop.com";
    
    $emailid=$responce['mailview'][0]->Email;
    //$emailid='pawan@mindztechnology.com';
    $msg = $this->load->view('userordmail/orderrevisemail',$responce, true);
    $this->load->library('email');
    $this->email->from($bizzgainemail, 'MindzShop.com');
    $this->email->to($emailid); 
    //$this->email->cc($listbizzmail);
    $this->email->subject('Your order revised mail - ('.$responce['mailview'][0]->OrderNumber.')');
    $this->email->message($msg);
    
    if($this->email->send()){
      $successmsg="Your order revised mail for order number ".$responce['mailview'][0]->OrderNumber." has been send successfully.";
      $this->session->set_flashdata("message", $successmsg);
      redirect('admin/order/dispatchorder');
//echo "send";
    } else {
      $failmsg="Your order revised mail for order number ".$responce['mailview'][0]->OrderNumber." has not been send.";
      $this->session->set_flashdata("message", $failmsg);
      redirect('admin/order/dispatchorder');
//echo "Not send";
    }

    //$this->load->view('userordmail/orderrevisemail',$responce);

  }  

  public function ordrevisemobilemsg($ordid){ 
     $parameter        = array('act_mode'=>'dispatchdetail','row_id'=>'','orderstatus'=>'dispatch','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $msgdata = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
     //p($msgdata);exit;
//"Hi <name> your order <ORD1234> has been dispatched with revised order details. Call Us at +91 9711007307, if you need help."
     //-------------start SMS API--------------------
              $smsphnon = $msgdata->ContactNo; 
              $smsname = $msgdata->ShippingName;
              
              $smsmsg = "has been dispatched with revised order details. Kindly keep Cash / Cheque ready. For more details, Call/watsapp us at +91 9910079206.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$msgdata->OrderNumber.' '.$smsmsg);
              
              $file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------
      $successmsg="Your order revised Message for order number ".$msgdata->OrderNumber." has been send successfully.";
      $this->session->set_flashdata("message", $successmsg);
      redirect('admin/order/dispatchorder');

  }

public function add_first_purchase_offer()
{
  if($this->input->post('submit'))
  {
    if(!empty($this->input->post('userwallid')))
    {
      $walletamt=$this->input->post('userwallamt');
      $userid=$this->input->post('userwallid');
      
      for ($i=0; $i < count($userid); $i++) { 

        $parameter=array('act_mode'=>'addwalletmoneyfirst',
                       'row_id'=>'',
                       'wa_orderid'=>0,
                       'wa_userid'=>$userid[$i],
                       'wa_type'=>$this->input->post('userwalltype'),
                       'wa_amt'=>$walletamt,
                       'wa_status'=>$this->input->post('userwallordid')
                       );
        //p($parameter);
        $record['userswallet'] = $this->supper_admin->call_procedure('proc_wallet',$parameter);
      }
      
      
      $this->session->set_flashdata("message", "Your information was successfully Saved");
      redirect("admin/wallet/first_purchase_offer");
    }
    else
    {
      $this->session->set_flashdata("message", "Atleast One User Select!");
      redirect("admin/wallet/first_purchase_offer");
    }
  }

    $parametermandet=array('act_mode'=>'allmanwallet','row_id'=>'','wa_orderid'=>'','wa_userid'=>'','wa_type'=>'','wa_amt'=>'','wa_status'=>'');
    $record['manufacwallet'] = $this->supper_admin->call_procedure('proc_wallet',$parametermandet);
    $this->load->view('helper/header');
    $this->load->view('adminwallet/firstpurchaseoffer',$record);
  }  

  public function first_purchase_offer()
{
    $parametermandet=array('act_mode'=>'firstwalletlist','row_id'=>'','wa_orderid'=>'','wa_userid'=>'','wa_type'=>'','wa_amt'=>'','wa_status'=>'');
    $record['wallet'] = $this->supper_admin->call_procedure('proc_wallet',$parametermandet);
    $this->load->view('helper/header');
    $this->load->view('adminwallet/firstpurchaseofferlist',$record);
  }  

}//end class
?>