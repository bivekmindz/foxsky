<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Addproorder extends MX_Controller{
  public function __construct(){
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->helper('string');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->load->library('form_validation');
    $this->userfunction->loginAdminvalidation();

  }


public function add_multi_product_exist_order()
{ 

  $parameter=array('act_mode'=>'vendor_typ','','');
  $responce['vendor_type'] = $this->supper_admin->call_procedure('proc_report',$parameter);

  $parameter= array('act_mode'=>'get_pro_list','row_id'=>'','orderstatus'=>'pending','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['product_list'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
  $parameter = array('user_details','','','',$ordid,'','','');
  $responce['user_dtls'] = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);

  $parameter = array('addproorddet','','','','','','','');
  $responce['addproorddet_dtls'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  $this->load->view('helper/header');
  $this->load->view('order/add_multipro_ord',$responce);
}


public function get_order_row()
  { 
    //p($_POST);exit;
    $parameterordven = array('ordervendor_dtls',$_POST['pro_ordid'],'','','','','','');
    $responcevenid = $this->supper_admin->call_procedureRow('proc_orderflow',$parameterordven);

    $parameter = array('vendor_dtls',$responcevenid->UserId,'','','','','','');
    $responce = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    
    $parameter=array('act_mode' =>'citygroupid','row_id' =>$responce->city,'vusername' =>$responce->stateid,'','');
    $data=$this->supper_admin->call_procedureRow('proc_memberlogin',$parameter);

     if(empty($_POST['colr']))
    {
       $parameter = array('product_details',$_POST['productid'],'','','','','','');
       $pro_data = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    }else{
      $parameter = array('product_dtls',explode(',', $_POST['colr'])[1],'','','','','','');
      $pro_data = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    }
    
    $unit_pz=number_format(get_unit_price($pro_data->promapid,$_POST['qty'],$data->cmid),1,".","");
    
    $parameter = array('tax_details',$responce->stateid,'',$pro_data->catid,'','','','');
    $tax_data = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
    //pend($tax_data);
    foreach ($tax_data as $value) {
     if($value->taxidd==$responce->tinid)
     {
       $tin=$value->taxprice;
     }elseif ($value->taxidd==$responce->cstid) {
        $cst=$value->taxprice;
     }elseif ($value->taxidd==5) {
        $entry=$value->taxprice;
     }
    }
    if(empty($_POST['colr']))
    {
     $style="background-color:#";
     $clr="";
    }else{
      $clr=explode(',', $_POST['colr'])[0];
     if(explode(',', $_POST['colr'])[0]=='multicolor')
    { 
      $urlmulti=base_url().'images/imgpsh_fullsize.jpg';
      $style='background-image:url('.$urlmulti.')';

    }else{
      $style="background-color:#".explode(',', $_POST['colr'])[0];
    }

    }
    $tin_t=number_format((($unit_pz*$_POST['qty'])*$tin/100),1,".","");
    $cst_t=number_format((($unit_pz*$_POST['qty'])*$cst/100),1,".","");
    $enrty_t=number_format((($unit_pz*$_POST['qty'])*$entry/100),1,".","");
    $subtotal=number_format(($unit_pz*$_POST['qty']),1,".","");
    $shipping_charge=number_format(tshippingcharge($unit_pz*$_POST['qty']+$_POST['amount']),1,".","");
    $final_total=number_format(($unit_pz*$_POST['qty']),1,".","");
    if(!empty($_POST)){ 
      
      $str='<tr>';
      $str.='<td class="sno">'.($_POST['ttl_row']+1).'</td>';
      $str.='<td class="sku">'.$pro_data->skuno.'</td>';
      $str.='<td><img class="img" src="'.$_POST['pro_img'].'" style="vertical-align:middle; width:80px;"></td>';
      $str.='<td class="pro_name" pid="'.$pro_data->promapid.'" catid="'.$pro_data->catid.'">'.$_POST['proname'].'</td>';
      $str.='<td class="qty">'.$_POST['qty'].'</td>';
      $str.='<td class="age">'.$_POST['age'].'</td>';
      $str.='<td>';

      $str.='<font class="colr" clr="'.$clr.'" style="'.$style.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br></td>';
      $str.='<td class="tin" tx="'.$tin.'">'.number_format((($unit_pz*$_POST['qty'])*$tin/100),1,".","").'</td>';
      $str.='<td class="cst" tx="'.$cst.'">'.number_format((($unit_pz*$_POST['qty'])*$cst/100),1,".","").'</td>';
      $str.='<td class="entry" tx="'.$entry.'">'.number_format((($unit_pz*$_POST['qty'])*$entry/100),1,".","").'</td>';
      $str.='<td class="unit">'.$unit_pz.'</td>';
      $str.='<td class="total">'.number_format(($unit_pz*$_POST['qty']),1,".","").'</td>';
      $str.="<td onclick='delete_row($(this))' style='cursor: pointer;color: blue'>Delete</td>";
      $str.='</tr>';
    }
    echo $str.'**'.$tin_t.'**'.$cst_t.'**'.$enrty_t.'**'.$subtotal.'**'.$shipping_charge.'**'.$final_total;
    
  }


  public function add_existing_order()
{ 
    /*$parameter = array('vendor_dtls',$_POST['vendoid'],'','','','','','');
    $responce = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);

    $parameter=array('act_mode' =>'citygroupid','row_id' =>$responce->city,'vusername' =>$responce->stateid,'','');
    $data=$this->supper_admin->call_procedureRow('proc_memberlogin',$parameter);
    $grandtotalcoddiscount='';
    if($_POST['paymentmode']=='COD')
    {
      $grandtotalcoddiscount = ceil(($_POST['subtotal']*0)/100);
    }
   
   $param = array(
                        'UserId'           =>$_POST['vendoid'],
                        'userregid'        =>$_POST['vendoid'],
                        'UserAddressId'    =>$_POST['ship_id'], 
                        'orderNumber'      =>'',
                        'totalAmount'      =>$_POST['final_total'],
                        'paymentMod'       =>$_POST['paymentmode'],
                        'shipcharge'       =>$_POST['shipping_charge'],
                        'couponamt'        =>'',
                        'couponcode'       =>'',
                        'userIpaddress'    =>'', 
                        'p_device'         =>$data->cmid,
                        'usedwalletamount' =>'',
                        'p_usedrewardpoint'=>$grandtotalcoddiscount,
                        'p_subtotal'       =>$_POST['subtotal'],
                        'p_orderfrom'      =>'offline',
                        'totaltax'         =>$_POST['total_tax'],
                        'csttax'           =>$_POST['tcst_tax'],
                        'tintax'           =>$_POST['ttin_tax'],
                        'entrytax'         =>$_POST['tentry_tax'],
                        'u_remark'         =>$_POST['remark'] 
                );
     
     $order_rep = $this->supper_admin->call_procedureRow('proc_offlineordr',$param);
    if($_POST['paymentmode']=='CHEQUE' && !empty($order_rep))
    {
     $param = array(
                        'act_mode'          => 'insertcheq',
                        'row_id'            => '',
                        'orderid'           => $order_rep->OrderId,
                        'chequeno'          => $this->input->post('cheque_no'),
                        'ifsc'              => $this->input->post('ifsc_code'),
                        'bankname'          => $this->input->post('bank_name'),
                        'chequedate'        => $this->input->post('cheque_date'),
                        'accno'             => $this->input->post('acc_no'),
                        'chequecollectdate' => $this->input->post('chequecollect_date'),
                        'chequeamount'      => $this->input->post('cheque_amount'),
                        'paymentMod'        => $this->input->post('paymentmode'),
                        'orderno'           => $order_rep->OrderNumber 
                        );
     $data = $this->supper_admin->call_procedureRow('proc_cheque',$param);
    }
        
    if(!empty($order_rep))
    { */
      
      for($i=0;$i<count($_POST['pro_qty']);$i++){
       $parameter = array(
                            'orderId'        =>$_POST['pro_ordid'],
                            'proid'          =>$_POST['promap_id'][$i],
                            'qty'            =>$_POST['pro_qty'][$i],
                            'shippingcharge' =>0,
                            'proname'        =>$_POST['proname'][$i],
                            'vendorId'       =>get_product_details($_POST['promap_id'][$i])->manufactureid,
                            'prodSize'       =>$_POST['pro_age'][$i],
                            'prodPrice'      =>$_POST['sale_pz'][$i],
                            'coupon_value'   =>'',
                            'p_color'        =>$_POST['pro_color'][$i],
                            'p_img'          =>$_POST['image'][$i],
                            'cstvalue'       =>$_POST['cst_tax'][$i],
                            'tinvalue'       =>$_POST['tin_tax'][$i],
                            'entryvalue'     =>$_POST['entry_tax'][$i],
                            'cst_p'         =>$_POST['cst_taxp'][$i],
                            'tin_p'         =>$_POST['tin_taxp'][$i],
                            'entry_p'       =>$_POST['entry_taxp'][$i]
                          );

//p($parameter);
       $result = $this->supper_admin->call_procedureRow('proc_orderpromap', $parameter);              
      }
      
      //exit();
      
    //}
    if($result->success=='success')
    {  
// Start push notification for app 
      /*$addprocount=count($_POST['pro_qty']);
      $parameterapp = array('act_mode'=>'getappaddorderdeviceid','row_id'=>$_POST['pro_ordid'],'catid'=>'');
      $appdeviceids  = $this->supper_admin->call_procedureRow('proc_product',$parameterapp);
      $appdeviceiddata = array($appdeviceids->appdeviceid);
      $appmsgdevice=array('message'=>$addprocount.' New Products are Added in your existing order. OrderId is '.$appdeviceids->OrderNumber,'title'=>'Add Products');
      $appnotification='2';
      send_andriod_notification($appdeviceiddata,$appmsgdevice,$appnotification);*/
// End push notification for app 
      $this->session->set_flashdata("success", "Products added in your Order successfully.");
    }else{
      $this->session->set_flashdata("error", "Something Went Wrong.");
    } 
    
}

public function get_existing_orderdtls(){
  $parameter = array('getexistorddtls',$_POST['pro_ordid'],'','','','','','');
  $responce = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);

  echo $responce->companyname.'#$'.$responce->ShippingAddress;

}

}// class
?>