<?php
class Ckeditor extends MX_Controller {
	
	public $data 	= 	array();
	
	public function __construct() {
		$this->load->model("supper_admin");
       $this->load->helper('my_helper');
		//parent::Controller();

		$this->load->helper('url'); //You should autoload this one ;)
		$this->load->helper('ckeditor');
		
		//Ckeditor's configuration
		$this->data['ckeditor'] = array(
		
			//ID of the textarea that will be replaced
			'id' 	=> 	'content',
			'path'	=>	'js/ckeditor',
		
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Full", 	//Using the Full toolbar
				'width' 	=> 	"850px",	//Setting a custom width
				'height' 	=> 	'300px',	//Setting a custom height
					
			),
		
			//Replacing styles from the "Styles tool"
			'styles' => array(
			
				//Creating a new style named "style 1"
				'style 1' => array (
					'name' 		=> 	'Blue Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Blue',
						'font-weight' 		=> 	'bold'
					)
				),
				
				//Creating a new style named "style 2"
				'style 2' => array (
					'name' 		=> 	'Red Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 			=> 	'Red',
						'font-weight' 		=> 	'bold',
						'text-decoration'	=> 	'underline'
					)
				)				
			)
		);
		
		$this->data['ckeditor_2'] = array(
		
			//ID of the textarea that will be replaced
			'id' 	=> 	'content_2',
			'path'	=>	'js/ckeditor',
		
			//Optionnal values
			'config' => array(
				'width' 	=> 	"550px",	//Setting a custom width
				'height' 	=> 	'100px',	//Setting a custom height
				'toolbar' 	=> 	array(		//Setting a custom toolbar
					array('Bold', 'Italic'),
					array('Underline', 'Strike', 'FontSize'),
					array('Smiley'),
					'/'
				)
			),
		
			//Replacing styles from the "Styles tool"
			'styles' => array(
			
				//Creating a new style named "style 1"
				'style 3' => array (
					'name' 		=> 	'Green Title',
					'element' 	=> 	'h3',
					'styles' => array(
						'color' 			=> 	'Green',
						'font-weight' 		=> 	'bold'
					)
				)
								
			)
		);
	}
	
public function index($id) {



	 $this->userfunction->loginAdminvalidation();
	 $loginid=$this->session->userdata('bizzadmin')->LoginID;

	 if($id)
	 {
	 	//echo $id;
	 	 if($this->input->post('submit')){

      $this->form_validation->set_rules('con_description', 'Description', 'required|xss_clean');
       
        $con_title    		=$this->input->post('con_title');
        $con_description    =$this->input->post('con_description', TRUE);
        $metatitle    		=$this->input->post('metatitle');
        $metakeywords 	    =$this->input->post('metakeywords');
        $metadescription    =$this->input->post('metadescription');
        $urlslug   		    =$this->input->post('urlslug');
        
     
     $parameter=array(
                      'act_mode'=>'updatecms',
                      'row_id'=>$id,
                      'loginid'=>$loginid,
                      'con_title'=>$con_title,
                      'con_description'=>$con_description,
                      'metatitle'=>$metatitle,
                      'metakeywords'=>$metakeywords,
                      'metadescription'=>$metadescription,
                      'urlslug'=>$urlslug,
                      'status'=>'',
                      'createdon'=>'',
                      'modifiedon'=>''
                      );
   
    //p($parameter);exit;
     $record =$this->supper_admin->call_procedure('proc_cms',$parameter);
     $this->session->set_flashdata('message', 'Your information was successfully Updated.');
     redirect('admin/ckeditor/cmsview');
 }
	 }
	 else
	 {
    

    if($this->input->post('submit')){
        
      $this->form_validation->set_rules('con_description', 'Description', 'required|xss_clean');
       
        $con_title   		 =$this->input->post('con_title');
        $con_description   	 =$this->input->post('con_description', TRUE);
        $metatitle   		 =$this->input->post('metatitle');
        $metakeywords    	 =$this->input->post('metakeywords');
        $metadescription     =$this->input->post('metadescription');
        $urlslug             =$this->input->post('urlslug');
        
     
     $parameter=array(
                      'act_mode'=>'insertcms',
                      'row_id'=>$id,
                      'loginid'=>$loginid,
                      'con_title'=>$con_title,
                      'con_description'=>$con_description,
                      'metatitle'=>$metatitle,
                      'metakeywords'=>$metakeywords,
                      'metadescription'=>$metadescription,
                      'urlslug'=>$urlslug,
                      'status'=>'',
                      'createdon'=>'',
                      'modifiedon'=>''
                      );
   
    
     $record =$this->supper_admin->call_procedure('proc_cms',$parameter);
     $this->session->set_flashdata('message', 'Your information was successfully Saved.');
     redirect('admin/ckeditor/index');
    
    }  

    }   

 //echo $id;
    $parameter=array('act_mode'=>'singleview','row_id'=>$id,'uid'=>'','con_title'=>'','con_description'=>'','metatitle'=>'','metakeywords'=>'','metadescription'=>'','urlslug'=>'','status'=>'','createdon'=>'','modifiedon'=>'');

   $this->data['cmsid'] =$this->supper_admin->call_procedureRow('proc_cms',$parameter);
   //p($this->data['cmsid']);exit();
 

$parameter=array('act_mode'=>'contentview','row_id'=>'','uid'=>'','con_title'=>'','con_description'=>'','metatitle'=>'','metakeywords'=>'','metadescription'=>'','urlslug'=>'','status'=>'','createdon'=>'','modifiedon'=>'');
    $this->data['menuview'] =$this->supper_admin->call_procedure('proc_cms',$parameter);

    $parameter=array('act_mode'=>'updatecontentview','row_id'=>'','uid'=>'','con_title'=>'','con_description'=>'','metatitle'=>'','metakeywords'=>'','metadescription'=>'','urlslug'=>'','status'=>'','createdon'=>'','modifiedon'=>'');
    $this->data['updatemenuview'] =$this->supper_admin->call_procedure('proc_cms',$parameter);


//p($record['menuview']);exit();
		$this->load->view('helper/header');
		$this->load->view('contentsection/addcontent',$this->data);
    	//$this->load->view('helper/footer'); 
		
	}


public function cmsview(){
    $this->userfunction->loginAdminvalidation();
    $parameter=array(
                      'act_mode'=>'cmsview',
                      'row_id'=>'',
                      'uid'=>'',
                      'pid'=>'',
                      'con_description'=>'',
                      'metatitle'=>'',
                      'metakeywords'=>'',
                      'metadescription'=>'',
                      'urlslug'=>'',
                      'status'=>'',
                      'createdon'=>'',
                      'modifiedon'=>''
                      
            );
    
     $record['cms'] =$this->supper_admin->call_procedure('proc_cms',$parameter);
     //p($record['cms']);exit;
     $this->load->view('helper/header');
     $this->load->view('contentsection/viewcmslist',$record);
     //$this->load->view('helper/footer');

  }

  public function statuscms(){
  $id            = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='1'?'cmsactiveandinactive':'cmsinactiveandinactive';
  $parameter=array(
                      'act_mode'=>$act_mode,
                      'row_id'=>$id,
                      'uid'=>'',
                      'con_title'=>'',
                      'con_description'=>'',
                      'metatitle'=>'',
                      'metakeywords'=>'',
                      'metadescription'=>'',
                      'urlslug'=>'',
                      'status'=>'',
                      'createdon'=>'',
                      'modifiedon'=>''
                      );
  $record =$this->supper_admin->call_procedure('proc_cms',$parameter);
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/ckeditor/cmsview');
  }

}

?>