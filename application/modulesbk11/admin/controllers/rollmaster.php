<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Rollmaster extends MX_Controller{
  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }


  public function rolelist(){
  
   $parameter = array(
          'roles' => '',
          'userid' => '',
          'menuid' => '',
          'submenuid' => '',
          'act_mode' => 'view_role_list',
          'start'=>'',
          'total_rows'=>''
          );    


     $response['rolelist'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
     //p($response['rolelist']); exit;
     $this->load->view('helper/header'); 
     $this->load->view('rollmanagement/roll_list_view',$response); 
      
  }

  public function delrole($id)
  { 
    $userid = $this->session->userdata('bizzadmin')->LoginID; 
    $parameter = array(
          'roles' => '',
          'userid' => $id,
          'menuid' => '',
          'submenuid' => '',
          'act_mode' => 'del_role_list',
          'start'=>'',
          'total_rows'=>$userid
          );    


    $roledel = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
    $this->session->set_flashdata('message', 'Your Information successfully deleted!');
    redirect('admin/rollmaster/rolelist');  
  }

  public function rolestatus(){
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='A'?'role_inactive':'role_active';
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $parameter = array(
            'roles' => $status,
            'userid' => $rowid,
            'menuid' => '',
            'submenuid' => '',
            'act_mode' => $act_mode,
            'start'=>'',
            'total_rows'=>$userid
            );    


    $rolestatus = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
    $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
    redirect('admin/rollmaster/rolelist');
  }

  public function employeelist(){
      $parameter = array(
          'roles' => '',
          'userid' => '',
          'menuid' => '',
          'submenuid' => '',
          'act_mode' => 'view_employee_list',
          'start'=>'',
          'total_rows'=>''
          );    

     $data['list'] = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
     $this->load->view('helper/header');
     $this->load->view('rollmanagement/employeelist',$data);
  }

  public function delemployee($id)
  { 
    $userid = $this->session->userdata('bizzadmin')->LoginID; 
    $parameter = array(
          'roles' => '',
          'userid' => $id,
          'menuid' => '',
          'submenuid' => '',
          'act_mode' => 'del_employee_list',
          'start'=>'',
          'total_rows'=>$userid
          );    

     $empdel = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
     $this->session->set_flashdata('message', 'Your Information successfully deleted!');
     redirect('admin/rollmaster/employeelist');  
  }

  public function employeestatus(){
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='Active'?'employee_inactive':'employee_active';
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $parameter = array(
          'roles' => '',
          'userid' => $rowid,
          'menuid' => '',
          'submenuid' => '',
          'act_mode' => $act_mode,
          'start'=>'',
          'total_rows'=>$userid
          );    

     $empstatus = $this->supper_admin->call_procedure('proc_assignrole', $parameter);
     $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
     redirect('admin/rollmaster/employeelist');
  }

  public function assignrole(){
    $parameter2=array(
      'id' => '',
      'act_mode' => 'mainmenu'
      );
       $data['menuaccess'] = $this->supper_admin->call_procedure('proc_SubMenu', $parameter2);   
      
      if ($this->input->post('addUserMenu')){    
      //p($_POST); exit;  
        $roles      = $this->input->post('role');
        $userid    = $this->input->post('user');
        $erole_userid = $this->session->userdata('bizzadmin')->LoginID;

        if($this->input->post('div2')!=""){
         
          foreach($this->input->post('div2') as $val){
            $getIds = explode("-",$val);
            $parameterch = array('act_mode'=>'check_assignrole',
                               'Param1'=>$roles,
                               'Param2'=>$userid,
                               'Param3'=>$getIds[0],
                               'Param4'=> $getIds[1],
                               'Param5'=>'',
                               'Param6'=>'',
                               'Param7'=>'',
                               'Param8'=>'',
                               'Param9'=>'',
                               'Param10'=>''
                                );
            $check_repeat=$this->supper_admin->call_procedureRow('proc_rolluser', $parameterch);

            if($check_repeat->count_assign_id==0){
              $parameter = array('act_mode'=>'insert',
                                 'Param1'=>$roles,
                                 'Param2'=>$userid,
                                 'Param3'=>$getIds[0],
                                 'Param4'=> $getIds[1],
                                 'Param5'=>'',
                                 'Param6'=>'',
                                 'Param7'=>'',
                                 'Param8'=>'',
                                 'Param9'=>'',
                                 'Param10'=>$erole_userid
                                  );
              $this->supper_admin->call_procedureRow('proc_rolluser', $parameter); 
            }  
          }
          $this->session->set_flashdata('message', 'Your Information was successfully Inserted.');
          redirect('admin/rollmaster/employeelist');
        } 
      }
      $parameter =array(
       'act_mode'=>'fetchemployeelist',
       'Param1'=>'',
       'Param2'=>'',
       'Param3'=>'',
       'Param4'=>'',
       'Param5'=>'',
       'Param6'=>'',
       'Param7'=>'',
       'Param8'=>'',
       'Param9'=>'',
       'Param10'=>''


        );          
    $data['employeelist']=$this->supper_admin->call_procedure('proc_rolluser',$parameter); 
   // p( $data['employeelist']); exit;

    $parameter1=array(
       'act_mode'=>'get_role',
       'Param1'=>'',
       'Param2'=>'',
       'Param3'=>'',
       'Param4'=>'',
       'Param5'=>'',
       'Param6'=>'',
       'Param7'=>'',
       'Param8'=>'',
       'Param9'=>'',
       'Param10'=>''


      );
    $data['rolename']=$this->supper_admin->call_procedure('proc_rolluser',$parameter1);      
    $this->load->view('helper/header');
    $this->load->view('rollmanagement/assignroles',$data);
  }

  public function getsubmenss()
  {
    $parameter2 = array('Id' => $_POST['menuid'],'act_mode' => 'submanu');
    $data['result'] = $this->supper_admin->call_procedure('proc_SubMenu' ,$parameter2);
    $res_opt = "";
    foreach($data['result'] as $k=>$v){
      $res_opt .= "<option value='".$_POST['menuid'].'-'.$v->id."'>".$v->menuname."</option>";
    }
    p($res_opt);   
  }

  public function updateassignrole($rid){
    $parameter2=array(
      'id' => '',
      'act_mode' => 'mainmenu'
      );
       $data['menuaccess'] = $this->supper_admin->call_procedure('proc_SubMenu', $parameter2); 

       $parameters =array(
       'act_mode'=>'getemproledetail',
       'Param1'=>$rid,
       'Param2'=>'',
       'Param3'=>'',
       'Param4'=>'',
       'Param5'=>'',
       'Param6'=>'',
       'Param7'=>'',
       'Param8'=>'',
       'Param9'=>'',
       'Param10'=>''


        );          
    $data['emproledetail']=$this->supper_admin->call_procedure('proc_rolluser',$parameters);   
      
   if ($this->input->post('addUserMenu')){  
   //p($_POST);exit;    
     
        $roles      = $data['emproledetail'][0]->role_id;
        $userid    = $rid;
        $erole_userid = $this->session->userdata('bizzadmin')->LoginID;
         $parameter12=array(
                           'act_mode'=>'delemproledetail',
                           'Param1'=>$userid,
                           'Param2'=>'',
                           'Param3'=>'',
                           'Param4'=>'',
                           'Param5'=>'',
                           'Param6'=>'',
                           'Param7'=>'',
                           'Param8'=>'',
                           'Param9'=>'',
                           'Param10'=>''
                          );
         $data['deleterole']=$this->supper_admin->call_procedure('proc_rolluser',$parameter12);

         if($this->input->post('div2')!=""){
         
          foreach($this->input->post('div2') as $val){
           $getIds = explode("-",$val);
         $parameter = array(

             'act_mode'=>'insert',
                   'Param1'=>$roles,
                   'Param2'=>$userid,
                   'Param3'=>$getIds[0],
                   'Param4'=> $getIds[1],
                   'Param5'=>'',
                   'Param6'=>'',
                   'Param7'=>'',
                   'Param8'=>'',
                   'Param9'=>'',
                   'Param10'=>$erole_userid
                    );
         //p($parameter); exit;
         $this->supper_admin->call_procedureRow('proc_rolluser', $parameter); 
                          
     }
    } 
      $this->session->set_flashdata('message', 'Your Information was successfully Updated.');
      redirect('admin/rollmaster/employeelist');
    }
    
     $this->load->view('helper/header');
     $this->load->view('rollmanagement/editroles',$data);
  
  }

}// end class
?>