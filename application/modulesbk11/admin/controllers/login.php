<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    
  }
  
//............. DEFAULT FUNCTION ............... //
  public function index(){
    $result = null;
    if($this->input->post('submit')){
      $this->form_validation->set_rules('email', 'Email','trim|required|xss_clean|valid_email');
      $this->form_validation->set_rules('password', 'Password','trim|required|max_length[20]|xss_clean');
      if($this->form_validation->run() == true){
       $parameter = array(
                    'useremail'    => $this->input->post('email'),
                    'userpassword' => md5($this->input->post('password')),
                    'act_mode'     => 'login',
                    'row_id'       => '');

       $data['result'] = $this->supper_admin->call_procedureRow('proc_Adminlogin', $parameter);
        //p($data['result']); exit;
       if(isset($data['result']->LoginID) && $data['result']->LoginID >0){
          if($data['result']->EmployeeId==9988){
            $this->session->set_userdata('bizzadmin',$data['result']); 
            #p($this->session->all_userdata()); exit;
            redirect('admin/login/dashboard');
          } else {
            $this->session->set_userdata('bizzadmin',$data['result']); 
            #p($this->session->all_userdata()); exit;
            redirect('admin/login/emp_dashboard');
          }
			  }
        else
          $result['result']='Invalid Login !!';
      }
	  }
    $this->load->view("user/login",$result);
  }

//.............  User Logout ............... //  
  public function logout(){
    //$this->session->sess_destroy();
    $this->session->unset_userdata('bizzadmin');
    redirect('admin/login');
  }

//.............  User Dashboard ............... //  
   public function dashboard(){
    		#p($this->session->all_userdata()); exit;
		    $this->userfunction->loginAdminvalidation();

        //-- Collect data from tables
        $params = array("act_mode" => "total_order_count");
        $data['result']['orderCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_orders");
        $data['result']['ordersTotal'] = $this->supper_admin->call_procedure('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_order_cod");
        $data['result']['orderCountCod'] = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_order_cheque");
        $data['result']['orderCountCheque'] = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_order_online");
        $data['result']['orderCountOnline'] = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_order_credit");
        $data['result']['orderCountCredit'] = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "manu_count");
        $data['result']['manuCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "manu_list");
        $data['result']['manuTotal'] = $this->supper_admin->call_procedure('proc_dashboard_data',$params);

        $params = array("act_mode" => "retail_count");
        $data['result']['retailCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "consumer_count");
        $data['result']['consumerCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "brand_count");
        $data['result']['brandCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "order_ofmonth");
        $data['result']['orderofmonth']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_ofmonth");
        $data['result']['totalofmonth']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "pending_order");
        $data['result']['pendingorder']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "dispatch_order");
        $data['result']['dispatchorder']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

          $params = array("act_mode" => "product_live");
        $data['result']['productlive']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);
     
       $params = array("act_mode" => "brnad_live");
        $data['result']['brnadlive']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);
     

        $params = array("act_mode" => "pric_ofday");
        $data['result']['pricofday']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);


        $params = array("act_mode" => "pending_paymentof");
        $data['result']['pendingpaymentss']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        #p($data['result']);

        $this->load->view('helper/header'); 
        $this->load->view('user/dashboard',$data);
	# $this->load->view('helper/footer');
    
    }

    public function emp_dashboard(){

      $this->userfunction->loginAdminvalidation();

        //-- Collect data from tables
        $params = array("act_mode" => "total_order_count");
        $data['result']['orderCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_orders");
        $data['result']['ordersTotal'] = $this->supper_admin->call_procedure('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_order_cod");
        $data['result']['orderCountCod'] = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_order_cheque");
        $data['result']['orderCountCheque'] = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_order_online");
        $data['result']['orderCountOnline'] = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_order_credit");
        $data['result']['orderCountCredit'] = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "manu_count");
        $data['result']['manuCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "manu_list");
        $data['result']['manuTotal'] = $this->supper_admin->call_procedure('proc_dashboard_data',$params);

        $params = array("act_mode" => "retail_count");
        $data['result']['retailCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "consumer_count");
        $data['result']['consumerCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "brand_count");
        $data['result']['brandCount']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "order_ofmonth");
        $data['result']['orderofmonth']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "total_ofmonth");
        $data['result']['totalofmonth']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "pending_order");
        $data['result']['pendingorder']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

        $params = array("act_mode" => "dispatch_order");
        $data['result']['dispatchorder']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

          $params = array("act_mode" => "product_live");
        $data['result']['productlive']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);
     
       $params = array("act_mode" => "brnad_live");
        $data['result']['brnadlive']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);
     

        $params = array("act_mode" => "pric_ofday");
        $data['result']['pricofday']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);


        $params = array("act_mode" => "pending_paymentof");
        $data['result']['pendingpaymentss']  = $this->supper_admin->call_procedureRow('proc_dashboard_data',$params);

      $this->load->view('helper/header'); 
      $this->load->view('user/emp_dashboard',$data);
    }
   
//.............  Website Configuration ............... //  
   public function siteconfig(){

      $this->userfunction->loginAdminvalidation();
      $parameter         = array('act_mode'=>'viewsiteconfig','row_id'=>'');
      $data['viewsdata'] = $this->supper_admin->call_procedure('proc_siteconfig', $parameter); 
      $this->load->view('helper/header');
      $this->load->view('user/siteconfig',$data);
      $this->load->view('helper/footer');
    
  }

//.............  Update Website Configuration ............... //  
  public function updatesiteconfig(){

      $this->userfunction->loginAdminvalidation();
      $rowid = $this->uri->segment(4);
      //p($_POST);exit();

      if($_POST){
        $sitevalue   = $this->input->post('sitevalue');
        $sitevalueid = $this->input->post('sitevalueid');
        $parameter1  = array('act_mode'=>'updateconfigdata','row_id'=>$sitevalueid,'qty'=>'','pay_status'=>$sitevalue);
        $response['updatesite'] = $this->supper_admin->call_procedure('proc_orderflow', $parameter1);
        $this->session->set_flashdata('message', 'Data Update Successfully');
        redirect('admin/login/siteconfig');

      }//end if.

      $parameter         = array('act_mode'=>'viewupdateconfig','row_id'=>$rowid);
      $data['viewsdata'] = $this->supper_admin->call_procedureRow('proc_siteconfig', $parameter); 
      $this->load->view('helper/header');
      $this->load->view('user/updatesiteconfig',$data);
      $this->load->view('helper/footer');
    
  }

//.............  Unique Admin Password ............... //  
  public function adminUniquepassword() {

       $this->userfunction->loginAdminvalidation();
       $allsession    = $this->session->userdata('bizzadmin');
       $userid        = $allsession->LoginID;
       $useremail     = $allsession->UserName;
       
       $valuepassword = $_POST['oldpasswod'];

       $parameter = array(
                     'act_mode'    => 'checkpassword',
                     'n_email'     => $useremail,
                     'oldpassword' => md5($valuepassword),
                     'row_id'      => $userid,
                     'newpassword' => ''
                    );
        
        $result['value'] = $this->supper_admin->supper_admin->call_procedureRow('proc_adminpasswordcheck', $parameter);
        //p($result['value']); exit;
        echo json_encode($result['value']);
  }

//.............  Update Admin Password ............... //  
  public function adminupdatepassword(){
    $this->userfunction->loginAdminvalidation();
    $this->load->view('helper/header');

    //p($this->session->all_userdata()); exit;
    $allsession   = $this->session->userdata('bizzadmin');
    $userid       = $allsession->LoginID;
    $useremail    = $allsession->UserName;
    
   // echo $userid."/ ".$useremail; exit;

    if ($this->input->post('submit')) {
        //p($_POST); exit;
        $valuepassword        = $this->input->post('old_password');
        $confirmvaluepassword = $this->input->post('confirm_password');
        
        $parameter            = array(
                                  'act_mode'    => 'updatepassword',
                                  'n_email'     => $useremail,
                                  'oldpassword' => md5($confirmvaluepassword),
                                  'row_id'      => $userid,
                                  'newpassword' => base64_encode($confirmvaluepassword)
                                );

        //p($parameter); exit;

        $data = $this->supper_admin->supper_admin->call_procedureRow('proc_adminpasswordcheck', $parameter);
           
        if ($data->adminemailcount == 0) {
            $this->session->set_flashdata('message', 'Password Update Successfully!');
            redirect('admin/login/adminupdatepassword');
        }
      }  
     $this->load->view('adminupdatepassword');
     //$this->load->view('helper/footer');
  }
public function dashboardview()
    {

       $actmode =   $this->uri->segment(4); 
        


         $parameter = array('act_mode'=>$actmode,'row_id'  => '');

         $data['orderofday'] = $this->supper_admin->supper_admin->call_procedure('proc_dashboardview', $parameter);

         

        //p($data['orderofday']);
        $this->load->view('helper/header');
         $this->load->view('order/dashboard_view',$data);
    }

public function permission_error(){
  $this->load->view('check_permission');
}    

}//end class
?>