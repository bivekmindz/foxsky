<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Store extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('upload');
    $this->userfunction->loginAdminvalidation();
    
  }
  
  public function addstore(){

    if($this->input->post('submit')){
        
        $num = rand(1111, 9999);
        $userid = $this->session->userdata('bizzadmin')->LoginID;
        $param=array('act_mode' => 'add_store',
                     'param2' => $this->input->post('sto_vendor'),
                     'param3' => 'STOR'.$num,
                     'param4' => $this->input->post('sto_name'),
                     'param5' => $this->input->post('sto_address'),
                     'param6' => $this->input->post('sto_country'),
                     'param7' => $this->input->post('sto_state'),
                     'param8' => $this->input->post('sto_city'),
                     'param9' => $this->input->post('sto_email'),
                     'param10' => $this->input->post('sto_phone'),
                     'param11' => $this->input->post('sto_dopen'),
                     'param12' => '',
                     'param13' => $userid,
                     'param14' => '');
        $storedata = $this->supper_admin->call_procedureRow('proc_store',$param);

        foreach ($this->input->post('stp_person') as $key => $value) {
            $paramsp=array('act_mode' => 'add_store_contacts',
                     'param2' => $storedata->lastid,
                     'param3' => $value,
                     'param4' => $this->input->post('stp_person_mobile')[$key],
                     'param5' => $this->input->post('stp_person_email')[$key],
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '',
                     'param11' => '',
                     'param12' => '',
                     'param13' => $userid,
                     'param14' => '');
            $storeperson = $this->supper_admin->call_procedureRow('proc_store',$paramsp);
        }

        $this->session->set_flashdata('message', 'Store Added Successfully.');
        redirect('admin/store/addstore');
    }

    $parameter = array('vendor_list','','manufacturer','','','','','');
    $record['vendor_list'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
    
    $parameter               = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $record['viewcountry']   = $this->supper_admin->call_procedure('proc_geographic',$parameter);

    $this->load->view('helper/header');
    $this->load->view('store/addstore',$record);
  }

  public function storelist(){
      $userid = $this->session->userdata('bizzadmin')->LoginID;

    if($this->input->post('submit')){
     foreach ($this->input->post( 'storedelete') as $key => $value) {
        $paramdel=array('act_mode' => 'delete_store',
                       'param2' => $value,
                       'param3' => '',
                       'param4' => '',
                       'param5' => '',
                       'param6' => '',
                       'param7' => '',
                       'param8' => '',
                       'param9' => '',
                       'param10' => '',
                       'param11' => '',
                       'param12' => '',
                       'param13' => $userid,
                       'param14' => '');
        $del_store = $this->supper_admin->call_procedure('proc_store',$paramdel);
     }
     $this->session->set_flashdata("message", "Information was successfully deleted.");
     redirect("admin/store/storelist");
    }

    $paramsp=array('act_mode' => 'get_store_list',
                   'param2' => '',
                   'param3' => '',
                   'param4' => '',
                   'param5' => '',
                   'param6' => '',
                   'param7' => '',
                   'param8' => '',
                   'param9' => '',
                   'param10' => '',
                   'param11' => '',
                   'param12' => '',
                   'param13' => '',
                   'param14' => '');
    $allstore_data = $this->supper_admin->call_procedure('proc_store',$paramsp);
    //p($record['store_detail']);exit;

    //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/store/storelist?";
     $config['total_rows']       = count($allstore_data);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $record["links"]  = explode('&nbsp;',$str_links );
     $paramsps=array('act_mode' => 'get_store_list',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => $page,
                    'param14' => $second);
     $record['store_data'] = $this->supper_admin->call_procedure('proc_store',$paramsps);

    $this->load->view('helper/header');
    $this->load->view('store/storelist',$record); 
  }

  public function deletestore($id){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $param=array('act_mode' => 'delete_store',
                   'param2' => $id,
                   'param3' => '',
                   'param4' => '',
                   'param5' => '',
                   'param6' => '',
                   'param7' => '',
                   'param8' => '',
                   'param9' => '',
                   'param10' => '',
                   'param11' => '',
                   'param12' => '',
                   'param13' => $userid,
                   'param14' => '');
    $del_store = $this->supper_admin->call_procedure('proc_store',$param);
    $this->session->set_flashdata("message", "Information was successfully deleted.");
    redirect("admin/store/storelist");
  }

  public function statusstore(){

    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $ch_status     = $status==0?1:0;
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $param = array('act_mode' => 'status_store',
                   'param2' => $rowid,
                   'param3' => $ch_status,
                   'param4' => '',
                   'param5' => '',
                   'param6' => '',
                   'param7' => '',
                   'param8' => '',
                   'param9' => '',
                   'param10' => '',
                   'param11' => '',
                   'param12' => '',
                   'param13' => $userid,
                   'param14' => '');
    $st_store = $this->supper_admin->call_procedureRow('proc_store',$param);
    $this->session->set_flashdata("message", "Status was successfully updated.");
    redirect("admin/store/storelist");
  }

  public function updatestore($id){

    $param = array('act_mode' => 'get_store_detail',
                   'param2' => $id,
                   'param3' => '',
                   'param4' => '',
                   'param5' => '',
                   'param6' => '',
                   'param7' => '',
                   'param8' => '',
                   'param9' => '',
                   'param10' => '',
                   'param11' => '',
                   'param12' => '',
                   'param13' => '',
                   'param14' => '');
    $record['store_detail'] = $this->supper_admin->call_procedureRow('proc_store',$param);
    //p($record['store_detail']);exit();

    $paramsp=array('act_mode' => 'get_store_contacts',
                   'param2' => $id,
                   'param3' => '',
                   'param4' => '',
                   'param5' => '',
                   'param6' => '',
                   'param7' => '',
                   'param8' => '',
                   'param9' => '',
                   'param10' => '',
                   'param11' => '',
                   'param12' => '',
                   'param13' => '',
                   'param14' => '');
    $record['store_contact'] = $this->supper_admin->call_procedure('proc_store',$paramsp);

    $parameter = array('vendor_list','','retail','','','','','');
    $record['vendor_list'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
    
    $parameter               = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $record['viewcountry']   = $this->supper_admin->call_procedure('proc_geographic',$parameter);

    if($this->input->post('submit')){
        $userid = $this->session->userdata('bizzadmin')->LoginID;
        $paramspup=array('act_mode' => 'update_store',
                         'param2' => $id,
                         'param3' => $this->input->post('sto_vendor'),
                         'param4' => $this->input->post('sto_name'),
                         'param5' => $this->input->post('sto_address'),
                         'param6' => $this->input->post('sto_country'),
                         'param7' => $this->input->post('sto_state'),
                         'param8' => $this->input->post('sto_city'),
                         'param9' => $this->input->post('sto_email'),
                         'param10' => $this->input->post('sto_phone'),
                         'param11' => $this->input->post('sto_dopen'),
                         'param12' => '',
                         'param13' => $userid,
                         'param14' => '');
        $store_contact_update = $this->supper_admin->call_procedure('proc_store',$paramspup);

        foreach ($record['store_contact'] as $key => $value) {
            $paramspup=array('act_mode' => 'update_store_contacts',
                           'param2' => $value->stm_id,
                           'param3' => $this->input->post('stp_person'.$key),
                           'param4' => $this->input->post('stp_person_mobile'.$key),
                           'param5' => $this->input->post('stp_person_email'.$key),
                           'param6' => '',
                           'param7' => '',
                           'param8' => '',
                           'param9' => '',
                           'param10' => '',
                           'param11' => '',
                           'param12' => '',
                           'param13' => $userid,
                           'param14' => '');
            $store_contact_update = $this->supper_admin->call_procedure('proc_store',$paramspup);
        }

        $this->session->set_flashdata("message", "Information was successfully updated.");
        redirect("admin/store/storelist");
    }

    $this->load->view('helper/header');
    $this->load->view('store/updatestore',$record);
  }
  
}//end class
?>