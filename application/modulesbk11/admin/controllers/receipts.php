<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Receipts extends MX_Controller {

  public function __construct() {    
      $this->load->model("supper_admin");
      $this->load->helper('my_helper');
      $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
      $this->userfunction->loginAdminvalidation();    
  } 

  public function index() {

    if($this->uri->segment(3)=='')
    {
      $param1='';
      $param3='';
      $storeall='1';
    }
    else
    {
      if($this->uri->segment(4)=='Pending')
      {
          $param1='';
          $param3='Pending';
          $storeall='';
      }
      else
      {
          $param1=$this->uri->segment(4);
          $param3='';
          $storeall='';
      }
    }
    if($this->uri->segment(4)=='Approved_by_MPC')
      {
        
          $param1='';
          $param3='Approved by MPC';
          $storeall='';
      } 

      if($this->uri->segment(4)=='autoapproved')
      {
        
          $param1='';
          $param3='autoapproved';
          $storeall='';
      }

      if($this->uri->segment(4)=='')
      {
        $url='admin/receipts?';
      }
      else
      {
        $url='admin/receipts/index/'.$this->uri->segment(4).'?';
      }

       if(!empty($this->uri->segment(5)))
       $datasearch=$this->uri->segment(5);
       else
      $datasearch='';
     /// all 
      if($this->input->get('date')!='')
      {
        $datasearch=$this->input->get('date');
      }

      if($this->session->userdata('popcoin_login')->s_usertype==1 || $this->session->userdata('popcoin_login')->s_usertype==10 || $this->session->userdata('popcoin_login')->s_usertype==9 || $this->session->userdata('popcoin_login')->s_usertype==8) {
       //redirect(base_url().'admin/login');
      
      
       $parameter=array( 'act_mode' => 'receipts',
                  'row_id'    => '',
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '1',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
       //p($parameter); exit;
      $response['view1'] = $this->supper_admin->call_procedure('proc_other', $parameter);
      //p($response['view1']); exit;

      if($this->input->post('newsexcel'))
        {
           $finalExcelArr = array('Bill Uid','Store Id','Store Name','Location','User Id','User Name','Date and time of upload','Date of Purchase','Date and time of bill sent to retailers',
            'Bill no','Bill Amount','Paid By cash/card','Paid by Popcoins','cashback %','cashback amount','status');
            $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Complete List of Reciept');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
           $objPHPExcel->getActiveSheet()->freezePane('A2');
           $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
           for($i=0;$i<count($finalExcelArr);$i++)
           {
 $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
 $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );
 $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
 foreach ($response['view1'] as $key => $value) {

  $newvar = $j+$key;

   $up=number_format(($value->bill_update_amt*$value->mpc_margin)/100,2);

    $a=100-$tocom->tcom;
   $b=($a*$value->mpc_margin)/100;
      $cash=$value->mpc_margin;

       if($this->session->userdata('popcoin_login')->s_usertype==2 || $this->session->userdata('popcoin_login')->s_usertype==4)
         { $amt=$value->bill_update_amt; }
        else
         {$amt=$value->bill_amt; }


       if($value->sent_to_ret_date=='0000-00-00 00:00:00') 
                       { 
          $d2=date("d-m-Y g:i:s A", strtotime($value->re_modified));
                          }
            else
             {
        $d2= date("d-m-Y g:i:s A", strtotime($value->sent_to_ret_date));
        }

        //$d=date("d-m-Y ", strtotime($value->re_createdon);
           // $d1=date("d-m-Y", strtotime($value->date_of_purchase); 

        $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);            
    $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar,$value->bill_uid);
     $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->s_storeunid);
     $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->s_name);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->location);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->user_uid);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->t_FName);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar,date("d-m-Y g:i:s A", strtotime($value->re_createdon)));
             $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar,date("d-m-Y", strtotime($value->date_of_purchase)));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $d2);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->bill_no);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $amt);
  
              $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->bill_update_amt);
               $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->popcoins_amt);
               $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $cash. ' %');
               $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $up);
               $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->re_status);
             

             

          
         
 }

  $filename='Receiptlist.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
   }//for end
       
    }//if end


      $config['base_url']         = base_url($url);
      $config['total_rows']       = count($response['view1']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

      $parameter=array( 'act_mode' => 'receipts',
                  'row_id'    => '',
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '0',
                  'Param10'   => $page,
                  'Param11'    => $second,
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
      //p($response['view']); exit;

$vpage="receipts";

       } 
       else if($this->session->userdata('popcoin_login')->s_usertype==10) {

        $parameter=array( 'act_mode' => 'receipts',//'receipts_LM', 23 sep
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '1',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => ''); 
       // p($parameter); exit; 
      $response['view1'] = $this->supper_admin->call_procedure('proc_other', $parameter);
      //p($response['view']); exit; 

      $config['base_url']         = base_url($url);
      $config['total_rows']       = count($response['view1']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

      $parameter=array( 'act_mode' => 'receipts',
                  'row_id'    => '',
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '0',
                  'Param10'   => $page,
                  'Param11'    => $second,
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
$vpage="receipts";
       }

       else if($this->session->userdata('popcoin_login')->s_usertype==9) {

        $parameter=array( 'act_mode' => 'receipts',//receipts_TM',23 sep
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '1',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view1'] = $this->supper_admin->call_procedure('proc_other', $parameter);

      $config['base_url']         = base_url($url);
      $config['total_rows']       = count($response['view1']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

      $parameter=array( 'act_mode' => 'receipts',
                  'row_id'    => '',
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '0',
                  'Param10'   => $page,
                  'Param11'    => $second,
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
$vpage="receipts";
       }
       else if($this->session->userdata('popcoin_login')->s_usertype==8) {

        $parameter=array( 'act_mode' => 'receipts',//'receipts_LH', 23 sep
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '1',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view1'] = $this->supper_admin->call_procedure('proc_other', $parameter);

      $config['base_url']         = base_url($url);
      $config['total_rows']       = count($response['view1']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

      $parameter=array( 'act_mode' => 'receipts',
                  'row_id'    => '',
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '0',
                  'Param10'   => $page,
                  'Param11'    => $second,
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
$vpage="receipts";
       }
       else if($this->session->userdata('popcoin_login')->s_usertype==2) {

       /* $parameter=array( 'act_mode' => 'receipts_R',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => $storeall,
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view1'] = $this->supper_admin->call_procedure('proc_other', $parameter);*/

$parameter=array( 'act_mode' => 'receipts_R', 
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',#$storeall,
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '1',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view1'] = $this->supper_admin->call_procedure('proc_other', $parameter);

      $config['base_url']         = base_url($url);
      $config['total_rows']       = count($response['view1']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

      $parameter=array( 'act_mode' => 'receipts_R',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => '',#$storeall,
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '0',
                  'Param10'   => $page,
                  'Param11'    => $second,
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
//p($response['view1']);
$vpage="receipts_r_s";

      if($this->input->post('newsexcel'))
        {
           $finalExcelArr = array('Bill Uid','Store Id','Store Name','Location','User Id','User Name','Date and time of upload','Date of Purchase','Date and time of bill sent to retailers',
            'Bill no','Bill Amount','cashback %','cashback amount','status');
            $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Complete List of Reciept');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
           $objPHPExcel->getActiveSheet()->freezePane('A2');
           $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
           for($i=0;$i<count($finalExcelArr);$i++)
           {
 $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
 $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );
 $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
 foreach ($response['view1'] as $key => $value) {

  $newvar = $j+$key;

   $up=number_format(($value->bill_update_amt*$value->mpc_margin)/100,2);

    $a=100-$tocom->tcom;
   $b=($a*$value->mpc_margin)/100;
      $cash=$value->mpc_margin;

       if($this->session->userdata('popcoin_login')->s_usertype==2 || $this->session->userdata('popcoin_login')->s_usertype==4)
         { $amt=$value->bill_update_amt; }
        else
         {$amt=$value->bill_amt; }


       if($value->sent_to_ret_date=='0000-00-00 00:00:00') 
                       { 
          $d2=date("d-m-Y g:i:s A", strtotime($value->re_modified));
                          }
            else
             {
        $d2= date("d-m-Y g:i:s A", strtotime($value->sent_to_ret_date));
        }

        //$d=date("d-m-Y ", strtotime($value->re_createdon);
           // $d1=date("d-m-Y", strtotime($value->date_of_purchase); 

        $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);            
    $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar,$value->bill_uid);
     $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->s_storeunid);
     $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->s_name);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->location);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->user_uid);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->t_FName);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar,date("d-m-Y g:i:s A", strtotime($value->re_createdon)));
             $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar,date("d-m-Y", strtotime($value->date_of_purchase)));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $d2);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->bill_no);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->bill_update_amt);
               $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $cash. ' %');
               $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $up);
               $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->re_status);
             

             

          
         
 }

  $filename='Receiptlist.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
   }//for end
       
    }//if end

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==4) {


       /* $parameter=array( 'act_mode' => 'receipts_S',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => $storeall,
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view1'] = $this->supper_admin->call_procedure('proc_other', $parameter);*/

 $parameter=array( 'act_mode' => 'receipts_S', 
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => $storeall,
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '1',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view1'] = $this->supper_admin->call_procedure('proc_other', $parameter);

      $config['base_url']         = base_url($url);
      $config['total_rows']       = count($response['view1']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

      $parameter=array( 'act_mode' => 'receipts_S',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => $param1,
                  'Param2'    => '',
                  'Param3'    => $param3,
                  'Param4'    => $datasearch,
                  'Param5'    => $storeall,
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '0',
                  'Param10'   => $page,
                  'Param11'    => $second,
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
$vpage="receipts_r_s";

      if($this->input->post('newsexcel'))
        {
           $finalExcelArr = array('Bill Uid','Store Id','Store Name','Location','User Id','User Name','Date and time of upload','Date of Purchase','Date and time of bill sent to retailers',
            'Bill no','Bill Amount','cashback %','cashback amount','status');
            $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Complete List of Reciept');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
           $objPHPExcel->getActiveSheet()->freezePane('A2');
           $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
           for($i=0;$i<count($finalExcelArr);$i++)
           {
 $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
 $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );
 $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
 foreach ($response['view1'] as $key => $value) {

  $newvar = $j+$key;

   $up=number_format(($value->bill_update_amt*$value->mpc_margin)/100,2);

    $a=100-$tocom->tcom;
   $b=($a*$value->mpc_margin)/100;
      $cash=$value->mpc_margin;

       if($this->session->userdata('popcoin_login')->s_usertype==2 || $this->session->userdata('popcoin_login')->s_usertype==4)
         { $amt=$value->bill_update_amt; }
        else
         {$amt=$value->bill_amt; }


       if($value->sent_to_ret_date=='0000-00-00 00:00:00') 
                       { 
          $d2=date("d-m-Y g:i:s A", strtotime($value->re_modified));
                          }
            else
             {
        $d2= date("d-m-Y g:i:s A", strtotime($value->sent_to_ret_date));
        }

        //$d=date("d-m-Y ", strtotime($value->re_createdon);
           // $d1=date("d-m-Y", strtotime($value->date_of_purchase); 

        $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);            
    $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar,$value->bill_uid);
     $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->s_storeunid);
     $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->s_name);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->location);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->user_uid);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->t_FName);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar,date("d-m-Y g:i:s A", strtotime($value->re_createdon)));
             $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar,date("d-m-Y", strtotime($value->date_of_purchase)));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $d2);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->bill_no);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->bill_update_amt);
               $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $cash. ' %');
               $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $up);
               $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->re_status);
             

             

          
         
 }

  $filename='Receiptlist.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
   }//for end
       
    }//if end
       }
//p($response['view']);

       $parameter=array( 'act_mode' => 'mpc_commistion',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['tocom'] = $this->supper_admin->call_procedureRow('proc_other', $parameter);

      $this->load->view('helper/header');
      //$this->load->view('receipts',$response);
       $this->load->view($vpage,$response);
  } 


  public function approved() {

      if($this->session->userdata('popcoin_login')->s_usertype==1) {
       //redirect(base_url().'admin/login');
      
      
       $parameter=array( 'act_mode' => 'receipts',
                  'row_id'    => '',
                  'Param1'    => 'Approved',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '1',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view1'] = $this->supper_admin->call_procedure('proc_other', $parameter);

      $config['base_url']         = base_url().'admin/receipts/approved?';
      $config['total_rows']       = count($response['view1']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

      $parameter=array( 'act_mode' => 'receipts',
                  'row_id'    => '',
                  'Param1'    => 'Approved',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '0',
                  'Param10'   => $page,
                  'Param11'    => $second,
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

      //p($response['view']); exit;
       } 
       else if($this->session->userdata('popcoin_login')->s_usertype==10) {

        $parameter=array( 'act_mode' => 'receipts_LM',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }

       else if($this->session->userdata('popcoin_login')->s_usertype==9) {

        $parameter=array( 'act_mode' => 'receipts_TM',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==8) {

        $parameter=array( 'act_mode' => 'receipts_LH',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==2) {

        $parameter=array( 'act_mode' => 'receipts_R',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==4) {

        $parameter=array( 'act_mode' => 'receipts_S',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
//p($response['view']);

       $parameter=array( 'act_mode' => 'mpc_commistion',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['tocom'] = $this->supper_admin->call_procedureRow('proc_other', $parameter);

      $this->load->view('helper/header');
      $this->load->view('receipts_approved',$response);
  } 

  public function approvedbympc() {

      if($this->session->userdata('popcoin_login')->s_usertype==1) {
       //redirect(base_url().'admin/login');
      
      
       $parameter=array( 'act_mode' => 'receipts',
                  'row_id'    => '',
                  'Param1'    => 'Approved by MPC',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '1',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
       } 
       else if($this->session->userdata('popcoin_login')->s_usertype==10) {

        $parameter=array( 'act_mode' => 'receipts_LM',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved by MPC',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }

       else if($this->session->userdata('popcoin_login')->s_usertype==9) {

        $parameter=array( 'act_mode' => 'receipts_TM',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved by MPC',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==8) {

        $parameter=array( 'act_mode' => 'receipts_LH',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved by MPC',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==2) {

        $parameter=array( 'act_mode' => 'receipts_R',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved by MPC',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==4) {

        $parameter=array( 'act_mode' => 'receipts_S',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Approved by MPC',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
//p($response['view']);

       $parameter=array( 'act_mode' => 'mpc_commistion',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['tocom'] = $this->supper_admin->call_procedureRow('proc_other', $parameter);

      $this->load->view('helper/header');
      $this->load->view('receipts_aprovedbympc',$response);
  } 


  public function rejects() {

      if($this->session->userdata('popcoin_login')->s_usertype==1) {
       //redirect(base_url().'admin/login');
      
      
       $parameter=array( 'act_mode' => 'receipts',
                  'row_id'    => '',
                  'Param1'    => 'Rejected',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '1',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
       } 
       else if($this->session->userdata('popcoin_login')->s_usertype==10) {

        $parameter=array( 'act_mode' => 'receipts_LM',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Rejected',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }

       else if($this->session->userdata('popcoin_login')->s_usertype==9) {

        $parameter=array( 'act_mode' => 'receipts_TM',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Rejected',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==8) {

        $parameter=array( 'act_mode' => 'receipts_LH',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Rejected',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==2) {

        $parameter=array( 'act_mode' => 'receipts_R',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Rejected',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
       else if($this->session->userdata('popcoin_login')->s_usertype==4) {

        $parameter=array( 'act_mode' => 'receipts_S',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => 'Rejected',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

       }
//p($response['view']);

       $parameter=array( 'act_mode' => 'mpc_commistion',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['tocom'] = $this->supper_admin->call_procedureRow('proc_other', $parameter);

      $this->load->view('helper/header');
      $this->load->view('receipts_rejected',$response);
  } 


public function pending() {

  $param =array('useremail'=>'',
              'userpassword'=>'',
              'act_mode'=>'receipt_store',
              'row_id'=>'');

 $response['store'] = $this->supper_admin->call_procedure('proc_Adminlogin', $param);

      if($this->session->userdata('popcoin_login')->s_usertype==1) {
        //redirect(base_url().'admin/login');
      
      
       $parameter=array( 'act_mode' => 'receiptspending',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
    }
    else if($this->session->userdata('popcoin_login')->s_usertype==10)
    {
        $parameter=array( 'act_mode' => 'receiptspending_LM',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
    }
    else if($this->session->userdata('popcoin_login')->s_usertype==9)
    {
        $parameter=array( 'act_mode' => 'receiptspending_TM',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
    }
    else if($this->session->userdata('popcoin_login')->s_usertype==8)
    {
        $parameter=array( 'act_mode' => 'receiptspending_LH',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
    }

    else if($this->session->userdata('popcoin_login')->s_usertype==2)
    {
        $parameter=array( 'act_mode' => 'receiptspending_R',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
    }
    else if($this->session->userdata('popcoin_login')->s_usertype==4)
    {
        $parameter=array( 'act_mode' => 'receiptspending_S',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
    }

//p($response['view']);
      $this->load->view('helper/header');
      $this->load->view('receipts-pending',$response);
  } 


public function newimagetab($id)
{
   //echo $id;

 $parameter=array( 'act_mode' => 'receiptsimg',
                  'row_id'    => '',
                  'Param1'    => $id,
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['img'] = $this->supper_admin->call_procedure('proc_other', $parameter);

     //pend($response);


      $this->load->view('helper/header');
      $this->load->view('newimgtab',$response);

}

  public function selectlocation()
  {   
      $parameter = array(
                  'act_mode' => 'selectlocation', 
                  'row_id'    => $this->input->post('storeid'),
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');
      $location = $this->supper_admin->call_procedureRow('proc_other', $parameter);
      if($location->location){
        echo $location->location;
      }
      
      //echo $location['location'];
      // if(!empty($location))
      // {
      //   $str = '';
      //   foreach($location as $k=>$v){   
      //     $str .= "<option value=".$v->location_id.">".$v->location."</option>";
      //   }
      //   echo $str;
      // }
  }

  public function updatereceipts_old() {

    $loginid=$this->session->userdata('popcoin_login')->s_admin_id;

    $param=array( 'act_mode' => 'getdata',
                  'row_id'    => $this->input->post('uid'),
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');   
                  
      $response1 = $this->supper_admin->call_procedureRow('proc_other', $param);
 
    if($this->input->post('ust')=='Approved by MPC' || $this->input->post('ust')=='Approved by Retailer')
    {
        $act='app_receipts';

        $param=array( 'act_mode' => 'getdatadeal',
                  'row_id'    => $response1->store_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $response2 = $this->supper_admin->call_procedureRow('proc_other', $param);
      if($response1->paid_by==1)
      {
          $deal=$response2->commission;
          $cashback=($response1->bill_amt*$deal)/100;
          $cus_status='Pending';
      }
      else
      {
          $deal='0';
          $cashback='0';
          $cus_status='Pending';
      }

    }
   
    else
    {
        $act='pere_receipts';
        $deal='';
        $cashback='';
        $cus_status='Rejected';

    }

     $parameter=array( 'act_mode' => $act,
                  'row_id'    => $this->input->post('uid'),
                  'Param1'    => $response1->bill_uid,
                  'Param2'    => $response1->cust_id,
                  'Param3'    => $response1->store_id,
                  'Param4'    => $response1->date_of_purchase,
                  'Param5'    => $response1->bill_no,
                  'Param6'    => $response1->bill_amt,
                  'Param7'    => $response1->paid_by,
                  'Param8'    => $cus_status,
                  'Param9'    => $response1->bill_img,
                  'Param10'   => $this->input->post('ust1'),
                  'Param11'    => $deal,
                  'Param12'    => $cashback,
                  'Param13'    => $this->input->post('ust'),
                  'Param14'    => $loginid,
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                 // p($parameter);die(); 
      $response = $this->supper_admin->call_procedureRow('proc_other', $parameter);
  }


public function updatereceipts() {

    $loginid=$this->session->userdata('popcoin_login')->s_admin_id;

    $param=array( 'act_mode' => 'getdata',
                  'row_id'    => $this->input->post('uid'),
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $response1 = $this->supper_admin->call_procedureRow('proc_other', $param);


      if($this->input->post('storeid')!='')
      {
          $storeidd=$this->input->post('storeid');
      }
      else
      {
          $storeidd=$response1->store_id;
          
      }

      $param=array( 'act_mode' => 'getdatadeal',
                  'row_id'    => $storeidd,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $store_cbper = $this->supper_admin->call_procedureRow('proc_other', $param);

      $param2=array( 'act_mode' => 'getreferaldata',
                  'row_id'    => $response1->cust_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $referal = $this->supper_admin->call_procedureRow('proc_other', $param2);  

      $param1=array( 'act_mode' => 'getalltax',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $res = $this->supper_admin->call_procedure('proc_other', $param1);

  
     
      foreach ($res as $key => $value) 
      {
         if($value->commision_title=='Tax')
         {
              $tax=$value->commision_title;
              $taxamt=$value->commission_per;
         }
          if($value->commision_title=='MPC Margin')
         {
              $mpc=$value->commision_title;
              $mpcamt=$value->commission_per;
         }
          if($value->commision_title=='MLM')
         {
              $mlm=$value->commision_title;
              $mlmamt=$value->commission_per;
         }

      }

      
     //p($this->input->post('paymenttype'));
     //p($this->input->post('billamt'));
     //p($this->input->post('billamtpop'));
     //p(trim($this->input->post('ust1'),','));
      //p($taxamt);
      //p($mpcamt);
     // p($mlmamt);

      /*if($this->input->post('paymenttype')==1)// cash
      {
          $bill_update_amt=$this->input->post('billamt');
          $bill_update_amt1=$this->input->post('billamtpop');
      }
      else // 2 mypopcoins
      {
          $bill_update_amt=$this->input->post('billamt');
          $bill_update_amt1=$this->input->post('billamtpop');
      }
*/
       $bill_update_amt=$this->input->post('billamt');
       $bill_update_amt1=$this->input->post('billamtpop');

      $sname=$store_cbper->s_name;
      $smob=$store_cbper->s_contactno;
      

      $tax_mpc=$taxamt+$mpcamt;
      //$cbper=$store_cbper->commission;
      $cbper=$response1->mpc_margin;
      if($cbper==0)
      {
          //$cashback=$response1->bill_update_amt;
          $cashback=0;
      }
      else
      {
          //$cashback=($response1->bill_update_amt*$cbper)/100;
              $cashback=($bill_update_amt*$cbper)/100;
      }
      
      $mpcdata=($cashback*$tax_mpc)/100;

      $st=($cashback*$taxamt)/100;
      $margintax=($cashback*$mpcamt)/100;
      $reject_tax=$tax_mpc+$mlmamt;
//p($bill_update_amt);
      /*----------------------MLM Process ------------------------*/
  if($this->input->post('ust')=='Rejected')
  {
          $act='rejected';
          /*$mlmd=$mlmamt;
          $retcb=($cashback*$mlmd)/100;
          $retype1='M';
          $reid1='1';
          $retype2='';
          $reid2='';
          $customer_cb=$cashback-($mpcdata+$retcb);
          $mpc_cb=$mpcdata+$retcb; */ 

          //for mail

            $param1=array( 'act_mode' => 'getemails',
                  'row_id'    => $storeidd,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
            $emails = $this->supper_admin->call_procedure('proc_other', $param1);
            //p($emails->remail);
            //p($emails->mgremail); 
            $emailid=$emails->remail;
            $stremailid=$emails->mgremail;
            $receipt=$response1->bill_uid;
            $billamnt=$response1->bill_amt;
            $reason=trim($this->input->post('ust1'),',');
            $content='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{background-color:#16369d; width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:37%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{    width: 100%;
    float: left;color:#444;}
.body_contain ul.top-list{ padding:0px;list-style-type:none;}
.body_contain ul.top-list li:first-child{font-size:16px; color:#444; line-height:22px;}
.body_contain ul.top-list li.second{font-size:13px; color:#444; line-height:20px;}
.body_contain ul.top-list li:last-child{font-size:13px; color:#444; line-height:20px;}
.content{ width:100%; float:left;}
.content .left_div{ width:60%; float:left;}
.content .left_div ul{ padding:0px; width:100%; float:left;list-style-type:none;}
.content .left_div ul li{ width:100%; float:left;}
.content .left_div ul li .list-img{ float:left; padding:0 10px 0 0;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left; background:#16369d; margin-top:15px;}
.left-f{padding-left:10px; color:#FFF; width:60%; float:left;}
.right-f{float:right; color:#fff; padding-right:5px;}
ul{ padding:0px;}
ul li{font-size:13px; color:#404040; list-style-type:none; padding:5px 0;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}

}
</style>
</head>

<body>
<div class="main_div">
  <div class="header">
    <div class="container">
        <div class="logo">
            <img src="http://115.124.98.243/~mindzshop/assets/webapp/img_hm/logo-02.png">
      </div>
        <div class="right_div">
        <div class="right-txt">Delhi,<br> , <br> New                 Delhi - 110020 <br>Website : <a href="https://www.mypopcoins.com">www.mypopcoins.com</a></div>
        </div>
    </div>
  </div>

<div class="body_contain">
  <div class="container">
      <ul>
          <li>Welcome to MyPopCoins</li>
            <li>Your receipt is rejected</li>
          <li>Details of the receipt is :-</li>
        </ul>     
        <div class="user-form">
          <table>
            <tr>
              <td class="left">Receipt UID :</td>
                <td class="right">'.$receipt.'</td>
            </tr>
            <tr>
              <td class="left">Bill Amount :</td>
                <td class="right">'.$billamnt.'</td>
            </tr>
            <tr>
              <td class="left">Reason :</td>
                <td class="right">'.$reason.'</td>
            </tr>
          </table>
        </div>    
    
   </div>
</div>  
<div class="footer">
  <div class="container">
    <p class="left-f">Looking forward to do Business with You..!!</p>
    <p class="right-f">MindzShop Team.</p>
    </div>
</div>    
    
</div>

</body>
</html>';
              $usertype=$this->session->userdata('popcoin_login')->s_usertype;
              if($usertype==2 || $usertype==4)
              {    
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('info@MindzShop.com', 'MindzShop');
                  $this->email->to('support@mindzshop.com');
                  //$this->email->cc($emailid);
                  //$this->email->cc($stremailid);
                  //$this->email->cc('priya@mindztechnology.com');
                  $this->email->subject('Receipt Rejected');
                  $this->email->message($content);
                  #$this->email->send();
              }
                  

          if(empty($referal))
      {
          //$act='noreferal';
          $mlmd=$mlmamt;
          $retcb=($cashback*$mlmd)/100;
          $retype1='M';
          $reid1='1';
          $retype2='';
          $reid2='';
          $customer_cb=$cashback-($mpcdata+$retcb);
          $mpc_cb=$mpcdata+$retcb;
      }
      else
      {
          if($referal->referal==1)
          { 
              //$act='referal_m_r';
              $mlmd=$mlmamt/2;
              $retcb=($cashback*$mlmd)/100;
              $retcb1=($cashback*$mlmamt)/100;
              $retype1=$referal->fromtype;
              $reid1=$referal->from_id;
              $retype2=$referal->fromtype2;
              $reid2=$referal->from_id2;
              $customer_cb=$cashback-($mpcdata+$retcb1);
              $mpc_cb=$mpcdata+$retcb;
          }
          else
          {
              if($referal->fromtype=='R')
              {
                  //$act='referal_r_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;
              }
              else
              {
                  //$act='referal_c_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;


                  
              }
          }
      }


      $param=array( 'act_mode' => 'get_noti_storename',
                  'p_t_imgname'    => '',
                  'p_t_path'    => '',
                  'imgtype'    => '',
                  'row_id'    => $storeidd,
                  'imagename'    => '');

     $store = $this->supper_admin->call_procedureRow('proc_img', $param);


      $custid=$response1->cust_id;
      $catid=$store->s_categoryid;
      $uid=$response1->bill_uid;
      $dated=$this->input->post('purdate');
      $reason=trim($this->input->post('ust1'),',');
      $strname=$store->s_name;

   $no_url=base_url().'api/mainapi/receiptrejected/format/json?custid='.$custid.'&cat_id='.$catid.'&sname='.urlencode($strname).'&uid='.$uid.'&dated='.urlencode($dated).'&reason='.urlencode($reason);
  #file_get_contents($no_url);



  }
  else if($this->input->post('ust')=='Approved by MPC')
  {
      $act='Sent to Retailer';
     if(empty($referal))
     {
          //$act='noreferal';
          $mlmd=$mlmamt;
          $retcb=($cashback*$mlmd)/100;
          $retype1='M';
          $reid1='1';
          $retype2='';
          $reid2='';
          $customer_cb=$cashback-($mpcdata+$retcb);
          $mpc_cb=$mpcdata+$retcb;
      }
      else
      {
          if($referal->referal==1)
          { 
              //$act='referal_m_r';
              $mlmd=$mlmamt/2;
              $retcb=($cashback*$mlmd)/100;
              $retcb1=($cashback*$mlmamt)/100;
              $retype1=$referal->fromtype;
              $reid1=$referal->from_id;
              $retype2=$referal->fromtype2;
              $reid2=$referal->from_id2;
              $customer_cb=$cashback-($mpcdata+$retcb1);
              $mpc_cb=$mpcdata+$retcb;
          }
          else
          {
              if($referal->fromtype=='R')
              {
                  //$act='referal_r_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;
              }
              else
              {
                  //$act='referal_c_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;


                  
              }
          }
      }



/*Dear Patron, You have Receipt no.___of Rs.___/- pending for the approval. Kindly log on to goo.gl/s7TJ8C and take appropriate action. Regards MPC Team*/
$burl=base_url();
      $msg=urlencode('Dear '.$sname.', You have Receipt no. '.$response1->bill_uid.' of Rs.'.$bill_update_amt.'/- pending for the approval. Kindly log on to goo.gl/s7TJ8C and take appropriate action. Regards MPC Team');


//$url='http://sms.tattler.co/app/smsapi/index.php?key=558709407EE8FE&routeid=310&type=text&contacts='.$smob.'&senderid=MPCAPP&msg='.$msg; 
 $data11=SMSURL;
  $url=$data11.$smob.'&message='.$msg;  
  #file_get_contents($url);


    
  }
  else
  {  //echo 'ji';
      if(empty($referal))
      {
          $act='noreferal';
          $mlmd=$mlmamt;
          $retcb=($cashback*$mlmd)/100;
          $retype1='M';
          $reid1='1';
          $retype2='';
          $reid2='';
          $customer_cb=$cashback-($mpcdata+$retcb);
          $mpc_cb=$mpcdata+$retcb;
      }
      else
      {
          if($referal->referal==1)
          { 
              $act='referal_m_r';
              $mlmd=$mlmamt/2;
              $retcb=($cashback*$mlmd)/100;
              $retcb1=($cashback*$mlmamt)/100;
              $retype1=$referal->fromtype;
              $reid1=$referal->from_id;
              $retype2=$referal->fromtype2;
              $reid2=$referal->from_id2;
              $customer_cb=$cashback-($mpcdata+$retcb1);
              $mpc_cb=$mpcdata+$retcb;
          }
          else
          {
              if($referal->fromtype=='R')
              {
                  $act='referal_r_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;
              }
              else
              {
                  $act='referal_c_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;


                  
              }
          }
      }
  }
       /*----------------------MLM Process end------------------------*/

if($this->input->post('permission')==1)
{
    $checkbill=$this->input->post('billno');

     $param =array('useremail'=>$checkbill,
                  'userpassword'=>'',
                  'act_mode'=>'checkbill',
                  'row_id'=>'');

     $res = $this->supper_admin->call_procedureRow('proc_Adminlogin', $param);
    echo $permission=$res->checkbill;
}
else
{
    $permission=0;
}

if($permission==0)
{

 $parameter=array( 'act_mode' => $act,
                  'row_id'    => $this->input->post('uid'),
                  'Param1'    => $response1->bill_uid,
                  'Param2'    => $response1->cust_id,
                  'Param3'    => $storeidd,//$response1->store_id,
                  'Param4'    => $this->input->post('purdate'),//$response1->date_of_purchase,
                  'Param5'    => $this->input->post('billno'),//$response1->bill_no,
                  'Param6'    => $response1->bill_amt,
                  'Param7'    => $this->input->post('paymenttype'),
                  'Param8'    => $bill_update_amt,//$response1->bill_update_amt,
                  'Param9'    => $response1->bill_img,
                  'Param10'   => trim($this->input->post('ust1'),','),
                  'Param11'    => $cbper,
                  'Param12'    => $cashback,
                  'Param13'    => $this->input->post('ust'),
                  'Param14'    => $taxamt,
                  'Param15'    => $mpcamt,
                  'Param16'    => $mlmd,
                  'Param17'    => $retype1,
                  'Param18'    => $reid1,
                  'Param19'    => $retype2,
                  'Param20'    => $reid2,
                  'Param21'    => $customer_cb,
                  'Param22'    => $mpc_cb,
                  'Param23'    => $loginid,
                  'Param24'    => $response1->re_createdon,
                  'Param25'    => $retcb,
                  'Param26'    => $st,
                  'Param27'    => $margintax,
                  'Param28'    => $bill_update_amt1,
                  'Param29'    => '',
                  'Param30'    => '',
                  'Param31'    => $response1->sent_to_ret_date,
                  'Param32'    => $reject_tax);
                  

                 

            // p($parameter);die(); 
     //$response = $this->supper_admin->call_procedureRow('proc_other1', $parameter);
     $response = $this->supper_admin->call_procedureRow('proc_receipts', $parameter);
      //exit;
 }
   

  }


  public function updatereceipts_24() {

    $loginid=$this->session->userdata('popcoin_login')->s_admin_id;

    $param=array( 'act_mode' => 'getdata',
                  'row_id'    => $this->input->post('uid'),
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $response1 = $this->supper_admin->call_procedureRow('proc_other', $param);


      $param=array( 'act_mode' => 'getdatadeal',
                  'row_id'    => $response1->store_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $store_cbper = $this->supper_admin->call_procedureRow('proc_other', $param);

      $param2=array( 'act_mode' => 'getreferaldata',
                  'row_id'    => $response1->cust_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $referal = $this->supper_admin->call_procedureRow('proc_other', $param2);  

      $param1=array( 'act_mode' => 'getalltax',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $res = $this->supper_admin->call_procedure('proc_other', $param1);

  
     
      foreach ($res as $key => $value) 
      {
         if($value->commision_title=='Tax')
         {
              $tax=$value->commision_title;
              $taxamt=$value->commission_per;
         }
          if($value->commision_title=='MPC Margin')
         {
              $mpc=$value->commision_title;
              $mpcamt=$value->commission_per;
         }
          if($value->commision_title=='MLM')
         {
              $mlm=$value->commision_title;
              $mlmamt=$value->commission_per;
         }

      }

      
     // p($referal);
      //p($taxamt);
      //p($mpcamt);
     // p($mlmamt);

      $tax_mpc=$taxamt+$mpcamt;
      $cbper=$store_cbper->commission;
      if($cbper==0)
      {
          //$cashback=$response1->bill_update_amt;
          $cashback=0;
      }
      else
      {
          $cashback=($response1->bill_update_amt*$cbper)/100;
      }
      
      $mpcdata=($cashback*$tax_mpc)/100;

      $st=($cashback*$taxamt)/100;
      $margintax=($cashback*$mpcamt)/100;
//p($cbper);
      /*----------------------MLM Process ------------------------*/
  if($this->input->post('ust')=='Rejected')
  {
          $act='rejected';
          $mlmd=$mlmamt;
          $retcb=($cashback*$mlmd)/100;
          $retype1='M';
          $reid1='1';
          $retype2='';
          $reid2='';
          $customer_cb=$cashback-($mpcdata+$retcb);
          $mpc_cb=$mpcdata+$retcb;    
  }
  else
  {
      if(empty($referal))
      {
          $act='noreferal';
          $mlmd=$mlmamt;
          $retcb=($cashback*$mlmd)/100;
          $retype1='M';
          $reid1='1';
          $retype2='';
          $reid2='';
          $customer_cb=$cashback-($mpcdata+$retcb);
          $mpc_cb=$mpcdata+$retcb;
      }
      else
      {
          if($referal->referal==1)
          { 
              $act='referal_m_r';
              $mlmd=$mlmamt/2;
              $retcb=($cashback*$mlmd)/100;
              $retcb1=($cashback*$mlmamt)/100;
              $retype1=$referal->fromtype;
              $reid1=$referal->from_id;
              $retype2=$referal->fromtype2;
              $reid2=$referal->from_id2;
              $customer_cb=$cashback-($mpcdata+$retcb1);
              $mpc_cb=$mpcdata+$retcb;
          }
          else
          {
              if($referal->fromtype=='R')
              {
                  $act='referal_r_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;
              }
              else
              {
                  $act='referal_c_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;


                  
              }
          }
      }
  }
       /*----------------------MLM Process end------------------------*/



 $parameter=array( 'act_mode' => $act,
                  'row_id'    => $this->input->post('uid'),
                  'Param1'    => $response1->bill_uid,
                  'Param2'    => $response1->cust_id,
                  'Param3'    => $response1->store_id,
                  'Param4'    => $response1->date_of_purchase,
                  'Param5'    => $response1->bill_no,
                  'Param6'    => $response1->bill_amt,
                  'Param7'    => $response1->paid_by,
                  'Param8'    => $response1->bill_update_amt,
                  'Param9'    => $response1->bill_img,
                  'Param10'   => $this->input->post('ust1'),
                  'Param11'    => $cbper,
                  'Param12'    => $cashback,
                  'Param13'    => $this->input->post('ust'),
                  'Param14'    => $taxamt,
                  'Param15'    => $mpcamt,
                  'Param16'    => $mlmd,
                  'Param17'    => $retype1,
                  'Param18'    => $reid1,
                  'Param19'    => $retype2,
                  'Param20'    => $reid2,
                  'Param21'    => $customer_cb,
                  'Param22'    => $mpc_cb,
                  'Param23'    => $loginid,
                  'Param24'    => $response1->re_createdon,
                  'Param25'    => $retcb,
                  'Param26'    => $st,
                  'Param27'    => $margintax);  
                // p($parameter);die(); 
     $response = $this->supper_admin->call_procedureRow('proc_other1', $parameter);
      //exit;
 
   

  }

 public function updatereceipts1() {

    $loginid=$this->session->userdata('popcoin_login')->s_admin_id;

      $param=array( 'act_mode' => 'udateretailer',
                  'row_id'    => $this->input->post('uid'),
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => 'Approved by Retailer',
                  'Param14'    => $loginid,
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');   
                 //p($param);die(); 
      $response = $this->supper_admin->call_procedureRow('proc_other', $param);
  }


public function retailer_receipts() {

      /*if($this->session->userdata('popcoin_login')->s_usertype!=2) {
        redirect(base_url().'admin/login');
      }*/
      
if($this->session->userdata('popcoin_login')->s_admin_id==1 && $this->session->userdata('popcoin_login')->s_usertype==1)
    {

      $parameter=array(
                  'act_mode' => 'retailer_receipts',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
   
       //p($response['vieww']); exit;
    }else  if($this->session->userdata('popcoin_login')->s_usertype==2){

        

      $parameter=array(
                  'act_mode' => 'retailer_receipts1',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);
      //p($response['vieww']); exit;

    } else if($this->session->userdata('popcoin_login')->s_usertype==4){

      $parameter=array(
                  'act_mode' => 'retailer_receipts_manager',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

    }else if($this->session->userdata('popcoin_login')->s_usertype==8)
    {

      $parameter=array(
                  'act_mode' => 'retailer_receipts_OH',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);


    }else if($this->session->userdata('popcoin_login')->s_usertype==9)
    {
       $parameter=array(
                  'act_mode' => 'retailer_receipts_TL',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

    }else{

       $parameter=array(
                  'act_mode' => 'retailer_receipts_OM',
                  'row_id'    => $this->session->userdata('popcoin_login')->s_admin_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');   
      $response['view'] = $this->supper_admin->call_procedure('proc_other', $parameter);

   

    }

    $parameter=array( 'act_mode' => 'mpc_commistion',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
      $response['tocom'] = $this->supper_admin->call_procedureRow('proc_other', $parameter);


      $this->load->view('helper/header');
      $this->load->view('retailer_receipts',$response);
  } 

public function transactions() {

    

      $param=array( 'act_mode' => 'transactions',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');   
                 //p($param);die(); 
       $response['view']= $this->supper_admin->call_procedure('proc_other', $param);

       //p($response['view']);die();
      $this->load->view('helper/header');
      $this->load->view('transactions',$response);
  }

  /*function getWednesdays($y, $m)
{
    return new DatePeriod(
        new DateTime("first wednesday of $y-$m"),
        DateInterval::createFromDateString('next wednesday'),
        new DateTime("last day of $y-$m")
    );
} 
*/

function getdays()
{
  $param=array('country_id' => '',
                  'state_name' => '',
                  'countrycode'=> '',
                  'row_id'     => '',
                  'act_mode'   => 'fetchcycles');
  $cycles = $this->supper_admin->call_procedureRow('proc_state', $param);
 // p($cycles->day_name);exit;
  $month=$this->input->post('month');
  $year=$this->input->post('year');
  $day=$cycles->day_name;
  $fromto=$cycles->diff_days_to;
  $paydateg=$cycles->pay_day;
  $dd=array();
  $val='<div class="row"><div class="col-lg-7"><select name="cycle" class="form-control"><option value="">Select Day</option>';
  $i=0;
  foreach (getWednesdays($year, $month, $day) as $wednesday) { $i++;
    $dateday=$wednesday->format("l, Y-m-d\n");
    $dd=explode(',', $dateday);
    //echo "--Start--";
    $date = strtotime(date($dd[1]));
    //$date = strtotime("+".$fromto." day", $date);// 3 feb Pavan
    $date = strtotime("-".$fromto." day", $date);
    //print_r($dd[1]);
    //echo " -To- ";
    $ndate=trim(date('Y-m-d', $date));
    //echo " |-Paydate-| ";
    //$paydate = strtotime(date($ndate));//3 feb pavan
    $paydate = strtotime(date($dd[1]));
    //$paydate = strtotime(date('Y-m-d'));
    $paydate = strtotime("+".$paydateg." day", $paydate);
    $paydate=trim(date('Y-m-d', $paydate));
    $pay = strtotime($dd[1]);
    $pay = strtotime("- 1 day", $pay);
    $pay=trim(date('Y-m-d', $pay));
    //echo "--End--";

    $date_string = trim($dd[0]).'*'.$ndate.'*'.$pay.'*'.$paydate.'*'.trim($i).'*'.trim($dd[1]);
    
    $select ='';

    if($date_string == trim($this->input->post('filter_date_string'))) {
      $select =  'selected';
     
    } else {
      $select = '';
    }  


    
    $val .='<option "/'.$select.'/" value="'.$date_string.'">'.$i.'-'.$dateday.'</option>';
}
  $val .='</select></div><div class="col-lg-5">
                                            <div class="submit_tbl">
                                                <input type="submit" value="Search" name="filter" class="btn_button sub_btn" />
                                            </div>
                                        </div></div>';
  echo $val;
}


  public function pendingtransactions() {

      /*if($this->session->userdata('popcoin_login')->s_usertype!=1) {
        redirect(base_url().'admin/login');
      }*/
      
     if($this->input->get('pay_type')!="")
     {
        

        $cycle=explode("*",$this->input->get('cycle'));
          $cycle_day=$cycle[0];
          $start_date=trim($cycle[1]);
          $end_date=$cycle[2];
          $pay_date=$cycle[3];
          $cycle_id=$cycle[4];
          
/*p($cycle_day);
p($start_date);
p($end_date);
p($pay_date);*/

        $param=array('act_mode' => 'all_transaction',
                    'Param'     => '',
                    'Param1'    => $start_date,
                    'Param2'    => $end_date,
                    'Param3'    => '',
                    'Param4'    => '',
                    'Param5'    => '',
                    'Param6'    => '',
                    'Param7'    => '',
                    'Param8'    => $this->input->get('pay_type'),
                    'Param9'    => '',
                    'Param10'   => '');
        //p($param);exit;
        $response['view'] = $this->supper_admin->call_procedure('proc_transaction', $param);
        //p($response['view']);exit;
      }


      if(isset($_POST['save_data'])) {

         $this->form_validation->set_rules('checkvalue','Please Select atleast one!','required');
          //exit;
          if($this->form_validation->run() != false) {

         // $rand=rand(1000000,9999999);
          //$txn_id='RID'.$rand;
//echo $txn_id;
$check=$this->input->post('checkvalue');



      for ($i=0; $i < count($check); $i++) { 

        $rand=rand(1000000,9999999);
          $txn_id='RID'.$rand;
          $txn_str[]='RID'.$rand;
        //echo $check[$i];

        $param=array('act_mode' => 'single_transaction',
                    'Param'     => $check[$i],
                    'Param1'    => $start_date,
                    'Param2'    => $end_date,
                    'Param3'    => '',
                    'Param4'    => '',
                    'Param5'    => '',
                    'Param6'    => '',
                    'Param7'    => '',
                    'Param8'    => $this->input->get('pay_type'),
                    'Param9'    => '',
                    'Param10'   => '');
        //p($param);exit;
        $resview = $this->supper_admin->call_procedure('proc_transaction', $param);
        $resviewmsg[] = $this->supper_admin->call_procedure('proc_transaction', $param);
      //p($resview);
        $param1=array('act_mode' => 'final_transaction',
                    'Param'     => $check[$i],
                    'Param1'    => $start_date,
                    'Param2'    => $end_date,
                    'Param3'    => '',
                    'Param4'    => '',
                    'Param5'    => '',
                    'Param6'    => '',
                    'Param7'    => '',
                    'Param8'    => $this->input->get('pay_type'),
                    'Param9'    => '',
                    'Param10'   => '');
        //p($param);//exit;
        $finalprocess = $this->supper_admin->call_procedureRow('proc_transaction', $param1);
       //p($finalprocess);
//}
       //exit;

       foreach($resview as $val) {
            $trans_array[] = $val->t_id;
            $param2 = array(
                          'act_mode' => 'insertsuccesstransactions', 
                          'row_id' => $val->t_id,
                          'param1' => $val->receipts_id,
                          'param2' => $val->order_id,
                          'param3' => $check[$i],
                          'param4' => $val->store_id,
                          'param5' => $val->deal_id,
                          'param6' => $val->user_id,
                          'param7' => $val->user_type,
                          'param8' => $val->pay_type,
                          'param9' => $val->tax_type,
                          'param10' => $val->tax_amt,
                          'param11' => $val->total_amt,
                          'param12' => $val->pay_amt,
                          'param13' => $txn_id,
                          'param14' => $cycle_id,
                          'param15' => $val->txn_id_point,
                          'param16' => '',
                          'param17' => '',
                          'param18' => ''
                        );  
                    //p($param2);           
           $res1 = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);

          }
          $t_ids = implode(",",$trans_array);
          $param3 = array(
                          'act_mode' => 'inserttransmap', 
                          'row_id' => '',
                          'param1' => $start_date,
                          'param2' => $end_date,
                          'param3' => $check[$i],
                          'param4' => $pay_date,
                          'param5' => $this->input->get('pay_type'),
                          'param6' => $finalprocess->totalamt,
                          'param7' => @$finalprocess->storecount,
                          'param8' => '',
                          'param9' => '',
                          'param10' => '',
                          'param11' => '',
                          'param12' => $cycle_day,
                          'param13' => $txn_id,
                          'param14' => $cycle_id,
                          'param15' => '',
                          'param16' => '',
                          'param17' => '',
                          'param18' => ''//$t_ids
                        );      
        //p($param3); 
          $res2 = $this->supper_admin->call_procedureRow('proc_procedure4', $param3);
      } //for 
      //exit;

/*----------------------------- Start Retailer Mail/SMS----------------------------*/ 

      foreach ($resviewmsg as $value) 
      {
          $email=$value[0]->s_loginemail;
          $mobile= $value[0]->contact_no;
          //$mobile= '9997785061';
          $sum = 0;
          foreach ($value as $key => $value1) 
          {
              $sid[]=$value1->store_id;
             if($value1->s_admin_id==$value[0]->s_admin_id)
             {
                  $sum+=$value1->pay_amt;
             }

          }

        //echo $sum;
          //echo $txn_id;
if($this->input->get('pay_type')=='Receivables'){ //mail
         $msg=urlencode('DEAR MPC Partner, Bill Generated for Retailer ID: '.$value[0]->retailer_codeid.' ('.$value[0]->company_name.') from '.date("d/m/Y",strtotime($start_date)).' to '.date("d/m/Y",strtotime($end_date)).'. Total Due Amt: Rs '.round($sum,2).'/-. Pymt due on '.date("d/m/Y",strtotime($pay_date)));

          //$url='http://sms.tattler.co/app/smsapi/index.php?key=558709407EE8FE&routeid=288&type=text&contacts='.$mobile.'&senderid=WEBPRO&msg='.$msg; 
                $data11=SMSURL;
           $url=$data11.$mobile.'&message='.$msg;  
          #file_get_contents($url);
              //print_r(file_get_contents($url));


    $content='<table border="0" cellpadding="0" cellspacing="0" height="auto" width="100%" style="border: solid 1px #CCCCCC;max-width: 620px;margin:auto;font-family: Rubik, sans-serif;font-size: 13px;">
     <tr>
    <td align="center" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="600">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr style="width:200px; float:left;">
                                <td align="left" valign="top" style="width:200px;float:left;">
                                    <img src="'.base_url().'assets/webapp/img/logoNew.png">
                                </td>
                </tr>
                <tr style="width:380px; float:left;">
                <td align="right" valign="top" style="padding:20px 6px;float: right;">
                <a href="https://twitter.com/MyPopCoins">
                <img src="https://mypopcoins.com/partner/assets/img/social1.png" alt="twitter" style="width:33px;height:33px;border-radius:5px;">
                </a></td>
                <td align="right" valign="top" style="padding:20px 6px;float: right;">
                <a href="https://www.youtube.com/channel/UCny71U-VfGi5G9k8hs27bkg">
                <img src="https://mypopcoins.com/partner/assets/img/social2.png" alt="youtube" style="width:33px;height:33px;border-radius:5px;">
                </a></td>
                <td align="right" valign="top" style="padding:20px 6px;float: right;">
                <a href="https://www.facebook.com/mypopcoins/">
                <img src="https://mypopcoins.com/partner/assets/img/social3.png" alt="facebook" style="width:33px;height:33px;border-radius:5px;">
                </a></td>
                <td align="right" valign="top" style="padding:20px 6px;float: right;">
                <a href="https://www.linkedin.com/in/mypopcoins">
                <img src="https://mypopcoins.com/partner/assets/img/social4.png" alt="linkedin" style="width:33px;height:33px;border-radius:5px;">
                </a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
             </table>
                <table border="0" cellpadding="0" cellspacing="0" style="width:620px; float:left;background: #ffe5e6;">
             <tr style="width:300px;float:left;padding:10px 0px;">
               <td border="0" cellpadding="0" cellspacing="0" style="font-family: Rubik, sans-serif;width:100%;float:left;font-size:13px;color:#444; padding:5px 10px;font-weight: 600;">Retailer ID: '.$value[0]->retailer_codeid.'</td>
             <td border="0" cellpadding="0" cellspacing="0" style="font-family: Rubik, sans-serif;width:100%;float:left;font-size:13px;color:#444; padding:5px 10px;font-weight: 600;">Retailer Name: '.$value[0]->company_name.'</td>
             </tr>
              <tr style="width:300px;float:left;padding:10px 0px;">
               <td border="0" cellpadding="0" cellspacing="0" style="font-family: Rubik, sans-serif;width:100%;float:left;font-size:13px;color:#444; padding:5px 0px;font-weight: 600;text-align: right;">Due Date: '.date("d/m/Y",strtotime($pay_date)).'</td>
             </tr>
             
              <tr style="width:620px;float:left;padding:10px 0px;">
               <td border="0" cellpadding="0" cellspacing="0" style="width:200px;float:left;font-size:13px;color:#444; padding:5px 10px;font-weight: 600;">
             <img src="https://mypopcoins.com/partner/assets/img/coins.jpg" alt="">
             </td>
             <td style="width:400px;font-family:Rubik, sans-serif;color: #ff0004;font-weight: 600;font-size: 16px;padding: 15px 0px;float:left;">Dear MPC Partner,</td>
             <td style="width:400px;font-family:Rubik, sans-serif;color: #444;font-weight: 600;font-size: 18px;padding: 0px 0px;float:left;">Total Amount Due <span style="color:#ff0004">'.round($sum,2).'/-</span></td>
             <td style="width:400px;font-family:Rubik, sans-serif;color: #444;font-size: 13px;padding: 13px 0px;float:left;">*Please make the payment before due date to avoid Penalties.</td>
             <td style="width:400px;font-family:Rubik, sans-serif;color: #444;font-size: 13px;padding: 13px 0px;float:left;">(From:'.date("d/m/Y",strtotime($start_date)).' to '.date("d/m/Y",strtotime($end_date)).')</td>
             </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0"style="width:620px; float:left;">
          <tr style="width:100%;float:left;padding:10px 0px;">
            <td style="width:300px;float:left;font-size:13px;padding:0px 7px;">Looking forward to do Business with You..!!</td>
          <td style="width:300px;float:left;text-align:right;font-size:13px;">MindzShop Team.</td>
          </tr>
        </table>
    </td>
   </tr>
   </table>';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('support@mindzshop.com', 'MindzShop');
                  $this->email->to($email);
                  //$this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Process Payment');
                  $this->email->message($content);
                  #$this->email->send();
} //mail
   
      }
/*----------------------------- End Retailer Mail/SMS----------------------------*/ 
$sid1=array_unique($sid);
foreach ($sid1 as $key => $val) 
{
//p($sid1);
  $t=0;
  foreach ($resviewmsg as $value) 
  {
     $sum1 = 0;
      foreach ($value as $value1) 
      {
        //p($value1);
         if($val==$value1->store_id)
         {    
              
               $sum1+=$value1->pay_amt;
         }
     }

  
                     /*$param11=array('act_mode' => 'storebill',
                                    'Param'     => $val,
                                    'Param1'    => $sum1,
                                    'Param2'    => $txn_id,
                                    'Param3'    => '',
                                    'Param4'    => $this->input->get('pay_type'),
                                    'Param5'    => '',
                                    'Param6'    => '',
                                    'Param7'    => '',
                                    'Param8'    => '',
                                    'Param9'    => '',
                                    'Param10'   => '');
                        //p($param11);//exit;
                       $storebill = $this->supper_admin->call_procedureRow('proc_transaction', $param11);*/

         
      if($sum1!=0)
      {

                     $param11=array('act_mode' => 'storebill',
                                    'Param'     => $val,
                                    'Param1'    => $sum1,
                                    'Param2'    => $txn_str[$t],
                                    'Param3'    => '',
                                    'Param4'    => $this->input->get('pay_type'),
                                    'Param5'    => '',
                                    'Param6'    => '',
                                    'Param7'    => '',
                                    'Param8'    => '',
                                    'Param9'    => '',
                                    'Param10'   => '');
                        //p($param11);//exit;
                       $storebill = $this->supper_admin->call_procedureRow('proc_transaction', $param11);


            $param1=array('act_mode' => 'storemail',
                          'Param'     => $val,
                          'Param1'    => $sum1,
                          'Param2'    => '',
                          'Param3'    => '',
                          'Param4'    => '',
                          'Param5'    => '',
                          'Param6'    => '',
                          'Param7'    => '',
                          'Param8'    => '',
                          'Param9'    => '',
                          'Param10'   => '');
              //p($param1);
              $storemail = $this->supper_admin->call_procedureRow('proc_transaction', $param1);


              if(!empty($storemail))
              {
                  //p($storemail->s_name);
                 $mobiles=$storemail->contact_no;
                 //$mobiles= '9997785061';
if($this->input->get('pay_type')=='Receivables'){ //mail
                 $msg=urlencode('DEAR MPC Partner, Bill Generated for Store ID: '.$storemail->s_storeunid.' ('.$storemail->s_name.') from '.date("d/m/Y",strtotime($start_date)).' to '.date("d/m/Y",strtotime($end_date)).'. Total Due Amt: Rs '.round($storemail->amt,2).'/-. Pymt due on '.date("d/m/Y",strtotime($pay_date)));

          //$url='http://sms.tattler.co/app/smsapi/index.php?key=558709407EE8FE&routeid=310&type=text&contacts='.$mobiles.'&senderid=MPCAPP&msg='.$msg; 
                $data11=SMSURL;
           $url=$data11.$mobiles.'&message='.$msg;  
          #file_get_contents($url);
             



$content1='<table border="0" cellpadding="0" cellspacing="0" height="auto" width="100%" style="border: solid 1px #CCCCCC;max-width: 620px;margin:auto;font-family: Rubik, sans-serif;font-size: 13px;">
     <tr>
    <td align="center" valign="top">
      <table border="0" cellpadding="0" cellspacing="0" width="600">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr style="width:200px; float:left;">
                                <td align="left" valign="top" style="width:200px;float:left;">
                                    <img src="'.base_url().'assets/webapp/img/logoNew.png">
                                </td>
                </tr>
                <tr style="width:380px; float:left;">
                <td align="right" valign="top" style="padding:20px 6px;float: right;">
                <a href="https://twitter.com/MyPopCoins">
                <img src="https://mypopcoins.com/partner/assets/img/social1.png" alt="twitter" style="width:33px;height:33px;border-radius:5px;">
                </a></td>
                <td align="right" valign="top" style="padding:20px 6px;float: right;">
                <a href="https://www.youtube.com/channel/UCny71U-VfGi5G9k8hs27bkg">
                <img src="https://mypopcoins.com/partner/assets/img/social2.png" alt="youtube" style="width:33px;height:33px;border-radius:5px;">
                </a></td>
                <td align="right" valign="top" style="padding:20px 6px;float: right;">
                <a href="https://www.facebook.com/mypopcoins/">
                <img src="https://mypopcoins.com/partner/assets/img/social3.png" alt="facebook" style="width:33px;height:33px;border-radius:5px;">
                </a></td>
                <td align="right" valign="top" style="padding:20px 6px;float: right;">
                <a href="https://www.linkedin.com/in/mypopcoins">
                <img src="https://mypopcoins.com/partner/assets/img/social4.png" alt="linkedin" style="width:33px;height:33px;border-radius:5px;">
                </a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
             </table>
                <table border="0" cellpadding="0" cellspacing="0" style="width:620px; float:left;background: #ffe5e6;">
             <tr style="width:300px;float:left;padding:10px 0px;">
               <td border="0" cellpadding="0" cellspacing="0" style="font-family: Rubik, sans-serif;width:100%;float:left;font-size:13px;color:#444; padding:5px 10px;font-weight: 600;">Store ID: '.$storemail->s_storeunid.'</td>
             <td border="0" cellpadding="0" cellspacing="0" style="font-family: Rubik, sans-serif;width:100%;float:left;font-size:13px;color:#444; padding:5px 10px;font-weight: 600;">Store Name: '.$storemail->s_name.'</td>
             </tr>
              <tr style="width:300px;float:left;padding:10px 0px;">
               <td border="0" cellpadding="0" cellspacing="0" style="font-family: Rubik, sans-serif;width:100%;float:left;font-size:13px;color:#444; padding:5px 0px;font-weight: 600;text-align: right;">Due Date: '.date("d/m/Y",strtotime($pay_date)).'</td>
             </tr>
             
              <tr style="width:620px;float:left;padding:10px 0px;">
               <td border="0" cellpadding="0" cellspacing="0" style="width:200px;float:left;font-size:13px;color:#444; padding:5px 10px;font-weight: 600;">
             <img src="https://mypopcoins.com/partner/assets/img/coins.jpg" alt="">
             </td>
             <td style="width:400px;font-family:Rubik, sans-serif;color: #ff0004;font-weight: 600;font-size: 16px;padding: 15px 0px;float:left;">Dear MPC Partner,</td>
             <td style="width:400px;font-family:Rubik, sans-serif;color: #444;font-weight: 600;font-size: 18px;padding: 0px 0px;float:left;">Total Amount Due <span style="color:#ff0004">'.round($storemail->amt,2).'/-</span></td>
             <td style="width:400px;font-family:Rubik, sans-serif;color: #444;font-size: 13px;padding: 13px 0px;float:left;">*Please make the payment before due date to avoid Penalties.</td>
             <td style="width:400px;font-family:Rubik, sans-serif;color: #444;font-size: 13px;padding: 13px 0px;float:left;">(From:'.date("d/m/Y",strtotime($start_date)).' to '.date("d/m/Y",strtotime($end_date)).')</td>
             </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0"style="width:620px; float:left;">
          <tr style="width:100%;float:left;padding:10px 0px;">
            <td style="width:300px;float:left;font-size:13px;padding:0px 7px;">Looking forward to do Business with You..!!</td>
          <td style="width:300px;float:left;text-align:right;font-size:13px;">MindzShop Team.</td>
          </tr>
        </table>
    </td>
   </tr>
   </table>';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('support@mindzshop.com', 'MyPopCoins');
                  $this->email->to($storemail->s_loginemail);
                  //$this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Process Payment');
                  $this->email->message($content1);
                  #$this->email->send();
} //mail
              }
      }
$t++;  }
}//exit;

$this->session->set_flashdata('message', 'Your payment process has been successful - Cycle ('.$cycle_id.') !!');
    //$this->session->set_flashdata('message', 'Your payment process has been successful - Cycle ('.$cycle_id.'), Ref-ID ('.$txn_id.') !!');
        redirect(base_url().'admin/receipts/pendingtransactions');//exit;
      }
    }


      $param=array('country_id' => '',
                  'state_name' => '',
                  'countrycode'=> '',
                  'row_id'     => '',
                  'act_mode'   => 'fetchcycles');
      $response['cycles'] = $this->supper_admin->call_procedure('proc_state', $param);
      $this->load->view('helper/header');
      $this->load->view('transaction',$response);
  } 

  public function pendingtransactions_old() {

      if($this->session->userdata('popcoin_login')->s_usertype!=1) {
        redirect(base_url().'admin/login');
      }
      if($this->input->get('cycle')=="") {
        $param=array('country_id' => '',
                    'state_name' => '',
                    'countrycode'=> 'pending',
                    'row_id'     => '',
                    'act_mode'   => 'gealltransactions');
        $response['view'] = $this->supper_admin->call_procedure('proc_state', $param); 
      } else {
        if($this->input->get('cycle')!="") {
          $cycle=explode("*",$this->input->get('cycle'));
          $cycle_id=$cycle[0];
          $pay_date=$cycle[1];
          $cycledate=explode("-",$cycle[2]);
          $startdate=$cycledate[0];
          $enddate=$cycledate[1];
        } else {
          $startdate="";
          $enddate="";
        }
        if($this->input->get('month')!="") {
          $array=explode("-",$this->input->get('month'));
          $month=$array[0];
          $year=$array[1];
        } else {
          $month="";
          $year="";
        }
        if($this->input->get('store_id')!="") {
          $store_id=$this->input->get('store_id');
        } else {
          $store_id="";
        }
        if($this->input->get('pay_type')!="") {
          $pay_type=$this->input->get('pay_type');
        } else {
          $pay_type="";
        }       
        $dateObj   = DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F');
        $monthend = date("d",strtotime('last day of '.$monthName, time()));

        $start_date = $year.'-'.$month.'-'.$startdate.' 00:00:00';          
        $start_date1 = $year.'-'.$month.'-'.$startdate;
        if($enddate >= $startdate && $enddate <= $monthend) {
          $end_date = $year.'-'.$month.'-'.$enddate.' 00:00:00'; 
          $end_date1 = $year.'-'.$month.'-'.$enddate;   
        } else {
          $nextmonth = date('Y-m',strtotime('+1 month',strtotime($start_date1)));
          $end_date = $nextmonth.'-'.$enddate.' 00:00:00'; 
          $end_date1 = $nextmonth.'-'.$enddate;
        }
        $param=array('act_mode' => 'transaction_filter',
                    'Param'     => 'pending',
                    'Param1'    => $start_date,
                    'Param2'    => $end_date,
                    'Param3'    => '',
                    'Param4'    => '',
                    'Param5'    => '',
                    'Param6'    => '',
                    'Param7'    => $store_id,
                    'Param8'    => $pay_type,
                    'Param9'    => '',
                    'Param10'   => '');
        $response['view'] = $this->supper_admin->call_procedure('proc_transaction', $param);
        $param=array('act_mode' => 'transaction_storewisetotalprice',
                    'Param'     => 'pending',
                    'Param1'    => $start_date,
                    'Param2'    => $end_date,
                    'Param3'    => '',
                    'Param4'    => '',
                    'Param5'    => '',
                    'Param6'    => '',
                    'Param7'    => $store_id,
                    'Param8'    => $pay_type,
                    'Param9'    => '',
                    'Param10'   => '');
        $response['amount'] = $this->supper_admin->call_procedure('proc_transaction', $param);
        if($response['amount'][0]->totalamt>=0){
          $type="Receivables";
        } else {
          $type="Payables";
        }
        $response['date'] = array("start_date"=>$start_date1,"end_date"=>$end_date1,"pay_date"=>$pay_date,"pay_type"=>$type);
        //pend($response['date']);
      }

      if(isset($_POST['save_data'])) {
          $rand=rand(1111,9999);
          $txn_id='TXN-'.$rand;
          foreach($response['view'] as $val) {
            $trans_array[] = $val->t_id;
            $param1 = array(
                          'act_mode' => 'insertsuccesstransactions', 
                          'row_id' => $val->t_id,
                          'param1' => $val->receipts_id,
                          'param2' => $val->order_id,
                          'param3' => $response['amount'][0]->s_admin_id,
                          'param4' => $val->store_id,
                          'param5' => $val->deal_id,
                          'param6' => $val->user_id,
                          'param7' => $val->user_type,
                          'param8' => $val->pay_type,
                          'param9' => $val->tax_type,
                          'param10' => $val->tax_amt,
                          'param11' => $val->total_amt,
                          'param12' => $val->pay_amt,
                          'param13' => $txn_id,
                          'param14' => $cycle_id,
                          'param15' => '',
                          'param16' => '',
                          'param17' => '',
                          'param18' => ''
                        );             
            $res1 = $this->supper_admin->call_procedureRow('proc_procedure4', $param1);
          }
          $t_ids = implode(",",$trans_array);
          $param2 = array(
                          'act_mode' => 'inserttransmap', 
                          'row_id' => '',
                          'param1' => $response['date']['start_date'],
                          'param2' => $response['date']['end_date'],
                          'param3' => $response['amount'][0]->s_admin_id,
                          'param4' => $response['date']['pay_date'],
                          'param5' => $response['date']['pay_type'],
                          'param6' => $response['amount'][0]->totalamt,
                          'param7' => $response['amount'][0]->store_id,
                          'param8' => '',
                          'param9' => '',
                          'param10' => '',
                          'param11' => '',
                          'param12' => '',
                          'param13' => $txn_id,
                          'param14' => $cycle_id,
                          'param15' => '',
                          'param16' => '',
                          'param17' => '',
                          'param18' => $t_ids
                        );             
          $res2 = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
          $this->session->set_flashdata('message', 'Your transaction has been successful - '.$txn_id.' (TXN ID)!');
          redirect(base_url().'admin/receipts/successtransactions');
      }
      $param=array('country_id' => '',
                  'state_name' => '',
                  'countrycode'=> 'pending',
                  'row_id'     => '',
                  'act_mode'   => 'gettransactionsstore');
      $response['stores'] = $this->supper_admin->call_procedure('proc_state', $param);

      $param=array('country_id' => '',
                  'state_name' => '',
                  'countrycode'=> '',
                  'row_id'     => '',
                  'act_mode'   => 'fetchcycles');
      $response['cycles'] = $this->supper_admin->call_procedure('proc_state', $param);
      $this->load->view('helper/header');
      $this->load->view('transaction',$response);
  } 
  
  public function successtransactions() {

      if($this->session->userdata('popcoin_login')->s_usertype!=1) {
        redirect(base_url().'admin/login');
      }
      if($this->input->get('cycle')=="") {
        $param=array('country_id' => '',
                    'state_name' => '',
                    'countrycode'=> 'success',
                    'row_id'     => '',
                    'act_mode'   => 'gealltransactions');
        $response['view'] = $this->supper_admin->call_procedure('proc_state', $param); 
      } else {
        if($this->input->get('cycle')!="") {
          $cycle=explode("*",$this->input->get('cycle'));
          $pay_date=$cycle[1];
          $cycledate=explode("-",$cycle[2]);
          $startdate=$cycledate[0];
          $enddate=$cycledate[1];
        } else {
          $startdate="";
          $enddate="";
        }
        if($this->input->get('month')!="") {
          $array=explode("-",$this->input->get('month'));
          $month=$array[0];
          $year=$array[1];
        } else {
          $month="";
          $year="";
        }
        if($this->input->get('store_id')!="") {
          $store_id=$this->input->get('store_id');
        } else {
          $store_id="";
        }
        if($this->input->get('pay_type')!="") {
          $pay_type=$this->input->get('pay_type');
        } else {
          $pay_type="";
        }       
        $dateObj   = DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F');
        $monthend = date("d",strtotime('last day of '.$monthName, time()));

        $start_date = $year.'-'.$month.'-'.$startdate.' 00:00:00';          
        $start_date1 = $year.'-'.$month.'-'.$startdate;
        if($enddate >= $startdate && $enddate <= $monthend) {
          $end_date = $year.'-'.$month.'-'.$enddate.' 00:00:00'; 
          $end_date1 = $year.'-'.$month.'-'.$enddate;   
        } else {
          $nextmonth = date('Y-m',strtotime('+1 month',strtotime($start_date1)));
          $end_date = $nextmonth.'-'.$enddate.' 00:00:00'; 
          $end_date1 = $nextmonth.'-'.$enddate;
        } 
        $param=array('act_mode' => 'transaction_filter',
                    'Param'     => 'success',
                    'Param1'    => $start_date,
                    'Param2'    => $end_date,
                    'Param3'    => '',
                    'Param4'    => '',
                    'Param5'    => '',
                    'Param6'    => '',
                    'Param7'    => $store_id,
                    'Param8'    => $pay_type,
                    'Param9'    => '',
                    'Param10'   => '');
        $response['view'] = $this->supper_admin->call_procedure('proc_transaction', $param);
      } 
      
      $param=array('country_id' => '',
                  'state_name' => '',
                  'countrycode'=> 'success',
                  'row_id'     => '',
                  'act_mode'   => 'gettransactionsstore');
      $response['stores'] = $this->supper_admin->call_procedure('proc_state', $param);
      
      $param=array('country_id' => '',
                  'state_name' => '',
                  'countrycode'=> '',
                  'row_id'     => '',
                  'act_mode'   => 'fetchcycles');
      $response['cycles'] = $this->supper_admin->call_procedure('proc_state', $param);
      $this->load->view('helper/header');
      $this->load->view('transaction',$response);
  } 

  public function transactionsfetchfilter() {          
      $param=array('act_mode' => 'transaction_filter',
                  'Param'     => 'pending',
                  'Param1'    => '2016-12-20 00:00:00',
                  'Param2'    => '2016-12-22 00:00:00',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '2',
                  'Param8'    => 'Receivables',
                  'Param9'    => '',
                  'Param10'   => '');
      $response = $this->supper_admin->call_procedure('proc_transaction', $param);
  } 

function paymentgateway()
{
    $this->load->view('helper/header');
    $this->load->view('paymentprocess');
}



function paymentgateway1()
{
    $txn_id=$this->input->post('txn_id');
    $param= array('country_id' => '',
                  'state_name'=>$txn_id,
                  'countrycode'=>'',
                  'row_id'=>'',
                  'act_mode'=>'updatepaymentstatus' );
//p($param);exit;
    $response = $this->supper_admin->call_procedure('proc_state', $param);

  $this->session->set_flashdata('message', 'Your transaction has been successfully - '.$txn_id.' (Ref ID)!');
  //redirect(base_url().'admin/tax/paymentprocess');
  redirect(base_url().'admin/receipts/paymentsuccess');
}

function paymentsuccess()
{
    $this->load->view('helper/header');
    $this->load->view('paymentsuccess');
}



public function receiptsvalidity(){

      if($this->input->post('submit')){
        $this->form_validation->set_rules('validity_hours','Validity hours','trim|required|xss_clean');
        if($this->form_validation->run() != false){
          $param=array( 'act_mode' => 'insertvalidity',
                  'row_id'    => '',
                  'Param1'    => $this->input->post('validity_hours'),
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '');       
          $response=$this->supper_admin->call_procedure('proc_search',$param);
          $this->session->set_flashdata('message', 'Data has been inserted successfully!');
          redirect(base_url().'admin/receipts/receiptsvalidity');
        }
      }//if submit
      $param=array( 'act_mode' => 'fetchvalidities',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '');       
      $response['vieww']=$this->supper_admin->call_procedure('proc_search',$param); 
      $this->load->view('helper/header');
      $this->load->view('helper/sidebar');
      $this->load->view('receiptsvalidity',$response);
      $this->load->view('helper/footer');
  }

  public function payment_payables()
  {

     $param = array(
                          'act_mode' => 'getratailertransactionpayables', 
                          'row_id' => '',
                          'param1' => '',
                          'param2' => '',
                          'param3' => '',
                          'param4' => '',
                          'param5' => '',
                          'param6' => '',
                          'param7' => '',
                          'param8' => '',
                          'param9' => '',
                          'param10' => '',
                          'param11' => '',
                          'param12' => '',
                          'param13' => '',
                          'param14' => '',
                          'param15' => '',
                          'param16' => '',
                          'param17' => '',
                          'param18' => ''
                        );      
        //p($param3); 
          $res = $this->supper_admin->call_procedure('proc_procedure4', $param);

          $arrpop=array();
          foreach ($res as $key => $value) {
            //p($value->retailer_id);
         // }
          //exit;
      $param3 = array(
                          'act_mode' => 'transactionpayables', 
                          'row_id' => $value->retailer_id,
                          'param1' => '',
                          'param2' => '',
                          'param3' => '',
                          'param4' => '',
                          'param5' => '',
                          'param6' => '',
                          'param7' => '',
                          'param8' => '',
                          'param9' => '',
                          'param10' => '',
                          'param11' => '',
                          'param12' => '',
                          'param13' => '',
                          'param14' => '',
                          'param15' => '',
                          'param16' => '',
                          'param17' => '',
                          'param18' => ''
                        );      
        //p($param3); 
          $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param3);
 //p($res2);
 foreach ($res2 as $key => $val) {
  //p($value);
  array_push($arrpop, $val);
}

}

$finalExcelArr = array('Process Id','Beneficiary Account Number','Instrument Amount','Customer Name','Beneficiary Name','IFSC Code','Bene Bank Name','Email ID');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Payment Details');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                      
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
            foreach ($arrpop as $key => $valu) {  
           
             
            $newvar = $j+$key;
            

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $valu->txn_id);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $valu->acc_number);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $valu->amt);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $valu->name);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $valu->accholder_name);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $valu->ifsc_code);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $valu->bank_name);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $valu->email);
            
                 
            }
          }
      

          $filename='PaymentDetails.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');



  }

function checkbill()
{
  $checkbill=$this->input->post('billno');
  if($this->input->post('stid')=='')
  {
  $storeid=$this->input->post('sitid');
  }
  else{
    $storeid=$this->input->post('stid');
  }

 $param =array('useremail'=>$checkbill,
              'userpassword'=>$storeid,
              'act_mode'=>'checkbill',
              'row_id'=>'');

 $res = $this->supper_admin->call_procedureRow('proc_Adminlogin', $param);
echo $res->checkbill;

}

function test11()
{
 $param= array(
                'act_mode' => 'getrefereluser', 
                'p_t_imgname' => '',
                'p_t_path' => '',
                'imgtype' => '',
                'row_id' => '18',
                'imagename' => ''
                      ); 
    //p($param); 
          $get = $this->supper_admin->call_procedureRow('proc_img',$param);

        $custid=$get->userid;
        $uname=$get->t_FName;
           //p($custid); 

        $no_url=base_url().'api/mainapi/userreferal/format/json?custid='.$custid.'&uname='.urlencode($uname);

          #file_get_contents($no_url);

}

public function autoupdatereceipts() {

    $param=array( 'act_mode' => 'getautoapproveddata',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $response['autoapproved'] = $this->supper_admin->call_procedure('proc_other', $param);
      //p($response['autoapproved']); 

    foreach ($response['autoapproved'] as $key => $value2) 
    {
     //echo 'hi'; 
    $param=array( 'act_mode' => 'getdata',
                  'row_id'    => $value2->re_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
       //p($param);           
      $response1 = $this->supper_admin->call_procedureRow('proc_other', $param);

    
      // if($this->input->post('storeid')!='')
      // {
      //     $storeidd=$this->input->post('storeid');
      // }
      // else
      // {
          $storeidd=$response1->store_id;
          
      //}

      $param=array( 'act_mode' => 'getdatadeal',
                  'row_id'    => $storeidd,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $store_cbper = $this->supper_admin->call_procedureRow('proc_other', $param);

      $param2=array( 'act_mode' => 'getreferaldata',
                  'row_id'    => $response1->cust_id,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $referal = $this->supper_admin->call_procedureRow('proc_other', $param2);  

      $param1=array( 'act_mode' => 'getalltax',
                  'row_id'    => '',
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
      $res = $this->supper_admin->call_procedure('proc_other', $param1);

  
     
      foreach ($res as $key => $value) 
      {
         if($value->commision_title=='Tax')
         {
              $tax=$value->commision_title;
              $taxamt=$value->commission_per;
         }
          if($value->commision_title=='MPC Margin')
         {
              $mpc=$value->commision_title;
              $mpcamt=$value->commission_per;
         }
          if($value->commision_title=='MLM')
         {
              $mlm=$value->commision_title;
              $mlmamt=$value->commission_per;
         }

      }

    
       //$bill_update_amt=$this->input->post('billamt');
       //$bill_update_amt1=$this->input->post('billamtpop');

       $bill_update_amt=$response1->bill_update_amt;
       $bill_update_amt1=$response1->popcoins_amt;

      $sname=$store_cbper->s_name;
      $smob=$store_cbper->s_contactno;
      

      $tax_mpc=$taxamt+$mpcamt;
      $cbper=$store_cbper->commission;
      if($cbper==0)
      {
          //$cashback=$response1->bill_update_amt;
          $cashback=0;
      }
      else
      {
          //$cashback=($response1->bill_update_amt*$cbper)/100;
              $cashback=($bill_update_amt*$cbper)/100;
      }
      
      $mpcdata=($cashback*$tax_mpc)/100;

      $st=($cashback*$taxamt)/100;
      $margintax=($cashback*$mpcamt)/100;
      $reject_tax=$tax_mpc+$mlmamt;
//p($bill_update_amt);
      /*----------------------MLM Process ------------------------*/
  

//echo 'ji';
      if(empty($referal))
      {
          $act='noreferal';
          $mlmd=$mlmamt;
          $retcb=($cashback*$mlmd)/100;
          $retype1='M';
          $reid1='1';
          $retype2='';
          $reid2='';
          $customer_cb=$cashback-($mpcdata+$retcb);
          $mpc_cb=$mpcdata+$retcb;
      }
      else
      {
          if($referal->referal==1)
          { 
              $act='referal_m_r';
              $mlmd=$mlmamt/2;
              $retcb=($cashback*$mlmd)/100;
              $retcb1=($cashback*$mlmamt)/100;
              $retype1=$referal->fromtype;
              $reid1=$referal->from_id;
              $retype2=$referal->fromtype2;
              $reid2=$referal->from_id2;
              $customer_cb=$cashback-($mpcdata+$retcb1);
              $mpc_cb=$mpcdata+$retcb;
          }
          else
          {
              if($referal->fromtype=='R')
              {
                  $act='referal_r_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;
              }
              else
              {
                  $act='referal_c_c';
                  $mlmd=$mlmamt/2;
                  $retcb=($cashback*$mlmd)/100;
                  $retcb1=($cashback*$mlmamt)/100;
                  $retype1=$referal->fromtype;
                  $reid1=$referal->from_id;
                  $retype2=$referal->fromtype2;
                  $reid2=$referal->from_id2;
                  $customer_cb=$cashback-($mpcdata+$retcb1);
                  $mpc_cb=$mpcdata;


                  
              }
          }
      }

       /*----------------------MLM Process end------------------------*/


 $parameter=array( 'act_mode' => $act,
                  'row_id'    => $value2->re_id,
                  'Param1'    => $response1->bill_uid,
                  'Param2'    => $response1->cust_id,
                  'Param3'    => $storeidd,//$response1->store_id,
                  'Param4'    => $response1->date_of_purchase,//$response1->date_of_purchase,
                  'Param5'    => $response1->bill_no,//$response1->bill_no,
                  'Param6'    => $response1->bill_amt,
                  'Param7'    => $response1->paid_by,
                  'Param8'    => $bill_update_amt,//$response1->bill_update_amt,
                  'Param9'    => $response1->bill_img,
                  'Param10'   => 'autoapproved',
                  'Param11'    => $cbper,
                  'Param12'    => $cashback,
                  'Param13'    => 'Approved',
                  'Param14'    => $taxamt,
                  'Param15'    => $mpcamt,
                  'Param16'    => $mlmd,
                  'Param17'    => $retype1,
                  'Param18'    => $reid1,
                  'Param19'    => $retype2,
                  'Param20'    => $reid2,
                  'Param21'    => $customer_cb,
                  'Param22'    => $mpc_cb,
                  'Param23'    => 0,
                  'Param24'    => $response1->re_createdon,
                  'Param25'    => $retcb,
                  'Param26'    => $st,
                  'Param27'    => $margintax,
                  'Param28'    => $bill_update_amt1,
                  'Param29'    => '',
                  'Param30'    => '',
                  'Param31'    => $response1->sent_to_ret_date,
                  'Param32'    => $reject_tax);
                  

                 

             //p($parameter);
     //$response = $this->supper_admin->call_procedureRow('proc_other1', $parameter);
     $response = $this->supper_admin->call_procedureRow('proc_receipts', $parameter);
      //exit;

     //for mail
            $param1=array( 'act_mode' => 'getemails',
                  'row_id'    => $storeidd,
                  'Param1'    => '',
                  'Param2'    => '',
                  'Param3'    => '',
                  'Param4'    => '',
                  'Param5'    => '',
                  'Param6'    => '',
                  'Param7'    => '',
                  'Param8'    => '',
                  'Param9'    => '',
                  'Param10'   => '',
                  'Param11'    => '',
                  'Param12'    => '',
                  'Param13'    => '',
                  'Param14'    => '',
                  'Param15'    => '',
                  'Param16'    => '',
                  'Param17'    => '',
                  'Param18'    => '',
                  'Param19'    => '',
                  'Param20'    => '');  
                  
            $emails = $this->supper_admin->call_procedure('proc_other', $param1);

            //p($emails->remail);
            //p($emails->mgremail); 
            $emailid=$emails->remail;
            $stremailid=$emails->mgremail;
            $receipt=$response1->bill_uid;
            $billamnt=$response1->bill_amt;
            $content='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{background-color:#16369d; width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:37%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{    width: 100%;
    float: left;color:#444;}
.body_contain ul.top-list{ padding:0px;list-style-type:none;}
.body_contain ul.top-list li:first-child{font-size:16px; color:#444; line-height:22px;}
.body_contain ul.top-list li.second{font-size:13px; color:#444; line-height:20px;}
.body_contain ul.top-list li:last-child{font-size:13px; color:#444; line-height:20px;}
.content{ width:100%; float:left;}
.content .left_div{ width:60%; float:left;}
.content .left_div ul{ padding:0px; width:100%; float:left;list-style-type:none;}
.content .left_div ul li{ width:100%; float:left;}
.content .left_div ul li .list-img{ float:left; padding:0 10px 0 0;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left; background:#16369d; margin-top:15px;}
.left-f{padding-left:10px; color:#FFF; width:60%; float:left;}
.right-f{float:right; color:#fff; padding-right:5px;}
ul{ padding:0px;}
ul li{font-size:13px; color:#404040; list-style-type:none; padding:5px 0;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}

}
</style>
</head>

<body>
<div class="main_div">
  <div class="header">
    <div class="container">
        <div class="logo">
            <img src="https://mypopcoins.com/partner/assets/webapp/img/logoNew.png">
      </div>
        <div class="right_div">
        <div class="right-txt">AAA Ltd.<br> A 12, FIEE Complex,<br> Okhla Industrial Area - Phase II, <br> New                 Delhi - 110020 <br>Website : <a href="https://www.mypopcoins.com">www.mypopcoins.com</a></div>
        </div>
    </div>
  </div>

<div class="body_contain">
  <div class="container">
      <ul>
          <li>Welcome to MyPopCoins</li>
            <li>Your receipt is autoapproved</li>
          <li>Details of the receipt is :-</li>
        </ul>     
        <div class="user-form">
          <table>
            <tr>
              <td class="left">Receipt UID :</td>
                <td class="right">'.$receipt.'</td>
            </tr>
            <tr>
              <td class="left">Bill Amount :</td>
                <td class="right">'.$billamnt.'</td>
            </tr>
           
          </table>
        </div>    
    
   </div>
</div>  
<div class="footer">
  <div class="container">
    <p class="left-f">Looking forward to do Business with You..!!</p>
    <p class="right-f">MindzShop Team.</p>
    </div>
</div>    
    
</div>

</body>
</html>
                ';
                  
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('support@mindzshop.com', 'MyPopCoins');
                  $this->email->to('support@mindzshop.com');
                  $this->email->cc($emailid);
                  $this->email->cc($stremailid);
                  //$this->email->cc('priya@mindztechnology.com');
                  $this->email->subject('Receipt Autoapproved');
                  $this->email->message($content);
                  #$this->email->send();
    }
  }


  public function pay_sms_mail() {

      $mobiles= '9997785061';
      $ret= 'RET(534)POP';
      $sto= 'STO(544)POP';
      $cmp= 'Google Pvt Ltd.';
      $scmp= 'IBM Store';
      $fd= '12/09/2017';
      $td= '22/09/2017';
      $pd= '29/09/2017';
      $sum= '39.59999999999999';


         $msg=urlencode('DEAR MPC Partner, Bill Generated for Retailer ID: '.$ret.' ('.$cmp.') from '.$fd.' to '.$td.'. Total Due Amt: Rs '.round($sum,2).'/-. Pymt due on '.$pd);

           $data11=SMSURL;
           $url=$data11.$mobiles.'&message='.$msg;  
         // file_get_contents($url);
             

    $content='<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:42%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{width: 100%;float: left;color:#444;background:#ffe5e6;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left;}
.left-f{padding-left:10px; color:#000; width:60%; float:left;}
.right-f{float:right; color:#000; padding-right:5px;}
.inner-left .para-1{line-height:normal; font-weight: 600;}
.inner-left .para-2{line-height:normal; font-weight: 600;}
.para-t{padding-left:10px; margin-top:25px; width:100%; float:left; font-size:13px; color:#404040;}
.inner{ width:100%; float:left;}
.inner-left{width:45%; height:auto; float:left; padding:5px;}
.inner-right{width:45%; height:auto; float:right; padding:5px;}
.inner-right .para-1{line-height:normal; font-weight: 600; text-align:right;}
.content-area{width: 100%; height: auto; float: left; padding-top: 30px;}
.content-area-left{width:30%; height:auto; float:left;}
.content-area-right{width:65%; height:auto; float:right;}
.content-area-right h2{font-size: 16px; font-weight: 600; color: #ff0004;}
.content-area-right h3{font-size: 18px; padding-top: 5px;line-height: 2px;}
content-area-right .para-b1{ font-size: 11px; font-weight: 600; color: #6b6868; padding: 10px 0px;}
content-area-right .para-b2{color: red; font-weight: 600; font-size: 13px; text-align:center;}
ul li{ list-style-type:none; display:inline-block;}
ul li a img{width: 33px; height: 33px; text-decoration: none;border-radius: 5px;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}
ul{ width:100%; text-align:center;padding: 0;}
.content-area-left{width:100%; height:auto; float:left; text-align:center;}
.content-area-right{width:100%; height:auto; float:left;}

}
</style>
</head>
<body>
<div class="main_div">
  <div class="header">
      <div class="logo"><img src="'.base_url().'assets/webapp/img/logoNew.png"></div>
        <div class="right_div">
            <ul>
<li><a href="https://twitter.com/MyPopCoins"><img src="https://mypopcoins.com/partner/assets/img/social1.png" alt="" /></a></li>

<li><a href="https://www.youtube.com/channel/UCny71U-VfGi5G9k8hs27bkg"><img src="https://mypopcoins.com/partner/assets/img/social2.png" alt=""/></a></li>

<li><a href="https://www.facebook.com/mypopcoins/"><img src="https://mypopcoins.com/partner/assets/img/social3.png" alt=""/></a></li>

<li><a href="https://www.linkedin.com/in/mypopcoins"><img src="https://mypopcoins.com/partner/assets/img/social4.png" alt=""/></a></li>
</ul>
    </div> 
  </div>
<div class="body_contain">
    <div class="container">
        <div class="inner">
            <div class="inner-left">
                <p class="para-1">Retailer ID: '.$ret.'</p>
                <p class="para-2">Retailer Name: '.$cmp.'</p>
            </div>
            <div class="inner-right">
                <p class="para-1">Due Date: '.$pd.'</p>
            </div>
            <div class="content-area">
                <div class="content-area-left">
                        <img src="https://mypopcoins.com/partner/assets/img/coins.jpg" alt="" />
                </div>
<div class="content-area-right">
<h2>Dear MPC Partner,</h2>
<h3>Total Amount Due <span style="color:red;"><i class="fa fa-inr" aria-hidden="true"></i> '.round($sum,2).'/-</span></h3>
<p class="para-b1">*Please make the payment before due date to avoid Penalties.</p>
<p class="para-b2">(From:'.$fd.' to '.$td.')</p>
</div>
</div>
        </div>
    </div>    
</div>

<div class="footer">
  <p class="left-f">Looking forward to do Business with You..!!</p>
  <p class="right-f">MindzShop Team.</p>
</div>    
    
</div>

</body>
</html>
';
                   $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('support@mindzshop.com', 'MyPopCoins');
                  $this->email->to('pavankmr703@gmail.com');
                  $this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Process Payment');
                  $this->email->message($content);
                  #$this->email->send();



                 $msg1=urlencode('DEAR MPC Partner, Bill Generated for Store ID: '.$sto.' ('.$scmp.') from '.$fd.' to '.$td.'. Total Due Amt: Rs '.round($sum,2).'/-. Pymt due on '.$pd);

                $data11=SMSURL;
           $url=$data11.$mobiles.'&message='.$msg1;  
          //file_get_contents($url);
             



$content1='<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:42%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{width: 100%;float: left;color:#444;background:#ffe5e6;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left;}
.left-f{padding-left:10px; color:#000; width:60%; float:left;}
.right-f{float:right; color:#000; padding-right:5px;}
.inner-left .para-1{line-height:normal; font-weight: 600;}
.inner-left .para-2{line-height:normal; font-weight: 600;}
.para-t{padding-left:10px; margin-top:25px; width:100%; float:left; font-size:13px; color:#404040;}
.inner{ width:100%; float:left;}
.inner-left{width:45%; height:auto; float:left; padding:5px;}
.inner-right{width:45%; height:auto; float:right; padding:5px;}
.inner-right .para-1{line-height:normal; font-weight: 600; text-align:right;}
.content-area{width: 100%; height: auto; float: left; padding-top: 30px;}
.content-area-left{width:30%; height:auto; float:left;}
.content-area-right{width:65%; height:auto; float:right;}
.content-area-right h2{font-size: 16px; font-weight: 600; color: #ff0004;}
.content-area-right h3{font-size: 18px; padding-top: 5px;line-height: 2px;}
content-area-right .para-b1{ font-size: 11px; font-weight: 600; color: #6b6868; padding: 10px 0px;}
content-area-right .para-b2{color: red; font-weight: 600; font-size: 13px; text-align:center;}
ul li{ list-style-type:none; display:inline-block;}
ul li a img{width: 33px; height: 33px; text-decoration: none; border-radius: 5px;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}
ul{ width:100%; text-align:center;padding: 0;}
.content-area-left{width:100%; height:auto; float:left; text-align:center;}
.content-area-right{width:100%; height:auto; float:left;}

}
</style>
</head>
<body>
<div class="main_div">
  <div class="header">
      <div class="logo"><img src="'.base_url().'assets/webapp/img/logoNew.png"></div>
        <div class="right_div">
            <ul>
<li><a href="https://twitter.com/MyPopCoins"><img src="https://mypopcoins.com/partner/assets/img/social1.png" alt="" /></a></li>

<li><a href="https://www.youtube.com/channel/UCny71U-VfGi5G9k8hs27bkg"><img src="https://mypopcoins.com/partner/assets/img/social2.png" alt=""/></a></li>

<li><a href="https://www.facebook.com/mypopcoins/"><img src="https://mypopcoins.com/partner/assets/img/social3.png" alt=""/></a></li>

<li><a href="https://www.linkedin.com/in/mypopcoins"><img src="https://mypopcoins.com/partner/assets/img/social4.png" alt=""/></a></li>
</ul>
    </div> 
  </div>
<div class="body_contain">
    <div class="container">
        <div class="inner">
            <div class="inner-left">
                <p class="para-1">Store ID: '.$sto.'</p>
                <p class="para-2">Store Name: '.$scmp.'</p>
            </div>
            <div class="inner-right">
                <p class="para-1">Due Date: '.$pd.'</p>
            </div>
            <div class="content-area">
                <div class="content-area-left">
                        <img src="https://mypopcoins.com/partner/assets/img/coins.jpg" alt="" />
                </div>
<div class="content-area-right">
<h2>Dear MPC Partner,</h2>
<h3>Total Amount Due <span style="color:red;"><i class="fa fa-inr" aria-hidden="true"></i> '.round($sum,2).'/-</span></h3>
<p class="para-b1">*Please make the payment before due date to avoid Penalties.</p>
<p class="para-b2">(From:'.$fd.' to '.$td.')</p>
</div>
</div>
        </div>
    </div>    
</div>

<div class="footer">
  <p class="left-f">Looking forward to do Business with You..!!</p>
  <p class="right-f">MindzShop Team.</p>
</div>    
    
</div>

</body>
</html>
';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('support@mindzshop.com', 'MyPopCoins');
                  $this->email->to('rabjeetkmr@gmail.com');
                  $this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Process Payment');
                  $this->email->message($content1);
                  #$this->email->send();

  }





}
?>
