<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Vendor extends MX_Controller{

  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->load->library('upload');
    $this->userfunction->loginAdminvalidation();

  }

//---------------------- Add Vendor  -------------------------//
  public function addvendor(){
  if($this->input->post('submit')){
  	$vendortype   = 'manufacturer';
  	$comname      = $this->input->post('comname');
  	$emailid      = $this->input->post('emailid');
  	$mphn         = $this->input->post('mphn');
  	$password     = $this->input->post('password');
  	$fname        = $this->input->post('fname');
  	$lasname      = $this->input->post('lasname');
  	//$brandid      = $this->input->post('brandid');
    $subcatidd      = $this->input->post('subcatidd');
    $countryid    = $this->input->post('countryid');
  	$stateid      = $this->input->post('stateid');
  	$cityid       = $this->input->post('cityid');
  	$vendoraddress= $this->input->post('vendoraddress');
    $pincode      = $this->input->post('pincode');
  	$pannumber    = $this->input->post('pannumber');
  	$accountholder= $this->input->post('accountholder');
  	$accountnumber= $this->input->post('accountnumber');
  	$banknumber   = $this->input->post('banknumber');
  	$branchname   = $this->input->post('branchname');
  	$IfscCode1    = $this->input->post('IfscCode1');
  	$neftdetail   = $this->input->post('neftdetail');
  	$AccountType  = $this->input->post('AccountType');
    if($this->input->post('gstno')){
    $cstnamber    = $this->input->post('gstno'); 
    $cstid='2';
    }
    else{
       $cstid='4';

    }
    $vatnumber = ''; 
    $tinid='6';
    
    $code         = 'manufacturer'; 
    $num          = rand(111111, 999999); 
    $uniqueCode   = $code . $num;
    //$recomende    = implode($brandid, ',');
    $recomende    = implode($subcatidd, ',');
    $checkimg2    = explode("\\",basename($_FILES['memchequefile']['name']));
    $checkimg     = $checkimg2[0];
    $panimg2      = explode("\\",basename($_FILES['mempanfile']['name']));
    $panimg       = $panimg2[0];
    $cstimg2      = explode("\\",basename($_FILES['memgstfile']['name']));
    $cstimg       = $cstimg2[0];

    $field_name   = 'memchequefile';
    $Imgdata1     = $this->do_upload($field_name);
    $field_name1  = 'mempanfile';
    $Imgdata2     = $this->do_upload($field_name1);
    $field_name3  = 'memgstfile';
    $Imgdata4     = $this->do_upload($field_name3);

    $prameter     = array(
                         'act_mode'   =>  'insert',
                         'row_id'     =>  '0',
                         'ventype'    =>  $vendortype,
                         'vencode'    =>  $uniqueCode,
                         'vcomp'      =>  $comname,
                         'vemail'     =>  $emailid,
                         'vcontact'   =>  $mphn,
                         'vpass'      =>  base64_encode($password),
                         'vname'      =>  $fname,
                         'vlast'      =>  $lasname,
                         'vcatid'     =>  '',
                         'vcountryid' =>  $countryid,
                         'vstateid'   =>  $stateid,
                         'vcityid'    =>  $cityid,
                         'vaddress'   =>  $vendoraddress,
                         'vpincode'   =>  $pincode,
                         'vpannumber' =>  '',
                         'vtinnumber' =>  $pannumber,
                         'accontname' =>  $accountholder,
                         'accnumber'  =>  $accountnumber,
                         'bankname'   =>  $banknumber,
                         'branchname' =>  $branchname,
                         'ifsccode'   =>  $IfscCode1,
                         'neftdetail' =>  $neftdetail,
                         'acctype'    =>  $AccountType,
                         'cstnumber'  =>  $cstnamber,
                         'vatnumber'  =>  $vatnumber,
                         'cstidd'     =>  $cstid,
                         'tinidd'     =>  $tinid,
                         'vatcopy'    =>  '',
                         'cstcopy'    =>  time().str_replace(' ', '', $cstimg),
                         'chequecopy' =>  time().str_replace(' ', '', $checkimg),
                         'pancopy'    =>  time().str_replace(' ', '', $panimg)
                         );
    
    $record['parentCatdata'] = $this->supper_admin->call_procedureRow('proc_vendorlogin',$prameter);
    $prameterubrand = array('act_mode'=>'insertmanchildcategory','row_id'=>$record['parentCatdata']->id,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>$recomende,'vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'  =>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    $record['brandupdate'] = $this->supper_admin->call_procedure('proc_vendorlogin',$prameterubrand);
    $this->session->set_flashdata("message", "Your information was successfully Saved");
    redirect("admin/vendor/viewvendor");

  }

  $parameterbv =array('act_mode'=>'manbrandview','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $record['viewmanbrand']  = $this->supper_admin->call_procedure('proc_manproduct',$parameterbv);
  $parameter               = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
  $record['viewcountry']   = $this->supper_admin->call_procedure('proc_geographic',$parameter);

  $parameterr                = array('act_mode'=>'parecatview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $record['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr);
  
  $this->load->view('helper/header');
  $this->load->view('vendor/addvendor',$record);
 
 } 
 
//---------------------- City State  -------------------------//
 public function do_upload($field_name){
    $config['upload_path'] = './manufacturer-files/';
    $config['allowed_types'] = 'jpg|jpeg|gif|png|pdf';
    $config['max_size'] = '10000000';
    $config['file_name']  = time().str_replace(' ', '', $_FILES[$field_name]['name']);
    $this->upload->initialize($config);
     if ( ! $this->upload->do_upload($field_name)){
        $data['error'] = array('error' => $this->upload->display_errors());
        
    } else {
        $data['name'] = array('upload_data' => $this->upload->data());
    }
    return $data;
  }


 public function citystate(){
 	$stid               = $this->input->post('stid');
 	$parameter          = array('act_mode'=>'citystate','row_id'=>$stid,'counname'=>'','coucode'=>'','commid'=>'');
 	$response['vieww']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  $str                = '';

  foreach($response['vieww'] as $k=>$v){
      
    if($v->cityid == $this->input->post('sam_ide')){
        $str .= "<option selected value=".$v->cityid.">".$v->cityname."</option>";
    } else {
        $str .= "<option value=".$v->cityid.">".$v->cityname."</option>";
    }
  }
    echo $str;
 } 

//---------------------- View Vendor  -------------------------//
 public function viewvendor(){
     $this->userfunction->loginAdminvalidation();	
     if($this->input->post('submit')){
      foreach($this->input->post( 'attdelete') as $key => $value) {
        $parameter          = array('act_mode'=>'delete','row_id'=>$value,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
        $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);
      }

      $this->session->set_flashdata("message", "Your information was successfully delete.");
      redirect("admin/vendor/viewvendor");
 }

 //--------------------------multiple ststus ------------------------------//
 if($this->input->post('submitstatus')){
   foreach ($this->input->post( 'attdelete') as $key => $value) {
    $status             = $this->input->post('attstatu')[$value];
    $act_mode           = $status=='A'?'activeven':'inactiveven';	
    $parameter          = array('act_mode'=>$act_mode,'row_id'=>$value,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);
   }
    $this->session->set_flashdata("message", "Your Status was successfully Updated.");
    redirect("admin/vendor/viewvendor");
 }
   $parameter           = array('act_mode'=>'viewmanxls','row_id'=>'','ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
   $response['vieww']   = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);


   //----------------  Download Excel ----------------------------//

      $parameter           = array('act_mode'=>'viewmanxls','row_id'=>'','ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
      $response['viewmanxls']   = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Vendor Code','Vendor Name','Vendor Email','Vendor Contact Number','Shop/Company Name','Address','Account Holder Name','Account Number','Account Type','Bank Name','Bank Branch Address','IFSC Code','NEFT Detail','GST Number','Pan Number','Created on');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Manufacturer Vendor Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($response['viewmanxls'] as $key => $value) {
             
            $newvar = $j+$key;
            if($value->AcountType=='C'){
              $actype='Current Account';
            }
            if($value->AcountType=='S'){
              $actype='Saving Account';
            }

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);


            array('Vendor Code','Vendor Name','Vendor Email','Vendor Contact Number','Shop/Company Name','Address','Account Holder Name','Account Number','Account Type','Bank Name','Bank Branch Address','IFSC Code','NEFT Detail','GST Number','Pan Number','Created on');
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->vendorcode);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->firstname.' '.$value->lastname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->compemailid);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->contactnumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->companyname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->vendoraddress);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->AccountHolderName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->AccountNo);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $actype);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->VenBankName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->venbranchname);            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->IfscCode);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->NeftDetail);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->cstvalue);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $value->panno);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->createdon);
            }
          }

          $filename='Manufacturer Vendor.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------// 
          //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/vendor/viewvendor?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter           = array('act_mode'=>'view','row_id'=>$page,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>$second,'vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    #$response['vieww']   = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);

    //----------------  end pagination ------------------------//  

   $this->load->view('helper/header');
   $this->load->view('vendor/viewvendor',$response);
 
}
 
//---------------------- Vendor Status Change  -------------------------//
public function vendorstatus($id){
$rowid         = $this->uri->segment(4);
$status        = $this->uri->segment(5);
$act_mode      = $status=='A'?'activeven':'inactiveven';	
$parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
$response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);
$this->session->set_flashdata("message", "Your information was successfully Saved");
redirect("admin/vendor/viewvendor");	
}

public function vendorstatusretail($id){
$rowid         = $this->uri->segment(4);
$status        = $this->uri->segment(5);
$act_mode      = $status=='A'?'activeven':'inactiveven';  
$parameter     = array('act_mode'=>$act_mode,
					'row_id'=>$rowid,
					'ventype'=>'',
					'vencode'=>'',
					'vcomp'=>'',
					'vemail'=>'',
					'vcontact'=>'',
					'vpass'=>'',
					'vname'=>'',
					'vlast'=>'',
					'vcatid'=>'',
					'vcountryid'=>'',
					'vstateid'=>'',
					'vcityid'=>'',
					'vaddress'=>'',
					'vpincode'=>'',
					'vpannumber'=>'',
					'vtinnumber'=>'',
					'accontname'=>'',
					'accnumber'=>'',
					'bankname'=>'',
					'branchname'=>'',
					'ifsccode'=>'',
					'neftdetail'=>'',
					'acctype'=>'',
					'cstnumber'=>'',
					'vatnumber'=>'',
					'cstidd'=>'',
					'tinidd'=>'',
					'vatcopy'=>'',
					'cstcopy'=>'',
					'chequecopy'=>'',
					'pancopy'=>'');

$response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);
$this->session->set_flashdata("message", "Your information was successfully Saved");
redirect("admin/vendor/viewvendorretail");  
}

//---------------------- Vendor Delete -------------------------//
public function vendordelete($id){
  $parameter          = array('act_mode'=>'delete','row_id'=>$id,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
  $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);
  $this->session->set_flashdata("message", "Your information was successfully delete.");
  redirect("admin/vendor/viewvendor"); 	
}

public function vendordeleteretail($id){

	
  $parameter          = array('act_mode'=>'delete','row_id'=>$id,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
  $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);
  $this->session->set_flashdata("message", "Your information was successfully delete.");
  redirect("admin/vendor/viewvendorretail");  
}

//---------------------- Country State  -------------------------//
public function countrystate(){
$countryid          = $this->input->post('countryid');
$parameter          = array('act_mode'=>'countrystate','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
$response['vieww']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
$str                = '';

foreach($response['vieww'] as $k=>$v){   
  if($v->stateid==$this->input->post('samagamid'))
    {
        $str .= "<option selected value=".$v->stateid.">".$v->statename."</option>";
    } else {
        $str .= "<option value=".$v->stateid.">".$v->statename."</option>";
    }
  }
  echo $str;
} 

//---------------------- Vendor Update -------------------------//
public function vendorupdate($id){

  $parameter          = array('act_mode'=>'viewid','row_id'=>$id,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
  $response['vieww']  = $this->supper_admin->call_procedureRow('proc_vendorlogin',$parameter);
  if($this->input->post('submit')){
    	$vendortype   =  'manufacturer';
    	$comname      =  $this->input->post('comname');
    	$emailid      =  $this->input->post('emailid');
    	$mphn         =  $this->input->post('mphn');
    	$password     =  $this->input->post('password');
    	$fname        =  $this->input->post('fname');
    	$lasname      =  $this->input->post('lasname');
    	$countryid    =  $this->input->post('countryid');
    	$stateid      =  $this->input->post('stateid');
    	$cityid       =  $this->input->post('cityid');
    	$vendoraddress=  $this->input->post('vendoraddress');
      $pincode      =  $this->input->post('pincode');
    	$pannumber    =  $this->input->post('pannumber');
    	$accountholder=  $this->input->post('accountholder');
    	$accountnumber=  $this->input->post('accountnumber');
    	$banknumber   =  $this->input->post('banknumber');
    	$branchname   =  $this->input->post('branchname');
    	$IfscCode1    =  $this->input->post('IfscCode1');
    	$neftdetail   =  $this->input->post('neftdetail');
    	$AccountType  =  $this->input->post('AccountType');
    	$subcatidd      = $this->input->post('subcatidd');
    	/*$oldbrandids    =  $response['vieww']->brandid;
      $newbrandid     =  $this->input->post('brandidd');*/
//p($newbrandid);exit;
    if($this->input->post('gstno')){
      $cstnamber    = $this->input->post('gstno'); 
      $cstid='2';
    } else {
      $cstid='4';
    }
    $vatnumber = ''; 
    $tinid='6';

    $code         = 'manufacturer'; 
    $num          = rand(111111, 999999); 
    $uniqueCode   = $code . $num;
    $recomende    = implode($subcatidd, ',');
    $checkimg2    = explode("\\",basename($_FILES['memchequefile']['name']));
    $checkimg1     = $checkimg2[0];
    if($checkimg1){
    $checkimg =  time().str_replace(' ', '', $checkimg1);
    }
    else{
     $checkimg =  $this->input->post('memchequefileold');
    }   
    
    $panimg2      = explode("\\",basename($_FILES['mempanfile']['name']));
    $panimg1       = $panimg2[0];
    if($panimg1){
    $panimg =  time().str_replace(' ', '', $panimg1);
    }
    else{
     $panimg =  $this->input->post('mempanfileold');
    } 
    $tinimg =  '';
    
    $cstimg2      = explode("\\",basename($_FILES['memgstfile']['name']));
    $cstimg1       = $cstimg2[0];
    if($cstimg1){
    $cstimg =  time().str_replace(' ', '', $cstimg1);
    }
    else{
     $cstimg =  $this->input->post('memgstfileold');
    }
    $field_name   = 'memchequefile';
    $Imgdata1     = $this->do_upload($field_name);
    $field_name1  = 'mempanfile';
    $Imgdata2     = $this->do_upload($field_name1);
    $field_name3  = 'memgstfile';
    $Imgdata4     = $this->do_upload($field_name3);
  	
    /*if(empty($newbrandid)){
  		$updatebrandid  =   $oldbrandids;
    } 
  	else{
      $newbrandidss= implode(',', $newbrandid); 
  		if(empty($oldbrandids)){
        $updatebrandid   = $newbrandidss;
      } else {
        $updatebrandid   = $newbrandidss.','.$oldbrandids;
      }
    
  	}*/
      $prameter      = array(
         'act_mode'  =>  'update',
         'row_id'    =>  $id,
         'ventype'   =>  $pannumber,
         'vencode'   =>  $uniqueCode,
         'vcomp'     =>  $comname,
         'vemail'    =>  $emailid,
         'vcontact'  =>  $mphn,
         'vpass'     =>  $password,
         'vname'     =>  $fname,
         'vlast'     =>  $lasname,
         'vcatid'    =>  '',
         'vcountryid'=> $countryid,
         'vstateid'  =>  $stateid,
         'vcityid'   =>  $cityid,
         'vaddress'  =>  $vendoraddress,
         'vpincode'  =>  $pincode,
         'vpannumber'=> '',
         //'vtinnumber'=> $tinnumber,
         'vtinnumber'=> '',
         'accontname'=> $accountholder,
         'accnumber' =>  $accountnumber,
         'bankname'  =>  $banknumber,
         'branchname'=> $branchname,
         'ifsccode'  =>  $IfscCode1,
         'neftdetail'=> $neftdetail,
         'acctype'   =>  $AccountType,
         'cstnumber' =>  $cstnamber,
         'vatnumber' =>  $vatnumber,
         'cstidd'    =>  $cstid,
         'tinidd'    =>  $tinid,
         'vatcopy'   =>  $tinimg,
         'cstcopy'   =>  $cstimg,
         'chequecopy'=>  $checkimg,
         'pancopy'   =>  $panimg);
      
    $record['parentCatdata'] = $this->supper_admin->call_procedure('proc_vendorlogin',$prameter);
    $prameterubrand = array('act_mode'=>'insertmanchildcategory','row_id'=>$id,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>$recomende,'vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'  =>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    $record['brandupdate'] = $this->supper_admin->call_procedure('proc_vendorlogin',$prameterubrand);
    $this->session->set_flashdata("message", "Your information was successfully Update");
    redirect("admin/vendor/viewvendor");

  }
  
  $parameterbv =array('act_mode'=>'manbrandview','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $response['viewmanbrand']  = $this->supper_admin->call_procedure('proc_manproduct',$parameterbv);

  $parameter          = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
  $response['viewcountry']   = $this->supper_admin->call_procedure('proc_geographic',$parameter);

  $parameterbv = array('act_mode'=>'viewmancat','n_email'=>'','oldpassword'=>$response['vieww']->brandid,'row_id'=>'');
    $response['existmanbrand']  = $this->supper_admin->call_procedure('proc_manpasswordcheck',$parameterbv);

  $parameterr                = array('act_mode'=>'parecatview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $response['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr);



  $prameterscat = array('act_mode'=>'get_vendor_sub_category','row_id'=>'','ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>$response['vieww']->catid,'vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'  =>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    $vendor_subcat = $this->supper_admin->call_procedure('proc_vendorlogin',$prameterscat);
    
    $vendor_subcatid=array();
    foreach ($vendor_subcat as $key => $value) {
      array_push($vendor_subcatid, $value->parentid);
    }
    $response['vendor_subcategory']=array_unique($vendor_subcatid);
  

  $prametemcat = array('act_mode'=>'get_vendor_main_category','row_id'=>'','ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>implode(',', $response['vendor_subcategory']),'vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'  =>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    $vendor_maincat = $this->supper_admin->call_procedure('proc_vendorlogin',$prametemcat);

    $vendor_maincatid=array();
    foreach ($vendor_maincat as $key => $value) {
      array_push($vendor_maincatid, $value->parentid);
    }
    $response['vendor_maincategory']=array_unique($vendor_maincatid);

  $this->load->view('helper/header');
  $this->load->view('vendor/editvendor',$response);
 	
}


public function vendorupdateretail($id){

  $parameter          = array('act_mode'=>'viewid','row_id'=>$id,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
  $response['vieww']  = $this->supper_admin->call_procedureRow('proc_vendorlogin',$parameter);
  if($this->input->post('submit')){
      $vendortype   =  $this->input->post('typeid');
      $comname      =  $this->input->post('comname');
      $emailid      =  $this->input->post('emailid');
      $mphn         =  $this->input->post('mphn');
      $password     =  $this->input->post('password');
      $fname        =  $this->input->post('fname');
      $lasname      =  $this->input->post('lasname');
      $countryid    =  $this->input->post('countryid');
      $stateid      =  $this->input->post('stateid');
      $cityid       =  $this->input->post('cityid');
      $vendoraddress=  $this->input->post('vendoraddress');
      $pincode      =  $this->input->post('pincode');
      $pannumber    =  $this->input->post('pannumber');
      //$tinnumber    =  $this->input->post('tinnumber');
      $accountholder=  $this->input->post('accountholder');
      $accountnumber=  $this->input->post('accountnumber');
      $reaccnumber  =  $this->input->post('reaccnumber');
      $banknumber   =  $this->input->post('banknumber');
      $branchname   =  $this->input->post('branchname');
      $IfscCode1    =  $this->input->post('IfscCode1');
      $neftdetail   =  $this->input->post('neftdetail');
      $AccountType  =  $this->input->post('AccountType');
      /*$cstnamber    =  $this->input->post('cstno');
      $vatnumber    =  $this->input->post('vatno');*/
      //$oldcatids    =  $this->input->post('vencatid');
      //$newcatid     =  $this->input->post('catidd');

      $oldbrandids    =  $response['vieww']->brandid;
      $newbrandid     =  $this->input->post('brandidd');

      if($this->input->post('cstno')){
    $cstnamber    = $this->input->post('cstno'); 
    $cstid='2';
    }
    else{
       $cstid='4';

    }
    if($this->input->post('vatno')){
    $vatnumber    = $this->input->post('vatno'); 
    $tinid='1';
    }
    else{
       $tinid='6';

    }
    $code         = $this->input->post('typeid'); 
    $num          = rand(111111, 999999); 
    $uniqueCode   = $code . $num;
    $recomende    = implode($catid, ',');
    $checkimg2    = explode("\\",basename($_FILES['memchequefile']['name']));
    $checkimg1     = $checkimg2[0];
    if($checkimg1){
    $checkimg =  time().$checkimg1;
    }
    else{
     $checkimg =  $this->input->post('memchequefileold');
    }

    
    $panimg2      = explode("\\",basename($_FILES['mempanfile']['name']));
    $panimg1       = $panimg2[0];
    if($panimg1){
    $panimg =  time().$panimg1;
    }
    else{
     $panimg =  $this->input->post('mempanfileold');
    } 

    $tinimg2      = explode("\\",basename($_FILES['memvatfile']['name']));
    $tinimg1       = $tinimg2[0];
    if($tinimg1){
    $tinimg =  time().$tinimg1;
    }
    else{
     $tinimg =  $this->input->post('memvatfileold');
    }


    $cstimg2      = explode("\\",basename($_FILES['memcstfile']['name']));
    $cstimg1       = $cstimg2[0];
    if($cstimg1){
    $cstimg =  time().$cstimg1;
    }
    else{
     $cstimg =  $this->input->post('memcstfileold');
    }
    $field_name   = 'memchequefile';

    $Imgdata1     = $this->do_upload($field_name);
    $field_name1  = 'mempanfile';
    $Imgdata2     = $this->do_upload($field_name1);
    $field_name2  = 'memvatfile';
    $Imgdata3     = $this->do_upload($field_name2);
    $field_name3  = 'memcstfile';
    $Imgdata4     = $this->do_upload($field_name3);
    
    if(empty($newbrandid)){
      $updatebrandid  =   $oldbrandids;
    } 
    else{
      $newbrandidss= implode(',', $newbrandid); 
      if(empty($oldbrandids)){
        $updatebrandid   = $newbrandidss;
      } else {
        $updatebrandid   = $newbrandidss.','.$oldbrandids;
      }
    
    }
      
      $prameter      = array(
         'act_mode'  =>  'update',
         'row_id'    =>  $id,
         'ventype'   =>  $pannumber,
         'vencode'   =>  $uniqueCode,
         'vcomp'     =>  $comname,
         'vemail'    =>  $emailid,
         'vcontact'  =>  $mphn,
         'vpass'     =>  $password,
         'vname'     =>  $fname,
         'vlast'     =>  $lasname,
         'vcatid'    =>  '',
         'vcountryid'=> $countryid,
         'vstateid'  =>  $stateid,
         'vcityid'   =>  $cityid,
         'vaddress'  =>  $vendoraddress,
         'vpincode'  =>  $pincode,
         'vpannumber'=> '',
         //'vtinnumber'=> $tinnumber,
         'vtinnumber'=> '',
         'accontname'=> $accountholder,
         'accnumber' =>  $accountnumber,
         'bankname'  =>  $banknumber,
         'branchname'=> $branchname,
         'ifsccode'  =>  $IfscCode1,
         'neftdetail'=> $neftdetail,
         'acctype'   =>  $AccountType,
         'cstnumber' =>  $cstnamber,
         'vatnumber' =>  $vatnumber,
         'cstidd'     =>  $cstid,
         'tinidd'     =>  $tinid,
         'vatcopy'    =>  $tinimg,
         'cstcopy'    =>  $cstimg,
         'chequecopy' =>  $checkimg,
         'pancopy'    =>  $panimg);
      
    $record['parentCatdata'] = $this->supper_admin->call_procedure('proc_vendorlogin',$prameter);
    $prameterubrand = array('act_mode'=>'insertmanbrand','row_id'=>$id,'ventype'=>'','vencode'=>'','vcomp'=>$updatebrandid,'vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'  =>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    $record['brandupdate'] = $this->supper_admin->call_procedure('proc_vendorlogin',$prameterubrand);
    $this->session->set_flashdata("message", "Your information was successfully Update");
    redirect("admin/vendor/viewvendorretail");

  }
  /*$parameterr                = array('act_mode'=>'catview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $response['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr); */ 

  $parameterbv =array('act_mode'=>'manbrandview','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $response['viewmanbrand']  = $this->supper_admin->call_procedure('proc_manproduct',$parameterbv);

  $parameter          = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
  $response['viewcountry']   = $this->supper_admin->call_procedure('proc_geographic',$parameter);

  $parameterbv = array('act_mode'=>'viewmancat','n_email'=>'','oldpassword'=>$response['vieww']->brandid,'row_id'=>'');
    $response['existmanbrand']  = $this->supper_admin->call_procedure('proc_manpasswordcheck',$parameterbv);

  
//p($response['vieww']);exit;
  $this->load->view('helper/header');
  $this->load->view('vendor/editvendor',$response);
  
}

//---------------------- View Vendor Details -------------------------//
public function viewvendorretail(){
 $this->userfunction->loginAdminvalidation();	
   
 $parameter          =  array('act_mode'=>'viewreta','row_id'=>'','ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
 $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);

          //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/vendor/viewvendorretail?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter          =  array('act_mode'=>'viewreta','row_id'=>$page,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>$second,'vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    #$response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);

    //----------------  end pagination ------------------------//  

 $this->load->view('helper/header');
 $this->load->view('vendor/viewvendorretail',$response);
 
}

public function viewvendorretailsearch(){

  $parameter          =  array('act_mode'=>'viewretasearch','row_id'=>'','ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'%'.$_GET['searchboxretail'].'%','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);

  
  $this->load->view('helper/header');
  $this->load->view('vendor/viewvendorretail',$response);

}  


//---------------------- View Vendor Details -------------------------//
public function emilcheck(){
  $emailcheck   = $this->input->post('email');
  $ven_typeid   = $this->input->post('ven_typeid');

  $parameter    = array('act_mode'=>'ademailch','row_id'=>'','ventype'=>$ven_typeid,'vencode'=>'','vcomp'=>'','vemail'=>$emailcheck,'vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
  $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);
  //p($response['vieww']);
  
  $data = $response['vieww'][0]->coun;
  if($data==0){
    echo "false";
  }else{
    echo "true";
  }
}

//---------------------- Phone no check -------------------------//
public function phncheck(){
  $phcheck            = $this->input->post('mphn');
  $ven_typeid   = $this->input->post('ven_typeid');
  $parameter          = array('act_mode'=>'adphnch','row_id'=>'','ventype'=>$ven_typeid,'vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>$phcheck,'vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
  $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);
  $data               = $response['vieww'][0]->coun;
  if($data==0){
        echo "false";
  }else{
        echo "true";
  }
}

public function manageconsumer(){
  $this->userfunction->loginAdminvalidation();

  $parameter    = array('act_mode'=>'viewconsumer','row_id'=>'','ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>'','vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
  $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);

  //----------------  Download Newsletter Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Vendor Type','Consumer Code','Consumer Name','Consumer Email','Consumer Contact No.','Consumer State','Consumer City');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Consumer Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($response['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->vendortype);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->vendorcode);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->firstname.' '.$value->lastname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->compemailid);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->contactnumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->statename);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->cityname);
            }
          }

          $filename='Registered Consumers Listing.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Newsletter Excel ------------------------// 
          //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/vendor/manageconsumer?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter          =  array('act_mode'=>'viewconsumer','row_id'=>$page,'ventype'=>'','vencode'=>'','vcomp'=>'','vemail'=>'','vcontact'=>'','vpass'=>'','vname'=>'','vlast'=>'','vcatid'=>$second,'vcountryid'=>'','vstateid'=>'','vcityid'=>'','vaddress'=>'','vpincode'=>'','vpannumber'=>'','vtinnumber'=>'','accontname'=>'','accnumber'=>'','bankname'=>'','branchname'=>'','ifsccode'=>'','neftdetail'=>'','acctype'=>'','cstnumber'=>'','vatnumber'=>'','cstidd'=>'','tinidd'=>'','vatcopy'=>'','cstcopy'=>'','chequecopy'=>'','pancopy'=>'');
    $response['vieww']  = $this->supper_admin->call_procedure('proc_vendorlogin',$parameter);

    //----------------  end pagination ------------------------//  
  
  $this->load->view('helper/header');
  $this->load->view('vendor/viewconsumer',$response);

}

public function viewpaymentdetail($venid){

  $parameter=array('act_mode'=>'retailerpaymentdetail','row_id'=>'','ppy_orderid'=>'','ppy_userid'=>$venid,'ppy_paymentmode'=>'','ppy_amt'=>'','ppy_status'=>'');
  $record['orddetail'] = $this->supper_admin->call_procedure('proc_orderpartialpayment',$parameter);

  $parameterrem=array('act_mode'=>'remtotalretailerpay','row_id'=>'','ppy_orderid'=>'','ppy_userid'=>$venid,'ppy_paymentmode'=>'','ppy_amt'=>'','ppy_status'=>'');
  $record['totalrempay'] = $this->supper_admin->call_procedure('proc_orderpartialpayment',$parameterrem);

  $parameterman=array('act_mode'=>'manpartpaydetail','row_id'=>'','ppy_orderid'=>'','ppy_userid'=>$venid,'ppy_paymentmode'=>'','ppy_amt'=>'','ppy_status'=>'');
  $record['vendetail'] = $this->supper_admin->call_procedureRow('proc_orderpartialpayment',$parameterman);


  //----------------  Download Order Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
          $finalExcelArr = array('S.NO.','Retailer Name','Retailer Code','Order Number','Order Date','Order Status','Order Amount','Payment Type','Paid Amount');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Orders Payment Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
            $sno=1;
            foreach ($record['orddetail'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $sno);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, ucwords($record['vendetail']->firstname).' '.ucwords($record['vendetail']->lastname));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $record['vendetail']->vendorcode);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->OrderNumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->OrderDate);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->PaymentStatus);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, number_format($value->TotalAmt,1,".",""));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->pp_paymentmode);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->pp_ammount);
            $sno++;
            }
          }

          $filename='Retailer Orders Payment.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Order Excel ------------------------// 


  $this->load->view('helper/header');
  $this->load->view('vendor/viewretailpaydetail',$record);
}

public function manufactuerexcel(){

  $parameter =array('act_mode'=>'viewmanexcel','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['vieww']  = $this->supper_admin->call_procedure('proc_manproduct',$parameter);
  $parameterman =array('act_mode'=>'viewmanexcelfilter','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['viewwman']  = $this->supper_admin->call_procedure('proc_manproduct',$parameterman);
  if(!empty($_GET['filter_by']))
  { 
    $parametmandfil =array('act_mode'=>'viewfiltermanexcel','row_id'=>$_GET['filter_by'],'catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
    //p($parametmandfil);exit;
    $responce['vieww']  = $this->supper_admin->call_procedure('proc_manproduct',$parametmandfil);
  }

  $this->load->view('helper/header');
  $this->load->view('vendor/manexcel',$responce);
}

public function manufactuerzip(){

  $parameter =array('act_mode'=>'viewmanzip','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['vieww']  = $this->supper_admin->call_procedure('proc_manproduct',$parameter);
  $parameterman =array('act_mode'=>'viewmanzipfilter','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['viewwman']  = $this->supper_admin->call_procedure('proc_manproduct',$parameterman);
  if(!empty($_GET['filter_by']))
  { 
    $parametmandfil =array('act_mode'=>'viewfiltermanzip','row_id'=>$_GET['filter_by'],'catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
    //p($parametmandfil);exit;
    $responce['vieww']  = $this->supper_admin->call_procedure('proc_manproduct',$parametmandfil);
  }
  $this->load->view('helper/header');
  $this->load->view('vendor/manzip',$responce);
}

public function mandownloadexcel($id){

  $parameter =array('act_mode'=>'downloadmanexcel','row_id'=>$id,'catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['vieww']  = $this->supper_admin->call_procedureRow('proc_manproduct',$parameter);

  $filepath=FCPATH.'manproductexcel/'.$responce['vieww']->mex_filename;
  $name=$responce['vieww']->mex_filename;
  if (file_exists($filepath))
  {

    header('Content-Description: File Transfer');
    //header('Content-Type: application/vnd.ms-excel');
    header("Content-Type: application/force-download");
    //header("Content-type: application/xls"); 
    //header("Content-Type: application/octet-stream");
    //header("Content-Type: application/download");
    header("Content-Disposition: attachment; filename=\"" . $name . "\";");
    ob_end_clean();
    flush();
    readfile($filepath); //showing the path to the server where the file is to be download
    exit;
  }
   redirect("admin/vendor/manufactuerexcel");
}

public function mandownloadzip($id){

  $parameter =array('act_mode'=>'downloadmanexcel','row_id'=>$id,'catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['vieww']  = $this->supper_admin->call_procedureRow('proc_manproduct',$parameter);

  $filepath=FCPATH.'manproductimgzip/'.$responce['vieww']->mex_filename;

  if (file_exists($filepath))
    {
      $this->load->library('zip');
      $this->zip->read_file($filepath);
      $this->zip->download($responce['vieww']->mex_filename);
    }
    redirect("admin/vendor/manufactuerzip");
}

public function manufactuerrequest(){

  $parameter =array('act_mode'=>'viewadminmanrequest','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['vieww']  = $this->supper_admin->call_procedure('proc_manproduct',$parameter);
  $parameterfil =array('act_mode'=>'viewadminmanrequestfil','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['viewwman']  = $this->supper_admin->call_procedure('proc_manproduct',$parameterfil);

  //--------------- approve request ------------------
  if($this->input->post('apprequest')){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    
    foreach ($this->input->post('reqapp') as $key => $value) {
      $parameterstatus =array('act_mode'=>'adminmanreqstatus','row_id'=>$value,'catid'=>'','manid'=>$userid,'attrname'=>'approve','atttextname'=>'');
      
      $responce = $this->supper_admin->call_procedure('proc_manproduct',$parameterstatus);
    }
    
    $this->session->set_flashdata('message', 'Request Approved Successfully!');
    redirect('admin/vendor/manufactuerrequest');
    
  }
  //--------------- end approve request --------------


  if(!empty($_GET['filter_by']))
        { 
             $parameterfill =array('act_mode'=>'viewmanreqfill','row_id'=>'','catid'=>'','manid'=>$_GET['filter_by'],'attrname'=>'','atttextname'=>'');
             $responce['vieww'] = $this->supper_admin->call_procedure('proc_manproduct',$parameterfill);
          //----------------  start pagination configuration ------------------------// 

         $config['base_url']         = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
         $config['total_rows']       = count($responce['vieww']);
         $config['per_page']         = 50;
         $config['use_page_numbers'] = TRUE;

         $this->pagination->initialize($config);
         if($_GET['page']){
           $page         = $_GET['page']-1 ;
           $page         = ($page*50);
           $second       = $config['per_page'];
         }
         else{
           $page         = 0;
           $second       = $config['per_page'];
         }
         
         $str_links = $this->pagination->create_links();
         $responce["links"]  = explode('&nbsp;',$str_links );

          //----------------  end pagination configuration ------------------------// 
         
         $parameterfill =array('act_mode'=>'viewmanreqfill','row_id'=>$page,'catid'=>$second,'manid'=>$_GET['filter_by'],'attrname'=>'','atttextname'=>'');
         $responce['vieww'] = $this->supper_admin->call_procedure('proc_manproduct',$parameterfill);
        }else{
  
  //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/vendor/manufactuerrequest?";
      $config['total_rows']       = count($responce['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;
//p($config);exit;
     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce['links']  = explode('&nbsp;',$str_links);

    $parameter =array('act_mode'=>'viewadminmanrequest','row_id'=>$page,'catid'=>$second,'manid'=>'','attrname'=>'','atttextname'=>'');
    #$responce['vieww']  = $this->supper_admin->call_procedure('proc_manproduct',$parameter);
  }
    //----------------  end pagination ------------------------//  
  
  $this->load->view('helper/header');
  $this->load->view('vendor/viewmanrequest',$responce);
}


public function manufactuerexcelupdate(){

  $parameter =array('act_mode'=>'hm_viewmanexcelupdate','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['vieww']  = $this->supper_admin->call_procedure('proc_manproduct',$parameter);
  $parameterman =array('act_mode'=>'viewmanexcelfilterupdate','row_id'=>'','catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['viewwman']  = $this->supper_admin->call_procedure('proc_manproduct',$parameterman);
  if(!empty($_GET['filter_by']))
  { 
    $parametmandfil =array('act_mode'=>'hm_viewfiltermanexcelupdate','row_id'=>$_GET['filter_by'],'catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
    //p($parametmandfil);exit;
    $responce['vieww']  = $this->supper_admin->call_procedure('proc_manproduct',$parametmandfil);
  }

  $this->load->view('helper/header');
  $this->load->view('vendor/manexcelupdate',$responce);
}

public function mandownloadexcelupdate($id){

  $parameter =array('act_mode'=>'downloadmanexcelupdate','row_id'=>$id,'catid'=>'','manid'=>'','attrname'=>'','atttextname'=>'');
  $responce['vieww']  = $this->supper_admin->call_procedureRow('proc_manproduct',$parameter);

  $filepath=FCPATH.'manbulkproductexcels/'.$responce['vieww']->mexb_filename;
  $name=$responce['vieww']->mexb_filename;
  if (file_exists($filepath))
  {

    header('Content-Description: File Transfer');
    //header('Content-Type: application/vnd.ms-excel');
    header("Content-Type: application/force-download");
    //header("Content-type: application/xls"); 
    //header("Content-Type: application/octet-stream");
    //header("Content-Type: application/download");
    header("Content-Disposition: attachment; filename=\"" . $name . "\";");
    ob_end_clean();
    flush();
    readfile($filepath); //showing the path to the server where the file is to be download
    exit;
  }
   redirect("admin/vendor/manufactuerexcelupdate");
}

}// end class.
?>