<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Addmobsubcat extends MX_Controller{

//............. Default Construct function ............... //
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('upload');
    $this->userfunction->loginAdminvalidation();
    
  
  }// end function.

//............. Slider Manager ............... //
  public function addstationarysubcat(){
//p('hello ');exit();
    $parameters = array(
                    'act_mode'  => 'viewsub_catapp',
                    'row_id'    => '',
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );

  $record['viewsub_catapp'] = $this->supper_admin->call_procedure('proc_homebanner',$parameters);
//p($record['viewsub_catapp']);exit();

    $parameterr                = array('act_mode'=>'parecatviewapp','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $record['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 
    $parameterr                = array('act_mode'=>'cattype','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $record['cattype'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 



    if($this->input->post('submit')){

     $imgurl            = $this->input->post('imgurl');
     $imgslide          = preg_replace('/\s+/', '', $_FILES['imgslide']['name']);
          
     $field_name_img    = 'imgslide';
     $img_file          = $this->image_uploads($field_name_img);

     $allowedExts       = array("gif","jpeg","jpg","png");
     $temp              = explode(".",$imgslide);
     $extension         = end($temp);
     //echo $extension;exit();
     if (in_array($extension, $allowedExts)){

        $parameter      = array(
                            'act_mode'  => 'addsubcatapp',
                            'row_id'    => $this->input->post('type_id'),
                            'imagename' => time().$imgslide,
                            'imageurl'  => $this->input->post('ms_parant_id'),
                            'imgtype'   => $this->input->post('ms_catid'),
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status'=> ''
                          );
//p($parameter);exit();
        $record = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
        $this->session->set_flashdata('messag', 'Your information was successfully Saved.');
        redirect('admin/addmobsubcat/addstationarysubcat');
     }
     else{        
        $this->session->set_flashdata('messag', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
        redirect('admin/addmobsubcat/addstationarysubcat');
     }
    }

    $this->load->view('helper/header');
    $this->load->view('mobilesubcat/add_sub_stationary', $record);
  
    }
public function updatestationarysubcat($id) {
   //p(FCPATH);exit();

    $parameters     = array(
                        'act_mode'   => 'viewsubcat',
                        'row_id'     => $id,
                        'imagename'  => '',
                        'imageurl'   => '',
                        'imgtype'    => '',
                        'img_head'   => '',
                        'img_shead'  => '',
                        'img_status' => ''
                        );
    $record['view'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameters);
    
  $parameterr = array('act_mode'=>'childcatviewapp','row_id'=>$record['view']->ms_parent_id,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $record['childCatdata']  = $this->supper_admin->call_procedure('proc_category',$parameterr);  
  


    $parameterr  = array('act_mode'=>'parecatviewapp','row_id'=>$record['view']->ms_type_id,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $record['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 
    $parameterr                = array('act_mode'=>'cattype','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $record['cattype'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 


    if($this->input->post('submit')){

     $imgurl            = $this->input->post('imgurl');
     $imgslide          = preg_replace('/\s+/', '', $_FILES['imgslide']['name']);
          
     $field_name_img    = 'imgslide';
     $img_file          = $this->image_uploads($field_name_img);

     $allowedExts       = array("gif","jpeg","jpg","png");
     $temp              = explode(".",$imgslide);
     $extension         = end($temp);
     //echo $extension;exit();

     if(!empty($imgslide)){
         if (in_array($extension, $allowedExts)){

            unlink(FCPATH."assets/mobilebanners/".$record['view']->ms_img);
            $parameter      = array(
                                'act_mode'  =>'update_mob_subcat',
                              'row_id'    => $id,
                              'imagename' => time().$imgslide,
                              'imageurl'  => $this->input->post('ms_parant_id'),
                              'imgtype'   => $this->input->post('ms_catid'),
                              'img_head'  => $this->input->post('type_id'),
                              'img_shead' => '',
                              'img_status'=> ''
                                );
            $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/addmobsubcat/addstationarysubcat');
            
         }
         else{            
            $this->session->set_flashdata('message2', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            redirect('admin/addmobsubcat/addstationarysubcat');
         }
        } 
        else{
            $parameter      = array(
                                'act_mode'  => 'updatemobsubcat',
                                'row_id'    => $id,
                                'imagename' => $record['view']->ms_img,
                                'imageurl'  => '',
                                'imgtype'   => '',
                                'img_head'  => '',
                                'img_shead' => '',
                                'img_status'=> ''
                              );

            $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/addmobsubcat/addstationarysubcat');
        }     
    }

    $this->load->view('helper/header');
    $this->load->view('mobilesubcat/updatesubcat',$record);
  }
//............. Slider Status ............... //
  public function statusmobsubcat(){
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='A'?'activeubcat':'inactivesubcat';
    
    $parameter     = array(
                    'act_mode'  => $act_mode,
                    'row_id'    => $rowid,
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );
    //p($parameter);exit();
    $record        = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
    redirect('admin/addmobsubcat/addstationarysubcat');
  }  

//............. End Slider Manager ............... //


//............. Delete Slider ............... //
  public function deletemobsubcat($id) {


    $parameter      = array(
                    'act_mode'   => 'delmobsubcat',
                    'row_id'     => $id,
                    'imagename'  => '',
                    'imageurl'   => '',
                    'imgtype'    => '',
                    'img_head'   => '',
                    'img_shead'  => '',
                    'img_status' => ''
                    );

    $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect('admin/addmobsubcat/addstationarysubcat');
  }

//............. Slider Status ............... //

    // end function.
//............. Update Slider ............... //
  public function image_uploads($field_name){

    $config['upload_path']   = './assets/mobilebanners/';
    $config['allowed_types'] = 'jpg|jpeg|gif|png';
    $config['max_size']      = '10000000';
    $config['file_name']     = time().preg_replace('/\s+/', '', $_FILES[$field_name]['name']);
    
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload($field_name)){
        $data['error']  = array('error' => $this->upload->display_errors());
    } else {
        $data['name']   = array('upload_data' => $this->upload->data());
    }
    return $data;
  }

  public function get_main_cat(){

    $parameterr                = array('act_mode'=>'parecatviewapp','row_id'=>$this->input->post('type_id'),'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    
    $record = $this->supper_admin->call_procedure('proc_category',$parameterr); 
  foreach ($record as $key => $value) {
   ?>
   <option value="<?php echo $value->catid;?>"><?php echo $value->catname;?></option>
   <?php
  }
  }

//............. Delete Slider ............... //


  }