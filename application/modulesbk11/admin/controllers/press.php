<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Press extends MX_Controller{

 public function __construct() {
  $this->load->model("supper_admin");
  $this->load->helper('my_helper');
  //$this->load->library('PHPExcel');
  //$this->load->library('PHPExcel_IOFactory');
  $this->userfunction->loginAdminvalidation();

 }

public function index()
{

	 $parameter         = array('act_mode'=>'viewpress','row_id'=>'','brandname'=>'','brandimage'=>'','');

               $parameter = array(
                             'act_mode'  =>'viewpress',
                             'Param1'    =>'',
                             'Param2' =>'',
                             'Param3'=>'',
                             'Param4'     =>'',
                             'Param5'     =>'',
                             'Param6'     =>'',
                             'Param7'     =>'',
                             'Param8'     =>'',
                             'Param9'     =>'',
                            
                          );
   
    $response['vieww'] = $this->supper_admin->call_procedure('proc_press',$parameter);

    //pend($response['vieww']);
	  $this->load->view('helper/header');
  $this->load->view('press/index',$response);
	
}


public function leftbanner()
{

   $parameter         = array('act_mode'=>'viewpress','row_id'=>'','brandname'=>'','brandimage'=>'','');

               $parameter = array(
                             'act_mode'  =>'viewleftbanner',
                             'Param1'    =>'',
                             'Param2' =>'',
                             'Param3'=>'',
                             'Param4'     =>'',
                             'Param5'     =>'',
                             'Param6'     =>'',
                             'Param7'     =>'',
                             'Param8'     =>'',
                             'Param9'     =>'',
                            
                          );
   
    $response['vieww'] = $this->supper_admin->call_procedure('proc_press',$parameter);

    //pend($response['vieww']);
    $this->load->view('helper/header');
  $this->load->view('press/leftbanner',$response);
  
}



public function add()
{

	if($this->input->post('submit')){

		 $bname        = $this->input->post('pdate');
    $video_img    = $_FILES['press_img']['name'];
    //p($video_img);

      $title=$this->input->post('title');
        $descp=$this->input->post('descp');
          $url=$this->input->post('url');
            $medianame=$this->input->post('medianame');
    //pend($url);
   // $purl=$this->input->post('purl');

    //p($video_img);

       $label    = time();
                  // pend($label);
                   $filename = $label.$video_img;
                   //pend($filename);
                   $filename = preg_replace("/[^a-zA-Z0-9.]/", "", $filename);
                   //pend($_FILES["press_img"]["tmp_name"]);
                 

                $tmp_name = $_FILES["press_img"]["tmp_name"];
                   move_uploaded_file($tmp_name,"imagefiles/pressimages/".$filename);


//pend($_POST);

            
               $parameter = array(
                             'act_mode'  =>'addpress',
                             'Param1'    =>$title,
                             'Param2' =>$descp,
                             'Param3'=>$url,
                             'Param4'     =>$medianame,
                             'Param5'     =>$filename,
                             'Param6'     =>'',
                             'Param7'     =>'',
                             'Param8'     =>'',
                             'Param9'     =>'',
                            
                          );

//pend($parameter);

              $record    = $this->supper_admin->call_procedureRow('proc_press',$parameter);
          
          
               $this->session->set_flashdata('message', 'Your information was successfully Saved.');
              redirect('admin/press/index');


             
      	
           
}//end submit

	$this->load->view('helper/header');
    $this->load->view('press/add');

}

//............. Image Upload ............... //
  public function image_upload($field_name){

    $config['upload_path']    = './assets/imagefiles/pressimages/';
    $config['allowed_types']  = 'jpg|jpeg|gif|png';
    $config['max_size']       = '10000000';
    $config['file_name']      = time().$_FILES[$field_name]['name'];
    
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload($field_name)){
        $data['error']       = array('error' => $this->upload->display_errors());
    } else {
        $data['name']        = array('upload_data' => $this->upload->data());
    }
    return $data;
  }


public function pressupdate($id)
{
	 if($this->input->post('submit')){
    
  $bname        = $this->input->post('pdate');
    $video_img    = $_FILES['press_img']['name'];
   // p($video_img);

      $title=$this->input->post('title');
        $descp=$this->input->post('descp');
          $url=$this->input->post('url');
            $medianame=$this->input->post('medianame');
              $mediaimg=$this->input->post('mediaimg');
   
if($video_img !='') {
       $label    = time();
                  // pend($label);
                   $filename = $label.$video_img;
                   //pend($filename);
                   $filename = preg_replace("/[^a-zA-Z0-9.]/", "", $filename);
                   //pend($_FILES["press_img"]["tmp_name"]);
                 

                $tmp_name = $_FILES["press_img"]["tmp_name"];
                   move_uploaded_file($tmp_name,"imagefiles/pressimages/".$filename);
}
else
{
  $filename=$mediaimg;
}

  $parameter = array(
                             'act_mode'  =>'editpress',
                             'Param1'    =>$title,
                             'Param2' =>$descp,
                             'Param3'=>$url,
                             'Param4'     =>$medianame,
                             'Param5'     =>$filename,
                             'Param6'     =>$id,
                             'Param7'     =>'',
                             'Param8'     =>'',
                             'Param9'     =>'',
                            
                          );

//pend($parameter);

              $record    = $this->supper_admin->call_procedureRow('proc_press',$parameter);
          
          




 
  $this->session->set_flashdata("message", "Your information was successfully Update.");
  redirect("admin/press/index");
  } 

 $parameter = array(
                             'act_mode'  =>'viewpressimgid',
                             'row_id'    =>$id,
                             'brandname' =>'',
                             'brandimage'=>'',
                             'catid'     =>''
                          );

   $response['view']   = $this->supper_admin->call_procedureRow('proc_brand',$parameter); 

  
  $this->load->view('helper/header');
  $this->load->view('press/update',$response);

}


public function pressdelete($id)
{ 

	$parameter = array(
                             'act_mode'  =>'deletepressimg',
                             'row_id'    =>$id,
                             'brandname' =>'',
                             'brandimage'=>'',
                             'catid'     =>''
                          );
//pend($parameter);
   $record    = $this->supper_admin->call_procedureRow('proc_brand',$parameter);
   $this->session->set_flashdata("message", "Your information was successfully delete.");
   redirect("admin/press/index"); 
}


public function pressstatus()
{
  $rowid             = $this->uri->segment(4);
  $status            = $this->uri->segment(5);
  $act_mode          = $status=='1'?'activepress':'inactivepress';
  //$userid       = $this->session->userdata('bizzadmin')->LoginID;
 $parameter = array(
                             'act_mode'  =>$act_mode,
                             'row_id'    =>$rowid,
                             'brandname' =>'',
                             'brandimage'=>'',
                             'catid'     =>''
                          );
  $response['vieww'] = $this->supper_admin->call_procedure('proc_brand',$parameter); 
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/press/index');
}


public function leftbannerupdate($id)
{
   if($this->input->post('submit')){
    
  $bname        = $this->input->post('pdate');
    $video_img    = $_FILES['press_img']['name'];
   // p($video_img);

        $mediaimg=$this->input->post('mediaimg');
   
if($video_img !='') {
       $label    = time();
                  // pend($label);
                   $filename = $label.$video_img;
                   //pend($filename);
                   $filename = preg_replace("/[^a-zA-Z0-9.]/", "", $filename);
                   //pend($_FILES["press_img"]["tmp_name"]);
                 

                $tmp_name = $_FILES["press_img"]["tmp_name"];
                   move_uploaded_file($tmp_name,"imagefiles/pressimages/".$filename);
}
else
{
  $filename=$mediaimg;
}

  $parameter = array(
                             'act_mode'  =>'editleftbanner',
                             'Param1'    =>'',
                             'Param2' =>'',
                             'Param3'=>'',
                             'Param4'     =>'',
                             'Param5'     =>$filename,
                             'Param6'     =>$id,
                             'Param7'     =>'',
                             'Param8'     =>'',
                             'Param9'     =>'',
                            
                          );

//pend($parameter);

              $record    = $this->supper_admin->call_procedureRow('proc_press',$parameter);
          
          




 
  $this->session->set_flashdata("message", "Your information was successfully Update.");
  redirect("admin/press/leftbanner");
  } 

 $parameter = array(
                             'act_mode'  =>'viewleftbannerid',
                             'row_id'    =>$id,
                             'brandname' =>'',
                             'brandimage'=>'',
                             'catid'     =>''
                          );

   $response['view']   = $this->supper_admin->call_procedureRow('proc_brand',$parameter); 

  
  $this->load->view('helper/header');
  $this->load->view('press/leftbannerupdate',$response);

}


}