<div class="wrapper sub_nav_bg shopping_steps">
  <div class="container-fluid">
      <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="steps_box">
                    <ul class="steps_list">
                        <li class="active-orage">
                            <div class="steps_num"><span>1</span></div>
                            <div class="steps_detail">Shopping cart</div>
                            <div class="icon-arrow"><img src="<?php echo base_url();?>assets/images/right-arrow.png" alt=""></div>                  
                        </li>
                        <li>
                            <div class="steps_num"><span>2</span></div>
                            <div class="steps_detail">ORDER INFORMATION</div>
                            <div class="icon-arrow"><img src="<?php echo base_url();?>assets/images/right-arrow.png" alt=""></div>                  
                        </li>
                        <li>
                            <div class="steps_num"><span>3</span></div>
                            <div class="steps_detail">COMPLETE PAYMENT</div>
                        </li>
                      </ul>
                </div>
            </div>
        </div>
    </div>
</div>    
        
        
<!-- --> 
<div class="">
  <div class="container-fluid">
      <div class="row">
          <div class="col-lg-12">
              <p class="cart-tips">Free shipping for orders above <span><img src="assets/images/rupees-orange.png" alt=""> 500 </span></p>
            </div>
            
            <div class="cart-goods-list">
              <div class="list-head">
                  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">Product Name</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">Price</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">Quantity</div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">Total</div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
                </div>
             

                <div class="list-body">
                  <?php $subtotal = 0; foreach($cart as $key => $val) { ?>
                    <div class="list-body-item">
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <div class="item-img"><img src="<?php echo base_url();?>images/thumimg/<?php echo $val['image'];?>" alt=""></div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <h3 class="item_title"><?php echo $val['name'];?></h3>
                            <p class="sold_by">Sold by: TVS ELECTRONICS LIMITED</p>    
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">
                          <div class="item_price_txt"><span><img src="<?php echo base_url();?>assets/images/rupees-gay.png" alt=""></span><?php echo $val['price'];?></div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="change_goods_num">
                              <a href="javascript:void(0);" proid="<?php echo $val['id'];?>" id="btqty<?php echo $val['rowid'];?>" onclick="updatesaveqtym('<?php echo $val['rowid'];?>',1,'<?php echo $val['max'];?>');" class="num_left border_1">
                                <img src="<?php echo base_url();?>assets/images/minus.png" alt="">
                              </a>
                              <div class="num_center">
                                <input type="number" min="1" size="1" readonly  id="<?php echo $val['rowid'];?>" value="<?php echo $val['qty']; ?>" class="num_qnt">
                              </div>
                              <a href="javascript:void(0);" proid="<?php echo $val['id'];?>" id="btqty<?php echo $val['rowid'];?>" onclick="updatesaveqtyp('<?php echo $val['rowid'];?>',1,'<?php echo $val['max'];?>');" class="num_left border_2">
                                <img src="<?php echo base_url();?>assets/images/plus.png" alt="">
                              </a>
                            </div>
                            <input type="submit" class="save_btn" onClick="updateqty('<?php echo $val['rowid'];?>',<?php echo $val['id'];?>);" value="Save">
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">
                          <div class="item_price_total"><span><img src="<?php echo base_url();?>assets/images/rupees-orange.png" alt=""></span><?php echo $val['price']*$val['qty'];?></div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                          <a href="javascript:void(0);" onClick="delcart('<?php echo $val['rowid'];?>');" class="close_btn_cart">
                            <img src="<?php echo base_url();?>assets/images/close_cart.png" alt="">
                          </a>
                        </div>
                    </div>
                  <?php 
                   $subtotal += $this->cart->total();
                  } ?>  
                         
                    <div class="change_pincode">
                      <div class="col-lg-7"></div>
                        <div class="col-lg-5">
                          <div class="pincode_box">
                                <div class="question_icon">
                                    <div class="qutin_icon"><img src="<?php echo base_url();?>assets/images/question.png" alt="">
                                      <div class="question_txt">
                                        Cash on Delivery (COD) payment mode is available only in select areas. Please enter your pin-code below to check if your address is eligible for COD.
                                      </div>
                                    </div>
                                 </div>
                                 <div class="pin_change">
                                    <div class="pin_h6">
                                      <h6>Delivery to</h6>
                                      <p>NEW DELHI</p>
                                    </div>
                                    <div class="change_txt"><a href="javascript:void(0);">Change</a></div>
                                </div>
                                <div class="pin_show">
                                    <h6>Delivery to</h6>
                                    <div class="check_wrapper pin_p">
                                      <input class="pincode_detail" type="text" placeholder="Enter your pincode" maxlength="6" autocomplete="off">
                                      <a href="javascript:void(0);" class="pincode_btn">Go</a>
                                    </div>
                                </div>
                             </div> 
                        </div>
                    </div>
                    
                    <div class="cart-action-bar">
                      <div class="col-lg-7"></div>
                        <div class="col-lg-5">
                          <div class="cart-action-box">
                              <div class="total_price_item"><span><img src="<?php echo base_url();?>assets/images/rupees-orange.png" alt=""></span><?php echo $subtotal;?>
                              </div>
                                <a href="<?php echo base_url();?>home" class="che_btn_black">Continue Shopping</a>
                                <a href="<?php echo base_url();?>orderinformation" class="checkout_btn">Checkout</a>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Div -->                
          </div>    
        </div>     
     </div>
</div>

<div class="wrapper">
  <div class="policies_white">
    <div class="container-fluid">
      <div class="row">
            <div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
      </div>
        </div>
  </div>
</div>
</div>
 
        
    <script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    
    $(document).ready(function(){
      $(".navigation > ul > li > a").hover(function(){
        //alert("ddd");
        if($(this).next(".product_item_view").length > 0){
        $(".logo_row").addClass("show_nav");
        $(this).next(".product_item_view").css({"display":"block"});
        }
        else{
          $(".logo_row").removeClass("show_nav");
          $(".product_item_view").css({"display":"none"});
        }
      });
      $(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
        $(".product_item_view").css({"display":"none"});
      });
    });
    </script>
    <script src="assets/js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
   
   
<script>
$(document).ready(function(){
    $(".change_txt").click(function(){
        $(".pin_change").hide();
    $(".pin_show").show();
    });
});
</script>
    
<script type="text/javascript">
  function updatesaveqtyp(ids,qty,max) {
    var gugetQty = parseInt($('#'+ids).val());
    if((gugetQty >= 1 && gugetQty <= max)){
       var gu = parseInt($('#'+ids).val())+1;  
       if(gu <= max)
       {
          $('#'+ids).val(gu);
          return false;
       }
     else
       {
          $('#'+ids).val(gugetQty);
          return false;
       }
    }else{
      return false;
    }
  }

  function updatesaveqtym(ids,qty,max) {
      var gu = parseInt($('#'+ids).val());
      if((1 <= gu)){
        if(gu==1)
        {
          $('#'+ids).val(gu);
        }
        else
        {
          $('#'+ids).val(gu-1);
        }
     }else{
       return false;
     }
  }

  function updateqty(rowid,productId){
    var getarrayid = rowid;
    var qnty       = $("#"+rowid).val();
    var proId      = productId;
    var base_url = '<?php echo base_url();?>';
    $.ajax({
       url:base_url+'webapp/cart/updatecart',
       type: 'POST',
       dataType: 'json',
       data: {arrayid:getarrayid,qnty:qnty,productId:proId},
       success:function(data){
         $("#totalcartpricewithout").html(data.grandTotal);
         $("#Grandtotalwithcart").html(data.grandTotalwithdilabery);
          
          location.reload(); // temp code added by manishkr
      }
    });
  }

  function delcart(rowid){
    var getarrayid = rowid;
    var base_url = '<?php echo base_url();?>';
    $.ajax({
       url:base_url+'webapp/cart/deletecart',
       type: 'POST',
       dataType: 'json',
       data: {arrayid:getarrayid},
       success:function(data){
        location.reload(); // temp code added by manishkr
      }
    });
  }
</script>     

  </body>
</html>