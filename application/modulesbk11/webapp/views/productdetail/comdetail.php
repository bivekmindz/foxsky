<?php //p($ProductDetails);exit;?>
  <!--zoom effect-->
    <link href="<?php echo base_url();?>assets/webapp/css/jquery.simpleLens.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/webapp/css/jquery.simpleGallery.css" rel="stylesheet" />
    
    <!--zoom end-->

<div class="page_pro1" style="height:auto;padding:0px 0px 0px;background: #fff">
  <div class="col-lg-12">
  <div class="row">
  <div class="heading-box">
    <div class="heading_name">
     <a href="<?php echo base_url(); ?>" class="">Home</a>
      <a href="<?php echo base_url().'combo/combolist'; ?>" class=""><i class="fa fa-angle-right"></i><?php echo "Combo Super Saver"; ?></a>
  </div>
</div>
</div>
 </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-sm-12">
<div class="main_box">
<div class="row">
<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
<div class="left_box">
<div class="col-lg-5 col-sm-4 col-sm-4 col-xs-12">
<div class="row">
<div class="small-slider">
<div class="simpleLens-gallery-container" id="demo-1">
                                        <div class="simpleLens-container">
                                            <div class="simpleLens-big-image-container">
                                                <a class="simpleLens-lens-image" data-lens-image="<?php echo base_url(); ?>assets/comboimage/<?php echo $ProductDetails[0]->combo_img; ?>">
                                                    <img width="100%" src="<?php echo base_url(); ?>assets/comboimage/<?php echo $ProductDetails[0]->combo_img; ?>" class="simpleLens-big-image">
                                                </a>
                                            </div>
                                        </div>
                                
                                        <div class="simpleLens-thumbnails-container">
                                            <?php
if($ProductDetails[0]->combo_img!='' && $ProductDetails[0]->combo_img!='na'){
  $filename=base_url()."assets/comboimage/thumnail/".$ProductDetails[0]->combo_img;
  $mediumname=base_url()."assets/comboimage/".$ProductDetails[0]->combo_img;
  $largename=base_url()."assets/comboimage/".$ProductDetails[0]->combo_img;
  echo '<a href="#" class="simpleLens-thumbnail-wrapper"
                                               data-lens-image="'.$largename.'"
                                               data-big-image="'.$mediumname.'">
                                                <img src="'.$filename.'">
                                            </a>';
}

if($ProductDetails[0]->combo_seimg!='' && $ProductDetails[0]->combo_seimg!='na'){
  $filename=base_url()."assets/comboimage/thumnail/".$ProductDetails[0]->combo_seimg;
  $mediumname=base_url()."assets/comboimage/".$ProductDetails[0]->combo_seimg;
  $largename=base_url()."assets/comboimage/".$ProductDetails[0]->combo_seimg;
  echo '<a href="#" class="simpleLens-thumbnail-wrapper"
                                               data-lens-image="'.$largename.'"
                                               data-big-image="'.$mediumname.'">
                                                <img src="'.$filename.'">
                                            </a>';
}

if($ProductDetails[0]->combo_seimg2!='' && $ProductDetails[0]->combo_seimg2!='na'){
  $filename=base_url()."assets/comboimage/thumnail/".$ProductDetails[0]->combo_seimg2;
  $mediumname=base_url()."assets/comboimage/".$ProductDetails[0]->combo_seimg2;
  $largename=base_url()."assets/comboimage/".$ProductDetails[0]->combo_seimg2;
  echo '<a href="#" class="simpleLens-thumbnail-wrapper"
                                               data-lens-image="'.$largename.'"
                                               data-big-image="'.$mediumname.'">
                                                <img src="'.$filename.'">
                                            </a>';
}

if($ProductDetails[0]->combo_seimg3!='' && $ProductDetails[0]->combo_seimg3!='na'){
  $filename=base_url()."assets/comboimage/thumnail/".$ProductDetails[0]->combo_seimg3;
  $mediumname=base_url()."assets/comboimage/".$ProductDetails[0]->combo_seimg3;
  $largename=base_url()."assets/comboimage/".$ProductDetails[0]->combo_seimg3;
  echo '<a href="#" class="simpleLens-thumbnail-wrapper"
                                               data-lens-image="'.$largename.'"
                                               data-big-image="'.$mediumname.'">
                                                <img src="'.$filename.'">
                                            </a>';
}
    
  ?>      
                                        
                                          
                                           
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    


</div>
</div>
</div>
<div class="col-lg-7 col-md-8 col-sm-8 col-xs-12">
 <div class="product_box">
 <h1 class="text-name"><?php echo ucwords($ProductDetails[0]->name); ?></h1>
 <?php $pronamestrdet=str_replace(' ','-',$ProductDetails[0]->name);
 $pronamestrnamedet = seoUrl(str_replace('/','-',$pronamestrdet)); ?>
<input type="hidden" id="pronamepro" value="<?php echo $pronamestrnamedet;?>" />
 <div class="row">
 <div class="col-lg-12">
 	<div class="prige">
 <span class="brabd_bold">Price:</span>
 <?php
 //$cityprice =citybaseprice($ProductDetails->FinalPrice,$ProductDetails->cityvalue);
 $prodtsc=number_format((1-$ProductDetails[0]->comboprice/$ProductDetails[0]->total_sellingprice)*100,1,".",""); ?>
Rs. <?php echo $ProductDetails[0]->comboprice; ?> <span class="under_line">Rs. <?php echo $ProductDetails[0]->total_sellingprice; ?></span><span class="off-color"><?php echo $prodtsc.' %'; ?></span>
</div>
 <?php  foreach ($ProductDetails as $key => $value) {  $totcomboqty+=$value->product_qty;   } ?>
<h2 class="brand"><samp class="brabd_bold">Total Combo Products Quantity :</samp> <?php echo $totcomboqty; ?></h2>
</div>


<div style="margin-top: 60px;max-height: 200px;overflow-y: auto;" class="table-unit">
  <table class="table_boder">
    <thead> 
      <tr>
        <th>Product</th>
        <th style="text-align: center;">Quantity</th>
        <th style="text-align: center;">Product MRP</th>
        <th style="text-align: center;">Product Price</th>
      </tr>
    </thead>
    <tbody>
    <?php  foreach ($ProductDetails as $key => $value) {
       
    
       $about = $value->about;
       $ingrients = $value->ingridients;
    ?>

       <tr>
        <td><?php echo  $value->proname; ?></td>
        <td style="text-align: center;"><?php echo  $value->product_qty; ?></td>
        <td style="text-align: center;"><span class="under_line">Rs. <?php echo  $value->pro_mrp*$value->product_qty;  ?></span></td>
        <td style="text-align: center;">Rs. <?php echo  $value->pro_combo_price*$value->product_qty;  ?></td>
      </tr>
       
    <?php } 
$pid = $ProductDetails[0]->ComboId;
if(in_cart_array($this->cart->contents(), $pid) == true){
      $addClassd = "disablebutton";
      $data_is_in_cart = 'true';
    } else { 
      $data_is_in_cart = 'false';
      $addClassd ="";
  }
    ?>
    </tbody>
  </table>
</div>  
        
<div style="margin-top: 20px;" class="table-unit">
<table class="table_boder table-responsive">
    <thead> 
      <tr>
        <th>Qty</th>
        <th>Price per unit</th>
        <th>Discount</th>
      </tr>
    </thead>
    <tbody> 
       <tr>
        <td><?php echo str_replace('to', ' to ', $value->qty); ?></td>
        <td>Rs. <?php echo cityqtyprice($value->comboprice,$value->cityvalue,$value->price); ?></td>
        <td><?php echo $prodtsc.'%';  ?> <?php echo ($value->price!="" && $value->price!=0)?'+ '.$value->price.'%':'';?></td>
      </tr>
       <tr>
        <td><?php echo str_replace('to', ' to ', $value->qty1); ?></td>
        <td>Rs. <?php echo cityqtyprice($value->comboprice,$value->cityvalue,$value->price1); ?></td>
      <td><?php echo $prodtsc.'%';  ?> <?php echo ($value->price1!="" && $value->price1!=0)?'+ '.$value->price1.'%':'';?></td>
      </tr>
     <tr>
        <td><?php echo str_replace('to', ' to ', $value->qty2); ?></td>
        <td>Rs. <?php echo cityqtyprice($value->comboprice,$value->cityvalue,$value->price2); ?></td>
        <td><?php echo $prodtsc.'%';  ?> <?php echo ($value->price2!="" && $value->price2!=0)?'+ '.$value->price2.'%':'';?></td>
      </tr>
    </tbody>
</table>
</div>

<div style="float:left; padding-top:10px;"><h1 class="q_size">QUANTITY</h1>
<span class="first2" data-href="<?=$ProductDetails[0]->ComboId?>" id="first2" onclick="cart_update_listing($(this));"><a href="javascript:void(0);" class="q_link"><i class="fa fa-minus"></i></a></span>
<input type="text" id="a1<?=$ProductDetails[0]->ComboId?>" value="1" class="value_size" style="margin-right:5px;">
<span class="last2" data-href="<?=$ProductDetails[0]->ComboId?>" data-hrefnew="<?=$ProductDetails[0]->quantityLeft?>" id="last2" onclick="cart_update_listing($(this));"><a href="javascript:void(0);" class="q_link"><i class="fa fa-plus"></i></a></span>
  
              
        <a href="javascript:void(0)" data-href="<?=$ProductDetails[0]->ComboId?>" data-hrefnew="<?php echo $data_is_in_cart; ?>" class="add_cart addtocartcombo <?php echo $addClassdNoproduct;?> <?php echo $addClassd;?>" id="<?php echo $addtocartID; ?>"    
        data-is-in-cart ="<?php echo $data_is_in_cart; ?>"  data-is-listing-buyable="<?php echo $disableButton; ?>"  data-buy-listing-id = "<?php echo $pid;?>" type="submit" name="submit">

<i class="fa fa-shopping-cart"> Add to Cart</i></a>            
</div>      
</div>
      
<div class="col-lg-12">
<div class="row">
<div class="left_easy">
<div class="easy_box">
<div class="easy-contain">
<div class="img_easy"><img src="http://192.168.1.30/bizzgainlive/assets/webapp/images/01.jpg"></div>
  <div class="g_box"><strong>100% GENUINE</strong><br>Products Guarantee</div>
</div>

<div class="easy-contain">
<div class="img_easy"><img src="http://192.168.1.30/bizzgainlive/assets/webapp/images/02.jpg"></div>
  <div class="g_box"><strong>EASY RETURN</strong><br>No Question Asked!</div>
</div>


<div class="easy-contain">
<div class="img_easy"><img src="http://192.168.1.30/bizzgainlive/assets/webapp/images/03.jpg"></div>
  <div class="g_box"><strong>15 DAYS</strong><br>Moneyback Guarantee</div>
</div>


<div class="easy-contain">
<div class="img_easy"><img src="http://192.168.1.30/bizzgainlive/assets/webapp/images/04.jpg"></div>
  <div class="g_box"><strong>SECURE PAYMENT</strong><br>100% Secure Money</div>
</div>

</div>
</div>
</div>
</div>
</div>   
</div>
</div>
</div>

<!-- start related products -->
     
 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
<div class="row_sty">
<div class="related_bg">
<h1 class="related_size">Related Products</h1>
<div class="space1"></div>
<?php
  

if($similarProduct->scalar != "Something Went Wrong"){  
foreach ($similarProduct as $key => $value) {
$cityprice =citybaseprice($value['FinalPrice'],$value['cityvalue']);
?>
<div class="pro_box_m">
<div class="pro_box">
<div class="col-lg-3 col-md-3 col-sm-3">
<div class="row">
<div class="pro_img">
<a href="<?php echo base_url().'product/'.seoUrl(strtolower($value['ProName'])).'/'.$value['promapid'].'.html'?>">
<img src="<?php echo proimgurl('images/hoverimg/'.$value['proImg']);?>" class="img-responsive" onerror="imageError(this)" alt="<?php echo ucwords($value['ProName'])?>">
</a>

</div>
</div>
</div>

<div class="col-lg-9 col-md-9 col-sm-9">
<div class="pro_cont">
  <div class="size"><a href="<?php echo base_url().'product/'.seoUrl(strtolower($value['ProName'])).'/'.$value['promapid'].'.html'?>"><?php echo $value['ProName']; ?></a></div>
    <div class="size1">Rs. <?php echo $cityprice; ?></div>
      <div class="product_wishlist">
       <a href="javascript:void(0);" style="outline:none;" data-href="<?=$value['promapid']?>" class="button inquiry" data-toggle="modal" data-target="#myModal"><i class="fa fa-files-o"></i><span class="compare_size"></span></a>
        <?php  echo listwishlistlink('view',$userid,$value['promapid']); ?>
  </div>
</div>
</div>
</div>
</div>
<?php } }?>
</div>
</div>
</div>       
<!-- end related products -->

<!-- start most viewed -->

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<h1 class="most_font">MOST VIEWED</h1>
<div class="row">
<div class="relat_page">
<?php 
foreach ($mostview as $key => $valu) {

$value=(object)$valu;
?>

<div class="product_list">
<div class="product_tab">

<div class="product_tab_inner">
<div class="img_box">
<a href="<?php echo base_url().'product/'.seoUrl(strtolower($value->ProName)).'/'.$value->promapid.'.html'?>">
<img src="<?php echo proimgurl('images/hoverimg/'.$value->proImg);?>" onerror="imageError(this)" alt="<?php echo ucwords($value->ProName)?>">

</a>
</div>
<div class="product_info">
<div class="pro_name"><a href="<?php echo base_url().'product/'.seoUrl(strtolower($value->ProName)).'/'.$value->promapid.'.html'?>"><?php echo ucfirst($value->ProName);?></a></div>

<div class="price">
<?php 
$cityprice =citybaseprice($value->FinalPrice,$value->cityvalue);
$prodtsc=number_format((1-$cityprice/number_format($value->productMRP,1,".",""))*100,1,".","");
if($value->ProDiscount<=0){
echo '<span class="new_p">Rs.'.number_format($value->productMRP,1,".","").'</span>';
} else {
if($value->ProDisType=='fixed')
$distype='Rs. Off';
else
$distype='% Off';
echo '<span class="old_p">Rs.'.number_format($value->productMRP,1,".","").'</span>
<span class="new_p">Rs.'.$cityprice.'</span>';
echo '<div class="sale_ad">'.$prodtsc.'<small>'.$distype.'</small></div>';
}
?>
</div>

<div class="p_link">
<a href="<?php echo base_url().'product/'.seoUrl(strtolower($value->ProName)).'/'.$value->promapid.'.html'?>" class="add_cart"><i class="fa fa-shopping-cart"></i> Add to cart</a>

</div>
<div class="p_link2">
<a href="javascript:void(0);" style="outline:none;" data-href="<?=$value->promapid?>" class="button inquiry" data-toggle="modal" data-target="#myModal"><i class="fa fa-files-o"></i><samp class="compare_size"></samp></a>
<?php  echo listwishlistlink('view',$userid,$value->promapid); ?>
</div>
</div>
</div>
</div>
</div>
<?php } ?>
</div>
</div>
</div>
</div>

<!-- end most viewed -->
    
    
<div class="line_top">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-3 col-md-3 ">
<div class="row">
<div class="discription_left">
<ul class="description">
    <li><a href="javascript:void(0)" id="dis_a">Description</a></li>
</ul>
</div>
</div>
</div>


<div class="col-lg-9 col-md-9" id="pay_a" style="display: block;">
<div class="row">
<div class="description_box">
<p class="size"><?php echo $about; ?></p>
</div>
</div>
</div>
</div>    
</div>
    
    
<!--product slider-->
 
<script type="text/javascript">
  function test(val1,val2,val3){
     var classme = "si_"+val1;
    $( "span.siz" ).each(function( index ) {
                    //console.log( index + ": " + $( this ).text() );
                    if($(this).hasClass(classme)){
                      //alert($(this).html());
                      $(this).show();
                    }else{
                      $(this).hide();
                    }
                  });
  }
</script>  
<style type="text/css">
.highlightSize {
  background: #444;
  color: #fff !important;
}
span.siz {
  border: 1px solid #E6E2E2;
  display: inline-block;
  text-align: center;
  margin-right: 5px;
  cursor: pointer;
  padding: 3px 10px 3px 10px;
  margin-left:15px;

 
  color: #444;
}
span.siz a {
  color: #444;
  font-size: 14px
}
.siz.highlightSize a {
  color: #fff
}
span.siz:hover {
  background: #444;
  color: #f1f1f1
}
span.siz:hover a {
  color: #fff
}
  
  li.red {
  width: 27px;
  height: 25px;
  background-color: red;
  margin: 5px;
  cursor: pointer;
  border: 0px solid #888; float:left; list-style-type:none;
 

}
li.red:hover {
  /*box-shadow: 0 0 4px #333*/
}
.addHighliter {
/*  border: 2px solid #FFF;
  box-shadow: 0 0 8px #6E6A6A*/
}
</style>
 
    
    <script>

    $( "#dis_a" ).click(function() {
    $( "#pay_a" ).css("display","block");
    $( "#pay_b" ).css("display","none");
    $( "#pay_c" ).css("display","none");
});
$( "#dis_b" ).click(function() {
    $( "#pay_b" ).css("display","block");
    $( "#pay_a" ).css("display","none");
    $( "#pay_c" ).css("display","none");
});
$( "#dis_c" ).click(function() {
    $( "#pay_c" ).css("display","block");
    $( "#pay_a" ).css("display","none");
    $( "#pay_b" ).css("display","none");
});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
     pro_img_s();
     main_img();
});
   $(window).resize(function(){
 setTimeout( function(){ 
   pro_img_s();
   main_img();
  }  , 500 );

  });

function pro_img_s(){
  var x = $(".img_box").width();
  $(".img_box a").height(x);
}

function main_img(){
  var x1 = $(".simpleLens-container").width();
   $(".simpleLens-big-image-container").height(x1);
 $(".simpleLens-lens-image").height(x1);
}

function coltest(colpromapid){

  var pronamedet=$('#pronamepro').val();
  var prourl='<?php echo base_url();?>product/'+pronamedet+'-'+colpromapid+'.html';
  window.location.href=prourl;
}

    

    $(".addtocartcombo").on('click',function(){
    
    var proId = $(this).attr("data-href");
    var proId =  $.trim(proId);
    var qtyval=$('#a1'+proId).val();
   
     var data_in_cart = $(this).attr("data-hrefnew");


  
     
if(data_in_cart == 'true'){
     
       $("#sizerror"+proId).css('display', 'block');
       $("#sizerror"+proId).html("Product is already in cart");
       return false;
   }else{
      var base_url = '<?php echo base_url(); ?>';
    $.ajax({
   
        url: base_url+'webapp/combo/comboaddtocart',
        type: 'POST',
       dataType: 'json',
        data: {'proId': proId,'qtyval':qtyval},
        success: function(data){
             
           $(".add_cart_pro_name").html(data.combo.name+"<span>This combo has been add to your cart</span>");
           $(".add_cart_pro_img").html("<img src="+data.combo.img+" style='width:80px;height:80px;'/>");
           $("#smallcart").css("display","block").slideDown().delay(3000).slideUp();
           $(".displayincarttotal").html(data['total']+" "+"("+data['TotalInCart']+")");
           $(".cart_item").html(data['TotalInCart']);
        }

      });

   }

    



});



</script>