<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.slide.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
      <script type="text/javascript">
        $(document).ready(function(){
           $(".menu-icon i").click(function(){
            $(".navigation ul").addClass("activemenu");
            $("body").addClass("overlayboday");
           });
           
        });

        </script>
        <body >
<div class="wrapper top_link">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-4">

						<div class="menu-icon">
							 <i class="fa fa-bars"></i>
						</div><!--end of menu-icon-->

						<ul class="top_link_left">
							<li><a href="#">Foxsky India</a></li>
							<li><a href="#">Foxsky Community</a></li>
							<li><a href="#">Download Foxsky store app</a></li>
						</ul>
					</div>
					<?php  
						$param =  $this->cart->contents(); 
					    $total = count($param); 
			        ?> 
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
				        <div class="cart_entry">
                            <a href="<?php echo base_url();?>cart">
	                        	<i class="fa fa-shopping-cart"></i> 
	                        	<span class="cart_txt">Items</span>
	                            <span class="cart_number">( <?php echo $total; ?> )</span>
                            </a>
                            
                            <div class="cart_over">
                                <?php foreach($param as $key => $val) {?>
                            	<div class="mini-cart-item">
                                	<div class="mini-cart-pic"><img src="<?php echo base_url();?>images/thumimg/<?php echo $val['image'];?>" alt=""></div>
                                		<div class="mini-cart-txt">
                                			<a href="#"><?php echo $val['name'];?></a>
                                    		<span><img src="<?php echo base_url();?>assets/images/rupees-orange.png" alt=""> <?php echo $val['price']*$val['qty'];?></span>
                                            <div class="close-cart-icon"><img src="<?php echo base_url();?>assets/images/close_cart.png" alt=""></div>
                                	</div>
                                </div>
                               <?php } ?>
                                <div class="mini-cart-summary-info">
                                	<div class="summary_total_h">
                                    	<h5>Subtotal <span>( <?php echo $total;?> items )</span></h5>
                                        <div class="summary_price_h"><img src="<?php echo base_url();?>assets/images/rupees-orange.png" alt=""><span><?php echo $this->cart->total();?></span></div>
                                    </div>
                                    <a href="<?php echo base_url();?>orderinformation" class="checkout_btn_cart">Checkout</a>
                                </div>                     
                            </div>
                        </div> 
						<ul class="top_link_right">
							<?php   
								$logval = logd();
								$memberdata = $this->session->userdata('foxsky');
	                            $memname = $memberdata->firstname;
							
							if(!empty($logval)){ 
							?>
							<li>
								<?php 
								if(!empty($memberdata)){
								   $url1 = '<a href='.base_url('logout').'>Logout</a>';
								   echo $url1;
								}
								?>
							</li>
							<li>
								<?php 
								if(!empty($memberdata)){
     								$url = '<a href='.base_url('myaccount').'>My Account</a>';
     								echo $url;
								}
								?>
							</li>
							<?php } else { ?>
                                <li><a href="<?php echo base_url("register");?>">Sign up</a></li>
							    <li><a href="<?php echo base_url("login");?>">Sign in</a></li>
                            <?php }?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<!-- navigation bar and foxsky logo -->
		<div class="wrapper logo_row">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					  <div class="logo">
					  	<a href="<?php echo base_url();   ?>">
					  		<img src="<?php echo base_url();?>assets/webapp/images/logo.png" alt="">
					  	</a>
					  </div>
		     		</div>
					<?php  $this->load->view('helper/nav');   ?>
				</div>
	        </div>
		</div>
        