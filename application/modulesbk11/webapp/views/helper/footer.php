
<div class="footer">
  <div class="container-fluid">
      <div class="row">
        
             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no_padding">
               <h1 class="footer_heading">SUPPORT</h1>
                   <ul class="footer_list">
                       <li><a href="<?php echo base_url(); ?>">Home</a></li>
                       <li><a href="<?php echo base_url('aboutus'); ?>">About Us</a></li>
                       <li><a href="<?php echo base_url('privacy_policy'); ?>">Privacy Policy</a></li>
                       <li><a href="<?php echo base_url('feedback'); ?>">FeedBack</a></li>
                       <li><a href="<?php echo base_url('feedback'); ?>">Contact us</a></li>
                       <li><a href="<?php echo base_url('feedback'); ?>">Help</a></li>
                    </ul>
                  </div>
                            
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                  <h1 class="footer_heading">Products</h1>
                      <ul class="footer_list">
                         <li><a href="#">Foxsky Tv</a></li>
                         <li><a href="#">Audio</a></li>
                         <li><a href="#">Smart Band</a></li>
                         <li><a href="#">Anti Theft Wallet</a></li>
                         <li><a href="#">Power Banks</a></li>
                         <li><a href="#">Accessories &amp; Support</a></li>
                      </ul>
                  </div>                

               
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                <h1 class="footer_heading no__bottom_space">FOLLOW Foxsky</h1>
                    <p class="p_txt_fotter">We want to hear from you!</p>
                        <div class="em-social">
                                    <a class="em-social-icon em-facebook f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-twitter f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-pinterest  f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-google f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    
                                </div>
                            </div>
                            
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                    <h1 class="footer_heading no__bottom_space">Toll Free Number</h1>
                       <h3 class="number_free">1800 5328 777</h3>
                          <h1 class="let_heading">LET'S STAY IN TOUCH</h1>
                             <p class="get_txk">Get updates on sales specials and more</p>
                                <div class="block block-subscribe">
                                    <form action="" method="post">
                                        <div class="block-content">
                                            <div class="form-subscribe-content">
                                                <div class="input-box">
                  <input type="text" name="email" title="Sign up for our newsletter" placeholder="Enter Email Address" id="newsemail" class="input-text required-entry validate-email">
              </div>
           <p id="returnmsgNew" style="width: 100%;float: left; color: #fff;"></p>
        <div class="actions"> <button type="submit" title="Subscribe" class="button" id="newslettersubmit"><span><span>Subscribe</span></span></button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                
                            </div>
                            

                            
        <div class="col-md-12 no_padding">
          <span class="copyright">Copyright © 2017  Foxsky. All Rights Reserved.</span>
        </div>                
                            
           </div>
        </div>
   </div>
   
</div>


<!-- video popup-->

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title txt_pop">Foxsky Video Mobile</h4>
        </div>
        <div class="modal-body">
          <video width="100%" height="400" controls autoplay>
              <source src="http://192.168.1.30/foxsky/videos/Raazi - Official Trailer (HQ 360p).mp4" type="video/mp4">
              <source src="movie.ogg" type="video/ogg">
          </video>
          
        </div>
        
      </div>
    </div>
  </div>
</div>


    <script>
     new WOW().init();
    </script>
    
  <script src="<?php echo base_url();?>assets/js/jquery.bxslider.min.js"></script>
     <script>
        $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 330,
        slideMargin: 0,
        ticker: true,
        speed: 30000
    });
       </script>
 
    <script type="text/javascript">
    $(function() {
    $('.slide').slide({
    'slideSpeed': 3000,
    'isShowArrow': true,
    'dotsEvent': 'mouseenter',
    'isLoadAllImgs': true,
    'isAutoSlide':false
    });
    });
    
      $(function() {
    $('.slide_bottom').slide({
    'slideSpeed': 3000,
    'isShowArrow': true,
    'dotsEvent': 'mouseenter',
    'isLoadAllImgs': true,
    'isAutoSlide':false
    });
    });
    

    
    $(document).ready(function(){
      $(".navigation > ul > li > a").hover(function(){
        //alert("ddd");
        if($(this).next(".product_item_view").length > 0){
        $(".logo_row").addClass("show_nav");
        $(this).next(".product_item_view").css({"display":"block"});
        }
        else{
          $(".logo_row").removeClass("show_nav");
          $(".product_item_view").css({"display":"none"});
        }
      });
      $(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
        $(".product_item_view").css({"display":"none"});
      });
    });

//
    </script>
        
 <script>
            $(".accessories_list > li > a").mouseover(function () {
                $(".gift_active").removeClass("gift_active");
                $(this).addClass("gift_active");
                var x = $(this).attr("title");
        
                $(".tab_gift_box").removeClass("active_gift");
        $(".tab_gift_box").removeClass("in"); 
                $("#" + x).addClass("active_gift");
        
        // setTimeout(function(){
//                 $("#" + x).addClass("in");
//                }, 100);
                
            });
  </script>
  <script>
              $(".order_tab > li > a").click(function () {
                $(".active_review").removeClass("active_review");
                $(this).addClass("active_review");
                var x = $(this).attr("title");
        
                $(".order_box_tab").removeClass("active_review_tab");
        $(".order_box_tab").removeClass("in");  
                $("#" + x).addClass("active_review_tab");
        
         setTimeout(function(){
                 $("#" + x).addClass("in");
                }, 100);
                
            });
  </script>