    <div class="wrapper sub_nav_bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sub_nav">

                       
                        <div class="pull-right">
                            <ul class="sub_tv">
                           
<?php if($producttab->countOverview>0) { ?>
                      <li><a href="<?php echo base_url();?>product/<?php echo $urlname; ?>" class="active-font">Overview</a></li>
                      <?php } ?>
                      <?php if($producttab->countspecification>0) { ?>
                      <li><a href="<?php echo base_url();?>productspecification/<?php echo $urlname; ?>">Specs</a></li> <?php } ?>
                       <?php if($producttab->countacces>0) { ?>
                        <li><a href="<?php echo base_url();?>productaccessories/<?php echo $urlname; ?>">Accessories</a></li><?php } ?>
                        <li><a href="<?php echo base_url();?>review/<?php echo $urlname; ?>">Review</a></li>
                       <!--  <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>">F-code</a></li> -->
                         <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>" class="btn-small btn-orange">Buy Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper white_bg">
<?php foreach ($accessoriesdata as $key => $value) {?>



        <div class="container-fluid">
         <div class="asses_content_box">
             <div class="asses_title"><?php echo $value->proname; ?></div>
             <div class="asses_dec"><?php echo $value->description; ?></div>
            
         </div>
        </div>
        <div class="asses_img"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value->prd_main_image; ?>" height="375" width="50"> </div>
        
<div style="text-align: center;padding-top:20px ">

<a href="<?php echo base_url().'productaccessoriesdetails/'.str_replace(' ', '_', $value->proname).'-'.$value->proid.'';?>">

Learn more</a>

</div>

<?php } ?>




       
    </div>
    <div class="wrapper">
        <div class="policies_white">
            <div class="container-fluid">
                <div class="row">
                    <div class="footer_policies">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                            <div class="service-policy">
                                <a href="#">
                                    <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                                    <div class="policy_txt">
                                        <strong>Hassle-free replacement</strong>
                                        <br>
                                        <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                                    </div>

                                </a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                            <div class="service-policy pull_space">
                                <a href="#">
                                    <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                                    <div class="policy_txt">
                                        <strong>100% secure payments</strong>
                                        <br>
                                        <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                                    </div>

                                </a>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                            <div class="service-policy pull_right">
                                <a href="#">
                                    <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                                    <div class="policy_txt">
                                        <strong>Vast service network</strong>
                                        <br>
                                        <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                                    </div>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no_padding">
                    <h1 class="footer_heading">SUPPORT</h1>
                    <ul class="footer_list">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">FeedBack</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Help</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                    <h1 class="footer_heading">Products</h1>
                    <ul class="footer_list">
                        <li><a href="#">Foxsky Tv</a></li>
                        <li><a href="#">Audio</a></li>
                        <li><a href="#">Smart Band</a></li>
                        <li><a href="#">Anti Theft Wallet</a></li>
                        <li><a href="#">Power Banks</a></li>
                        <li><a href="#">Accessories &amp; Support</a></li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                    <h1 class="footer_heading no__bottom_space">FOLLOW Foxsky</h1>
                    <p class="p_txt_fotter">We want to hear from you!</p>
                    <div class="em-social">
                        <a class="em-social-icon em-facebook f-left" title="em-sample-title" href="javascript:void(0)">
                            <span class="fa fa-fw"></span>
                        </a>
                        <a class="em-social-icon em-twitter f-left" title="em-sample-title" href="javascript:void(0)">
                            <span class="fa fa-fw"></span>
                        </a>
                        <a class="em-social-icon em-pinterest  f-left" title="em-sample-title" href="javascript:void(0)">
                            <span class="fa fa-fw"></span>
                        </a>
                        <a class="em-social-icon em-google f-left" title="em-sample-title" href="javascript:void(0)">
                            <span class="fa fa-fw"></span>
                        </a>

                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                    <h1 class="footer_heading no__bottom_space">Toll Free Number</h1>
                    <h3 class="number_free">1800 5328 777</h3>
                    <h1 class="let_heading">LET'S STAY IN TOUCH</h1>
                    <p class="get_txk">Get updates on sales specials and more</p>
                    <div class="block block-subscribe">
                        <form action="" method="post">
                            <div class="block-content">
                                <div class="form-subscribe-content">
                                    <div class="input-box">
                                        <input type="text" name="email" title="Sign up for our newsletter" placeholder="Enter Email Address" id="newsemail" class="input-text required-entry validate-email">
                                    </div>
                                    <p id="returnmsgNew" style="width: 100%;float: left; color: #fff;"></p>
                                    <div class="actions"> <button type="submit" title="Subscribe" class="button" id="newslettersubmit"><span><span>Subscribe</span></span></button></div>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>



                <div class="col-md-12 no_padding">
                    <span class="copyright">Copyright © 2017  Foxsky. All Rights Reserved.</span>
                </div>

            </div>
        </div>
    </div>

    <!-- video popup-->

    <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title txt_pop">Foxsky Video Mobile</h4>
                    </div>
                    <div class="modal-body">
                        <video width="100%" height="400" controls autoplay>
                            <source src="http://192.168.1.30/foxsky/videos/Raazi - Official Trailer (HQ 360p).mp4" type="video/mp4">
                            <source src="movie.ogg" type="video/ogg">
                        </video>

                    </div>

                </div>
            </div>
        </div>
    </div>
