<div class="wrapper sub_nav_bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sub_nav">
                        <div class="pull-left">
                            <h2> </h2>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="wrapper">
	<div class="container-fluid">
	<?php foreach($editreview as $order=>$editreview){?>
		<div class="review_box white_bg">
			<div class="review_left_block">
				<div class="rating_view"><?php echo $editreview->rating/2;?></div>
				<div class="rating_star_view star_padding">

					<div class="rating_empty_star">
						<div class="rating_fill_star" style="width:<?php echo ($editreview->rating*10);?>%"></div>
					</div>
				</div>
				<div class="rating_text"><b><?php echo $editreview->rating/2;?></b> out of 5 stars</div>
			</div>
			<a href="#" class="all_review">Customer Reviews (0)</a>
		</div>


		<!-- Make Comment -->


		<div class="review_box_comment">
			<div class="review__left">
				<form method="post" action="<?php echo base_url(); ?>editreviewupdate">
					<input class="reply_input_1" required="required" name="tag" placeholder="Tag Line" type="text" value="<?php echo $editreview->headline;?>">

					<select name="star" class="reply_input_1">
					<?php for($i=1; $i<=10; $i++){?>
						<option value="<?php echo $i;?>" <?php if($editreview->rating == $i) echo "selected";?> ><?php echo $i;?></option>
					<?php } ?>

					</select>

					<textarea class="reply_input_1 t_code" rows="5" id="comment" name="review" required="required"
					 value=""><?php echo $editreview->review_des;?></textarea>
					<input type="hidden" name="urldata" value="6">
					<input type="hidden" name="proname" value="<?php echo $proname; ?>">
					<input type="hidden" name="ordid" value="<?php echo $ordid; ?>">
					<input type="hidden" name="proid" value="<?php echo $proid; ?>">
					<input type="hidden" name="reviewid" value="<?php echo $reviewid; ?>">
				
					<input class="btn_review_comment" name="editcomments" value="Edit Comments" type="submit">
					<div class="review_successfully"></div>

				</form>
			</div>
			<?php }?>

		</div>
		<!--End Comment -->
         
			</div>
		</div>

		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script>
			$(document).ready(function(){
				$("button").click(function(){
					var rid = $(this).val();
       //alert(rid);
       $.ajax({
       	url: "http://192.168.1.196/foxsky/ajax",
       	async: false,
       	type:"POST",
       	data:'keyword='+$(this).val(),
       	success: function(result){
            // alert(result);
        }});
   });
			});
		</script>

 -->