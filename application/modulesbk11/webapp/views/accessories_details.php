<div class="wrapper sub_nav_bg breadcrumbs"> <div class="pull-right">
              <ul class="sub_tv">

<?php if($producttab->countOverview>0) { ?>
                      <li><a href="<?php echo base_url();?>product/<?php echo $urlname; ?>" class="active-font">Overview</a></li>
                      <?php } ?>
                      <?php if($producttab->countspecification>0) { ?>
                      <li><a href="<?php echo base_url();?>productspecification/<?php echo $urlname; ?>">Specs</a></li> <?php } ?>
                       <?php if($producttab->countacces>0) { ?>
                        <li><a href="<?php echo base_url();?>productaccessories/<?php echo $urlname; ?>">Accessories</a></li><?php } ?>
                        <li><a href="<?php echo base_url();?>review/<?php echo $urlname; ?>">Review</a></li>
                       <!--  <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>">F-code</a></li> -->
                         <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>" class="btn-small btn-orange">Buy Now</a></li>
                       
                    </ul>
              </div>

 <!--  <div class="container-fluid">
      <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
              <a href="#">Home</a><img src="<?php echo base_url();?>assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
                <a href="#">All Products</a><img src="<?php echo base_url();?>assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
                <a href="#">Accessories</a><img src="<?php echo base_url();?>assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
                <a href="#">Bands</a><img src="<?php echo base_url();?>assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
                <span>Foxsky Band Strap</span>
            </div>
        </div>
  </div>     -->
</div>

<div class="wrapper white_bg detail_main">
  <div class="container-fluid">
    <div class="row">
          <div class="col-lg-7">
              <div class="gallery">
          <div class="previews">

<?php  
//p($viewproduct);
//echo $img[$i];
?>

        <a href="javascript:void(0);" data-full="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->Image; ?>"><img src="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->Image; ?>" style="width:566px" /></a>
<?php if($viewproduct->prd_img1!='') { ?>
     <a href="javascript:void(0);" data-full="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->prd_img1; ?>"><img src="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->prd_img1; ?>" style="width:566px" /></a><?php } ?>
      <?php if($viewproduct->prd_img2!='') { ?>    <a href="javascript:void(0);" data-full="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->prd_img2; ?>"><img src="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->prd_img2; ?>" style="width:566px" /></a><?php } ?>
           <?php if($viewproduct->prd_img3!='') { ?>    <a href="javascript:void(0);" data-full="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->prd_img3; ?>"><img src="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->prd_img3; ?>" style="width:566px" /></a><?php } ?>
            <?php if($viewproduct->prd_img4!='') { ?>     <a href="javascript:void(0);" data-full="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->prd_img4; ?>"><img src="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->prd_img4; ?>" style="width:566px" /></a><?php } ?>
         
          </div>
          <div class="full border-none"> 
          <!-- first image is viewable to start --> 
          <img src="<?php echo base_url();?>images/thumimg/<?php echo $viewproduct->Image; ?>" /> </div>
        </div>
            </div>
            <div class="col-lg-5">
              <div class="item_details">
                  <div class="item_head">
                      <h1><?php echo $viewproduct->ProductName; ?></h1></div>
                     <!--  <?php p($viewproduct); ?> -->
                    <div class="details_discount">
                      <span class="dis_off"><?php echo number_format(((1-( $viewproduct->SellingPrice/$viewproduct->productMRP))*100),1) ?>% OFF</span>
                    </div>
                    
                    <div class="item_price">
                      <div class="item_price_value">
                        <i class="fa fa-inr"></i>
                          <span><?php echo $viewproduct->SellingPrice; ?></span>
                        </div>
                        <div class="item_price_low">
                        <del><i class="fa fa-inr"></i>
                          <span><?php echo $viewproduct->productMRP; ?></span><del>
                        </div>
                    </div>
                    
                    <!-- <div class="item_color">
                      <span class="style-name">Colour: Orange</span>
                        <ul class="item_band">
                          <li><a href="#"><img src="<?php echo base_url();?>assets/images/band/small1.jpg" alt="arrow"></a></li>
                            <li><a href="#"><img src="<?php echo base_url();?>assets/images/band/small2.jpg" alt="arrow"></a></li>
                            <li><a href="#"><img src="<?php echo base_url();?>assets/images/band/small3.jpg" alt="arrow"></a></li>
                        </ul>
                    </div> -->
                    
                    <div class="item_delivery">
                      <h6 class="check_title">Delivery to</h6>
                        <div class="check_wrapper">
                        <input class="pincode_detail" type="text" placeholder="Enter your pincode" maxlength="6" autocomplete="off" id="txt_name" required>
                        <a href="javascript:void(0);" class="pincode_btn">Go</a>
                        </div>
                        <div id="movie-data"></div>
                    </div>
                    
                    <div class="cart_btn">
                        <a href="#" class="btn_orange_cart"><i class="fa fa-shopping-cart"></i><span class="cover-btn">Add To Cart</span></a>
                    </div>
                    
                <!--     <div class="more_brand">
                      <div class="left_more"><a href="#"><i class="fa fa-th-large" aria-hidden="true"></i> More Bands</a></div>
                        <div class="right_more"><a href="#"><i class="fa fa-info-circle" aria-hidden="true"></i> Seller information</a></div>
                    </div> -->
                  </div>
            </div>
        </div>
  </div>
</div>




<div class="wrapper">
  <div class="policies_white">
    <div class="container-fluid">
      <div class="row">
            <div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();?>assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();?>assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();?>assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
      </div>
        </div>
  </div>
</div>
</div>


<script>
$(document).on("click",'.btn_orange_cart',function(e){
     var base_url = '<?php echo base_url();?>';
     var id = '<?php echo $id;?>';
    // alert(id);
     $.ajax({
        url:base_url+'webapp/productdetails/checkproavailty',
        type: 'POST',
        dataType: 'json',
        data: {'proId': id},
        success: function(data){
          console.log(data);
          $(".displayincarttotal").html(data['total']+" "+"("+data['TotalInCart']+")");
          $(".cart_number").html("("+data['TotalInCart']+")");
          $(".cart_txt").html(data['total']);
        }
      });    
});

$(document).on("click",'.pincode_btn',function(e){
   var base_url = '<?php echo base_url();?>';
var pincodename = $('#txt_name').val();
  $.ajax({
        url:base_url+'webapp/productdetails/checkincodeavailty',
        type: 'POST',
        dataType: 'json',
        data: {'pincodename': pincodename},
        success: function(data){
//$(".divpost").append(postresult);
if(data==1)
{
  $("#movie-data").html("Pincode Avaiable");
}
else
{

  $("#movie-data").html("Pincode Not Avaiable");
}
           // $("#movie-data").html(data);
        }
      }); 
});
</script>