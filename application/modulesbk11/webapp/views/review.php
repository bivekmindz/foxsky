<div class="wrapper sub_nav_bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sub_nav">
                        <div class="pull-left">
                            <h2><?php echo $response[0]->proname;?> </h2>
                        </div>
                        <div class="pull-right">
                            <ul class="sub_tv">
                               <?php if($producttab->countOverview>0) { ?>
                      <li><a href="<?php echo base_url();?>product/<?php echo $urlname; ?>" >Overview</a></li>
                      <?php } ?>
                      <?php if($producttab->countspecification>0) { ?>
                      <li><a href="<?php echo base_url();?>productspecification/<?php echo $urlname; ?>" class="active-font">Specs</a></li> <?php } ?>
                       <?php if($producttab->countacces>0) { ?>
                        <li><a href="<?php echo base_url();?>productaccessories/<?php echo $urlname; ?>">Accessories</a></li><?php } ?>
                        <li><a href="<?php echo base_url();?>review/<?php echo $urlname; ?>">Review</a></li>
                       <!--  <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>">F-code</a></li> -->
                         <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>" class="btn-small btn-orange">Buy Now</a></li>
                       
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="wrapper">
    <div class="container-fluid">
        <div class="review_box white_bg">
            <div class="review_left_block">
                <div class="rating_view"><?php $rate = $rating[0]->rating/2; echo $rate;?></div>
                <div class="rating_star_view star_padding">

                    <div class="rating_empty_star">
                        <div class="rating_fill_star" style="width:<?php echo ($rate*20);?>%"></div>
                    </div>
                </div>
                <div class="rating_text"><b><?php echo $rate;?></b> out of 5 stars</div>
            </div>
            <a href="#" class="all_review">Customer Reviews (<?php echo ($count[0]->count); ?>)</a>
        </div>


<!-- Make Comment -->
            <?php

            $user = $this->session->userdata('foxsky');
            $email = $user->emailid;
            $id = $user->id;
            $ccount = $ucount[0]->c;

             if(!empty($ccount) && !empty($id)){;?>


                        <div class="review_box_comment">
                        	<div class="review__left">
                            	<form method="post" action="<?php echo base_url();?>addreview">
                            <input class="reply_input_1" required="required" name="tag" placeholder="Tag Line" type="text">

                            <select name="star" class="reply_input_1">
                                <?php for($i=1; $i<=10; $i++){ ?>
                                <option value="<?php echo $i;?>"> <?php echo $i;?> </option>
                                <?php } ?>
                            </select>
                           
							<textarea class="reply_input_1 t_code" rows="5" id="comment" name="review" required="required" placeholder="Write A Comment"></textarea>
                             <input type="hidden" name="oid" value="<?php echo $this->uri->segment(3);?>">
                            <input type="hidden" name="urldata" value="<?php echo $urldata; ?>">
                            <input class="btn_review_comment" value="Add Comments" type="submit">
                            <div class="review_successfully"><?php
            if(!empty($this->session->flashdata('success')))
            {
                echo $this->session->flashdata('success');
            }
?></div>

                        </form>
                            </div>
                        
                        </div>
            <?php }?>
<!--End Comment -->



<?php foreach($response as $key => $value){ ?>

       <div class="review_div fade in active_review_tab" id="review_a">
          <div class="sort_review_box white_bg bottom_pd">
            <div class="review_box_left">

                <div class="review_rate">
                    <div class="rating_empty_star">
                        <div class="rating_fill_star" style="width:<?php echo ($value->rating*10 / 2); ?>%;"></div>
                    </div>
                    <div class="review_date"><?php echo $value->created_at;?></div>
                </div>
                <div class="review_title">
                    <a href="#"><?php echo $value->headline;?></a>
                </div>
                <div class="review_status"><?php echo $value->proname;?></div>
                <div class="review_content"><?php echo $value->review_des;?></div>


                        <div class="review_share_like">
                            <div class="share_to"> Share to </div>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <button name="keyword" value="<?php echo $value->review_id;?>" class="review_like"><i class="fa fa-thumbs-up"></i> Likes(<?php echo ($value->likes); ?>)</button>
                        </div>
                    <?php if($id!=''){;?>
                        <div class="reply_box">
                        <form method="post" action="<?php echo base_url();?>addcomments">
                            <input type="hidden" name="rid" value="<?php echo $value->review_id;?>">

                            <input class="reply_input" required="required" name="comments" placeholder="Write down your reply" type="text">
                            <input type="hidden" name="urldata" value="<?php echo $urldata; ?>">

                            <input class="reply_btn" value="Reply" type="submit">
                        </form>
                        </div>
                    <?php } ?>
                        
                        <ul class="comment_box">
                             <?php 
                                $array = array('act_mode' =>'getproductreviewcomment',
                                'row_id'=>$value->review_id,
                                'param3'=>'',
                                'param4'=>'',
                                'param5'=>'',
                                'param6'=>'',
                                'param7'=>'',
                                'param8'=>'',
                                'param9'=>'',
                                'param10'=>'');
                                $data['comment']=$this->supper_admin->call_procedure('proc_ten',$array);
                                
                                foreach($data['comment'] as $comment=>$comments){
                            ?>
                        <li>
                             <div class="reply-user-img"><img src="assets/images/tv/head-3.png"></div>
                             <h4><?php echo $comments->emailid;?></h4>
                             <p><?php echo $comments->comments;?></p>
                        </li>
<?php } ?>
                     </ul>      

                 </div>


                 <div class="review_box_right">
                    <div class="review_user_detail">
                        <div class="user_img">
                            <img src="assets/images/tv/user.png">
                        </div>
                        <div class="user_name"> <?php echo $response[0]->proname;?></div>
                    </div>
                    <div class="review_user_other_d">
                        <ul>
                            <li>
                                <div class="left_title">Active Since:</div>
                                <div class="right_text">February 26, 2018</div>
                            </li>
                            <li>
                                <div class="left_title">My Reviews:</div>
                                <div class="right_text">
                                    <?php echo ($countcomments[0]->u); ?> reviews
                                      <?php echo ($getccomments[0]->comments); ?> comments
                                </div>
                            </li>
                            <li>
                                <div class="left_title">Helpful Votes:</div>
                                <div class="right_text">
                                  <?php echo ($like[0]->l); ?>
                             </div>
                         </li><!-- getccomments
                         <li>
                            <div class="left_title">Most Recent Review:</div>
                            <div class="right_text">
                               <?php echo $response[0]->proname;?>
                            </div>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

        <div class="review_div fade" id="review_b">

          <div class="sort_review_box white_bg bottom_pd">
            <div class="review_box_left">
                <div class="review_rate">
                    <div class="rating_empty_star">
                        <div class="rating_fill_star" style="width:50%;"></div>
                    </div>
                    <div class="review_date">March 15, 2018</div>
                </div>
                <div class="review_title">
                    <a href="#">Good service</a>
                </div>
                <div class="review_status">Mi LED Smart TV 4A 108 cm (43)</div>
                <div class="review_content">Let me tell you guys how I wound up getting this technological marvel first, 
                My previous 32" TV broke down 9 months ago since then I was looking for a cheap yet the best TV in the market, I even went to showrooms during festival offer time to look if I will get any lu</div>
                        <div class="review_share_like">
                            <div class="share_to"> Share to </div>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="review_like"><i class="fa fa-thumbs-up"></i> Likes(50)</a>
                        </div>
                        <div class="reply_box">
                            <input class="reply_input" placeholder="Write down your reply" type="text">
                            <input class="reply_btn" value="Reply" type="submit">
                        </div>
                    </div>

                    
                    <div class="review_box_right">
                        <div class="review_user_detail">
                            <div class="user_img">
                                <img src="assets/images/tv/user.png">
                            </div>
                            <div class="user_name">Dr Mahesh Patted</div>
                        </div>
                        <div class="review_user_other_d">
                            <ul>
                                <li>
                                    <div class="left_title">Active Since:</div>
                                    <div class="right_text">February 26, 2018</div>
                                </li>
                                <li>
                                    <div class="left_title">My Reviews:</div>
                                    <div class="right_text">
                                        1 reviews
                                        0 comments
                                    </div>
                                </li>
                                <li>
                                    <div class="left_title">Helpful Votes:</div>
                                    <div class="right_text">
                                     40
                                 </div>
                             </li>
                             <li>
                                <div class="left_title">Most Recent Review:</div>
                                <div class="right_text">
                                    Mi LED Smart TV 4A 108 cm (43)
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="review_div fade" id="review_c">
            <div class="sort_review_box white_bg bottom_pd">
                <div class="review_box_left">
                    <div class="review_rate">
                        <div class="rating_empty_star">
                            <div class="rating_fill_star" style="width:50%;"></div>
                        </div>
                        <div class="review_date">March 15, 2018</div>
                    </div>
                    <div class="review_title">
                        <a href="#">Good service</a>
                    </div>
                    <div class="review_status">Mi LED Smart TV 4A 108 cm (43)</div>
                    <div class="review_content">Let me tell you guys how I wound up getting this technological marvel first, 
                    My previous 32" TV broke down 9 months ago since then I was looking for a cheap yet the best TV in the market, I even went to showrooms during festival offer time to look if I will get any lu</div>
                        <div class="review_share_like">
                            <div class="share_to"> Share to </div>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="review_like"><i class="fa fa-thumbs-up"></i> Likes(50)</a>
                        </div>
                        <div class="reply_box">
                            <input class="reply_input" placeholder="Write down your reply" type="text">
                            <input class="reply_btn" value="Reply" type="submit">
                        </div>
                        

                    </div>

                    
                    <div class="review_box_right">
                        <div class="review_user_detail">
                            <div class="user_img">
                                <img src="assets/images/tv/user.png">
                            </div>
                            <div class="user_name">Dr Mahesh Patted</div>
                        </div>
                        <div class="review_user_other_d">
                            <ul>
                                <li>
                                    <div class="left_title">Active Since:</div>
                                    <div class="right_text">February 26, 2018</div>
                                </li>
                                <li>
                                    <div class="left_title">My Reviews:</div>
                                    <div class="right_text">
                                        1 reviews
                                        0 comments
                                    </div>
                                </li>
                                <li>
                                    <div class="left_title">Helpful Votes:</div>
                                    <div class="right_text">
                                     40
                                 </div>
                             </li>
                             <li>
                                <div class="left_title">Most Recent Review:</div>
                                <div class="right_text">
                                    Mi LED Smart TV 4A 108 cm (43)
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>            
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("button").click(function(){
        var rid = $(this).val();
       //alert(rid);
            $.ajax({
            url: "<?php echo base_url();?>ajax",
            async: false,
            type:"POST",
            data:'keyword='+$(this).val(),
            success: function(result){
            // alert(result);
            }});
    });
});
</script>

