<div class="wrapper sub_nav_bg breadcrumbs">
	<div class="container-fluid">
    	<div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
            	<a href="#">Home</a><img src="assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
                My account
            </div>
        </div>
	</div>  	
</div>

<div class="wrapper my_account">
	<div class="container-fluid">
    	<div class="row">
        	<div class="my_acount_main">
            	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                	<div class="my_nav">
                    
                    	<h3 class="my_title">ORDERS</h3>
                        	<ul class="order_menu">
                            	<li><a href="<?php echo base_url();?>myorder">My Orders</a></li>
                                <li><a href="#">Returns</a></li>
                            </ul>
                       
                        <h3 class="my_title">Foxsky</h3>
                        	<ul class="order_menu">
                            	<li><a href="<?php echo base_url();?>myaccount">My Account</a></li>
                                <li><a href="<?php echo base_url();?>myreview">Reviews</a></li>
                                <li><a href="#" class="current_active">Address Book</a></li>
                                
                            </ul>
                 
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no_padding">
                	<div class="my_right">
                    	<h1>MY ADDRESS BOOK</h1>
                    		<div class="section-address my-address">
        						<div class="row">
                   	 				<!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        				<div class="address-list" data-toggle="modal" data-target="#Checkout">
                        					<a href="#" class="address-pop"><div>+</div>
                            					<div>Add New Address</div>
                            				</a>
                        				</div>
                    				</div> -->
<!-- <?php echo base_url(); ?>webapp/myaccount/deleteaddress/<?php echo $custdetails->shippingid ;?> -->
                                    <?php foreach($details as $order=>$custdetails){ ?>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="address-list address-hide">
                                    	<p class="ad_01"><b>NAME :<?php echo $custdetails->shipname;?></b></p>
                                    	<p class="ad_01">Contact :<?php echo $custdetails->shipcontact;?></p>
                                    	<p class="ad_01">Shipping Address :<?php echo $custdetails->shipaddress;?></p>
                                        <p class="ad_01">City :<?php echo $custdetails->shipcity;?> , Pin :<?php echo $custdetails->shippincode;?></p>
                                        <!-- <p class="add_default">DEFAULT</p> -->
                                    	<p class="ad_01 edit_add">
                                        	<a href="#" data-toggle="modal"  onClick="getDetails(<?php echo $custdetails->shippingid;?>);">
                                                EDIT</a>
                                        	<a href="#" onClick="deletedetails(<?php echo $custdetails->shippingid;?>);">DELETE</a>
                                        </p>
                                        
                                    </div>
                                    </div>
                                    <?php }?>
                  			</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper">
    <div class="policies_white">
        <div class="container-fluid">
            <div class="row">
                <div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="container">
    <div class="modal fade" id="checkoutEdit" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title mar-left">New Address</h4>
            </div>
            <div class="modal-body pop_padding">
              <form method="post" action="" id="shipaddress">
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipname_edit" name="shipname" required placeholder="Name">
                    <input type="hidden" class="form-control form-check" id="shippingid_edit" name="shippingid" required placeholder="Name">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" onkeypress="return isNumberKey(event);" maxlength="10" minlength="6" id="shippincode_edit" name="shippincode" required placeholder="Pin-code">
                  </div>
                  <p>Please make sure your address is accurate. This cannot be changed once order is placed.</p>
                  <div class="form-group">
                      <textarea class="form-control" rows="2" id="address_edit" name="address" required placeholder="Address"></textarea>
                  </div>  
              
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="landmark_edit" name="landmark" required placeholder="Landmark">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipcity_edit" name="shipcity" required placeholder="City">
                  </div>
                  <div class="form-group">
                      <select class="form-control form-check" name="stateid" required id="shipstate_edit">
                          <option value="">State</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipemailid_edit" name="shipemailid" required placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipcontact_edit" onkeypress="return isNumberKey(event);" maxlength="15" name="shipcontact" required placeholder="Phone">
                  </div>
                  <div class="form-group form_gray_bg">
                    <a href="#" class="cancel_btn">Cancel</a>
                    <input type="submit" name="submit" value="Confirm" class="cancel_btn orage_cancle"/>
                  </div>  
                </form>
            </div>
        </div>  
      </div>
    </div>
  </div>
<div class="container">
    <div class="modal fade" id="Checkout" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title mar-left">New Address</h4>
            </div>
            <div class="modal-body pop_padding">
              <form method="post" action="" id="shipaddress">
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipname" name="shipname" required placeholder="Name">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" onkeypress="return isNumberKey(event);" maxlength="10" minlength="6" id="shippincode" name="shippincode" required placeholder="Pin-code">
                  </div>
                  <p>Please make sure your address is accurate. This cannot be changed once order is placed.</p>
                  <div class="form-group">
                      <textarea class="form-control" rows="2" id="address" name="address" required placeholder="Address"></textarea>
                  </div>  
              
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="landmark" name="landmark" required placeholder="Landmark">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipcity" name="shipcity" required placeholder="City">
                  </div>
                  <div class="form-group">
                     <select class="form-control form-check" name="stateid" required id="stateid">
                        <option value="">State</option>
                        <?php foreach($state as $key => $val) { ?>
                          <option value="<?php echo $val->stateid;?>"><?php echo $val->statename;?></option>
                        <?php } ?>
                      </select>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipemailid" name="shipemailid" required placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipcontact" onkeypress="return isNumberKey(event);" maxlength="15" name="shipcontact" required placeholder="Phone">
                  </div>
                  <div class="form-group form_gray_bg">
                    <a href="#" class="cancel_btn">Cancel</a>
                    <input type="submit" name="submit" value="Confirm" class="cancel_btn orage_cancle"/>
                  </div>  
                </form>
            </div>
        </div>  
      </div>
    </div>
  </div>
<script>
    function getDetails(ship_id){
        $.ajax({
          type: "POST",
          url: "webapp/cart/getshipdetails",
          data: "ship_id=" + ship_id,
          dataType: "json",
          success: function (data) {
              console.log('Data',data.shipname);
              $("#shipname_edit").val(data.shipname);
              $("#shippincode_edit").val(data.shippincode);
              $("#address_edit").val(data.shipaddress);
              $("#landmark_edit").val(data.landmark);

              $("#shipcity_edit").val(data.shipcity);
              $("#shipemailid_edit").val(data.shipemailid);
              $("#shipcontact_edit").val(data.shipcontact);
              $("#shipstate_edit").html(data.shipstate);
              $("#shippingid_edit").val(data.shippingid);
          }
        });

        $("#checkoutEdit").modal('show');
    }

     function deletedetails(ship_id){
     // var x = <?php echo $custdetails->shippingid;?>;
      // alert(ship_id);
        $.ajax({
          type: "POST",
          url: "webapp/myaccount/deleteaddress",
          data: 'ship_id='+ship_id,
          success: function(data){
            alert (data);
          }
        });
    }
</script>