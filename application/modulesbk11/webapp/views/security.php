<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Security</title>
		<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/app_css.css">
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/animate.min.css">
        
</head>
<body>
   <div class="container">
   		<div class="row">
            <div class="profile_main">
            <div class="profile_header">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                	<div class="profile-logo"><img src="assets/images/logo.png" alt=""></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                	<a href="<?php echo base_url();?>logout" class="signout">Sign out</a>
                </div>
             </div>
             
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="n-main-nav">
                    <li><a href="my-security.html" class="active-orage">Security</a>
                    <div class="arrow_box"></div>
                    </li>
                    <li><a href="<?php echo base_url();?>myprofile">Personal info</a></li>
                    
                </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               	 	<div class="n-account-area-box">
                    	<div class="n-account-area">
                        	<div class="na-info">
								<p class="na-name">Set nickname</p>
								<p class="na-num"><?php echo $profile->Name;?></p>
		  					</div>
                            <div class="n_account_pic"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $profile->userimage;?>" alt=""></div>
                        </div>
                	</div>
                </div>
                
                <div class="n_frame">
                
                <div class="Security_count">
                <h4 class="title-big">Security check-up</h4>
                <p class="security_p"><span class="score_2">60</span>Points</p>
                
                <div class="slider-area">
                	<div class="slider-bar-bg"></div>
                	<div class="slider-bar-line score_bg_2" style="width:60%;"></div>
                	<div class="drag-ico" style="left:60%"></div>
                </div>
                <p class="dis-inb"><span>2</span>risks detected</p>
                </div>
                
                	<div class="change_Password">
                    	<div class="change_icons"><img src="assets/images/change_icon.png" alt=""></div>
                        <div class="change_txt_box">
                        	<h2>Password</h2>
                            <p>Set a strong password to keep your account secure</p>
                        </div>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#chang_password">Change</a>
                    </div>
                    
                </div>
           </div>
            	
           <div class="policy-area">
                	<ul class="policy-select-list">
                    	<li><a href="#">English</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="<?php echo base_url();?>privacy">Privacy Policy</a></li>
                    </ul>
                    <p class="reserved-intro">Foxsky Inc., All rights reserved</p>
                </div>
        	</div>
   		</div> 
   
<div class="container"> 
  <div class="modal" id="chang_password" role="dialog">
    <div class="modal-dialog modal_dialog03">
    
      <!-- Modal content-->

      <div class="modal-content modal_content02">
        <div class="modal-header modal_header02">Change password
          <button type="button" class="close close02" data-dismiss="modal">&times;</button>
        </div>
        <form method="post" action=""> 
        <div class="modal-body modal_body02">
          <div class="Editpersanal">
          	<div class="form-group">
            	<div class="col-lg-12"><span class="new_passwrod">Current password</span></div>
                <div class="col-lg-12"><input type="password" class="input-verification" placeholder="Enter password"  name="cpassword"></div> 
            </div>
            
            <div class="form-group">
            	<div class="col-lg-12"><span class="new_passwrod">New password</span></div>
                <div class="col-lg-12"><input type="password" class="input-verification c-m" placeholder="New password" id="password" name="npassword"></div> 
                <div class="col-lg-12"><input type="password" class="input-verification" placeholder="Confirm password" id="repassword" name="rpassword" onblur="checkrepass();">
                <span class="repasserror"></div> 
            </div>
            
            <p>Password must be 8-16 characters long and include at least two of the following: letters, numbers, or symbols.</p>
            
            
          </div>
                        
            <input type="submit" name="submit" class="nb_submit font-14" value="Ok" /> 
            <button type="button" class="nb_submit gray_border font-14" data-dismiss="modal">Cancel</button>
            
            <div class="Privacy_Policy">
            	<a href="<?php echo base_url();?>privacy">Privacy Policy</a>
            </div>
            
            
        </div>
        </form>
      </div>
      
    </div>
  </div>
  
</div>



		<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
  		<script>new WOW().init();</script>

		
   
</body>
</html>
<script type="text/javascript">
     function checkrepass()
     {
      var pass=$('#password').val();
      var repass=$('#repassword').val();
     
      if(repass!=pass)
        {
          //alert('hghg');
          $('.repasserror').html('Re-password should be same as password').css('border-color','red');
          // $('#repassword').focus();
           return false;
        }
        else
        {
          $('.repasserror').html('').css('border-color','green');
        }
     }


   </script>