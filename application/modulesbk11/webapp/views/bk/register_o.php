 <div class="container">
   		<div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<div class="layout_sign-in sign-up">
                	<div class="lgnheader">
                    	<div class="lgnheader-logo"><img src="<?php echo base_url();  ?>assets/webapp/images/logo.png" alt=""></div>
                        <h4 class="header_tit_txt">Create Foxsky Account</h4>
                        <div class="tabs-login">
                        
                        
                        
                        	<div class="login_area">
                                  <form method="post" action="" id="addCont1">
                                <div class="form-group select-regions">
                                <label class="form-txt"><span>City</span></label>
                               	<select class="selectpicker select-contry" data-live-search="true" name="city">
                                <?php  foreach ($viewshipstate as $key => $value) { ?>
                              	<option  value="<?php echo $value->stateid;   ?>"><?php echo $value->statename;  ?></option>
                             <?php    }  ?>
                              
                            	</select>
								</div>
                                
                                <div class="phone_step1">
                                	<div class="region_tip_text">You won't be able to change your region after you create your account.</div>
                                    <div class="form-group form-box">
                                  	<label class="form-txt">Mobile number</label>
                                    <div>
                                    	<div class="number-country">
                                        	<div class="select-regions selecty-number-all">
                                                <select class="selectpicker select-contry" data-live-search="true">
                                                <option data-tokens="ketchup mustard">+ 91</option>
                                              
                                              </select></div>
                                        </div>
                                        <div class="number-fild"><input type="mobile" class="login-input border-right-none" id="mobile" placeholder="Enter phone number" onblur="regmobileotp();"></div>
                                    </div>
                                  </div>
                                    <div class="form-group form-box" id="otpmobile" style="display:none">
                                     <p>One Time Password (OTP) has been sent to your Mobile. please Verify Your Mobile Number.
                        </p>
                                      <label class="form-txt">Enter Otp</label>
                                      <div class="capt-code"><input type="password" class="login-input" id="otpmob" placeholder="Enter otp" name="otpmob"></div>
                                      <input type="hidden" class="login-input" id="otpmobhide" placeholder="Enter otp" name="otpmobhide">
                                      <input type="button" class="rsnd-otp" onclick="regmobileotp();" value="Resend OTP">
                                     
                                  </div>
                                  
                                      <div class="form-group form-box"  >
                                      <label class="form-txt">Captcha</label>
                                      <div class="capt-code"><input type="password" class="login-input" id="captcha" placeholder="Enter code" name="captcha"></div>
                                      <div class="capt-img"><img src="<?php echo base_url();  ?>captcha.php" alt=""></div>
                                        
                                  </div>
                                  
                                </div>
                                
                                  <div class="form-group">
                                  <label class="form-txt">Email</label>
                                    <input type="email" class="login-input" id="email" name="email" placeholder="Enter email address">
                                  </div>
                                  <div class="form-group btns_bg">
                                  <input type="submit" class="btnadpt btn_orange btn_orangeH" value="Create Foxsky Account"></input>
                                  </div>
                                </form>
                                <div class="facebool-login click-btn">
                                    <a href="#" class="btnadpt btn_reg_1">Create using a phone number </a>
                                </div>
                        	</div>
                        </div>
                    </div>
                   
                   
                   	<div class="privacy_box">
                    	
                    	<label class="redio-f">Stay up to date with news and offers from the Mi Store. 
                      		<input type="checkbox" checked>
                      		<span class="checkmark"></span>
                    		</label>
                            
                            
                            <label class="redio-f">By creating an account, you are agreeing to our <a href="#">User Agreement</a> and <a href="#">Privacy Policy.</a>
                      		<input type="checkbox" checked>
                      		<span class="checkmark"></span>
                    		</label>
                                        
                   	
                    </div>
                  
                    
                </div>
            	
                <div class="policy-area">
                	<ul class="policy-select-list">
                    	<li><a href="#">English</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                    <p class="reserved-intro">Foxsky Inc., All rights reserved</p>
                </div>
                
            </div>
        </div>

        <script>
        $(document.ready(function()
        {
          $('#addCont1').validate({
            rules: 
            {
              city  : "required",
              email:{
                      required: true,
                      email: true
                    },
},
            }

          });

        }));


        </script>

        <script type="text/javascript">
          function regmobileotp()
          {
            var umob=$('#mobile').val();
            alert(umob);
            $.ajax({
              url: '<?php echo base_url();  ?>webapp/memberlogin/verifyotp',
              type: 'POST',
              data: {umob:umob},
               success:function(data){
                alert(data);
                $('#otpmobile').show();
                $("#otpmobhide").val(data);

               }


            });

            
          }


        </script>