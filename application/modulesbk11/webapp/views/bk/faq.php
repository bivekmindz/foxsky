<div class="product_bg">
 <div class="product_banner"><img src="<?php echo base_url(); ?>assets/webapp/images/faq.jpg"  alt="" /></div>
     
</div>


	<div class="container">
    <div class="col-lg-12 col-md-12 col-lg-12">
        <div class="faq_ul">
            <ul><li data-faq="about_faq" class="active_f">About Bizzgain</li>
            <li data-faq="sell_biz">Sell on Bizzgain</li>
            <li data-faq="b_part">Become a Bizzgain Partner/Start your own Business</li></ul>
        </div>
    </div>
    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="row">
            	<div class="static_page">
                <div id="about_faq" class="faq_hid">
                	<h1>ABOUT BIZZGAIN</h1>
                		<div class="faq_box">
                        	<div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            	<div class="row">
                            		<p>Question</p>
                            </div>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                            	<div class="faq_q">
                            		<p>What is Bizzgain?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        
                        <div class="faq_box">
                        
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Bizzgain is an online toys & office stationery wholesalers platform for purchase and sale of the various type of toys (including soft toys, small toys & stuffed toys for kids), office supplies, stationery at wholesale prices. It offers complete information of products being offered by various manufacturers / wholesalers at competitive prices for various locations depending upon the delivery place.</p>
                                </div>
                            </div>
                    	</div>
                        
                        <div class="faq_box">
                        <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>How do I use Bizzgain?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>You need to register yourself as a new customer, complete the details asked for including Name, Contact no. Email-id, Address, Shop name etc. in the registration page. Search for toys or stationery in bulk that you are looking to buy, add to cart the interested products, select payment options, and delivery address. Rest of the procurement services and delivery process will be taken care of us.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                        <div class="faq_box">
                        <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>I’m not a Retailer or a Wholesale business owner, can I still buy from Bizzgain?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Yes, you can buy as a consumer. You need to register yourself as Consumer. The only difference in services of Retailer and Consumer is that Consumer would have to pay for Bizzgain toys & stationery in advance and shipping charges are extra in case of the city currently not being served by us.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                         <div class="faq_box">
                         <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Can I order from Bizzgain from anywhere in the country (India)?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Yes, you can order toys in bulk from anywhere in the country. Delivery time and shipping charges shall vary according to the place of delivery.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                        
                         <div class="faq_box">
                         <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Are there any extra shipping charges?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Yes, products in bulk and location of delivery drive the prices for shipping at our platform.</p>
                                </div>
                            </div>
                    	</div>
                        
                                         <div class="faq_box">
                                         <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>What is the minimum amount of products or order for Wholesalers/ Retailers / Individuals?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>INR 5,000/- (Indian Rupees Five Thousand only) is the minimum amount of products anyone needs to buy at our platform.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                                      <div class="faq_box">
                                      <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Is there any membership fees to sign up on Bizzgain?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>No. There are no registration charges or sign up fees. It is absolutely free of cost.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                                  <div class="faq_box">
                                  <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>What all payment modes are available?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>There are various payment options available on our website including Cash On Delivery, Cheque on Delivery, Internet banking, Direct debit, Debit card, Credit Card, Credit etc. Restrictions are applicable based on city of delivery, product and customer profile.</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        <div id="sell_biz" class="faq_hid">
                        <h2>SELL ON BIZZGAIN</h2>
                        
                            <div class="faq_box">
                            <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Why should I sell on Bizzgain?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Bizzgain gives you a much customized approach to target toy wholesalers / retailers / large volume buyers across India on a click of a button. It not only provides you complete fulfillment services but also enables payment and trade assurance. You can upload your products based on quantity at differential pricing, which is not offered by any platform till date. Furthermore, it gives you an opportunity to offer differentiated pricing for different cities at the same time. The platform comes with great flexibility to adapt to your working conditions and we offer full support in loading / editing of information with value added services including product shoots and customer care support.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                           <div class="faq_box">
                           <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>What are the documents required to become a seller on Bizzgain?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Standard KYC documents, canceled cheque copy, pan card, shop registration certificate, TIN No., VAT registration certificate and other related documents.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                          <div class="faq_box">
                          <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Do I get charged to put my products on Bizzgain?</p>
                                </div>
                            </div>
                    	</div>
                        
                        </div>
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>The listing, display of products and related product information is offered at zero cost. We charge on the basis of the successful transaction being consummated from our platform at a very nominal rate.</p>
                                </div>
                            </div>
                    	</div>
                        
                                 <div class="faq_box">
                                 <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Who decides the price of my products?</p>
                                </div>
                            </div>
                    	</div>
                        
                        </div>
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>If Bizzgain is purchasing the product and selling, then Bizzgain has a right to decide the selling price. If you are uploading the products as wholesale toys suppliers in India to sell directly to our customers then you decide the selling price and we only charge commissions as per our agreements.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                               <div class="faq_box">
                               <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>How many minima and a maximum number of products can I upload?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>There is no limit on a number of products you can upload.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                            <div class="faq_box">
                            <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>What is my selling commission?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Based on category and quantity of products, selling commission is decided. Further, it is related to relationship and brand image of the product and company.</p>
                                </div>
                            </div>
                    	</div>
                        
                                      <div class="faq_box">
                                      <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Who will deliver my products?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Bizzgain shall fulfill the complete procurement process based on terms with the seller.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                                         <div class="faq_box">
                                         <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>How do I manage my orders on Bizzgain?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>A dashboard shall be given to each seller (toys supplier or wholesaler) where they can manage their orders, check the status of delivery, payments etc.</p>
                                </div>
                            </div>
                    	</div>
                        
                        
                                                      <div class="faq_box">
                                                      <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>How do I know whether my area is serviceable under Bizzgain?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Intimation will come on the page if you are not covered under the Bizzgain serviceable area. We shall make all efforts to each out to you as soon as possible.</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        <div id="b_part" class="faq_hid">
                        
                         <h2>BECOME A BIZZGAIN PARTNER / START YOUR OWN BUSINESS </h2>
                                                          <div class="faq_box">
                                                          <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Why should I become a Bizzgain Partner?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>There are various advantages, which Bizzgain platform is offering to its partners including but not limited to:</p>
                                	<ul>
                                    			
                                                <li>First trading platform for wholesale Toys & Stationery in India</li>
                                                <li>Access to products at Factory prices</li>
                                                <li>Large orders execution with Manufacturers directly</li>
                                                <li>Transparent and right information</li>
                                                <li>80+ brands and 10,000+ product listing in one platform</li>
                                                <li>Complete marketing and technical support</li>
                                                <li>We sell, you deliver and get commission</li>
                                    </ul>
                                
                                </div>
                            </div>
                    	</div>
                        
                        
                                                     <div class="faq_box">
                                                     <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>What cost is involved in me signing up as a partner?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>There is no cost involved in signing up with Bizzgain as partner</p>
                                </div>
                            </div>
                    	</div>
                        
                                                       <div class="faq_box">
                                                          <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                         
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>What is the investment required to be done to become Bizzgain Partner?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Total Investment of INR 6,50,000 is required to be done as below:</p>
                                	<ul>
                                    	<li>INR 150,000/- (Indian Rupees One Lakh Fifty Thousand only) towards refundable security deposit</li>
                                        <li>INR 500,000/- (Indian Rupees Five Lakhs only) towards stock (adjustable basis on inventory uptick in the region, based on 2 months sales inventory)</li>
                                        <li>500-1,000 sq ft of warehouse space</li>
                                    </ul>
                                
                                </div>
                            </div>
                    	</div>
                        
                                                     <div class="faq_box">
                                                     <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>What are the requirements for becoming a partner?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Business acumen, eager to grow, positive attitude, open to work, Investment, Basic infrastructure are among few pre-requisite to become Bizzgain partner</p>
                                </div>
                            </div>
                    	</div>
                        
                                               <div class="faq_box">
                                               <div class="qus">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Question</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>What are the marketing and technical support being provided by the company?</p>
                                </div>
                            </div>
                    	</div>
                        </div>
                        
                        <div class="faq_box">
                    		<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            <div class="row">
                            	<p>Answer</p>
                            </div>
                            </div>
                           <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                           		<div class="faq_q">
                            	<p>Bizzgain shall provide you with brochures, pamphlets, stationery and many other promotional materials at its discretion. All these would be free of charge. Further, Bizzgain shall give its partners technical support in the form of the web platform, which shall be used by its partner to increase sales and know about various programs being promoted by Bizzgain. Also, this platform shall ensure the information of all products like office stationery, stuffed toys, soft toys, etc. that Bizzgain’s partner can sell, the incentive being earned, new products being launched etc.</p>
                                </div>
                            </div>
                    	</div>
                        
                        </div>
                        
                        
                        
                        
                        
                        
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
    .faq_hid{
        display: none;
    }
    </style>
     <script type="text/javascript">

        $(document).ready(function(){
          
          $("#about_faq").fadeIn();
       $(".faq_ul ul li").click(function(){
        var op_tab = $(this).attr("data-faq");
        $(".faq_ul ul li").removeClass("active_f");
        $(this).addClass("active_f");
        $(".faq_hid").hide();
        //alert();
        $("#"+op_tab).fadeIn();
       });
        });
         
    </script>
