 <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border-bottom:1px solid red!important;}
   /* select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }*/
    .sessionerror{border:0px solid red!important; color:red; font-weight: normal; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules

          rules: {
            emailmob     : "required",
            acctype      : "required",
            pwd          : {
                              required  : true,
                              minlength : 6
                           },
            
        },
        // Specify the validation error messages
       /* messages: {
            emailmob      : "Email / Mobile Number is required",
            acctype       : "Choose Account type",
            pwd           : {
                              required: "Password is required",
                              minlength: "Your password must be at least 6 characters long"
                            },
            
        },*/
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>
<?php if($this->session->flashdata('item')){ ?>
 <div class="pl_login">
    <i class="fa fa-times close_pl" onclick="window.location.href=window.location.href"> </i>
    <?php echo $this->session->flashdata('item'); ?></div>
          
<?php } ?>

 <div class="close_sign"></div>
        <div class="left_login_pop">
            <div class="login_logo">
                <img src="<?php echo base_url(); ?>assets/webapp/img_hm/logo-02.png">
            </div>
            <div class="social_l_pop">
                <h4>Login with Social</h4>
                <a href="#" class="facebook"><i class="fa fa-facebook-square" aria-hidden="true"></i>Login with facebook</a>
                <a href="#" class="google"><i class="fa fa-google-plus-square" aria-hidden="true"></i>Login with Google</a>
            </div>
        </div>
        <div class="right_login">
              <form method="post" action="<?php echo base_url('login')?>" id="addCont"> 
              <h3>Login</h3>
                <div class="sessionerror"><?php echo $this->session->flashdata('message'); ?></div>
                <div class="form_div">
                    <input type="text" name="emailmob" id="emailmob" class="input_text"  required>
                    <label>Enter Email / Username</label>
                </div>
                <div class="form_div">
                     <input type="password" class="input_text" id="pwd" name="pwd"  required>
                    <label>Password</label>
                </div>
                <div class="form_div">
                      <input type="hidden" class="input_text" name="current_url" value="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
                      <!-- <input type="radio" id="acctype" name="acctype" value="consumer" />  Consumer &nbsp;&nbsp;&nbsp; --><input type="hidden" id="acctype" name="acctype" value="retail" /><!--   Retailer &nbsp;&nbsp;&nbsp; -->
                       </div>
                <div class="form_div">
                    <div class="submit_d_pop">
                        <input type="submit" id="submit" name="submit" value="LOGIN" class="submit_btn">
                    </div>
                    <div class="forgot_p_pop" ><span class="m_popup" data-sh="forgot_pwd" data-hi="login" style="cursor: pointer;">Forgot password ?</span></div>
                </div>
                <div class="form_div">
                    <div class="create_text">
                        <span class="m_popup"><a href="<?php echo base_url('register');?>" class="m_popup"> Create New Account</a></span>
                    </div>
                </div>
</form>        </div>



<script type="text/javascript">
  function statecountry(){
    var  stid=$('#mem_shipstate').val();
    $.ajax({
    url: '<?php echo base_url()?>webapp/memberlogin/citystate',
    type: 'POST',
    data: {'stid': stid},
    success: function(data){
      //console.log(data);
      //alert(data);return false;
    var  option_brand = '<option value="">Select City</option>';
    $('#mem_shipcity').empty();
    $("#mem_shipcity").append(option_brand+data);
    }
    });
  }
</script>

