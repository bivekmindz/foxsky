


<div class="wrapper sub_nav_bg">
	<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="sub_nav">
            
            <div class="pull-left">
            <h2><?php echo $ProductName;   ?></h2>
            <?php  if(!empty($other)) { ?>
                <ul class="sub_title">
                    	<li><a href="#">|</a></li>
                    	<li><a href="#">Mi LED Smart TV 4A 43</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="#">Mi LED Smart TV 4 32</a></li>
                     </ul>
                     <?php   } ?>
            </div>

			<div class="pull-right">
            	<ul class="sub_tv">
             
                    	<li><a href="#">Overview</a></li>
                    	<li><a href="<?php echo base_url().'productspec/'.$ProductName.'-'.$ProId.'.html';?>">Specs</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Review</a></li>
                        <li><a href="#">F-code</a></li>
                        <li><a href="#" class="btn-small btn-orange">Sale @ 12 PM, 24th April</a></li>
                    </ul>
            	</div>
              </div>
            </div>
        </div>
    </div>
</div>		
        
       
<div class="wrapper overall_tv_one">
	<img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-top-bg.jpg" alt="">
    	<div class="container">
			<div class="title_tv_main">
          			<h1 class="sub_title_tv wow fadeInUp animated">Mi LED Smart TV <span class="red_tv"> 4 </span><span class="small_tv">138.8cm (55)</span></h1>
          			<p class="title_tv wow fadeInUp animated">World's<br> thinnest <br>LED TV</p>
          			<p class="xm-pro-price wow fadeInUp animated"><i class="fa fa-inr"></i><span class="xm-price">39,999</span></p>
        </div>    
	 </div>
	</div>
    
		     
<div class="wrapper videoTv_section">
	<div class="container-fluid">
    	<div class="row">
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<div class="videos_adds">
                    <a href="#"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/video_tv1.png" alt="">
                    <span class="icon_tv_control"></span>
                    <p class="tv_product_title wow fadeInUp animated">Product Video</p>
                   </a>
               </div>
            </div>
            
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<div class="videos_adds">
                    <a href="#"><img src="<?php echo base_url();  ?>assets/images/webapp/tv/video_tv2.jpg" alt="">
                    <span class="icon_tv_control"></span>
                    <p class="tv_product_title wow fadeInUp animated">The story behind</p>
                   </a>
               </div>
            </div>
            
            
            
        </div>
    </div>
</div> 
		 
 
<div class="tv_slider">
  <div class="tv_slider_heading">
        <p class="tv_slider_desc wow fadeInUp animated">World's thinnest LED TV</p>
        <h1 class="tv_slider_title wow fadeInUp animated">4.9mm Ultra-thin</h1>
        <p class="tip"></p>
      </div>
  <div id="myCarousel" class="carousel slide height-auto" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv_slider01.png" alt="" width="100%">
      </div>

      <div class="item">
        <img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv_slider02.png" alt="" width="100%">
      </div>
    
      <div class="item">
         <img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv_slider03.png" alt="" width="100%">
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control slider_none" href="#myCarousel" data-slide="prev">
      <span class="arrow-prev"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/arrow-prev.png" alt=""></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control slider_none" href="#myCarousel" data-slide="next">
      <span class="arrow-next"><img src="<?php echo base_url();  ?>assets/images/webapp/tv/arrow-next.png" alt=""></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>


<div class="wrapper tv_4">
	<div class="container-fluid">
    <div class="row">
	<div class="display_tv">
        <p class="wow fadeInUp animated">Immersive viewing experience</p>
        <h1 class="wow fadeInUp animated">Frameless display</h1>
    </div>
    </div>
    </div>
    <div class="image_foxsky_tv">
    	<img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-bezelless-tv-light.png" alt="">
    </div>
    
</div>

<div class="wrapper section_audio">
<div class="container-fluid">
<div class="row">
	<div class="display_tv">
        <p class="wow fadeInUp animated">Cinema quality sound, now in your living room</p>
        <h1 class="wow fadeInUp animated">Dolby+DTS</h1>
    	</div>
  	</div>
  </div>
</div>


<div class="wrapper section_performance">
<div class="container-fluid">
	<div class="row">
		<div class="display_tv">
        	<p class="wow fadeInUp animated">Flagship performance</p>
        	<h1 class="wow fadeInUp animated">64 bit Quad-core<br>2GB<span class="ram">RAM</span> + 8GB<span class="ram">Storage</span></h1>
    	</div>
  	</div>
 </div>
</div> 

<div class="wrapper section-params">
	<div class="container-fluid">
    	<div class="row">
        <div class="blocks1">
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<figure>
                	<div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/ports.png" alt=""></div>
                    <div class="thumbnail img_thumbnail"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-system-img2.jpg" alt=""></div>
                </figure>
            </div>
            
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<div class="block-item">
                	<div class="system-icon"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-bluetooth-icon.png" alt=""></div>
                    <p class="wow fadeInUp animated">Bluetooth 4.0<br>Low energy</p>
                </div>
                
                
                <div class="block-item wifi">
                	<div class="system-icon"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-wifi-icon.png" alt=""></div>
                    <p class="wow fadeInUp animated">Wi-Fi<br>2.4GHz / 5GHz</p>
                </div>
            	
            </div>
            </div>
        </div>
    </div> 	
</div>


<div class="wrapper section-source">
<div class="container-fluid">
	<div class="row">
		<div class="display_tv">
        	<p class="wow fadeInUp animated">PatchWall: Revolutionary TV experience</p>
        	<h1 class="wow fadeInUp animated">500,000+ hours of content</h1>
    	</div>
  	</div>
 </div>
 <div class="person"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/people.png" alt=""></div>
</div> 


<!-- logo slider -->
<div class="logo-slider">	
<ul class="bxslider">
  <li><img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv-logo1.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv-logo2.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv-logo3.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv-logo4.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv-logo5.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv-logo6.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv-logo7.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv-logo8.png" alt=""></li>

</ul>

        
    <p class="tip-logo wow fadeInUp animated">Subscriptions/charges may be required for certain content. </p>
</div>

<div class="wrapper section-cup">
	<div class="container-fluid">
		<div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="overall-cup-img">
                    <img src="assets/images/tv/overall-cup-img.jpg" alt="">
                </div>
                <div class="cplive">
                   <p class="more-title">MOBILE AND INTERNET BROADCASTER</p>
                   <p class="desc wow fadeInUp animated">2018 FIFA World Cup Russia <sup>TM</sup> </p>
                   <p class="tip">* Watch 2018 FIFA World Cup Russia in India on SONY LIV on Mi TV</p>
                 </div>
                 
            </div>
        </div>
    </div>
</div>


<div class="wrapper section-system-1">
	<div class="container-fluid">
		<div class="row">
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<div class="block_section">
                 	<div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/recommend.png" alt=""></div>
                 	<div class="text_recommeds">
                    	<p class="wow fadeInUp animated">PatchWall recommends<br>content that you'll love</p>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<div class="block_section">
                 	<div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/search.png" alt=""></div>
                 	<div class="text_recommeds">
                    	<p class="wow fadeInUp animated">Easily discover content<br>across all Apps</p>
                    </div>
                </div>
            </div>
            
        </div>
	</div>
</div>


<div class="wrapper section-system-2">
	<div class="container-fluid">
		<div class="row">
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<div class="block_section">
                 	<div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/screen.png" alt=""></div>
                 	<div class="text_recommeds s2">
                    	<p class="wow fadeInUp animated">Supports<br>screen mirroring</p>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<div class="block_section">
                 	<div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/remote.png" alt=""></div>
                 	<div class="text_recommeds s2">
                    	<p class="wow fadeInUp animated">Control the TV<br>with your phone</p>
                        <p class="tip wow fadeInUp animated">*Supported on Android phones on same Wi-Fi network.</p>
                    </div>
                </div>
            </div>
            
        </div>
	</div>
</div>

<!-- slider part -->
<div class="tv_slider">
  <div id="myCarouse2" class="carousel slide height-auto" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarouse2" data-slide-to="0" class="active"></li>
      <li data-target="#myCarouse2" data-slide-to="1" class=""></li>
      <li data-target="#myCarouse2" data-slide-to="2" class=""></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv_slider4.png" alt="" width="100%">
      </div>

      <div class="item">
        <img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv_slider5.png" alt="" width="100%">
      </div>
    
      <div class="item">
         <img src="<?php echo base_url();  ?>assets/webapp/images/tv/tv_slider6.png" alt="" width="100%">
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control slider_none" href="#myCarouse2" data-slide="prev">
      <span class="arrow-prev"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/arrow-prev.png" alt=""></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control slider_none" href="#myCarouse2" data-slide="next">
      <span class="arrow-next"><img src="<?php echo base_url();  ?>assets/webapp/images/tv/arrow-next.png" alt=""></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>



<div class="wrapper section-service">
    <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-title section-title">
                    <h1 class="wow fadeInUp animated">Supports table top and wall mounting</h1>
                </div>
        	</div>
     	</div>
     </div>
</div> 

<div class="wrapper section-revolution">
    <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h1 class="wow fadeInUp animated">SMi LED TV 4 138.8 cm (55) is here to <br>revolutionise how you watch TV</h1>
                </div>
            
            <ul class="items">
            	<li>
                    <img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-revolution-img1.png" alt="">
                    <p>The "Apple of China" just showed off a TV <br>that's thinner than an iPhone — and it's gorgeous.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-revolution-img2.png" alt="">
                    <p>The base of Mi TV4 is intentionally designed to look transparent so it would appear as if it were "floating" wherever it is placed.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-revolution-img3.png" alt="">
                    <p>Xiaomi's Mi TV 4 manages to stand out even at a CES that has been punctuated with eye-catching TVs.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-revolution-img4.png" alt="">
                    <p>Xiaomi's Mi TV 4 manages to stand out even at a CES that has been punctuated with eye-catching TVs.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-revolution-img5.png" alt="">
                    <p>Xiaomi's modular Mi TV 4 is exactly the kind of innovation.we need in the U.S.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/webapp/images/tv/overall-revolution-img6.png" alt="">
                    <p>Xiaomi's Mi TV 4 sets a new standard for thin televisions.</p>
                </li>
                
            </ul>
            
            </div>
            
        </div>
     </div>
</div> 


<div class="wrapper section-see">
    <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <a href="#" class="see_more">See specs ></a>
        	</div>
     	</div>
     </div>
</div>




<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/webapp/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/webapp/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/webapp/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>
