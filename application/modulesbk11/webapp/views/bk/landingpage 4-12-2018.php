<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $seotags->seo_meta_title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $seotags->seo_meta_desc;?>">
    <link rel='shortcut icon' href="<?php echo base_url();?>assets/fevicon.png"/>
    <link href="<?php echo base_url();?>assets/webapp/css/style.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/webapp/css/bizzgain4.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/webapp/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/webapp/fontawsome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/webapp/bootstrap/css/full-slider.css" rel="stylesheet" />
    
    
    <link href="<?php echo base_url();?>assets/webapp/js2/animate.css" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="<?php echo base_url();?>assets/webapp/js2/wow.min.js"></script>

    <script>
        new WOW().init();
    </script>

    
	
	<link type='text/css' rel='stylesheet' href='<?php echo base_url();?>assets/webapp/js2/style.css' />
    <link type='text/css' rel='stylesheet' href='<?php echo base_url();?>assets/webapp/js2/liquidcarousel.css' />
    <script type="text/javascript" src="<?php echo base_url();?>assets/webapp/js2/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/webapp/js2/jquery.liquidcarousel.pack.js"></script>
   
	<script type="text/javascript">

		$(document).ready(function() {
			$('#liquid1').liquidcarousel({height:129, duration:100, hidearrows:false});
		});
        var base_url="<?php echo base_url();?>";
	</script>

</head>
<body>
<!-- Google Tag Manager -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MFGG8L"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-MFGG8L');</script>

<!-- End Google Tag Manager -->
<div class="header_top">
    <div class="container-fluid">
            <div class="logo_new wow fadeInLeft" data-wow-delay="0.3s"><a href="<?php echo base_url();?>search"><img src="<?php echo base_url();?>assets/webapp/images/logo.png" title="Bizzgain | Toys and Stationery"  alt="Bizzgain | Toys and Stationery" /></a></div>
                <div class="nav">
                <span class="meni-icon hidden-md hidden-lg"><i class="fa fa-bars" aria-hidden="true"></i></span>
                   <div class="right_responsive">
                   <div class="close-icon hidden-md hidden-lg">X</div>
                    <div class="right_nav">
                        <ul class="ul_l_n">
                           
                            <li><a href="javascript:void();" onclick="$('#about').animatescroll({ scrollSpeed: 1000 });"><i class="fa fa-university"></i>About</a></li>
                             <li><a href="javascript:void();" onclick="$('#pro_dc').animatescroll({ scrollSpeed: 1000 });"><i class="fa fa-cubes"></i> Categories</a></li>
                            <li><a href="javascript:void();" onclick="$('#sell_on').animatescroll({ scrollSpeed: 1000 });"><i class="fa fa-bar-chart" aria-hidden="true"></i>Sell on Bizzgain </a></li>
                            <li><a href="javascript:void();" onclick="$('#start_y').animatescroll({ scrollSpeed: 1000 });"><i class="fa fa-hand-o-right" aria-hidden="true"></i>Start your own business</a></li>
                            <li><a href="javascript:void();" onclick="$('#brans_di').animatescroll({ scrollSpeed: 1000 });"><i class="fa fa-hand-o-right" aria-hidden="true"></i>Brands</a></li>
                        </ul>
                    </div>
                    </div>
                </div>
    </div>
 </div>
    <script type="text/javascript">
    	$(document).ready(function(){
        $(".ul_l_n > li").click(function(){
        	$(".ul_l_n > li").removeClass("activ_n");
      $(this).addClass("activ_n");
        });

    	});
    </script>
    
    
    <div class="top_space"></div>
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="heading_home">
                <div class="fill">
                <img src="<?php echo base_url();?>assets/webapp/landingimages/slider_bg.jpg"/></div>
                <div class="carousel-caption">
                	
                    <h1>'India's first Wholesale trading platform for Toys & Stationery'</h1>
                </div>
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="heading_home">
                <div class="fill">
                <img src="<?php echo base_url();?>assets/webapp/landingimages/slider_bg1.jpg"/></div>
                <div class="carousel-caption">
                     <h1>'India's first Wholesale trading platform for Toys & Stationery'</h1>
                </div>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="heading_home">
                <div class="fill">
                <img src="<?php echo base_url();?>assets/webapp/landingimages/slider_bg5.jpg"/></div>
                <div class="carousel-caption">
                    <h1>'India's first Wholesale trading platform for Toys & Stationery'</h1>
                </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </header>
    
  <div class="container" style="position:relative;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="login_box_center"> 
                	<div class="new_gust_login position">
                        	<div class="guest_login wow fadeInLeft" data-wow-delay="0.4s">
                            	<a href="<?php echo base_url();?>register">New user login</a>
                            </div>
                            <div class="guest_user wow fadeInRight" data-wow-delay="0.4s">
                                    <a href="<?php echo base_url();?>login">Existing user login</a>
                            </div>
                    </div>
                </div>  
                </div>
    </div>
    
    
    
    
    
<!--<div class="slider_bg">
	<div class="container">
    	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<h1 class="main_heaidng wow fadeInLeft" data-wow-delay="0.6s">'India's first Wholesale trading platform for Toys'</h1>
            	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<div class="serach_box wow fadeInUp" data-wow-delay="0.4s">
                    	<div class="search_type">
                        	<div class="search_put">
                            	<input type="text" name="searchland" id="searchland" value="" placeholder="Search By Brand, Category, and Product. " class="search_col" >
                            </div>
                            <div class="search_name" style="cursor: pointer;" onclick="searchll();">Search</div> 
                        </div>
                    </div>
                </div>
                
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<div class="new_gust_login">
                        	<div class="guest_login wow fadeInLeft" data-wow-delay="0.4s">
                            	<a href="<?php echo base_url();?>register">New user login</a>
                            </div>
                            <div class="guest_user wow fadeInRight" data-wow-delay="0.4s">
                                    <a href="<?php echo base_url();?>login">Existing user login</a>
                            </div>
                    </div>
                </div>
        </div>
    </div>
</div>-->
    
   
   
 <div class="our_product_bg curcle_space">
 <div id="about" style="position: absolute;top:-85px;"></div>
 	<div class="container-fluid">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
     <div class="toy wow fadeInUp" data-wow-delay="0.6s"><img src="<?php echo base_url();?>assets/webapp/landingimages/toy_small.png" alt=""/></div>
   
   	  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
        	<div class="box_curcel">
            	<div class="cur_text">
                	<h2 class="brands">75+ brands<br> &<br> 8,000+ SKUs,</h2>
                </div>
                <div class="con_curcle"><img src="<?php echo base_url();?>assets/webapp/landingimages/c1.png" alt="brands"/></div>
            </div>
        	
        </div>
        
        
       <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
        	<div class="box_curcel2">
            	<div class="cur_text">
                	<h2 class="brands">Wholesale prices<br>
&<br>
Trade/Quality<br>Assurance,</h2>
                </div>
                <div class="con_curcle"><img src="<?php echo base_url();?>assets/webapp/landingimages/c2.png" alt="brands"/></div>
            </div>
        	
        </div>
        
        
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
        	<div class="box_curcel3">
            	<div class="cur_text">
                	<h2 class="brands">Order on a click<br>
 &<br>
 free delivery<br>across India,</h2>
                </div>
                <div class="con_curcle"><img src="<?php echo base_url();?>assets/webapp/landingimages/c3.png" alt="brands"/></div>
            </div>
        	
        </div>
        
        
       <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
        	<div class="box_curcel4">
            	<div class="cur_text">
                	<h2 class="brands">Credit facility<br>against<br>stock purchase</h2>
                </div>
                <div class="con_curcle"><img src="<?php echo base_url();?>assets/webapp/landingimages/c4.png" alt="brands"/></div>
            </div>
        	
        </div>
        
    </div>
 </div>  
</div>

<div class="our_product_bg_white">
<div id="pro_dc" style="position:absolute;top:-85px;"></div>
	<div class="container-fluid">
   <h1 class="p_categories wow fadeInLeft" data-wow-delay="0.4s">Product categories</h1>
    	<div class="row wow fadeInUp" data-wow-delay="0.3s">
         <div class="margin_left_space">
        	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>electronictoys"><img src="<?php echo base_url();?>assets/webapp/images/product_c_01.jpg" class="" title="Electronic Toys" alt="Electronic Toys"/></a>
                    	<div class="cag_name"><h3>ELECTRONIC TOYS</h3></div>
                    </div>
           		</div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>softtoys"><img src="<?php echo base_url();?>assets/webapp/images/product_c_02.jpg" class="" title="Soft Toys" alt="Soft Toys" /></a>
                    	<div class="cag_name"><h3>SOFT TOYS</h3></div>
                    </div>
           		</div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>schoolsupplies"><img src="<?php echo base_url();?>assets/webapp/images/product_c_03.jpg" class="" title="School Supplies" alt="School Supplies | Toys" /></a>
                    	<div class="cag_name"><h3>SCHOOL SUPPLIES</h3></div>
                    </div>
           		</div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>dolltoys"><img src="<?php echo base_url();?>assets/webapp/images/product_c_04.jpg" class="" title="Dolls & Dolls Houses" alt="Dolls & Dolls Houses" /></a>
                    	<div class="cag_name"><h3>DOLLS & DOLLS HOUSES</h3></div>
                    </div>
           		</div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>strollersandWalkers"><img src="<?php echo base_url();?>assets/webapp/images/product_c_05.jpg" class="" title="Prams, Strollers & Walkers" alt="Prams, Strollers & Walkers" /><a/>
                    	<div class="cag_name"><h3>PRAMS, STROLLERS & WALKERS</h3></div>
                    </div>
           		</div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>rideons"><img src="<?php echo base_url();?>assets/webapp/images/product_c_06.jpg" class="" title="Ride Ons" alt="Ride Ons" /></a>
                    	<div class="cag_name"><h3>RIDE ONS</h3></div>
                    </div>
           		</div>
            </div>
            
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                     <a href="<?php echo base_url(); ?>outdoorsprots"><img src="<?php echo base_url();?>assets/webapp/images/product_c_07.jpg" class="" title="Outdoor Sports" alt="Outdoor Sports | Toys" /></a>
                    	<div class="cag_name"><h3>OUTDOOR SPORTS</h3></div>
                    </div>
           		</div>
            </div>
            
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>diecastvehicles"><img src="<?php echo base_url();?>assets/webapp/images/product_c_08.jpg" class="" title="Die Cast Vehicles" alt="Die Cast Vehicles | Toys" /></a>
                    	<div class="cag_name"><h3>DIE CAST VEHICLES</h3></div>
                    </div>
           		</div>
            </div>
            
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>strollersandWalkers"><img src="<?php echo base_url();?>assets/webapp/images/Binder-And-Clipboard.jpg" class="" title="Binders & Clipboards" alt="Binders & Clipboards | Stationery" /><a/>
                    	<div class="cag_name"><h3>BINDERS AND CLIPBOARDS</h3></div>
                    </div>
           		</div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>rideons"><img src="<?php echo base_url();?>assets/webapp/images/FIle-And-Folder.jpg" class="" alt="1"/></a>
                    	<div class="cag_name"><h3>FILE FOLDERS AND BOXES</h3></div>
                    </div>
           		</div>
            </div>
            
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                     <a href="<?php echo base_url(); ?>outdoorsprots"><img src="<?php echo base_url();?>assets/webapp/images/ink-and-toner-cartridges.jpg" class="" title="Ink and Toner" alt="Ink And Toner | Printer Cartridges" /></a>
                    	<div class="cag_name"><h3>INK AND TONER</h3></div>
                    </div>
           		</div>
            </div>
            
            
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<div class="row">
                    <div class="pro-img_box">
                    <a href="<?php echo base_url(); ?>diecastvehicles"><img src="<?php echo base_url();?>assets/webapp/images/Paper-and-Notebook.jpg" class="" title="Paper & Notebooks" alt="Paper & Notebooks| Corporate Stationery" /></a>
                    	<div class="cag_name"><h3>PAPER AND NOTEBOOKS</h3></div>
                    </div>
           		</div>
            </div>
            
            
            
            
          	</div>  
        </div>
    </div>
</div>
    

  <div class="your_sell">
       
        
<div class="container-fluid">
<div class="sell_colr_bg">
<div id="sell_on" style="position:absolute;top:-85px;"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<h2 class="Partner wow fadeInRight" data-wow-delay="0.3s" style="color:#464646;">SELL ON BIZZGAIN</h2>
       
       	  
        
		<div class="c_back_box">
       	  <div class="c_box">

            	<div class="c_icon"><img src="<?php echo base_url();?>assets/webapp/landingimages/sell_on_1.jpg" alt=""/></div>
                	<h4 class="c_heading">Into Manufacturing or Wholesaling?</h4>
                    
                    
            </div>
        </div>
        
        <div class="c_back_box">
       	  <div class="c_box">
            	<div class="c_icon"><img src="<?php echo base_url();?>assets/webapp/landingimages/sell_on_2.jpg" alt="" width="100%"/ ></div>
                	<h4 class="c_heading">Register and upload your products</h4>
                    
            </div>
        </div>
        
        
        
        <div class="c_back_box">
       	  <div class="c_box">
            	<div class="c_icon"><img src="<?php echo base_url();?>assets/webapp/landingimages/sell_on_3.jpg" alt=""/></div>
                	<h4 class="c_heading">Sell to millions of Retailers</h4>
                    
            </div>
        </div>
          
        
        <div class="c_back_box">
       	  <div class="c_box">
            	<div class="c_icon"><img src="<?php echo base_url();?>assets/webapp/landingimages/sell_on_4.jpg" alt=""/></div>
                	<h4 class="c_heading">Receive order and package it</h4>
                   
            </div>
        </div>
        
          <div class="c_back_box">
       	  <div class="c_box1">
            	<div class="c_icon"><img src="<?php echo base_url();?>assets/webapp/landingimages/sell_on_5.jpg" alt=""/></div>
                	<h4 class="c_heading">We deliver and guarantee the payment</h4>
                    
            </div>
        </div>

       
       	  
       
        
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 get_s">
    <div class="getintouch">
    <a href="<?php echo base_url();?>manufacturer/login" target="_blank">GET IN TOUCH</a>
     </div>
    </div>
    </div>
    
</div>        
         
         <div class="sell_colr_bg_new" >
         <div class="container-fluid">

<div id="start_y" style="position:absolute;top:-85px;"></div>
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<h2 class="Partner wow fadeInRight" data-wow-delay="0.3s">START YOUR OWN BUSINESS</h2>
      
       	  
        
		<div class="c_back_box">
       	  <div class="c_box">

            	<div class="c_icon_bottom"><img src="<?php echo base_url();?>assets/webapp/landingimages/start_1.png" alt=""/></div>
                	<h3 class="c_heading">Partner with India's largest online wholesale platform for toys & Stationery</h3>
                    
            </div>
        </div>
        
        <div class="c_back_box">
       	  <div class="c_box">
            	<div class="c_icon_bottom"><img src="<?php echo base_url();?>assets/webapp/landingimages/start_2.png" alt=""/></div>
                	<h3 class="c_heading">Offering 75+ Brands and 10,000+ Products</h3>
                    
            </div>
        </div>
        
        
        
        <div class="c_back_box">
       	  <div class="c_box">
            	<div class="c_icon_bottom"><img src="<?php echo base_url();?>assets/webapp/landingimages/start_3.png" alt=""/></div>
                	<h3 class="c_heading">Access the latest technology and share the booming business of Toys & Stationery</h3>
                    
            </div>
        </div>
          
        
        <div class="c_back_box">
       	  <div class="c_box">
            	<div class="c_icon_bottom"><img src="<?php echo base_url();?>assets/webapp/landingimages/start_4.png" alt=""/></div>
                	<h3 class="c_heading">Register and we contact you immediately</h3>
                   
            </div>
        </div>
        
          <div class="c_back_box">
       	  <div class="c_box1">
            	<div class="c_icon_bottom"><img src="<?php echo base_url();?>assets/webapp/landingimages/start_5.png" alt=""/></div>
                	<h3 class="c_heading">Requirement: Warehouse of 500-1,000 sq ft and Investment of Rs 5 lakhs</h3>
                    
            </div>
        </div>
   
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 get_b">
    <div class="getintouch_b">
    <a href="<?php echo base_url();?>startbusiness" target="_blank">GET IN TOUCH</a>
     </div>
    </div>
</div>
         
	</div>

    

    
    
    <div class="container-fluid"  style="background:#FFFFFF;">
<div class="container">
     <div class="col-lg-12">
 
        	<h2 class="Partner"></h2>
            <!--<p class="partner_font">Start you own new business. Open your own wholesale store in your city to distribute products listed on our platform. We provide you with complete accessibility of our infrastructure (Technical / Inventory / IT / Sales / Online promotions etc). Against a nominal investment, you could get potential to earn in lakhs within few months.</p>-->
        </div>
    </div>
  
    
    	<div class="container-fluid">

        	
        	<div class="client_box wow fadeInUp" data-wow-delay="0.3s" style="position: relative;" onclick="go_to_login_page()">
            <div id="brans_di" style="position:absolute;top:-95px;"></div>
           
            	 <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet35.jpg" title="Annie" ait="Annie"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet25.jpg" title="Itoys" alt="Itoys"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet30.jpg" title="Apple Fun" alt="Apple Fun"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet11.jpg" title="Awals" alt="Awals"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet20.jpg" title="Centy Toys" alt="Centy Toys"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet23.jpg" title="Ekta" alt="Ekta"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet9.jpg" title="Funskool" alt="Funskool"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet5.jpg" title="Mattel" alt="Mattel"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet21.jpg" title="Devaryan Toys" alt="Devaryan Toys"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet32.jpg" title="Asian" alt="Asian"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet8.jpg" title="Intex" alt="Intex"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet28.jpg" title="Funzoo Group" alt="Funzoo Group"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet36.jpg" title="Mansa Ji" alt="Mansa Ji"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet6.jpg" title="Chicco" alt="Chicco"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet15.jpg" title="Dash" alt="Dash"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet37.jpg" title="Novelty" alt="Novelty"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet38.jpg" title="Smartivity" alt="Smartivity"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet39.jpg" title="Sunbaby" alt="Sunbaby"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet3.jpg" title="Shinsel" alt="Shinsel"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet27.jpg" title="Funride" alt="Funride"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet40.jpg" title="Luusa" alt="Luusa"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet24.jpg" title="Kids Zone" alt="Kids Zone"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet41.jpg" title="Masoom" alt="Masoom"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet42.jpg" title="Honeybee" alt="Honeybee"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet46.jpg" title="Lovely" alt="Lovely"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet2.jpg" title="Anand" alt="Anand"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet43.jpg" title="Girnar" alt="Girnar"></a>   
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet44.jpg" title="bruder" alt="bruder"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet45.jpg" title="Toyzone" alt="Toyzone"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet14.jpg" title="Toy Kraft" alt="Toy Kraft"></a>
                <a href="javascript:void(0);" class="clinet_border"><img src="<?php echo base_url();?>assets/webapp/landingimages/clinet48.jpg" title="Little  Angel" alt="Little  Angel"></a>   
        	</div>
          </div>	
</div>

<div class="space_client"></div>

    </div>
    <script type="text/javascript">
        function searchll(){
            var ss=$("#searchland").val();
            if(ss==''){
                return false;
            } else {
                window.location.href = "<?php echo base_url();?>login";
            }
        }
    </script>
     <script type="text/javascript" src="<?php echo base_url();?>assets/webapp/js/animatescroll.min.js"></script>

    <script src="<?php echo base_url();?>assets/webapp/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/webapp/js/bootstrap.min.js"></script>
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
    
  <script>
    $(document).ready(function () {
			$(".meni-icon").click(function(){
			
		  $(".right_responsive").toggleClass("active_mm");
        });
		   $(".close-icon").click(function(){
			 
		  $(".right_responsive").toggleClass("active_mm");
        });
		});
    </script>
    
</body>
</html>