<!--script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script-->
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
    .sessionerror{border:0px solid red!important; color:red; font-weight: normal; }
  </style>
  
  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules

          rules: {
            acctype       : "required",
            mem_fname     : "required",
            mem_lname     : "required",
            mem_shipstate : "required",
            mem_shipcity  : "required",
            mem_OTP       : "required",
            mem_companyname : "required",
            terms: {
                 required : true

              },
            mem_email:{
                required: true,
                email: true
            },
            mem_mobile:{
                required: true,
                digits: true,
                minlength: 10
            },
            mem_pass:{
                required: true,
                minlength: 6
            },
            mem_conpass:{
                required: true,
                equalTo: "#mem_pass"
            }
            
        },
        // Specify the validation error messages
        messages: {
          //terms : "please agree terms & condition"
            /*acctype       : "Choose Account type",
            mem_fname     : "First Name is required",
            mem_lname     : "Last Name is required",
            mem_shipstate : "State is required",
            mem_shipcity  : "City is required",
            mem_email: {
                required: "Email is required",
                email: "Please enter a valid email"
            },
            mem_mobile: {
                required: "Contact Number is required",
                digits: "Please enter only digits"
            },
            mem_pass: {
                required: "Password is required",
                minlength: "Your password must be at least 6 characters long"
            },
            mem_conpass: {
                required: "Confirm Password is required",
                equalTo: "Please enter the same Password again"
            }
            */
            
        },
        submitHandler: function(form) {
            var otp = $("#mem_OTP").val();
            var cpotp = $("#mem_ch").val();
            var facctype=$(".acctype:checked").val();
            var fmem_mobile=$("#mem_mobile").val();
            var fmem_shipstate=$("#mem_shipstate").val();
            var fmem_shipcity=$("#mem_shipcity").val();
            var fmem_compname=$("#mem_companyname").val();
            if($("#cstvalue").val()=='')
            {
                var cstid='4';
            }
            else
            {
               var cstid='2';
               var cstvalue=$("#cstvalue").val();
            }
             if($("#tinvalue").val()=='')
            {
                var tinid='6';
            }
            else
            {
               var tinid='1';
               var tinvalue=$("#tinvalue").val();
            }
           //alert(cstvalue + tinvalue); 
            //return false;
            if(otp==cpotp){
            $("#dlb").css('display', 'block');
            $("#mem_submit").css('display', 'none');
            $.ajax({
                  url: '<?php echo base_url(); ?>webapp/google_login/registernew',
                  type: 'POST',
                  data: {'facctype':facctype,'fmem_mobile':fmem_mobile,'fmem_shipstate':fmem_shipstate,'fmem_shipcity':fmem_shipcity,'fmem_cstid':cstid,'fmem_cstvalue':cstvalue,'fmem_tinid':tinid,'fmem_tinvalue':tinvalue,'fmem_compname':fmem_compname},
                  success: function (data){                    
                  //console.log(data);
                  //alert(data);return false;
                  $("#dlb").css('display', 'none');
                  $("#mem_submit").css('display', 'block');
                  window.location.href = '<?php echo base_url();?>webapp/google_login/google_auth';
                  /*if(data == 'existemail'){
                    $(".sessionerror").html('Email alerady exists.');
                    return false;
                  } else if(data == 'existmobileno'){
                    $("#mem_ch").val('');
                    $("#mem_OTP").val('');
                    $(".sessionerror").html('Mobile number already exist.');
                    return false;
                  } else {
                    if(data == 'manufacturer'){
                      $(".sessionerror").html("User successfully registered."); 
                      //window.location = base_url + "manufacturer/login";
                      window.location = base_url + "home";
                    } else {
                      $(".sessionerror").html("User successfully registered."); 
                      window.location = base_url + "home";
                    }  
                  }*/                  
                }
              });
            } else {
              alert("OTP Password is Wrong.");
              return false;
            }

        }
      

    });

  });
  
</script>


  <div class="page_pro1" style="height:auto;padding:0px 0px 0px;">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                	<div class="heading-box">
                    	<div class="heading_name">
                    		<a class="#">Home</a>
                            <a class="#"><i class="fa fa-angle-right"></i> Google+ Login</a>
                           
                    </div>
				</div>
              </div>
           </div>

<!--Start Compare part-->
<div class="bg-login">
        <div class="main_space contact_space" id="myDiv">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="row">
             <h1 class="login_account">LOGIN WITH GOOGLE+</h1>

             <div class='flashmsg' style="color:red;text-align:center;margin-bottom:20px;"><?php echo $this->session->flashdata('message'); ?></div>
             
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            	<div class="theme-login">
                    <div class="content">
                    	<h2 class="corporate_font">Register with Bizzgain using Google+</h2>
                    <hr>
                    	 
                    <br>
                    
                    
                    
                    
<form method="post" action="" id="addCont">                    
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="row">
    
     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<div class="row">
			 <label for="acctype1">	
                         <div style="float:left;width:100%;text-align:center;">
                         <img src="<?php echo base_url();?>assets/webapp/images/consum.png"/>
                         </div>
                         <div style="width:100%;text-align:center;padding:15px 0px;float:left;"class="consum_error"> Consumer<br/><input type="radio" class="acctype" id="acctype1" name="acctype" value="consumer" onclick="regtype();" /></div>
                         </label>
                         </div>
				</div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<div class="row">
			<label for="acctype2">		
                         <div style="float:left;width:100%;text-align:center;">
                         <img src="<?php echo base_url();?>assets/webapp/images/retal.png"/>
                         </div>
                         <div style="width:100%;text-align:center;padding:15px 0px;float:left;"> Retailer<br/><input type="radio" id="acctype2" name="acctype" class="acctype" value="retail" onclick="regtype();" /></div>
</label>                         
</div>
				</div>
		
            

        <div class="sessionerror"></div>
        <div id="registermem" style=" display: none;">
		<div style="width:100%;height:20px;float:left;"></div>
		
    <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="row">
				<div class="form_reg">
        <div class="place_hold">First Name <span>*</span></div>
                      <input type="text" id="mem_fname" name="mem_fname" class="form-control2 form-control3" placeholder="" >
                      
                    </div>
				</div>
			</div> -->
            
            
            <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="row">
				<div class="form_reg">
         <div class="place_hold">Last Name <span>*</span></div>
                      <input type="text" id="mem_lname" name="mem_lname" class="form-control2 form-control3" placeholder="" >
                      
                    </div>
				</div>
			</div> -->
            
            
           <!--  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="row">
				<div class="form_reg">
        <div class="place_hold">Email <span>*</span></div>
                      <input type="text" id="mem_email" name="mem_email" class="form-control2 form-control3" placeholder="" >
                      
                    </div>
				</div>
			</div> -->
            
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="row">
				<div class="form_reg">
          <div class="place_hold">Mobile <span>*</span></div>
                      <input type="text" id="mem_mobile" maxlength=10 name="mem_mobile" onblur="regmobotp()" class="form-control2 form-control3" placeholder="" >
                      
                    </div>
				</div>
			</div>
            
            <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="row">
				<div class="form_reg">
        <lable class="place_hold" for="mem_pass">Password <span>*</span></lable>
                      <input type="password" id="mem_pass" name="mem_pass" class="form-control2 form-control3" placeholder="" >
                      
                    </div>
				</div>
			</div> -->
            
           <!--  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="row">
				<div class="form_reg">
          <lable class="place_hold" for="mem_conpass">Confirm Password <span>*</span></lable>
                      <input type="password" id="mem_conpass" name="mem_conpass" class="form-control2 form-control3" placeholder="" >
                      
                    </div>
				</div>
			</div>
			 -->

      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div class="row">
        <div class="form_reg">
            <select id="mem_shipstate" name="mem_shipstate" onchange="statecountry();" class="form-control2 form-control3">
                <option value="">Select State</option>
            <?php foreach ($viewshipstate as $key => $value) { ?>
                <option value="<?php echo $value['stateid'];?>"><?php echo $value['statename'];?></option>
            <?php } ?>
            </select>
                      
        </div>
      </div>
      </div>

      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <div class="row">
        <div class="form_reg">
                      <select id="mem_shipcity" name="mem_shipcity" class="form-control2 form-control3">
                      <option value="">Select City</option>
                      </select>
                      
                    </div>
        </div>
      </div>
<div id="taxdet">

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="row">
        <div class="form_reg">
        <div class="place_hold">Company / Shop Name<span>*</span></div>
           <input type="text" id="mem_companyname" name="mem_companyname" class="form-control2 form-control3" placeholder="" >              
               <input type="hidden" id="cstvalue" name="cstvalue" maxlength="11" class="form-control2 form-control3" placeholder="" >  
               <input type="hidden" id="tinvalue" name="tinvalue" maxlength="11" class="form-control2 form-control3" placeholder="VAT/TIN No." >               
                    </div>
        </div>
      </div>
<!--<div class="col-lg-6">
      <div class="row">
        <div class="form_reg">
                  
                      
                    </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
      <div class="row">
        <div class="form_reg">
                     
                      
                    </div>
        </div>
      </div>-->
</div>

<div id="otpmob" style="display: none;" >

     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="row">
      <p>One Time Password (OTP) has been sent to your Mobile. please Verify Your Mobile Number.
</p>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <div class="row">
        <div class="form_reg">
         <div class="place_hold">Enter OTP<span>*</span></div>
           <input type="text" id="mem_OTP" name="mem_OTP" class="form-control2 form-control3" placeholder="" >
                        
        </div>
        </div>
        </div>
        
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <div class="row">
        <div class="form_reg">
        
           <input type="button" onclick="regmobotp();" value="Resend OTP">            
        </div>
        </div>
        </div>
        </div>
        </div>
      </div>  

            
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="row"><p>Note : All fields are compulsory</p></div></div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="row"><p class="ter"><input type="checkbox" id="terms" name="terms" style="margin-right:8px;"/>I Agree To The <a target="_blank" href="<?php echo base_url();?>termsandcondition" style="color: #199EDE;text-decoration: none;">Terms & Conditions</a>.</p></div></div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <input type="hidden" id="mem_ch" name="mem_ch" />    
       <div class="form-group">
          <div class="row">
            <span class="loadingBtn"  style="display:none; width: 96%; margin-left: 0;" id="dlb" ><i class="fa fa-spinner fa-spin"></i></span>
            <!-- <input type="submit" name="mem_submit" id="mem_submit" value="GMAIL LOGIN" class="login_botton" style="width:90%;"> -->
            <div class="social_register_gm">
            <span class="f_iconss"><i class="fa fa-google-plus" aria-hidden="true"></i></span>
            <span class="text_ff">
           <input type="submit" name="mem_submit" id="mem_submit" value="Register with Google+">
           </span>
          </div>
            
          </div>
        </div>  
        </div> 
        </div>                


</div>
              </div>      
                </form>    
                    </div>
                 </div>   
            </div>
             
       
        
            	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            	<div class="theme-regis">
                    <div class="content">
                    <h2 class="corporate_font">Login to your account using Google+</h2>
                    <hr style="background:#CCCCCC; border:none; height:1px;">
                    <p>Creating an account has many benefits. Already Registered Members Click On Login.</p>
                    
                   
         <!--  <div class="form-group">
              <a href="<?php echo base_url();?>webapp/google_login/google_auth" class="submit_b_r" style="text-decoration: none;">LOGIN</a>           
					</div> -->

          <div class="social_login_g">
                  <a href="<?php echo base_url();?>webapp/google_login/google_auth"><span class="f_iconss"><i class="fa fa-google-plus" aria-hidden="true"></i></span> <span class="text_ff">Login with Google+</span></a> 
                </div><!--end of social login--> 
                    
				<div class="toy_register pull-right"><img src="<?php echo base_url();?>assets/webapp/images/toy.png" alt=""/></div>

                  </div>
                    
                    </div>
                    
            </div>
            
            
            
            </div>
            
            </div>
            
         </div>
        
    </div>
    
    </div>
    
</div>
<style type="text/css">
.consum_error{
	position:relative;
}
.consum_error label.error{
	    position: absolute;
    width: 100%;
    bottom: -6px;
    left: 96%;
}
</style>
<script type="text/javascript">
$(document).ready(function(){

    $('.form_reg input').focusin(function(){
    
     $(this).prev().hide();
    });
     $('.form_reg input').focusout(function(){
    if($(this).val() == ""){
       $(this).prev().show();
    }
    else{
      $(this).prev().hide();
    }
    
    });
$("input[name='acctype']").click(function() {
    $('html, body').animate({
        scrollTop: $("#myDiv").offset().top
    }, 1000);
});

});
  function statecountry(){
    var  stid=$('#mem_shipstate').val();
    $.ajax({
    url: '<?php echo base_url()?>webapp/memberlogin/citystate',
    type: 'POST',
    data: {'stid': stid},
    success: function(data){
      //console.log(data);
      //alert(data);return false;
    //var  option_brand = '<option value="">Select City</option>';
    //$('#mem_shipcity').empty();
    $("#mem_shipcity").html(data.trim());
    }
    });
  }
</script>