 <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            fd_fname   : "required",
            fd_lname   : "required",
            fd_email   :{
                          required: true,
                          email: true
                        },
            fd_mobile  : "required",
            fd_comment : "required",
        },
        // Specify the validation error messages
        messages: {
            fd_fname   : "First Name is required",
            fd_lname   : "Last Name is required",
            fd_email   : {
                            required: "Email is required",
                            email: "Please enter a valid email"
                         },
            fd_mobile  : "Mobile no is required",
            fd_comment : "Feedback Comment is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>
<style>

.form-control{ width:100% !important;}
</style>
<div class="col-lg-12">
    
    <div class="row">
      <div class="heading-box">
        <div class="container">
        <div class="heading_name">
          <a href="<?php echo base_url();?>" class="">Home</a>
                                <a href="javascript:void(0);" class=""><i class="fa fa-angle-right"></i>FeedBack</a>
                   
        </div>
      </div>
    </div>
  </div>
  </div>
  
 <div class="fee_d"> 
  
 <div class="container">
             <div class="row">
                         <div class="col-md-8 ">
                         
                         <div class="form_feed">
                         
                         <div class="hed_feed">Feedback Form</div>
                         <form method="post" id="addCont">
                         <div class="form_de">
                         <div style="text-align: center;color: green;margin: 5px 0px 5px 0px;">
                            <?php
                                  if($this->session->flashdata('message')){
                                    echo $this->session->flashdata('message'); 
                                  }
                                ?>    
                         </div>
                         <div class="form-f">
                                    <div class="col-lg-2 col-md-2">First Name <span style="color:red;font-weight: bold;">*</span></div>
                                    <div class="col-lg-4 col-md-4"> <input type="text" name="fd_fname" class="form-control" id="fd_fname" required></div>
                                    
                                    <div class="col-lg-2 col-md-2">Last Name <span style="color:red;font-weight: bold;">*</span></div>
                                    <div class="col-lg-4 col-md-4"> <input type="text" name="fd_lname" class="form-control" id="fd_lname" required></div>
                                    
                                </div>
                                
                                <div class="form-f">
                                   
                                    
                                     <div class="col-lg-2 col-md-2">Email ID <span style="color:red;font-weight: bold;">*</span></div>
                                    <div class="col-lg-4 col-md-4"> <input type="text" name="fd_email" class="form-control" id="fd_email" required></div>
                                    
                                     <div class="col-lg-2 col-md-2">Mobile no. <span style="color:red;font-weight: bold;">*</span></div>
                                    <div class="col-lg-4 col-md-4"> <input type="text" name="fd_mobile" maxlength="10" class="form-control" id="fd_mobile" onkeypress="javascript:return isNumber(event)" required></div>
                                    
                                   
                                    
                                </div>
                                
                                 <div class="form-f">
                                 
                                  <div class="col-lg-2 col-md-2">Feedback Comment <span style="color:red;font-weight: bold;">*</span></div>
                                    <div class="col-lg-10 col-md-10">  <textarea class="form-control" style="resize: none;"  id="fd_comment" name="fd_comment" rows="3" required></textarea></div>
                                 
                                 </div>
                                
                                 <div class="form-f-r">
                                 
                                  <input type="submit" name="submit" id="submit" value="Submit">
                                 
                                 </div>
                         </div>
                         
                         </form>
                         
                         
                        
                        
                         </div>
                        
                         
                         
                         
                         
                         </div>
                         
                         <div class="col-md-4">
                         <div class="right_image">
                         
                         <a href="javascript:void(0)"><img src="<?php echo base_url();?>images/Solid_Color_1.png"></a>
                         </div>
                         
                         </div>
             
             </div>
             
             <?php if(!empty($viewfeedback)){ ?>
             <div class="row">
             
             <div class="coment_fe">
             
             <div class="heading_frt">
             
             <h1>Feedback</h1>
             </div>
             
             <div class="first_b">
             <?php foreach ($viewfeedback as $key => $value) { ?>
                <div class="se_fe">
             
                 <h1><?php echo ucwords($value->f_name); ?></h1>
                 
                 <div class="comment_t">
                 <p><?php echo $value->f_comment; ?></p>
                 
                 <span><?php echo date_format(date_create($value->f_createdon), 'd/m/Y'); ?></span>
                 </div>
                 </div>   
             <?php } ?>
                          
                     
             </div>
             
             </div>
             </div>
             <?php } ?>
 </div>
 
 </div>