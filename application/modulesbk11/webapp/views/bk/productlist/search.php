<style type="text/css">
.ui-slider-horizontal {   background: #223742;    position: relative;    height: 10px;    width: 95%;    margin-left: 1%; }
.ui-slider-handle {  position: absolute;    width: 10px;    background: #058ED0;    height: 20px;    top: -5px;    border-radius: 2px;    border: 1px solid #058ED0; }
</style>

<?php $this->load->view('searchfilter'); ?>
<form> <input type="hidden" id="type_view"></form>
<div class="col-lg-9">
	<div class="row">
	<div class="col-lg-12">
		<div class="category_name">
			<h2><?php echo ucfirst($catname->catname);?></h2>
			<div id="wishmsg"></div>
			<div class="sort_by">
				<span class="close_s"><i class="fa fa-times" aria-hidden="true"></i></span>
				<ul>    
					<li onClick="return chk_sh_up('4')" style="cursor: pointer;">
						<a style="<?php echo ($_GET['porder']=='4')?'background:#5F5F5F;color: #fff;box-shadow: 0px 0px 1px #fff inset;':''; ?>">New</a> </li>    
						<li onClick="return chk_sh_up('3')" style="cursor: pointer;">
							<a style="<?php echo ($_GET['porder']=='3')?'background:#5F5F5F;color: #fff;box-shadow: 0px 0px 1px #fff inset;':''; ?>">Discount</a> </li>    
							<li onClick="return chk_sh_up('2')" style="cursor: pointer;"><a style="<?php echo ($_GET['porder']=='2')?'background:#5F5F5F;color: #fff;box-shadow: 0px 0px 1px #fff inset;':''; ?>">Low Price</a></li>    
							<li onClick="return chk_sh_up('1')" style="cursor: pointer;"><a style="<?php echo ($_GET['porder']=='1')?'background:#5F5F5F;color: #fff;box-shadow: 0px 0px 1px #fff inset;':''; ?>">High Price</a> </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="list_page" id="contentNew">
				<div class="ajaxnone" style="display:none1"><?php if(count($resultspage)>0){ ?>
					<div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
						<div class="sm_filter">  
							<div class="fil_btn"><i class="fa fa-filter" aria-hidden="true"></i>filter</div>
							<div class="short_btn"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i>sort by</div>  
						</div>
					</div>
					<?php $imgUrl = proimgurl().'images/'; 
					$i=0;
					foreach ($resultspage as $key => $prolistnew) 
					{   $i++;
						$prolist=(object)$prolistnew;  
						$pid=$prolist->prodtmappid;  
						$discount='';  
						$price=$prolist->productMRP;   
						$productsize=explode(':', $prolist->Size);  
						$productcolor=explode(':', $prolist->color); 
						?> 
<div class="product_list proid_<?php echo $i?>" vtAlgin productid='<?php echo $prolist->prodtmappid?>'>
<script src="<?php echo js_url('weightchange.js');?>" type="text/javascript"></script>
	<div class="product_tab">
		<div class="qty_ad">
 <?php if($prolist->qty==0){ echo '<span style="color: red;">OUT OF STOCK</span>'; }else{ ?>
		<span>Qty.</span><a href="javascript:void();">
			<span class="first2" data="<?php echo $pid?>" id="first2<?php echo $pid?>" onclick="cart_update_list($(this));"><i class="fa fa-minus-circle"></i></span></a> 
			<input type="text" id="a1<?php echo $pid?>" value="1" readonly> <a href="javascript:void();">
			<span class="last2" data="<?php echo $pid?>" data-hrefnew="<?php echo $prolist->qty?>" id="last2<?php echo $pid?>" onclick="cart_update_list($(this));"><i class="fa fa-plus-circle"></i></span></a>
<?php } ?>
			</div> 
			<div class="product_tab_inner"> 
				<div class="img_box"><?php if($prolist->proQnty<=0){ $div= '<div class="outofstock"></div>'; } else { $div= ''; } $pronamestr=str_replace(' ','-',$prolist->ProName); $pronamestrname = seoUrl(str_replace('/','-',$pronamestr)); echo '<a href="'.base_url().'product/'.$pronamestrname.'-'.$prolist->prodtmappid.'.html'.'">'.$div.' <span><img id="mainImage" src="'.$imgUrl.'thumimg/'.$prolist->image.'" alt="'.$prolist->ProName.'"></span></a>'; ?> </div>
				<div class="product_info">
					<div class="pro_name"><a href="<?php echo base_url().'product/'.$pronamestrname.'-'.$prolist->prodtmappid.'.html';?>"> <?php echo ucfirst($prolist->ProName);?></a></div>
					<div class="price">
					<?php $cityprice =citybaseprice($prolist->FinalPrice,$prolist->cityvalue);
					 $prodtsc=number_format((1-$cityprice/$prolist->productMRP)*100,1,".","");
					  if($prolist->ProDiscount<=0)
					  	{ 
					  		echo '<span class="new_p">Rs.'.$prolist->productMRP.'</span>'; 
					  	} 
					  	else 
					  	{ 
					  		if($prolist->ProDisType=='fixed')
					  		{ 
					  			$distype='Rs. Off'; 
					  		} 
					  		else 
					  		{ 
					  			$distype='% Off'; 
					  		} 
					  			echo '<span class="old_p">Rs.'.$prolist->productMRP.'</span> <span class="new_p">Rs.'.$cityprice.'</span>'; 
					  			#echo '<div class="sale_ad">'.$prodtsc.'<small>'.$distype.'</small></div>'; 
					  	} ?>
				</div>

				 <?php if($prodtsc>0){
			   if($prodtsc != "" || $distype !=""){
			     echo '<div class="sale_ad">'.$prodtsc.'<small>'.$distype.'</small></div>';
			   }
			   else{

			   }}
			  ?>
				<!--<div class="selet_qtnty">
 <select class='weightattr'>
  <?php  $weightex=explode(',', $prolistnew->color);
      $weightInd='';
      foreach ($weightex as $key => $value) {
        
        $getweight=explode(':', $value);
        if($prolistnew->prodtmappid==$getweight[1]){
          $getwe=explode(':', $value);
          $weightInd=$getwe[0];
        
        } ?>
        <option <?php if($getweight[0]==$weightInd) echo 'selected'; ?> value="<?php echo $getweight[1].'_'.$i; ?>"><?php echo $getweight[0]; ?></option>
        <?php
      } 

      ?>
   
    </select>  
  </div>-->

 <?php if($prolist->qty==0){ 
   echo '<div class="p_link"><a href="'.base_url().'product/'.$pronamestrname.'-'.$prolist->prodtmappid.'.html'.'" style="background: #71af0e;padding: 5px 8px;color: #fff;display: inline-block;border-radius: 4px;text-decoration: none;"><i class="fa fa-history"></i> View Details</a></div>'; 
    }else{ ?>
		
					<div class="p_link">
					<form action="" method="post"> 
					<?php   if($prolist->qty == 0 || $prolist->qty <= 0)
							{      
								$addClassdNoproduct = "";      
								$disableButton = "true";      
								$addtocartID = 'addtocart';   
							}
							else
							{      
								$addClassdNoproduct = "";      
								$disableButton = "true";      
								$addtocartID = 'addtocart';   
							}  
							if(in_cart_array($this->cart->contents(), $pid) == true)
							{      
								$addClassd = "disablebutton";      
								$data_is_in_cart = 'true';    
							} 
							else 
							{       
								$data_is_in_cart = 'false';      
								$addClassd ="";  
							}
							if(!empty($sizearray))
							{ 
								$data_with_size = "true";
							} 
							else
							{
								$data_with_size = "false";
							} ?>
					<span class="sizerror" id="sizerror<?=$prolist->prodtmappid?>"></span> 
					<a href="javascript:void(0)" data-href="<?=$prolist->prodtmappid?>" data-hrefnew="<?php echo $data_is_in_cart; ?>" class="add_cart addtocart <?php echo $addClassdNoproduct;?> <?php echo $addClassd;?>" id="<?php echo $addtocartID; ?>"  data-seller-id="<?php echo $prolist->manufacturerid ?>" data-with-size='<?php  echo $data_with_size; ?>' data-with-size-val='<?php  echo $productsize[0] ?>' data-with-color-val='<?php  echo $weightInd ?>' data-is-in-cart ="<?php echo $data_is_in_cart; ?>"  data-is-listing-buyable="<?php echo $disableButton; ?>"  data-buy-listing-id = "<?php echo $pid;?>" type="submit" name="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</a> 
					<span class="sizerrorbuynow"></span></form> 
				</div>
				<div class="p_link2">
				<?php  echo bizzinquiry($prolist->prodtmappid); ?>
				<?php  echo listwishlistlink('view',$userid,$pid); ?>
				<?php  echo bizzcompare($prolist->prodtmappid); ?>
					
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div><?php } }else {       echo "<div class='nofound' style='float:left; width:100%; text-align:center; font-size:16px; padding:35px 0px; '>No products available.</div>"; } ?></div>  <?php if($resultspage->scalar!='Something Went Wrong'){ $imgUrl = proimgurl().'images/'; } else {       echo "<div class='nofound' style='float:left; width:100%; text-align:center; font-size:16px; padding:35px 0px; '>No products available.</div>"; } ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<script>
$(document).ready(function(){
  pro_img_s();

 });

function pro_img_s(){
  var x = $(".img_box").width();
  $(".img_box a").height(x);
}


/*
    $(".weightattr").change(function(){ 
    var id = $(this).val();
    var arr = id.split('_');

    $.ajax({
    url: '<?php echo base_url(); ?>webapp/catproductlisting/weightchnage',
    type: 'POST',
    data: {'weight_proid':arr[0],'pro_replaceid':arr[1]},
    success: function (data){
    //alert(data);
    $('.proid_'+arr[1]).html(data);

    }
    });
    
});
        */    
</script>
