<div class="product_bg">
	<div class="product_banner"><img src="<?php echo base_url(); ?>assets/webapp/images/product_banners.jpg"  alt="" /></div>
    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    		<h1 class="soft_toys_heading">Toys Categories</h1>
            <div class="line_soft_boys"></div>
    	</div>
</div>


<div class="product_margin_box"> 
    <div class="container-fluid">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                        <a href="<?php echo base_url(); ?>maincategory/electronic-toys-4.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_01.jpg" class="img-responsive" alt="" />
                        
                        <div class="product_name_top">
                        	<h1>Electronic Toys</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/electronic-toys-4.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                        <a href="<?php echo base_url(); ?>maincategory/soft-toys-12.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_02.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Soft Toys</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/soft-toys-12.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                         <a href="<?php echo base_url(); ?>maincategory/school-supplies-56.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_03.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>School Supplies</h1>
                            
                        </div></a>
                        <div class="shop_now"> <a href="<?php echo base_url(); ?>maincategory/school-supplies-56.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                      <a href="<?php echo base_url(); ?>maincategory/dolls-en-dolls-houses-17.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_04.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Dolls & Dolls Houses</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/dolls-en-dolls-houses-17.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                        <a href="<?php echo base_url(); ?>maincategory/prams,-strollers-en-walkers-57.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_05.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Prams, Strollers & Walkers</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/prams,-strollers-en-walkers-57.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                         <a href="<?php echo base_url(); ?>maincategory/ride-ons-58.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_06.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Ride Ons</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/ride-ons-58.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                         <a href="<?php echo base_url(); ?>maincategory/outdoor-sports-20.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_07.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Outdoor Sports</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/outdoor-sports-20.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                       <a href="<?php echo base_url(); ?>maincategory/die-cast-vehicles-59.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_08.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Die Cast Vehicles</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/die-cast-vehicles-59.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                        <a href="<?php echo base_url(); ?>maincategory/baby-en-toddler-toys-13.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_09.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Baby & Toddler Toys</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/baby-en-toddler-toys-13.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                       <a href="<?php echo base_url(); ?>maincategory/musical-toys-60.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_010.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Musical Toys</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/musical-toys-60.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                        <a href="<?php echo base_url(); ?>maincategory/indoor-games-19.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_011.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Indoor Games</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/indoor-games-19.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                        <a href="<?php echo base_url(); ?>maincategory/kids-room-decor-18.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_012.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Kids Room Décor</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/kids-room-decor-18.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                         <a href="<?php echo base_url(); ?>maincategory/board-games-puzzles-en-cubes-15.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_013.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Board Games,Puzzles & Cubes</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/board-games-puzzles-en-cubes-15.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                        <a href="<?php echo base_url(); ?>maincategory/action-toys-and-figures-211.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_014.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Action Toys & Figures</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/action-toys-and-figures-211.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                         <a href="<?php echo base_url(); ?>maincategory/activity-sets-16.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_015.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Activity Sets</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/activity-sets-16.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="product_main_box">
                       <a href="<?php echo base_url(); ?>maincategory/educational-toys-14.html"><img src="<?php echo base_url(); ?>assets/webapp/images/product_c_016.jpg" class="img-responsive" alt="" />
                        <div class="product_name_top">
                        	<h1>Educational Toys</h1>
                            
                        </div></a>
                        <div class="shop_now"><a href="<?php echo base_url(); ?>maincategory/educational-toys-14.html">Shop Now &nbsp;<i class="fa fa-chevron-circle-right"></i></a></div>
                        
                    </div>
            </div>
    
            
            
        </div>
        
    

          </div>

  </div>

</div>