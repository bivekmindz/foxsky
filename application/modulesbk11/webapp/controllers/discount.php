<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Discount extends MX_Controller {
	public function __construct(){
		$this->load->model("supper_admin");
	}




  public function applycoupon(){
    $usedwalletamt_ = !empty($this->session->userdata('usedwalletamount')) ? $this->session->userdata('usedwalletamount') :0.00;
    $coupon_discounted_products=array();  
    $coupon_disc_in_percentage=0;
    $amounttobediscount=0;
    $userdata = $this->session->userdata('logindetail');
    if($userdata==false){
      $user_id =0;
    } else {
      $user_id   = $userdata->LoginID;
    }
    $totalwithaditiondisc=0;
    $stotal = $this->cart->total();
    if(empty($userdata)){
     $data=array('couponid'=>0,'discount'=>0,'msg'=>'Please Login to avail the Offer!','coupon_disc_inpercent_perporduct'=>0);
   }else{
    $parameter=array('promocode'=>$this->input->post('promo'),'userid'=>$user_id,
    'totalwithaddiscount'=>$totalwithaditiondisc,'subtotal'=>$stotal);
    $path =api_url().'discount/applycoupon/format/json/';
    $response=curlpost($parameter,$path); 
    $copid=$response->cop_id;
    $purchaseamout=$response->purchaseamt;
    $discounttype=$response->discounttype;
    $discountamt=$response->discountamt;
    $applyondiscounted=$response->applyondiscounted;
    $DiscountOnMaxAmt=$response->DiscountOnMaxAmt;
    $MaxPurchaseAmt=$response->MaxPurchaseAmt;
    //$CouponCode=$response->CouponCode;
    if($response=='Something Went Wrong')
    {   
      $data=array('couponid'=>0,'discount'=>0,'msg'=>'Invalid Coupon Code','coupon_disc_inpercent_perporduct'=>0);
    }
    else{ 
      if($response->discountOn=='allcat'){
        foreach($this->cart->contents() AS $catid){
          $pram=array('cop_id'=>$copid,'discounton_id'=>$catid['catid'],'discount_on'=>'allcat','brand_id'=>$catid['brandid'],'vendor_id'=>$catid['VendorID']);
          $path =api_url().'discount/applycoupon_checkdiscountid/format/json/';
          $responsedata=curlpost($pram,$path);
          if($responsedata->discountedid!=''){
            if($applyondiscounted=='no' && ($catid['addiscount']>0 || $catid['prodiscount']>0) ){
              $amounttobediscount+=0;
            }else{
              $amounttobediscount+=$catid['subtotal'];
            }
          }   
        }
        $amounttobediscount = $amounttobediscount-$usedwalletamt_;
        if($amounttobediscount>0){
          $data=$this->discountsetting($copid,$discounttype,$amounttobediscount,$purchaseamout,$discounttype,$discountamt,$DiscountOnMaxAmt,$coupon_discounted_products,$MaxPurchaseAmt);
        }else{
          $data=array('discount'=>0,'couponid'=>0,'msg'=>'Offer Not Valid on Any one or more than one product added in your cart!','coupon_disc_inpercent_perporduct'=>0);  
        } 
      }else if($response->discountOn=='brand'){
        foreach($this->cart->contents() AS $catid){
          $pram=array('cop_id'=>$copid,'discounton_id'=>$catid['brandid'],'discount_on'=>'brand','brand_id'=>$catid['brandid'],'vendor_id'=>$catid['VendorID']);
          $path =api_url().'discount/applycoupon_checkdiscountid/format/json/';
          $responsedata=curlpost($pram,$path);
          if($responsedata->discountedid!=''){
            if($applyondiscounted=='no' && ($catid['addiscount']>0 || $catid['prodiscount']>0)){
              $amounttobediscount+=0;
            }else{
              $amounttobediscount+=$catid['subtotal'];
              $coupon_discounted_products[]=$catid['id'];
            }
          }
        }
        $amounttobediscount=$amounttobediscount-$usedwalletamt_;
        if($amounttobediscount>0){
         $data=$this->discountsetting($copid,$discounttype,$amounttobediscount,$purchaseamout,$discounttype,$discountamt,$DiscountOnMaxAmt,$coupon_discounted_products,$MaxPurchaseAmt);
       }else{
         $data=array('discount'=>0,'couponid'=>0,'msg'=>'Offer Not Valid on Any one or more than one product added in your cart!','coupon_disc_inpercent_perporduct'=>0);  
       }
     }else if($response->discountOn=='product'){
      foreach($this->cart->contents() AS $catid){
        $pram=array('cop_id'=>$copid,'discounton_id'=>$catid['proid'],'discount_on'=>'product','brand_id'=>$catid['brandid'],'vendor_id'=>$catid['VendorID']);
        $path =api_url().'discount/applycoupon_checkdiscountid/format/json/';
        $responsedata=curlpost($pram,$path);
        if($responsedata->discountedid!=''){
          if($applyondiscounted=='no' && ($catid['addiscount']>0 || $catid['prodiscount']>0)){
           $amounttobediscount+=0;
         }else{
           $amounttobediscount+=$catid['subtotal'];
         }
       }
     }
     $amounttobediscount=$amounttobediscount-$usedwalletamt_;
     if($amounttobediscount>0){
       $data=$this->discountsetting($copid,$discounttype,$amounttobediscount,$purchaseamout,$discounttype,$discountamt,$DiscountOnMaxAmt,$coupon_discounted_products,$MaxPurchaseAmt);
     }else{
       $data=array('discount'=>0,'couponid'=>0,'msg'=>'Offer Not Valid on Any one or more than one product added in your cart!','coupon_disc_inpercent_perporduct'=>0);  
     }
   }else if($response->discountOn=='all'){
    foreach($this->cart->contents() AS $catid){
      $pram=array('cop_id'=>$copid,'discounton_id'=>$catid['proid'],'discount_on'=>'all','brand_id'=>$catid['brandid'],'vendor_id'=>$catid['VendorID']);
      $path =api_url().'discount/applycoupon_checkdiscountid/format/json/';
      $responsedata=curlpost($pram,$path);
      if($responsedata->discountedid!=''){
       if($applyondiscounted=='no' && ($catid['addiscount']>0 || $catid['prodiscount']>0)){
         $amounttobediscount+=0;
       }else{
         $amounttobediscount+=$catid['subtotal'];
       }
     }
   }
   $amounttobediscount=$amounttobediscount-$usedwalletamt_;
   if($amounttobediscount>0){
    $data=$this->discountsetting($copid,$discounttype,$amounttobediscount,$purchaseamout,$discounttype,$discountamt,$DiscountOnMaxAmt,$coupon_discounted_products,$MaxPurchaseAmt);
  }else{
    $data=array('discount'=>0,'couponid'=>0,'msg'=>'Offer Not Valid on Any one or more than one product added in your cart!','coupon_disc_inpercent_perporduct'=>0);  
  }
}else if($response->discountOn=='category'){
  foreach($this->cart->contents() AS $catid){
    $pram=array('cop_id'=>$copid,'discounton_id'=>$catid['catid'],'discount_on'=>'category','brand_id'=>$catid['brandid'],'vendor_id'=>$catid['VendorID']);
    $path =api_url().'discount/applycoupon_checkdiscountid/format/json/';
    $responsedata=curlpost($pram,$path);
    if($responsedata->discountedid!=''){
      if($applyondiscounted=='no' && ($catid['addiscount']>0 || $catid['prodiscount']>0)){
        $amounttobediscount+=0;
      }else{
        $amounttobediscount+=$catid['subtotal'];
      }
    }
  }
  $amounttobediscount=$amounttobediscount-$usedwalletamt_;
  if($amounttobediscount>0){
    $data=$this->discountsetting($copid,$discounttype,$amounttobediscount,$purchaseamout,$discounttype,$discountamt,$DiscountOnMaxAmt,$coupon_discounted_products,$MaxPurchaseAmt);
  }else{
   $data=array('discount'=>0,'couponid'=>0,'msg'=>'Offer Not Valid on Any one or more than one product added in your cart!','coupon_disc_inpercent_perporduct'=>0);  
 } 
}
$this->session->set_userdata('couponcode',$CouponCode);
}
}
$this->session->set_userdata('discount',$data['discount']);
//$this->session->set_userdata('couponcode',$CouponCode);
$this->session->set_userdata('couponid',$data['couponid']);
$this->session->set_userdata('coupon_discounted_products',$coupon_discounted_products);
$this->session->set_userdata('coupon_disc_inpercent_perporduct',$data['coupon_disc_inpercent_perporduct']);
$usedwall= !empty($this->session->userdata('usedwalletamount')) ? $this->session->userdata('usedwalletamount') :0.00;
$usewalletarray=array('usedwalletamount'=>$usedwall);
$data=array_merge($data,$usewalletarray);
echo json_encode($data);
}
function diacountcalucaltion($amt_to_be_discount,$purchase_amt,$disc_type,$disc_amt){
 if($disc_type=='fixed'){
   $discamt=$disc_amt;
 }
 else{
  $discamt=($amt_to_be_discount*$disc_amt)/100;
}
return $discamt;
}
function discountsetting($copid,$discounttype,$amounttobediscount,$purchaseamout,$discounttype,$discountamt,$DiscountOnMaxAmt,$coupon_discounted_products,$MaxPurchaseAmt){
 if($discounttype=="fixed" && $purchaseamout==0.00){
                          	//$discounted_amt= $discountamt<$DiscountOnMaxAmt ? $discountamt : $DiscountOnMaxAmt;
  if($discountamt<$DiscountOnMaxAmt){
    $discounted_amt=$discountamt;
    $coupon_disc_inpercent_perporduct=$this->coupon_disc_in_percentage_calculation($discounted_amt,$amounttobediscount,$discountamt,$discounttype);
    $msg="Coupon applied succesfully";
  }else{
    $discounted_amt=$DiscountOnMaxAmt;
    $coupon_disc_inpercent_perporduct=($DiscountOnMaxAmt*100)/$amounttobediscount;
    $msg="Maximum Discount will be calculated on INR ".$MaxPurchaseAmt." /-";
  }
}else if($amounttobediscount>=$purchaseamout){
 if($this->diacountcalucaltion($amounttobediscount,$purchaseamout,$discounttype,$discountamt) < $DiscountOnMaxAmt){
   $discounted_amt=$this->diacountcalucaltion($amounttobediscount,$purchaseamout,$discounttype,$discountamt);
   $coupon_disc_inpercent_perporduct=$this->coupon_disc_in_percentage_calculation($discounted_amt,$amounttobediscount,$discountamt,$discounttype);
   $msg="Coupon applied succesfully";
 }else{
   $discounted_amt= $DiscountOnMaxAmt;
   $coupon_disc_inpercent_perporduct=($DiscountOnMaxAmt*100)/$amounttobediscount;
                             //$coupon_disc_inpercent_perporduct=$this->coupon_disc_in_percentage_calculation($amounttobediscount,$amounttobediscount,$discountamt,$discounttype);
   $msg="Maximum Discount will be calculated on INR ".$MaxPurchaseAmt." /-";
 }
}else{
 $copid=0;
 $discounted_amt=0;
 $msg="Minimum Cart Value should be INR 1000 on the offered products!";
 $coupon_disc_inpercent_perporduct=0;
                          	//$msg="Your amount is less than purchase amount for this coupon";
}
return  $array=array('couponid'=>$copid,'discount'=>$discounted_amt,'msg'=>$msg,'coupon_disc_inpercent_perporduct'=>$coupon_disc_inpercent_perporduct);
}
function coupon_disc_in_percentage_calculation($coupondisc_inrupees,$amttobediscount,$couponamt,$coupon_disc_type){
  if($coupon_disc_type=='fixed'){
    $disc_in_percent=($coupondisc_inrupees*100)/$amttobediscount;
  }else{
    $disc_in_percent=$couponamt;
  }
  return $disc_in_percent;
}  
function setwalletsession(){
  $usedwalletamt=$cop_disc=$totalcartamount=$shipppingchrg=$totalaftershipchrg=0.00;
  $cop_disc= !empty($this->session->userdata('discount')) ? $this->session->userdata('discount') :0.00;
  $totalcartamount=$this->cart->total();
  $shipppingchrg=tshippingcharge($totalcartamount);
  $totalaftershipchrg=Totalvalincart($totalcartamount,$shipppingchrg);
  if(($totalaftershipchrg-$cop_disc)>=$_POST['walletamt']){
   $usedwallet_cal=$_POST['walletamt'];
 }else{
  $usedwallet_cal=($totalaftershipchrg+$cop_disc);
}
$this->session->set_userdata('usedwalletamount',$usedwallet_cal);
$usedwalletamt= !empty($this->session->userdata('usedwalletamount')) ? $this->session->userdata('usedwalletamount') :0.00;
$newamount=($totalaftershipchrg-($usedwalletamt+$cop_disc));
$newamount= $newamount <=0 ? 0.00 : $newamount;
$newdata=array('newamount'=>$newamount,'coupondisc'=>$cop_disc,'usedwalletamt'=>$usedwalletamt,'msg'=>'Wallet applied successfully ','usedwalletamt'=>$usedwalletamt);
echo json_encode($newdata);
}




function test(){
	p($this->session->all_userdata());
}

public function cuponvalid(){


      
  //$regid = $this->session->userdata['logindetail']->UserRegId;




  $regid = '';
  
  $cuponcode = $_POST['coupon'];
  $parameter = array('p_userid' =>$regid,'couponcode' => $cuponcode);
  $path = api_url().'discount/applycuponvalid/format/json'; 
  $datadiscount  = curlpost($parameter,$path);         
  

  $data_c = json_decode(json_encode($datadiscount), true);
  
  //p($data_c); exit;
    

  $parameter6 = array('actmode'=>'categorydiscount', 'id'=> '');
  $path1 = api_url().'discount/specificcategorymaster/format/json';
  $data  = curlpost($parameter6,$path1);
  $data_categorydiscounty = json_decode(json_encode($data), true);

 // p($data_categorydiscounty); exit;

  $total = $this->cart->total();
 
  //p($this->cart->contents()); exit; 
 
        if($data_c['PurchaseAmt']>=$total){

           echo json_encode("Invalid"); 
           exit();
        }else{

           if(empty($data_c['nodata'])){

            $productids=explode(",", $data_c['ProductMapid']);
            
            foreach ($this->cart->contents() as $key => $value) {
              
           
                 if((in_array($value['id'],$productids)) || ($data_c['DiscountOn']=='all') || ($data_c['DiscountOn']=='user') || ($data_c['DiscountOn']=='cart') || ($value['catid']==$data_categorydiscounty['categorydisId'])){

                //
                  
                  //p($this->cart->total()); exit();
               if($data_c->PurchaseAmt <= $this->cart->total()){
                    
                    if($data_c['DiscountType'] == 'percentage') {
                       $discountAmt = ($data_c['DiscountAmt'])*($this->cart->total())/100;

                    }
                    else if($data_c['DiscountType'] == 'fixed') {
                        $discountAmt = $data_c['DiscountAmt'];

                    }

                   if ($value['product_type'] == 'single') {

                      $singleproducttotal[] = $value['subtotal'];

                  }
                 

                
            $total = $this->cart->total();

           /* $total_cat_discount = $this->session->userdata['toal_cart_disc']['total_cart_discount'];
            $dilivery_charge = $this->session->userdata['toal_cart_disc']['dilivery_charge'];
            $p_usedwalletamt = $this->session->userdata['toal_cart_disc']['usedwallet'];*/
           
              $parameter=array('act_mode'=>'view','p_id'=>0);
              $path=api_url().'cartapi/shippingcharge/format/json/';
              $shipdetail=curlpost($parameter,$path);
              $cartData = array(
                            
                            'coupanid'        =>  $data_c['copid'],
                            'coupondiscount'  =>  $discountAmt, 
                            'PurchaseAmt'     =>  $data_c['PurchaseAmt'], 
                            'couponcode'      =>  $cuponcode, 
                            'applyonexitdis'  =>  $data_c['applyonexitdis'],
                            'subtotal'        =>   $total 
                          
                      );

           

              $this->session->set_userdata('toal_cart_disc', $cartData);
              echo json_encode($cartData);  
              //p($cartData);
          
            }
        else{
             //p($data_c);   
            //echo "1"; exit();
            
            echo json_encode($data_c);
        } 
       exit;
            }
            else{
    
            }
       }
        }else{
      
              // p($data_c);   
              echo json_encode($data_c);
          }
            
     }
      
          
        
    }

}
?>