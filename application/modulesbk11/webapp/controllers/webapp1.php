<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Webapp1 extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
  }

  public function index(){

     if($this->session->userdata('bizzgainmember')->id){
    $mainslidepath = api_url().'home/mainslider/format/json/';
    $record['mainslide']  = curlget($mainslidepath);
    $rightbanpath = api_url().'home/rightbanner/format/json/';
    $record['rightban']  = curlget($rightbanpath);
    $selectpath1 = api_url().'home/selectoneimg/format/json/';
    $record['selectimgone']  = curlget($selectpath1);
    $selectpath2 = api_url().'home/selecttwoimg/format/json/';
    $record['selectimgtwo']  = curlget($selectpath2);
    $selectpath3 = api_url().'home/selectthreeimg/format/json/';
    $record['selectimgthree']  = curlget($selectpath3);
    $selectpath4 = api_url().'home/selectfourimg/format/json/';
    $record['selectimgfour']  = curlget($selectpath4);
    $sectionpath = api_url().'home/sectionmaster/format/json/';
    $record['sectionmasterview']  = curlget($sectionpath);
    $sectiondetpath = api_url().'home/sectiondetail/format/json/';
    $record['sectiondetailview']  = curlget($sectiondetpath);
    $iconpath = api_url().'home/iconimgdata/format/json/';
    $record['iconimg']  = curlget($iconpath);
    //p($record['iconimg']);exit();
    $this->load->view("helper/header1");
    $this->load->view("index",$record);
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function newsletter(){
    $useremail=$this->input->post('newsemails');
    $parameter=array('act_mode'=>'insert',
                     'useremail'=>$useremail,
                     'n_status'=>'A',
                     'rowid'=>''
                    );
    $path = api_url().'userapi/newlettersub/format/json/';
    $data=curlpost($parameter,$path);
    //p($data);exit;
    //$p['mail']=array('useremail' => $useremail);
    $parameter5 =array('act_mode'=>'bizzgainconfig','row_id'=>'');
    $result['bizzgainconfig']= $this->supper_admin->call_procedureRow('proc_siteconfig',$parameter5);
    $emailconfig=$result['bizzgainconfig']->confivalue;
    if($data->lastid>0){
  
      echo 'Newletter Subscription Successfull.';
    }
    else if($data->lastid==0){
      echo 'You are already listed for Newsletter';
    }
    else{
      echo 'Opps! Something went wrong!!';
    }
  }

  public function privacy_policy(){

    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("privacypolicy");
    $this->load->view("helper/footer1");

  }

  public function contactus(){

   if($this->input->post('c_submit')){
      $c_name=$this->input->post('c_name');
      $c_email=$this->input->post('c_email');
      $c_mobile=$this->input->post('c_mobile');
      $c_enquiry=$this->input->post('c_enquiry');
//p($c_name);exit();
      $bizzmail="support@mindzshop.com";
      $replyemail="anurag.dua@MindzShop.com";
       
    $msg ='<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <div style="float: left; padding: 2px 2px; background: #fff; border: solid 1px #F3F0F0;">
        <div style="padding: 10px 10px; float: left; width: 600px; font-family: sans-serif; font-size: 12px; line-height: 18px; background: #F5F5F5;">
            <div style="float:left;text-align: center;width:100%;">
                <img src="'.base_url().'images/logo3.png" />
            </div>
            <div style="float: left; width: 100%; text-align: center; padding: 10px 0px; font-weight: bold; font-size: 20px; ">Contact Us Enquiry Information</div>
            <div style="float:left;width:100%;font-size:15px;padding:10px 0px;">Dear User</div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Name : <span style="font-weight:100;">'.$c_name.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Email : <span style="font-weight:100;">'.$c_email.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Contact Number : <span style="font-weight:100;">'.$c_mobile.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Enquiry : <span style="font-weight:100;">'.$c_enquiry.'</span></div>
            <div style="float:left;width:100%;font-weight:bold;padding:15px 0px 5px;">
                Regards,<br />
                MindzShop Team
            </div>
        </div>
        <div></div>
    </div>

</body>
</html>
';
         $this->load->library('email');
         $this->email->from($bizzmail, 'MindzShop.com');
         $this->email->to($bizzmail);
         $this->email->cc('pawan@mindztechnology.com'); 
         $this->email->bcc($replyemail);
         $this->email->subject('Contact Us BIZZGAIN');
         $this->email->message($msg);  
         if($this->email->send()){
          $this->session->set_flashdata("message", "Information sent successfully. we will contact you Shortly.");
          redirect(base_url()."contactus");
        }
        else {
          $this->session->set_flashdata("message", "Information fail to send. Please try again");
          redirect(base_url()."contactus");
        }

    }
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("contactus");
    $this->load->view("helper/footer1");

  }

public function advancesearch(){
$myval = $this->input->post('thiavalue');
if(isset($_POST)){
$this->form_validation->set_rules('thiavalue', 'The Search', 'trim|required|xss_clean');
if($this->form_validation->run() == true){
  $parameter = array('getval' => $this->input->post('thiavalue'),'act_mode'=>'cat');          
  $len       = strlen($this->input->post('thiavalue'));
if($len>1){
if(!empty($this->input->post('thiavalue'))){
  $parameter       = array('getval' => $this->input->post('thiavalue'),'act_mode'=>'product');

  $path            = api_url().'productlisting/advancesearch/format/json/';
  $data['product'] = $response = curlpost($parameter, $path);
//p($data['product']);
}
  $this->load->view('searchdesine1',$data);
} 
}
}
}

public function search(){
 
  $this->userfunction->UserValidation();

$search=$this->input->get('token');

 $parameterp = array(
  'act_mode'=>'search',
  'token'=>$search, 
  'proName'=>'',
  'category_id'=>'',
  'barand_id'=>'',
  'size'=>'',
  'color'=>'',
  'minprice'=>'',
  'maxprice'=>'',
  'mindiscount'=>'',
  'pageorder'=>'',
  'pid'=>'',
  'perpage'=>'',
  'features'=>'',
  'procityidd'=>''
  
  
);
//p($parameterp); exit;
$data['resultspage']=$this->supper_admin->call_procedure('proc_search',$parameterp);


 
$this->load->view("helper/header1");
$this->load->view("helper/nav1");
$this->load->view("helper/nav2");
$this->load->view("productlist/search1",$data);
$this->load->view("helper/footer1");
}

public function searchnorecord(){
  if($this->session->userdata('bizzgainmember')->id){
  $this->load->view("helper/header1");
$this->load->view("helper/nav1");
$this->load->view("helper/nav2");
$this->load->view("productlist/no-record");
$this->load->view("helper/footer1");
}
  else
  {
    redirect(base_url());
  }

}

public function success($ordid){
  //p('hi');
  //p($ordid);
  //exit;
  //$ordid=1062;
   $this->cart->destroy();
   $this->session->unset_userdata('toal_cart_disc');
   $this->session->unset_userdata('cart_tax');

  $path = api_url().'checkoutapi/thankyou/orderId/'.$ordid.'/format/json/';
  $response['viewthanku'] = curlget($path);
  //p($response['viewthanku']);

  //exit;

    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("thankyou",$response);
    $this->load->view("helper/footer1");

}

public function landing(){
  
    if($this->session->userdata('bizzgainmember')->id){
    redirect(base_url()."home");
  }
  else{
    $this->load->view("landingpage");
     $this->load->view("helper/footer1");
  }
}

public function subscribe()
{
  $useremail=$this->input->post('subs');
  if(!empty($useremail))
  {
    $parameter=array('act_mode'=>'insert',
                     'useremail'=>$useremail,
                     'n_status'=>'A',
                     'rowid'=>''
                    );
    $path = api_url().'userapi/newlettersub/format/json/';
    $data=curlpost($parameter,$path);
  }
    
    //$this->session->unset_userdata('bizzgainmember');
    redirect(base_url().'myaccount');

}

public function morecategory(){
     if($this->session->userdata('bizzgainmember')->id){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("morecategory");
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function electronictoys(){
    $this->userfunction->UserValidation();
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/electronictoys");
    $this->load->view("helper/footer1");
  
  }

  public function softtoys(){
    $this->userfunction->UserValidation();
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/softtoys");
    $this->load->view("helper/footer1");
   
  }


  public function educationaltoys(){
     if($this->session->userdata('bizzgainmember')->id){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/educationaltoys");
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function dolltoys(){
    $this->userfunction->UserValidation();
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/dolltoys");
    $this->load->view("helper/footer1");
 
  }

  public function strollertoys(){
    $this->userfunction->UserValidation();
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/strollertoys");
    $this->load->view("helper/footer1");
    }

  public function rideontoys(){
    $this->userfunction->UserValidation();
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/rideontoys");
    $this->load->view("helper/footer1");
 
  }

  public function outdoortoys(){
    $this->userfunction->UserValidation();
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/outdoor");
    $this->load->view("helper/footer1");
  
  }

  public function diecasttoys(){
    $this->userfunction->UserValidation();
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/diecast");
    $this->load->view("helper/footer1");
 
  }

  public function babytoys(){
     if($this->session->userdata('bizzgainmember')->id){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/babytoys");
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function musicaltoys(){
     if($this->session->userdata('bizzgainmember')->id){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/musicaltoys");
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function indoortoys(){
     if($this->session->userdata('bizzgainmember')->id){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/indoortoys");
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function kidroom(){
     if($this->session->userdata('bizzgainmember')->id){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/kidroom");
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function puzzeltoys(){
     if($this->session->userdata('bizzgainmember')->id){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/puzzeltoys");
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function actiontoys(){
     if($this->session->userdata('bizzgainmember')->id){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/actiontoys");
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function activitytoys(){
     if($this->session->userdata('bizzgainmember')->id){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/activitytoys");
    $this->load->view("helper/footer1");
  }
  else{
    redirect(base_url());
  }
  }

  public function schooltoys(){
    $this->userfunction->UserValidation();
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("landingsubcat/schooltoys");
    $this->load->view("helper/footer1");
 
  }

  public function terms(){
     
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("terms");
    $this->load->view("helper/footer1");
  
  }

  public function aboutus(){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("aboutus");
    $this->load->view("helper/footer1");
  }

  public function careers(){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("careers");
    $this->load->view("helper/footer1");
  }

  public function faq(){
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("faq");
    $this->load->view("helper/footer1");
  }

  public function startbusiness(){

    if($this->input->post('sbsubmit')){
      $st_name=$this->input->post('st_name');
      //$st_address=$this->input->post('st_address');
      $st_email=$this->input->post('st_email');
      $st_mobile=$this->input->post('st_mobile');
      //$st_pincode=$this->input->post('st_pincode');
      $parameter=array('act_mode'=>'insertstartbus',
                     'row_id'=>'',
                     'b_username'=>$st_name,
                     'b_email'=>$st_email,
                     'b_mobilenum'=>$st_mobile,
                     'b_pincode'=>'',
                     'b_address'=>'',
                     'b_status'=>''
                    );
                  
    $path = api_url().'userapi/startbusiness/format/json/';
    $data=curlpost($parameter,$path);
    $bizzmail="support@mindzshop.com";
    $msg ='<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <div style="float: left; padding: 2px 2px; background: #fff; border: solid 1px #F3F0F0;">
        <div style="padding: 10px 10px; float: left; width: 600px; font-family: sans-serif; font-size: 12px; line-height: 18px; background: #F5F5F5;">
            <div style="float:left;text-align: center;width:100%;">
                <img src="'.base_url().'images/logo3.png" />
            </div>
            <div style="float: left; width: 100%; text-align: center; padding: 10px 0px; font-weight: bold; font-size: 20px; ">START YOUR OWN BUSINESS USER INFORMATION</div>
            <div style="float:left;width:100%;font-size:15px;padding:10px 0px;">Dear User</div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Name : <span style="font-weight:100;">'.$st_name.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Email : <span style="font-weight:100;">'.$st_email.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Contact Number : <span style="font-weight:100;">'.$st_mobile.'</span></div>
            <div style="float:left;width:100%;font-weight:bold;padding:15px 0px 5px;">
                Regards,<br />
                MindzShop Team
            </div>
        </div>
        <div></div>
    </div>

</body>
</html>
';
         $this->load->library('email');
         $this->email->from($bizzmail, 'START YOUR OWN BUSINESS WITH BIZZGAIN');
         $this->email->to($st_email); 
         //$this->email->bcc($replyemail);
         $this->email->subject('START YOUR OWN BUSINESS WITH BIZZGAIN');
         $this->email->message($msg);  
         if($this->email->send()){
          $this->session->set_flashdata("message", "Information sent successfully. we will contact you as soon as possible.");
          redirect(base_url()."startbusiness");
        }
        else {
          $this->session->set_flashdata("message", "Information fail to send. Please try again");
          redirect(base_url()."startbusiness");
        }

    //$this->session->set_flashdata("message", "Information sent successfully. we will contact you as soon as possible.");
    //redirect(base_url()."startbusiness");

    }

    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("startbusiness");
    $this->load->view("helper/footer1");
  }

  public function creditpurchase(){
     
    $this->load->view("helper/header1");
    $this->load->view("helper/nav1");
    $this->load->view("helper/nav2");
    $this->load->view("creditpurchase");
    $this->load->view("helper/footer1");
  
  }

public function ajaxsearch(){
  
  //p($this->uri->segments); exit();

  $ex=explode('&',$this->uri->segment(3));

  foreach ($ex as $key => $value) {
 $ex2[]=explode('=',$value);
    }
//p($ex2);//exit;


    $search=$ex2[0][1];




if($ex2[2][0]=='sbrand' && $ex2[1][0]=='show_brand')
{
    if($ex2[3][0]=='porder' )
    {
        $sbrand=urldecode($ex2[1][1]);
        $porder=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else
    {
          $show_brandfb=urldecode($ex2[1][1]);
          $sbrandfb=urldecode($ex2[2][1]);
          $catfb=urldecode($ex2[3][1]);

          if($show_brandfb!='' && $sbrandfb!='')
          {
              $sbrand=$show_brandfb;
              $cattid1=urldecode($ex2[3][1]);
          }
    }
               
}

if($ex2[3][0]=='sbrand' && $ex2[1][0]=='show_brand')
{
          $show_brandfb=urldecode($ex2[1][1]);
          $por=urldecode($ex2[2][1]);
          $sbrandfb=urldecode($ex2[3][1]);
          $catfb=urldecode($ex2[4][1]);

      if($show_brandfb!='' && $sbrandfb!='')
      {
        $sbrand=$show_brandfb;
        $porder=$por;
        $cattid1=$catfb;
      }
               
}

else if($ex2[2][0]=='price' && $ex2[3][0]=='discount' && $ex2[4][0]=='color' && $ex2[5][0]=='size')
{
    $sbrand=urldecode($ex2[1][1]);
    $price=urldecode($ex2[2][1]);
    $discount=urldecode($ex2[3][1]);
    $color=urldecode($ex2[4][1]);
    $size=urldecode($ex2[5][1]);
    $cattid1=urldecode($ex2[6][1]);//exit;
}
else if($ex2[2][0]=='discount' && $ex2[3][0]=='size')
{ 
    $sbrand=urldecode($ex2[1][1]);
    $discount=urldecode($ex2[2][1]);
    
    $size=urldecode($ex2[3][1]);
    $cattid1=urldecode($ex2[5][1]);//exit;
}
else if($ex2[2][0]=='discount' && $ex2[3][0]=='color')
{ 
    $sbrand=urldecode($ex2[1][1]);
    $discount=urldecode($ex2[2][1]);
    
    $color=urldecode($ex2[3][1]);
    $cattid1=urldecode($ex2[5][1]);//exit;
}
else if($ex2[2][0]=='discount')
{
    $sbrand=urldecode($ex2[1][1]);
    $discount=urldecode($ex2[2][1]);
    $cattid1=urldecode($ex2[4][1]);
}
else if($ex2[2][0]=='price')
{
    $sbrand=urldecode($ex2[1][1]);
    $price=urldecode($ex2[2][1]);
    $cattid1=urldecode($ex2[4][1]);

    if($ex2[3][0]=='discount')
    {
        $discount=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[5][1]);
    }
    if($ex2[3][0]=='color')
    {
        $color=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[5][1]);
    }
    if($ex2[3][0]=='size')
    {
        $size=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[5][1]);
    }
}
else if($ex2[2][0]=='color')
{
    if($ex2[1][0]=='price')
    {
        $price=urldecode($ex2[1][1]);
        $color=urldecode($ex2[2][1]);
        $sbrand=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else
    {
        $sbrand=urldecode($ex2[1][1]);
        $color=urldecode($ex2[2][1]);
        $cattid1=urldecode($ex2[4][1]);
      }
}
else if($ex2[2][0]=='size')
{
    if($ex2[1][0]=='price')
    {
        $price=urldecode($ex2[1][1]);
        $size=urldecode($ex2[2][1]);
        $sbrand=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else
    {
      $sbrand=urldecode($ex2[1][1]);
      $size=urldecode($ex2[2][1]);
      $cattid1=urldecode($ex2[4][1]);
    }
}
else if($ex2[1][0]=='discount')
{
    if($ex2[3][0]=='porder')
    {
        $discount=urldecode($ex2[1][1]);
        $sbrand=urldecode($ex2[2][1]);
        $porder=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else if($ex2[2][0]=='porder')
    {
        $discount=urldecode($ex2[1][1]);
        $porder=urldecode($ex2[2][1]);
        $sbrand=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else
    {
        $discount1=urldecode($ex2[1][1]);
        $sbrandfd=urldecode($ex2[2][1]);
        $catfd=urldecode($ex2[3][1]);
        if($discount1!='')
        {
          $discount=$discount1;
          $sbrand=$sbrandfd;
          $cattid1=$catfd;
        }
    }
}

else if($ex2[1][0]=='size')
{
    if($ex2[3][0]=='porder')
    {
        $size=urldecode($ex2[1][1]);
        $sbrand=urldecode($ex2[2][1]);
        $porder=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else if($ex2[2][0]=='porder')
    {
        $size=urldecode($ex2[1][1]);
        $porder=urldecode($ex2[2][1]);
        $sbrand=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else
    {
        $size1=urldecode($ex2[1][1]);
        $sbrandfs=urldecode($ex2[2][1]);
        $catfs=urldecode($ex2[3][1]);
        if($size1!='')
        {
          $size=$size1;
          $sbrand=$sbrandfs;
          $cattid1=$catfs;
        }
    }    
}
else if($ex2[1][0]=='color')
{
    if($ex2[3][0]=='porder')
    {
        $color=urldecode($ex2[1][1]);
        $sbrand=urldecode($ex2[2][1]);
        $porder=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else if($ex2[2][0]=='porder')
    {
        $color=urldecode($ex2[1][1]);
        $porder=urldecode($ex2[2][1]);
        $sbrand=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else
    {
        $color1=urldecode($ex2[1][1]);
        $sbrandfc=urldecode($ex2[2][1]);
        $catfc=urldecode($ex2[3][1]);
        if($color1!='')
        {
          $color=$color1;
          $sbrand=$sbrandfc;
          $cattid1=$catfc;
        }
    }    
}

else if($ex2[1][0]=='price')
{
    if($ex2[3][0]=='porder')
    {
        $price=urldecode($ex2[1][1]);
        $sbrand=urldecode($ex2[2][1]);
        $porder=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else if($ex2[2][0]=='porder')
    {
        $price=urldecode($ex2[1][1]);
        $porder=urldecode($ex2[2][1]);
        $sbrand=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else
    {
        $price1=urldecode($ex2[1][1]);
        $sbrandfp=urldecode($ex2[2][1]);
        $catfp=urldecode($ex2[3][1]);
        if($price1!='')
        {
          $price=$price1;
          $sbrand=$sbrandfp;
          $cattid1=$catfp;
        }
     }   
}
else if($ex2[1][0]=='sbrand')
{

    if($ex2[2][0]=='porder')
    {
        $sbrand=urldecode($ex2[1][1]);
        $porder=urldecode($ex2[2][1]);
        $cattid1=urldecode($ex2[3][1]);
    }
    else
    {
        $sbrandsb1=urldecode($ex2[1][1]);
        $catfsb=urldecode($ex2[2][1]);
        
        if($sbrandsb1!='')
        {
          $sbrand=$sbrandsb1;
          $cattid1=$catfsb;
        }
    }
}

else if($ex2[2][0]=='show_brand')
{
    $catfsb=urldecode($ex2[1][1]);
    $showb=urldecode($ex2[2][1]);
    
    if($showb!='')
    {
      $sbrand=$showb;
      $cattid1=$catfsb;
    }
    else if($ex2[4][0]=='sbrand' && $ex2[2][0]=='show_brand')
    { //echo "ok";

           $sbrand=urldecode($ex2[4][1]);
           $show_brand=urldecode($ex2[2][1]);
            $sdata=urldecode($ex2[1][1]);


        $newssdata= explode('#',$sdata);
        //p($newssdata);exit;

        foreach ($newssdata as $key => $value) {
         $newcat= explode(',',$value);
          $sdsa .=$newcat[0].',';
         $brandsdsa .= $newcat[2].',';



        if($brandsdsa=='')
        {
          $brandsdsa1 =  0;
          
        }else{
          
          $brandsdsa1 =  rtrim($brandsdsa,',');
           $brandsdsa1 = implode(',', array_keys(array_flip(explode(',', $brandsdsa1))));

        }

        }

        $cattid1=trim($sdsa,',');

        $j=1;
    }
}

else if($ex2[4][0]=='sbrand' && $ex2[2][0]=='show_brand')
{ //echo "ok";

           $sbrand=urldecode($ex2[4][1]);
           $show_brand=urldecode($ex2[2][1]);
            $sdata=urldecode($ex2[1][1]);


        $newssdata= explode('#',$sdata);
        //p($newssdata);exit;

        foreach ($newssdata as $key => $value) {
         $newcat= explode(',',$value);
          $sdsa .=$newcat[0].',';
         $brandsdsa .= $newcat[2].',';



        if($brandsdsa=='')
        {
          $brandsdsa1 =  0;
          
        }else{
          
          $brandsdsa1 =  rtrim($brandsdsa,',');
           $brandsdsa1 = implode(',', array_keys(array_flip(explode(',', $brandsdsa1))));

        }

        }

        $cattid1=trim($sdsa,',');

        $j=1;
}
else
{
}

if(!empty($sdata)){
$s_catid=$s_brandid=" ";
$realdata= str_replace("~"," ",$sdata);
$liararray= explode('|',$realdata);
//p($liararray);
foreach($liararray as $key => $value_cat ){
$list= explode(',',$value_cat);
//p($list);
if(trim(strtolower($list[3]))==trim(strtolower($search))){
$sbrand=$list[2];
//p($sbrand);
}
if(trim(strtolower($list[1]))==trim(strtolower($search))){
 $s_token_catid=$list[0]; 
}
if(strpos($s_brandid,$list[2])){
}else{
$s_brandid.=$list[2].',';
}
if($j<5){
if(strpos($s_catid,$list[0])){
}else{
//  $s_catid.=$list[0].',';
$s_catid=$list[0].','.$s_catid;
$j++;
}
}
}
$s_catid = str_replace(" ","",$s_catid);
$s_catid=rtrim($s_catid,',');
//$cattid1=$s_catid;
}

//if(empty($cattid1)){

/*if($cattid1==''){
  
$myurl=base_url().'searchnorecord';
redirect($myurl);
exit;
}*/
if(!empty($s_token_catid)){
$cattid1=$s_token_catid;
}
$pa = array('act_mode'=>'catsearch','row_id'=>$cattid1,'');
//p($pa); exit;
$data['catsearch']=$this->supper_admin->call_procedure('proc_productmasterdata',array('act_mode'=>'catsearch','row_id'=>$cattid1,''));
//p($data['catsearch']); exit();

$parameter=array('act_mode'=>'attview','row_id'=>$cattid1,'','','','','');
//p($parameter);exit;
if($show_brand!=''){
  $brand=$show_brand;
  $brandurl='&show_brand='.$brand;
}
else if(empty($show_brand) && !empty($sbrand)){
   $brand=$sbrand;
   $brandurl='&show_brand='.$brand;
}
else if(!empty($show_brand) && !empty($sbrand))
{
  $brand=$sbrand;
  $brandurl='&show_brand='.$brand;
}

  
if($price!=''){
  $price=explode('-',$price);
  $minprice=$price[0];
  $maxprice=$price[1];
  $priceurl='&price='.$price;
}
else{
  $minprice=0;
  $maxprice=0;
  $priceurl=null; 
}

if($discount!=''){
  //$discount=$this->input->get('discount');
  $discounturl='&discount='.$discount;
  //echo "Hello".$discounturl; exit;
}
else{
  $discount=0;
  $discounturl=null;
}
if($color!=''){
  $color=$color;
  $colorurl='&color='.$color;
}
else{
  $color=0;
  $colorurl=null;
}
if($size!=''){
  $size=$size;
  $sizeurl='&size='.$size;
}
else{
  $size=0;
  $sizeurl=null;
}

if($this->input->get('feature')){
  $name=$this->input->get('feature');
  $name2 = str_replace("%20"," ", $name);
  $feature3=str_replace(",","|",$name2);
  $feature =strtolower($feature3);
  $featureurl='&feature='.$name;
}
else{
  $feature=0;
  $featureurl=null;
}

if($this->input->get('feature')){
  $feature=$this->input->get('feature');
  $featureurl='&feature='.$feature;
 }
else{
  $feature="0";
  $featureurl=null;
 }
if($porder!=''){
  $pageorder=$porder;
  $porderulr='&porder='.$pageorder;
   }
else{
   $pageorder=0;
   $porderulr=null;
}
$totallistoffset1   = $_POST['offset'];

if($totallistoffset1==0){
  $totallistoffset = 0;
}else{
  $totallistoffset = $_POST['offset'];
  #$totallistoffset = $_POST['offset']+12;
}

$searchnew=str_replace('+', ' ', $search);
$parameterp = array(
  'proName'=>$searchnew,
  'category_id'=>$cattid1,
  'barand_id'=>$brand,
  'size'=>$size,
  'color'=>$color,
  'minprice'=>$minprice,
  'maxprice'=>$maxprice,
  'mindiscount'=>$discount,
  'pageorder'=>$pageorder,
  'pid'  => $totallistoffset,
  'perpage'  => $_POST['number'],
  'features'=>$feature,
  'procityidd'=>$this->session->userdata('logincity')->cmid
  //'token'=>'',
  //'actmode'=>'' 
  );
//p($parameterp); exit;
$data['resultspage']=$this->supper_admin->call_procedure('proc_Productlistnew',$parameterp);
//p($data['resultspage']);
$data['image_view']=$_POST['seetyp'];
 $this->load->view('productlist/searchnew1',$data);

  }






/*
public function ajaxsearch(){
  
  //p($this->uri->segments); exit();

  $ex=explode('&',$this->uri->segment(3));

  foreach ($ex as $key => $value) {
 $ex2[]=explode('=',$value);
    }
//p($ex2);//exit;


    $search=$ex2[0][1];



    if($ex2[2][0]=='sbrand' && $ex2[1][0]=='show_brand')
    {
          $show_brandfb=urldecode($ex2[1][1]);
          $sbrandfb=urldecode($ex2[2][1]);
          $catfb=urldecode($ex2[3][1]);

      if($show_brandfb!='' && $sbrandfb!='')
      {
        $sbrand=$show_brandfb;
        $cattid1=urldecode($ex2[3][1]);
      }
               
}

else if($ex2[2][0]=='price' && $ex2[3][0]=='discount' && $ex2[4][0]=='color' && $ex2[5][0]=='size')
{
    $sbrand=urldecode($ex2[1][1]);
    $price=urldecode($ex2[2][1]);
    $discount=urldecode($ex2[3][1]);
    $color=urldecode($ex2[4][1]);
    $size=urldecode($ex2[5][1]);
    $cattid1=urldecode($ex2[6][1]);//exit;
}
else if($ex2[2][0]=='discount' && $ex2[3][0]=='size')
{ 
    $sbrand=urldecode($ex2[1][1]);
    $discount=urldecode($ex2[2][1]);
    
    $size=urldecode($ex2[3][1]);
    $cattid1=urldecode($ex2[5][1]);//exit;
}
else if($ex2[2][0]=='discount' && $ex2[3][0]=='color')
{ 
    $sbrand=urldecode($ex2[1][1]);
    $discount=urldecode($ex2[2][1]);
    
    $color=urldecode($ex2[3][1]);
    $cattid1=urldecode($ex2[5][1]);//exit;
}
else if($ex2[2][0]=='discount')
{
    $sbrand=urldecode($ex2[1][1]);
    $discount=urldecode($ex2[2][1]);
    $cattid1=urldecode($ex2[4][1]);
}
else if($ex2[2][0]=='price')
{
    $sbrand=urldecode($ex2[1][1]);
    $price=urldecode($ex2[2][1]);
    $cattid1=urldecode($ex2[4][1]);

    if($ex2[3][0]=='discount')
    {
        $discount=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[5][1]);
    }
    if($ex2[3][0]=='color')
    {
        $color=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[5][1]);
    }
    if($ex2[3][0]=='size')
    {
        $size=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[5][1]);
    }
}
else if($ex2[2][0]=='color')
{
    if($ex2[1][0]=='price')
    {
        $price=urldecode($ex2[1][1]);
        $color=urldecode($ex2[2][1]);
        $sbrand=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else
    {
        $sbrand=urldecode($ex2[1][1]);
        $color=urldecode($ex2[2][1]);
        $cattid1=urldecode($ex2[4][1]);
      }
}
else if($ex2[2][0]=='size')
{
    if($ex2[1][0]=='price')
    {
        $price=urldecode($ex2[1][1]);
        $size=urldecode($ex2[2][1]);
        $sbrand=urldecode($ex2[3][1]);
        $cattid1=urldecode($ex2[4][1]);
    }
    else
    {
      $sbrand=urldecode($ex2[1][1]);
      $size=urldecode($ex2[2][1]);
      $cattid1=urldecode($ex2[4][1]);
    }
}
else if($ex2[1][0]=='discount')
{
    $discount1=urldecode($ex2[1][1]);
    $sbrandfd=urldecode($ex2[2][1]);
    $catfd=urldecode($ex2[3][1]);
    if($discount1!='')
    {
      $discount=$discount1;
      $sbrand=$sbrandfd;
      $cattid1=$catfd;
    }
}

else if($ex2[1][0]=='size')
{
    $size1=urldecode($ex2[1][1]);
    $sbrandfs=urldecode($ex2[2][1]);
    $catfs=urldecode($ex2[3][1]);
    if($size1!='')
    {
      $size=$size1;
      $sbrand=$sbrandfs;
      $cattid1=$catfs;
    }
}
else if($ex2[1][0]=='color')
{
    $color1=urldecode($ex2[1][1]);
    $sbrandfc=urldecode($ex2[2][1]);
    $catfc=urldecode($ex2[3][1]);
    if($color1!='')
    {
      $color=$color1;
      $sbrand=$sbrandfc;
      $cattid1=$catfc;
    }
}

else if($ex2[1][0]=='price')
{
    $price1=urldecode($ex2[1][1]);
    $sbrandfp=urldecode($ex2[2][1]);
    $catfp=urldecode($ex2[3][1]);
    if($price1!='')
    {
      $price=$price1;
      $sbrand=$sbrandfp;
      $cattid1=$catfp;
    }
}
else if($ex2[1][0]=='sbrand')
{
    $sbrandsb1=urldecode($ex2[1][1]);
    $catfsb=urldecode($ex2[2][1]);
    
    if($sbrandsb1!='')
    {
      $sbrand=$sbrandsb1;
      $cattid1=$catfsb;
    }
}

else if($ex2[2][0]=='show_brand')
{
    $catfsb=urldecode($ex2[1][1]);
    $showb=urldecode($ex2[2][1]);
    
    if($showb!='')
    {
      $sbrand=$showb;
      $cattid1=$catfsb;
    }
    else if($ex2[4][0]=='sbrand' && $ex2[2][0]=='show_brand')
    { //echo "ok";

           $sbrand=urldecode($ex2[4][1]);
           $show_brand=urldecode($ex2[2][1]);
            $sdata=urldecode($ex2[1][1]);


        $newssdata= explode('#',$sdata);
        //p($newssdata);exit;

        foreach ($newssdata as $key => $value) {
         $newcat= explode(',',$value);
          $sdsa .=$newcat[0].',';
         $brandsdsa .= $newcat[2].',';



        if($brandsdsa=='')
        {
          $brandsdsa1 =  0;
          
        }else{
          
          $brandsdsa1 =  rtrim($brandsdsa,',');
           $brandsdsa1 = implode(',', array_keys(array_flip(explode(',', $brandsdsa1))));

        }

        }

        $cattid1=trim($sdsa,',');

        $j=1;
    }
}

else if($ex2[4][0]=='sbrand' && $ex2[2][0]=='show_brand')
{ //echo "ok";

           $sbrand=urldecode($ex2[4][1]);
           $show_brand=urldecode($ex2[2][1]);
            $sdata=urldecode($ex2[1][1]);


        $newssdata= explode('#',$sdata);
        //p($newssdata);exit;

        foreach ($newssdata as $key => $value) {
         $newcat= explode(',',$value);
          $sdsa .=$newcat[0].',';
         $brandsdsa .= $newcat[2].',';



        if($brandsdsa=='')
        {
          $brandsdsa1 =  0;
          
        }else{
          
          $brandsdsa1 =  rtrim($brandsdsa,',');
           $brandsdsa1 = implode(',', array_keys(array_flip(explode(',', $brandsdsa1))));

        }

        }

        $cattid1=trim($sdsa,',');

        $j=1;
}
else
{
}

if(!empty($sdata)){
$s_catid=$s_brandid=" ";
$realdata= str_replace("~"," ",$sdata);
$liararray= explode('|',$realdata);
//p($liararray);
foreach($liararray as $key => $value_cat ){
$list= explode(',',$value_cat);
//p($list);
if(trim(strtolower($list[3]))==trim(strtolower($search))){
$sbrand=$list[2];
//p($sbrand);
}
if(trim(strtolower($list[1]))==trim(strtolower($search))){
 $s_token_catid=$list[0]; 
}
if(strpos($s_brandid,$list[2])){
}else{
$s_brandid.=$list[2].',';
}
if($j<5){
if(strpos($s_catid,$list[0])){
}else{
//  $s_catid.=$list[0].',';
$s_catid=$list[0].','.$s_catid;
$j++;
}
}
}
$s_catid = str_replace(" ","",$s_catid);
$s_catid=rtrim($s_catid,',');
//$cattid1=$s_catid;
}

//if(empty($cattid1)){

//if($cattid1==''){
  
//$myurl=base_url().'searchnorecord';
//redirect($myurl);
//exit;
//}
if(!empty($s_token_catid)){
$cattid1=$s_token_catid;
}
$pa = array('act_mode'=>'catsearch','row_id'=>$cattid1,'');
//p($pa); exit;
$data['catsearch']=$this->supper_admin->call_procedure('proc_productmasterdata',array('act_mode'=>'catsearch','row_id'=>$cattid1,''));
//p($data['catsearch']); exit();

$parameter=array('act_mode'=>'attview','row_id'=>$cattid1,'','','','','');
//p($parameter);exit;
if($show_brand!=''){
  $brand=$show_brand;
  $brandurl='&show_brand='.$brand;
}
else if(empty($show_brand) && !empty($sbrand)){
   $brand=$sbrand;
   $brandurl='&show_brand='.$brand;
}
else if(!empty($show_brand) && !empty($sbrand))
{
  $brand=$sbrand;
  $brandurl='&show_brand='.$brand;
}

  
if($price!=''){
  $price=explode('-',$price);
  $minprice=$price[0];
  $maxprice=$price[1];
  $priceurl='&price='.$price;
}
else{
  $minprice=0;
  $maxprice=0;
  $priceurl=null; 
}

if($discount!=''){
  //$discount=$this->input->get('discount');
  $discounturl='&discount='.$discount;
  //echo "Hello".$discounturl; exit;
}
else{
  $discount=0;
  $discounturl=null;
}
if($color!=''){
  $color=$color;
  $colorurl='&color='.$color;
}
else{
  $color=0;
  $colorurl=null;
}
if($size!=''){
  $size=$size;
  $sizeurl='&size='.$size;
}
else{
  $size=0;
  $sizeurl=null;
}

if($this->input->get('feature')){
  $name=$this->input->get('feature');
  $name2 = str_replace("%20"," ", $name);
  $feature3=str_replace(",","|",$name2);
  $feature =strtolower($feature3);
  $featureurl='&feature='.$name;
}
else{
  $feature=0;
  $featureurl=null;
}

if($this->input->get('feature')){
  $feature=$this->input->get('feature');
  $featureurl='&feature='.$feature;
 }
else{
  $feature="0";
  $featureurl=null;
 }
if($this->input->get('porder')!=''){
  $pageorder=$this->input->get('porder');
  $porderulr='&porder='.$pageorder;
   }
else{
   $pageorder=0;
   $porderulr=null;
}
$totallistoffset1   = $_POST['offset'];

if($totallistoffset1==0){
  $totallistoffset = 0;
}else{
  $totallistoffset = $_POST['offset'];
  #$totallistoffset = $_POST['offset']+12;
}


$parameterp = array(
  'proName'=>$search,
  'category_id'=>$cattid1,
  'barand_id'=>$brand,
  'size'=>$size,
  'color'=>$color,
  'minprice'=>$minprice,
  'maxprice'=>$maxprice,
  'mindiscount'=>$discount,
  'pageorder'=>$pageorder,
  'pid'  => $totallistoffset,
  'perpage'  => $_POST['number'],
  'features'=>$feature,
  'procityidd'=>$this->session->userdata('logincity')->cmid
  //'token'=>'',
  //'actmode'=>'' 
  );
//p($parameterp); exit;
$data['resultspage']=$this->supper_admin->call_procedure('proc_Productlistnew',$parameterp);
//p($data['resultspage']);

 $this->load->view('productlist/searchnew',$data);

  }

*/




}//end of class
?>