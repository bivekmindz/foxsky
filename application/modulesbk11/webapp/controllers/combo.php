<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Combo extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
    $this->userfunction->UserValidation();
  }


//----------  Start Combo Product Listing ----------- //
 
 public function combolisting(){



if($this->input->get('price')!=''){
  $price       = explode('-',$this->input->get('price'));
  $minprice    = $price[0];
  $maxprice    = $price[1];
  $priceurl    = '&price='.$this->input->get('price');
  //p($price); exit;
}
else{
  $minprice    = 0;
  $maxprice    = 0;
  $priceurl    = null; 
}



  $parameter     = array(
                          'proName'    =>'',
                          'category_id'=>'',
                          'barand_id'  =>'',
                          'size'       =>'',
                          'color'      =>'',
                          'minprice'   =>$minprice,
                          'maxprice'   =>$maxprice,
                          'mindiscount'=>'',
                          'pageorder'   =>'',
                          'pid'       =>'',
                          'perpage'   =>'',
                          'features'  =>'',
                          'procityidd'=>'' 
                          );
      
      //p($parameter);exit();
     
  $result['resultspage'] =  $this->supper_admin->call_procedure('proc_combolistnew',$parameter); 
     
//p($result['resultspage']); exit;
  

  $this->load->view("helper/header");
  $this->load->view("helper/nav1");
  $this->load->view("helper/nav2");
  $this->load->view("productlist/combolisting",$result);
  $this->load->view("helper/footer");


}

//----------  Start Combo Product Listing ----------- //


//----------- start combo details -------------------//



public function productdetail(){

 $reviews          = '#'.$this->input->get('reviewsre'); 
 //$data['pinCheck'] = $this->input->cookie('pin',true);
 $url              = array_pop($this->uri->segments);
 $urlexp           = explode("-",$url);
 $urlexp2          = $urlexp;
 $getProductId     = array_pop($urlexp);



 if($getProductId == ""){
  redirect();
  exit();
  }
 //$p_cityid=$this->session->userdata('logincity')->cmid;
 /*$path = api_url().'productlisting/getPdetail/name/'.$getProductId.'/cityidd/'.$p_cityid.'/format/json/';
 $response = curlget($path);*/
  $parameter = array(

        'act_mode'  =>  'cominfo',
        'p_name'  =>  '',
        'p_qty'  =>  '',
        'p_start'  =>  '',
        'p_end'  =>  '',
        'p_ingridients'  => '' ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  '',
        'p_comboid'   => $getProductId,
        'p_cat_id'    =>  '',
        'p_sub_catid' => '',
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
     
      );


  
   $response['ProductDetails'] = $this->supper_admin->call_procedure('proc_saveCombo', $parameter); 

 $p_cityid=$this->session->userdata('logincity')->cmid;

 //p($response['ProductDetails']); exit;


$path = api_url().'productlisting/get_bredcum/catid/'.$response['ProductDetails'][0]->sub_catid.'/act/parent/format/json/';
 
 $responce = (array) curlget($path);
 
  $pathsimilarProduct = api_url().'productlisting/getsimilarcomboProduct/catId/'.$response['ProductDetails'][0]->sub_catid.'/cityidd/'.$p_cityid.'/format/json/';
  
  

  $response['similarProduct']  = curlget($pathsimilarProduct); 
 //p($response['similarProduct']->promapid);
 $smproids=array();
 foreach ($response['similarProduct'] as $key => $value) {
   array_push($smproids, $value['promapid']);
 }
 //p($smproids);
 if(!empty($smproids)){
  $getmvproids=str_replace(',', '~', implode(',', $smproids));
 } else {
  $getmvproids='';
 }



 $data['preant_bredcum']=array('name'=>$responce[0]['catname'],'url'=>base_url().'maincategory/'.str_replace('&','en',strtolower(str_replace(' ','-',$responce[0]['catname']))).'-'.$responce[0]['catid'].'.html');
 $path = api_url().'productlisting/get_bredcum/catid/'.$response->catid.'/act/child/format/json/';
 $responce = (array) curlget($path);
 
 $data['child_bredcum']=array('name'=>$responce[0]['catname'],'url'=>base_url().'category/'.str_replace('&','en',strtolower(str_replace(' ','-',$responce[0]['catname']))).'-'.$responce[0]['catid'].'.html');

 /*$pathsimilarProduct = api_url().'productlisting/getsimilarProduct/catId/'. $response->catid.'/promapId/'.$getProductId.'/cityidd/'.$p_cityid.'/format/json/';
 $data['similarProduct']  = curlget($pathsimilarProduct); */
 $path1 = api_url().'productlisting/getviewProductcombo/catId/'.$response['ProductDetails'][0]->sub_catid.'/promapId/'.$getmvproids.'/cityidd/'.$p_cityid.'/format/json/';
 $response['mostview']  = curlget($path1); 
 //p($response['mostview']);


 if(!empty($response->Size)){
   $sizeId = $response->Size;
   $explodeSizeId = explode(',', $sizeId);
   $explodedId = explode(':',$explodeSizeId[0]);
   $canonicalUrlId = $explodedId['1'];
   $patha = api_url().'productlisting/getPdetail/name/'.$canonicalUrlId.'/format/json/';
   $responsee = curlget($patha);
   $productNamee = $responsee->ProName;
   $data['canonicalUrlId'] = seoUrl($productNamee).'-'.$canonicalUrlId.'.html';
}else{
   $data['canonicalUrlId']  = "";
}
  //confirmurlDetail($urlexp2, $response->ProName,$response->promapid,$reviews);

if($response == 'Something Wen Wrong'){
  //redirect('webapp');
  //exit();
}else{
   
  $data['ProductDetails'] = $response;
  $productId              = $getProductId;
           
$path = api_url().'productlisting/productfeatures/format/json/';
$data['profeature'] = curlpost(array('proId'=>$productId),$path);

} 

  //p($response); exit;

$this->load->view("helper/header");
$this->load->view("helper/nav1");
$this->load->view("helper/nav2");
//$this->load->view("productdetail/combodetail", $response);
$this->load->view("productdetail/comdetail", $response);
$this->load->view("helper/footer");

}

//-------------------end detail page -------------------------// 

//------------------- combo insert in cart  -----------------------//


public function comboaddtocart()
{
  



   $Productid = $_POST['proId']; 
  
    $parameter = array(

        'act_mode'  =>  'cominfo',
        'p_name'  =>  '',
        'p_qty'  =>  '',
        'p_start'  =>  '',
        'p_end'  =>  '',
        'p_ingridients'  => '' ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  '',
        'p_comboid'   => $Productid,
        'p_cat_id'    =>  '',
        'p_sub_catid' => '',
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
     
      );


 
   $response = $this->supper_admin->call_procedure('proc_saveCombo', $parameter); 
   
   //p($response); exit; 
   $mulcat=array();
   foreach ($response as $key => $value) {
     array_push($mulcat, $value->sub_catid);
   }
   $combomulcat=implode(',', $mulcat);
   //p($combomulcat);exit;
    
    $newfinalPrice = '';
// now calling the api to check the product avalilibility
   //$p_cityid=$this->session->userdata('logincity')->cmid;
   
  curlget(api_url().'productlisting/update_catdetails/userid/'.$this->session->userdata('bizzgainmember')->id.'/prodmap_id/'.$Productid.'/qty/'.$_POST['qtyval'].'/format/json/');
   
   $newqty   =explode('to', $response[0]->qty);
   $newqty2   =explode('to', $response[0]->qty1);
   $newqty3   =explode('to', $response[0]->qty2);

 if($_POST['qtyval']<$newqty[0] ){
    $newfinalPrice = '';
    $newfinalPrice=citybaseprice($response[0]->comboprice,$response[0]->cityvalue);
} 
 if($_POST['qtyval']>=$newqty[0] && $_POST['qtyval']<=$newqty[1] ){
    $newfinalPrice = '';
    $newfinalPrice=cityqtyprice($response[0]->comboprice,$response[0]->cityvalue,$response[0]->price);
}
 if($_POST['qtyval']>=$newqty2[0] && $_POST['qtyval']<=$newqty2[1] ){
    $newfinalPrice = '';
    $newfinalPrice=cityqtyprice($response[0]->comboprice,$response[0]->cityvalue,$response[0]->price1);
}

 if($_POST['qtyval']>=$newqty3[0] && $_POST['qtyval']<='100000' ){
    $newfinalPrice = '';
    $newfinalPrice=cityqtyprice($response[0]->comboprice,$response[0]->cityvalue,$response[0]->price2);
}
    
    /*$data['cart'] = array(
                   'id'      => $response->promapid,
                   'qty'     => $_POST['qtyval'],
                   'price'   => $newfinalPrice,
                   'name'    => $response->ProName,
                   'VendorID' => $response->manufacturerid,
                   'vendorName'=>$response->CompanyName,
                   'addiscount'=>$response->AdditionDiscount,
                   'options' => array('sizeattr' =>$sizeattr,'color'=>$coloridd),
                   'img'     => proImg_url($getImg['0']),
                   'brandid'  =>$response->brandId,
                   'catid'    =>$response->catid,
                   'prodiscount'=>$response->ProDiscount,
                   'proid'    => $response->proid,
                   'product_type' => 'single',
                   'proqty1'      =>$response->qty1,
                   'proqty2'      =>$response->qty2,
                   'proqty3'      =>$response->qty3,
                   'price1'       =>$response->price,
                   'price2'       =>$response->price2,
                   'price3'       =>$response->price3,
                   'sku'          =>$response->sellersku,
                   'cityval'      =>$response->cityvalue,
                   'baseprice'    =>$response->FinalPrice,
                   'mrpprice'     =>$response->productMRP

         );*/
   

   /*$data['combo'] = array(

                   'id'      => $Productid,
                   'qty'     => $_POST['qtyval'],
                   'price'   => $response[0]->comboprice,
                   'name'    => $response[0]->name,
                   'addiscount'=>$response[0]->p_disPer,
                   'img'     => base_url().'assets/comboimage/'.$response[0]->combo_img,
                   'catid'    =>$response[0]->cat_id,
                  'proid'    => $response[0]->ComboId,
                   'startdate'      =>$response[0]->start_date,
                   'enddate'      =>$response[0]->end_date,
                   'ingrients'      =>$response[0]->ingridients,
                   'about'       =>$response[0]->about,
                   'totalqty'       =>$response[0]->totalqty,
                   'total_sellingprice'       =>$response[0]->total_sellingprice,
                   'p_totalsaving'          =>$response[0]->p_totalsaving,
                   'product_qty'      =>$response[0]->product_qty,
                   'pro_seling'    =>$response[0]->pro_seling,
                   'pro_combo_price'     =>$response[0]->pro_combo_price,
                    'proname'     =>$response[0]->proname,
                    'proqty1'      =>$response[0]->qty,
                   'proqty2'      =>$response[0]->qty1,
                   'proqty3'      =>$response[0]->qty2,
                   'price1'       =>$response[0]->price,
                   'price2'       =>$response[0]->price2,
                   'price3'       =>$response[0]->price3,
                   'cityval'      => $response[0]->cityvalue,
                   'baseprice'    =>$response[0]->pro_combo_price,
                   'mrpprice'     =>100,
                   'new_extrafild'    =>  '',
                   
         
         );*/

    $data['combo'] = array(
                   'id'      => $Productid,
                   'qty'     => $_POST['qtyval'],
                   'price'   => $newfinalPrice,
                   'name'    => $response[0]->name,
                   'VendorID' => '',
                   'vendorName'=>'admin',
                   'addiscount'=>$response[0]->p_disPer,
                   'options' => '',
                   'img'     => base_url().'assets/comboimage/'.$response[0]->combo_img,
                   'brandid'  =>$response[0]->brand_id,
                   //'catid'    =>$response[0]->cat_id,
                   'catid'    =>$combomulcat,
                   'prodiscount'=>'',
                   'proid'    => $response[0]->ComboId,
                   'product_type' => 'combo',
                   'proqty1'      =>$response[0]->qty,
                   'proqty2'      =>$response[0]->qty1,
                   'proqty3'      =>$response[0]->qty2,
                   'price1'       =>$response[0]->price,
                   'price2'       =>$response[0]->price1,
                   'price3'       =>$response[0]->price2,
                   'sku'          =>'',
                   'cityval'      =>$response[0]->cityvalue,
                   'baseprice'    =>$response[0]->comboprice,
                   'mrpprice'     =>$response[0]->total_sellingprice,
                   'startdate'      =>$response[0]->start_date,
                   'enddate'      =>$response[0]->end_date,
                   'ingrients'      =>$response[0]->ingridients,
                   'about'       =>$response[0]->about,
                   'totalqty'       =>$response[0]->totalqty,
                   'total_sellingprice'       =>$response[0]->total_sellingprice,
                   'p_totalsaving'          =>$response[0]->p_totalsaving,
                   'product_qty'      =>$response[0]->product_qty,
                   'pro_seling'    =>$response[0]->pro_seling,
                   'pro_combo_price'     =>$response[0]->pro_combo_price,
                    'proname'     =>$response[0]->proname,

         );
   //p($data['combo']); exit;
  



   /*foreach ($response as $key => $value) {
 
    $newarray[] =  $value->product_id.'/'.$value->pro_combo_price."<br>";
    
    $p_cityid=$this->session->userdata('logincity')->cmid;
    $path = api_url().'productlisting/proAvailCheck/name/'.$value->product_id.'/cityidd/'.$p_cityid.'/format/json/';
    $comboproduct[] = curlget( $path);

}*/
/*foreach ($comboproduct as $key => $value) {


     
 $getImg = getImgProdetail($value->image);
 
   
  $data['combopro'] = array(

                   'id'      => $value->promapid,
                   'qty'     => $_POST['qtyval'],
                   'price'   => $value->FinalPrice,
                   'name'    => $value->ProName,
                   'VendorID' => $value->manufacturerid,
                   'vendorName'=>$value->CompanyName,
                   'addiscount'=>$value->AdditionDiscount,
                   'options' => array('sizeattr' =>$sizeattr,'color'=>$coloridd),
                   'img'     => proImg_url($getImg['0']),
                   'brandid'  =>$value->brandId,
                   'catid'    =>$value->catid,
                   'prodiscount'=>$value->ProDiscount,
                   'proid'    => $value->proid,
                   'product_type' => 'single',
                   'proqty1'      =>$value->qty1,
                   'proqty2'      =>$value->qty2,
                   'proqty3'      =>$value->qty3,
                   'price1'       =>$value->price,
                   'price2'       =>$value->price2,
                   'price3'       =>$value->price3,
                   'sku'          =>$value->sellersku,
                   'cityval'      =>$value->cityvalue,
                   'baseprice'    =>$value->FinalPrice,
                   'mrpprice'     =>$value->productMRP,
                   'extrafild'    =>  ''

         );

              
  
}*/





  $this->cart->insert($data);

  $param=array('userid'  =>$this->session->userdata('bizzgainmember')->id,'prodmap_id'=>$Productid,'qty'=>$_POST['qtyval'],
    'size'=>'combo','color'=>'');
    $path = api_url().'productlisting/add_catdetails/format/json/';
    $dataqty=curlpost($param,$path);
    
    $data['total']       = $this->cart->total();
    $data['TotalInCart'] =  count($this->cart->contents()); 
    $data['contentss'] = $this->cart->contents();
    //p($data);
    //echo json_encode($data); 
   echo json_encode($data); 

}

}
?>