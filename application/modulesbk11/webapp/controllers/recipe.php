<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Recipe extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
  }

  public function recipelist(){

    $param=array('act_mode' => 'front_get_recipe_list',
                     'param2' => '',
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '');
    $record['all_recipe'] = $this->supper_admin->call_procedure('proc_recipe',$param);
    //p($record['all_recipe']);exit();
    $this->load->view("helper/header");
    $this->load->view("recipe/recipelist",$record);
    $this->load->view("helper/footer");
  }

  public function recipedetail($rowid){
    
    $id=decrypt($rowid);
    $param=array('act_mode' => 'front_recipe_data',
                     'param2' => $id,
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '');
    $record['recipe_detail'] = $this->supper_admin->call_procedureRow('proc_recipe',$param);

    $params=array('act_mode' => 'front_recipe_steps',
                     'param2' => $id,
                     'param3' => '',
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '');
    $record['recipe_steps'] = $this->supper_admin->call_procedure('proc_recipe',$params);

    $paramp=array('act_mode' => 'front_recipe_product',
                     'param2' => $id,
                     'param3' => $this->session->userdata('logincity')->cmid,
                     'param4' => '',
                     'param5' => '',
                     'param6' => '',
                     'param7' => '',
                     'param8' => '',
                     'param9' => '',
                     'param10' => '');
    $record['recipe_product'] = $this->supper_admin->call_procedure('proc_recipe',$paramp);
    
    $this->load->view("helper/header");
    $this->load->view("recipe/recipedetail",$record);
    $this->load->view("helper/footer");
  }

}//end of class
?>