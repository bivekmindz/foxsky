<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class RegisterLogin extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
   // $this->load->model("registerlogin");
    $this->load->library('upload');
    $this->load->library('email');

  }
  public function register(){

    if($this->session->userdata('bizzgainmember')->id){
      redirect(base_url()."home");
    }
    
   // require(base_url().'recaptcha-master/src/autoload.php'); 
   $path = api_url().'userapi/shipstate/format/json/';
   $record['viewshipstate']  = curlget($path);
   //pend($record['viewshipstate']);

//     $recaptchaSecret = '6LdgElYUAAAAAMiD6cTh_QHINwHWTzt0AqsM1ObT
// ';
// $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret);
  


   $this->load->view("helper/head");
   $this->load->view("register",$record);
   $this->load->view("helper/foot");
  

  }

  public function forgotpwd(){
  $email=$this->input->post('forgetemail');
    if($this->input->post('submit')=='Submit'){
    $parameter=array('act_mode' =>'check_email_id_exit' ,
                    'param1'=>$email,
                    'param2'=>'',
                    'param3'=>'',
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');
    
    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    // p($response);
    // exit();
    if($response->c>0)
    {

      $num = rand(1111, 9999);
      $parameter=array('act_mode' =>'pass' ,
                    'param1'=>$email,
                    'param2'=>base64_encode($num),
                    'param3'=>'',
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');
      
    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    // p($parameter);
    // p($response);


    $this->email->from('ankit@mindztechnology.com');
    $this->email->to($email);
    $this->email->set_mailtype("html");
    //$this->email->cc('student@guruq.in'); 
    $this->email->subject('Forgot Password mail');
    #$link = $this->load->view('link',$email,true);
    
    $this->email->message($num);  
    $this->email->send();

    $this->session->set_flashdata('success', 'Mail send to your mail id');
    redirect('webapp/registerlogin');


    
    }
    else{

     $this->session->set_flashdata('fail', 'Mail id is not registered');
    redirect('webapp/registerlogin');
    }
    }

    $this->load->view("forgotpwd");
  
  }

  public function checkemail()
  {
    $email=$this->input->post('email');
   // echo $email;die;
    $parameter=array('act_mode' =>'check_email_id_exit' ,
                      'param1'=>$email,
                      'param2'=>'',
                      'param3'=>'',
                      'param4'=>'',
                      'param5'=>'',
                      'param6'=>'',
                      'param7'=>'',
                      'param8'=>'',
                      'param9'=>'',
                      'param10'=>'' );
    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    // echo $response->c;
    if($response->c>0)
    {
      $result=1;
    }
    else
    {
      $result=0;
    }

  echo $result;
    //$result=$this->rating_model->check_user($email);
    //echo $result;




  }

  public function saveuser()
  {
    $email=$this->input->post('email');
    // $email='pavan@gmail.com';
    $pass=$this->input->post('pass');
    $city=$this->input->post('city');

    // $email=$this->input->post('email');
   // echo $email;die;
    $parameter=array('act_mode' =>'check_email_id_exit' ,
                      'param1'=>$email,
                      'param2'=>'',
                      'param3'=>'',
                      'param4'=>'',
                      'param5'=>'',
                      'param6'=>'',
                      'param7'=>'',
                      'param8'=>'',
                      'param9'=>'',
                      'param10'=>'' );
 
    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    // echo $response->c;
    if($response->c>0)
    {
      echo "Email id already exists";
    }
    else
    {
      
     $parameter=array('act_mode' =>'createuser' ,
                      'param1'=>$email,
                      'param2'=>base64_encode($pass),
                      'param3'=>$city,
                      'param4'=>'',
                      'param5'=>'',
                      'param6'=>'',
                      'param7'=>'',
                      'param8'=>'',
                      'param9'=>'',
                      'param10'=>'');
// p( $parameter);
//     die;
    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    $data['email']=$email;
   
   #echo $link;

    $this->email->from('ankit@mindztechnology.com');
    $this->email->to($email);
    $this->email->set_mailtype("html");
    //$this->email->cc('student@guruq.in'); 
    $this->email->subject('Verification Email');
    #$link = $this->load->view('link',$email,true);
    $link = $this->load->view('link',$data,true);
    $this->email->message($link);  
    $this->email->send();

    echo "success";
  }
}


  public function msaveuser()
  {
    // p($_POST);
    // exit();

 $phone=$this->input->post('phone');
    // p($phone);
    // exit();
    // $email='pavan@gmail.com';
    $pass=$this->input->post('pass');
    $city=$this->input->post('city1');
    $parameter=array('act_mode' =>'checkmobileexit' ,
                      'param1'=>'',
                      'param2'=>$phone,
                      'param3'=>'',
                      'param4'=>'',
                      'param5'=>'',
                      'param6'=>'',
                      'param7'=>'',
                      'param8'=>'',
                      'param9'=>'',
                      'param10'=>'');



    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    
    if($response->cc>0)
    {

      echo "Mobile no. already exists";
    }
    else
    {
    $parameter=array('act_mode' =>'mcreateuser' ,
                      'param1'=>$phone,
                      'param2'=>base64_encode($pass),
                      'param3'=>$city,
                      'param4'=>'',
                      'param5'=>'',
                      'param6'=>'',
                      'param7'=>'',
                      'param8'=>'',
                      'param9'=>'',
                      'param10'=>'');
 // p($parameter);
 //    exit();
    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    // $data['phone']=$phone;
    echo "success";
  }

    

     
    
    
   #echo $link;

    // $this->email->from('ankit@mindztechnology.com');
    // $this->email->to($email);
    // $this->email->set_mailtype("html");
    // //$this->email->cc('student@guruq.in'); 
    // $this->email->subject('Verification Email');
    // #$link = $this->load->view('link',$email,true);
    // $link = $this->load->view('link',$data,true);
    // $this->email->message($link);  
    // $this->email->send();

    // echo $phone;
  }



  public function verification()
  {
     $email=decrypt($this->uri->segment(4));

     $parameter=array('act_mode' =>'email_status' ,
                      'param1'=>'',
                      'param2'=>$email,
                      'param3'=>'',
                      'param4'=>'',
                      'param5'=>'',
                      'param6'=>'',
                      'param7'=>'',
                      'param8'=>'',
                      'param9'=>'',
                      'param10'=>'');

    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    // echo "Succesfully registered.";
    // echo "<script>base_url().href='webapp/successfully.php';</script>";
    $this->load->view("successfully");
  }

 

  public function checkmobile()
  {
    $city=$this->input->post('city');
    $mobile=$this->input->post('phone');
    
    $parameter=array('act_mode' =>'checkmobileexit' ,
                      'param1'=>'',
                      'param2'=>$mobile,
                      'param3'=>'',
                      'param4'=>'',
                      'param5'=>'',
                      'param6'=>'',
                      'param7'=>'',
                      'param8'=>'',
                      'param9'=>'',
                      'param10'=>'');



    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    
    if($response->cc>0)
    {
      $result=1;
    }
    else
    {
    $result=0;    
  }

    echo $result;
  }

 


   public function verifyotp(){

    $smsphnon = $this->input->post('umob');
  
    $num = rand(1111, 9999);
   
      $smsmsgg = urlencode('Hi User, your OTP Password is '.$num.' for Mobile Number Verification.'); 
      #$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');
      echo $num;
    }

}
