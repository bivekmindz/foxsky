<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
  }

  public function index(){
  	
    
  }

  public function catwrite1()
  {
    $xmlpath = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
    $file =$xmlpath.'/category11.xml';
    $parameter = array( 'act_mode'     => 1 );
  $response= $this->supper_admin->call_procedure('proc_menu',$parameter);
  //pend($response);
  $filedata='<?xml version="1.0" encoding="UTF-8"?>';
  $filedata.='<mainroot>';
  foreach ($response as $key => $value) {
    $filedata.='<root>';
    $filedata.='<categoryname>'.str_replace("&","~",$value->catname).'</categoryname>';
    $filedata.='<catid>'.$value->catid.'</catid>';
    $filedata.='<productname>'.$value->ProductName.'<productname>';
    $filedata.='<productid>'.$value->ProId.'</productid>';
     $filedata.='<productprice>'.$value->productMRP.'</productprice>';
     $filedata.='<image>'.$value->Image.'</image>';
     $filedata.='</root>';
  }
    $filedata.='</mainroot>';
//p($filedata);exit;
 if(file_put_contents($file,$filedata)){
  echo 'success';
  }else{
  echo 'fail';
}



  }

  public function catwrite(){
  $xmlpath = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

  $file =$xmlpath.'/category.xml';
  $parameter = array( 'act_mode'     => 1 );
  $response= $this->supper_admin->call_procedure('proc_menu',$parameter);
  //p($response);exit;
  $filedata='<?xml version="1.0" encoding="UTF-8"?>';
  $filedata.='<mainroot>';
  foreach ($response as $key => $value) {
              $filedata.='<root>';
              $filedata.='<ParentName>'.str_replace("&","~",$value->parentname).'</ParentName>';
              $filedata.='<catparentid>'.$value->parentid.'</catparentid>';
              $filedata.='<CatLogo>'.$value->catlogo.'</CatLogo>';
              $filedata.='<CatImageFirst>'.$value->imgfirst.'</CatImageFirst>';
              $filedata.='<CatImageSecond>'.$value->imgsecond.'</CatImageSecond>';
              $filedata.='<CatImageThird>'.$value->imgthird.'</CatImageThird>';
              $filedata.='<CatImagealtname>'.str_replace("&","~",$value->imgaltname).'</CatImagealtname>';
              $filedata.='<ChildName>'.str_replace("&","~",$value->catname).'</ChildName>';
              $filedata.='<childid>'.$value->childid.'</childid>';
              $filedata.='<catlevel>'.$value->catlevel.'</catlevel>';
              $filedata.='<products>'.$value->products.'</products>';
              $filedata.='<url>'.str_replace("&","~",$value->url).'</url>';
              $filedata.='</root>';
  }
  
  $filedata.='</mainroot>';
//p($filedata);exit;
 if(file_put_contents($file,$filedata)){
  echo 'success';
  }else{
  echo 'fail';
}
}

public function forgate_passwordEmail()
{
 $parameter=array('act_mode'=>'mememailcheck','row_id'=>'','vusername'=>$_POST['email'],'vpassword'=>'','vstatus'=>'');
 $path=api_url().'userapi/chkuniquemail/format/json/';
 $response=curlpost($parameter,$path);
echo json_encode($response);
}

public function forgate_passwordmob()
{
 $parameter=array('act_mode'=>'memmobcheck','row_id'=>'','vusername'=>$_POST['chmobile'],'vpassword'=>'','vstatus'=>'');
 $path=api_url().'userapi/chkuniquemail/format/json/';
 $response=curlpost($parameter,$path);
echo json_encode($response);
}

public function forgate_passwordEmail_send(){
$user_mail=$this->input->post('email'); 
$mem_type =$this->input->post('memtype'); 
//p($user_mail);
//p($mem_type);exit;
 function rand_string( $length ) {
  $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  return substr(str_shuffle($chars),0,$length);
 }
$password= rand_string(6);
$parameter=array('act_mode'=>'forgetpassmem','row_id'=>'','vusername'=>$user_mail,'vpassword'=>$password,'vstatus'=>$mem_type);
//p($parameter);exit;
$path_st = api_url().'userapi/forgatepassword/format/json/';
$userinfo['userdet']=curlpost($parameter,$path_st);
$userinfo['userpass']=$password;
//p($userinfo['userdet']);exit;
if($userinfo['userdet']->scalar=="Something Went Wrong"){
  echo "fail";
} else {
$hmtopemail="support@mindzshop.com";
$msg = $this->load->view('bizzgainmailer/forgotpassword',$userinfo, true);
$this->load->library('email');
$this->email->from($hmtopemail, 'MindzShop.com');
$this->email->to($user_mail); 
$this->email->subject('Forgot Password - ('.$user_mail.')');
$this->email->message($msg);
//$this->email->send();
//echo $this->email->print_debugger();exit;
//print $this->email->print_debugger(); exit;
if($this->email->send())
{
  echo "success";
}

}// end if

}// end function


public function forgate_passwordmobile_send(){
$user_mob=$this->input->post('chmobile'); 
$mem_type =$this->input->post('memtype'); 
 function rand_string( $length ) {
  $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  return substr(str_shuffle($chars),0,$length);
 }
$password= rand_string(6);
$parameter=array('act_mode'=>'forgetpassmemmob','row_id'=>'','vusername'=>$user_mob,'vpassword'=>$password,'vstatus'=>$mem_type);
$path_st = api_url().'userapi/forgatepassword/format/json/';
$userinfo['userdet']=curlpost($parameter,$path_st);
if($userinfo['userdet']->scalar=="Something Went Wrong"){
  echo "fail";
} else {
$smsname = $userinfo['userdet']->firstname.' '.$userinfo['userdet']->lastname;
$smsmsgg = urlencode('Hi '.$smsname.' your new password is '.$password.'.');

//$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$user_mob.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');
  echo "success";

}// end if

}// end function

public function getlocationcity(){

    $cityid=$_POST['cityid'];
    $stateid=$_POST['stateid'];
    sess_city($cityid,$stateid);
    
    
  }

public function test()
{
  $userinfo='';
  $this->load->view('bizzgainmailer/forgotpassword');
}

}//end of class
?>