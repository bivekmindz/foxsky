<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Review extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
  }

  public function index() {
    $url = array_pop($this->uri->segments);
    //  pend($url);
    if($url!="ajaxprolist"){
      $category  = explode("-",$url);
      $cid       = array_pop($category);
      if($cid<=0){
      // redirect();
      }
    }
   // p($this->session->userdata('foxsky'));die;//$this->session->set_userdata('foxsky');
    $user = ($this->session->userdata('foxsky'));
    $email = $user->emailid;
    $id = $user->id;


 $data['urlname'] =$url;
    $parameter=array('act_mode'=>'get_product_tab',
                    'row_id'=>$cid,
                    'catid'=>'');
     
    $data['producttab'] =  $this->supper_admin->call_procedurerow('proc_product',$parameter); 

    $array = array('act_mode' =>'checkuser' ,
                    'row_id'=>$cid ,
                    'param3'=>$id,
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');
    $data['ucount']=$this->supper_admin->call_procedure('proc_ten',$array);

    $array = array('act_mode' =>'getrating' ,
                    'row_id'=>$cid ,
                    'param3'=>'',
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');
    $data['rating']=$this->supper_admin->call_procedure('proc_ten',$array);
    // Get Count //
    $array = array('act_mode' =>'count',
                    'row_id'=>$cid ,
                    'param3'=>'',
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');
    $data['count']=$this->supper_admin->call_procedure('proc_ten',$array);

    $array = array('act_mode' =>'getproductreview' ,
                    'row_id'=>$cid ,
                    'param3'=>'',
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');
    $data['response']=$this->supper_admin->call_procedure('proc_ten',$array);



    $array = array('act_mode' =>'countcomments',
                    'row_id'=>$cid ,
                    'param3'=>$id,
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>''); 
    $data['countcomments']=$this->supper_admin->call_procedure('proc_ten',$array);
    //p($data['countcomments']);
// get total like 
    $array = array('act_mode' =>'likecomments',
                    'row_id'=>$cid ,
                    'param3'=>$id,
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>''); //p($array);
    $data['like']=$this->supper_admin->call_procedure('proc_ten',$array);
 
 // get Total Comment
    $array = array('act_mode' =>'getccomments',
                    'row_id'=>$cid ,
                    'param3'=>$id,
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>''); //p($array);
    $data['getccomments']=$this->supper_admin->call_procedure('proc_ten',$array);
   // p($data['like']);
$data['urldata']=$url;
    $this->load->view("helper/head");
    $this->load->view("helper/header");
    $this->load->view("review",$data);
    $this->load->view("helper/footer");
    $this->load->view("helper/foot");

  }

  public function likes() {

    $user = ($this->session->userdata('foxsky'));
    $email = $user->emailid;
    $id = $user->id;
 if($url!="ajaxprolist"){
      $category  = explode("-",$url);
      $cid       = array_pop($category);
      if($cid<=0){
      // redirect();
      }
    }
    if($id!='')
    {
      $ReviewID = $_POST["keyword"];

      $array = array('act_mode' =>'addlikes' ,
                      'row_id'=>$ReviewID,
                      'param3'=>'',
                      'param4'=>'',
                      'param5'=>'',
                      'param6'=>'',
                      'param7'=>'',
                      'param8'=>'',
                      'param9'=>'',
                      'param10'=>'');
        $data['like']=$this->supper_admin->call_procedure('proc_ten',$array);
    }

  }


    public function addcomments() {

     $user = ($this->session->userdata('foxsky'));
     $email = $user->emailid;
      $url = $_POST['urldata'];
    //  pend($url);
    if($url!="ajaxprolist"){
      $category  = explode("-",$url);
      $cid       = array_pop($category);
      if($cid<=0){
      // redirect();
      }
    }
     $id = $user->id; 
     if($id!=''){
       $user = $this->session->userdata('foxsky');
       $ReviewID = $this->input->get_post("rid");  
       $comments = $this->input->get_post("comments");  

       $array = array('act_mode' =>'addcomments',
                    'row_id'=>$ReviewID,
                    'param3'=>$comments,
                    'param4'=>$id,
                    'param5'=>$cid ,
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');
      // p($array);

        $data['insert']=$this->supper_admin->call_procedure('proc_ten',$array);
        $this->session->set_flashdata('success', 'Your comments submmited successfully');
         redirect('review/'.$_POST['urldata']);
      }
      else{
        $this->session->set_flashdata('success', 'Please Login your Account');
         redirect('review/'.$_POST['urldata']);
  }
}



  public function addreview() {


 

     $user = ($this->session->userdata('foxsky'));
     $email = $user->emailid;
     $id = $user->id; 
    $url = $_POST['urldata'];
    //  pend($url);
    if($url!="ajaxprolist"){
      $category  = explode("-",$url);
      $cid       = array_pop($category);
      if($cid<=0){
      // redirect();
      }
    }
   // echo $cid; exit;

  

     if($_POST['review']!=''){
       $user = $this->session->userdata('foxsky');
       $ReviewID = $this->input->get_post("rid");  
       $Ordid = $this->input->get_post("oid"); 
       $comments = $this->input->get_post("comments");  


       $array = array('act_mode' =>'addreview',
                    'row_id'=>$cid ,
                    'param3'=>$id,
                    'param4'=>$_POST['tag'],
                    'param5'=>$_POST['star'],
                    'param6'=>$_POST['review'],
                    'param7'=>$_POST['oid'],
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');
        $data['insert']=$this->supper_admin->call_procedure('proc_ten',$array);
        $this->session->set_flashdata('success', 'Your post submmited successfully');
        redirect('review/'.$_POST['urldata']);
      }
      else{
        $this->session->set_flashdata('success', 'Please Login your Account');
        redirect('review/'.$_POST['urldata']);
      }
  }

 
}
?>