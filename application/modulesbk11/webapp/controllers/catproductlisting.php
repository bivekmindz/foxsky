<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Catproductlisting extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
    //$this->userfunction->UserValidation();
  }

 public function productlisting(){

$url = array_pop($this->uri->segments);
if($url!="ajaxprolist"){
  $category  = explode("-",$url);
  $oldcid       = array_pop($category);
  $catfilter = maincatList2((int)$oldcid);

 /* SHEETEHS CODING STRAT FROm HERE */
     $paraSeo=array('act_mode' => 'cat','p_catid' => $oldcid,'p_extra' => 'extra' );
    // p($datas[$paraSeo]);
    $datas['myseotags']=$this->supper_admin->call_procedureRow('proc_getseo',$paraSeo);
   /* SHEETEHS CODING STRAT FROm HERE */

   
 // p($datas['myseotags']); die();
 if($oldcid==12){ // ---- soft toys category page seo ----
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>11);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }
  if($oldcid==15){ // ----- board and puzzle category page seo ------
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>12);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }
  if($oldcid==14){ // ----- educational category page seo ------
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>13);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }
  if($oldcid==13){ // ----- baby toddler category page seo ------
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>14);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }
  if($oldcid==19){ // ----- indoor category page seo ------
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>15);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }
  if($oldcid==20){ // ----- outdoor category page seo ------
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>16);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }
  if($oldcid==18){ // ----- kid room decor category page seo ------
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>17);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }
  if($oldcid==4){ // ----- electronic category page seo ------
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>18);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }
  if($oldcid==16){ // ----- activity category page seo ------
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>19);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }
  if($oldcid==17){ // ----- doll houses category page seo ------
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>20);
    $datas['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
  }

  foreach ($catfilter as $key => $value) {
    //p($value);
    $valuenew = (object) $value;
    $pcatcid[]=$valuenew->childid;
  }
  $cid=implode(',',$pcatcid);
  if($cid<=0){
    redirect();
  }
}
if($this->input->get('show_brand')!=''){
  $brand      = $this->input->get('show_brand');
  $brandurl   = '&show_brand='.$brand;
}
else{
   $brand     = 0;
   $brandurl  = null;
}
if($this->input->get('price')!=''){
  $price       = explode('-',$this->input->get('price'));
  $minprice    = $price[0];
  $maxprice    = $price[1];
  $priceurl    = '&price='.$this->input->get('price');
}
else{
  $minprice    = 0;
  $maxprice    = 0;
  $priceurl    = null; 
}
if($this->input->get('discount')!=''){
  $discount    = $this->input->get('discount');
  $discounturl = '&discount='.$discount;
}
else{
  $discount    = 0;
  $discounturl = null;
}/*
if($this->input->get('color')!=''){
  $color    = $this->input->get('color');
  $colorurl = '&color='.$color;
}
else{
  $color    = 0;
  $colorurl = null;
}
if($this->input->get('size')!=''){
  $size    = $this->input->get('size');
  $sizeurl = '&size='.$size;
}
else{
  $size    = 0;
  $sizeurl = null;
}*/

if($this->input->get('weight')!=''){
  $weight    = $this->input->get('weight');
  $weighturl = '&weight='.$weight;
}
else{
  $weight    = 0;
  $weighturl = null;
}
if($this->input->get('food')!=''){
  $food    = $this->input->get('food');
  $foodurl = '&food='.$food;
}
else{
  $food    = 0;
  $foodurl = null;
}

if($this->input->get('feature')){
  $name           = $this->input->get('feature');
  $name2          = str_replace("%20"," ", $name);
  $feature3       = str_replace(",","|",$name2);
  $feature        = strtolower($feature3);
  $featureurl     = '&feature='.$name;
}
else{
  $feature        = 0;
  $featureurl     = null;
}

if($this->input->get('feature')){
  $feature        = $this->input->get('feature');
  $featureurl     = '&feature='.$feature;
 }
else{
  $feature        = "0";
  $featureurl     = null;
 }
if($this->input->get('porder')!=''){
  $pageorder      = $this->input->get('porder');
  $porderulr      = '&porder='.$pageorder;
}
else{
   $pageorder     = 0;
   $porderulr     = null;
}
$totallistoffset   = $_POST['offset'];

if($totallistoffset == 0){
  $totallistoffset = 12;
}else{
  $totallistoffset  = $_POST['offset'];
  #$totallistoffset = $_POST['offset']+12;
}
$parameter           = array('act_mode'=>'discount','','');
$data['fildiscount'] = $this->supper_admin->call_procedure('proc_productmasterdata',$parameter);

$parameter            = array('act_mode'=>'catatt','row_id'=>'','catid'=>$cid);
//p($parameter);//exit;
$data['coloratt']     = $this->supper_admin->call_procedure('proc_product',$parameter);
//p($data['coloratt']);//exit;
$excelHeadArr         = array();
$excelHeadArrcvc      = array();
$arrPart1             = array('Feature1','Feature2');
$allArr               = array();
$templevel            = 0;  
$newkey               = 0;
$grouparr[$templevel] = "";

foreach($data['coloratt'] as $k=>$v){
  $excelHeadArr[]    = $v->attname;
  $excelHeadArrcvc[] = $v->id;
  $allArr[]          = $v->attname;
  $grouparr[$k]      = $v->id;
}
foreach ($excelHeadArrcvc as $key => $value) {
  $parameter = array('act_mode'=>'pcatattvalue','row_id'=>$value,'pvalue'=>$cid);
  //p($parameter);
  $data['attData'][$grouparr[$key]] =$this->supper_admin->call_procedure('proc_productmasterdata',$parameter);
  //p($data['attData'][$grouparr[$key]]);
  $j         = 0;
}
//exit;
$c = 0;
foreach($data['attData'] as $k=>$v){ 
 foreach($v as $kk=>$vv){
 if($k == $vv->attid){
  $mainArr[$allArr[$c]][$vv->attmapid] = $vv->attvalue;}
}
  $c++;
}
$data['attrMapData'] = $mainArr;
//p($data['attrMapData']);exit;
$parameter      = array(
  'proName'     => 0,
  'category_id' => $cid,
  'barand_id'   => $brand,
  'size'        => $weight,#$size,
  'color'       => $food,#$color,
  'minprice'    => $minprice,
  'maxprice'    => $maxprice,
  'mindiscount' => $discount,
  'pageorder'   => $pageorder,
  'pid'         => 0,
  'perpage'     => 6,
  'features'    => $feature,
  'procityidd'  => $this->session->userdata('logincity')->cmid);
  //p($parameter);exit();
  
  $path                = api_url().'productlisting/prolist/format/json/';
  $data['resultspage'] = curlpost($parameter,$path);
  //p($data['resultspage']);exit;
  $this->load->view("helper/header",$datas);
 /* $this->load->view("helper/nav1");
  $this->load->view("helper/nav2");*/
  $this->load->view("productlist/catlisting",$data);
  $this->load->view("helper/footer");

}
//----------  End Product Listing ----------- //
  
function ajaxprolist(){
$url = array_pop($this->uri->segments);
//echo $url; 
//print $url; exit;
if($url!="ajaxprolist"){
 $category  = explode("-",$url);
 //$cid       = array_pop($category);
 $oldcid       = array_pop($category);
  $catfilter = maincatList2((int)$oldcid);
  foreach ($catfilter as $key => $value) {
    //p($value);
    $valuenew = (object) $value;
    $pcatcid[]=$valuenew->childid;
  }
  $cid=implode(',',$pcatcid);
 if($cid<=0){
  redirect();
}  

if($this->input->get('show_brand')!=''){
  $brand=$this->input->get('show_brand');
  $brandurl='&show_brand='.$brand;
}
else{
  $brand=0;
  $brandurl=null;
}
if($this->input->get('price')!=''){
  $price=explode('-',$this->input->get('price'));
  $minprice=$price[0];
  $maxprice=$price[1];
  $priceurl='&price='.$this->input->get('price');
}
else{
  $minprice=0;
  $maxprice=0;
  $priceurl=null; 
}
if($this->input->get('discount')!=''){
  $discount=$this->input->get('discount');
  $discounturl='&discount='.$discount;
}
else{
  $discount=0;
  $discounturl=null;
}
/*if($this->input->get('color')!=''){
  $color=$this->input->get('color');
  $colorurl='&color='.$color;
}
else{
  $color=0;
  $colorurl=null;
}
if($this->input->get('size')!=''){
  $size=$this->input->get('size');
  $sizeurl='&size='.$size;
}
else{
  $size=0;
  $sizeurl=null;
}*/

if($this->input->get('weight')!=''){
  $weight=$this->input->get('weight');
  $weighturl='&weight='.$weight;
}
else{
  $weight=0;
  $weighturl=null;
}
if($this->input->get('food')!=''){
  $food=$this->input->get('food');
  $foodurl='&food='.$food;
}
else{
  $food=0;
  $foodurl=null;
}

if($this->input->get('feature')){
  $name=$this->input->get('feature');
  $name2 = str_replace("%20"," ", $name);
  $feature3=str_replace(",","|",$name2);
  $feature =strtolower($feature3);
  $featureurl='&feature='.$name;
}
else{
  $feature    = 0;
  $featureurl = null;
}
if($this->input->get('feature')){
  $feature    = $this->input->get('feature');
  $featureurl = '&feature='.$feature;
}
else{
  $feature    = "0";
  $featureurl = null;
}
if($this->input->get('porder')!=''){
  $pageorder = $this->input->get('porder');
  $porderulr = '&porder='.$pageorder;
}
else{
  $pageorder = 0;
  $porderulr = null;
}

$totallistoffset = $_POST['offset'];

if($totallistoffset == 0){
  $totallistoffset = 0;
}else{
  $totallistoffset = $_POST['offset'];
}
$parameterp = array(
          'proName'     => 0,
          'category_id' => $cid,
          'barand_id'   => $brand,
          'size'        => $weight,#$size,
          'color'       => $food,#$color,
          'minprice'    => $minprice,
          'maxprice'    => $maxprice,
          'mindiscount' => $discount,
          'pageorder'   => $pageorder,
          'pid'         => $totallistoffset,
          //'perpage'     => $_POST['number'],
          'perpage'     => 6,
          'features'    => $feature,
          'procityidd'  => $this->session->userdata('logincity')->cmid
          );

 $path                = api_url().'productlisting/prolist/format/json/';

$data['resultspage'] = curlpost($parameterp,$path);
  $data['view_cls']=$_POST['seetyp'];
  #p($data['resultspage']);exit;

  #print 'count'.count($data['resultspage']);exit;
  //$this->load->view("productlist/listing",$data);
  $this->load->view('productlist/ajaxcatproductlisting', $data);

  }
}

//----------  End Ajax Product Listing ----------- //
 
public function weightchnage()
{

       $parameter = array('act_mode' => 'get_product_by_weight',
                         'row_id' => $_POST['weight_proid'],
                         'cat_id' => $this->session->userdata('logincity')->cmid,
                         'name' => '',
                         'sec_order' => '',
                         'media_type' => '',
                         'img_name' => '',
                         'img_url' => '',
                         'img_head' => '',
                         'imgshead' => '');
                         //p($parameter);
      $data['prolist'] = $this->supper_admin->call_procedureRow('proc_sectiondetails',$parameter); 
      $data['autoid'] = $_POST['pro_replaceid'];
      //p($data['get_sec_pro']);
      echo $view = $this->load->view('weightbyproduct',$data,TRUE);
  

}



}
?>