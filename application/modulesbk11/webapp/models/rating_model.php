<?php
class Rating_Model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}
	public function exist_id($id,$uid){
        $query=$this->db->query("SELECT ProId FROM tblproductmap where prodtmappid=".$id."");
         $data = $query->result();
        $pro_id  = $data['0']->ProId;
		$query = $this->db->query("SELECT * FROM tblrating where ProId=$pro_id and UserId=$uid");
		$close= $this->db->close();   
        return $query->num_rows();	
        
	}


    public function check_user($email)
    {
        $query=$this->db->query("SELECT count(*) FROM tbl_manufacturermaster where compemailid=".$email);
        $data = $query->result(); 
        $close= $this->db->close(); 
        return $data;

    }

	public function insert_id($id,$uid){
         $query=$this->db->query("SELECT ProId FROM tblproductmap where prodtmappid=".$id."");
           $data = $query->result();
         $pro_id  = $data['0']->ProId;
 		$sql = "INSERT INTO tblrating (ProId,UserId) VALUES ('".$pro_id."','".$uid."')"; 
		$str = $this->db->query($sql);
        $close = $this->db->close();
    }
    public function exist_all($id,$uid){

         $query=$this->db->query("SELECT ProId FROM tblproductmap where prodtmappid=".$id."");
           $data = $query->result();
         $pro_id  = $data['0']->ProId;
        $query = $this->db->query("SELECT TotalVotes,TotalValues,UsedIP FROM tblrating where ProId=$pro_id and UserId=$uid");
        $data  = $query->result_array();
        $close = $this->db->close();
        return $data;
    }

    public function get_ip($id,$ip,$uid){


         $query=$this->db->query("SELECT ProId FROM tblproductmap where prodtmappid=".$id."");
           $data = $query->result();
         $pro_id  = $data['0']->ProId;

    	$query = $this->db->query("SELECT UsedIP FROM tblrating where UsedIP LIKE '%".$ip."%' AND ProId=$pro_id AND UserId=$uid");
        $rows = $query->num_rows();
        $close = $this->db->close();
        return $rows;
    }

    public function update_rating($added,$sum,$insertip,$id,$uid){

         $query=$this->db->query("SELECT ProId FROM tblproductmap where prodtmappid=".$id."");
         $data = $query->result();
         $pro_id  = $data['0']->ProId;

    	$data = array(
                'TotalVotes' => $added,
                'TotalValues' => $sum,
                'UsedIp' => $insertip );
        
        $this->db->where(array('ProId'=>$pro_id,'UserId'=>$uid));
        $this->db->update('tblrating', $data);
        $close = $this->db->close();        
       // return $data ;
        //$this->db->where($where);  
    }
    public function lastinsrt_id($proid,$user_id,$headline,$review_des){

        $query = $this->db->query("SELECT ProId FROM tblproductmap where prodtmappid=$proid");
        $data = $query->result();
        $pro_id  = $data['0']->ProId;
        $sql = "INSERT INTO tblreview (prod_id,user_id,headline,review_des,status) VALUES ('".$pro_id."','".$user_id."','".$headline."','".addslashes($review_des)."','I')";
        $str = $this->db->query($sql);
        $query = $this->db->query('SELECT LAST_INSERT_ID() FROM tblreview');
        $row = $query->row_array();
        $LastIdInserted = $row['LAST_INSERT_ID()'];
        $close= $this->db->close(); 
        return $LastIdInserted;
    }
    public function prodrate_info($id,$uid){

        $query=$this->db->query("SELECT ProId FROM tblproductmap where prodtmappid=".$id."");
          $data = $query->result();
        $pro_id  = $data['0']->ProId;
        //echo "SELECT TotalVotes,TotalValues FROM tblrating where ProId=$id and UserId=$uid"; exit;
        $query = $this->db->query("SELECT TotalVotes,TotalValues FROM tblrating where ProId=$pro_id  and UserId=$uid");
        $data  = $query->result_array();
        $close= $this->db->close(); 
        return $data;    
    }
    public function prod_info($proid){
        $query = $this->db->query("SELECT pr.ProName,fn_image(pm.prodtmappid) as image 
         FROM tblproduct as pr, tblproductmap as pm  where pm.prodtmappid=$proid and pr.ProId=pm.ProId");
        $data = $query->result();
        $close= $this->db->close();    
        return $data;   
    }
    public function reviewinfo($proid,$userid){
        $query = $this->db->query("SELECT headline,review_des FROM tblreview where prod_id=$proid and user_id=$userid");
        $data = $query->result_array(); 
        $close= $this->db->close();  
        return $data;  
    }
    public function review_list($userid){
       $query = $this->db->query("SELECT review_des,CreatedOn FROM tblreview where status='A' and user_id=$userid"); 
       $data = $query->result();  
       $close= $this->db->close(); 
       return $data;  
    }
    // search page query
    public function brandsearch($keyword){
        $query = $this->db->query('select pm.BrandID,b.BrandName
                    from tblproduct p 
                    inner join tblproductmap pm on p.ProId=pm.ProId
                    inner join tblbrand as b on pm.BrandID=b.BrandId 
                    where lower(p.ProName) like "%'.$keyword.'%" and fn_image(pm.prodtmappid)!="" and pm.BrandID!=0 group by pm.BrandID');
       
        $data = $query->result(); 
        $close= $this->db->close();  
        return $data; 
    }

}	

?>
