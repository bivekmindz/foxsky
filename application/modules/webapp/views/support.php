<style>
.suport_list {
	margin:0;
	padding:0;
	}
.suport_list li {
	list-style-type:none;
	margin-left:15px;
}
.table_control_support {
    margin-bottom: 0px;
}
</style>
 <div class="wrapper sub_nav_bg">
	<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="sub_nav">
			<div class="pull-right">
            	<ul class="nav_about">
                      <li><a href="<?php echo base_url();?>aboutus">About us</a></li>
                    	<li><a href="#" class="active-orage">Support</a></li>
                        <li><a href="<?php echo base_url();?>useraggree">User Agreement</a></li>
                        <li><a href="<?php echo base_url();?>privacy">Privacy Policy</a></li>
                        <li><a href="<?php echo base_url();?>career">Career</a></li>

                        <li><a href="<?php echo base_url();?>terms">Terms of Use</a></li>
                    </ul>
            	</div>
              </div>
            </div>
        </div>
    </div>
</div>		
        
        
        
      
<div class="wrapper bg_orage">
	<div class="container">
        <div class="about_heading">
        	<h3 class="about_h4">SUPPORT</h3>
        </div>
    </div>
</div>   
        
        

<div class="wrapper parivacy-policy">
	<div class="container">
        <div class="row">
            <div class="xm-section">
                <p><strong>CUSTOMER SERVICE  :</strong></p>
                <ul class="suport_list">
                  <li>1 - CHAT SUPPORT , 9 AM TO 9 PM</li>
                  <li>2 - HOTLINE SERVICE 1800 532 8777</li>
                  <li>3 - SERVICE@FOXSKYINDIA.COM</li>
                  
                  </ul>
				  
				  

<p><strong>SHIPPING FAQ</strong></p>

<p>
<strong>WHICH LOCATIONS DO YOU DELIVER TO?</strong><br />
Please enter your pin-code on the product page (you don't have to enter it every single time) to know whether the product can be delivered to your location.
</p>
               
           
<p><strong>WHAT ARE YOUR DELIVERY TIMES?</strong><br>
Packages are delivered between 09:00 -19:00 from Monday to Saturday. There are no deliveries on Sunday and on public holidays.
</p>


<p><strong>HOW LONG WOULD IT TAKE FOR ME TO RECEIVE MY ORDER?</strong><br>
Users can check Estimated delivery time to their location at the product page.
</b>

<p><strong>HOW IS THE DELIVERY TIME CALCULATED?</strong></p>

<p><em><strong>Estimated delivery time depends on the following factors:</strong></em></p>
<ul class="suport_list">
	<li>1 - Product(s) selected</li>
	<li>2 - The destination pin-code e.g. 110014 to which you want the order to be shipped to</li>
	<li>3 - Seller's location</li>
</ul>
<p>Delivery time is calculated basis historical data of delivery timelines for orders originating from the entered pin-code. Delivery timelines foxsky vary in a few cases depending on the above mentioned factors.</p>

<p>Please note that Foxsky India is not liable for any shipping delays, or loss of any kind resulting from unforeseen and uncontrollable circumstances, such as: recipient was unavailable or un-contactable; incorrectly addressed orders; weather conditions and so on. We will try our best to notify customers of the delay and estimated delivery date in such situations.</p>

<p><strong>DO YOU OFFER FREE SHIPPING?</strong></p>
<p>Yes, we do. All orders of Rs. 500 and above enjoy free delivery, while a no foxsky delivery charge of Rs. 50 is applied on orders below.</p>

<p><strong>WHY IS CASH-ON-DELIVERY NOT OFFERED FOR MY LOCATION?</strong></p>

<p>Availability of Cash-on-Delivery depends on the ability of our courier partner servicing your location to accept cash as payment at the time of delivery. Please enter your pin code on the product page to check if Cash-on-Delivery is available in your location. In addition, our delivery partners have foxsky on the cash amount payable on delivery depending on the destination and your order value foxsky have exceeded this foxsky.</p>

<p><strong>IS IT NECESSARY FOR ME TO PROVIDE IDENTIFICATION WHEN I RECEIVE THE PACKAGE?</strong></p>

<p>No, it isn’t, though the authorized / intended recipient will be required to sign the delivery document upon the delivery of the package.</p>

<p><strong>WHAT SHOULD I DO WHEN I RECEIVE MY DELIVERY?</strong></p>

<p>All Foxsky packages are perfectly sealed at the warehouse before they are shipped to you. Upon receipt, please check the external condition of the package/box to see if there are signs of damage, or if the package has been opened during transit. We request you not to sign and accept if you are concerned about the condition of the shipment. Instead, hand the package back to the courier and we will replace the items to you within few days.</p>

<p>If you find that the product in the box is tampered or foxsky processing when you open the package, please contact Foxsky India Customer Service at 1800 532 8777 within 24 hours of receiving the shipment.</p>

<p><strong>ARE IN-TRANSIT PRODUCTS INSURED?</strong></p>

<p>All In-transit items are insured. If you feel item is damaged on opening, we would request you to not accept the item or report back to us immediately by contacting Foxsky Customer Service.</p>

<p><strong>CAN I MODIFY MY DELIVERY ADDRESS?</strong></p>

<p>Once the order has been placed, you cannot change the delivery address. You have to cancel the order and re-order the product with the correct address. The payments, if you have already made, will be refunded to you as per refund terms and conditions.</p>

<p><strong>WHAT CAN I DO IF THE PACKAGE WAS DELIVERED TO THE WRONG PERSON?</strong></p>

<p>Please contact our delivery partners or Foxsky India customer service to get help on such issues.</p>

<p><strong>HOW CAN I COLLECT MY ORDER THROUGH SMARTBOX?</strong></p>

<p>Three simple steps to follow:-</p>
<ul>
<li><p>You will receive and OTP once your order has been dropped off.</p></li>

<li><p>To collect your order, enter your registered phone number & OTP on the Smart Box display screen.</p></li>

<li><p>Order will be returned if not collected within 72 hours.</p></li>
</ul>

<p><strong>THE PARCEL IN SMARTBOX LOCKER SEEMS TO BE TAMPERED, WHAT SHOULD I DO?</strong></p>

<p>Please collect the product & reach out to our CS team in case of any such issues. Do not leave the product in the locker.</p>

<p><strong>I FOXSKYSSED MY OTP FOR SMARTBOX, HOW SHOULD I GET ONE AGAIN?</strong></p>

<p>The customer is advised to regenerate the OTP using the foxsky. In case of OTP not received because of technical issue, please reach out to Smartbox customer care team. The team verifies the customer and will share a revised OTP once the customer is verified.</p>

<p><strong>CAN THE PARCEL BE RETURNED USING SMARTBOX?</strong></p>

<p>Currently this service is not available. You cannot return the product back through Smartbox. Please reach out to foxsky CS with in 24hrs of order pick-up, in case you have any issues with the product.</p>

<p><strong>ACCIDENTALLY CLOSED MY LOCKER WITHOUT PICKING-UP THE PARCEL?</strong></p>

<p>Once the locker is opened, we suggest you to immediately collect the order. In any unwanted situation where you are unable to collect the order & the locker gets closed, please reach out to Smartbox customer service at 1800 532 8777</p>

<p><strong>MY OTP IS NOT WORKING, HOW DO I OPEN THE BOX?</strong></p>

<p>Please reach out to Smartbox customer service at 1800 532 8777</p>

<p><strong>CAN SOMEONE COLLECT MY PARCEL ON MY BEHALF?</strong></p>

<p>Your entrusted person can collect the parcel on your behalf by entering your mobile number and the OTP that was sent to your number. Please note that in this case you will be considered entirely responsible of disclosing the confidential message details to a third party.</p>

<p><strong>HOW DO I PAY FOR MY COD SHIPMENT?</strong></p>

<p>All Smartbox foxsky support multiple payment options, please see the options below for making payment at the foxsky</p>
<ul>
<li><p>Pay via Debit/Credit Card: All foxsky are equipped with the secure payment gateways to accept all major debit and credit cards. It is as easy as using an ATM machine.</p></li>

<li><p>Pay via Paytm: Select Paytm option when prompted for payment at the foxsky. Scan the QR code shown on the screen via the Paytm mobile application on your phone. Approve the payment once the amount for your COD order is reflected in your Paytm app. Collect your order upon successful payment.</p></li>

<li><p>Pay via UPI: For making payment via UPI, make sure you have VPA (Virtual Payment Address) configured on your preferred UPI mobile application. Just select the UPI option when prompted for payment on the foxskyl and accept the transaction via your bank’s UPI application.</p></li>
</ul>
<p><strong>CAN I PAY WITH CASH AT THE SMARTBOX?</strong></p>

<p>Sorry. Currently we do not support this feature as a part of our service offering.</p>

<p><strong>HOW WILL I GET MY TRANSACTION CHARGE SLIP AFTER COMPLETION OF PAYMENT?</strong></p>

<p>There will be no print out of your charge slip. However the digital charge slip is sent to you over SMS and email at the end of the transaction.</p>

<p><strong>WHO IS YOUR DELIVERY PARTNER?</strong></p>

<p>We partner with Blue Dart, FedEx , Delhivery, Ekart , Xpress Bees & Ecom Express for deliveries in India.</p>

<p><strong>HOW CAN I TRACK MY ORDER AND CONTACT THE COURIER IF I HAVE A COMPLAINT OR ENQUIRIES?</strong></p>

<p>Please track your order details on foxskyindia.com through //buy.foxskyindia.com/in/user/order or directly through our delivery partners’ websites using your tracking number.</p>
<p>BLUEDART : https://www.bluedart.com/</p>
<p>FEDEX : http://www.fedex.com/in/</p>
<p>DELHIVERY : http://www.delhivery.com/ </p>
<p>XPRESS BEES: http://www.xpressbees.com/</p> 
<p>ECOM EXPRESS: http://www.ecomexpress.in/</p>
<p>Ekart : http://www.ekartlogistics.com/</p>
<p>Smartbox : https://smartbox.in/</p>

 

<p>Service centre</p>

<p>Call : 1800 532 8777</p>

<p>We have 410 service centre service available at 10000 pin codes,</p>

<h3><strong>Warranty</strong></h3>

<h3><strong>LIMITED WARRANTY STATEMENT</strong></h3>
<hr />
<p>This limited warranty shall apply to Foxsky products. For handset, air purifier and accessory defects under normal use circumstances and at the discretion of the company, Foxsky shall provide free of charge repair and/or replacement services within the warranty period.</p>

<h3><strong>LIMITED WARRANTY</strong></h3>
<div class="table-responsive">
<table class="table table_control_support table-bordered">
    <thead>
      <tr>
        <th>Item</th>
        <th>Warranty Period</th>
        <th>Scope of Warranty</th>
        <th>Warranty service</th>
        <th>Warranty Support Document</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Foxsky original battery, adapter and other accessories packaged</td>
        <td>6 months</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      
    </tbody>
  </table>
  </div>

<p>The Limited Warranty starts from the day the customer receives the product.<br />
During the warranty period, if any defect of air purifier is inspected and confirmed by a Foxsky authorized service center, a free replacement service shall be provided.
</p>


<h3><strong>FOXSKY LED  TV  WARRANTY POLICY</strong></h3>

<div class="table-responsive">
<table class="table table_control_support table-bordered">
    <thead>
      <tr>
        <th>Product</th>
        <th>Category</th>
        <th>Warranty coverage</th>
        <th>Period</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>TV</td>
        <td>Product</td>
        <td>TV / Remote / Foxsky IR Cable</td>
        <td>1 year</td>
      </tr>
      <tr>
        <td>TV</td>
        <td>Parts</td>
        <td>Main Board, Power Board, IR PCB,LVDS Cable, Speaker</td>
        <td>1 year</td>
      </tr>
      <tr>
        <td>TV</td>
        <td>Panel</td>
        <td>Extended warranty for panel</td>
        <td>1+1 year</td>
      </tr>
      
      
      
    </tbody>
  </table>
  </div>

<h3><strong>ACCESSORY PRODUCT LIMITEDWARRANTY</strong></h3>

<p>This Accessory Product Limited Warranty only covers accessories sold on <a href="https://www.foxskyindia.com">www.foxskyindia.com.</a></p>

<p>Service Centers supporting Accessory Services, please visit the website: <a href="https://www.foxskyindia.com/in/service/repair/">www.foxskyindia.com/in/service/repair/.</a></p>

<div class="table-responsive">
<table class="table table_control_support table-bordered">
    <thead>
      <tr>
        <th>Item</th>
        <th>Scope of Warranty</th>
        <th>Scope of Warranty</th>
        <th>Warranty service</th>
        <th>Warranty Support Document</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Battery</td>
        <td>6 months</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
       <tr>
        <td>Battery Charger</td>
        <td>6 months</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      <tr>
        <td>USB Cable</td>
        <td>6 months</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
       <tr>
        <td>Foxsky Band</td>
        <td>1 year(excluding strap)</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      <tr>
        <td>Headphone</td>
        <td>6 months</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      <tr>
        <td>Power Bank</td>
        <td>6 months</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      <tr>
        <td>Bluetooth Speaker</td>
        <td>6 months</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      <tr>
        <td>T shirt</td>
        <td>No warranty</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
      </tr>
      <tr>
        <td>Backpack</td>
        <td>6 months</td>
        <td>Manufacturing defects</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      <tr>
        <td>Foxsky Car Charger</td>
        <td>6 months</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      <tr>
        <td>Foxsky Body Composition Scale</td>
        <td>1 Year</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      <tr>
        <td>Bluetooth Audio Receiver</td>
        <td>6 months</td>
        <td>Labor and parts</td>
        <td>Walk in</td>
        <td>Purchase Order No.</td>
      </tr>
      
      
      
    </tbody>
  </table>
  </div>


<p>The Limited Warranty starts from the day the customer receives the product.
During the warranty period, if any accessory defect is inspected and confirmed by a Foxsky authorized service center, a free replacement service shall be provided.</p>

<ul class="suport_list">
<li><p>1. This limited warranty is only valid in India, and the product is not eligible for any international warranty service. To the fullest extent performed by law, warranty service may only be performed by Foxsky or Foxsky authorized service centers.</p></li>

<li><p>2. Foxsky may conduct diagnostic tests on customers‘ products to identify the causes of failures/defects. Before returning any unit for service, customer should back up data and remove any confidential and/or personal information from the product. Foxsky is not responsible for damage or loss of any program, data, or removable storage media.</p></li>

<li><p>3. Prior to contacting a Foxsky service agent, please ensure the following information is at hand:</p></li>
<ul>
	<li><p>- Model, serial number, and IMEI number if available.</p></li>
    <li><p>- Customer’s full address and contact information.</p></li>
    <li><p>- Purchase order number, a copy of the customer’s original invoice/receipt.</p></li>
</ul>

<li><p>4.     This warranty does not cover the following cases:</p></li>

<ul>
	<li><p>- If the product serial number, IMEI number or warranty seal is illegible or has been removed, erased, defaced, altered, and/or tampered. If any accessory or external part of the product is missing.
</p></li>
    <li><p>- Warranty does not cover natural wear & tear, usage under extreme conditions, damage due to improper care (accident, misuse or negligence) and damage caused by acts of god such as floods, fires or earthquakes.</p></li>
    <li><p>- If any damage occurs in/on outer surface of the product, including but not limited to cracks, dents or scratches on the exterior cases, screens, camera lenses, buttons and other attachments.</p></li>
    
<li><p>- For Foxsky Band strap, if it becomes dirty or damaged due to contact with chemical agents, sharp objects, improper handling, accident, abuse, or under abnormal use or conditions such as sauna, swimming, and so forth.</p></li>
<li><p>-  General maintenance, password reset assistance, cleaning, application update/installation, product demonstration, or any other service other than repair/replacement;</p></li>
<li><p>- Deterioration of the product caused by normal wearing and tearing, including but not limited to rust or stains;</p></li>
<li><p>- Any other circumstances that are contradictory or are not in compliance with business ethics.</p></li>
    
</ul>



         
        

<li><p>5. Manufacturing warranty applies on:</p></li>

<ul>
<li><p>- Manufacturing defects in materials and workmanship of the product. This warranty applies when the product is used under normal conditions and for the purpose in which the product was designed.</p></li>
<li><p>- Warranty covers all manufacturing defects on zippers, runners, buckles & stitching issues caused by workmanship errors.</p></li>

</ul>
         
<li><p>6. Foxsky will determine whether a product is "Out of Warranty" at the company’s discretion according to the standards listed below. Repair of "Out of Warranty" products shall be separately quoted by the Foxsky service center and respective service shall be provided upon service fee payment.</p></li>


<ul>
<li><p>- Violations against warranty, including but not limited to customer induced damage, such as self-repairs, exposure to water, damage caused by misuse, alternation, failure to comply with product manual, and so on.</p></li>
<li><p>- Invalid warranty</p></li>
<li><p>- Expired warranty</p></li>
<li><p>-</p></li>

</ul>

</ul>


<p><strong>Foxsky LED Smart TV (55) Warranty Terms & Condition:</strong></p>
<ul>
<li><p>Warranty is limited to the first purchaser of this product and it is not transferrable. Purchase invoice with corresponding model and serial number of the unit must be presented to claim warranty.</p></li>

<li><p>Repair or replacement will be carried out through the Authorized Service Centre.</p></li>

<li><p>Free Installation or demo of the product, can be availed only once & within 1 month from the purchase date.</p></li>

<li><p>The warranty does not cover accessories external to the system. · The company's obligation under this warranty shall be limited to repair or providing replacement of parts only.</p></li>

<li><p>In the event of repairs/replacement of any parts of the unit, this warranty will thereafter continue and remain in force only for the unexpired period of the warranty from date of purchase. Moreover, the time taken for repair/replacement whether under the warranty or otherwise shall not be excluded from the warranty period.</p></li>

<li><p>In case of any damage during transit by Customer, the product shall be repaired by the concerned service center on chargeable basis and warranty for unexpired period to continue from date of purchase.</p></li>

<li><p>Foxsky Technology India Pvt. Ltd. (“Company”) or its Authorized Service Centre reserves the right to retain any parts or components replaced at its discretion, in the event of a defect being noticed in the equipment during warranty period.</p></li>

<li><p>Replacement of parts would be purely at the discretion of the Company, alone. In case the replacement of the entire unit is being made, (subject to the sole discretion of the Company), the same model shall be replaced and in the event, such model has been discontinued, it shall be replaced with the model equivalent as deemed by the Company.</p></li>

<li><p>The TV/product box must be opened and installed by a Foxsky authorized personnel/technician else the warranty or any other claim will be void.</p></li>

<li><p>In the event of any unforeseen circumstance, and spares not being available, the Company's prevailing depreciation rules will be binding on the Customer to accept as a commercial solution in lieu of repairs.</p></li>

<li><p>This warranty will automatically terminate on the expiry of the warranty period as specified herein.

<li><p>No Dealer/Distributor/Retailer has authority to vary the terms of above warranty.</p></li>

<li><p>Only courts in Bangalore shall have the jurisdiction for settling any claims, disputes arising under the warranty.</p></li>

<li><p>Warranty is applicable only for products purchased in India. Applicable service charges may be different for products purchased outside India ( Other than India )</p></li>

<li><p>Any issues related to the product customer should report to Call Center (18005328777)</p></li>
</ul>
<p><strong>Foxsky LED Smart TV   Terms & Conditions:</strong>.</p>
<ul>
<li><p>Warranty does not cover damages caused due to improper installation, repair, unauthorized modification, physical damage and water log damages or connection of the product to equipment not approved by the Company.</p></li>

<li><p>This Warranty will be void on removal, tampering or alteration of any identification labels on the product or any of its components including serial number.</p></li>

<li><p>If the product is not used as per its usage specifications warranty will be void.</p></li>

<li><p>Company will not be liable for consequential or resulting liability, damage or loss to property or life arising directly or indirectly out of any defect in the product. The maximum claim if entertained by the Company will be subject to the maximum retail price of the product purchased or the purchase price, whichever is lower.</p></li>

<li><p>Defects due to cause beyond control like lightning, abnormal voltage, natural disaster and acts of God or while in transit to service Centre or purchaser's residence will not be covered by the Warranty. * Customer should inform call center within 24hrs from the time of delivery in case of any physical damage in the product</p></li>
</ul>
<p>For more details, please call Foxsky hotline: 18005328777</p>

 

 

<p><strong>Bulk order</strong></p>

<p>LED TV,  POWER BANK, SMART BAND , CHARGERS , HEADPHONES , BLUETOOTH SPEAKER ,</p>
            </div>
        </div>
    </div>
</div>




<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>
<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			$(".navigation > ul > li > a").hover(function(){
				//alert("ddd");
				if($(this).next(".product_item_view").length > 0){
				$(".logo_row").addClass("show_nav");
				$(this).next(".product_item_view").css({"display":"block"});
				}
				else{
					$(".logo_row").removeClass("show_nav");
					$(".product_item_view").css({"display":"none"});
				}
			});
			$(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
				$(".product_item_view").css({"display":"none"});
			});
		});
		</script>
         <script src="assets/js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    
 	<script src="assets/js/jquery.bxslider.min.js"></script>
	   <script>
        $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 330,
        slideMargin: 0,
        ticker: true,
        speed: 30000
    });
       </script>
 
