 <div class="container">
   		<div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<div class="layout_sign-in">
                	<div class="lgnheader">
                    	<div class="lgnheader-logo"><img src="<?php echo base_url();  ?>assets/webapp/images/logo.png" alt=""></div>
                        <h4 class="header_tit_txt">Sign in to your Foxsky Account</h4>
                        <div class="tabs-login">
                        	<div class="login_area">
                                <form method="post">
                                  <div class="form-group">
                                    <input type="text" class="login-input" id="email" name="email" placeholder="Email/Phone/Foxsky Account">
                                  </div>
                                  <div class="form-group">
                                    <input type="password" class="login-input" id="pwd" name="pwd" placeholder="Password">
                                  </div>
                                  <div class="form-group btns_bg">
                                  <button type="submit" name="submit" class="btnadpt btn_orange">Sign in</button>
                                  </div>
                                </form>
                                <div class="facebool-login">
                                    <a href="<?php echo base_url(); ?>webapp/fblogin/login" class="btnadpt btn_facebook"><img src="<?php echo base_url();  ?>assets/webapp/images/facebook-con.png" alt="facebook" class="facebook-icon">Sign in with Facebook</a>
                                </div>
                                
                                <div class="forgot-pwd">
                                	<a href="<?php echo base_url(); ?>register">Create account</a>
                                    <a href="JavaScript:Void(0);">|</a>
                                    <a href="<?php echo base_url(); ?>webapp/memberlogin/forgotpwd">Forgot password?</a>
                                </div>
                                
                        	</div>
                        </div>
                    </div>
                </div>
            	
                <div class="policy-area">
                	<ul class="policy-select-list">
                    <!-- 	<li><a href="#">English</a></li>
                        <li><a href="#">|</a></li> -->
                        <li><a href="<?php echo base_url(); ?>useraggree">User Agreement</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="<?php echo base_url(); ?>privacy">Privacy Policy</a></li>
                    </ul>
                    <p class="reserved-intro">Foxsky Inc., All rights reserved</p>
                </div>
                
            </div>
        </div>
   </div> 