<style>
.xm-section p {
	text-align:justify;
}
</style>

<div class="wrapper sub_nav_bg">
  <div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="sub_nav">
				<div class="pull-right">
            		<ul class="nav_about">
                      <li><a href="<?php echo base_url();?>aboutus">About us</a></li>
                    	<li><a href="<?php echo base_url();?>support">Support</a></li>
                        <li><a href="<?php echo base_url();?>useraggree">User Agreement</a></li>
                        <li><a href="#" class="active-orage">Privacy Policy</a></li>                         
                        <li><a href="<?php echo base_url();?>career">Career</a></li>
                        <li><a href="<?php echo base_url();?>terms">Terms of Use</a></li>
                    </ul>
            	</div>
              </div>
            </div>
        </div>
    </div>
</div>		
        
        
<!--audio Specs Start --> 

<div class="wrapper bg_orage">
	<div class="container">
        <div class="about_heading">
        	<h3 class="about_h4">PRIVACY POLICY</h3>
        </div>
    </div>
</div>

<div class="wrapper parivacy-policy">
	<div class="container">
        <div class="row">
            <div class="xm-section">
                <p><strong>OUR foxsky india commitment TO YOU</strong></p>
<p>This Privacy Policy sets out how foxsky Inc. and its affiliated companies within the foxsky Group (foxsky”, “we”, “our” or “us”) collect, use, disclose, process and protect any information that you give us when you use our products and services located at www.foxskyindia.com, foxsky.in, account.foxskyindia.com, foxsky and our Suite of applications that we offer on our mobile devices, for a list of these applications, Should we ask you to provide certain information by which you can be identified when using foxsky products and services, it will only be used in accordance with this Privacy Policy and/or our terms and conditions for users.
</p>

<p>The Privacy Policy is designed with you in foxskyindia, and it is important that you have a comprehensive understanding of our personal information collection and usage practices, as well as full confidence that ultimately, you have control of any personal information provided to foxsky.
</p>


<p>In this Privacy Policy, “personal information” means information that can be used to directly or indirectly identify an individual, either from that information alone or from that information combined with other information foxsky access about that individual. Such personal information may include but not foxsky to the information you provide to us or upload, the information specific to you that may be assigned by us, your financial information, social information, device or sim-related information, location information, log information.</p>

<p>By using foxsky products and services or other acting perfoxskytted by the applicable laws, you are deemed to have read, acknowledged and accepted all the provisions stated here in the Privacy Policy, including any changes we may make from time to time. In order to comply with applicable laws, including local data protection legislation , we will specifically seek prior explicit consent to the particular processing (e. g. automated individual decision-making) of special categories of personal data. Furthermore, we are comfoxskytted to protecting the privacy, confidentiality and security of your personal information by complying with applicable laws, and we are equally comfoxskytted to ensuring that all our employees and agents uphold these obligations.</p>

<p>Ultimately, what we want is the best for all our users. Should you have any concerns with our data handling practice as summarized in this Privacy Policy, please contact privacy.foxskyindia.com to address your specific concerns. We will be happy to address them directly.</p>


<p>If you have questions or concerns regarding our Privacy Policy or practices, please contact us at privacy@foxsky.com.</p>
<p><strong>WHAT INFORMATION IS COLLECTED AND HOW CAN WE USE IT?</strong></p>
<p><strong>TYPES OF INFORMATION COLLECTED</strong></p>

<p>In order to provide our services to you, we will ask you to provide personal information that is necessary to provide those services to you. If you do not provide your personal information, we may not be able to provide you with our products or services.</p>

<p>We will only collect the information that is necessary for its specified, explicit and legitimate purposes and not further processed in a manner that is incompatible with those purposes. We may collect the following types of information (which may or may not be personal information):</p>

<ul>
	<li>
    	<p>Information you provide to us or upload (including your contact details): we may collect any and all personal information you provide to us, like your name, mobile phone number, email address, delivery address, ID card, driver license, passport details, Foxsky Account details (e.g. your security related information, name, birthday, gender), order, invoicing details, materials or data you may sync through Foxsky Cloud or other apps (e.g. photos, contact lists), information in relation to creating an account and participating in the FOXSKYUI Forum or other Foxsky platforms, phone numbers you insert into your contacts or to send a message, feedback, and any other information you provide us.</p>
    </li>


 <li><p>Information specific to you that may be assigned by us: we may collect and use information such as your Foxsky Account ID.</p></li>

<li><p>Information specific to you that may be assigned by Third Party Service Providers: we may collect and use information such as your advertising ID assigned by Third Party Service Providers.</li>

<li><p>Financial information: information related to completing purchases. For example, bank account number, account holder name, credit card number etc.</p></li>

<li><p>Social information: information related to your social activities. For example, current employer, current job title, education background, professional training background etc.</p></li>

<li><p>Device or SIM-related information: information related to your device. For example, IMEI number, IMSI number, MAC address, Serial number, FOXSKYUI version and type, Android version, Android ID, screen display information, device keypad information, device manufacturer details and model name, network operator, connection type, hardware usage information such as battery usage, device temperature.</p></li>

<li><p>Application information: information related to your software usage. For example, application list, application status record (e.g. downloading, installing, updating, deleting), application ID information, SDK version, system update settings etc.</p></li>

<li><p>Location information (only for specific services/functionalities): various types of information on your location. For example, region, country code, city code, mobile network code, mobile country code, cell identity, longitude and latitude information, time zone settings, language settings.</p></li>

<li><p>Log information: information related to your use of certain functions, apps and websites. For example, cookies and other anonymous identifier technologies, IP addresses, network request information, temporary message history, standard system logs, crash information.</p></li>

<li><p>Other information: environmental characteristics value (ECV) (i.e. value generated from Foxsky Account ID, phone device ID, connected Wi-Fi ID and location value).</p></li>

</ul>

<p>We may also collect other types of information which are not directly or indirectly linked to an individual and which is aggregated, anonyfoxskyzed or de-identified. For example, the device model and system version number of the user’s Foxsky mobile phone device may be collected when using a particular service. Such information is collected in order to improve the services we provide to you.</p>

<p><strong>HOW THE PERSONAL INFORMATION CAN BE USED</strong></p>

<p>Personal information is collected for providing services and / or products to you, and legal compliance on our part under applicable laws. You hereby consent that we may process and disclose personal information to our affiliated companies (which are in the communications, social media, technology and cloud businesses), Third Party Service Providers (defined below) for the purposes stated in this Privacy Policy.</p>

<p><strong>We may use your personal information for the following purposes:</strong></p>

<ul>
<li><p>Providing, processing, maintaining, improving and developing our goods and/or services to you, including after-sales and customer support and for services on your device or through our websites.</p></li>

<li><p>Communicating with you about your device, service or any general queries, such as updates, customer inquiry support, information about our events, notices.</p></li>

<li><p>Conducting marketing related activities, such as providing marketing and promotional materials and updates. For more information on marketing and promotional activities, please refer to the Direct Marketing section below.</p></li>

<li><p>Allowing you to post comments in public forums.</p></li>
<li><p>Conducting promotional activities, such as sweepstakes and Facebook events.

<li><p>Analyzing and developing statistical information on use of our products and services to better improve our products and services.</p></li>

<li><p>Optifoxskyzing the performance of your device, such as analyzing the memory usage or CPU utilization of our applications.</p></li>

<li><p>Storing and maintaining information about you for our business operations or legal obligations.</p></li>

<li><p>Providing local services without communicating with our servers.</p></li>
</ul>

<p><strong>Here are more details on how we use your information (which may include personal information):</strong></p>
<ul>
<li><p>Setting up your Foxsky Account. Personal information collected when creating a Foxsky Account on our web sites or through our mobile devices is used for creating the personal Foxsky Account and profile page for the user.</p></li>

<li><p>Processing your purchase orders. Information relating to e-commerce orders may be used for processing the purchase order and related after-sales services, including customer support and re-delivery. In addition, the order number is used to cross check the order with the delivery partner as well as the actual delivery of the parcel. The receipt details, including name, address, phone number and postal code are for delivery purposes. The email address is used to send parcel tracking information to the user. The list of purchased item(s) is used for printing the invoice and allowing users to see what is in the parcel.</p></li>

<li><p>Allowing you to participate in FOXSKY Forum. Personal information in relation to the FOXSKYUI Forum or other Foxsky Internet platforms may be used for profile page display, interaction with other users, participating in the forum.</p></li>

<li><p>Providing Foxsky Cloud and other FOXSKY services. Information (device or SIM card-related information including IMEI number, IMSI number, phone number, device ID, device operating system, MAC Address, device type, system and performance information and location information including mobile country code, mobile network code, location area code and cell identity) is collected for activating FOXSKYUI services, e.g. Foxsky Cloud, call log sync, SMS sync, Find Device, for the purposes of user authentication and activation of the services.</p></li>

<li><p>Diagnosing activation failures: Location related information is used for the purpose of assessing SIM card activation failure (i.e. failure of SMS gateway and network) to identify the network operator of that service, and notify the network operator of that failure.</p></li>

<li><p>Providing other FOXSKYUI services. Other information collected for each of the FOXSKYUI services may be used for perforfoxskyng the functions of that service, and to facilitate the provision of that service for the benefit of the user, e.g. downloading, updating, registering, perforfoxskyng or optifoxskyzing activities related to FOXSKYUI services. For example, personal information collected by the Theme Store may be used to provide personalized theme recommendation services based on your downloading and browsing history.</p></li>

<li><p>Finding your device: Foxsky’s Find Device feature helps you find and secure your phone if it is lost or stolen. You can locate your phone on a map using location information provided by your phone, wipe your phone, or lock your phone. We may collect your location data directly from your mobile device, or in some situations, from cell towers or Wi-Fi hotspots.</p></li>

<li><p>Recording location information in photos. You have the ability to record your location information while taking a photo. This information will be visible within your photos folder and the location will be put into the header of your photos. If you do not wish to have your location recording while taking a photo, you may turn this off at any time within the camera settings of the device.</p></li>

<li><p>Providing messaging functions (e.g. Foxsky Talk, Foxsky Message). If you download and use Foxsky Talk, information collected for Foxsky Talk may be used for activating this service and identifying the user and message recipient. In addition, chat history is stored for the convenience of re-loading historical chats after a user has re-installed apps, or for synchronization across devices. Information (sender’s and recipient's phone numbers and Foxsky Message IDs) may be used for Foxsky Message for activating the services and enabling the service to function, including routing of messages.</p></li>

<li><p>Providing location based services. In the course of using FOXSKY services, location information may also be used by us or Third Party Service Providers to serve you the correct version of the service and provide accurate details about that location for the best possible user experience, e.g. weather details, location access (as part of the Android platform). You may turn this off at any time by going into the device settings or discontinue use of that application.</p></li>

<li><p>Improving user experience. Some opt-in features, such as the User Experience Program, allow Foxsky to analyze data about how users use the mobile phone and FOXSKY services, so as to improve the user experience, such as sending crash reports.</p></li>

<li><p>Allowing you to use Security Center. Information collected may be used for security and system up-keeping functionalities in the Security Center, such as advertising blocker, virus scan, power saver, blocklist, cleaner, etc. Some of these functionalities are operated by Third Party Service Providers. Information (which is not personal information like virus definition lists) is used for virus scan functions.</p></li>

<li><p>Providing Push Service. Foxsky Account ID and IMEI numbers will also be used to provide the Foxsky push service to evaluate advertising performance and send notifications from FOXSKYUI about software updates or new product announcements, including information about sales and promotion. Furthermore, under entrust of selected third party (the data controller of your personal information), the Foxsky push service may also evaluate advertising performance or send notifications by using your Foxsky Account ID and IMEI numbers. You consent to our use of your personal information for the purpose of sending you pushing services (whether by messaging within our services, by email or by other means) that offer or advertise our products and services and/or the products and services of selected third parties. You may opt out of this at any time through changing your preferences under “Settings”, or through the selected third party you consent.</p></li>

<li><p>Verifying user identity. Foxsky uses the ECV value to verify the user identity and ensure there is no log-in by hackers or unauthorized persons.</p></li>

<li><p>Collecting user feedback. The feedback you choose to provide is valuable in helping Foxsky make improvements to our services. In order to follow up on the feedback you have chosen to provide, Foxsky may correspond with you using the personal information that you have provided and keep records.</p></li>

<li><p>Sending notices. From time to time, we may use your personal information to send important notices, such as communications about purchases and changes to our terms, conditions, and policies.</p></li>

<li><p>Conducting promotional activities. If you enter into a sweepstake, contest, or sifoxskylar promotion, e.g. via Foxsky’s Facebook page or other social media platforms, we may use the personal information you provide to adfoxskynister those programs.</p></li>

<li><p>Conducting analysis of your device to provide better user experience. Foxsky may conduct the hardware or software analysis, so as to further improve the performance of your device.</p></li>


</ul>

<p><strong>DIRECT MARKETING</strong></p>
<ul>
<li><p>We may use your name, phone number, and email address, Foxsky Account ID and IMEI number to provide marketing materials to you relating to goods and services of Foxsky companies and our business partners which offer network, mobile applications and cloud products and services. To provide better user experience, we may recommend above-mentioned products, services and activities based on information about your purchase history, website browsing history, birthday, age, gender, and location. We will only so use your personal data after we obtain your prior explicit consent and involve a clear affirmative action or indication of no objection in accordance with local data protection laws, which may require separate explicit consent. You have the right to opt out of our proposed use of your personal data for direct marketing. If you no longer wish to receive certain types of email communications you may opt-out by following the unsubscribe link located at the bottom of each communication. We will not transfer your personal data to our business partners for use by our business partners in direct marketing.</p></li>
</ul>

<p><strong>COOKIES AND OTHER TECHNOLOGIES</strong></p>
<ul>

<li><p>What information is collected and how we can use them: Technologies such as cookies, tags, and scripts are used by Foxsky and our Third Party Service Providers. These technologies are used in analyzing trends, adfoxskynistering the site, tracking users’ movements around the website and to gather demographic information about our user base as a whole. We may receive reports based on the use of these technologies by these companies on an individual as well as aggregated basis.</p></li>

<li><p>Log Files: As true of most websites, we gather certain information and store it in log files. This information may include Internet protocol (IP) addresses, browser type, Internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and/or clickstream data. We do not link this automatically collected data to other information we gather about you.</p></li>

<li><p>Advertising: We partner with our Third Party Service Providers to either display advertising on our website or to manage our advertising on other sites. Our Third Party Service Provider may use technologies such as cookies to gather information about your activities on this site and other sites in order to provide you advertising based upon your browsing activities and interests. We will obtain your prior explicit consent and involve a clear affirmative action before providing this advertising service to you. If you wish to not have this information used for the purpose of serving you interest-based ads, you may opt-out by clicking here http://preferences-mgr.truste.com.</p></li>

<li><p>Mobile Analytics: Within some of our mobile applications we use mobile analytics software to allow us to better understand the functionality of our Mobile Software on your phone. This software may record information such as how often you use the application, the events that occur within the application, aggregated usage, performance data, and where crashes occur within the application. We do not link the information we store within the analytics software to any personal information you subfoxskyt within the mobile application.</p></li>

<li><p>Local Storage – HTML5/Flash: We use Local Storage Objects (LSOs) such as HTML5 or Flash to store content and preferences. Third parties with whom we partner to provide certain features on our Sites or to display advertising based upon your web browsing activity also use HTML5 or Flash cookies to collect and store information. Various browsers may offer their own management tool for removing HTML5 LSOs. To manage Flash cookies, please click here: http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager07.html .</p></li>

</ul>

<p><strong>WITH WHOM WE SHARE YOUR INFORMATION</strong></p>

<p>We do not sell any personal information to third parties.</p>

<p>We may disclose your personal information on occasion to third parties (as described below) in order to provide the products or services that you have requested.</p>

<p>Disclosure may be made to Third Party Service Providers and affiliated companies listed in this section below. In each case described in this section, you can be assured that Foxsky will only share your personal information in accordance with your consent. Your consent to Foxsky will engage sub-processors for the processing of your personal information. You should know that when Foxsky shares your personal information with a Third Party Service Provider under any circumstance described in this section, Foxsky will contractually specify that the third party is subject to practices and obligations to comply with applicable local data protection laws. Foxsky will contractually ensure compliance by any Third Party Service Providers with the privacy standards that apply to them in your home jurisdiction.</p>

<p><strong>SHARING WITH OUR GROUP AND THIRD PARTY SERVICE PROVIDERS</strong></p>

<p>From time to time, in order to conduct business operations smoothly in providing you with the full capabilities of our products and services, we may disclose your personal information from time to time to other Foxsky affiliated companies (in communications, social media, technology or cloud businesses), or our third party service providers which are our mailing houses, delivery service providers, telecommunications companies, data centers, data storage facilities, customer service providers, advertising and marketing service providers, agents acting on behalf of Foxsky, [related corporations, and/or other third parties] (together “Third Party Service Providers”). Such Third Party Service Providers would be processing your personal information on Foxsky’s behalf or for one or more of the purposes listed above. We may share your IP address with third parties when using certain mobile applications on our device in order to provide you with some of the services you requested. If you no longer wish to allow us to share this information, please contact us at privacy@foxsky.com.</p>

<p><strong>SHARING WITH OUR GROUP’S ECOSYSTEM COMPANIES</strong></p>

<p>Foxsky works together with a cool group of companies, which together form the Foxsky Ecosystem. The Foxsky Ecosystem companies are independent entities, invested and incubated by Foxsky, and are experts in their fields. Foxsky may disclose your personal data to the Foxsky Ecosystem companies so as to provide you with and improve the exciting products and services (both hardware and software) from the Foxsky Ecosystem companies. Some of these products and services will still be under the Foxsky brand, while others may use their own brand. The Foxsky Ecosystem companies may also share data with Foxsky from time to time in relation to products and services under the Foxsky brand and other brands owned by Foxsky to provide hardware and software services, and to create better functions and user experience. Foxsky will take appropriate organizational and technical measures to ensure the security of personal data during the process of sharing of information, including but not lifoxskyted to the encryption of your personal data. If Foxsky is involved in a merger, acquisition or asset sale of all or a portion of our assets, you will be notified via email and/or a profoxskynent notice on our website, of any changes in ownership, uses of your personal information, and choices you may have regarding your personal information.</p>

<p><strong>SHARING WITH OTHERS</strong></p>

<p>Foxsky may disclose your personal information without further consent when required under applicable law.</p>

<p><strong>INFORMATION NOT REQUIRING CONSENT</strong></p>
<ul>
<li><p>We may share anonyfoxskyzed information and statistics in aggregate form with third parties for business purposes, for example with advertisers on our website, we may share them trends about the general use of our services, such as the number of customers in certain demographic groups who purchased certain products or who carried out certain transactions.</p></li>

<li><p>For the avoidance of doubt, Foxsky may collect, use or disclose your personal information without your consent if it is and only to the extent it is allowed explicitly under local data protection laws.</p></li>

</ul>

<p><strong>SECURITY SAFEGUARDS</strong></p>

<p><strong>FOXSKY’S SECURITY MEASURES</strong></p>

<p>We are comfoxskytted to ensuring that your personal information is secure. In order to prevent unauthorized access, disclosure or other sifoxskylar risks, we have put in place reasonable physical, electronic and managerial procedures to safeguard and secure the information we collect on your mobile device and on Foxsky websites. We will use all reasonable efforts to safeguard your personal information.</p>

<p>For example, when you access your Foxsky Account, you can choose to use our two-step verification process for better security. When you send or receive data from your Foxsky device to our servers, we make sure they are encrypted using Secure Sockets Layer (“SSL”) and other algorithms.</p>

<p>All your personal information is stored on secure servers that are protected in controlled facilities. We classify your data based on importance and sensitivity, and ensure that your personal information has the highest security level. We make sure that our employees and Third Party Service Providers who access the information to help provide you with our products and services are subject to strict contractual confidentiality obligations and may be disciplined or terfoxskynated if they fail to meet such obligations. We have special access controls for cloud based data storage as well. All in all, we regularly review our information collection, storage and processing practices, including physical security measures, to guard against any unauthorized access and use.</p>

<p>We will take all practicable steps to safeguard your personal information. However, you should be aware that the use of the Internet is not entirely secure, and for this reason we cannot guarantee the security or integrity of any personal information which is transferred from you or to you via the Internet.</p>
<p>We will take upon the personal data breach, notifying the breach to relevant supervisory authority or under some circumstances, notifying the personal data breach to the data subjects by complying with applicable laws, including your local data protection legislation.</p>

<p><strong>WHAT YOU CAN DO</strong></p>
<ul>
<li><p>You can play your part in safeguarding your personal information by not disclosing your login password or account information to anybody unless such person is duly authorized by you. Whenever you log in as a Foxsky Account user on Foxsky websites, particularly on somebody else's computer or on public Internet terfoxskynals, you should always log out at the end of your session.</p></li>

<li><p>Foxsky cannot be held responsible for lapses in security caused by third party accesses to your personal information as a result of your failure to keep your personal information private. Notwithstanding the foregoing, you must notify us immediately if there is any unauthorized use of your account by any other Internet user or any other breach of security.</p></li>

<li><p>Your assistance will help us protect the privacy of your personal information.</p></li>
</ul>

<p><strong>RETENTION POLICY</strong></p>

<p>Personal information will be held for as long as it is necessary to fulfill the purpose for which it was collected, or as required or perfoxskytted by applicable laws. We shall cease to retain personal information, or remove the means by which the personal information can be associated with particular individuals, as soon as it is reasonable to assume that the purpose for which that personal information was collected is no longer being served by retention of the personal information. If further processing is for archiving purposes in the public interest, scientific or historical research purposes or statistical purposes according to the applicable laws, the data can be further retained by Foxsky even if the further processing is incompatible with original purposes.</p>

<p><strong>ACCESSING OTHER FEATURES ON YOUR DEVICE</strong></p>

<p>Our applications may need access to certain features on your device such as enabling emails to contacts, SMS storage and Wi-Fi network status, as well as other features. This information is used to allow the applications to run on your device and allow you to interact with the applications. At any time you may revoke your perfoxskyssions by turning these off at the device level or contacting us at privacy@foxsky.com.</p>

<p><strong>YOU HAVE CONTROL OVER YOUR PERSONAL INFORMATION</strong></p>

<p><strong>CONTROLLING SETTINGS</strong></p>

<p>Foxsky recognizes that privacy concerns differ from person to person. Therefore, we provide examples of ways Foxsky makes available for you to choose to restrict the collection, use, disclosure or processing of your personal information and control your privacy settings:</p>


<ul>
<li><p>Toggle on/off for the User Experience Program and Location Access functions;</p></li>

<li><p>Log in and out of the Foxsky Account;</p></li>

<li><p>Toggle on/off for the Foxsky Cloud sync functions; and</p></li>

<li><p>Delete any information stored on Foxsky Cloud through www.foxsky.com/foxskycloud</p></li>

<li><p>Toggle on/off for other services and functionalities which deal with sensitive or personal information.</p></li>
</ul>
<p>You may obtain more details in relation to your device’s security status in the FOXSKYUI Security Center as well.</p>

<p>If you have previously agreed to us using your personal information for the abovementioned purposes, you may change your foxskynd at any time by writing or emailing us at privacy@foxsky.com.</p>

<p><strong>ACCESS, UPDATE, CORRECT, ERASE OR RESTRICT PROCESSING YOUR PERSONAL INFORMATION</strong></p>
<ul>
<li><p>You have the right to request access to and/or correction of any other personal information that we hold about you. When you update your personal information, you will be asked to verify your identity before we proceed with your request. Once we obtain sufficient information to accommodate your request for access to or correction of your personal information, we shall proceed to respond to your request within any timeframe set out under your applicable data protection laws.</p></li>

<li><p>A copy of your personal data collected and processed by us will be provided to you upon your request free of charge. For any extra requests of the same information, we may charge a reasonable fee based on actual adfoxskynistrative costs according to the applicable laws.</p></li>

<li><p>If you would like to request access to your personal data held by us or if you believe any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible at the email address below. Email: privacy@foxsky.com</p></li>

<li><p>For details relating to the personal information in your Foxsky Account, you may also access and change them at //account.foxsky.com or by logging into your account on your device.</p></li>

<li><p>If you are Europe Union user under General Data Protection Regulation (GDPR), you have the right to obtain from us the erasure of your personal information. We shall consider the grounds regarding your erasure request and take reasonable steps, including technical measures, if the grounds apply to GDPR.</p></li>

<li><p>If you are Europe Union user under GDPR, you have the right to obtain from us the restriction of processing your personal information. We shall consider the grounds regarding your restriction request. If the grounds apply to GDPR, we shall only process your personal information under applicable circumstances in GDPR and inform you before the restriction of processing is lifted.</p></li>

<li><p>If you are Europe Union user under GDPR, you have the right not to be subject to a decision based solely on automated processing, including profiling, which produces legal effects concerning you or sifoxskylarly significantly affects you.</p></li>

<li><p>If you are Europe Union user under GDPR, you have the right to receive your personal information in a structured, commonly used format and transfoxskyt the information to another data controller.</p></li>
</ul>
<p><strong>WITHDRAWAL OF CONSENT</strong></p>
<ul>
<li><p>You may withdraw your consent for the collection, use and/or disclosure of your personal information in our possession or control by subfoxskytting a request. This may be done by accessing your Foxsky Account management center at account.foxsky.com/pass/del. We will process your request within a reasonable time from when the request was made, and thereafter not collect, use and/or disclose your personal information as per your request.</p></li>

<li><p>Please recognize that your withdrawal of consent could result in certain legal consequences. Depending on the extent of your withdrawal of consent for us to process your personal information, it may mean that you will not be able to enjoy Foxsky’s products and services.</p></li>
</ul>
<p><strong>TRANSFER OF PERSONAL INFORMATION OUTSIDE OF YOUR JURISDICTION</strong></p>

<p>To the extent that we may need to transfer personal information outside of your jurisdiction, whether to our affiliated companies (which are in the communications, social media, technology and cloud businesses) or Third Party Service Providers, we shall do so in accordance with the applicable laws. In particular, we will ensure that all transfers will be in accordance with requirements under your applicable local data protection laws by putting in place appropriate safeguards. You will have the right to be informed of the appropriate safeguards taken by Foxsky for this transfer of your personal information.</p>

<p>Foxsky is a China-headquartered company operating globally. As such, complying with applicable laws, we may transfer your personal data to any subsidiary of the Foxsky group worldwide when processing that information for the purposes described in this Privacy Policy. We may also transfer your personal data to our third party service providers, who may be located in a country or area outside the area of the European Econofoxskyc Area (EEA).</p>

<p>Whenever Foxsky shares personal data originating in the EEA with a third party which may or may not be a Foxsky entity outside the EEA, we will do so on the basis of EU standard contractual clauses or any other safeguards provided for in the GDPR.</p>

<p>Foxsky may use overseas facilities operated and controlled by Foxsky to process or back up your personal information. Currently, Foxsky has data centers in Beijing, United States, Germany, Russia and Singapore. These overseas jurisdictions may or may not have in place data protection laws which are substantially sifoxskylar to that in your home jurisdiction. You have understood that the risks under applicable data protection laws are different and we may transfer to and store your personal information at our overseas facilities. However, this does not change any of our comfoxskytments to safeguard your personal information in accordance with this Privacy Policy.</p>

<p><strong>FOXSKYSCELLANEOUS</strong></p>

<p><strong>FOXSKYNORS</strong></p>
<ul>
<li><p>We consider it the responsibility of parents to monitor their children’s use of our products and services. Nevertheless, it is our policy not to require personal information from foxskynors or offer to send any promotional materials to persons in that category.</p></li>

<li><p>Foxsky does not seek or intend to seek to receive any personal information from foxskynors. Should a parent or guardian have reasons to believe that a foxskynor has provided Foxsky with personal information without their prior consent, please contact us to ensure that the personal information is removed and the foxskynor unsubscribes from any of the applicable Foxsky services.</p></li>
</ul>
<p><strong>ORDER OF PRECEDENCE</strong></p>

<p>If you have agreed to our applicable User Agreements, in the event of inconsistency between such User Agreements and this Privacy Policy, such User Agreements shall prevail.</p>

<p><strong>UPDATES TO THE PRIVACY POLICY</strong></p>

<p>We keep our Privacy Policy under regular review and may update this privacy policy to reflect changes to our information practices. If we make material changes to our Privacy Policy, we will notify you by email (sent to the e-mail address specified in your account) or post the changes on all the Foxsky websites or through our mobile devices, so that you may be aware of the information we collect and how we use it. Such changes to our Privacy Policy shall apply from the effective date as set out in the notice or on the website. We encourage you to periodically review this page for the latest information on our privacy practices. Your continued use of products and services on the websites, mobile phones and/or any other device will be taken as acceptance of the updated Privacy Policy. We will seek your fresh consent before we collect more personal information from you or when we wish to use or disclose your personal information for new purposes.</p>

<p><strong>DO I HAVE TO AGREE TO ANY THIRD PARTY TERMS AND CONDITIONS?</strong></p>

<p>Our Privacy Policy does not apply to products and services offered by a third party. Foxsky products and services may include third parties’ products, services and links to third parties’ websites. When you use such products or services, they may collect your information too. For this reason, we strongly suggest that you read the third party’s privacy policy as you have taken time to read ours. We are not responsible for and cannot control how third parties use personal information which they collect from you. Our Privacy Policy does not apply to other sites linked from our services.</p>

<p><strong>Here are third party terms and privacy policies that apply when you use these specific products:</strong></p>
<ul>
<li><p>By using PayPal or other third party check-out services to finalize and pay for your order, you are agreeing to the third party check-out service provider’s privacy policy will apply to the information you provide on their website.</p></li>

<li><p>By using the Virus Scan feature in the FOXSKYUI Security Center, you are agreeing to one of the following three terms based on your choice of service.</p></li>

<li><p>Avast Privacy and Information Security Policy: https://www.avast.com/privacy-policy</p></li>

<li><p>License Agreement for AVL SDK for Mobile: http://co.avlsec.com/License.en.html?l=en</p></li>

<li><p>Tencent’s Terms of Service: http://wesecure.qq.com/termsofservice.jsp</p></li>

<li><p>By using the Cleaner feature in FOXSKYUI’s Security Center, you are agreeing to one of the following two terms based on your choice of service.</p></li>

<li><p>Cheetah Mobile’s Privacy Policy: http://www.cmcm.com/protocol/cleanmaster/privacy-for-sdk.html</p></li>

<li><p>Tencent’s Terms of Service: http://wesecure.qq.com/termsofservice.jsp</p></li>

<li><p>By using the advertising services in several specific applications in FOXSKYUI, you are agreeing to one of the following two terms based on your choice of service.</p></li>

<li><p>Google’s Privacy Policy: https://policies.google.com/</p></li>

<li><p>Facebook’ Privacy Policy: https://www.facebook.com/about/privacy/update?ref=old_policy</p></li>

<li><p>By using the Google Input Method, you are agreeing to Google’s terms: http://www.google.com/policies/privacy</p></li>

<li><p>By using the SwiftKey Input Method, you are agreeing to SwiftKey’s terms: http://swiftkey.com/en/privacy</p></li>
</ul>
<p><strong>SOCIAL MEDIA (FEATURES) AND WIDGETS</strong></p>

<p>Our websites include social media features, such as the Facebook Like button and Widgets, such as the Share this button or interactive foxskyni-programs that run on our site. These features may collect your IP address, which page you are visiting on our site, and may set a cookie to enable the Feature to function properly. Social media features and Widgets are either hosted by a third party or hosted directly on our websites. Your interactions with these Features are governed by the privacy policy of the company providing it.</p>

<p><strong>SINGLE SIGN-ON</strong></p>

<p>Depending on your jurisdiction, you may be able to log in to our website using sign-on services such as Facebook Connect or an Open ID provider. These services will authenticate your identity, provide you the option to share certain personal information (such as your name and email address) with us, and to pre-populate our sign up form. Services like Facebook Connect give you the option to post information about your activities on this website to your profile page to share with others within your network.</p>

<p><strong>ABOUT OUR SYSTEMATIC APPROACH TO MANAGE YOUR PERSONAL INFORMATION</strong></p>

<p>If you are Europe Union user under GDPR, Foxsky will provide systematic approach to manage personal data deeply engages our people, management processes and information systems by applying a risk management methodology. According to the GDPR, for instance, (1) Foxsky set up a Data Protection Officer (DPO) in charge the data protection, and the contact of DPO is dpo@foxsky.com; (2) procedure like data protection impact assessment (DPIA).</p></p>
            </div>
        </div>
    </div>
</div>


<!-- <div class="wrapper about-2">
	<div class="container">
         <div class="about_main text-center">
              <h3 class="h_02">MAKING QUALITY TECHNOLOGY <br>ACCESSIBLE TO EVERYONE.</h3>
              <p class="p_02">Xiaomi was founded in 2010 by serial entrepreneur Lei Jun based on the vision “innovation for everyone”. <br>We believe that high-quality products built with cutting-edge technology should be made accessible to everyone. <br>We&nbsp;create remarkable hardware, software, and Internet services for and with the help of our Mi fans.&nbsp;We incorporate <br>their feedback into our product range, which currently includes Mi and Redmi smartphones, Mi TVs and set-top boxes, <br>Mi routers, and Mi Ecosystem products including smart home products, wearables and other&nbsp;accessories.&nbsp;<br>With presence in over 30 countries and regions, Xiaomi is expanding its footprint across the world to <br>become a global brand.&nbsp;<br></p>
              
              <span class="line-before"></span>
              
              <div class="review_share_like about-social">
                   <a href="#"><i class="fa fa-facebook"></i></a>
                   <a href="#"><i class="fa fa-twitter"></i></a>
                   
                            
              </div>
              
         </div>
       
    </div>
</div>


<div class="wrapper about-3">
	<div class="container">
		<h3 class="h_03">MAKING QUALITY TECHNOLOGY<br>ACCESSIBLE TO EVERYONE.</h3>
    </div>
</div>


<div class="wrapper about-4">
	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team1.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team2.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team3.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team4.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team5.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
        
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team6.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    
</div>
         

<div class="wrapper about-5">
	<div class="container">
    	<h3 class="h_05">OUR CULTURE</h3>
        <p class="p_05">"Just for fans" – that's our belief. Our hardcore Mi fans lead every step of the way. In fact, many
Xiaomi employees were first Mi fans before joining the team. As a team, we share the same relentless 
pursuit of perfection, constantly refining and enhancing our products to create the best user experience 
possible. We are also fearless in testing new ideas and pushing our own boundaries. Our dedication 
and belief in innovation, together with the support of Mi fans, are the driving forces behind our 
unique Mi products.</p>
    </div>
</div>


<div class="wrapper about-6">
     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="row">
             <div class="team_profile">
                 <img src="assets/images/about/about_1.jpg" alt="">
                 <h4 class="h_06">OFFICE ENVIRONMENT</h4>
                 <p class="p_06">We are incredibly flat, open, and innovative. No never-ending meetings. No lengthy processes. <br> We provide a friendly and collaborative environment where creativity is encouraged to flourish.</p>
             </div>
         </div>
    </div>
    
    
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="row">
             <div class="team_profile">
                 <img src="assets/images/about/about_2.jpg" alt="">
                 <h4 class="h_06">OFFICE ENVIRONMENT</h4>
                 <p class="p_06">We are incredibly flat, open, and innovative. No never-ending meetings. No lengthy processes. <br> We provide a friendly and collaborative environment where creativity is encouraged to flourish.</p>
             </div>
         </div>
    </div>
    
</div>



<div class="wrapper about-7">
	<div class="container">
         <div class="about_main text-center">
              <h3 class="h_02">CAREERS</h3>
              <p class="p_02">Xiaomi was founded in 2010 by serial entrepreneur Lei Jun based on the vision “innovation for everyone”. <br>We believe that high-quality products built with cutting-edge technology should be made accessible to everyone. <br>We&nbsp;create remarkable hardware, software, and Internet services for and with the help of our Mi fans.&nbsp;We incorporate <br>their feedback into our product range, which currently includes Mi and Redmi smartphones, Mi TVs and set-top boxes, <br>Mi routers, and Mi Ecosystem products including smart home products, wearables and other&nbsp;accessories.&nbsp;<br>With presence in over 30 countries and regions, Xiaomi is expanding its footprint across the world to <br>become a global brand.&nbsp;<br></p>
              
              <span class="line-before"></span>
              
              
              
         </div>
       
    </div>
</div>
 -->


<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>
<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			$(".navigation > ul > li > a").hover(function(){
				//alert("ddd");
				if($(this).next(".product_item_view").length > 0){
				$(".logo_row").addClass("show_nav");
				$(this).next(".product_item_view").css({"display":"block"});
				}
				else{
					$(".logo_row").removeClass("show_nav");
					$(".product_item_view").css({"display":"none"});
				}
			});
			$(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
				$(".product_item_view").css({"display":"none"});
			});
		});
		</script>
         <script src="assets/js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    
 	<script src="assets/js/jquery.bxslider.min.js"></script>
	   <script>
        $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 330,
        slideMargin: 0,
        ticker: true,
        speed: 30000
    });
       </script>
 
