

<div class="wrapper sub_nav_bg breadcrumbs">
	<div class="container-fluid">
    	<div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
            	<a href="#">Home</a><img src="<?php echo base_url();  ?>assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
                My account
            </div>
        </div>
	</div>  	
</div>

<div class="wrapper my_account">
	<div class="container-fluid">
    	<div class="row">
        	<div class="my_acount_main">
            	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                	<div class="my_nav">
                    
                    	<h3 class="my_title">ORDERS</h3>
                        	<ul class="order_menu">
                            	<li><a href="<?php echo base_url();?>myorder">My Orders</a></li>
                                <!-- <li><a href="#">Returns</a></li> -->
                            </ul>
                       
                        <h3 class="my_title">Foxsky</h3>
                        	<ul class="order_menu">
                            	<li><a href="#" class="current_active">My Account</a></li>
                                <!-- <li><a href="#">Coupons</a></li>
                                <li><a href="#">Gift Card</a></li>
                                <li><a href="#">F-code</a></li> -->
                                <li><a href="<?php echo base_url();?>myreview">Reviews</a></li>
                                <li><a href="<?php echo base_url();?>myaddress">Address Book</a></li>
                                <!-- <li><a href="#">Messages</a></li>
                                <li><a href="#">Notifications</a></li> -->
                                
                            </ul>
                            
                            <!-- <h3 class="my_title">SERVICES</h3>
                        	<ul class="order_menu">
                            	<li><a href="#">Foxsky Exchange</a></li>
                                <li><a href="#">Foxsky Protect</a></li>
                                <li><a href="#">Support</a></li> -->
                                
                            </ul>
                            
                            
                        
                        
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no_padding">
                  
                	<div class="my_right">
                        <?php
                          if($this->session->flashdata('message')){ 
                            echo $this->session->flashdata('message'); 
                         }
                        ?>
                    	<div class="user-account-summary">
                        	<div class="user-head">
              					<div class="pic_profile"><img src="<?php echo base_url();  ?>assets/profileimages/<?php echo $profile->userimage;?>" alt=""></div>
          					</div>
                            <div class="user-name"><?php echo $profile->Name;?></div>
                            <!-- <div class="user-social-connect"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></div> -->
                            <!-- <div class="user-messages"><a href="#"><span>0</span></a></div> -->
                            <div class="user-settings"><a href="<?php echo base_url();?>myprofile"></a></div>
                        </div>
                    </div>
                    <?php echo $count->Orderid;?>
                    <div class="myright_col">
                    	<div class="user-account-entry">
                        <div class="icon_after_use"></div>
                        	<h3>My Orders </h3>
                            <div class="fast-links">
                            <a href="<?php echo base_url();?>myorder?id=shipping">Pending (<?php echo $pcount->pendingcount;?>) </a>
                            <a href="<?php echo base_url();?>myorder?id=closed">Delivered (<?php echo $dcount->delivercount;?>) </a>
                            </div>
                        </div>
                        
                        <div class="user-account-entry">
                        <div class="icon_after_use2"></div>
                        	<h3>My Reviews</h3>
                            <div class="fast-links">
                            <a href="<?php echo base_url();?>myreview">Reviews (<?php  echo $rcount->reviewcount;?>)</a>
                            </div>
                        </div>
                        
                        <div class="user-account-entry">
                        <div class="icon_after_use3"></div>
                        	<h3>Address Book</h3>
                            <div class="fast-links">
                            <a href="<?php echo base_url();?>myaddress">Addresses currently in use (<?php echo $sadd->totcount;?>)</a>
                            </div>
                        </div>
                        
                        
                        <div class="user-account-entry">
                        <div class="icon_after_use4"></div>
                        	<h3>Address Book</h3>
                            <div class="fast-links">
                            <a href="<?php echo base_url();?>myaddress">Addresses currently in use (<?php echo $sadd->totcount;?>)</a>
                            </div>
                        </div>
                        
                        
                    </div>  
                    
                                     
                    
                </div>
            </div>
        </div>
    </div>
</div>




<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>