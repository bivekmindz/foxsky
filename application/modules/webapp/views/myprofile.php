<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Profile</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/app_css.css">
        <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
        
</head>
<body> -->

 <div class="container">
   		<div class="row">
            <div class="profile_main">
            <div class="profile_header">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                	<div class="profile-logo"><img src="assets/images/logo.png" alt=""></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                	<a href="<?php echo base_url();?>logout" class="signout">Sign out</a>
                </div>
             </div>
             
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="n-main-nav">
                    <li><a href="<?php echo base_url();?>security">Security</a></li>
                    <li><a href="My-Profile.html" class="active-orage">Personal info</a>
                    <div class="arrow_box"></div>
                    </li>
                </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               	 	<div class="n-account-area-box">
                    	<div class="n-account-area">
                        	<div class="na-info">
								<p class="na-name">Set nickname</p>
								<p class="na-num"><?php echo $profile->Name;?></p>
		  					</div>
                            <div class="n_account_pic"><img src="<?php echo base_url();  ?>assets/profileimages/<?php echo $profile->userimage;?>" alt=""></div>
                        </div>
                	</div>
                </div>
                
                <div class="n_frame">
                	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    	<div class="na-img-area">
                        	<a href="#" data-toggle="modal" data-target="#set_photo">
                            <div class="user_img_profile">
                            	<div class="pic_profile"><img src="<?php echo base_url();  ?>assets/profileimages/<?php echo $profile->userimage;?>" alt=""></div>
                                <span class="set_photo">Set Photo</span>
                           		<span class="set_edit"><img src="assets/images/n-ava-edit.png" alt=""></span>

                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    	<div class="framedatabox">
                        	<div class="fdata">
                                <h3>Personal info</h3>
                                <a href="#" data-toggle="modal" data-target="#Edit_profile" class="pull-right"><img src="assets/images/download.png" alt=""> Change</a>
                        	</div>
                            <div class="fdata">
                                <p><span>Name :</span> <span style="color: #999;"><?php echo $profile->Name;?></span></p>
                        	</div>
                            <div class="fdata">
                                <p><span>Gender :</span> <span style="color: #999;"><?php echo $profile->gender;?></span></p>
                        	</div>
                        </div>
                        
                        <div class="framedatabox">
                        	<div class="fdata">
                                <h3>Advanced settings</h3>
                        	</div>
                            <div class="fdata">
                                <p><span>Location :</span> <span style="color: #999;">India</span></p>
                        	</div>
                        </div>
                    </div>
                </div>
           </div>
            	
           <div class="policy-area">
                	<ul class="policy-select-list">
                    	<!-- <li><a href="#">English</a></li>
                        <li><a href="#">|</a></li> -->
                        <li><a href="<?php echo base_url();?>useraggree">User Agreement</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="<?php echo base_url();?>privacy">Privacy Policy</a></li>
                    </ul>
                    <p class="reserved-intro">Foxsky Inc., All rights reserved</p>
                </div>
        </div>
   </div> 
   
<div class="container"> 
  <div class="modal" id="Edit_profile" role="dialog">
    <div class="modal-dialog modal_dialog02">
    
      <!-- Modal content-->
      <div class="modal-content modal_content02">
        <div class="modal-header modal_header02">Edit personal info
          <button type="button" class="close close02" data-dismiss="modal">&times;</button>
        </div>
       <form method="post" action=""> 
        <div class="modal-body modal_body02">
          <div class="Editpersanal">
          
          	<div class="form-group">
            	<div class="col-lg-3"><span class="name_nic">Name:</span></div>
                <div class="col-lg-9"><input type="text" class="input-verification" required value="<?php echo $profile->Name;?>" name="name"></div> 
            </div>
            
            <div class="form-group">
            	<div class="col-lg-3"><span class="name_nic1" name="gender">Gender:</span></div>
                	<div class="col-lg-9">
                    <div class="male_radio">
                		<label class="checkbox-stock6">Male
                        <input type="radio" name="gender" value="M" <?php if($profile->gender=='M') echo 'checked';?>>
                        <span class="checkmark-6"></span> 

                        </label>
                    </div>
                    
                    <div class="female_radio">
                		<label class="checkbox-stock6">Female
                        <input type="radio" name="gender" value="F" <?php if($profile->gender=='F') echo 'checked';?>>
                        <span class="checkmark-6"></span>
                        </label>
                    </div>
               	</div> 
            </div>
            
         </div>
                        
            <input type="submit" name="submit" class="nb_submit font-14" value="Save" /> 
            <button type="button" class="nb_submit gray_border font-14" data-dismiss="modal">Close</button>
            
            <div class="Privacy_Policy">
            	<a href="<?php echo base_url();?>privacy">Privacy Policy</a>
            </div>
                    
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="container"> 
  <div class="modal" id="set_photo" role="dialog">
    <div class="modal-dialog modal_dialog02">
    
      <!-- Modal content-->
      <div class="modal-content modal_content02">
        <div class="modal-header modal_header02">Set photo
          <button type="button" class="close close02" data-dismiss="modal">&times;</button>
        </div>
        <form method="post" action="" enctype="multipart/form-data"> 
        <div class="modal-body modal_body02">
          <div class="Editpersanal">
          	<div class="form-group">
            	<div class="col-lg-12"><span class="new_passwrod">Choose a photo</span></div>
                <div class="col-lg-12">

                <div class="uplode_img">
					
                    <div class="tbl_input"><input id="image" name="image" type="file" /></div>
                    <!-- <input type="button" class="btn_tip btn_commom" value="Browse">
            		<input class="uplodefile" name="image" type="file"> -->
          		</div>
                
                <p class="png_format">Image size can't exceed 2MB and must be in PNG or JPG format.</p>
                
                </div> 
            </div>
          </div>
            
            <input type="submit" name="submitpic" class="nb_submit font-14" value="Submit" />
            <button type="button" class="nb_submit gray_border font-14" data-dismiss="modal">Cancle</button>
            
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

		<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/wow.min.js"></script>
  		<script>new WOW().init();</script>
<!--    
</body>
</html>