<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Foxsky India</title>
		<link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/detail-slider1.css">
		<link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/detail-slider2.css">
		<link rel="stylesheet" href="<?php echo base_url();  ?>assets/webapp/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();  ?>assets/webapp/css/bootstrap-select.min.css">
		<link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/app_css.css">
        <link href="<?php echo base_url();  ?>assets/webapp/fontawsome/css/font-awesome.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/animate.min.css">
        <link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/responsive.css">
        
	</head>