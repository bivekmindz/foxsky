<body>
<div class="wrapper top_link">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<ul class="top_link_left">
							<li><a href="#">Foxsky India</a></li>
							<li><a href="#">Foxsky Community</a></li>
							<li><a href="#">Download Foxsky store app</a></li>
						</ul>
					</div>
					<?php  
						$param =  $this->cart->contents(); 
					    $total = count($param); 
			        ?> 
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="cart_entry">
                            <a href="<?php echo base_url();?>cart">
	                        	<i class="fa fa-shopping-cart"></i> 
	                        	<span class="cart_txt">Items</span>
	                            <span class="cart_number">( <?php echo $total; ?> )</span>
                            </a>
                            
                            <div class="cart_over">
                            	<div class="mini-cart-item">
                                	<div class="mini-cart-pic"><img src="<?php echo base_url();?>assets/images/air2.jpg" alt=""></div>
                                		<div class="mini-cart-txt">
                                			<a href="#">Foxsky Mix 2 Black 6GB+128GB</a>
                                    		<span><img src="<?php echo base_url();?>assets/images/rupees-orange.png" alt=""> 32,999 × 1</span>
                                            <div class="close-cart-icon"><img src="<?php echo base_url();?>assets/images/close_cart.png" alt=""></div>
                                	</div>
                                </div>
                                
                                
                                <div class="mini-cart-item">
                                	<div class="mini-cart-pic"><img src="<?php echo base_url();?>assets/images/air2.jpg" alt=""></div>
                                		<div class="mini-cart-txt">
                                			<a href="#">Foxsky Mix 2 Black 6GB+128GB</a>
                                    		<span><img src="<?php echo base_url();?>assets/images/rupees-orange.png" alt=""> 32,999 × 1</span>
                                            <div class="close-cart-icon"><img src="<?php echo base_url();?>assets/images/close_cart.png" alt=""></div>
                                	</div>
                                </div>
                                
                                
                                <div class="mini-cart-summary-info">
                                	<div class="summary_total_h">
                                    	<h5>Subtotal <span>( 3 items )</span></h5>
                                        <div class="summary_price_h"><img src="<?php echo base_url();?>assets/images/rupees-orange.png" alt=""><span>38,997</span></div>
                                    </div>
                                    <a href="#" class="checkout_btn_cart">Checkout</a>
                                </div>
                            </div>
                        </div> 
						<ul class="top_link_right">
						<!-- 	<li><a href="<?php echo base_url(); ?>cart">	
								<?php  $param =  $this->cart->contents(); 
									   $total = count($param); 
			                     ?> 
			                     <?php echo $total; ?> item(s)</a>
			                     <span class="displayincarttotal" id="cratqty">
								  0 item(s)</span>
			                </li>  -->
		                <li>
							<a href="<?php echo base_url('logout');?>">
								Logout
								<i class="fa fa-sign-out" aria-hidden="true"></i>
							</a>
						</li>
                    	<li>
							<a href="<?php echo base_url('myaccount');?>" class="m_popup">
								Hi <?php echo ucfirst($memname);?>
								<i class="fa fa-unlock-alt fa-lg"></i>
							</a>
						</li>
							
						</ul>
							<!-- <li><a href="#">Sign up</a></li>
							<li><a href="#">Sign in</a></li>
						</ul> -->
					</div>
				</div>
			</div>
		</div>
		
		<!-- navigation bar and foxsky logo -->
		<div class="wrapper logo_row">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					  <div class="logo">
					  	<a href="<?php echo base_url();   ?>">
					  		<img src="<?php echo base_url();?>assets/webapp/images/logo.png" alt="">
					  	</a>
					  </div>
		     		</div>
					<?php  $this->load->view('helper/nav');   ?>
				</div>
	        </div>
		</div>
        