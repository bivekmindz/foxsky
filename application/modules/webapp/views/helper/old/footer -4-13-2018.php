<script src="<?php echo js_url('myothermain_extra.js');?>" type="text/javascript"></script> 

       <div class="footer">
       <div class="footer-ribbon">
							<span>Get in Touch</span>
						</div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <h1 class="f_heading_1">foxsky</h1>
                                <ul class="foot_1">
                                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                    <li><a href="<?php echo base_url('aboutus'); ?>">About Us</a></li>
                                    <li><a href="<?php echo base_url('privacy_policy'); ?>">Privacy Policy</a></li>
                                    <li><a href="<?php echo base_url('feedback'); ?>">FeedBack</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <h1 class="f_heading_1">PAYMENT OPTIONS</h1>
                                <div class="pay_img">
                                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>images/visa-logo.png"></a>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>images/master-logo.png"></a>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url();?>images/mastero-logo.png"></a>
                                    </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <h1 class="f_heading_1">Social</h1>
                                 <p class="em-social" style="margin: 5px -5px 0;">
                                    <a class="em-social-icon em-facebook f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-twitter f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-pinterest  f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-google f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    
                                </p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <h1 class="f_heading_1">News Letter</h1>
                                <div class="block block-subscribe">
                                    <form action="" method="post">
                                        <div class="block-content">
                                            <div class="form-subscribe-content">
                                                <div class="input-box">
                                                    <input type="text" name="email" title="Sign up for our newsletter" placeholder="Sign up for our newsletter" id="newsemail" class="input-text required-entry validate-email">
                                                </div>
                                                <p id="returnmsgNew" style="width: 100%;float: left;    color: #fff;"></p>
                                                <div class="actions"> <button type="submit" title="Subscribe" class="button" id="newslettersubmit"><span><span>Subscribe</span></span></button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                               
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer_bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="copyright">Copyright © 2017  Foxsky. All Rights Reserved.</span>
                            </div>
                        </div>
                    </div>
                </div>                 
                  <div class="login_pop" id="login">    
               <?php 
                $this->load->view("login.php");
               ?>    
               </div> 
               <div class="login_pop" id="sign_up">
                 <?php 
                #$this->load->view("register.php");
               ?>  
               </div> 
                <div class="login_pop" id="forgot_pwd">    
               <?php 
                $this->load->view("forgotpwd.php");
               ?>    
               </div>  
<div class="overlay"></div>
    <div class="overlay_n"></div>

<script type="text/javascript">
    $(document).on('click','#newslettersubmit', function(event) {

   var newsemail=$.trim($('#newsemail').val());
   
   var emailReg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   if(newsemail==''){
    $('#newsemail').css('border', '1px solid red');
    $("#returnmsgNew").html("Enter Email!");
    return false;
   }
   else if( !emailReg.test(newsemail)) {
    $('#newsemail').css('border', '1px solid red');
    $("#returnmsgNew").html("Enter valid Email!");
    return false;
   }
   else{
    $('#newsemail').css('border', '1px solid green');
    $("#returnmsgNew").html("");

   }
    $.ajax({
      url: "<?php echo base_url();?>webapp/newsletter",
      type: 'POST',
      data: {newsemails:newsemail},
      success:function(data){
            //$('#show_d').html(data.trim());
            //return false;
            //alert(data);
            //console.log(data);return false;
            if(data=='Newletter Subscription Successfull.'){
              $('#returnmsgNew').html(data);
              return false;
            } else {
              $('#returnmsgNew').html(data);
              
              return false;
            }
          }
    });
    
    $('#returnmsgNew').html('Please Wait...');
    return false;
});

</script>                        
                        
<!--zoom effect-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/webapp/js/jquery.simpleGallery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/webapp/js/jquery.simpleLens.js"></script>
<script type="text/javascript">
  $(".r_mor").click(function(){
  $(".discla span").toggleClass("span_op");
  $(this).toggleClass("hide_1");
  });
</script>
<script>
    $(document).ready(function(){
        $('#demo-1 .simpleLens-thumbnails-container img').simpleGallery({
            loading_image: 'assets/webapp/demo/images/loading.gif'
        });

        $('#demo-1 .simpleLens-big-image').simpleLens({
            loading_image: 'assets/webapp/demo/images/loading.gif'
        });
    });
</script>

<script>
    $(document).ready(function(){
        $('#demo-2 .simpleLens-thumbnails-container img').simpleGallery({
            loading_image: 'assets/webapp/demo/images/loading.gif',
            show_event: 'click'
        });

        $('#demo-2 .simpleLens-big-image').simpleLens({
            loading_image: 'assets/webapp/demo/images/loading.gif',
            open_lens_event: 'click'
        });
        $(".m_popup").click(function () {
            var show = $(this).attr("data-sh");
            var hide = $(this).attr("data-hi");
            $("#" + show).addClass("active_pop");
            $("#" + hide).removeClass("active_pop");
            $(".overlay").fadeIn();
        });
        $(".close_sign").click(function () {
            $(this).parent().removeClass("active_pop");
            $(".overlay").fadeOut();
        });
        $(".overlay").click(function () {
            $(".login_pop").removeClass("active_pop");
            $(this).fadeOut();
        });

    });
</script>
<!--zoom end-->


    <script src="<?php echo base_url(); ?>assets/js/jquery.visible.js" ></script>
     <script type="text/javascript">
     $(document)

      $(function(){

        // Add the spans to the container element.
        //$('#container dt').each(function(){ $(this).append('<span></span>'); });


        // Trigger the
        $(window).on('scroll',function(){
            
          // Select the detection type.
            var detectPartial = $('#detect_type').val("complete") == 'partial';

          // Loop over each container, and check if it's visible.
            $('.item_div .item_n').each(function () {

            // Is this element visible onscreen?
            var visible = $(this).visible( detectPartial );
            var tname = $(this).attr("title");
          
            // Set the visible status into the span.
              //$(this).find('span').text( visible ? 'Onscreen' : 'Offscreen' ).toggleClass('visible',visible);
            $("." + tname +" a").toggleClass('visible', visible);

        
        
          });
             // if($(".stick_nav_vis").visible( detectPartial )){
                  if($(".st_close").visible( detectPartial )){
            $(".stick_nav").hide();
             
          }
               else if($(".s0").visible( detectPartial )){
            $(".stick_nav").stop().show();
             
          }

            else if($(".s9").visible( detectPartial )){
           $(".stick_nav").hide();
             
          }
          else if($(".s8").visible( detectPartial )){
           $(".stick_nav").show();
             
          }
           
       
            
         
             
        });

      });
    </script>
     
 <script type="text/javascript" src="<?php echo base_url();?>assets/webapp/js/animatescroll.min.js"></script>
 
  <!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/bootstrap.min.css"/>

<!-- 
    RTL version
-->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/alertify.rtl.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/default.rtl.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.0/build/css/themes/semantic.rtl.min.css"/>
<!-- Bootstrap theme -->
</body>
</html>
