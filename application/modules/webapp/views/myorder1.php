<div class="wrapper sub_nav_bg breadcrumbs">
	<div class="container-fluid">
   <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
       <a href="#">Home</a><img src="<?php echo base_url();  ?>assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
      
       My order
     </div>
   </div>
 </div>  	
</div>

<div class="wrapper my_account">
	<div class="container-fluid">
   <div class="row">
     <div class="my_acount_main">
       <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
         <div class="my_nav">

           <h3 class="my_title">ORDERS</h3>
           <ul class="order_menu">
             <li><a href="#" class="current_active">My Orders</a></li>
             <li><a href="#">Returns</a></li>
           </ul>

           <h3 class="my_title">Foxsky</h3>
           <ul class="order_menu">
             <li><a href="<?php echo base_url();?>myaccount">My Account</a></li>
             <li><a href="#">Reviews</a></li>
             <li><a href="<?php echo base_url();?>myaddress">Address Book</a></li>
           </ul>
         </div>
       </div>
       <?php  if($_GET['id']=='closed'){
          $closedli="active_review";
          $closeddata ="active_review_tab";
       }

       elseif ($_GET['id']=='return') {
         $returnli="active_review";
         $returndata="active_review_tab";

       }

       elseif ($_GET['id']=='shipping') {
         $shippingli="active_review";
         $shippingdata="active_review_tab";

       }
    else{
      $allli="active_review";
      $alldata="active_review_tab";
    }


       ?>

       <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no_padding">
         <div class="my_right">
          <h1>My Orders</h1>

          <div class="order_nav_tab">
           <ul class="order-list-range order_tab">
            <li onclick="hidden();"><a href="javascript:void(0);" id="pInfo" title="all" class="<?php echo $allli; ?>">ALL</a></li>
            <li onclick="hidden();"><a href="javascript:void(0);" id="firstphoto" title="awaiting_payment" class="<?php echo $awaiting_payment; ?>">Return</a></li>
            <li onclick="hidden();" ><a href="javascript:void(0);" id="firstphoto" title="shipping" class="<?php echo $shippingli; ?>">Shipping</a></li>
            <li><a href="javascript:void(0);" id="firstphoto" title="closed" class="<?php echo $closedli; ?>">Closed</a></li>
          </ul>
        </div>



        <div class="order_box_tab in <?php echo $alldata; ?>" id="all">
          <?php foreach($prodetails as $order=>$customerorder){ ?>
          <div class="order_container">
           <div class="order_cont_title">
             <h2>Waiting for payment</h2>
             <p>*Please complete the payment within 2 minutes 22 seconds . Unpaid orders (except for COD ones) will be cancelled automatically afterwards.</p>
           </div>

           <div class="order_cont_list">
             <ul class="order_list_left">
             	<li><?php echo $customerorder->OrderDate;?></li>
              <li><?php echo $customerorder->ShippingName;?></li>
              <li>Order number : <a href="#"><?php echo $customerorder->OrderNumber;?></a></li> 
            </ul>

            <div class="order_list_right">
             Total <img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><span><?php echo $customerorder->FinalPrice;?></span>
           </div>

           <div class="order-unit-goods-info">
            <div class="order_list_one">
              <div class="col-lg-9">
                <div class="row">
                 <div class="or_left">
                  <div class="col-lg-2"><div class="order_picture"><img src="<?php echo base_url(); ?>assets/images/phone1.jpg" alt=""></div></div>
                  <div class="col-lg-9">
                    <p class="p7"><?php echo $customerorder->ProName;?></p>
                    <p class="p7"><img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><?php echo $customerorder->ProPrice;?>X<?php echo $customerorder->OrdQty;?></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-3">
             <div class="row">
               <a href="<?php echo base_url();?>orderdetails/<?php echo base64_encode($customerorder->OrderId);?>" class="btn_paynow1">ORDER DETAILS</a>
             </div>
           </div>

         </div>
       </div>
     </div>
   </div>
   <?php } ?>
 </div>



 <div class="order_box_tab " id="awaiting_payment">
 <?php foreach($return as $order=>$returned){ ?>
   <div class="order_container">
		<div class="order_cont_title">
			<h2>Waiting for payment</h2>
		    <p>*Please complete the payment within 2 minutes 22 seconds . Unpaid orders (except for COD ones) will be cancelled automatically afterwards.</p>
		</div>

		<div class="order_cont_list">
			<ul class="order_list_left">
		    	<li><?php echo $returned->OrderDate;?></li>
		        <li><?php echo $returned->ShippingName;?></li>
		        <li>Order number : <a href="#"><?php echo $returned->OrderNumber;?></a></li>
		    </ul>
		    
		    <div class="order_list_right">
		    	Total <img src="assets/images/rupees-gay.png" alt=""><span><?php echo $returned->FinalPrice;?></span>
		    </div>
		    
		    <div class="order-unit-goods-info">
		    <div class="order_list_one">
		    <div class="col-lg-9">
		    <div class="row">
		    
		     <div class="or_left">
		     	<div class="col-lg-2"><div class="order_picture"><img src="assets/images/phone1.jpg" alt=""></div></div>
		    	<div class="col-lg-9">
		    		<p class="p7"><?php echo $returned->ProName;?></p>
		    		<p class="p7"><img src="assets/images/rupees-gay.png" alt=""><?php echo $returned->ProPrice;?>X<?php echo $returned->OrdQty;?></p>
		    	</div>
		     </div>
		    <!--  <div class="or_left">
		     	<div class="col-lg-2"><div class="order_picture"><img src="assets/images/phone1.jpg" alt=""></div></div>
		    	<div class="col-lg-9">
		    		<p class="p7">India Standard Charger 5V 2A Black</p>
		    		<p class="p7"><img src="assets/images/rupees-gay.png" alt="">349 × 2</p>
		    	</div>
		     </div> -->
		     
		     </div>
		    </div>
                                
                <div class="col-lg-3">
		       <div class="row">
		         <!-- <a href="#" class="btn_paynow">Pay Now</a> -->
		         <a href="<?php echo base_url();?>orderdetails/<?php echo base64_encode($returned->OrderId);?>" class="btn_paynow1">ORDER DETAILS</a>
		       </div>
       			</div>
        </div>
        </div>
                            
                            
       <?php } ?>                     
                            
    </div>
 </div>
 <div class="order_box_tab <?php echo $shippingdata; ?>" id="shipping">
  <!-- <div class="order_box_tab active_review_tab in" id="all"> -->
    <?php foreach($shipping as $order=>$shipped){ 
    	// p($shipped);exit();
    	?>
    <div class="order_container">
     <div class="order_cont_title">
       <h2>Waiting for payment</h2>
       <p>*Please complete the payment within 2 minutes 22 seconds . Unpaid orders (except for COD ones) will be cancelled automatically afterwards.</p>
     </div>

     <div class="order_cont_list">
       <ul class="order_list_left">
        <li><?php echo $shipped->OrderDate;?></li>
        <li><?php echo $shipped->ShippingName;?></li>
        <li>Order number : <a href="#"><?php echo $shipped->OrderNumber;?></a></li> 
      </ul>

      <div class="order_list_right">
       Total <img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><span><?php echo $shipped->FinalPrice;?></span>
     </div>

     <div class="order-unit-goods-info">
      <div class="order_list_one">
        <div class="col-lg-9">
          <div class="row">

           <div class="or_left">
            <div class="col-lg-2"><div class="order_picture"><img src="<?php echo base_url(); ?>assets/images/phone1.jpg" alt=""></div></div>
            <div class="col-lg-9">
              <p class="p7"><?php echo $shipped->ProName;?></p>
              <p class="p7"><img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><?php echo $shipped->ProPrice;?>X<?php echo $shipped->OrdQty;?></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3">
       <div class="row">
         <!-- <a href="#" class="btn_paynow">Pay Now</a> -->
         <a href="<?php echo base_url();?>orderdetails/<?php echo base64_encode($shipped->OrderId);?>" class="btn_paynow1">ORDER DETAILS</a>
       </div>
     </div>

   </div>
 </div>
</div>
</div>
<?php } ?>
</div>
<!-- </div> -->

<div class="order_box_tab <?php echo $closeddata; ?>" id="closed">
<?php foreach($delivered as $order=>$delivered){ ?>
 <div class="order_container">
   <div class="order_cont_title">
     <h2>Closed</h2>
   </div>

   <div class="order_cont_list">
     <ul class="order_list_left">
       <li><?php echo $delivered->OrderDate;?></li>
       <li><?php echo $delivered->ShippingName;?></li>
       <li>Order number : <a href="#"><?php echo $delivered->OrderNumber;?></a></li>
     </ul>

     <div class="order_list_right">
       Total <img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><span><?php echo $delivered->FinalPrice;?></span>
     </div>

     <div class="order-unit-goods-info">
      <div class="order_list_one">
        <div class="col-lg-9">
          <div class="row">

           <div class="or_left">
            <div class="col-lg-2"><div class="order_picture"><img src="<?php echo base_url(); ?>assets/images/phone1.jpg" alt=""></div></div>
            <div class="col-lg-9">
              <p class="p7"><?php echo $delivered->ProName;?></p>
              <p class="p7"><img src="<?php echo base_url();  ?>assets/images/rupees-gay.png" alt=""><?php echo $delivered->ProPrice;?>X<?php echo $delivered->OrdQty;?></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3">
       <div class="row">
      	 <a href="<?php echo base_url();?>orderdetails/<?php echo base64_encode($delivered->OrderId);?>" class="btn_paynow1">ORDER DETAILS</a>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="wrapper">
 <div class="policies_white">
  <div class="container-fluid">
   <div class="row">
    <div class="footer_policies">
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
        <div class="service-policy">
          <a href="#">
           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy1.jpg" alt=""></div>
           <div class="policy_txt">
            <strong>Hassle-free replacement</strong>
            <br>
            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
          </div>
        </a>
      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
      <div class="service-policy pull_space">
        <a href="#">
         <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy2.jpg" alt=""></div>
         <div class="policy_txt">
          <strong>100% secure payments</strong>
          <br>
          <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
        </div>

      </a>
    </div>
  </div>

  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
    <div class="service-policy pull_right">
      <a href="#">
       <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy3.jpg" alt=""></div>
       <div class="policy_txt">
        <strong>Vast service network</strong>
        <br>
        <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
      </div>
    </a>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>

