<?php $data['cart'] = $this->cart->contents();?>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
           <script type="text/javascript">

function yesnoCheck() {


    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.visibility = 'visible';
          document.getElementById('ifno').style.visibility = 'hidden';
    }

    else if (document.getElementById('noCheck').checked) {
        document.getElementById('ifno').style.visibility = 'visible';
        document.getElementById('ifYes').style.visibility = 'hidden';
    } 
    else document.getElementById('ifYes').style.visibility = 'visible';

}

</script>
<div class="wrapper sub_nav_bg shopping_steps">
  <div class="container-fluid">
      <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="steps_box">
                    <ul class="steps_list">
                        <li>
                            <div class="steps_num"><span>1</span></div>
                            <div class="steps_detail">Shopping cart</div>
                            <div class="icon-arrow"><img src="assets/images/right-arrow.png" alt=""></div>                  
                        </li>
                        <li class="active-orage">
                            <div class="steps_num"><span>2</span></div>
                            <div class="steps_detail">ORDER INFORMATION</div>
                            <div class="icon-arrow"><img src="assets/images/right-arrow.png" alt=""></div>                  
                        </li>
                        <li>
                            <div class="steps_num"><span>3</span></div>
                            <div class="steps_detail">COMPLETE PAYMENT</div>
                        </li>
                      </ul>
                </div>
            </div>
        </div>
    </div>
</div>    

<!-- --> 
<div class="wrapper chekout_main">
  <div class="container-fluid">
      <div class="row">
            <div class="cart-goods-list chekout_head">
              <div class="section-address">
                    <div class="col-lg-12">
                        <h3 class="address-title">Recipient information
                          <span class="title_span">
                            <?php
                              if($message){ 
                                echo $message; 
                             }
                            ?>
                             <?php
                              if($this->session->flashdata('message')){ 
                                echo $this->session->flashdata('message'); 
                             }
                            ?>
                        </span>
                        </h3>
                    </div>
                    <?php foreach($record as $key => $val) { ?>
                    <div class="col-lg-5">
                        <div class="address-list address-hide">
                            <p class="ad_01"><b><?php echo $val->shipname;?></b></p>
                            <p class="ad_01"><?php echo $val->shipcontact;?></p>
                            <p class="ad_01"><?php echo $val->shipaddress;?></p>
                            <p class="add_default">DEFAULT</p>
                            <p class="ad_01 edit_add">
                            <button type="button" class="deliver-here" id="deliverhere" onClick="deliverhere(<?php echo $val->shippingid;
                             ?>);">Deliver Here</button>
                              <a href="#" id="edit" onClick="getDetails(<?php echo $val->shippingid;?>);">EDIT</a>
                              <a href="#">DELETE</a>
                            </p>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-lg-5">
                        <div class="address-list" data-toggle="modal" data-target="#Checkout">
                          <a href="#" class="address-pop">
                              <div>+</div>
                              <div>Add New Address</div>
                            </a>
                        </div>
                    </div>
                    
                    <div class="order-info-hd">
                      <?php foreach($data['cart'] as $key => $value) {?>
                        <div class="order-list1">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <h3 class="item_title order-text">Order information</h3>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 ">
                                <div class="order-item">
                                    <div class="item-img"><img src="<?php echo base_url();?>images/thumimg/<?php echo $value['image'];?>" alt=""></div>
                                    <div class="order-details">
                                    <h3 class="item_title order-text"><?php echo $value['name'];?></h3>
                                   
                                    </div>
                                </div>
                            </div>
                     
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
                             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">
                                <div class="item_price_txt">Qty:<?php echo $value['qty'];?></div>
                             </div>
                        
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">
                               <div class="item_price_total"><span><img src="assets/images/rupees-orange.png" alt=""></span><?php echo $value['subtotal'];?></div>
                            </div>
                        </div>
                        <?php } ?>
            
                       <!--  <div class="order-list1">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <h3 class="item_title order-text">Delivery Service</h3>
                                </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                    <h3 class="item_title order-text">Home Delivery</h3>
                             </div>
                        </div>
                 -->
                
                
                    <!--   <div class="order-list1">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                              <h3 class="item_title order-text">GST Details</h3>
                          </div>
                          <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 ">
                            <h3 class="item_title order-text">
                            <label for="chkPassport">
                              <input type="checkbox" class="gst_click" />
                              Use GSTIN for this order (optional)</h3>
                            </label>
                            <p class="sold_by">Xiaomi shall not be responsible for incorrect or inaccurate GSTIN number provided by you.</p>
                            <div class="gst-box">
                                <input class="gst-input" type="text" placeholder="22 AAAAA0000A 1Z5">
                            </div>   
                         </div>
                      </div> -->
                
                      <div class="order-list1">
                      <div> <span id="cpn_msg" style="font-size:12px;"></span></div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <h3 class="item_title order-text">Discount Coupon </h3>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                 
                                  <div class="add-card">
                                          <div class="form">
<input id="cpn_code" type="text" placeholder="Enter coupon code" class="J_cardNum card_width">
                                          <button id="cpn_btn" type="button" class="checkout_btn">Coupon</button>

                                          </div>
                                      </div>                     
                            </div>
                      </div>  
<div class="order-list1">
       <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <h3 class="item_title order-text">Payment Method </h3>
                        </div>
       <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
               <div class="cod_payment">
                <label class="checkbox-stock6 margin_left0">CC Avenue
                <input type="radio" name="cod" value="ccavenue" onclick="javascript:yesnoCheck();" id="yesCheck">
                <span class="checkmark-6"></span>
                </label>
            </div>
            <div class="cod_payment">
                <label class="checkbox-stock6 margin_left0">Cash On Delivery
                <input type="radio" name="cod" checked="checked" value="cod" onclick="javascript:yesnoCheck();" id="noCheck">
                <span class="checkmark-6"></span>
                </label>
            </div>
            </div>
    

       </div>



                  </div>  
              </div>
        
                    <div class="cart-action-bar">
                      <div class="col-lg-6"></div>
                        <div class="col-lg-6">
                          <div class="cart-action-box">
                              <p class="subtotal-price">Subtotal : <span id="totalcartpricewithout"><img src="assets/images/rupees-gay.png" alt=""><?php echo $total;?></span></p>
                                <!-- <p class="subtotal-price">Shipping : <span>Free shipping</span></p> -->
                                <p class="subtotal-price">Discount ： <span  ><img src="assets/images/rupees-gay.png" alt=""><span  id="coupon_discount">0</span></span></p>
                            
                              <div class="total_price_item total_big">
                                <span class="total_small_txt">Total</span>
                                <span><img src="assets/images/rupees-orange.png" alt=""></span><span  id="Grandtotalwithcart"><?php echo $total;?></span>
                              </div>
                              <div id="ifYes" style="visibility:hidden">
      <form action="ccrequesthander" method="post">
                                <input type="hidden" name="delivery_id" id="delivery_id"/>
                                <input type="submit" name="submitcod" id="submitcod" value="PLACE ORDER" class="checkout_btn"/>
                              </form>
    </div>
       <div id="ifno" style="visibility:visible; ">
     <form action="codpayment" method="post">
                                <input type="hidden" name="delivery_id1" id="delivery_id1"/>
                                <input type="submit" name="submitcod" id="submitcodval" value="Cod PLACE ORDER" class="checkout_btn"/>
                              </form>
    </div> 



                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          </div>    
        </div>     
     </div>
</div>
 
 <div class="wrapper">
    <div class="policies_white">
        <div class="container-fluid">
          <div class="row">
                <div class="footer_policies">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                          <div class="service-policy">
                              <a href="#">
                                 <div class="policy_pic"><img src="<?php echo base_url();?>assets/images/policy1.jpg" alt=""></div>
                                 <div class="policy_txt">
                                  <strong>Hassle-free replacement</strong>
                                  <br>
                                  <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                                 </div>
                               </a>
                            </div>
                        </div>
                        
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                          <div class="service-policy pull_space">
                              <a href="#">
                                 <div class="policy_pic"><img src="<?php echo base_url();?>assets/images/policy2.jpg" alt=""></div>
                                 <div class="policy_txt">
                                  <strong>100% secure payments</strong>
                                  <br>
                                  <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                                 </div>  
                                </a>
                            </div>
                        </div>
                        
                        
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                          <div class="service-policy pull_right">
                                <a href="#">
                                   <div class="policy_pic"><img src="<?php echo base_url();?>assets/images/policy3.jpg" alt=""></div>
                                   <div class="policy_txt">
                                    <strong>Vast service network</strong>
                                    <br>
                                    <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                                   </div>
                                </a>
                          </div>
                        </div>
                </div>
            </div>
      </div>
  </div>
</div>
   
  <div class="container">
    <div class="modal fade" id="Checkout" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title mar-left">New Address</h4>
            </div>
            <div class="modal-body pop_padding">
              <form method="post" action="" id="shipaddress">
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipname" name="shipname" required placeholder="Name">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" onkeypress="return isNumberKey(event);" maxlength="10" minlength="6" id="shippincode" name="shippincode" required placeholder="Pin-code">
                  </div>
                  <p>Please make sure your address is accurate. This cannot be changed once order is placed.</p>
                  <div class="form-group">
                      <textarea class="form-control" rows="2" id="address" name="address" required placeholder="Address"></textarea>
                  </div>  
              
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="landmark" name="landmark" required placeholder="Landmark">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipcity" name="shipcity" required placeholder="City">
                  </div>
                  <div class="form-group">
                     <select class="form-control form-check" name="stateid" required id="stateid">
                        <option value="">State</option>
                        <?php foreach($state as $key => $val) { ?>
                          <option value="<?php echo $val->stateid;?>"><?php echo $val->statename;?></option>
                        <?php } ?>
                      </select>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipemailid" name="shipemailid" required placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipcontact" onkeypress="return isNumberKey(event);" maxlength="15" name="shipcontact" required placeholder="Phone">
                  </div>
                  <div class="form-group form_gray_bg">
                    <a href="#" class="cancel_btn">Cancel</a>
                    <input type="submit" name="submit" value="Confirm" class="cancel_btn orage_cancle"/>
                  </div>  
                </form>
            </div>
        </div>  
      </div>
    </div>
  </div>

  <div class="container">
    <div class="modal fade" id="checkoutEdit" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title mar-left">New Address</h4>
            </div>
            <div class="modal-body pop_padding">
              <form method="post" action="updateshipaddre" id="shipaddress">
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipname_edit" name="shipname" required placeholder="Name">
                    <input type="hidden" class="form-control form-check" id="shippingid_edit" name="shippingid" required placeholder="Name">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" onkeypress="return isNumberKey(event);" maxlength="10" minlength="6" id="shippincode_edit" name="shippincode" required placeholder="Pin-code">
                  </div>
                  <p>Please make sure your address is accurate. This cannot be changed once order is placed.</p>
                  <div class="form-group">
                      <textarea class="form-control" rows="2" id="address_edit" name="address" required placeholder="Address"></textarea>
                  </div>  
              
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="landmark_edit" name="landmark" required placeholder="Landmark">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipcity_edit" name="shipcity" required placeholder="City">
                  </div>
                  <div class="form-group">
                      <select class="form-control form-check" name="stateid" required id="shipstate_edit">
                          <option value="">State</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipemailid_edit" name="shipemailid" required placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-check" id="shipcontact_edit" onkeypress="return isNumberKey(event);" maxlength="15" name="shipcontact" required placeholder="Phone">
                  </div>
                  <div class="form-group form_gray_bg">
                    <a href="#" class="cancel_btn">Cancel</a>
                    <input type="submit" name="submit" value="Confirm" class="cancel_btn orage_cancle"/>
                  </div>  
                </form>
            </div>
        </div>  
      </div>
    </div>
  </div>        
    <script type="text/javascript" src="<?php echo base_url();?>assets/webapp/js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
          $("#submitcod").click(function(){
              var delivery_id = $("#delivery_id").val();

              if(!delivery_id){
                alert("Please select one shipping address.");
                return false;
              }
              


          });
      });
            $(document).ready(function(){
          $("#submitcodval").click(function(){
              var delivery_id1 = $("#delivery_id").val();

              if(!delivery_id1){
                alert("Please select one shipping address.");
                return false;
              }
              


          });
      });


      function deliverhere(ship_id){
        var delivery_id = ship_id;
        $("#delivery_id").val(delivery_id);
         $("#delivery_id1").val(delivery_id);
      }

      function getDetails(ship_id){
        $.ajax({
          type: "POST",
          url: "webapp/cart/getshipdetails",
          data: "ship_id=" + ship_id,
          dataType: "json",
          success: function (data) {
              console.log('Data',data.shipname);
              $("#shipname_edit").val(data.shipname);
              $("#shippincode_edit").val(data.shippincode);
              $("#address_edit").val(data.shipaddress);
              $("#landmark_edit").val(data.landmark);

              $("#shipcity_edit").val(data.shipcity);
              $("#shipemailid_edit").val(data.shipemailid);
              $("#shipcontact_edit").val(data.shipcontact);
              $("#shipstate_edit").html(data.shipstate);
              $("#shippingid_edit").val(data.shippingid);
          }
        });

        $("#checkoutEdit").modal('show');
      }
    </script>

    <script type="text/javascript">
    /*  $(document).ready(function(){
        $(".navigation > ul > li > a").hover(function(){
          //alert("ddd");
          if($(this).next(".product_item_view").length > 0){
          $(".logo_row").addClass("show_nav");
          $(this).next(".product_item_view").css({"display":"block"});
          }
          else{
            $(".logo_row").removeClass("show_nav");
            $(".product_item_view").css({"display":"none"});
          }
        });
        $(".navigation").mouseleave(function(){
              $(".logo_row").removeClass("show_nav");
          $(".product_item_view").css({"display":"none"});
        });
      });*/

      function isNumberKey(evt)
      {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 31 
        && (charCode < 48 || charCode > 57))
        return false;

        return true;
      }
    </script>

 
   


    <script>
      $(document).ready(function(){

          $('#cpn_btn').on('click', function(){
            var post_data = {
               'cpn_code' : $('#cpn_code').val().trim(),
            }
          //  alert($('#cpn_code').val().trim());
               var baseurl = "<?php echo base_url(); ?>";
              $.ajax({
                url: baseurl+"webapp/cart/validate_coupon2",
                type: "POST",
                data: post_data,
                success: function (data) {
                    $('#cpn_msg').css('display','block');
                    $('#cpn_msg').html('');
                    var json_data = jQuery.parseJSON(data);
                    $("#Grandtotalwithcart").html(json_data['grandTotalwithdilabery']);
                    $('#amount').val(json_data['grandTotalwithdilabery']); // for online payment
                    
                    //update_wallet();
                    if(json_data['allow_discount'] == "true")
                    {
                      $('#checkusecoupcode').val(1); 
                      $('#chequsecoupcode').val(1);
                      $('#onlineusecoupcode').val(1);
                      $('#cpn_msg').css({'color' : 'green'});         
                    }
                    else if(json_data['allow_discount'] == "false"){
                        $('#checkusecoupcode').val(0); 
                        $('#chequsecoupcode').val(0);
                        $('#onlineusecoupcode').val(0);
                        $('#cpn_msg').css({'color' : 'red'});         
                    }

                    $('#cpn_msg').html(json_data['msg']);
                    setTimeout(function(){ $('#cpn_msg').fadeOut() }, 5000);
                    //$("#cpn_msg").fadeOut("slow");
                    $('#coupon_discount').html(json_data['cpn_discount']);

                    //---- wallet update with coupon -------
                    var totcart=parseFloat(json_data['grandTotalwithdilabery']);
                    var totremwall=parseFloat($('#remwallamt').html());
                    var actualwallamt=$('#actualwallamt').val();
                    var zerowall=0;
                    if(totcart<=actualwallamt){
                        if(totcart==actualwallamt){
                            $('#remwallamt').html(zerowall.toFixed(1));
                            $('#wallpayamt').html(json_data['grandTotalwithdilabery']);
                            $('#walletbtn').css("display","block");
                            $('.overlay_check1').css("display","block");
                            $('#relwallpay').css("display","none");
                        } else {
                            var addcoupwallet=totremwall+parseFloat(json_data['cpn_discount']);
                            $('#remwallamt').html(addcoupwallet.toFixed(1));
                            $('#wallpayamt').html(json_data['grandTotalwithdilabery']);
                        }
                        $('#totpayamt').html(json_data['grandTotalwithdilabery']);
                    } else {
                        var totremwallpay=parseFloat($('#resultwallpay').html());
                        var remwalbal= totcart-(totremwallpay-parseFloat(json_data['cpn_discount']));
                        $('#resultwallpay').html(remwalbal.toFixed(1));
                        $('#totpayamt').html(json_data['grandTotalwithdilabery']);
                    }
                    //---- end wallet update with coupon -------

                }
            });

          });
    });



</script> 
  </body>
</html>