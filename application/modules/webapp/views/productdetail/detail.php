<?php //p($ProductDetails);exit;?>
<!--zoom effect-->
<link href="<?php echo base_url();?>assets/webapp/css/jquery.simpleLens.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/webapp/css/jquery.simpleGallery.css" rel="stylesheet" />

<!--zoom end-->
<?php
if(isset($this->session->userdata('bizzgainmember')->id))
$userid=$this->session->userdata('bizzgainmember')->id;
else
$userid=null;
$getImg = getImgProdetail($ProductDetails->image);
$pid    = (int)$ProductDetails->promapid;
?>

<div class="page_pro1" style="height:auto;padding:0px 0px 0px;background: #fff">
  <div class="col-lg-12">
    
    <div class="row">
      <div class="heading-box">
        <div class="container">
        <div class="heading_name">
          <a href="<?php echo base_url(); ?>" class="">Home</a>
          <?php if (!empty($preant_bredcum['name'])) { ?>
            <a href="<?php echo $preant_bredcum['url']; ?>" class=""><i class="fa fa-angle-right"></i><?php echo $preant_bredcum['name']; ?></a>
          <?php } ?>
          <?php if (!empty($subparent_bredcum['name'])) { ?>
            <a href="<?php echo $subparent_bredcum['url']; ?>" class=""><i class="fa fa-angle-right"></i><?php echo $subparent_bredcum['name']; ?></a>
          <?php } ?>
          <a href="<?php echo $child_bredcum['url']; ?>" class="<?php echo $child_bredcum['url']; ?>"><i class="fa fa-angle-right"></i><?php echo $child_bredcum['name']; ?></a>
        </div>
      </div>
    </div>
  </div>
  </div>
  <div class="container">
  <div class="row">
    <div class="main_box">
      <div class="row">
        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
          <div class="left_box">
            <div class="col-lg-5 col-sm-5 col-sm-5 col-xs-12">
              <div class="row">
                <div class="small-slider">
                  <div class="simpleLens-gallery-container" id="demo-1">
                    <div class="simpleLens-container">
                      <div class="simpleLens-big-image-container">
                        <a class="simpleLens-lens-image" data-lens-image="<?php echo proimgurl('images/zoomimg/'.$getImg[0]);?>">
                          <img width="100%" src="<?php echo proimgurl('images/hoverimg/'.$getImg[0]);?>" class="simpleLens-big-image">
                        </a>
                      </div>
                    </div>
                    
                    <div class="simpleLens-thumbnails-container">
                      <?php
                      foreach ($getImg as $key => $value) {
                      if($value!='' && $value!='na'){
                      $filename=base_url()."images/thumimg/$value";
                      $mediumname=base_url()."images/hoverimg/$value";
                      $largename=base_url()."images/zoomimg/$value";
                      echo '<a href="#" class="simpleLens-thumbnail-wrapper"
                        data-lens-image="'.$largename.'"
                        data-big-image="'.$mediumname.'">
                        <img src="'.$filename.'">
                      </a>';
                      }
                      
                      }
                      ?>
                      
                      
                      
                    </div>
                  </div>
                  
                  
                  
                  
                </div>
              </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
              <div class="product_box">
                <h1 class="text-name"><?php echo ucwords($ProductDetails->ProName); ?></h1>
                <?php $pronamestrdet=str_replace(' ','-',$ProductDetails->ProName);
                $pronamestrnamedet = seoUrl(str_replace('/','-',$pronamestrdet)); ?>
                <input type="hidden" id="pronamepro" value="<?php echo $pronamestrnamedet;?>" />
                <div class="row">
                  <div class="col-lg-12">
                    <div class="prige">
                      <span class="brabd_bold">Price:</span>
                      <?php
                      $cityprice =citybaseprice($ProductDetails->FinalPrice,$ProductDetails->cityvalue);
                      $prodtsc=number_format((1-$cityprice/number_format($ProductDetails->prodmrp,1,".",""))*100,1,".","");
                      if($prodtsc>0){
                            if($ProductDetails->ProDiscount < 0){
                            echo '<span>Rs.'.number_format($ProductDetails->FinalPrice,1,".","").'</span>';
                            }else{
                            if($ProductDetails->ProDisType == 'fixed'){
                            $distype='Rs. Off';
                            } else{
                            $distype='% Off';
                            }
                            echo 'Rs.'.$cityprice.'<span class="under_line">Rs.'.number_format($ProductDetails->prodmrp,1,".","").'</span>';
                            echo '<span class="off-color" >'.$prodtsc.'<small>'.$distype.'</small></span>';
                            }
                    }
                      else
                      {
                            if($ProductDetails->ProDiscount <= 0){
                          echo '<span>Rs.'.number_format($ProductDetails->FinalPrice,1,".","").'</span>';
                          }else{
                          echo 'Rs.'.$cityprice;
                          }
                      }
                      ?>
                    </div>
                    
                    <div class="brand_name">
                      <h2 class="brand"><samp class="brabd_bold">Brand :</samp> <?php echo ucwords($ProductDetails->brandname); ?></h2>
                      <h2 class="brand"><samp class="brabd_bold">Product Code : </samp> <?php echo ucwords($ProductDetails->prosku); ?></h2>
                      <h2 class="brand"><samp class="brabd_bold">Availability :</samp>
                      <?php
                      if($ProductDetails->quantityLeft==0)
                      {
                      ECHO 'Out Of Stock';
                      }
                      else{
                      ECHO 'In Stock';
                      } ?>
                      </h2>

                       <h2 class="brand"><samp class="brabd_bold">Size :</samp> <?php  $weifood=explode(',', $ProductDetails->produtattr);
    
      foreach ($weifood as $key => $val) {
        
        $getfw=explode(':', $val);
        
        
        } echo $getfw[0]; ?></h2>
                      <?php
                   ?>
                    </div>
                       <div class="q-ty">
                       <div class="uiv2-rate-count-text">
                        <div class="bike_icon"><img src="<?php echo base_url();?>assets/webapp/img_hm/motorcycle.png"  alt=".." ></div>
                        <div class="bike_cont">Express Delivery: In 90 minutes</div>
                    </div>
                     <div class="uiv2-rate-count-text">
                        <div class="bike_icon"><img src="<?php echo base_url();?>assets/webapp/img_hm/delivery_icon.png" alt="..">
                        </div>
                        <div class="bike_cont">Standard Delivery: Tomorrow Morning</div>
                    </div>

                     <?php if($ProductDetails->quantityLeft==0){ #echo '<span style="color: red;">OUT OF STOCK</span>'; 
                     }else{ ?>
                      <div style="float:left; padding-top:10px;"><h1 class="q_size">QUANTITY</h1>
                      <a href="javascript:void(0);"><span class="first2 q_link" data-href="<?=$ProductDetails->promapid?>" id="first2" onclick="cart_update_listing($(this));"><i class="fa fa-minus"></i></span></a>
                      <input type="text" readonly id="a1<?=$ProductDetails->promapid?>" value="1" class="value_size_n" style="margin-right:5px;">
                      <a href="javascript:void(0);"><span class="last2 q_link" data-href="<?=$ProductDetails->promapid?>" data-hrefnew="<?=$ProductDetails->quantityLeft?>" id="last2" onclick="cart_update_listing($(this));"><i class="fa fa-plus"></i></span></a>
                    </div>
                    <?php } ?>
                    </div>
                  </div>
                  
                </div>
                
                
                <?php if($ProductDetails->quantityLeft!=0){ ?>
                
                <div class="col-lg-12">
                  <div class="row">
                    <div class="quantity">
                      
                      <div class="quantity-main">
                        <div class="col-lg-12">
                          <div class="row">
                            <form action="" method="post">
                              <?php
                              if($ProductDetails->quantityLeft == 0 || $ProductDetails->quantityLeft <= 0){
                              /*$disableButton = "false";
                              $addClassdNoproduct = "disablebutton";
                              $addtocartID = '';*/
                              $addClassdNoproduct = "";
                              $disableButton = "true";
                              $addtocartID = 'addtocart';
                              }else{
                              $addClassdNoproduct = "";
                              $disableButton = "true";
                              $addtocartID = 'addtocart';
                              }
                              if(in_cart_array($this->cart->contents(), $pid) == true){
                              $addClassd = "disablebutton";
                              $data_is_in_cart = 'true';
                              } else {
                              $data_is_in_cart = 'false';
                              $addClassd ="";
                              }
                              ?>
                              <?php if(!empty($sizearray)){ $data_with_size = "true";} else{$data_with_size = "false";} ?>
                <!--                <div class="selet_qtnty"><?php #echo $prolist->prodtmappid; ?>
    <select>
  <?php  $weightex=explode(',', $ProductDetails->color);
      $weightInd='';
      foreach ($weightex as $key => $value) {
        
        $getweight=explode(':', $value);
        if($ProductDetails->promapid==$getweight[1]){
          $getwe=explode(':', $value);
          $weightInd=$getwe[0];
        
        } ?>
        <option <?php if($getweight[0]==$weightInd) echo 'selected'; ?> value="<?php echo $getweight[1]; ?>"><?php echo $getweight[0]; ?></option>
        <?php
      } 

      ?>
   
    </select>  
  </div> -->
                              <span class="sizerror" id="sizerror<?=$ProductDetails->promapid?>"></span>
                              <a href="javascript:void(0)" data-hrefnew="<?php echo $data_is_in_cart; ?>" data-href="<?=$ProductDetails->promapid?>" class="add_cart_new addtocart<?php echo $addClassdNoproduct;?> <?php echo $addClassd;?>" id="<?php echo $addtocartID; ?>"  data-seller-id="<?php echo $ProductDetails->manufacturerid ?>" data-with-size='<?php  echo $data_with_size; ?>' data-with-size-val='<?php if(isset($golbalsizeval)) echo $golbalsizeval ?>'
                              data-with-color-val='<?php echo $weightInd; ?>'
                               data-is-in-cart ="<?php echo $data_is_in_cart; ?>"  data-is-listing-buyable="<?php echo $disableButton; ?>"  data-buy-listing-id = "<?php echo $pid;?>" type="submit" name="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
                              <span class="sizerrorbuynow"></span>
                            </form>
                           <!--  <div class="compare_main">
                              <?php  echo wishlistlink('view',$userid,$pid); ?>
                              <a href="javascript:void(0);" style="outline:none;" data-href="<?=$ProductDetails->promapid?>" class="button inquiry" data-toggle="modal" data-target="#myModal"><i class="fa fa-files-o"></i><samp class="compare_size"> add to inquiry</samp></a>
                            </div> -->
                          </div>
                        </div>
                        
                      </div>
                    </div>
                   
                    
                  </div>
                </div>
                 <?php } ?>
              </div>
             
            </div>
          </div>
        </div>
        <?php if($similarProduct->scalar != "Something Went Wrong"){ ?>
        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
          <div class="row_sty">
            <div class="related_bg">
              <h1 class="related_size">Related Products</h1>
              <div class="space1"></div>
              <?php
              foreach ($similarProduct as $key => $value) {
              $cityprice =citybaseprice($value['FinalPrice'],$value['cityvalue']);
              ?>
              <div class="pro_box_m">
                <div class="pro_box">
                  <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="row">
                      <div class="pro_img">
                        <a href="<?php echo base_url().'product/'.seoUrl(strtolower($value['ProName'])).'/'.$value['promapid'].'.html'?>">
                          <img src="<?php echo proimgurl('images/hoverimg/'.$value['proImg']);?>" class="img-responsive" onerror="imageError(this)" alt="<?php echo ucwords($value['ProName'])?>">
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-9">
                    <div class="pro_cont">
                      <div class="size"><a href="<?php echo base_url().'product/'.seoUrl(strtolower($value['ProName'])).'/'.$value['promapid'].'.html'?>"><?php echo $value['ProName']; ?></a></div>
                      <div class="size1">Rs. <?php echo $cityprice; ?></div>
                       <div class="size2"><a href="<?php echo base_url().'product/'.seoUrl(strtolower($value['ProName'])).'/'.$value['promapid'].'.html'?>">View More</a></div>
                      <!-- <div class="product_wishlist">
                        <a href="javascript:void(0);" style="outline:none;" data-href="<?=$value['promapid']?>" class="button inquiry" data-toggle="modal" data-target="#myModal"><i class="fa fa-files-o"></i><samp class="compare_size"></samp></a>
                        <?php  echo listwishlistlink('view',$userid,$value['promapid']); ?>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <!--Description stat-->
 <?php if($mostview->scalar!='Something Went Wrong'){ ?>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h1 class="most_font"> <a href="">MOST VIEWED</a></h1>
      <div class="row">
        <div class="relat_page">
          <?php 
          foreach ($mostview as $key => $valu) {
          $value=(object)$valu;
          $cityprice =citybaseprice($value->FinalPrice,$value->cityvalue);
          $prodtsc=number_format((1-$cityprice/number_format($value->productMRP,1,".",""))*100,1,".","");

          if($value->ProDiscount<=0){
         
          } else {
          if($value->ProDisType=='fixed')
          $distype='Rs. Off';
          else
          $distype='% Off';
          
          
          }
          ?>
          <div class="product_list product_list2">
              <?php 
              if($prodtsc>0){
   if($prodtsc != "" || $distype !=""){
     echo '<div class="sale_ad">'.$prodtsc.'<small>'.$distype.'</small></div>';
   }
   else{

   }}
  ?>
            <div class="product_tab">
              <div class="product_tab_inner">
                <div class="img_box">
                  <a href="<?php echo base_url().'product/'.seoUrl(strtolower($value->ProName)).'/'.$value->promapid.'.html'?>">
                    <img src="<?php echo proimgurl('images/hoverimg/'.$value->proImg);?>" onerror="imageError(this)" alt="<?php echo ucwords($value->ProName)?>">
                  </a>
                </div>
                <div class="product_info">
                  <div class="pro_name"><a href="<?php echo base_url().'product/'.seoUrl(strtolower($value->ProName)).'/'.$value->promapid.'.html'?>"><?php echo ucfirst($value->ProName);?></a></div>
                  <div class="price">
                    <?php
                    
                    if($value->ProDiscount<=0){
                    echo '<span class="new_p">Rs.'.number_format($value->productMRP,1,".","").'</span>';
                    } else {
                    
                    echo '<span class="old_p">Rs.'.number_format($value->productMRP,1,".","").'</span>
                    <span class="new_p">Rs.'.$cityprice.'</span>';
                    
                    }
                    ?>
                  </div>
           
                  <div class="p_link">
                    <a href="<?php echo base_url().'product/'.seoUrl(strtolower($value->ProName)).'/'.$value->promapid.'.html'?>" class="add_cart most_view_btn"><i class="fa fa-history"></i> View Details</a>
                  </div>
                  <div class="p_link2">
                    <!-- <a href="#"   class="inquiry" title="add to inquiry"><i class="fa fa-pencil-square-o"></i></a> -->
                    <a href="javascript:void(0);" style="outline:none;" data-href="<?=$value->promapid?>" class="button inquiry" data-toggle="modal" data-target="#myModal"><i class="fa fa-files-o"></i><samp class="compare_size"></samp></a>
                    <?php  echo listwishlistlink('view',$userid,$value->promapid); ?>
                    <!-- <a href="#" class="wishlist" title="add to Wishlist"><i class="fa fa-heart"></i></a>
                    <a href="#" class="Compare" title="add to Compare"><i class="fa fa-files-o"></i></a> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>

  <?php } ?>
  
  
  <div class="line_top">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-3 col-md-3 ">
        <div class="row">
          <div class="discription_left">
            <ul class="description">
              <li><a href="javascript:void(0)" id="dis_a">Description</a></li>
              <li><a href="javascript:void(0)" id="dis_b">Specification</a></li>
              <!-- <li><a href="javascript:void(0)" id="dis_c">Features</a></li> -->
              <!--<li><a href="javascript:void(0)" id="dis_c">Specification</a></li>-->
              
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-9" id="pay_a" style="display: block;">
        <div class="row">
          <div class="description_box">
            <p class="size"><?php echo $ProductDetails->description ?></p>
          </div>
        </div>
      </div>
      <div class="col-lg-9" id="pay_b" style="display: none;">
        <div class="row">
          <div class="description_box productispasifaction">
            <table style="width:100%">
              <?php
              $features = featureMain($profeature, $pid);
              ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
  
  
  <!--product slider-->
  
  <script type="text/javascript">
  function test(val1,val2,val3){
  var classme = "si_"+val1;
  $( "span.siz" ).each(function( index ) {
  //console.log( index + ": " + $( this ).text() );
  if($(this).hasClass(classme)){
  //alert($(this).html());
  $(this).show();
  }else{
  $(this).hide();
  }
  });
  }
  </script>
  <style type="text/css">
  .product_list2 {
    width: calc(100% /5);
    position: relative;
}
  .highlightSize {
  background: #444;
  color: #fff !important;
  }
  span.siz {
  border: 1px solid #E6E2E2;
  display: inline-block;
  text-align: center;
  margin-right: 5px;
  cursor: pointer;
  padding: 3px 10px 3px 10px;
  margin-left:15px;
  
  color: #444;
  }
  span.siz a {
  color: #444;
  font-size: 14px
  }
  .siz.highlightSize a {
  color: #fff
  }
  span.siz:hover {
  background: #444;
  color: #f1f1f1
  }
  span.siz:hover a {
  color: #fff
  }
  
  li.red {
  width: 27px;
  height: 25px;
  background-color: red;
  margin: 5px;
  cursor: pointer;
  border: 0px solid #888; float:left; list-style-type:none;
  
  }
  li.red:hover {
  /*box-shadow: 0 0 4px #333*/
  }
  .addHighliter {
  /*  border: 2px solid #FFF;
  box-shadow: 0 0 8px #6E6A6A*/
  }
  </style>
  
  
  <script>
  $( "#dis_a" ).click(function() {
  $( "#pay_a" ).css("display","block");
  $( "#pay_b" ).css("display","none");
  $( "#pay_c" ).css("display","none");
  });
  $( "#dis_b" ).click(function() {
  $( "#pay_b" ).css("display","block");
  $( "#pay_a" ).css("display","none");
  $( "#pay_c" ).css("display","none");
  });
  $( "#dis_c" ).click(function() {
  $( "#pay_c" ).css("display","block");
  $( "#pay_a" ).css("display","none");
  $( "#pay_b" ).css("display","none");
  });
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
  pro_img_s();
  main_img();
  });
  $(window).resize(function(){
  setTimeout( function(){
  pro_img_s();
  main_img();
  }  , 500 );
  });
  function pro_img_s(){
  var x = $(".img_box").width();
  $(".img_box a").height(x);
  }
  function main_img(){
  var x1 = $(".simpleLens-container").width();
  $(".simpleLens-big-image-container").height(x1);
  $(".simpleLens-lens-image").height(x1);
  }
  function coltest(colpromapid){
  var pronamedet=$('#pronamepro').val();
  var prourl='<?php echo base_url();?>product/'+pronamedet+'-'+colpromapid+'.html';
  window.location.href=prourl;
  }
  </script>