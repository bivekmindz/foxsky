<style>
.suport_list {
	margin:0;
	padding:0;
	}
.suport_list li {
	list-style-type:none;
	margin-left:15px;
}
.xm-section p {
    text-align: justify;
}
</style>

<div class="wrapper sub_nav_bg">
	<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="sub_nav">
			<div class="pull-right">
            	<ul class="nav_about">
                      <li><a href="<?php echo base_url();?>aboutus">About us</a></li>
                    	<li><a href="<?php echo base_url();?>support" >Support</a></li>
                        <li><a href="#" class="active-orage">User Agreement</a></li>
                        <li><a href="<?php echo base_url();?>privacy">Privacy Policy</a></li>
                        <li><a href="<?php echo base_url();?>career">Career</a></li>
                        <li><a href="<?php echo base_url();?>terms">Terms of Use</a></li>
                    </ul>
            	</div>
              </div>
            </div>
        </div>
    </div>
</div>		

<div class="wrapper bg_orage">
	<div class="container">
        <div class="about_heading">
        	<h3 class="about_h4">REPLACEMENT & REFUND POLICIES</h3>
        </div>
    </div>
</div>
        

<div class="wrapper parivacy-policy">
	<div class="container">
        <div class="row">
            <div class="xm-section">
            
            	<p><strong>Replacement & Refund  Policies</strong></p>
                
				<p><strong>HOW DO I CANCEL AN ORDER?</strong></p>

<p>You can cancel your order online before the product has been shipped. If you paid via a prepayment mode (credit card, debit card or net banking), then your entire order amount will be refunded.</p>

<p>Unfortunately, an order cannot be cancelled once the item has been shipped from our warehouses.</p>

<p>If you wish to cancel an order, please follow these steps:</p>
<ul>
	<li><p>Log into your foxsky account and go to the 'My Orders' page</p></li>
    <li><p>Choose cancellation option and fill in the required details.</p></li>
</ul>

<p>Once your cancellation request is created, we will ensure that the cancellation is processed as soon as possible.</p>

<p><strong>HOW LONG WILL IT TAKE TO PROCESS MY CANCELLATION REQUEST?</strong></p>

<p>Once you create a cancellation request, it will take us a maximum of 1-2 business days to cancel the order and initiate a refund. You will be notified of the same by email.</p>

<p>If you opt for having the money transferred back to the source of transaction, it may take up to 7-10 business days for the respective banks to process the refund. Please get in touch with the banks directly in case of any delays post confirmation of cancellation/refund by mi.com.</p>

<p><strong>WHAT ARE THE MODES OF REFUND AVAILABLE AFTER CANCELLATION?</strong></p>

<p>Except for Cash-on-Delivery transactions, refunds are made to the payment mode/account that was originally used to make the transaction. The refund shall be made in Indian Rupees only and be equivalent to the transaction payment received in Indian Rupees.</p>

<p><strong>HOW LONG DOES IT TAKE FOR A REFUND TO BE REFLECTED IN MY ACCOUNT?</strong></p>

<p>Generally, returns across all payment options take between 5 to 7 working days (excluding holidays) to reflect in your account and sometimes the process takes 8 to 10 working days, depending on the associated bank.</p>

<p>If the payment is not reflected within the estimated time, please reach out to us for your ARN/RRN numbers and then contact your respective bank.</p>

<p>Please note that refunds can only be made to the original card / account only and not through any other mode of payment.</p>

<p><strong>CAN I REQUEST FOR A REPLACEMENT?</strong></p>

<p>Our replacement policy allows you to request a replacement device at no additional cost if the device received by you is defective or is not as ordered. However, size-related replacement are allowed for selected products.</p>

<p>If you find that the package is tampered, please do not accept the item and hand the package back to the delivery person before signing the POD.</p>

<p>You will be eligible to get a replacement if</p>
<ul>
	<li><p>Our examination of the device shows a defect that qualifies the device for replacement</p></li>
    <li><p>A replacement request is made within 10 (ten) days of receipt of the delivery of the order<p></li>
    <li><p>The device has been received in the original product packaging along with supporting documentation such as receipt or proof of purchase, price tags, labels, warranty card and any freebies and accessories obtained along with the original product.<p></li>
    <li><p>For foxsky Business Backpack - Under Replacement policy, only manufacturing defect based replacements are allowed.<p></li>
    
</ul>
       

<p>Please note that replacement is subject to availability of a suitable replacement.</p>

<p>Please note that this policy will not cover routine product wear and tear, damage incurred during use or any other forms of damage and will not, in any event, entitle you to a refund, whether partial or otherwise.</p>

<p><strong>HOW DO I PLACE A REPLACEMENT REQUEST?</strong></p>

<p>You may initiate a replacement request by calling our customer care services at 1800 532 8777. Once you have discussed the nature of your replacement request with our customer care executives, they will try to resolve the issue with the device, and may, under certain circumstances, advice a replacement.</p>

<p><strong>WHAT IS THE PICKUP PROCESS FOR REPLACEMENTS?</strong></p>

<p>We will arrange for a pickup of the old product/device from your address registered with foxskyindia.com at the time of purchase of the product. If you wish to ship your device from any other location, or if our courier partners do not service your registered location, you will be required to return the device, at your own expense, to the following address:</p>

<p>Foxsky Communications and Logistics<br />
49/26 site 4 industrial area Sahibabad Ghaziabad U.P 201010, INDIA.</p>

<p>We recommend you use a reliable courier service to return the old product.
While handing over a device for shipping or otherwise shipping it back to us, please ensure that you pack the device securely to prevent any loss or damage during transit.</p>

<p><strong>I HAVE REQUESTED FOR A REPLACEMENT. WHEN WILL I RECEIVE IT?</strong></p>

<p>Once you have raised a replacement request, our team will investigate and initiate the replacement process. You will receive an e-mail advising you on the estimated delivery date. Based on customer location, delivery span varies. If you don't get the return within the promised date, contact us immediately.</p>
               
            </div>
        </div>
    </div>
</div>



<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>
<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			$(".navigation > ul > li > a").hover(function(){
				//alert("ddd");
				if($(this).next(".product_item_view").length > 0){
				$(".logo_row").addClass("show_nav");
				$(this).next(".product_item_view").css({"display":"block"});
				}
				else{
					$(".logo_row").removeClass("show_nav");
					$(".product_item_view").css({"display":"none"});
				}
			});
			$(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
				$(".product_item_view").css({"display":"none"});
			});
		});
		</script>
         <script src="assets/js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    
 	<script src="assets/js/jquery.bxslider.min.js"></script>
	   <script>
        $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 330,
        slideMargin: 0,
        ticker: true,
        speed: 30000
    });
       </script>