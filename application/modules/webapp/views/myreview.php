
<!--accessories-->



 <script type="text/javascript">
          
            $(document).ready(function(){
              $(".order_tab_h > li > a").click(function () {
        
                $(".active_review").removeClass("active_review");
                $(this).addClass("active_review");
                var x = $(this).attr("title");
                
                $(".order_box_tab_h").removeClass("active_review_tab");
                $(".order_box_tab_h").removeClass("in");    
                $("#" + x).addClass("active_review_tab");
                
                 setTimeout(function(){
                 $("#" + x).addClass("in");
                }, 100);
                                
            });
              });
  </script>

<div class="wrapper sub_nav_bg breadcrumbs">
	<div class="container-fluid">
    	<div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
            	<a href="#">Home</a><img src="assets/images/backarrow.png" alt="arrow" class="right-arrow-small">My Reviews
            </div>
        </div>
	</div>  	
</div>

<div class="wrapper my_account">
	<div class="container-fluid">
    	<div class="row">
        	<div class="my_acount_main">
            	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                	<div class="my_nav">
                    
                    	<h3 class="my_title">ORDERS</h3>
                        	<ul class="order_menu">
                            	<li><a href="<?php echo base_url();?>myorder">My Orders</a></li>
                                <!-- <li><a href="#">Returns</a></li> -->
                            </ul>
                       
                        <h3 class="my_title">Foxsky</h3>
                        	<ul class="order_menu">
                            	<li><a href="<?php echo base_url();?>myaccount">My Account</a></li>
                                <!-- <li><a href="#" class="current_active">F-code</a></li> -->
                                <li><a href="#" class="current_active">Reviews</a></li>
                                <li><a href="<?php echo base_url();?>myaddress">Address Book</a></li>
                                
                            </ul>
                            
                           
                    </div>
                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no_padding">
                    <div class="review-block">
                        <ul class="order-list-range order_tab_h">
                       
                            <li><a href="javascript:void(0);" id="firstphoto" title="reviews_b" class="active_review">Manage Reviews</a></li>
                                 <li><a href="javascript:void(0);" id="pInfo" title="reviews_a" class="">Write a Review</a></li>

                        </ul>
                    </div>
                    <div class="my_right">
                    <?php
                          if($this->session->flashdata('message')){ 
                            echo $this->session->flashdata('message'); 
                         }
                        ?>
                        <div class="order_box_tab_h fade in"  id="reviews_a">
                        <?php foreach($myreview as $order=>$review){ ?>
                        <div class="order_container">
                            <div class="order_cont_title">
                                <h2>Review</h2>
                            </div>
                            
                            <div class="order_cont_list">
                                <ul class="order_list_left">
                                    <li><?php echo $review->OrderDate;?></li>
                                    <li><?php echo $review->ShippingName;?></li>
                                    <li>Order number : <a href="#"><?php echo $review->OrderNumber;?></a></li>
                                </ul>
                                
                                <div class="order_list_right">
                                    Total <img src="assets/images/rupees-gay.png" alt=""><span><?php echo $review->FinalPrice;?></span>
                                </div>
                                
                                <div class="order-unit-goods-info">
                                <div class="order_list_one">
                                <div class="col-lg-9">
                                <div class="row">
                                
                                 
                                 <div class="or_left">
                                    <div class="col-lg-2"><div class="order_picture"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $review->ProImage;?>" alt=""></div></div>
                                    <div class="col-lg-9">
                                        <p class="p7"><?php echo $review->ProName;?></p>
                                        <p class="p7"><img src="assets/images/rupees-gay.png" alt=""><?php echo $review->ProPrice;?>X<?php echo $review->OrdQty;?></p>
                                    </div>
                                 </div>
                                 
                                 </div>
                                </div>
                                
                                <div class="col-lg-3">
                                    <div class="row">
                                        <a href="<?php echo base_url();?>review/<?php echo $review->ProName;?>/<?php echo $review->OrderId;?>/<?php echo $review->ProId;?>" class="btn_paynow1">Write A Review</a>
                                    </div>
                                 </div>
                                 
                                 </div>
                                </div>
                            </div>
                            
                        </div>
           <?php } ?>
                    </div>
            
            
            
            
   <div  class="order_box_tab_h fade active_review_tab in" id="reviews_b">
   <?php foreach($managemyreview as $order=>$managereview){ 

     // p($managereview);exit;?>

        <div class="order_container">
                            <div class="order_cont_title">
                                <h2>Review</h2>
                            </div>
                            
                            <div class="order_cont_list">
                                <ul class="order_list_left">
                                    <li><?php echo $managereview->OrderDate;?></li>
                                    <li><?php echo $managereview->ShippingName;?></li>
                                    <li>Order number : <a href="#"><?php echo $managereview->OrderNumber;?></a></li>
                                </ul>
                                
                                <div class="order_list_right">
                                    Total <img src="assets/images/rupees-gay.png" alt=""><span><?php echo $managereview->FinalPrice;?></span>
                                </div>
                                
                                <div class="order-unit-goods-info">
                                <div class="order_list_one">
                                <div class="col-lg-9">
                                <div class="row">
                                
                                 
                                 <div class="or_left">
                                    <div class="col-lg-2"><div class="order_picture"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $managereview->ProImage;?>" alt=""></div></div>
                                    <div class="col-lg-9">
                                        <p class="p7"><?php echo $managereview->ProName;?></p>
                                        <p class="p7"><img src="assets/images/rupees-gay.png" alt=""><?php echo $managereview->ProPrice;?>X<?php echo $managereview->OrdQty;?></p>
                                    </div>
                                 </div>
                                 
                                 </div>
                                </div>
                                
                                 
                                    </div>
                                </div>
                            </div>
                            
                            
                           
                           <div class="sort_review_box">
                                <div class="review_box_left_h border_top padding_bottom_20">
                                    <div class="review_title"><a href="#"><?php echo $managereview->headline;?></a></div>
                                        <div class="review_status"></div>
                        <div class="review_content"><?php echo $managereview->review_des;?></div>

<div class="write_review_edit">
<a href="<?php echo base_url();?>editreview/<?php echo $managereview->ProName;?>/<?php echo $managereview->OrderId;?>/<?php echo $managereview->ProId;?>/<?php echo $managereview->review_id;?>" class="btn_paynow1"><span><img src="assets/images/download.png" alt=""></span> Edit Review</a>
    </div>
</div>
</div>
</div>
<?php } ?>
</div>
</div>
</div>
<!--                 <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no_padding">
                	<div class="review-block">
                		<ul class="order-list-range order_tab_h">
                            <li><a href="javascript:void(0);" id="pInfo" title="reviews_a" class="active_review">Write a Review (2)</a></li>
                            <li><a href="javascript:void(0);" id="firstphoto" title="reviews_b" class="">Manage Reviews(1)</a></li>
                        </ul>
                	</div>
                	<div class="my_right">
                    	<div class="order_box_tab_h fade active_review_tab in" id="reviews_a">
                        <div class="sort_review_box border-review">
                			<div class="review_box_left_h">
                                <div class="review_title">
                                    <a href="#">Good service</a>
                                </div>
                        <div class="review_status">Heading 1</div>
                        <div class="review_content">Let me tell you guys how I wound up getting this technological marvel first, 
My previous 32" TV broke down 9 months ago since then I was looking for a cheap yet the best TV in the market, I even went to showrooms during festival offer time to look if I will get any lu</div>
                       
                             
                        
</div>

                    
                <div class="review_box_right_h">
                        <div class="review_user_detail">
                            <div class="user_img">
                                <img src="assets/images/tv/user.png">
                            </div>
                            <div class="user_name">5153476443</div>
                        </div>
                        <div class="review_user_other_d">
                            <ul>
                                <li>
                                    <div class="left_title">Age</div>
                                    <div class="right_text">30</div>
                                </li>
                                <li>
                                    <div class="left_title">Gender</div>
                                    <div class="right_text">Male</div>
                                    
                                </li>
                                <li>
                                    <div class="left_title">Active Since</div>
                                    <div class="right_text">May 8, 2018</div>
                                       
                                </li>
                                
                            </ul>
                        </div>
                    </div>
           </div>
           
           
           <div class="sort_review_box border-review">
                			<div class="review_box_left_h">
                                <div class="review_title">
                                    <a href="#">Good service</a>
                                </div>
                        <div class="review_status">Mi LED Smart TV 4A 108 cm (43)</div>
                        <div class="review_content">Let me tell you guys how I wound up getting this technological marvel first, 
My previous 32" TV broke down 9 months ago since then I was looking for a cheap yet the best TV in the market, I even went to showrooms during festival offer time to look if I will get any lu</div>
                       
                             
                        
</div>

                    
                <div class="review_box_right_h">
                        <div class="review_user_detail">
                            <div class="user_img">
                                <img src="assets/images/tv/user.png">
                            </div>
                            <div class="user_name">5153476443</div>
                        </div>
                        <div class="review_user_other_d">
                            <ul>
                                <li>
                                    <div class="left_title">Age</div>
                                    <div class="right_text">30</div>
                                </li>
                                <li>
                                    <div class="left_title">Gender</div>
                                    <div class="right_text">Male</div>
                                    
                                </li>
                                <li>
                                    <div class="left_title">Active Since</div>
                                    <div class="right_text">May 8, 2018</div>
                                       
                                </li>
                                
                            </ul>
                        </div>
                    </div>
           		</div>
        	</div>
            
            
            
            
   <div class="order_box_tab_h fade in" id="reviews_b">
      	<div class="sort_review_box border-review">
                			<div class="review_box_left_h">
                                <div class="review_title">
                                    <a href="#">Good service</a>
                                </div>
                        <div class="review_status">Mi LED Smart TV 4A 108 cm (43)</div>
                        <div class="review_content">Let me tell you guys how I wound up getting this technological marvel first, 
My previous 32" TV broke down 9 months ago since then I was looking for a cheap yet the best TV in the market, I even went to showrooms during festival offer time to look if I will get any lu</div>
                       
                             
                        
</div>

                    
                <div class="review_box_right_h">
                        <div class="review_user_detail">
                            <div class="user_img">
                                <img src="assets/images/tv/user.png">
                            </div>
                            <div class="user_name">5153476443</div>
                        </div>
                        <div class="review_user_other_d">
                            <ul>
                                <li>
                                    <div class="left_title">Age</div>
                                    <div class="right_text">30</div>
                                </li>
                                <li>
                                    <div class="left_title">Gender</div>
                                    <div class="right_text">Male</div>
                                    
                                </li>
                                <li>
                                    <div class="left_title">Active Since</div>
                                    <div class="right_text">May 8, 2018</div>
                                       
                                </li>
                                
                            </ul>
                        </div>
                    </div>
           </div>
     </div>
  </div>
                    
                     
                     
                </div> -->
            </div>
        </div>
    </div>
</div>


<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>
<!-- 

<div class="footer">
	<div class="container-fluid">
     	<div class="row">
        
             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no_padding">
               <h1 class="footer_heading">SUPPORT</h1>
                   <ul class="footer_list">
                   	   <li><a href="#">Home</a></li>
                       <li><a href="#">About Us</a></li>
                       <li><a href="#">Privacy Policy</a></li>
                       <li><a href="#">FeedBack</a></li>
                       <li><a href="#">Contact us</a></li>
                       <li><a href="#">Help</a></li>
                    </ul>
                  </div>
                            
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                  <h1 class="footer_heading">Products</h1>
                      <ul class="footer_list">
                         <li><a href="#">Foxsky Tv</a></li>
                         <li><a href="#">Audio</a></li>
                         <li><a href="#">Smart Band</a></li>
                         <li><a href="#">Anti Theft Wallet</a></li>
                         <li><a href="#">Power Banks</a></li>
                         <li><a href="#">Accessories &amp; Support</a></li>
                      </ul>
                  </div>
                            
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                <h1 class="footer_heading no__bottom_space">FOLLOW Foxsky</h1>
                    <p class="p_txt_fotter">We want to hear from you!</p>
                        <div class="em-social">
                                    <a class="em-social-icon em-facebook f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-twitter f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-pinterest  f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-google f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    
                                </div>
                            </div>
                            
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                    <h1 class="footer_heading no__bottom_space">Toll Free Number</h1>
                       <h3 class="number_free">1800 5328 777</h3>
                          <h1 class="let_heading">LET'S STAY IN TOUCH</h1>
                             <p class="get_txk">Get updates on sales specials and more</p>
                                <div class="block block-subscribe">
                                    <form action="" method="post">
                                        <div class="block-content">
                                            <div class="form-subscribe-content">
                                                <div class="input-box">
                  <input type="text" name="email" title="Sign up for our newsletter" placeholder="Enter Email Address" id="newsemail" class="input-text required-entry validate-email">
              </div>
           <p id="returnmsgNew" style="width: 100%;float: left; color: #fff;"></p>
        <div class="actions"> <button type="submit" title="Subscribe" class="button" id="newslettersubmit"><span><span>Subscribe</span></span></button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                
                            </div>
                            

                            
        <div class="col-md-12 no_padding">
        	<span class="copyright">Copyright © 2017  Foxsky. All Rights Reserved.</span>
        </div>                
                            
       	   </div>
        </div>
   </div>
   
        
		<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			$(".navigation > ul > li > a").hover(function(){
				//alert("ddd");
				if($(this).next(".product_item_view").length > 0){
				$(".logo_row").addClass("show_nav");
				$(this).next(".product_item_view").css({"display":"block"});
				}
				else{
					$(".logo_row").removeClass("show_nav");
					$(".product_item_view").css({"display":"none"});
				}
			});
			$(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
				$(".product_item_view").css({"display":"none"});
			});
		});
		</script>
	<script src="assets/js/wow.min.js"></script>
  <script>new WOW().init();</script>
  
        -->

