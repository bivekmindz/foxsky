<div class="wrapper sub_nav_bg breadcrumbs">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                <a href="#">Home</a><img src="<?php echo base_url(); ?>assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
                <a href="#">All Products</a>
            </div>
        </div>
    </div>      
</div>



<div class="wrapper product-list-bg">
    <div class="container-fluid">
        <div class="row">
        

        
        
            <div class="goods-list">

<?php foreach ($allcat as $key => $val) { ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 no_padding">
                    <div class="goods-item">
                        <div class="goods-item-info">
                            <div class="figure figure-img">
                                <a href="<?php echo base_url().'product/'.str_replace(' ', '_', $val->ProductName).'-'.$val->ProId.'.html';?>"><img src="<?php echo base_url();?>images/thumimg/<?php echo $val->Image; ?>" alt=""></a>
                            </div>
                        </div>
                        <h2 class="title"><a href="<?php echo base_url().'product/'.str_replace(' ', '_', $val->ProductName).'-'.$val->ProId.'.html';?>"><?php echo $val->ProductName;   ?></a></h2>
                        <p class="price"><i class="fa fa-inr"></i><?php  echo $val->SellingPrice;  ?><!-- <del><i class="fa fa-inr"></i>500</del> --></p>
                        
                       <!--  
                       <div class="actions-cart">
                            <a href="#"><img src="assets/images/cart.jpg" alt="Cart" class="cart-margin">ADD TO CART</a>
                        </div> -->
                  </div>
                </div>
                
<?php } ?>

              
                
            </div>
            
          <!--   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
            <div class="pagination-main">
                
                      <ul class="pagination-list">
                        <li><a href="#"><span class="arrow-paginatin-left"></span></a></li>
                        <li><a href="#" class="active-gray">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#"><span class="arrow-paginatin-right"></span></a></li>
                      </ul>
                    
                </div>
            </div> -->
            
            
        </div>
    </div>      
</div>




<div class="wrapper">
    <div class="policies_white">
        <div class="container-fluid">
            <div class="row">
                <div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url(); ?>assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url(); ?>assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url(); ?>assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
