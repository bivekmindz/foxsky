<!-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> -->
<style type="text/css">
.status_btn {
margin: 8px 0px;
padding: 6px 8px;
background: #71af0e;
color: #fff;
border: none;
}
.sel_status {
float: left;
width: 125px;
padding: 4px 4px;
margin: 8px;
background: none;
border: solid 1px #ccc;
}
h3.my_acc_h {
    padding: 7px 8px;
    margin: 0;
    background: #71af0e;
    color: #fff;
    font-size: 20px;
    width: 100%;
    float: left;
}
.my_acc_h span a{text-align: right;
    display: inline-block;
    float: right;
    color: #fff;
    text-decoration: none;
    background: #62960f;
    padding: 7px;
    font-size: 14px;
    box-sizing: border-box;}
    .my_acc_h span a .fa{ margin:0 0 0 5px; }
.add_coment_canc{position:fixed;background:#fff;padding:20px;z-index:9998;left:50%;top:25%;width:300px;margin-left:-150px;border-radius:10px}
.add_coment_canc textarea{padding:10px 9px;width:100%;float:left;margin: 0px 5px 0px 0px;
    width: 260px;
    height: 103px;
    resize: none;}
.comen_ca,.comen_su{color:#fff;padding:7px 12px;float:left;text-decoration:none}
.comen_su{background:#00518C;margin:15px 0 0 57px;border:none}
.comen_ca{background:#4EA1DE;margin:15px 0 0 7px}
.add_coment_canc h3{margin:0;padding:10px 0 15px;font-size:20px}
.overlay-popup{position:fixed;background:rgba(0,0,0,.8);width:100%;z-index:9997;top: 0;
    left: 0;
    height: 100%;}
    .table_my2 table.pro_d_list td .pro_ces_second{ display: block; }

  .error-class{border-color:red !important}

    .hmt_gtotal  {
    width: 100%;
    float: left;
    padding: 18px 0 0 0;
}
 .hmt_gtotal span{ font-size: 16px; }
</style>
<div class="wrapper sub_nav_bg breadcrumbs">
  <div class="container-fluid">
   <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
       <a href="#">Home</a><img src="<?php echo base_url();  ?>assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
      
       My order
     </div>
   </div>
 </div>   
</div>

<div class="wrapper my_account">
  <div class="container-fluid">
   <div class="row">
     <div class="my_acount_main">
       <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
         <div class="my_nav">

           <h3 class="my_title">ORDERS</h3>
           <ul class="order_menu">
             <li><a href="#" class="current_active">My Orders</a></li>
             <!-- <li><a href="#">Returns</a></li> -->
           </ul>

           <h3 class="my_title">Foxsky</h3>
           <ul class="order_menu">
             <li><a href="<?php echo base_url();?>myaccount">My Account</a></li>
             <li><a href="<?php echo base_url();?>myreview">Reviews</a></li>
             <li><a href="<?php echo base_url();?>myaddress">Address Book</a></li>
           </ul>
         </div>
       </div>
       <?php  if($_GET['id']=='closed'){
          $closedli="active_review";
          $closeddata ="active_review_tab";
       }

        elseif ($_GET['id']=='return') {
         $returnli="active_review";
         $returndata="active_review_tab";

       }

       elseif ($_GET['id']=='shipping') {
         $shippingli="active_review";
         $shippingdata="active_review_tab";
       }
    else{
      $allli="active_review";
      $alldata="active_review_tab";
    }


       ?>

       <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no_padding">
         <div class="my_right">
          <h1>My Orders</h1>

          <div class="order_nav_tab">
           <ul class="order-list-range order_tab">
            <li onclick="hidden();"><a href="javascript:void(0);" id="pInfo" title="all" class="<?php echo $allli; ?>">ALL</a></li>
            <li onclick="hidden();"><a href="javascript:void(0);" id="firstphoto" title="return" class="<?php echo $returnli; ?>">Return</a></li>
            <li onclick="hidden();" ><a href="javascript:void(0);" id="firstphoto" title="shipping" class="<?php echo $shippingli; ?>">Shipping</a></li>
            <li><a href="javascript:void(0);" id="firstphoto" title="closed" class="<?php echo $closedli; ?>">Closed</a></li>
          </ul>
        </div>



        <div class="order_box_tab in <?php echo $alldata; ?>" id="all">
          <?php foreach($prodetails as $order=>$customerorder){  

$ordid=$customerorder->OrderId;
$payment_md=$customerorder->PaymentMode;

            ?>
              <input type="hidden" name="mode<?php echo $ordid; ?>" id="mode<?php echo $ordid; ?>" value="<?php echo $payment_md; ?>" >
          <div class="order_container">
            
                           <!--  <div class="order_select">
                            <div class="select_box_cencle">
                              <div class="form-group">
                                  <select class="form_control_1" id="sel1">
                                    <option>Select</option>
                                    <option>Return</option>
                                    <option>Cancel</option>
                                  </select>
                                </div>

                            </div>
                             -->
                             <?php  if($customerorder->ShipStatus!='returned' && $customerorder->ShipStatus!='dispatch'   && $customerorder->ShipStatus!='return'  && $customerorder->ShipStatus!='return cancel') { ?>
                             <tr style="float: left;"><td colspan="4"><select name="changestat<?php echo $ordid; ?>" id="changestat<?php echo $ordid; ?>" onchange="multichstat('<?php echo $ordid; ?>');" class="changestat<?php echo $ordid; ?> sel_status" >
                                        <option value="">Select Status</option>
                                        <?php if($customerorder->ShipStatus=='delivered') { ?>
                                        <option value="return processing">Return</option><?php } ?>
                                        <?php if($customerorder->ShipStatus=='pending' || $customerorder->ShipStatus=='onprocess' || $customerorder->ShipStatus=='hold' ) { ?>
                                        <option value="cancel">Cancel</option><?php } ?>
                                    </select>
                                    <button class="status_btn" onclick="change_stats('<?php echo $ordid; ?>');" >SUBMIT</button>
                                </td></tr><?php } ?>
                            <div class="submit_Cancel">
                            <!-- <input type="submit" class="btn__cancel" value="Submit" data-toggle="modal" data-target="#btn_cancel_ck" onclick=" orderstatuschange();"></input> -->
                            <!-- <input type="submit" class="btn__cancel" value="Submit" onclick=" orderstatuschange();"></input>
                            </div> -->
                            
                            </div> 
           <div class="order_cont_title">
             <h2></h2>
             <p>  <input style="display: none;" type="checkbox" class="stats_<?php echo $customerorder->ShipStatus.$ordid; ?>" id="stats_<?php echo $customerorder->ShipStatus.$ordid; ?>" name="pro_stats_chang<?php echo $ordid; ?>[]" value="<?php echo $customerorder->StatusOrderId;?>"></p>
           </div>

           <div class="order_cont_list">
             <ul class="order_list_left">
              <li><?php echo $customerorder->OrderDate;?></li>
              <li><?php echo $customerorder->ShippingName; ?></li>
              <li>Order number : <a href="#"><?php echo $customerorder->OrderNumber;?></a></li> 
                 <?php if($customerorder->ShipStatus!='return'){ ?>
                                            <?php if($customerorder->ShipStatus=='cancel') { ?>
                                            <li class="text-center"><span class="pro_ces pro_ces_second chstval<?php echo $ordid; ?>" id="ship_stas_<?php echo $customerorder->StatusOrderId;?>">Cancelled</span></li>
                                            <?php } else { ?>
                                            <li class="text-center"><span class="pro_ces pro_ces_second chstval<?php echo $ordid; ?>" id="ship_stas_<?php echo  $customerorder->StatusOrderId;?>"><?php echo ucfirst($customerorder->ShipStatus)?></span>
                                            <?php } ?>
                                            <span><?php //echo date_format(date_create($valu['CreatedOn']),"d-m-Y H:i:s"); ?></span></li>
                                            
                                            <?php  } else {
                                            if (substr($customerorder->Remark, 0, 17)=="Order Refund Case") {
                                            echo "<li class='text-center'><span class='pro_ces pro_ces_second'>Refund Complete</span><span>".date_format(date_create($customerorder->CreatedOn),"d-m-Y H:i:s")."</span></li>";
                                            } else{ echo "<li class='text-center'><span class='pro_ces pro_ces_second'>Replacement Complete</span>".date_format(date_create($customerorder->CreatedOn),"d-m-Y H:i:s")."<span></span></li>";
                                            }  } ?>
                                            
            </ul>

            <div class="order_list_right">
             Total <img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><span><?php echo $customerorder->FinalPrice;?></span>
           </div>

           <div class="order-unit-goods-info">
            <div class="order_list_one">
              <div class="col-lg-9">
                <div class="row">
                 <div class="or_left">
                  <div class="col-lg-2"><div class="order_picture"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $customerorder->ProImage;?>" alt=""></div></div>
                  <div class="col-lg-9">
                    <p class="p7"><?php echo $customerorder->ProName;?></p>
                    <p class="p7"><img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><?php echo $customerorder->ProPrice;?>X<?php echo $customerorder->OrdQty;?></p>
                  </div>
                </div>
              </div>
            </div>

          <input type="hidden" id="qunty_<?php echo $customerorder->StatusOrderId;?>" value="<?php echo $customerorder->OrdQty; ?>" min="1" max="<?php echo $customerorder->OrdQty; ?>">
            <div class="col-lg-3">
             <div class="row">
               <a href="<?php echo base_url();?>orderdetails/<?php echo base64_encode($customerorder->OrderId);?>/<?php echo $customerorder->ProId;?>" class="btn_paynow1">ORDER DETAILS</a>
             </div>
           </div>

         </div>
       </div>
     </div>
   </div>
   <?php } ?>
 </div>



 <div class="order_box_tab <?php echo $returndata; ?>" id="return">
 <?php foreach($return as $order=>$returned){ ?>
    <div class="order_container">
    
     <!-- <div class="order_cont_title">
       <h2>Waiting for payment</h2>
       <p>*Please complete the payment within 2 minutes 22 seconds . Unpaid orders (except for COD ones) will be cancelled automatically afterwards.</p>
     </div> -->

     <div class="order_cont_list">
       <ul class="order_list_left">
        <li><?php echo $returned->OrderDate;?></li>
        <li><?php echo $returned->ShippingName;?></li>
        <li>Order number : <a href="#"><?php echo $returned->OrderNumber;?></a></li> 
        <li class="text-center"><span class="pro_ces pro_ces_second chstval<?php echo $ordid; ?>" id="ship_stas_<?php echo  $customerorder->StatusOrderId;?>"><?php echo ucfirst($returned->ShipStatus)?></span>
                                
                                           </li>
      </ul>

      <div class="order_list_right">
       Total <img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><span><?php echo $returned->FinalPrice;?></span>
     </div>

     <div class="order-unit-goods-info">
      <div class="order_list_one">
        <div class="col-lg-9">
          <div class="row">

           <div class="or_left">
            <div class="col-lg-2"><div class="order_picture"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $returned->ProImage;?>" alt=""></div></div>
            <div class="col-lg-9">
              <p class="p7"><?php echo $returned->ProName;?></p>
              <p class="p7"><img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><?php echo $returned->ProPrice;?>X<?php echo $returned->OrdQty;?></p>
            </div>
          </div>
        </div>
      </div>
        <input type="hidden" id="qunty_<?php echo $returned->StatusOrderId;?>" value="<?php echo $returned->OrdQty; ?>" min="1" max="<?php echo $returned->OrdQty; ?>">
      <div class="col-lg-3">
       <div class="row">
         <!-- <a href="#" class="btn_paynow">Pay Now</a> -->
         <a href="<?php echo base_url();?>orderdetails/<?php echo base64_encode($returned->OrderId);?>/<?php echo $returned->ProId;?>" class="btn_paynow1">ORDER DETAILS</a>
       </div>
     </div>

   </div>
 </div>
</div>
</div>
<?php } ?>
   
 </div>
 <div class="order_box_tab <?php echo $shippingdata; ?>" id="shipping">
  <!-- <div class="order_box_tab active_review_tab in" id="all"> -->
    <?php foreach($shipping as $order=>$shipped){ ?>
    <div class="order_container">
    
     <!-- <div class="order_cont_title">
       <h2>Waiting for payment</h2>
       <p>*Please complete the payment within 2 minutes 22 seconds . Unpaid orders (except for COD ones) will be cancelled automatically afterwards.</p>
     </div> -->

     <div class="order_cont_list">
       <ul class="order_list_left">
        <li><?php echo $shipped->OrderDate;?></li>
        <li><?php echo $shipped->ShippingName;?></li>
        <li>Order number : <a href="#"><?php echo $shipped->OrderNumber;?></a></li> 
      </ul>

      <div class="order_list_right">
       Total <img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><span><?php echo $shipped->FinalPrice;?></span>
     </div>

     <div class="order-unit-goods-info">
      <div class="order_list_one">
        <div class="col-lg-9">
          <div class="row">

           <div class="or_left">
            <div class="col-lg-2"><div class="order_picture"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $shipped->ProImage;?>" alt=""></div></div>
            <div class="col-lg-9">
              <p class="p7"><?php echo $shipped->ProName;?></p>
              <p class="p7"><img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><?php echo $shipped->ProPrice;?>X<?php echo $shipped->OrdQty;?></p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3">
       <div class="row">
         <!-- <a href="#" class="btn_paynow">Pay Now</a> -->
         <a href="<?php echo base_url();?>orderdetails/<?php echo base64_encode($shipped->OrderId);?>/<?php echo $shipped->ProId;?>" class="btn_paynow1">ORDER DETAILS</a>
       </div>
     </div>

   </div>
 </div>
</div>
</div>
<?php } ?>
</div>
<!-- </div> -->

<div class="order_box_tab <?php echo $closeddata; ?>" id="closed">
<?php foreach($delivered as $order=>$delivered){ ?>
 <div class="order_container">
  
   <div class="order_cont_title">
     <h2>Closed</h2>
   </div>

   <div class="order_cont_list">
     <ul class="order_list_left">
  
       <li><?php echo $delivered->OrderDate;?></li>
       <li><?php echo $delivered->ShippingName;?></li>
       <li>Order number : <a href="#"><?php echo $delivered->OrderNumber;?></a></li>
     </ul>

     <div class="order_list_right">
       Total <img src="<?php echo base_url(); ?>assets/images/rupees-gay.png" alt=""><span><?php echo $delivered->FinalPrice;?></span>
     </div>

     <div class="order-unit-goods-info">
      <div class="order_list_one">
        <div class="col-lg-9">
          <div class="row">

           <div class="or_left">
            <div class="col-lg-2"><div class="order_picture"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $delivered->ProImage;?>" alt=""></div></div>
            <div class="col-lg-9">
              <p class="p7"><?php echo $delivered->ProName;?></p>
              <p class="p7"><img src="<?php echo base_url();  ?>assets/images/rupees-gay.png" alt=""><?php echo $delivered->ProPrice;?>X<?php echo $delivered->OrdQty;?></p>
            </div>
          </div>
        </div>
      </div>
        <input type="hidden" id="qunty_<?php echo $customerorder->StatusOrderId;?>" value="<?php echo $customerorder->OrdQty; ?>" min="1" max="<?php echo $customerorder->OrdQty; ?>">
      <div class="col-lg-3">
       <div class="row">
         <a href="<?php echo base_url();?>orderdetails/<?php echo base64_encode($delivered->OrderId);?>/<?php echo $delivered->ProId;?>" class="btn_paynow1">ORDER DETAILS</a>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<?php } ?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="wrapper">
 <div class="policies_white">
  <div class="container-fluid">
   <div class="row">
    <div class="footer_policies">
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
        <div class="service-policy">
          <a href="#">
           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy1.jpg" alt=""></div>
           <div class="policy_txt">
            <strong>Hassle-free replacement</strong>
            <br>
            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
          </div>
        </a>
      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
      <div class="service-policy pull_space">
        <a href="#">
         <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy2.jpg" alt=""></div>
         <div class="policy_txt">
          <strong>100% secure payments</strong>
          <br>
          <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
        </div>

      </a>
    </div>
  </div>

  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
    <div class="service-policy pull_right">
      <a href="#">
       <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy3.jpg" alt=""></div>
       <div class="policy_txt">
        <strong>Vast service network</strong>
        <br>
        <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
      </div>
    </a>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="hide" id="add_comnt_div">
    <div class="add_coment_canc">
        <h3>ADD Comment</h3>
        <form>
            <div style="margin-bottom: 10px;">
                <span class="rtnreplacebtn"><input type="radio" id="rtncondition" class="rtnreplacebtnrd" name="rtncondition" checked="checked" value="Order Replacement Case : " />  Replacement</span>
               <span class="rtnrefundbtn"><input type="radio" id="rtncondition" class="rtnrefundbtnrd" name="rtncondition" value="Order Refund Case : " />  Refund</span>
            </div>
            <input type="hidden" id="row_id" value="">
            <input type="hidden" id="row_stats" value="">
            <textarea id="comnt" placeholder="Add Comment Here..."></textarea>
            <a href="#" class="comen_su" onclick="update_order_pro();">Submit</a>
            <a href="javascript:void(0);" class="comen_ca" onclick="window.open('<?php echo base_url(uri_string()); ?>','_self')">Cancel</a>
        </form>
    </div>
    <div class="overlay-popup"></div>
</div>

<script type="text/javascript">
  // <!-- function orderstatuschange(){
  //   var id=$('#sel1').val();
  //   alert(id);
  //   if(id==Return){


  //   }
  //   else if(id==Cancel){

  //   }
  // }-->
  function change_stats(oid)
{
var changestat=$('.changestat'+oid).val();
var mode=$('#mode'+oid).val();
//alert(changestat);
var output = jQuery.map($(':checkbox[name=pro_stats_chang'+oid+'\\[\\]]:checked'), function (n, i) {
return n.value;
}).join(',');
//var asss=$('#mode'+oid).val();
  //var output = document.getElementById("stats_"+oid).value;
    //document.getElementById("demo").innerHTML = x;
//alert(output); return false;
if(changestat==''){
$('.changestat'+oid).css('border', '1px solid red');
return false;
} else {
if(output==''){
$('#checkpro'+oid).html('To return product has to be delivered first.');
return false;
} else {
 
if(changestat=='cancel'){
$('.rtnreplacebtn').css('display','none');
$(".rtnreplacebtnrd").prop("checked", false);
if(mode=='CCAVENUES' && changestat=='cancel'){
$(".rtnrefundbtnrd").prop("checked", true);
}
if(mode!='ONLINE' && changestat=='cancel'){
$('.rtnrefundbtn').css('display','none');
}
}
$('#checkpro'+oid).html('');
$('.changestat'+oid).css('border', '1px solid #ccc');
$('#row_id').val(output);
$('#row_stats').val(changestat);
$('#add_comnt_div').removeClass('hide');
}
}
}

function multichstat(oid){
 
var changestat=$('.changestat'+oid).val();
$('.chstval'+oid).each(function(){
if(changestat=='cancel'){
$('.stats_pending'+oid).css('display','block');
$('.stats_pending'+oid).attr('checked', true);
//$('.stats_dispatch'+oid).css('display','block');

$('.stats_dispatch'+oid).attr('checked', true);
$('.stats_delivered'+oid).css('display','none');
$('.stats_delivered'+oid).attr('checked', false);
}
if(changestat=='return processing'){
/********************vandana 17-04-2018 *****************/
/*$('.stats_delivered'+oid).css('display','block');
$('.stats_dispatch'+oid).css('display','none');
$('.stats_dispatch'+oid).attr('checked', false);
$('.stats_pending'+oid).css('display','none');
$('.stats_pending'+oid).attr('checked', false);*/
$('.stats_delivered'+oid).css('display','block');
$('.stats_delivered'+oid).attr('checked', false);



}
});
}

function update_order_pro()
{
if($('#comnt').val() == "")
{
//$('#comnt').css('border-color','red', 'important');
$('#comnt').addClass('error-class');
}else{
$('.comen_su').prop('onclick',null).off('click');
$('.comen_ca').prop('onclick',null).off('click');
var rtncondition=$('input[name=rtncondition]:checked').val();
if($("input:radio[name='rtncondition']").is(":checked")){
var fullcomment=rtncondition+$('#comnt').val();
} else {
var fullcomment=$('#comnt').val();

}
// alert(fullcomment);
var allid=$('#row_id').val();

var id='';
var arr = allid.split(',');
var j=0;
var countarr=arr.length;
for(i=0; i < arr.length; i++){
j++;
id= arr[i];
current_qty=$('#qunty_'+id).val();
// alert(current_qty); return false;
old_qty=$('#qunty_'+id).attr('max');
// alert(old_qty); return false;
old_stat=$('#ship_stas_'+id).text();
// alert(old_stat); return false;
$.ajax({
type:'post',
url:'<?php echo base_url()?>webapp/myaccount/comment',
data:{'statsid':id,'flag':$('#row_stats').val(),'comment':fullcomment,'cur_qty':current_qty,'old_qty':old_qty,'old_stat':old_stat},
async: false,
success:function(rep)
{
alert(rep); return false;
//console.log(rep);
//document.location.href=window.location.href;
}
});
if (j==countarr) {
// alert("dddddddd");
document.location.href=window.location.href;
}
//alert(id+" @@ "+current_qty+" @@ "+old_qty+" @@ "+old_stat);
}
//return false;
}
}
</script> 