<?php //echo "<pre>"; print_r($image->imgname);?>

    
<div class="wrapper sub_nav_bg">
  <div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="sub_nav">
            
        <!--     <div class="pull-left">
            <h2>Foxsky LED Smart TV 4A 32</h2>
                <ul class="sub_title">
                      <li><a href="#">|</a></li>
                      <li><a href="#">Mi LED Smart TV 4A 43</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="#">Mi LED Smart TV 4 32</a></li>
                     </ul>
            </div> -->

      <div class="pull-right">
              <ul class="sub_tv">

                      <li><a href="<?php echo base_url();?>product/<?php echo $urlname; ?>" class="active-font">Overview</a></li>
                      <li><a href="<?php echo base_url();?>productspecification/<?php echo $urlname; ?>">Specs</a></li>
                        <li><a href="<?php echo base_url();?>productaccessories/<?php echo $urlname; ?>">Accessories</a></li>
                        <li><a href="<?php echo base_url();?>productreview/<?php echo $urlname; ?>">Review</a></li>
                        <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>">F-code</a></li>
                         <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>" class="btn-small btn-orange">Buy Now</a></li>
                       
                    </ul>
              </div>
              </div>
            </div>
        </div>
    </div>
</div>    
<!--         <?php p($image); ?>
 -->       
<div class="wrapper overall_tv_one">
  <img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Img1;?>" alt="">
      <div class="container">
      <div class="title_tv_main">
                <h1 class="sub_title_tv wow fadeInUp animated"><?php if($image->Head1!=0) { echo $image->Head1; } ?><span class="red_tv"></span><span class="small_tv"></span></h1>
                <p class="title_tv wow fadeInUp animated"><?php if($image->Subhead1!=0) {  echo $image->Subhead1; } ?></p>
                <p class="xm-pro-price wow fadeInUp animated"><i class="fa fa-inr"></i><span class="xm-price"></span></p>
        </div>    
   </div>
  </div>
    
         
<div class="wrapper videoTv_section">
  <div class="container-fluid">
      <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="videos_adds">
                    <a href="#"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Vedio1;?>" alt="">
                    <span class="icon_tv_control"></span>
                    <p class="tv_product_title wow fadeInUp animated"></p>
                   </a>
               </div>
            </div>
            
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="videos_adds">
                    <a href="#"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Vedio2;?>" alt="">
                    <span class="icon_tv_control"></span>
                    <p class="tv_product_title wow fadeInUp animated"></p>
                   </a>
               </div>
            </div>
            
            
            
        </div>
    </div>
</div> 
     
 
<div class="tv_slider">
  <div class="tv_slider_heading">
        <p class="tv_slider_desc wow fadeInUp animated">World's thinnest LED TV</p>
        <h1 class="tv_slider_title wow fadeInUp animated">4.9mm Ultra-thin</h1>
        <p class="tip"></p>
      </div>
  <div id="myCarousel" class="carousel slide height-auto" data-ride="carousel">
    <!-- Indicators -->
  <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1" ></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
     <div class="carousel-inner">

 <?php  foreach ($slide as $key => $value) { 
if($key=='0')
{ $clssdata="active";}
else {  $clssdata=""; }

  ?>
<!-- //active -->
      <div class="item <?php echo $clssdata; ?> ">
        <img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $value->imgname;?>" alt="" width="100%">
      </div>

<?php }?>
  
    </div> 

    <!-- Left and right controls -->
    <a class="left carousel-control slider_none" href="#myCarousel" data-slide="prev">
      <span class="arrow-prev"><img src="<?php echo base_url();  ?>assets/images/tv/arrow-prev.png" alt=""></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control slider_none" href="#myCarousel" data-slide="next">
      <span class="arrow-next"><img src="<?php echo base_url();  ?>assets/images/tv/arrow-next.png" alt=""></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>



<div class="wrapper tv_4">
  <div class="container-fluid">
    <div class="row">
  <div class="display_tv">
        <p class="wow fadeInUp animated"><?php echo $image->Head2;?></p>
        <h1 class="wow fadeInUp animated"><?php echo $image->Subhead2;?></h1>
    </div>
    </div>
    </div>
    <div class="image_foxsky_tv">
      <img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image2;?>" alt="">
    </div>
    
</div>

<div class="wrapper section_audio">
<div class="container-fluid">
<div class="row">
  <div class="display_tv">
        <p class="wow fadeInUp animated"><?php echo $image->Head3;?></p>
        <h1 class="wow fadeInUp animated"><?php echo $image->Subhead3;?></h1>
      </div>
    </div>
  </div>
</div>


<div class="wrapper section_performance">
<div class="container-fluid">
  <div class="row">
    <div class="display_tv">
          <p class="wow fadeInUp animated"><?php echo $image->Head4;?></p>
          <h1 class="wow fadeInUp animated"><?php echo $image->Subhead4;?></h1>
      </div>
    </div>
 </div>
</div> 

<div class="wrapper section-params">
  <div class="container-fluid">
      <div class="row">
        <div class="blocks1">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <figure>
                  <div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image5;?>" alt=""></div>
                    <div class="thumbnail img_thumbnail"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image7;?>" alt=""></div>
                </figure>
            </div>
            
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="block-item">
                  <div class="system-icon"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image6;?>" alt=""></div>
                    <p class="wow fadeInUp animated">Bluetooth 4.0<br>Low energy</p>
                </div>
                
                
                <div class="block-item wifi">
                  <div class="system-icon"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image8;?>" alt=""></div>
                    <p class="wow fadeInUp animated">Wi-Fi<br>2.4GHz / 5GHz</p>
                </div>
              
            </div>
            </div>
        </div>
    </div>  
</div>


<div class="wrapper section-source">
<div class="container-fluid">
  <div class="row">
    <div class="display_tv">
          <p class="wow fadeInUp animated"><?php echo $image->Head9;?></p>
          <h1 class="wow fadeInUp animated"><?php echo $image->Subhead9;?></h1>
      </div>
    </div>
 </div>
 <div class="person"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image9;?>" alt=""></div>
</div> 


<!-- logo slider -->
<div class="logo-slider"> 
<ul class="bxslider">
  <li><img src="<?php echo base_url();  ?>assets/images/tv/tv-logo1.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/images/tv/tv-logo2.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/images/tv/tv-logo3.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/images/tv/tv-logo4.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/images/tv/tv-logo5.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/images/tv/tv-logo6.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/images/tv/tv-logo7.png" alt=""></li>
  <li><img src="<?php echo base_url();  ?>assets/images/tv/tv-logo8.png" alt=""></li>

</ul>

        
    <p class="tip-logo wow fadeInUp animated">Subscriptions/charges may be required for certain content. </p>
</div>

<div class="wrapper section-cup">
  <div class="container-fluid">
    <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="overall-cup-img">
                    <img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image10;?>" alt="">
                </div>
                <div class="cplive">
                   <p class="more-title"><?php echo $image->Head10;?></p>
                   <p class="desc wow fadeInUp animated"><?php echo $image->Subhead10;?></p>
                   <p class="tip">* Watch 2018 FIFA World Cup Russia in India on SONY LIV on Mi TV</p>
                 </div>
                 
            </div>
        </div>
    </div>
</div>


<div class="wrapper section-system-1">
  <div class="container-fluid">
    <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="block_section">
                  <div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image11;?>" alt=""></div>
                  <div class="text_recommeds">
                      <p class="wow fadeInUp animated"><?php echo $image->Head11;?></p>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="block_section">
                  <div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image12;?>" alt=""></div>
                  <div class="text_recommeds">
                      <p class="wow fadeInUp animated"><?php echo $image->Head12;?></p>
                    </div>
                </div>
            </div>
            
        </div>
  </div>
</div>


<div class="wrapper section-system-2">
  <div class="container-fluid">
    <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="block_section">
                  <div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image13;?>" alt=""></div>
                  <div class="text_recommeds s2">
                      <p class="wow fadeInUp animated"><?php echo $image->Head13;?></p>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="block_section">
                  <div class="thumbnail img_thumbnail img-thumbnail-no"><img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $image->Image14;?>" alt=""></div>
                  <div class="text_recommeds s2">
                      <p class="wow fadeInUp animated"><?php echo $image->Head14;?></p>
                        <p class="tip wow fadeInUp animated">*Supported on Android phones on same Wi-Fi network.</p>
                    </div>
                </div>
            </div>
            
        </div>
  </div>
</div>

<!-- slider part -->
<div class="tv_slider">
  <div id="myCarouse2" class="carousel slide height-auto" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarouse2" data-slide-to="0" class="active"></li>
      <li data-target="#myCarouse2" data-slide-to="1" class=""></li>
      <li data-target="#myCarouse2" data-slide-to="2" class=""></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <?php  foreach ($slidee as $key => $value) { ?>

      <div class="item active">
        <img src="<?php echo base_url();  ?>assets/overviewimages/<?php echo $value->bottomImagename;?>" alt="" width="100%">
      </div>

<?php }?>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control slider_none" href="#myCarouse2" data-slide="prev">
      <span class="arrow-prev"><img src="<?php echo base_url();  ?>assets/images/tv/arrow-prev.png" alt=""></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control slider_none" href="#myCarouse2" data-slide="next">
      <span class="arrow-next"><img src="<?php echo base_url();  ?>assets/images/tv/arrow-next.png" alt=""></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>



<div class="wrapper section-service">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-title section-title">
                    <h1 class="wow fadeInUp animated">Supports table top and wall mounting</h1>
                </div>
          </div>
      </div>
     </div>
</div> 

<div class="wrapper section-revolution">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h1 class="wow fadeInUp animated">SMi LED TV 4 138.8 cm (55) is here to <br>revolutionise how you watch TV</h1>
                </div>
            
            <ul class="items">
              <li>
                    <img src="<?php echo base_url();  ?>assets/images/tv/overall-revolution-img1.png" alt="">
                    <p>The "Apple of China" just showed off a TV <br>that's thinner than an iPhone — and it's gorgeous.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/images/tv/overall-revolution-img2.png" alt="">
                    <p>The base of Mi TV4 is intentionally designed to look transparent so it would appear as if it were "floating" wherever it is placed.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/images/tv/overall-revolution-img3.png" alt="">
                    <p>Xiaomi's Mi TV 4 manages to stand out even at a CES that has been punctuated with eye-catching TVs.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/images/tv/overall-revolution-img4.png" alt="">
                    <p>Xiaomi's Mi TV 4 manages to stand out even at a CES that has been punctuated with eye-catching TVs.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/images/tv/overall-revolution-img5.png" alt="">
                    <p>Xiaomi's modular Mi TV 4 is exactly the kind of innovation.we need in the U.S.</p>
                </li>
                
                <li>
                    <img src="<?php echo base_url();  ?>assets/images/tv/overall-revolution-img6.png" alt="">
                    <p>Xiaomi's Mi TV 4 sets a new standard for thin televisions.</p>
                </li>
                
            </ul>
            
            </div>
            
        </div>
     </div>
</div> 


<div class="wrapper section-see">
    <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <a href="#" class="see_more">See specs ></a>
          </div>
      </div>
     </div>
</div>




<div class="wrapper">
  <div class="policies_white">
    <div class="container-fluid">
      <div class="row">
            <div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="<?php echo base_url();  ?>assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
      </div>
        </div>
  </div>
</div>
</div>


<!-- <div class="footer">
  <div class="container-fluid">
      <div class="row">
        
             <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no_padding">
               <h1 class="footer_heading">SUPPORT</h1>
                   <ul class="footer_list">
                       <li><a href="#">Home</a></li>
                       <li><a href="#">About Us</a></li>
                       <li><a href="#">Privacy Policy</a></li>
                       <li><a href="#">FeedBack</a></li>
                       <li><a href="#">Contact us</a></li>
                       <li><a href="#">Help</a></li>
                    </ul>
                  </div>
                            
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                  <h1 class="footer_heading">Products</h1>
                      <ul class="footer_list">
                         <li><a href="#">Foxsky Tv</a></li>
                         <li><a href="#">Audio</a></li>
                         <li><a href="#">Smart Band</a></li>
                         <li><a href="#">Anti Theft Wallet</a></li>
                         <li><a href="#">Power Banks</a></li>
                         <li><a href="#">Accessories &amp; Support</a></li>
                      </ul>
                  </div>
                            
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                <h1 class="footer_heading no__bottom_space">FOLLOW Foxsky</h1>
                    <p class="p_txt_fotter">We want to hear from you!</p>
                        <div class="em-social">
                                    <a class="em-social-icon em-facebook f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-twitter f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-pinterest  f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    <a class="em-social-icon em-google f-left" title="em-sample-title" href="javascript:void(0)">
                                        <span class="fa fa-fw"></span>
                                    </a>
                                    
                                </div>
                            </div>
                            
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                    <h1 class="footer_heading no__bottom_space">Toll Free Number</h1>
                       <h3 class="number_free">1800 5328 777</h3>
                          <h1 class="let_heading">LET'S STAY IN TOUCH</h1>
                             <p class="get_txk">Get updates on sales specials and more</p>
                                <div class="block block-subscribe">
                                    <form action="" method="post">
                                        <div class="block-content">
                                            <div class="form-subscribe-content">
                                                <div class="input-box">
                  <input type="text" name="email" title="Sign up for our newsletter" placeholder="Enter Email Address" id="newsemail" class="input-text required-entry validate-email">
              </div>
           <p id="returnmsgNew" style="width: 100%;float: left; color: #fff;"></p>
        <div class="actions"> <button type="submit" title="Subscribe" class="button" id="newslettersubmit"><span><span>Subscribe</span></span></button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                
                            </div>
                            

                            
        <div class="col-md-12 no_padding">
          <span class="copyright">Copyright © 2017  Foxsky. All Rights Reserved.</span>
        </div>                
                            
           </div>
        </div>
   </div> -->
   
<!-- video popup-->

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title txt_pop">Foxsky Video Mobile</h4>
        </div>
        <div class="modal-body">
          <video width="100%" height="400" controls autoplay>
              <source src="http://192.168.1.30/foxsky/videos/Raazi - Official Trailer (HQ 360p).mp4" type="video/mp4">
              <source src="movie.ogg" type="video/ogg">
          </video>
          
        </div>
        
      </div>
    </div>
  </div>
</div>
   
        
    <script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    
    $(document).ready(function(){
      $(".navigation > ul > li > a").hover(function(){
        //alert("ddd");
        if($(this).next(".product_item_view").length > 0){
        $(".logo_row").addClass("show_nav");
        $(this).next(".product_item_view").css({"display":"block"});
        }
        else{
          $(".logo_row").removeClass("show_nav");
          $(".product_item_view").css({"display":"none"});
        }
      });
      $(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
        $(".product_item_view").css({"display":"none"});
      });
    });
    </script>
         <script src="assets/js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    
  <script src="assets/js/jquery.bxslider.min.js"></script>
     <script>
        $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 330,
        slideMargin: 0,
        ticker: true,
        speed: 30000
    });
       </script>
 

    
    

  </body>
</html>

        
    <script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    
    $(document).ready(function(){
      $(".navigation > ul > li > a").hover(function(){
        //alert("ddd");
        if($(this).next(".product_item_view").length > 0){
        $(".logo_row").addClass("show_nav");
        $(this).next(".product_item_view").css({"display":"block"});
        }
        else{
          $(".logo_row").removeClass("show_nav");
          $(".product_item_view").css({"display":"none"});
        }
      });
      $(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
        $(".product_item_view").css({"display":"none"});
      });
    });
    </script>
         <script src="assets/js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    
  <script src="assets/js/jquery.bxslider.min.js"></script>
     <script>
        $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 330,
        slideMargin: 0,
        ticker: true,
        speed: 30000
    });
       </script>