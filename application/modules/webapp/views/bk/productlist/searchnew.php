<?php
if(count($resultspage)>0){ ?>
<div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
<div class="sm_filter">
  <div class="fil_btn"><i class="fa fa-filter" aria-hidden="true"></i>filter</div><div class="short_btn"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i>sort by</div>
  </div>
</div>
<?php
$imgUrl = proimgurl().'images/';
$i=0;
foreach ($resultspage as $key => $prolistnew) { $i++;
  $prolist=(object)$prolistnew;
  $pid=$prolist->prodtmappid;
  $discount='';
  $price=$prolist->productMRP; 
  $productsize=explode(':', $prolist->Size);
  $productcolor=explode(':', $prolist->color);
?>
<div class="product_list proid_<?php echo $i?>" vtAlgin productid='<?php echo $prolist->prodtmappid?>'>
<script src="<?php echo js_url('weightchange.js');?>" type="text/javascript"></script>
<div class="product_tab">
<div class="qty_ad">
 <?php if($prolist->qty==0){ echo '<span style="color: red;">OUT OF STOCK</span>'; }else{ ?>
<span>Qty.</span>
<a href="javascript:void();"><span class="first2" data="<?php echo $pid?>" id="first2<?php echo $pid?>" onclick="cart_update_list($(this));"><i class="fa fa-minus-circle"></i></span></a> 
<input type="text" id="a1<?php echo $pid?>" value="1" readonly> 
<a href="javascript:void();"><span class="last2" data="<?php echo $pid?>" data-hrefnew="<?php echo $prolist->qty?>" id="last2<?php echo $pid?>" onclick="cart_update_list($(this));"><i class="fa fa-plus-circle"></i></span></a>
<?php } ?>
</div>
 <div class="product_tab_inner"> 
<div class="img_box">
<?php
 if($prolist->proQnty<=0){
 $div= '<div class="outofstock"></div>';
 }
 else
 {
 $div= '';
 }
 $pronamestr=str_replace(' ','-',$prolist->ProName);
 $pronamestrname = seoUrl(str_replace('/','-',$pronamestr));
 echo '<a href="'.base_url().'product/'.$pronamestrname.'-'.$prolist->prodtmappid.'.html'.'">'.$div.' 
<span><img id="mainImage" src="'.$imgUrl.'thumimg/'.$prolist->image.'" alt="'.$prolist->ProName.'"></span></a>';
 ?> 
</div>
<div class="product_info">
<div class="pro_name"><a href="<?php echo base_url().'product/'.$pronamestrname.'-'.$prolist->prodtmappid.'.html';?>">
 <?php echo ucfirst($prolist->ProName);?></a></div>
<div class="price">
<?php 
$cityprice =citybaseprice($prolist->FinalPrice,$prolist->cityvalue);
$prodtsc=number_format((1-$cityprice/$prolist->productMRP)*100,1,".","");
if($prolist->ProDiscount<=0){
 echo '<span class="new_p">Rs.'.$prolist->productMRP.'</span>';
 } else {
 if($prolist->ProDisType=='fixed')
 $distype='Rs. Off';
 else
 $distype='% Off';
 echo '<span class="old_p">Rs.'.$prolist->productMRP.'</span>
 <span class="new_p">Rs.'.$cityprice.'</span>';
 #echo '<div class="sale_ad">'.$prodtsc.'<small>'.$distype.'</small></div>';
}
 ?>
</div>

 <?php if($prodtsc>0){
   if($prodtsc != "" || $distype !=""){
     echo '<div class="sale_ad">'.$prodtsc.'<small>'.$distype.'</small></div>';
   }
   else{

   }}
  ?>

<!--<div class="selet_qtnty">
 <select class='weightattr'>
  <?php  $weightex=explode(',', $prolistnew->color);
      $weightInd='';
      foreach ($weightex as $key => $value) {
        
        $getweight=explode(':', $value);
        if($prolistnew->prodtmappid==$getweight[1]){
          $getwe=explode(':', $value);
          $weightInd=$getwe[0];
        
        } ?>
        <option <?php if($getweight[0]==$weightInd) echo 'selected'; ?> value="<?php echo $getweight[1].'_'.$i; ?>"><?php echo $getweight[0]; ?></option>
        <?php
      } 

      ?>
   
    </select>  
  </div>-->

   <?php if($prolist->qty==0){ 
   echo '<div class="p_link"><a href="'.base_url().'product/'.$pronamestrname.'-'.$prolist->prodtmappid.'.html'.'" style="background: #71af0e;padding: 5px 8px;color: #fff;display: inline-block;border-radius: 4px;text-decoration: none;"><i class="fa fa-history"></i> View Details</a></div>'; 
    }else{ ?>

<div class="p_link">
<form action="" method="post"> 
<?php 
  if($prolist->qty == 0 || $prolist->qty <= 0){
      $addClassdNoproduct = "";
      $disableButton = "true";
      $addtocartID = 'addtocart';
   }else{
      $addClassdNoproduct = "";
      $disableButton = "true";
      $addtocartID = 'addtocart';
   }

  if(in_cart_array($this->cart->contents(), $pid) == true){
      $addClassd = "disablebutton";
      $data_is_in_cart = 'true';
    } else { 
      $data_is_in_cart = 'false';
      $addClassd ="";
  }
?>
<?php if(!empty($sizearray)){ $data_with_size = "true";} else{$data_with_size = "false";} ?>
<span class="sizerror" id="sizerror<?=$prolist->prodtmappid?>"></span>
 <a href="javascript:void(0)" data-href="<?=$prolist->prodtmappid?>" data-hrefnew="<?php echo $data_is_in_cart; ?>" class="add_cart addtocart <?php echo $addClassdNoproduct;?> <?php echo $addClassd;?>" id="<?php echo $addtocartID; ?>"  data-seller-id="<?php echo $prolist->manufacturerid ?>" data-with-size='<?php  echo $data_with_size; ?>' data-with-size-val='<?php  echo $productsize[0] ?>' data-with-color-val='<?php  echo $weightInd ?>' data-is-in-cart ="<?php echo $data_is_in_cart; ?>"  data-is-listing-buyable="<?php echo $disableButton; ?>"  data-buy-listing-id = "<?php echo $pid;?>" type="submit" name="submit"><i class="fa fa-shopping-cart"></i> Add to Cart</a> 
<span class="sizerrorbuynow"></span>
</form> 
</div>
<div class="p_link2">
<?php  echo bizzinquiry($prolist->prodtmappid); ?>
<?php  echo listwishlistlink('view',$userid,$pid); ?>
<?php  echo bizzcompare($prolist->prodtmappid); ?>
</div>
<?php } ?>
</div>
</div>
</div>
</div>
<?php } ?>
<?php
?>
<script>
$(document).ready(function(){
  pro_img_s();
 $(".grid_v").click(function(){
  setTimeout( function(){ 
   pro_img_s();
  }  , 500 );
  });
  $(".list_v").click(function(){
  setTimeout( function(){ 
   pro_img_s();
  }  , 500 );
  });
 });
 $(window).resize(function(){
 setTimeout( function(){ 
   pro_img_s();
  }  , 500 );
  });
function pro_img_s(){
  var x = $(".img_box").width();
  $(".img_box a").height(x);
}
$(document).ready(function(){
 $(".fil_btn").click(function(){
 $(".left_filter").toggleClass("min_filter");
 });
 $(".close_f").click(function(){
  $(".left_filter").removeClass("min_filter")
 });
$(".short_btn").click(function(){type_view
$(".sort_by").toggleClass("short_link");
});
$(".close_s").click(function(){
  $(".sort_by").removeClass("short_link");
});
  });
</script>
<?php
}
  else if(empty($resultspage)){
  echo "done";
  }
?>