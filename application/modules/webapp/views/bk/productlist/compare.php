<div class="page_pro1" style="height:auto;padding:0px 0px 0px;">
            <div class="col-lg-12">
                <div class="row">
                    <div class="heading-box">
                        <div class="heading_name">
                            <a class="#">Home</a>
                            <a class="#"><i class="fa fa-angle-right"></i> Product Comparison</a>
                            
                           
                    </div>
                </div>
              </div>
           </div>



 <div class="main_space">
  <div class="col-lg-12">
  <h1 class="page_title">Product Comparison</h1>
  <div class="compare_tbl">
  <div class="compare_div">
  <div class="compa_hed">
  Product
  </div>
  <div class="compa_hed">
  Image
  </div>
  <div class="compa_hed">
  Price
  </div>
   <div class="compa_hed">
 saving
  </div>
  <div class="compa_hed">
  Model
  </div>
  <div class="compa_hed">
  Brand
  </div>
  <div class="compa_hed">
  Availability
  </div>
    <div class="compa_hed">
  Weight
  </div>
   <div class="compa_hed">
  Dimensions (L x W x H)
  </div>
  <?php foreach($attname as $key => $value){  ?>
    <div class="compa_hed"><?php echo $value->attname; ?></div>
  <?php } ?>
<!-- <div class="compa_hed">
  Quantity
  </div> -->
  
  <!-- <div class="compa_hed">
  Battery Operated
  </div>
  <div class="compa_hed">
 Brand
  </div>
  <div class="compa_hed">
 Gender
  </div>
   <div class="compa_hed">
 Character
  </div>
   <div class="compa_hed">
 Color
  </div>
  <div class="compa_hed">
 Size
  </div>
   <div class="compa_hed">
 Delivery Time
  </div>
  <div class="compa_hed">
 Qty.
  </div> -->
  </div>





  <!-- Content Columns -->
  <?php //p($procomp); ?>
 <?php foreach($procomp as $key => $value){ 
    if($value->ProDiscount<=0){
        $new_p = '<span class="new_p">Rs.'.$value->productMRP.'</span>';
     } 
     else{
         if($value->ProDisType=='fixed')
         $distype = 'Rs. Off';
         else
         $distype = '% Off';
        $new_p = '<span class="old_p">Rs.'.$value->productMRP.'</span>
        <span class="new_p">Rs.'.$value->FinalPrice.'</span><font style="color:#29980F;margin-left:5px;">'.number_format($value->ProDiscount,1,".","").'<small>'.$distype.'</small></font>
        ';
        $new_pp = '<div class="sale_ad">'.number_format($value->ProDiscount,1,".","").'<small>'.$distype.'</small></div>';
    }
?>

   <div class="compare_div">
    <div class="compa_name"><?php echo $value->ProductName;?></div>
<div class="compa_img">
        <?php //echo $new_pp; ?>
         <img src="<?php echo base_url();?>images/thumimg/<?php echo $value->Image;?>" style="height:100%;" alt="Mattel Pp Nb Tod Rocker-Euro">
  </div>
<div class="compa_text">
    <div class="price">
    <?php echo $new_p; ?>
    </div>
</div>
<div class="compa_text qty_price_d">
        <div class="qty_price">
                <table>
                    <tbody><tr><th>Qty.</th><th>Price <span>per unit</span></th><th>Discount</th></tr>
                    <tr><td><?php echo str_replace('to', ' to ', $value->qty1); ?></td><td>Rs. <?php echo cityqtyprice($value->FinalPrice,$value->cityvalue,$value->price) ; ?></td><td>
<?php echo number_format($value->ProDiscount,1,".","").'%';  ?> <?php echo ($value->price!="" && $value->price!=0)?'+ '.$value->price.'%':'';?></td></tr>
    <tr><td><?php echo str_replace('to', ' to ', $value->qty2); ?></td><td>Rs. <?php echo cityqtyprice($value->FinalPrice,$value->cityvalue,$value->price2) ; ?></td><td>
<?php echo number_format($value->ProDiscount,1,".","").'%';  ?> <?php echo ($value->price2!="" && $value->price2!=0)?'+ '.$value->price2.'%':'';?>
</td></tr>
    <tr><td><?php echo str_replace('to', ' to ', $value->qty3); ?></td><td>Rs. <?php echo cityqtyprice($value->FinalPrice,$value->cityvalue,$value->price3) ; ?></td><td>
<?php echo number_format($value->ProDiscount,1,".","").'%';  ?> <?php echo ($value->price2!="" && $value->price2!=0)?'+ '.$value->price2.'%':'';?>
</td></tr>
                </tbody></table>
         </div>
  </div>
   <div class="compa_text"><?php echo $value->SKUNO;?></div>
  <div class="compa_text"><?php echo $value->BrandName;?></div>
  <div class="compa_text"><?php if($value->proQnty==0){ echo "Out of Stock"; }else{ echo "In Stock"; }?></div>
  <div class="compa_text"><?php echo $value->weight;?>  </div>
  <div class="compa_text"><?php echo $value->volumetricweight;?>   </div>

     
    <?php 
    foreach ($viewattdata as $k => $val) {
        for ($i=0; $i < count($val); $i++) { 
            if($value->prodtmappid==$val[$i]->promapid) {
                if($val[$i]->attributeid==1){
                  if ($val[$i]->attvalue=='multicolor') {
                    $urlmulti=base_url().'images/imgpsh_fullsize.jpg';
                    echo '<div class="compa_text"><div style="width:20px;height:20px;margin: 0px auto;background-image:url('.$urlmulti.');background-size: cover;"></div></div>';
                  } else {
                    echo '<div class="compa_text"><div style="width:20px;height:20px;margin: 0px auto;background:#'.$val[$i]->attvalue.'"></div></div>';

                  }

                    
                } else {

                    echo '<div class="compa_text">'.$val[$i]->attvalue.'</div>';
                }
            }
        }
    } 
    
    ?>
    
    <!-- <div class="compa_text">3-7 days  </div>
    

    <div class="compa_text">3-7 days  </div>
    

    <div class="compa_text">3-7 days  </div>
    

    <div class="compa_text">3-7 days  </div>
    

    <div class="compa_text">3-7 days  </div>
    

    <div class="compa_text">3-7 days  </div>
    
    <div class="compa_text">3-7 days  </div> -->

    <!-- <div class="compa_text">
<div class="qty_add"><a href="#"><i class="fa fa-minus-circle"></i></a><input id="Text1" type="text"><a href="#"><i class="fa fa-plus-circle"></i></a></div>
  </div> -->
  <div class="compa_text">
  <!-- <input type="button" value="Add to Cart" onclick="addToCart('8573');" class="button"> -->
  <a class="remove" href="javascript:void[0];" onclick="$(this).parent().parent().remove();">Remove</a><!-- <a href="#" class="wishlist" title="add to Wishlist"><i class="fa fa-heart"></i> --></a>
  </div>
   </div>

  <!-- End Content Columns -->

<?php }//end foreach. ?>

</div>
    
  <div class="buttons">
    <div class="right"><a href="<?php echo base_url();?>home">Continue</a></div>
  </div>

                     
                           
                          
                    
            
            </div>
         </div>
         </div>
     </div>
     