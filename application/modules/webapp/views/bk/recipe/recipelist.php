<div class="wrapper">
	<div class="container">
		<div class="title_recipe">Popular Recipes</div>
		<div class="recipe_row">


		<?php foreach ($all_recipe as $key => $value) { ?>
		
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
					  <a href="<?php echo base_url()?>recipedetail/<?php echo encrypt($value->r_id); ?>">
						<img src="<?php echo base_url()?>assets/webapp/recipe_img/<?php echo $value->r_banner ;?>" alt="">
		  			  </a>
					</div>
				</div>
				<div class="recipe_name">
					<?php echo $value->r_title; ?>
				</div>
			</div>
		</div>

		<?php } ?>
		


		<!-- <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
						</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
	</div>
	<div class="recipe_row">
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
	</div>
	<div class="recipe_row">
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<a href="#">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</a>
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
		<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
			<div class="recipe_d">
				<div class="recipe_img_sec">
					<div class="recipe_img">
						<img src="<?php echo base_url()?>assets/webapp/images/recipe/break_fast.jpg" alt="">
					</div>
				</div>
				<div class="recipe_name">
					Breakfast Bread
				</div>
			</div>
		</div>
	</div> -->
	</div>
</div>