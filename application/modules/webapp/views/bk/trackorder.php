<div class="page_pro1" style="height:auto;padding:0px 0px 0px;background:none;">
    <div class="heading-box">
        <div class="row">
            <div class="col-lg-12 col-md-12">                
                <div class="heading_name">
                    <a href="<?php echo base_url(); ?>" style="text-decoration: none;">Home</a>
                    <a href="<?php echo base_url('myaccount'); ?>" style="text-decoration: none;"><i class="fa fa-angle-right"></i> My Account</a>
                    <a class="#"><i class="fa fa-angle-right"></i> Track Order</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="main_space">

            <div class="col-lg-12 col-md-12">
                <div class="row">
                   
                        <div class="my_box active_tab" id="myorder">
                            <h3 class="my_acc_h"><span>Track Order</span></h3>
                            <div class="recent_order"><b>ORDER NO:</b> <?php echo $main[2]; ?><span class="pull-right pr-15"><b> ORDER DATE: </b><?php echo $main[1]; ?></span></div>
                        <ul>
                        <?php 
                        foreach ($uniqueid as $key => $val) 
                        { 
                            echo '<li>';
                            echo '<div class="col-lg-12 col-md-12">';
                            echo '<div class="col-lg-3 col-md-3">';
                            echo '<img src="'.base_url($val['imgurl'].$val['Image']).'" style="width: 50px;">&nbsp;';
                            echo $val['ProductName'];
                            echo '</div>';
                            echo '<div class="col-lg-9 col-md-9">';
                            echo '<div class="checkout-progress">
                            <ul class="progress-bar-one">';
                               
                            foreach ($order_detail as $key => $value) 
                            { 
                                if($val['ProId']==$value['ProId'])
                                {
                                    echo '<li class="checkout-step1 ap_'.$val['ProId'].' visited-step">
                                      <span class="triangle2"></span>
                                      <span class="track">'.ucwords($value['ShipStatus']).'</span><span class="triangle after"></span>
                                    <span class="date-p">'.date_format(date_create($value['CreatedOn']),"d-m-Y H:i").'</span></li>';
                                    echo '<script type="text/javascript">
                                    $( window ).load(function() {
                                    $(".ap_'.$val['ProId'].'").last().addClass("current-step");
                                    });
                                    </script>';
                                }
                            } 
                            echo '</ul></div>';
                            echo '</div>';
                            echo '</div>';
                            echo '</li>';
                        }
                          ?>
                       </ul>
                     </div>
              </div>
            </div>

        </div>
    </div>
</div>
