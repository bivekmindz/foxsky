﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <div style="float:left;width:700px;height:auto;border:dashed 1px #ccc;padding:10px 10px;font-family:arial, helvetica, sans-serif;">
        <div style="float:left;width:100%;text-align:center;">
            <img src="<?php echo base_url();?>images/logo3.png">
        </div>
        <div style="float:left;width:100%;text-align:center;background:#F1F1F1;padding:8px 0px;font-family:Arial, Helvetica, sans-serif;color: #4FAB29;margin:5px 0px;">
           FoxSkyindia Password Assistance
        </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;">Dear We received a request to reset the password associated with this <span style="color:#4FAB29;"><?php echo $userdet->compemailid;?></span> address. If you made this request, please follow the instructions below.  </div>
       <div style="float:left;width:100%;font-weight:bold;padding:15px 0px 10px;font-size:13px;">Your password has been reset to <span style="color:#4FAB29;"><?php echo $userpass;?></span></div>
       
       <div style="float:left;width:100%;font-weight:bold;padding:15px 0px 10px;font-size:13px;">If you did not request to have your password reset you can safely ignore this email. Rest assured your customer account is safe. </div>
       
       <div style="float:left;width:100%;font-weight:bold;padding:15px 0px 10px;font-size:13px;">Once you have successfully entered the above password to FoxSkyindia, we will give instructions for resetting your password. </div>
       
       <div style="float:left;width:100%;font-weight:bold;padding:15px 0px 10px;font-size:13px;">FoxSkyindia will never e-mail you and ask you to disclose or verify your FoxSkyindia password, credit card, or banking account number. If you receive a suspicious e-mail with a link to update your account information, do not click on the link--instead, report the e-mail to FoxSkyindia for investigation. Greetings from <span style="color:#4FAB29;">FoxSkyindia.com</span></div>
      
        <div style="float:left;width:100%;font-size:12px;line-height:20px;padding:5px 0px;">In case of any trouble please feel free to call us on <b> +91 9711000000</b> or mail us on <b>sales@FoxSkyindia.com</b></div>
        <div style="float:left;width:100%;font-size:13px;line-height:20px;font-weight:bold;padding-top:20px;">
            Regards,<br />
            FoxSkyindia Team
        </div>

    </div>

</body>
</html>

