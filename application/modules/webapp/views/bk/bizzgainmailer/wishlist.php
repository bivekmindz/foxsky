﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
 <?php //p($mailer=(object)$mailer); ?>
    <div style="float:left;width:700px;height:auto;border:dashed 1px #ccc;padding:10px 10px;font-family:arial, helvetica, sans-serif;">
        <div style="float:left;width:100%;text-align:center;">
            <img src="<?php echo base_url();?>images/logo3.png">
        </div>
        <div style="float:left;width:100%;text-align:center;background:#F1F1F1;padding:8px 0px;font-family:Arial, Helvetica, sans-serif;color: #4FAB29;margin:5px 0px;">
            Wish List  
        </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;">Dear <span style="color:#4FAB29;"><?php echo $userdet->firstname.' '.$userdet->lastname;?></span> </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;">Thank you for visiting <span style="color:#4FAB29;">FoxSkyindia.com</span> </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;line-height:21px;">Here is a snapshot of your wish list. To get a detailed information about the product, please log-in to your account. </div>
        <table style="border-collapse:collapse;margin:0px;padding:0px;float:left;width:100%;font-size:13px;margin:15px 0px;border-left:solid 1px #ccc;border-top:solid 1px #ccc;">
            <tr><th style="border-bottom:solid 1px #ccc;padding:7px 0px;width:50px;border-right:solid 1px #ccc;">S.No.</th><th style="border-bottom:solid 1px #ccc;padding:7px 0px;border-right:solid 1px #ccc;">Pro. image</th><th style="border-bottom:solid 1px #ccc;padding:7px 0px;border-right:solid 1px #ccc;">Pro. Name</th><th style="border-bottom:solid 1px #ccc;padding:7px 0px;border-right:solid 1px #ccc;">Price</th>
            </tr>
            <?php $i=0; foreach ($mailer as  $value) { $i++; ?>
                
           
            <tr>
            <td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo $i; ?></td>
            <td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><img src="<?php echo base_url();?>images/hoverimg/<?php echo $value['imgpath'];?>" style="width:30px;"></td>
            <td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo $value['proname'];?></td>
            <td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo $cityprice =citybaseprice($value['finalprice'],$value['cityvalue']);?></td>
            </tr>
           <?php } ?>
        </table>
        <div style="float:left;width:100%;font-size:12px;line-height:20px;padding:5px 0px;">For any queries please call us on <b>+91 9711000000</b> or mail us on <b>sales@FoxSkyindia.com</b></div>
        <div style="float:left;width:100%;font-size:13px;line-height:20px;font-weight:bold;padding-top:20px;">
            Regards,<br />
            FoxSkyindia Team
        </div>

    </div>

</body>
</html>
