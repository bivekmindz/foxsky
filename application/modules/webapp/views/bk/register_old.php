<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
<style>
.overlay_new{display: none;}
#overlay_evt{display: none;}
input.error{border-bottom:1px solid red!important;}
select.error{border-bottom:1px solid red!important;}
textarea.error{border-bottom:1px solid red!important;}
.input_text ~ label.error{ position: relative;padding: 10px 0 0 0;}
.rsnd-otp{ background: #62BB45; color: #fff; text-transform: uppercase; font-size: 14px; font-weight: 600;padding: 10px 25px;border: none;border-radius: 22px; }
.rsnd-otp:hover, .rsnd-otp:focus{ background: #388e1c; outline: none; }
.mem_ch_input{/*margin-top: 15px !important;*/width: 100% !important;}
.active-lable {    position: absolute;
    font-size: 11px;
    top: -7px !important; 
    left: 5px;
    color: #028543;
}
/* .error{border:1px solid red;}
label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
.sessionerror{border:0px solid red!important; color:red; font-weight: normal; }*/
</style>
<script>
$( function() {
$("#mem_dob").datepicker({
changeMonth: true,
changeYear: true,
autoclose: true,
yearRange : 'c-50:c+1',
dateFormat: "dd-mm-yy"
});
$("#mem_doa").datepicker({
changeMonth: true,
changeYear: true,
autoclose: true,
yearRange : 'c-10:c+10',
dateFormat: "dd MM yy"
});
});
</script>
<script language='javascript'>
$(document).ready(function() {
$("#addCont1").validate({
rules: {
mem_fname     : "required",
mem_lname     : "required",
mem_shipstate : "required",
mem_shipcity  : "required",
mem_pincode:{
required: true,
digits: true,
maxlength: 6,
minlength: 6
},
mem_address   : "required",
mem_OTP       : "required",
mem_email:{
required: true,
email: true
},
mem_mobile:{
required: true,
digits: true,
minlength: 10
},
mem_pass:{
required: true,
minlength: 6
},
mem_conpass:{
required: true,
equalTo: "#mem_pass"
}
},
/* messages: {
mem_fname     : "First Name is required",
mem_lname     : "Last Name is required",
mem_email: {
required: "Email is required",
email: "Please enter a valid email"
},
mem_mobile: {
required: "Contact Number is required",
digits: "Please enter only digits"
},
mem_pass: {
required: "Password is required",
minlength: "Your password must be at least 6 characters long"
},
mem_conpass: {
required: "Confirm Password is required",
equalTo: "Please enter the same Password again"
}
},*/
submitHandler: function(form) {
var otp = $("#mem_OTP").val();
var cpotp = $("#mem_ch").val();
var facctype=$("#acctype2").val();
var fmem_fname=$("#mem_fname").val();
var fmem_lname=$("#mem_lname").val();
var fmem_email=$("#mem_email").val();
var fmem_mobile=$("#mem_mobile").val();
var fmem_pass=$("#mem_pass").val();
var fmem_shipstate=$("#mem_shipstate").val();
var fmem_shipcity=$("#mem_shipcity").val();
var fmem_compname=$("#mem_address").val();
var cstvalue = $("#mem_dob").val();
var tinvalue = $("#mem_doa").val();
var cstid=$("#mem_pincode").val();
var tinid='';
if(otp==cpotp){
$("#dlb").css('display', 'block');
$("#mem_submit").css('display', 'none');
$.ajax({
url: '<?php echo base_url(); ?>webapp/memberlogin/registernew',
type: 'POST',
data: {'facctype':facctype,'fmem_fname':fmem_fname,'fmem_lname':fmem_lname,'fmem_email':fmem_email,'fmem_mobile':fmem_mobile,'fmem_pass':fmem_pass,'fmem_shipstate':fmem_shipstate,'fmem_shipcity':fmem_shipcity,'fmem_cstid':cstid,'fmem_cstvalue':cstvalue,'fmem_tinid':tinid,'fmem_tinvalue':tinvalue,'fmem_compname':fmem_compname},
success: function (data){
//console.log(data);
//alert(data);return false;
$("#dlb").css('display', 'none');
$("#mem_submit").css('display', 'block');
if(data == 'existemail'){
$(".sessionerror").html('Email alerady exist.');
return false;
} else if(data == 'existmobileno'){
$("#mem_ch").val('');
$("#mem_OTP").val('');
$(".sessionerror").html('Mobile number already exist.');
return false;
} else {
if(data == 'manufacturer'){
$(".sessionerror").html("User successfully registered.");
//window.location = base_url + "manufacturer/login";
window.location = base_url + "home";
} else {
$(".sessionerror").html("User successfully registered.");
window.location = base_url + "home";
}
}
}
});
}
else
{
alert("OTP Password is Wrong.");
return false;
}
}
});
});
</script>
<!-- <div class="close_sign"></div> -->
<div class="container">
    <div class="left_login">
        <div class="login_logo">
            <img src="<?php echo base_url(); ?>assets/webapp/img_hm/logo-02.png">
        </div>
        <div class="social_l">
            <h4>Sign Up with Social</h4>
            <a href="#" class="facebook"><i class="fa fa-facebook-square" aria-hidden="true"></i>Sign up with facebook</a>
            <a href="#" class="google"><i class="fa fa-google-plus-square" aria-hidden="true"></i>Sign up with Google</a>
        </div>
    </div>
    <div class="right_login">
        <form method="post" action="" id="addCont1">
            <input type="hidden" id="acctype2" name="acctype" class="acctype" value="retail" />
            <h3>Sign Up</h3>
            <div class="sessionerror"></div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form_div">
                        <input type="text" id="mem_fname" name="mem_fname" class="input_text" placeholder="" required>
                        <label>First Name</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form_div">
                        <input type="text" id="mem_lname" name="mem_lname" class="input_text" placeholder="" required>
                        
                        <label>Last Name</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form_div">
                        <input type="text" id="mem_email" name="mem_email" class="input_text" placeholder="" required>
                        <label>E-mail</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form_div">
                        <input type="text" id="mem_mobile" maxlength=10  name="mem_mobile" class="input_text" placeholder="" required onblur="regmobotp()" onkeypress="javascript:return isNumber(event)">
                        <label>Mobile</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form_div">
                        <input type="password" id="mem_pass" name="mem_pass" class="input_text" placeholder="" required>
                        
                        <label>Password</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form_div">
                        <input type="password" id="mem_conpass" name="mem_conpass" class="input_text" placeholder="" required>
                        
                        <label>Confirm Password </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form_div">
                        <input class="input_text" type="text" id="mem_dob" name="mem_dob"/>
                        <label>Date of Birth</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form_div">
                        <input class="input_text" type="text" id="mem_doa" name="mem_doa"/>
                        <label>Date of Anniversary</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form_div">
                        <select required id="mem_shipstate" name="mem_shipstate" onchange="statecountry();" class="input_text">
                            <option value="">Select State</option>
                            <?php foreach ($viewshipstate as $key => $value) { ?>
                            <option value="<?php echo $value['stateid'];?>"><?php echo $value['statename'];?></option>
                            <?php } ?>
                        </select>
                        <label></label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form_div">
                        <select required id="mem_shipcity" name="mem_shipcity" class="input_text">
                            <option>Select City</option>
                        </select>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form_div">
                        <input type="text" id="mem_pincode" name="mem_pincode" class="input_text" maxlength="6" required onkeypress="javascript:return isNumber(event)" onkeyup="verifypincode()" >
                        <label>Pincode</label><span id="pinverify"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form_div">
                        <!-- <label>Address</label> -->
                        <textarea id="mem_address" name="mem_address" placeholder="Address"></textarea>
                    </div>
                </div>
            </div>
            <div id="otpmob" style="display: none;" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <p>One Time Password (OTP) has been sent to your Mobile. please Verify Your Mobile Number.
                        </p>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form_div">
                                    <input type="text" id="mem_OTP" name="mem_OTP" class="input_text" placeholder="" required onkeypress="javascript:return isNumber(event)">
                                    <div class="form_div">
                                        <input type="text" class="mem_ch_input" id="mem_ch" name="mem_ch" readonly/>
                                    </div>
                                    <label>OTP Password</label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">                                
                                <div class="form_div">                                    
                                    <input type="button" class="rsnd-otp" onclick="regmobotp();" value="Resend OTP">
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  </div> -->
            <div class="form_div">
                <div class="submit_d">
                    <input type="submit" name="mem_submit" id="mem_submit" value="Submit" class="submit_btn">
                    <span class="loadingBtn"  style="display:none; width: 96%; margin-left: 0;" id="dlb" ><i class="fa fa-spinner fa-spin"></i></span>
                </div>
                <div class="forgot_p"><span class="m_popup" data-sh="login" data-hi="sign_up">Already Member</span></div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
$('.form_reg input').focusin(function(){
$(this).prev().hide();
});
$('.form_reg input').focusout(function(){
if($(this).val() == ""){
$(this).prev().show();
}
else{
$(this).prev().hide();
}
});
$("input[name='acctype']").click(function() {
$('html, body').animate({
scrollTop: $("#myDiv").offset().top
}, 1000);
});
});
function statecountry(){
var  stid=$('#mem_shipstate').val();
$.ajax({
url: '<?php echo base_url()?>webapp/memberlogin/citystate',
type: 'POST',
data: {'stid': stid},
success: function(data){
//console.log(data);
//alert(data);return false;
//var  option_brand = '<option value="">Select City</option>';
//$('#mem_shipcity').empty();
$("#mem_shipcity").html(data.trim());
}
});
}
</script>
<script>
$(document).ready(function(){
$("input").focusout(function(){
if($("input").val() == ""){
$(this).next(label).addClass("active-lable");
}
else{
$(this).next(label).removeClass("active-lable");
}
});
});
</script>