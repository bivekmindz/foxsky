<?php
//p($this->session->userdata('logincity')->cmid);
//p($resultspage);die();
if(count($resultspage)>0){ ?>
<div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
<div class="sm_filter">
  <div class="fil_btn"><i class="fa fa-filter" aria-hidden="true"></i>filter</div><div class="short_btn"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i>sort by</div>
  </div>
</div>
<?php
$imgUrl = proimgurl().'images/';
foreach ($resultspage as $key => $prolistnew) {
  $prolist=(object)$prolistnew;
  $pid=$prolist->prodtmappid[0];
  $discount='';
  $price=$prolist->productMRP[0];
  $sizearrp=specialre($prolist->SizeVal[0]);
  $colorarrp=specialre($prolist->color[0]);
  $sizearrp1=explode(':',$sizearrp);
  $colorarrp1=explode(':',$colorarrp);
  $productsize=$sizearrp1[0];
  $productcolor=$colorarrp1[0];
?>
<div class="<?php echo (!empty($image_view))?$image_view:'product_grid'; ?>" vtAlgin productid='<?php echo $prolist->prodtmappid[0];?>'>
<div class="product_tab">
<div class="qty_ad"><span>Qty.</span>
<a href="javascript:void();"><span class="first2" data="<?php echo $pid?>" id="first2<?php echo $pid?>" onclick="cart_update_list($(this));"><i class="fa fa-minus-circle"></i></span></a> 
<input type="text" id="a1<?php echo $pid?>" value="1" > 
<a href="javascript:void();"><span class="last2" data="<?php echo $pid?>" data-hrefnew="<?php echo $prolist->qty[0]?>" id="last2<?php echo $pid?>" onclick="cart_update_list($(this));"><i class="fa fa-plus-circle"></i></span></a>
</div>
 <div class="product_tab_inner"> 
<div class="img_box">
<?php
 if($prolist->proQnty[0]<=0){
 $div= '<div class="outofstock"></div>';
 }
 else
 {
 $div= '';
 }
 $pronamestr=str_replace(' ','-',specialre($prolist->ProductName[0]));
 $pronamestrname = seoUrl(str_replace('/','-',$pronamestr));
 echo '<a href="'.base_url().'product/'.$pronamestrname.'-'.$prolist->prodtmappid[0].'.html'.'">'.$div.' 
<span><img id="mainImage" src="'.$imgUrl.'thumimg/'.getsearchprodimage($prolist->ProId[0],$prolist->prodtmappid[0]).'" alt="'.specialre($prolist->ProductName[0]).'"></span></a>';
 ?> 
</div>
<div class="product_info">
<div class="pro_name"><a href="<?php echo base_url().'product/'.$pronamestrname.'-'.$prolist->prodtmappid[0].'.html';?>">
 <?php echo specialre(ucfirst($prolist->ProductName[0]));?></a></div>
<div class="price">
<?php 
$sescmid=$this->session->userdata('logincity')->cmid;
if($prolist->cityid[0]==$sescmid){
  $cmcityvalue=$prolist->cityvalue[0];
} 
if($prolist->cityid[1]==$sescmid){
  $cmcityvalue=$prolist->cityvalue[1];
} 
if($prolist->cityid[2]==$sescmid){
  $cmcityvalue=$prolist->cityvalue[2];
}

$cityprice =citybaseprice($prolist->FinalPrice,$cmcityvalue);
$prodtsc=number_format((1-$cityprice/$prolist->productMRP[0])*100,1,".","");
if($prolist->ProDiscount<=0){
 echo '<span class="new_p">Rs.'.number_format($prolist->productMRP[0],1,".","").'</span>';
 } else {
 if($prolist->ProDisType[0]=='fixed')
 $distype='Rs. Off';
 else
 $distype='% Off';
 echo '<span class="old_p">Rs.'.number_format($prolist->productMRP[0],1,".","").'</span>
 <span class="new_p">Rs.'.$cityprice.'</span>';
 echo '<div class="sale_ad">'.$prodtsc.'<small>'.$distype.'</small></div>';
}
 ?>
</div>
<div class="qty_price">
<table>
   <tr><th>Qty.</th><th>Price <span>per unit</span></th><th>Discount</th></tr>
    <tr><td><?php echo str_replace('to', ' to ', $prolist->qty1[0]); ?></td><td>Rs.<?php echo cityqtyprice($prolist->FinalPrice,$cmcityvalue,$prolist->price1[0]) ; ?></td><td><?php echo $prodtsc.'%';  ?> <?php echo ($prolist->price1[0]!="" && $prolist->price1[0]!=0)?'+ '.number_format($prolist->price1[0],1,".","").'%':'';?></td></tr>
    <tr><td><?php echo str_replace('to', ' to ', $prolist->qty2[0]); ?></td><td>Rs.<?php echo cityqtyprice($prolist->FinalPrice,$cmcityvalue,$prolist->price2[0]) ; ?></td><td><?php echo $prodtsc.'%';  ?> <?php echo ($prolist->price2[0]!="" && $prolist->price2[0]!=0)?'+ '.number_format($prolist->price2[0],1,".","").'%':'';?></td></tr>
    <tr><td><?php echo str_replace('to', ' to ', $prolist->qty3[0]                                                                                                                                                                                                                                  ); ?></td><td>Rs. <?php echo cityqtyprice($prolist->FinalPrice,$cmcityvalue,$prolist->price3[0]) ; ?></td><td><?php echo $prodtsc.'%';  ?> <?php echo ($prolist->price3[0]!="" && $prolist->price3[0]!=0)?'+ '.number_format($prolist->price3[0],1,".","").'%':'';?></td> </tr>
</table>
</div>
<div class="p_link">
<form action="" method="post"> 
<?php 
  if($prolist->qty[0] == 0 || $prolist->qty[0] <= 0){
      $addClassdNoproduct = "";
      $disableButton = "true";
      $addtocartID = 'addtocart';
   }else{
      $addClassdNoproduct = "";
      $disableButton = "true";
      $addtocartID = 'addtocart';
   }

  if(in_cart_array($this->cart->contents(), $pid) == true){
      $addClassd = "disablebutton";
      $data_is_in_cart = 'true';
    } else { 
      $data_is_in_cart = 'false';
      $addClassd ="";
  }
?>
<?php if(!empty($sizearray)){ $data_with_size = "true";} else{$data_with_size = "false";} ?>
<span class="sizerror" id="sizerror<?=$prolist->prodtmappid?>"></span>
 <a href="javascript:void(0)" data-href="<?=$prolist->prodtmappid[0]?>" data-hrefnew="<?php echo $data_is_in_cart; ?>" class="add_cart addtocart <?php echo $addClassdNoproduct;?> <?php echo $addClassd;?>" id="<?php echo $addtocartID; ?>"  data-seller-id="<?php echo $prolist->manufacturerid[0] ?>" data-with-size='<?php  echo $data_with_size; ?>' data-with-size-val='<?php  echo $productsize ?>' data-with-color-val='<?php  echo $productcolor ?>' data-is-in-cart ="<?php echo $data_is_in_cart; ?>"  data-is-listing-buyable="<?php echo $disableButton; ?>"  data-buy-listing-id = "<?php echo $pid;?>" type="submit" name="submit"><i class="fa fa-shopping-cart"> Add to Cart</i></a> 
<span class="sizerrorbuynow"></span>
</form> 
</div>
<div class="p_link2">
<?php  $userid=$this->session->userdata('bizzgainmember')->id; ?>
<?php  echo bizzinquiry($prolist->prodtmappid[0]); ?>
<?php  echo listwishlistlink('view',$userid,$pid); ?>
<?php  echo bizzcompare($prolist->prodtmappid[0]); ?>
</div>
</div>
</div>
</div>
</div>
<?php } ?>
<?php
?>
<script>
$(document).ready(function(){
  pro_img_s();
 $(".grid_v").click(function(){
  setTimeout( function(){ 
   pro_img_s();
  }  , 500 );
  });
  $(".list_v").click(function(){
  setTimeout( function(){ 
   pro_img_s();
  }  , 500 );
  });
 });
 $(window).resize(function(){
 setTimeout( function(){ 
   pro_img_s();
  }  , 500 );
  });
function pro_img_s(){
  var x = $(".img_box").width();
  $(".img_box a").height(x);
}
$(document).ready(function(){
 $(".fil_btn").click(function(){
 $(".left_filter").toggleClass("min_filter");
 });
 $(".close_f").click(function(){
  $(".left_filter").removeClass("min_filter")
 });
$(".short_btn").click(function(){type_view
$(".sort_by").toggleClass("short_link");
});
$(".close_s").click(function(){
  $(".sort_by").removeClass("short_link");
});
  });
</script>
<?php
}
  else if(empty($resultspage)){
  echo "done";
  }
?>