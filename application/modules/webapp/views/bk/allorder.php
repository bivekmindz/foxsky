<style type="text/css">
    .table_my2 table.pro_d_list td .pro_ces_second{ display: block; }
</style>
<div class="page_pro1" style="height:auto;padding:0px 0px 0px;background:none;">
    <div class="heading-box">
        <div class="row">
            <div class="col-lg-12 col-md-12">                
                <div class="heading_name">
                    <a href="<?php echo base_url(); ?>" style="text-decoration: none;">Home</a>
                   <a href="<?php echo base_url('myaccount'); ?>" style="text-decoration: none;"><i class="fa fa-angle-right"></i> My Account</a>
                    <a class="#"><i class="fa fa-angle-right"></i> My All Orders</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="main_space">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                  
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="my_box active_tab" id="myorder">
                            <h3 class="my_acc_h">My All Orders</h3>
                            <div class="recent_order"></div>
                            <?php if($this->session->flashdata('messageords')){ ?>
                            <div class="recent_order" style="text-align: center;color: green;"><?php echo $this->session->flashdata('messageords'); ?></div>
                            <?php } ?>
                            <div class="order-overflow">
                                <table class="table_my2 table-responsive">
                                  <?php foreach ($order_detail as $key => $value) {
                                    $ordid          = $value['OrderId'];
                                    $OrderNumber    = $value['OrderNumber'];
                                    $payment_status = $value['PaymentStatus'];
                                    $payment_md     = $value['PaymentMode'];
                                    $details        = detailsorder($ordid);
                                    
                                    $idds=base64_encode($ordid.'_'.date_format(date_create($value['OrderDate']),"d-m-Y H:i").'_'.$OrderNumber);
                                    $track_order=base_url('track-order').'/'.$idds; 
                                    ?>
                                    
                                    <tr>
                                        
                                        <th><a href="#" class="ord_id"><?php echo $OrderNumber; ?></a></th>
                                        <th><strong>Order Date :  <?php echo date_format(date_create($value['OrderDate']),"d-m-Y H:i:s"); ?></strong></th>
                                        <th><strong>Order Type : <?php echo $payment_md; ?></strong></th>
                                      <!--   <th><a href="<?php echo $track_order;?>" class="ord_id" style="background: #d40078;">Track Order</a></th> -->
                                        <input type="hidden" name="mode<?php echo $ordid; ?>" id="mode<?php echo $ordid; ?>" value="<?php echo $payment_md; ?>" >
                                       
                                    </tr>
                                    <tr style="float: left;"><td colspan="4"></td></tr>
                                <tr><td colspan="4" style="text-align: center;color: red;"><span id="checkpro<?php echo $ordid; ?>"></span></td></tr>
                                <tr><td colspan="4">
                                    <table class="pro_d_list">
                                        <?php 
                                        $subtotal=$tax=0;
                                        foreach ($details as $key => $valu) { ?>
                                        <tr>
                                            <td><img src="<?php echo $valu['ProImage']?>" width="50" height="50"></td>
                                            <td><?php echo $valu['ProName']?><br>
                                            <?php if($valu['ordproduct_type']=='combo'){ ?><b>Combo : </b><?php echo $valu['ord_comboname']; } ?></td>
                                            <td>Total Qty -<b>
                                                <?php echo $valu['ProQty'];?>
                                            </b></td>
                                            <?php if($valu['ShipStatus']!='return'){ ?>
                                            <td class="text-center"><span class="pro_ces pro_ces_second chstval<?php echo $ordid; ?>" id="ship_stas_<?php echo $valu['StatusOrderId']?>"><?php echo ucfirst($valu['ShipStatus'])?></span>
                                            <span><?php echo date_format(date_create($valu['CreatedOn']),"d-m-Y H:i:s"); ?></span></td>
                                           
                                            <?php  } else {
                                            if (substr($valu['Remark'], 0, 17)=="Order Refund Case") {
                                            echo "<td class='text-center'><span class='pro_ces pro_ces_second'>Refund Complete</span><span>".date_format(date_create($valu['CreatedOn']),"d-m-Y H:i:s")."</span></td>";
                                            } else{ echo "<td class='text-center'><span class='pro_ces pro_ces_second'>Replacement Complete</span>".date_format(date_create($valu['CreatedOn']),"d-m-Y H:i:s")."<span></span></td>";
                                            }  } ?>
                                            
                                            <td>Unit price: <b><?php echo number_format($valu['base_prize'],1,".","");?></b></td>
                                            <td>Total Price: <b><?php echo number_format($valu['base_prize']*$valu['ProQty'],1,".","");?></b></td>
                                            
                                        </tr>
                                        <?php   $subtotal+=number_format($valu['base_prize']*$valu['ProQty'],1,".","");
                                        $amt=number_format($valu['base_prize']*$valu['ProQty'],1,".","");
                                        $tin_tax_amt=number_format($amt * $valu['protaxvat_p'] / 100,1,".","");
                                        $cst_tax_amt=number_format($amt * $valu['protaxcst_p'] / 100,1,".","");
                                        $entry_tax_amt=number_format($amt * $valu['protaxentry_p'] / 100,1,".","");
                                        $tax+=$total_tax_amt=$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
                                        $totproprice=$amt+$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
                                        $t_coup+=get_partialdiscountpercentamt($totproprice,$valu['couponper']);
                                        $t_wall+=get_partialdiscountpercentamt($totproprice,$valu['walletper']);
                                        }
                                        $to_coup=number_format($t_coup,1,".","");
                                        $to_wall=number_format($t_wall,1,".","");
                                        ?>
                                    </table>
                                </td></tr>
                                <tr><td colspan="4">
                                    <table><tr><td class="grand_t">
                                        <span>Grand Total:<span>RS. <?php
                                            $ordamts=number_format(number_format($subtotal,1,".","")+number_format($tax,1,".","")+number_format($value['ShippingAmt'],1,".","")-$to_coup-$to_wall,1,".","");
                                            if($ordamts==0.0){
                                            echo number_format(number_format($subtotal,1,".","")+number_format($tax,1,".","")+number_format($value['ShippingAmt'],1,".","")-$to_coup,1,".","");
                                            } else {
                                            echo $ordamts;
                                            }
                                        ?></span></span>
                                        <?php if(!empty($value['usedwalletamt']) && $value['usedwalletamt']!=0.0){ ?>
                                        <span>Wallet (-) :<span> <?php echo $to_wall; ?> </span></span>
                                        <?php } ?>
                                        <?php if(!empty($value['CouponAmt'])){ ?>
                                        <span>Counpon (-) :<span> <?php echo $to_coup; ?> </span></span>
                                        <?php } ?>
                                        <span>Shipping Charges :<span> <?php echo number_format($value['ShippingAmt'],1,".",""); ?> </span></span>
                                        <span>Tax : <span><?php echo number_format($tax,1,".",""); ?></span></span>
                                        <span>Sub Total :<span>RS. <?php echo number_format($subtotal,1,".",""); ?></span></span>
                                    </td></tr></table>
                                </td></tr>
                                <tbody>
                                    
                                    
                                    <?php } ?>
                                    
                                </table>
                            </div>
                            
                        </div>
                       </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
