<div class="black_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            	<h1 class="current_heading">Current Openings</h1>
            <div class="col-lg-1"></div>
                <div class="col-lg-2">
                    <div class="jtb_icon">
                    	<a href="#car_page" data-high="Business">
                        <img src="<?php echo base_url(); ?>assets/webapp/images/icon1.jpg"  alt="" />
                        <h2>Business<br />Development</h2>
                        </a>
                    </div> 
                </div>
                
                <div class="col-lg-2">
                    <div class="jtb_icon">
                    	<a href="#car_page" data-high="Network">
                        <img src="<?php echo base_url(); ?>assets/webapp/images/icon2.jpg"  alt="" />
                        <h2>Network<br />Development Manager</h2>
                        </a>
                    </div> 
                </div>
                
                
                <div class="col-lg-2">
                    <div class="jtb_icon">
                    	<a href="#car_page" data-high="Catalogue">
                        <img src="<?php echo base_url(); ?>assets/webapp/images/icon3.jpg"  alt="" />
                        <h2>Catalogue<br />Manager</h2>
                        </a>
                    </div> 
                </div>
                
                
                <div class="col-lg-2">
                    <div class="jtb_icon">
                    	<a href="#car_page" data-high="Procurement">
                        <img src="<?php echo base_url(); ?>assets/webapp/images/icon4.jpg"  alt="" />
                        <h2>Procurement<br />Manager</h2>

                        </a>
                    </div> 
                </div>
                
                
                <div class="col-lg-2">
                    <div class="jtb_icon" >
                    	<a href="#car_page" data-high="Corporate">
                        <img src="<?php echo base_url(); ?>assets/webapp/images/icon5.jpg"  alt="" />
                        <h2>Corporate<br />Sales</h2>
                        </a>
                    </div> 
                </div>
                <div class="col-lg-1"></div>
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">

        $(document).ready(function(){
         
          $("#regis").fadeIn();
       $(".jtb_icon a").click(function(){
        var op_tab = $(this).attr("data-high");
        $(".career_box").removeClass("heig_l")
        $("."+op_tab).addClass("heig_l");
       });
        });
         
    </script>





<div class="container" style="position: relative;">
<div id="car_page" style="position:absolute;top:-50px;"> </div>
<div class="career_bx">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    	<div class="career_box Business">
        	
            	<div class="business_contain">
            		<h2>Business Development</h2>
                    <p><strong>Sales Executive :</strong> B2B, counter to counter sales profile. Freshers are also welcome</p>
                    <p><strong>Sales Manager :</strong> B2B, counter to counter sales profile, key clients handling and online sales support. Candidates with 3+ years of experience from Toys/FMCG/Pharma/Stationery industry shall be preferred</p>
                    <p><strong>Regional Sales head :</strong> Sales team management, Corporate sales and Key accounts management. Candidates with 7+ years of experience from Toys/FMCG/Pharma/Stationery industry shall be preferred</p>
            	<div class="appy"> <a href="mailto:career@mindzshop.com">Apply</a></div>
                </div>
                
                
        </div>
        
                
     </div>

<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    	<div class="career_box Network">
        	
            	<div class="business_contain">
            		<h2>Network Development Manager</h2>
                    <p>B2B, distributors appointments for various exclusive product catalogue. Candidates with 7+ years of experience from Toys/FMCG/Pharma/Stationery industry shall be preferred. Further knowledge of various regional markets is must.

</p>
 <div class="appy"> <a href="mailto:career@mindzshop.com">Apply</a></div>
            	</div>
               
                
        </div>



        
        </div>


<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    	<div class="career_box Catalogue">
        	
            	<div class="business_contain">
            		<h2>Catalogue Manager</h2>
                    <p><strong>Toys Catalogue Manager :</strong> 3+ years of experience, existing relationship with Toys manufacturers, wholesalers and importers</p>
                    <p><strong>Stationery Catalogue Manager :</strong>  3+ years of experience, existing relationship with Stationery manufacturers, wholesalers and importers</p>
                    <p><strong>Novelties/Gift products Catalogue Manager :</strong>  3+ years of experience, existing relationship with Novelties/gift products manufacturers, wholesalers and importers</p>
            	 <div class="appy"> <a href="mailto:career@mindzshop.com">Apply</a></div>
                </div>
               
        </div>
        
        
</div>


<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="career_box business_space Procurement">
        	
            	<div class="business_contain">
            		<h2>Procurement Manager</h2>
                    <p>Fresher / MBA having great negotiation and relationship management skills. Outgoing, extrovert and good communication skills. Looking to be on streets and develop new relationship, explore new players is an ideal candidate for this profile. 

</p>
				<div class="appy"> <a href="mailto:career@mindzshop.com">Apply</a></div>
                   </div>
                   
                   
            	</div>
</div>



    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    
    <div class="career_box business_space Corporate">
        	
            	<div class="business_contain">
            		<h2>Corporate Sales</h2>
                    <p>10+ years of experience. Existing relationships with large corporate clients for distribution of goods. Key accounts management and new business development across India.</p>
   <div class="appy"> <a href="mailto:career@mindzshop.com">Apply</a></div>
            	</div>
             
        </div>
   </div>
</div>
</div>