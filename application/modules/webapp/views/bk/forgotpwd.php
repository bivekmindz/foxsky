<link rel="stylesheet" href="<?php echo base_url();  ?>assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/bootstrap-select.min.css">

<link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/app_css.css">
<link href="<?php echo base_url();  ?>assets/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url();  ?>assets/css/animate.css">

<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border-bottom:1px solid red!important;}
   /* select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }*/
    .sessionerror{border:0px solid red!important; color:red; font-weight: normal; }
    .error_msg
    {
       position: relative;
       width: 100%;
       float: left;
    }
    .error_msg span
    {
    position: absolute;
    top: -28px;
    left: 12px;
    }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont2").validate({
        // Specify the validation rules

          rules: {
            email_mobile     : "required"
        },
        // Specify the validation error messages
      /*  messages: {
            emailmob      : "Email / Mobile Number is required",
            acctype       : "Choose Account type",
            pwd           : {
                              required: "Password is required",
                              minlength: "Your password must be at least 6 characters long"
                            },
            
        },*/
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>
<?php if($this->session->flashdata('item')){ ?>
 <div class="pl_login">
    <i class="fa fa-times close_pl" onclick="window.location.href=window.location.href"> </i>
    <?php echo $this->session->flashdata('item'); ?></div>
          
<?php } ?>


<div class="container">
      <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="layout_sign-in top-margin">
                  <div class="lgnheader">
                      <div class="lgnheader-logo"><img src="assets/images/logo.png" alt=""></div>
                        <h4 class="header_tit_txt">Reset password</h4>
                        <p>if you forget your password, enter your email address & mobile number for new password</p>
                        <div class="tabs-login">                        
                          <div class="login_area">
                           
   <!-- First Start-->                          
                
                                                              
                               
<!-- Forgot - Password Start-->        
 <form method="post"  action="" id="addforgetpass">                  
 <div class="re-password">
      <div class="form-group">
        <label class="form-txt text_600">Enter email</label>
        <input type="text" name="forgetemail" id="forgetemail" class="input_text">
      </div>
                                  
                                  
 <div class="region_tip_text light_color"></div>
      <!-- <div class="form-group btns_bg">
    <button type="submit" class="btnadpt btn_orange btn_orangeH">Next</button>
      </div> -->

      <div class="form-group btns_bg">
    <input type="submit" class="btnadpt btn_orange btn_orangeH"  value="Submit" name="submit" id="emailSubmit">
      </div>
      <p><?php
        if($this->session->flashdata('success')){
          echo $this->session->flashdata('success');
        }?></p>
    

         </form>

</div>
                            
 <!-- Phone number Start --> 
                            
 
                                                       
 <!-- opt Start   -->  
                            
                               
     
                          </div>
                        </div>
                    
                   
 </div>
              
                <div class="policy-area">
                  <ul class="policy-select-list">
                      <li><a href="#">English</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                    <p class="reserved-intro">Foxsky Inc., All rights reserved</p>
                </div>
                
            </div>
        </div>
   </div> 
