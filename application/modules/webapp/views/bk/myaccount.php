<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<style type="text/css">
.status_btn {
margin: 8px 0px;
padding: 6px 8px;
background: #1b4863;
color: #fff;
border: none;
}
.sel_status {
float: left;
width: 125px;
padding: 4px 4px;
margin: 8px;
background: none;
border: solid 1px #ccc;
}
h3.my_acc_h {
    padding: 7px 8px;
    margin: 0;
    background: #1b4863;
    color: #fff;
    font-size: 20px;
    width: 100%;
    float: left;
}
.my_acc_h span a{text-align: right;
    display: inline-block;
    float: right;
    color: #fff;
    text-decoration: none;
    background: rgb(16, 54, 76);
    padding: 7px;
    font-size: 14px;
    box-sizing: border-box;}
    .my_acc_h span a .fa{ margin:0 0 0 5px; }
.add_coment_canc{position:fixed;background:#fff;padding:20px;z-index:9998;left:50%;top:25%;width:300px;margin-left:-150px;border-radius:10px}
.add_coment_canc textarea{padding:10px 9px;width:100%;float:left;margin: 0px 5px 0px 0px;
    width: 260px;
    height: 103px;
    resize: none;}
.comen_ca,.comen_su{color:#fff;padding:7px 12px;float:left;text-decoration:none}
.comen_su{background:#00518C;margin:15px 0 0 57px;border:none}
.comen_ca{background:#4EA1DE;margin:15px 0 0 7px}
.add_coment_canc h3{margin:0;padding:10px 0 15px;font-size:20px}
.overlay-popup{position:fixed;background:rgba(0,0,0,.8);width:100%;z-index:9997;top: 0;
    left: 0;
    height: 100%;}
    .table_my2 table.pro_d_list td .pro_ces_second{ display: block; }

  .error-class{border-color:red !important}
</style>
<!-- add comment section start -->
<div class="hide" id="add_comnt_div">
    <div class="add_coment_canc">
        <h3>ADD Comment</h3>
        <form>
            <div style="margin-bottom: 10px;">
                <span class="rtnreplacebtn"><input type="radio" id="rtncondition" class="rtnreplacebtnrd" name="rtncondition" checked="checked" value="Order Replacement Case : " />  Replacement</span>
                <span class="rtnrefundbtn"><input type="radio" id="rtncondition" class="rtnrefundbtnrd" name="rtncondition" value="Order Refund Case : " />  Refund</span>
            </div>
            <input type="hidden" id="row_id" value="">
            <input type="hidden" id="row_stats" value="">
            <textarea id="comnt" placeholder="Add Comment Here..."></textarea>
            <a href="javascript:void(0);" class="comen_su" onclick="update_order_pro();">Submit</a>
            <a href="javascript:void(0);" class="comen_ca" onclick="window.open('<?php echo base_url(uri_string()); ?>','_self')">Cancel</a>
        </form>
    </div>
    <div class="overlay-popup"></div>
</div>
<!-- add comment section end -->
<div class="page_pro1" style="height:auto;padding:0px 0px 0px;background:none;">
    <div class="heading-box">
        <div class="row">
            <div class="col-lg-12 col-md-12">                
                <div class="heading_name">
                    <a href="<?php echo base_url(); ?>" style="text-decoration: none;">Home</a>
                    <a class="#"><i class="fa fa-angle-right"></i> My Account</a>
                   <!--  <a class="#"><i class="fa fa-angle-right"></i> My Orders</a> -->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="main_space">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="my_accoutn">
                            <div class="my_acc">My Account</div>
                            <ul class="my_tab" style="padding:0; margin:0;">
                                
                                <li><a href="javascript:void(0);" title="myorder"><i class="fa fa-cart-arrow-down"></i>My Orders</a></li>
                                <li><a href="javascript:void(0);" title="personalinformation"><i class="fa fa-user"></i>Personal Information</a></li>
                                <li><a href="javascript:void(0);" title="changepassword"><i class="fa fa-unlock-alt"></i>Change Password</a></li>
                                
                                <li><a href="javascript:void(0);" title="mywallet"><i class="fa fa-inr"></i>My Wallet</a></li>
                                <li><a href="javascript:void(0);" title="mypayments"><i class="fa fa-inr"></i>My Payment Details</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <div class="my_box active_tab" id="myorder">
                            <h3 class="my_acc_h"><span>My Orders </span>
                            <span><a href="<?php echo base_url('all-order'); ?>" title="Order">All Orders<i class="fa fa-arrow-circle-right " aria-hidden="true"></i></a></span></h3>
                            <div class="recent_order">RECENT ORDERS</div>
                            <?php if($this->session->flashdata('messageords')){ ?>
                            <div class="recent_order" style="text-align: center;color: green;"><?php echo $this->session->flashdata('messageords'); ?></div>
                            <?php } ?>
                            <div class="order-overflow">
                                <table class="table_my2 table-responsive">
                                  <?php foreach ($order_detail as $key => $value) { #p($value);
                                    $ordid          = $value['OrderId'];
                                    $OrderNumber    = $value['OrderNumber'];
                                    $payment_status = $value['PaymentStatus'];
                                    $payment_md     = $value['PaymentMode'];
                                    $details        = detailsorder($ordid);
                                    $checkstatus    = checkstatusorder($ordid,$value['OrderDate']);
                                    #$checkstatus    = 0;
                                    #p($checkstatus);
                                    /*if(($payment_md=='ONLINE' AND $payment_status=='paid')  OR ($payment_md=='COD' AND $payment_status=='unpaid') OR ($payment_md=='CHEQUE' AND $payment_status=='unpaid') OR ($payment_md=='CREDIT' AND $payment_status=='unpaid') ) {*/
                                    if($payment_md=='ONLINE' OR $payment_md=='COD') {
                                    if($ordid!=$checkstatus){ # Start  
        $idds=base64_encode($ordid.'_'.date_format(date_create($value['OrderDate']),"d-m-Y H:i").'_'.$OrderNumber);
                                    $track_order=base_url('track-order').'/'.$idds; 
                                    ?>
                                    
                                    <tbody>
                                    <tr>
                                        
                                        <th><span class="ord_id"><?php echo $OrderNumber; ?></span></th>
                                        <th><strong>Order Date :  <?php echo date_format(date_create($value['OrderDate']),"d-m-Y H:i:s"); ?></strong></th>
                                        <th><strong>Order Type : <?php echo $payment_md; ?></strong></th>
                                        <th><a href="<?php echo $track_order;?>" class="ord_id" style="background: #71af0e;">Track Order</a></th>
                                        <input type="hidden" name="mode<?php echo $ordid; ?>" id="mode<?php echo $ordid; ?>" value="<?php echo $payment_md; ?>" >
                                        <!-- <th>
                                            <a href="javascript:void(0);" id="reordbutton<?php echo $ordid; ?>" class="retrn_btn" style="text-decoration: none;" onclick="reorder('<?php echo $ordid; ?>');">Repeat Order</a>
                                            <i class="fa fa-spinner fa-spin hide" id="load<?php echo $ordid; ?>" style="font-size: 21px;"></i>
                                            
                                        </th> -->
                                    </tr>
                                    <tr style="float: left;"><td colspan="4"><select name="changestat<?php echo $ordid; ?>" id="changestat<?php echo $ordid; ?>" onchange="multichstat('<?php echo $ordid; ?>');" class="changestat<?php echo $ordid; ?> sel_status" >
                                        <option value="">Select Status</option>
                                        <option value="return processing">Return</option>
                                        <option value="cancel">Cancel</option>
                                    </select>
                                    <button class="status_btn" onclick="change_stats('<?php echo $ordid; ?>');" >SUBMIT</button>

                                </td>
                               

                                </tr>
                                <tr><td colspan="4" style="text-align: center;color: red;"><span id="checkpro<?php echo $ordid; ?>"></span></td></tr>
                                <tr><td colspan="4">
                                    <table class="pro_d_list">
                                        <?php //p($details);
                                        $subtotal=$tax=0;
                                        foreach ($details as $key => $valu) { ?>
                                        <tr>
                                            <td>
                                                <input style="display: none;" type="checkbox" class="stats_<?php echo $valu['ShipStatus'].$ordid; ?>" id="stats_<?php echo $valu['ShipStatus'].$ordid; ?>" name="pro_stats_chang<?php echo $ordid; ?>[]" value="<?php echo $valu['StatusOrderId']?>">
                                                
                                            </td>
                                            <td><img src="<?php echo $valu['ProImage']?>" width="50" height="50"></td>
                                            <td><?php echo $valu['ProName']?><br>
                                            <?php if($valu['ordproduct_type']=='combo'){ ?><b>Combo : </b><?php echo $valu['ord_comboname']; } ?></td>
                                            <td>Total Qty -<b>
                                                <?php if($valu['ShipStatus']!='return' && $valu['ShipStatus']!='cancel' && $valu['ShipStatus']!='return processing' && $valu['ShipStatus']!='return cancel'){ ?>
                                                <input type="hidden" id="qunty_<?php echo $valu['StatusOrderId']?>" value="<?php echo $valu['ProQty']?>" min="1" max="<?php echo $valu['ProQty']?>"> <?php }else{ #echo $valu['ProQty']; ?>
                                                
                                                <?php } echo $valu['ProQty'];?>
                                            </b></td>
                                            <?php if($valu['ShipStatus']!='return'){ ?>
                                            <td class="text-center"><span class="pro_ces pro_ces_second chstval<?php echo $ordid; ?>" id="ship_stas_<?php echo $valu['StatusOrderId']?>"><?php echo ucfirst($valu['ShipStatus'])?></span>
                                            <span><?php echo date_format(date_create($valu['CreatedOn']),"d-m-Y H:i:s"); ?></span></td>
                                            
                                            <?php  } else {
                                            if (substr($valu['Remark'], 0, 17)=="Order Refund Case") {
                                            echo "<td class='text-center'><span class='pro_ces pro_ces_second'>Refund Complete</span><span>".date_format(date_create($valu['CreatedOn']),"d-m-Y H:i:s")."</span></td>";
                                            } else{ echo "<td class='text-center'><span class='pro_ces pro_ces_second'>Replacement Complete</span>".date_format(date_create($valu['CreatedOn']),"d-m-Y H:i:s")."<span></span></td>";
                                            }  } ?>
                                            
                                            <td>Unit price: <b><?php echo number_format($valu['base_prize'],1,".","");?></b></td>
                                            <td>Total Price: <b><?php echo number_format($valu['base_prize']*$valu['ProQty'],1,".","");?></b></td>


                                           <td>


                                        
                                            <?php  if($valu['ShipStatus']=='delivered')
                                            
                                            {
                                                 $url=base_url('rate').'/'.$ordid; 
                                             ?>
                                             <a href="#"  class="m_popup" data-sh="rate" ><i class="fa fa-unlock-alt fa-lg"></i>Rate this Product</a>
                                         
                                            

                                              <? } ?>
                                              </td>
                                           
                                            
                                        </tr>
                                        <?php   $subtotal+=number_format($valu['base_prize']*$valu['ProQty'],1,".","");
                                        $amt=number_format($valu['base_prize']*$valu['ProQty'],1,".","");
                                        $tin_tax_amt=number_format($amt * $valu['protaxvat_p'] / 100,1,".","");
                                        $cst_tax_amt=number_format($amt * $valu['protaxcst_p'] / 100,1,".","");
                                        $entry_tax_amt=number_format($amt * $valu['protaxentry_p'] / 100,1,".","");
                                        $tax+=$total_tax_amt=$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
                                        $totproprice=$amt+$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
                                        $t_coup+=get_partialdiscountpercentamt($totproprice,$valu['couponper']);
                                        $t_wall+=get_partialdiscountpercentamt($totproprice,$valu['walletper']);
                                        }
                                        $to_coup=number_format($t_coup,1,".","");
                                        $to_wall=number_format($t_wall,1,".","");
                                        ?>
                                    </table>
                                </td></tr>
                                <tr><td colspan="4">
                                    <table><tr><td class="grand_t">
                                        <span>Grand Total:<span>RS. <?php
                                            $ordamts=number_format(number_format($subtotal,1,".","")+number_format($tax,1,".","")+number_format($value['ShippingAmt'],1,".","")-$to_coup-$to_wall,1,".","");
                                            if($ordamts==0.0){
                                            echo number_format(number_format($subtotal,1,".","")+number_format($tax,1,".","")+number_format($value['ShippingAmt'],1,".","")-$to_coup,1,".","");
                                            } else {
                                            echo $ordamts;
                                            }
                                        ?></span></span>
                                        <?php if(!empty($value['usedwalletamt']) && $value['usedwalletamt']!=0.0){ ?>
                                        <span>Wallet (-) :<span> <?php echo $to_wall; ?> </span></span>
                                        <?php } ?>
                                        <?php if(!empty($value['CouponAmt'])){ ?>
                                        <span>Counpon (-) :<span> <?php echo $to_coup; ?> </span></span>
                                        <?php } ?>
                                        <span>Shipping Charges :<span> <?php echo number_format($value['ShippingAmt'],1,".",""); ?> </span></span>
                                        <span>Tax : <span><?php echo number_format($tax,1,".",""); ?></span></span>
                                        <span>Sub Total :<span>RS. <?php echo number_format($subtotal,1,".",""); ?></span></span>
                                    </td></tr></table>
                                </td></tr>
                                </tbody>
                                    
                                    <?php } # Start  
                                    } }?>
                                    
                                </table>
                            </div>
                            
                        </div>



                        

                       
                        
                        
                        <!--Personal Information-->
                        <div class="my_box" id="personalinformation">
                            <div class="recent_order">General Information</div>
                            <form action="" name="profile_detail" id="profile_detail" method="POST" enctype="multipart/form-data"><br>
                                
                                <div class="form_box">
                                    <div class="col-lg-2 col-md-2">First Name</div>
                                    <div class="col-lg-4 col-md-4"> <input type="text" name="memfname" class="form-control" id="memfname" value="<?php echo $memview->firstname;?>"></div>
                                    
                                    <div class="col-lg-2 col-md-2">Last Name</div>
                                    <div class="col-lg-4 col-md-4"> <input type="text" name="memlname" class="form-control" id="memlname" value="<?php echo $memview->lastname;?>"></div>
                                    
                                </div>
                                
                                <div class="form_box">
                                    <div class="col-lg-2 col-md-2">Email Number</div>
                                    <div class="col-lg-4 col-md-4"> <input type="text" name="mememail" class="form-control" id="mememail" value="<?php echo $memview->compemailid;?>" readonly></div>
                                    
                                    <div class="col-lg-2 col-md-2">Mobile Number</div>
                                    <div class="col-lg-4 col-md-4"><input type="text" name="memcontact" class="form-control" id="memcontact" value="<?php echo $memview->contactnumber;?>" readonly></div>
                                    
                                </div>

                                 <div class="form_box">
                                    <div class="col-lg-2 col-md-2">Date Of Birth</div>
                                    <div class="col-lg-4 col-md-4"> <input type="text" name="memdob" class="form-control" id="memdob" value="<?php echo $memview->cust_dob;?>" readonly></div>
                                    
                                    <div class="col-lg-2 col-md-2">Date of Anniversary</div>
                                    <div class="col-lg-4 col-md-4"><input type="text" name="memdoa" class="form-control" id="memdoa" value="<?php echo $memview->cust_doa;?>" readonly></div>
                                    
                                </div>

                                <div class="form_box">
                                    
                                    <div class="col-lg-2 col-md-2">State</div>
                                    <div class="col-lg-4 col-md-4">
                                        <select class="form-control" name="memstate" onchange="state_country();" id="memstate">
                                            <option value="">Select State</option>
                                            <?php foreach ($viewshipstate as $key => $value) { ?>
                                            <option value="<?php echo $value['stateid'];?>" <?php if($memview->stateid==$value['stateid']){echo "selected";} ?> ><?php echo $value['statename'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 col-md-2">City</div>
                                    <div class="col-lg-4 col-md-4">
                                        <select class="form-control" name="memcity" id="memcity" onchange="$(document).ready(function(){ $('#memprofile').prop('disabled', false); });">
                                            <option value="">Select City</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form_box">
                                <div class="col-lg-2 col-md-2">Pincode</div>
                                    <div class="col-lg-4 col-md-4"> <input type="text" name="mempincode" class="form-control" id="mempincode" value="<?php echo $memview->venpincode;?>"></div>
                                    <div class="col-lg-2 col-md-2">Address</div>
                                    <div class="col-lg-4 col-md-4"> <textarea name="memaddress" class="form-control" id="memaddress" style="margin: 0px -12.3438px 0px 0px; width: 251px; height: 118px;    resize: none;"><?php echo $memview->vendoraddress;?></textarea></div>
                                    
                                </div>
                               
                                
                                <div class="form_box">
                                    <div class="col-lg-2 col-md-2"></div>
                                    <div class="col-lg-4 col-md-4">
                                        <input type="submit" class="memprofile" name="memprofile" id="memprofile" onclick="chstatecity()" value="Update">
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                        
                        
                        <!--Change Password-->
                        
                        <div class="my_box" id="changepassword">
                            <span class="success" id="confirmpassword"></span>
                            <div class="recent_order">Change Password</div>
                            <form name="profile_detail" id="profile_detail" method="POST"><br>
                                <div class="form_box">
                                    <span id="oldpassword" ></span>
                                    <div class="col-lg-2 right_select">Old Password</div>
                                    <div class="col-lg-4"> <input type="password" class="form-control" onblur="checkpass()" id="old_password"></div>
                                    
                                </div>
                                
                                <div class="form_box">
                                    <div class="col-lg-2 right_select">New Password</div>
                                    <div class="col-lg-4"> <input type="password" class="form-control" id="new_password"></div>
                                    
                                </div>
                                
                                <div class="form_box">
                                    <div class="col-lg-2 right_select">Confirm Password</div>
                                    <div class="col-lg-4"> <input type="password" class="form-control" id="confirm_password"></div>
                                    
                                </div>
                                <div class="form_box">
                                    <div class="col-lg-2 right_select"></div>
                                    <div class="col-lg-4">
                                        <input type="button" class="continue" name="save_profile" id="save_profile" onclick="return passChange()"  value="Save Changes">
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                        
                        <!--My Reviews & Ratings-->
                        
                        <!-- <div class="my_box" id="mycoupon">
                            <div class="recent_order">My Reviews & Ratings</div>
                            <div class="twotab">
                                <span class="firsttab active">Product Reviews</span> <span class="firsttab">Seller Reviews</span>
                            </div>
                            <p>No Orders Found!!</p>
                            
                        </div> -->
                        
                        
                        
                        
                        
                        <!-- MY coupon -->
                        <div class="my_box" id="mycoupon">
                            <div class="recent_order">My Coupon</div>
                            
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>Coupon Code</th>
                                        <th>Purchase Amount</th>
                                        <th>Discount Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($coupon1 as $value){
                                    
                                    ?>
                                    <tr class="copon_center">
                                        <td><?php echo $value->CouponCode; ?></td>
                                        <td><?php echo $value->PurchaseAmt; ?></td>
                                        <td><?php echo $value->DiscountAmt; ?></td>
                                    </tr>
                                    <?php } ?>
                                    
                                </tbody>
                            </table>
                            
                        </div>
                        <!-- MY wallet -->
                        <div class="my_box" id="mywallet">
                            <div class="recent_order">Welcome to your Wallet</div>
                            <?php $totfixwalletamt=0;
                            foreach($walletdrcr as $value){
                            $totfixwalletamt = number_format($totfixwalletamt,1,".","")+number_format($value['w_amount'],1,".","");
                            }
                            ?>
                            <div class="recent_order" style="font-size: 18px;font-weight: bold;">Total Balance : Rs. <?php echo number_format($totfixwalletamt,1,".","");?></div>
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>Order Id</th>
                                        <th>Debited Amount (-)</th>
                                        <th>Credited Amount (+)</th>
                                        <th>Total Amount</th>
                                        <th>Date</th>
                                        <th>Transfer Reason</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($walletdrcr->scalar!='Something Went Wrong'){
                                    $fixwalletamt=0;
                                    foreach($walletdrcr as $value){ ?>
                                    <tr class="copon_center">
                                        <td><?php if($value['w_orderid']!=0) echo "ORD".$value['w_orderid'];else echo '-'; ?></td>
                                        <td><?php if($value['w_type']=='debit'){ echo number_format($value['w_amount'],1,".",""); } else { echo "-"; } ?></td>
                                        <td><?php if($value['w_type']=='credit'){ echo number_format($value['w_amount'],1,".",""); } else { echo "-"; } ?></td>
                                        <?php $fixwalletamt = number_format($fixwalletamt,1,".","")+number_format($value['w_amount'],1,".",""); ?>
                                        <td><?php echo number_format($fixwalletamt,1,".",""); ?></td>
                                        <td><?php echo $value['w_date']; ?></td>
                                        <td><?php if($value['w_reason']!='') echo $value['w_reason'];else echo '-'; ?></td>
                                    </tr>
                                    <?php } } else { echo ' <tr class="copon_center"><td colspan="7">No data available</td></tr>';}?>
                                    
                                </tbody>
                            </table>
                        </div>

                         <div class="login_pop" id="rate">
                    <div class="recent_order">General Information</div>
                            
                                    
                        </div>
                        
                        <!-- End MY wallet -->
                        <!-- My payment details -->
                        <div class="my_box" id="mypayments">
                            <div class="recent_order">My Payment Details</div>
                            <div class="recent_order" style="font-size: 18px;font-weight: bold;">
                                <?php foreach ($totalrempay as $key => $value) {
                                if($value['PaymentStatus']=='unpaid' && $value['remamt']==''){
                                $grtot+=$value['TotalAmt'];
                                } else {
                                if($value['PaymentStatus']!='paid'){
                                $remaww = $value['TotalAmt']-$value['remamt'];
                                $grtot+=$remaww;
                                }
                                }
                                }?>
                                Remaining Balance :- Rs. <?php echo number_format($grtot,1,".",""); ?>
                            </div>
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>S.NO.</th>
                                        <th>Order Number</th>
                                        <th>Order Date</th>
                                        <th>Order Status</th>
                                        <th>Order Amount</th>
                                        <th>Payment Type</th>
                                        <th>Paid Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($orddetail->scalar!='Something Went Wrong'){
                                    $i=0; foreach ($orddetail as $key => $value) { $i++; #p($orddetail);
                                    /*if($orddetail[$key-1]->OrderNumber!=$value->OrderNumber && $orddetail[$key-1]->OrderNumber!=''){
                                    $rema="";
                                    }*/ ?>
                                    <tr class="copon_center">
                                        <td><?php echo $i ;?></td>
                                        <td><?php echo $value['OrderNumber'];?></td>
                                        <td><?php echo $value['OrderDate'];?></td>
                                        <td><?php echo $value['PaymentStatus'];?></td>
                                        <td><?php echo number_format($value['TotalAmt'],1,".","");?></td>
                                        <td><?php echo $value['PaymentMode'];?></td>
                                        <td><?php if($value['PaymentStatus']=='paid') echo number_format($value['TotalAmt'],1,".",""); else echo '-';?></td>
                                    </tr>
                                    <?php } } else { echo ' <tr class="copon_center"><td colspan="7">No data available</td></tr>';}?>
                                    
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- End My Payments -->
                        
                        
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var ttl="<?php echo $this->session->userdata('sesregister'); ?>";
if(ttl!="")
{
$(".my_box").removeClass("active_tab");
$("#personalinformation").addClass("active_tab");
$("#rate").addClass("active_tab");
}
var title_ses="<?php echo $this->session->userdata('title'); ?>";
if(title_ses!="")
{
$(".my_box").removeClass("active_tab");
$("#"+title_ses).addClass("active_tab");
}
$(document).ready(function(){
/*var wallrefresh='<?php echo $_GET['mywallet'] ?>';
if(wallrefresh=='mywalletdetails'){
//alert('hello');
var xw = 'mywallet';
$(".my_box").removeClass("active_tab");
$("#"+xw).addClass("active_tab");
} else { */
$(".my_tab >li > a").click(function(){
var x = $(this).attr("title");
$(".my_box").removeClass("active_tab");
$("#"+x).addClass("active_tab");
});
//}
});
function checkpass(){
var old_password = $("#old_password").val();
if(old_password!=""){
$.ajax({
url:base_url+'webapp/memberlogin/oldPassword',
type:'POST',
dataType:'json',
data:{'old_password' : old_password},
async:false,
success: function(data){
//alert(data); return false;
if(data.totalmatch == 0){
$("#oldpassword").html("Enter correct old password.");
$("#old_password").css('border', '1px solid red');
$("#respassword").val('1');
//alert("test");
return false;
}else{
$("#oldpassword").empty();
$("#respassword").val("");
$("#old_password").css('border', '1px solid green');
}
}
});
}else{
$("#old_password").css('border', '1px solid red');
//return false;
}
}
function passChange(){
var old_password = $("#old_password").val();
var new_password = $("#new_password").val();
var confirm_password = $("#confirm_password").val();
var respassword = $("#respassword").val();
if(old_password=="" || new_password=="" || confirm_password==""){
$("#old_password").css("border","1px solid red");
$("#new_password").css("border","1px solid red");
$("#confirm_password").css("border","1px solid red");
return false;
}
if(new_password!=confirm_password){
$("#new_password").css("border","1px solid red");
$("#confirmpassword").html("password did not match from new password");
return false;
}
$("#new_password").css("border","1px solid green");
$("#confirm_password").css("border","1px solid green");
$.ajax({
url:base_url+'webapp/memberlogin/changePassword',
type:'POST',
dataType:'JSON',
data:{'old_password':old_password,'new_password':new_password,'confirm_password':confirm_password},
success: function(data){
//alert(data); return false;
if(data.success == 'success'){
$(".success").html("Pasword update successfully Updated");
$("#old_password").val('');
$("#new_password").val('');
$("#confirm_password").val('');
}else{
$(".unsuccess").html("Password Not updated successfully");
}
}
});
}
</script>
<script type="text/javascript">
function state_country(){
var  stid=$('#memstate').val();
$('#memprofile').prop('disabled', false);
$.ajax({
url: '<?php echo base_url()?>webapp/memberlogin/citystateprofile',
type: 'POST',
data: {'stid': stid},
success: function(data){
//console.log(data);
//alert(data);return false;
var  option_brand = '<option value="">Select City</option>';
$('#memcity').empty();
$("#memcity").append(option_brand+data);
}
});
}
state_country();
function chstatecity(){
var  stid=$('#memstate').val();
var  ctid=$('#memcity').val();
if(stid==''){
alert('Please select any state!');
$('#memprofile').prop('disabled', true);
//return false;
} else {
$('#memprofile').prop('disabled', false);
if (ctid==''){
alert('Please select any city!');
$('#memprofile').prop('disabled', true);
//return false;
} else {
$('#memprofile').prop('disabled', false);
}
}
}
/*function change_stats(id,flag)
{
current_qty=$('#qunty_'+id).val();
old_qty=$('#qunty_'+id).attr('max');
if(parseInt(current_qty) > parseInt(old_qty))
{
alert('Invalid Quantity');return false;
}
$('#row_id').val(id);
$('#row_stats').val(flag);
$('#add_comnt_div').removeClass('hide');
}*/
function change_stats(oid)
{
var changestat=$('.changestat'+oid).val();
var mode=$('#mode'+oid).val();
var output = jQuery.map($(':checkbox[name=pro_stats_chang'+oid+'\\[\\]]:checked'), function (n, i) {
return n.value;
}).join(',');
if(changestat==''){
$('.changestat'+oid).css('border', '1px solid red');
return false;
} else {
if(output==''){
$('#checkpro'+oid).html('Please select any Product.');
return false;
} else {
if(changestat=='cancel'){
$('.rtnreplacebtn').css('display','none');
$(".rtnreplacebtnrd").prop("checked", false);
if(mode=='ONLINE' && changestat=='cancel'){
$(".rtnrefundbtnrd").prop("checked", true);
}
if(mode!='ONLINE' && changestat=='cancel'){
$('.rtnrefundbtn').css('display','none');
}
}
$('#checkpro'+oid).html('');
$('.changestat'+oid).css('border', '1px solid #ccc');
$('#row_id').val(output);
$('#row_stats').val(changestat);
$('#add_comnt_div').removeClass('hide');
}
}
}
/*arr = strVale.split(',');
for(i=0; i < arr.length; i++)
console.log(arr[i] + " * 2 = " + (arr[i])*2);*/
function update_order_pro()
{
if($('#comnt').val() == "")
{
//$('#comnt').css('border-color','red', 'important');
$('#comnt').addClass('error-class');
}else{
$('.comen_su').prop('onclick',null).off('click');
$('.comen_ca').prop('onclick',null).off('click');
var rtncondition=$('input[name=rtncondition]:checked').val();
if($("input:radio[name='rtncondition']").is(":checked")){
var fullcomment=rtncondition+$('#comnt').val();
} else {
var fullcomment=$('#comnt').val();
}
var allid=$('#row_id').val();
var id='';
var arr = allid.split(',');
var j=0;
var countarr=arr.length;
for(i=0; i < arr.length; i++){
j++;
id= arr[i];
current_qty=$('#qunty_'+id).val();
old_qty=$('#qunty_'+id).attr('max');
old_stat=$('#ship_stas_'+id).text();
$.ajax({
type:'post',
url:'<?php echo base_url()?>webapp/memberlogin/change_order_stats',
data:{'statsid':id,'flag':$('#row_stats').val(),'comment':fullcomment,'cur_qty':current_qty,'old_qty':old_qty,'old_stat':old_stat},
async: false,
success:function(rep)
{
//alert(rep); //return false;
//console.log(rep);
//document.location.href=window.location.href;
}
});
if (j==countarr) {
document.location.href=window.location.href;
}
//alert(id+" @@ "+current_qty+" @@ "+old_qty+" @@ "+old_stat);
}
//return false;
}
}

function reorder(ordid){
$('#reordbutton'+ordid).css('display','none');
$('#load'+ordid).removeClass('hide');
$.ajax({
url:'<?php echo base_url();?>webapp/memberlogin/repeatorder/',
type:'post',
data:{'ordid':ordid},
success:function(rep)
{
if(rep=='success'){
document.location.href='<?php echo base_url();?>cart';
} else {
document.location.href=window.location.href;
}
}
});
}
/*$(document).ready(function(){
var value=$("#myorder").height();
//alert(value);
window.scrollBy(0, value);
});*/
function multichstat(oid){
var changestat=$('.changestat'+oid).val();
$('.chstval'+oid).each(function(){
if(changestat=='cancel'){
$('.stats_pending'+oid).css('display','block');
$('.stats_delivered'+oid).css('display','none');
$('.stats_delivered'+oid).attr('checked', false);
}
if(changestat=='return processing'){
$('.stats_delivered'+oid).css('display','block');
$('.stats_pending'+oid).css('display','none');
$('.stats_pending'+oid).attr('checked', false);
}
});
}
</script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
<script>
$( function() {
$( "#memdob" ).datepicker({
changeMonth: true,
changeYear: true,
autoclose: true,
yearRange : 'c-50:c+1',
dateFormat: "dd-mm-yy"
});
$( "#memdoa" ).datepicker({
changeMonth: true,
changeYear: true,
autoclose: true,
yearRange : 'c-10:c+10',
dateFormat: "dd MM yy"
});
});
</script>
<?php
$this->session->unset_userdata('sesregister');
$this->session->unset_userdata('title');
?>