<div class="thankyou_main">
<?php //$thankdata=(array)$viewthanku;
     $thankdata =  (array) $viewthanku;
     //p($thankdata);exit;?>
     <div class="container">
        <div class="row">
            <div class="thankyou">
                <h1>Thank you  for placing your order at Foxsky.com</h1>
                <p>For any query please contact our Customer Care number +91 9732456709 or email us at sales@Foxsky.com for your purchase.</p>
                
                <div class="order_part">
                  <h2>Your Order No. : <?php echo $thankdata[0]['OrderNumber'];?></h2>
                     <h2>Your Order Date. : <?php echo $thankdata[0]['CreatedOn'];?></h2>
                    <h3>Your Payment Mode : <?php echo $thankdata[0]['PaymentMode'];?></h3>
                </div>
                
                <div class="address_thnk">
                  <h2>Shipping address</h2>
                    <p><?php echo $thankdata[0]['ShippingName'];?></p>
                    <p>Ph. no.  <?php echo $thankdata[0]['ContactNo'];?></p>
                    <p><?php echo $thankdata[0]['ShippingAddress'].','.$thankdata[0]['City'].','.$thankdata[0]['statename'].','.$thankdata[0]['Pin'].','.$thankdata[0]['name'];?></p>
                    
                </div>
                
                <div class="thank_list">
                
                <div class="thank_list_head">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">Images</div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">Product Name</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">Price</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">Quantity</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">Total</div>
                    </div>
                <?php //p($viewthanku);//exit; ?>
  <?php $i=0; foreach($viewthanku as $key => $value){ $i++;  ?>
                <div class="thankyou_body">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="item-img"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value['ProImage'];?>" alt=""></div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <h3><?php echo $value['ProName'];?></b><?php if(!empty($value['SkuNumber'])){ ?><br>Sku : <?php echo $value['SkuNumber']; }?></h3>
                          
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">
                          <div class="thank_item_txt"><span><img src="assets/images/rupees-gay.png" alt=""></span><?php echo $value['ProPrice'];?></div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                          <div class="thank_item_txt"><?php echo $value['OrdQty'];?></div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 txt-center">
                          <div class="thank_item_txt"><span><img src="assets/images/rupees-gay.png" alt=""></span><?php echo ($value['ProPrice']*$value['OrdQty']);?></div>
                        </div>
                        
                    </div>
                    
  <?php 
if($value['ordproduct_type']=='combo'){
$totordqty+=$value['ord_comboqty'];
} else {

  $totordqty+=$value['OrdQty']; 
  } 
  ?>


   <?php } ?>
           
                
                <div class="total_thank">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="total_summery">
                    <div class="row">
                          <div class="col-lg-6"><div class="text_right">Sub Total</div></div>
                            <div class="col-lg-6"><div class="text_right"><span><img src="assets/images/rupees-gay.png" alt=""></span><?php echo $value['subtotal'];?></div></div>
                        </div>

                         <?php if(!empty($value['CouponAmt'])){ ?>
                                             

                                               <div class="col-lg-6"><div class="text_right">Coupon Discount (<?php echo $value['CouponCode']; ?>)</div></div>
                            <div class="col-lg-6"><div class="text_right"><span><img src="assets/images/rupees-gay.png" alt=""></span> <?php echo number_format($value['CouponAmt'],1,".","");?></div></div>
                        </div>


                                              <?php } ?>


                        
                        <div class="row paid">
                          <div class="col-lg-6"><div class="text_right"><strong>Paid Amount</strong></div></div>
                            <div class="col-lg-6"><div class="text_right"><strong><span><img src="assets/images/rupees-gay.png" alt=""></span><?php echo $value['TotalAmt'];?></strong></div></div>
                        </div>
                        </div>
                  </div>
                </div>
                
                <div class="return_main_thank">
                  <p><strong>Return / Exchange policy for Consumers</strong></p>
                    <p>This policy is applicable according to each manufacturer policy. If there is any manufacturing default the product is either replaced wholly or the defective part is exchanged. Return of goods can be done only with the next immediate order and any return / exchange for more then 30 days shall not be accepted.</p>
                </div>
                
              </div>
            </div>
        </div>
     </div>
 </div>
       
