<div class="wrapper sub_nav_bg">
	<div class="parivacy-policy">
		<div class="container">
    		<div class="row">
        		<div class="feedback_main">
        		<?php
                          if($this->session->flashdata('message')){ 
                            echo $this->session->flashdata('message'); 
                         }
                        ?>
            		<div class="form_feedback">
                    	<h1>Feedback Form</h1>
                        <hr>
                        <form method="post">
                    	<div class="form-group">
                        	<label class="form-txt"><span>Name</span></label>
                            <input type="text" class="login-input" id="email" name="name" required placeholder="Name">
                        </div>
                        <div class="form-group">
                        	<label class="form-txt"><span>Email Id</span></label>
                            <input type="text" class="login-input" id="email" name="email" required placeholder="Email Id">
                        </div>
                        <div class="form-group">
                        	<label class="form-txt"><span>Message</span></label>
                            <textarea class="login-input" rows="5" id="comment" required name="comment"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btnadpt btn_orange btn_orangeH" value="submit" name="submit">Submit</button>
                        </div>
                        </form>
                       
                        
                    </div>
            	</div>
        	</div>
    	</div>
	</div>
</div>

<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>