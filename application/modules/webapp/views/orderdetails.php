<div class="wrapper sub_nav_bg breadcrumbs">
	<div class="container-fluid">
    	<div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
            	<a href="#">Home</a><img src="<?php echo base_url();?>assets/images/backarrow.png" alt="arrow" class="right-arrow-small">
                My Orderdetails
            </div>
        </div>
	</div>  	
</div>

<div class="wrapper my_account">
	<div class="container-fluid">
    	<div class="row">
        	<div class="my_acount_main">
            	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no_padding">
                	<div class="my_nav">
                    
                    	<h3 class="my_title">ORDERS</h3>
                        	<ul class="order_menu">
                            	<li><a href="#" class="current_active">My Orders</a></li>
<!--                                 <li><a href="<?php echo base_url();?>myorder">Returns</a></li>
 -->                            </ul>
                       
                        <h3 class="my_title">Foxsky</h3>
                        	<ul class="order_menu">
                            	<li><a href="<?php echo base_url();?>myaccount">My Account</a></li>
                                <li><a href="<?php echo base_url();?>myreview">Reviews</a></li>
                                <li><a href="<?php echo base_url();?>myaddress">Address Book</a></li>
                                
                            </ul>
                            
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 no_padding">
                	<div class="my_right">
                    <h1>My Orders</h1>
                    <div class="left-order-num">Order Number <?php echo $details->OrderNumber;?></div>
                    	<div class="or_detail">
                        <!-- <h2>Waiting for payment</h2>               -->
                    		<div class="row">
                         		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><div class="order_picture"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $details->ProImage;?>" alt=""></div></div>
                                	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                		<p class="p7 p-m15"><?php echo $details->ProName;?></p>
                                		<!-- <p class="p7 light_gray">Sold by: TVS ELECTRONICS LIMITED</p> -->
                                	</div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    	<p class="p7 p-m15"><img src="assets/images/rupees-gay.png" alt=""><?php echo $details->ProPrice;?>X<?php echo $details->OrdQty;?></p>
                                    </div>
                                 </div>
                            </div>
                            
                            <div class="delivery_address">
                            	<h3>Delivery address</h3>
                                <div class="info_table">
                                    <div class="row">
                                		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><p>Name</p></div>
                                        <div class="col-lg-30 col-md-10 col-sm-10 col-xs-12"><p><?php echo $details->ShippingName;?></p></div>
                            		</div>
                                <div class="row">
                                		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><p>Address</p></div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12"><p><?php echo $details->ShippingAddress;?></p></div>
                            		</div>
                                <div class="row">
                                		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><p>Phone</p></div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12"><p><?php echo $details->ContactNo;?></p></div>
                            		</div>
                                </div>
                            </div>
                            
                            
                            <div class="order_detail_total">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"></div>
                            	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="cart-action-box">
                                    <p class="subtotal-price">Net amount : <span><img src="assets/images/rupees-gay.png" alt=""><?php echo $details->ProPrice;?></span></p>
                                    <p class="subtotal-price">Shipping : <span><img src="assets/images/rupees-gay.png" alt=""><?php echo $details->shippingCharge;?></span></p>
                                    <div class="total_price_item">
                                    <span class="total_small_txt">Total amount</span>
                                    <span><img src="assets/images/rupees-orange.png" alt=""></span><?php echo $details->FinalPrice;?>
                                    </div>
                                    <!-- <a href="#" class="checkout_btn">Pay Now</a> -->
                                </div>
                        </div>
                            </div>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>