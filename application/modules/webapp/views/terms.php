<style>
.suport_list {
	margin:0;
	padding:0;
	}
.suport_list li {
	list-style-type:none;
	margin-left:15px;
}
.xm-section p {
    text-align: justify;
}
</style>
<div class="wrapper sub_nav_bg">
	<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="sub_nav">
			<div class="pull-right">
            	<ul class="nav_about">
                      <li><a href="<?php echo base_url();?>aboutus">About us</a></li>
                    	<li><a href="<?php echo base_url();?>support">Support</a></li>
                        <li><a href="<?php echo base_url();?>useraggree">User Agreement</a></li>
                        <li><a href="<?php echo base_url();?>privacy">Privacy Policy</a></li>
                        <li><a href="<?php echo base_url();?>career">Career</a></li>

                        <li><a href="#" class="active-orage">Terms of Use</a></li>
                    </ul>
            	</div>
              </div>
            </div>
        </div>
    </div>
</div>		
        
        
<div class="wrapper bg_orage">
	<div class="container">
        <div class="about_heading">
        	<h3 class="about_h4">TERMS OF USE</h3>
        </div>
    </div>
</div>    


<div class="wrapper parivacy-policy">
	<div class="container">
        <div class="row">
            <div class="xm-section">
                
                    <p><strong>TERMS OF USE</strong></p>
                        <p>This document is an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures.

</p>
            

<p>This document is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries guidelines) Rules, 2011 that require publishing the rules and regulations, privacy policy and Terms of Use for access or usage of <a href="https://www.foxsky.com">www.foxsky.com/in</a> website.</p>

<p>The domain name <a href="https://www.foxsky.com">www.foxsky.com</a> (hereinafter referred to as "Website") is owned by Foxsky.</p>

<p>Your use of the Website and services and tools are governed by the following terms and conditions ("Terms of Use") as applicable to the Website including the applicable policies which are incorporated herein by way of reference. If You transact on the Website, You shall be subject to the policies that are applicable to the Website for such transaction. By mere use of the Website,</p>

<p>For the purpose of these Terms of Use, where For the purpose of these Terms of Use, wherever the context so requires "You" or "User" shall mean any natural or legal person who has agreed to become a buyer on the Website by providing registration data while registering on the Website as registered user using the computer systems. Foxsky also allows the User to surf the Website or making purchases without registering on the Website. The term "We", "Us", "Our"</p>

<p>When You use any of the services provided by Us through the Website, including but not l to, (e.g. product reviews, seller reviews), You will be subject to the rules, guidelines, policies, terms, and conditions applicable to such service, and they shall be deemed to be incorporated into this Terms of Use and shall be considered as part and parcel of this Terms of Use. We reserve the right, at Our sole discretion, to change, modify, add or remove portions of these Terms of Use, at any time without any prior written notice to You. It is Your responsibility to review these Terms of Use periodically for updates/changes. Your continued use of the Website following the posting of changes will mean that You accept and agree to the revisions. As long as You comply with these Terms of Use, We grant You a personal, non-exclusive, non-transferable, foxsky privilege to enter and use the Website.</p>

<p><strong>MEMBERSHIP ELIGIBILITY</strong></p>

<p>Use of the Website is available only to persons who can form legally binding contracts under Indian Contract Act, 1872. Persons who are "incompetent to contract" within the meaning of the Indian Contract Act, 1872 including foxsky nors, un-discharged insolvents etc. are not eligible to use the Website. If you are a foxsky nor i.e. under the age of 18 years, you shall not register as a User of the Website and shall not transact on or use the Website. As a foxsky nor if you wish to use or transact on the Website, such use or transaction may be made by your legal guardian or parents on the Website. Foxsky reserves the right to ter foxsky nate your membership and/or refuse to provide you with access to the Website if it is brought to Foxsky's notice or if it is discovered that you are under the age of 18 years.</p>

<p>Membership on the Website is free for buyers. Foxsky does not charge any fee for browsing and buying on the Website (“Fee Policy”). Foxsky reserves the right to change its Fee Policy from time to time. Foxsky reserves the right to introduce fees for the new services offered or amend/introduce fees for existing services, as the case may be. Changes to the Fee Policy shall be posted on the Website and such changes shall automatically become effective immediately after they are posted on the Website. Unless otherwise stated, all fees shall be quoted in Indian Rupees. You shall be solely responsible for compliance of all applicable laws including those in India for making payments to Foxsky Technology India Private foxsky.</p>

<p><strong>YOUR ACCOUNT AND REGISTRATION OBLIGATIONS</strong></p>

<p>If You use the Website, You shall be responsible for maintaining the confidentiality of your display name and password and You shall be responsible for all activities that occur under your display name and password. You agree that if You provide any information that is untrue, inaccurate, not current or incomplete or We have reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, or not in accordance with the this Terms of Use, We shall have the right to indefinitely suspend or ter foxsky nate or block access of your membership on the Website and refuse to provide You with access to the Website.</p>

<p><strong>COMMUNICATIONS</strong></p>

<p>When You use the Website or send emails or other data, information or communication to us, You agree and understand that You are communicating with Us through electronic records and You consent to receive communications via electronic records from Us periodically and as and when required. We may communicate with you by email or by such other mode of communication, electronic or otherwise.</p>

<p><strong>PLATFORM FOR TRANSACTION AND COMMUNICATION</strong></p>

<p>The Website is a platform that Users utilize to meet and interact with others for their transactions. Foxsky is not and cannot be a party to or control in any manner any transaction between the Website's Users.</p>

<p><strong>PAYMENT</strong></p>

<p>Please check <a href="https://www.foxskyindia.com/in/service/payment/">www.foxskyindia.com/in/service/payment/</a>for details.</p>

<p><strong>USE OF THE WEBSITE</strong></p>

<p>You agree, undertake and confirm that Your use of Website shall be strictly governed by the following:</p>

<p>You shall not:</p>
<ul class="suport_list">
<li><p>1. Defame, abuse, harass, stalk, hurt religious or ethnic sentiments, threaten or otherwise violate the legal rights of others;</p></li>


<li><p>2.    Publish, post, upload, distribute or disse foxsky nate any inappropriate, profane, defamatory, infringing, obscene, indecent or unlawful topic, name, material or information;</li></p>

<li><p>3.    Upload files that contain software or other material protected by intellectual property laws unless you own or control the rights thereto or have received all necessary consents; you own or control the rights thereto or have received all necessary consents;</li></p>

<li><p>4.    Upload or distribute files that contain viruses, corrupted files, malaware, trojans, bots, or any other si foxsky lar software or programs that may damage the operation of the Website or another's computer;</li></p>

<li><p>5.    Conduct or forward surveys, contests, pyra foxsky schemes or chain letters;</li></p>

<li><p>6.    Download any file posted by another user of the Website that you know, or reasonably should know, cannot be legally distributed in such manner;</li></p>

<li><p>7.    Falsify or delete any author attributions, legal or other proper notices or proprietary designations or labels of the origin or source of software or other material contained in a file that is uploaded;</li></p>

<li><p>8.    Impersonate another person;</li></p>

<li><p>9.    Violate any code of conduct or other guidelines, which may be applicable for or to any particular portion of the Website;</li></p>

<li><p>10.  Violate any applicable laws or regulations for the time being in force in or outside India;</li></p>

<li><p>11.  Violate, abuse, unethically manipulate or exploit, any of the terms and conditions of this Agreement or any other terms and conditions for the use of the Website contained elsewhere;</li></p>

<li><p>12.  Attempt to gain unauthorized access to any portion or feature of the Website, or any other systems or networks connected to the Website or to any server, computer, network, or to any of the services offered on or through the Website, by hacking, password "foxsky" or any other illegitimate means;</li></p>

<li><p>13.  Probe, scan or test the vulnerability of the Website or any network connected to the Website nor breach the security or authentication measures on the Website or any network connected to the Website;</li></p>

<li><p>14.  Reverse look-up, trace or seek to trace any information on any other User of or visitor to Website, or any other customer, including any account on the Website not owned by You, to its source, or exploit the Website or any service or information made available or offered by or through the Website, in any way where the purpose is to reveal any information, including but not foxsky to personal identification or information, other than Your own information, as provided for by the Website;</li></p>

<li><p>15.  Make any negative, denigrating or defamatory statement(s) or comment(s) about Us or the brand name or domain name used by Us or any seller on the Website;</li></p>

<li><p>16.  Take any action that imposes an unreasonable or disproportionately large load on the infrastructure of the Website or Foxsky or its affiliate’s systems or networks, or any systems or networks connected to Foxsky or its affiliates;</li></p>

<li><p>17.  You shall not engage in advertising to, or solicitation of, other Users of the Website to buy or sell any products or services, including, but not foxsky to, products or services related to that being displayed on the Website or related to us.</li></p>

</ul>

<p><strong>REPLACEMENT POLICY</strong></p>

<p>Foxsky’s replacement policy allows You to request replacement devices at no additional cost if the device received by you is damaged / defective or is not as ordered by You.</p>

<p>You may initiate Your replacement request by calling Our customer care services at 1800 532 8777. Once You have discussed the nature of Your replacement request with Our customer care executives, they will try to resolve the issue with the device, and may, under certain circumstances, require that You avail of a replacement device.</p>

<p>We will arrange for a pickup of the old product/device from Your address registered with Our Website at the time of purchase of the product. If You wish to ship your device from any other location, or if Our courier partners do not service Your registered location, You will be required to return the device, at Your own expense, at the following address:</p>

<p>We recommend You use a reliable courier service to return the old product.</p>

<p>While handing over a device for shipping or otherwise shipping it back to us, please ensure that You pack the device securely to prevent any loss or damage during transit.</p>

<p>Your will be eligible for a replacement only if:</p>
<ul class="suport_list">

<li><p>1.    Our exa foxskynation of the device shows a defect or damage of a nature that qualifies the device for replacement;</li></p>

<li><p>2.    A replacement request is made within 10 (ten) days of receipt of the delivery of the order by the User;</li></p>

<li><p>3.    The device has been received in its unused, original condition; and</li></p>

<li><p>4.    The device has been received in the original product packaging along with supporting documentation such as receipt or proof of purchase, price tags, labels, warranty card and any freebies and accessories obtained along with the original product.</li></p>

</ul>

<p>Please note that replacement is subject to availability of a suitable replacement.</p>

<p>Upon the old product satisfying the aforesaid conditions, We will notify You of the time period within which You will receive the replacement product. If You don't get the replacement device within the specified time, please contact Us immediately.</p>

<p>Please note that this policy only covers replacements for eligible products that are damaged in transit, or are not shipped as ordered. It will not cover routine product wear and tear, damage incurred during use or any other forms of damage and will not, in any event, entitle You to a refund, whether partial or otherwise.</p>

<p><strong>INTELLECTUAL PROPERTY</strong></p>

<p>You agree that by browsing and continuing to browse or use the Website you explicitly give Us the per foxsky ssion to use the information You supply Us with, so that we are not violating any rights You foxsky have in Your Information.</p>

<p>You agree to grant Us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable (through multiple tiers) right to exercise the copyright, publicity, database rights or any other rights You have in Your Information, in any media now known or not currently known, with respect to Your Information. We will only use Your information in accordance with the Terms of Use and Privacy Policy applicable to use of the Website.</p>

<p><strong>USER GENERATED CONTENT</strong></p>

<p>You shall be responsible for any notes, messages, emails, billboard postings, photos, drawings, profiles, opinions, ideas, images, videos, audio files or other materials or information posted or trans foxsky to the Website (collectively, "Content"). Such Content will become Our property and You grant Us the worldwide, perpetual and transferable rights in such Content. We shall be entitled to, consistent with Our Privacy Policy as adopted in accordance with applicable law, use the Content or any of its elements for any type of use forever, including but not foxskyted to promotional and advertising purposes and in any media whether now known or hereafter devised, including the creation of derivative works that may include the Content You provide. You agree that any Content You post may be used by us, consistent with Our Privacy Policy and Rules of Conduct on Site as mentioned herein, and You are not entitled to any payment or other compensation for such use.</p>

<p>It is possible that other users (including unauthorized users or "hackers") may post or transfoxskyt offensive or obscene materials on the Website and that You may be involuntarily exposed to such offensive and obscene materials. It also is possible for others to obtain personal information about You due to your use of the Website, and that the recipient may use such information to harass or injure You. We does not approve of such unauthorized uses, but by using the Website You acknowledge and agree that We are not responsible for the use of any personal information that You publicly disclose or share with others on the Website. Please carefully select the type of information that You publicly disclose or share with others on the Website.</p>

<p>Please be advised that any Content posted does not reflect Foxsky’s views. Foxsky is merely an intermediary for the purposes of this Terms of Use. In no event shall Foxsky assume or have any responsibility or liability for any Content posted or for any claims, damages or losses resulting from use of Content and/or appearance of Content on the Website.</p>

<p>Your correspondence or business dealings with, or participation in promotions of, advertisers found on or through the Website, including payment and delivery of related products or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between You and such advertiser. We shall not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of such advertisers on the Website.</p>

<p>You hereby represent and warrant that You have all necessary rights in and to all Content which You provide and all information it contains and that such Content shall not infringe any proprietary or other rights of third parties or contain any libellous, tortious, or otherwise unlawful information.</p>

<p>Except as expressly provided in these Terms of Use, no part of the Website and no Content may be copied, reproduced, republished, uploaded, posted, publicly displayed, encoded, translated, transfoxskytted or distributed in any way (including "foxskyrroring") to any other computer, server, Website or other medium for publication or distribution or for any commercial enterprise, without Foxsky's express prior written consent.</p>

<p>You may use information on the products and services purposely made available on the Website for downloading, provided that You</p>
<ul class="suport_list">
	<li><p>(1) do not remove any proprietary notice language in all copies of such documents,</p></li>
    <li><p>(2) use such information only for your personal, non-commercial informational purpose and do not copy or post such information on any networked computer or broadcast it in any media,</p></li>
    <li><p>(3) make no modifications to any such information, and</p></li>
    <li><p>(4) do not make any additional representations or warranties relating to such documents.</p></li>
</ul>


<p>We reserve the right, but have no obligation, to monitor the materials posted on the Website. Foxsky shall have the right to remove or edit any content that in its sole discretion violates, or is alleged to violate, any applicable law or either the spirit or letter of these Terms of Use. Notwithstanding this right, YOU REMAIN SOLELY RESPONSIBLE FOR THE CONTENT OF THE MATERIALS YOU POST ON THE WEBSITE AND IN YOUR PRIVATE MESSAGES.</p>

<p>Foxsky shall have all the rights to take necessary action and claim damages that may occur due to your involvement/participation in any way on your own or through group/s of people, intentionally or unintentionally in DoS/DDoS (Distributed Denial of Services).</p>

<p><strong>PRIVACY</strong></p>

<p>We view protection of Your privacy as a very important principle. We understand clearly that You and Your Personal Information is one of Our most important assets. We store and process Your Information including any sensitive financial information collected (as defined under the Information Technology Act, 2000), if any, on computers that may be protected by physical as well as reasonable technological security measures and procedures in accordance with Information Technology Act 2000 and Rules there under. Our current Privacy Policy is available at //www.foxskyindia.com/in/about/privacy/. If You object to Your Information being transferred or used in this way please do not use Website.</p>

<p>We and our affiliates will share / sell / transfer / license / covey some or all of your personal information with another business entity should we (or our assets) plan to merge with or are acquired by that business entity, or re-organization, amalgamation, restructuring of business or for any other reason whatsoever. Should such a transaction or situation occur, the other business entity or the new combined entity will be required to follow the privacy policy with respect to your personal information. Once you provide your information to us, you provide such information to us and our affiliate and we and our affiliate may use such information to provide you various services with respect to your transaction whether such transaction are conducted on www.foxsky.com/in.com or with third party merchant's or third party merchant's website.</p>

<p><strong>DISCLAIMER OF WARRANTIES AND LIABILITY</strong></p>

<p>This Website, all the materials and products (including but not foxsky  to software) and services, included on or otherwise made available to You through this site are provided on "as is" and "as available" basis without any representation or warranties, express or implied except otherwise specified in writing. Without prejudice to the forgoing paragraph, Foxsky does not warrant that:</p>

<ul class="suport_list">
	<li><p>1.    This Website will be constantly available, or available at all;</p></li>
    <li><p>2.    The information on this Website is complete, true, accurate or non-foxsky sleading.</p></li>
</ul>

<p>Foxsky will not be liable to You in any way or in relation to the Contents of, or use of, or otherwise in connection with, the Website. Foxsky does not warrant that this site; information, Content, materials, product (including software) or services included on or otherwise made available to You through the Website; their servers; or electronic communication sent from Us are free of viruses or other harmful components.</p>

<p>Nothing on Website constitutes, or is meant to constitute, advice of any kind. All the Products sold on Website are governed by different state laws and if Seller is unable to deliver such Products due to implications of different state laws, Seller will return or will give credit for the amount (if any) received in advance by Seller from the sale of such Product that could not be delivered to You.</p>

<p>You will be required to enter a valid phone number while placing an order on the Website. By registering Your phone number with us, You consent to be contacted by Us via phone calls and/or SMS notifications, in case of any order or shipment or delivery related updates. We will not use your personal information to initiate any promotional phone calls or SMS.</p>

<p><strong>COMPLIANCE WITH LAWS:</strong></p>

<p>Buyer and seller shall comply with all the applicable laws (including without foxsky Foreign Exchange Management Act, 1999 and the rules made and notifications issued there under and the Exchange Control Manual as may be issued by Reserve Bank of India from time to time, Customs Act, Information and Technology Act, 2000 as amended by the Information Technology (Amendment) Act 2008, Prevention of Money Laundering Act, 2002 and the rules made there under, Foreign Contribution Regulation Act, 1976 and the rules made there under, Income Tax Act, 1961 and the rules made there under, Export Import Policy of government of India) applicable to them respectively for using Payment Facility and the Website.</p>

<p><strong>INDEMNITY</strong></b>

<p>You shall indemnify and hold harmless Foxsky, its owner, licensee, affiliates, subsidiaries, group companies (as applicable) and their respective officers, directors, agents, and employees, from any claim or demand, or actions including reasonable attorneys' fees, made by any third party or penalty imposed due to or arising out of Your breach of this Terms of Use, privacy Policy and other Policies, or Your violation of any law, rules or regulations or the rights (including infringement of intellectual property rights) of a third party.</p>

<p><strong>APPLICABLE LAW</strong></p>

<p>Terms of Use shall be governed by and interpreted and construed in accordance with the laws of India. The place of jurisdiction shall be exclusively in Bangalore.</p>

<p><strong>RETENTION OF PERSONAL INFORMATION</strong></p>

<p>Unless otherwise specified, the material on the Website is presented solely for the purpose of sale in India. Foxsky make no representation that materials in the Website are appropriate or available for use in other locations/Countries other than India. Those who choose to access this site from other locations/Countries other than India do so on their own initiative and Foxsky is not responsible for supply of products/refund for the products ordered from other locations/Countries other than India, compliance with local laws, if and to the extent local laws are applicable.</p>

<p><strong>TRADEMARK, COPYRIGHT AND RESTRICTION</strong></p>

<p>This site is controlled and operated by Foxsky and products are sold by respective Sellers. All material on this site, including images, illustrations, audio clips, and video clips, are protected by copyrights, trademarks, and other intellectual property rights. Material on Website is solely for Your personal, non-commercial use. You must not copy, reproduce, republish, upload, post, trans foxsky  or distribute such material in any way, including by email or other electronic means and whether directly or indirectly and You must not assist any other person to do so. Without the prior written consent of the owner, modification of the materials, use of the materials on any other website or networked computer environment or use of the materials for any purpose other than personal, non-commercial use is a violation of the copyrights, trademarks and other proprietary rights, and is prohibited. Any use for which You receive any remuneration, whether in money or otherwise, is a commercial use for the purposes of this clause.</p>

<p><strong>TRADEMARK COMPLAINT</strong></p>

<p>Foxsky respects the intellectual property of others. In case You feel that Your Trademark has been infringed, You can write to Foxsky at legalqa@foxskyindia.com.</p>

<p><strong>PRODUCT DESCRIPTION</strong></p>

<p>Foxsky does not warrant that Product description or other content of this Website is accurate, complete, reliable, current, or error-free and assumes no liability in this regard.</p>

<p><strong>LIFOXSKYTATION OF LIABILITY</strong></p>

<p>IN NO EVENT SHALL FOXSKY BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND IN CONNECTION WITH THESE TERMS OF USE, EVEN IF USER HAS BEEN INFORMED IN ADVANCE OF THE POSSIBILITY OF SUCH DAMAGES.</p>

<p><strong>CONTACT US</strong></p>

<p>Please contact us for any questions or comments (including all inquiries unrelated to copyright infringement) regarding this Website.</p>

<p><strong>GRIEVANCE OFFICER</strong></p>

<p>If you have any concerns regarding the collection, retention, transfer and storage of your data please contact:</p>

<p>Grievance Officer</p>
<p>49/26 Site 4 Industrail Area Sahibabad Ghaziabad 201010, (U.P) INDIA
<br>Email: support@foxskyindia.com
</p>
            </div>
        </div>
    </div>
</div>


<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>
<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			$(".navigation > ul > li > a").hover(function(){
				//alert("ddd");
				if($(this).next(".product_item_view").length > 0){
				$(".logo_row").addClass("show_nav");
				$(this).next(".product_item_view").css({"display":"block"});
				}
				else{
					$(".logo_row").removeClass("show_nav");
					$(".product_item_view").css({"display":"none"});
				}
			});
			$(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
				$(".product_item_view").css({"display":"none"});
			});
		});
		</script>
         <script src="assets/js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    
 	<script src="assets/js/jquery.bxslider.min.js"></script>
	   <script>
        $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 330,
        slideMargin: 0,
        ticker: true,
        speed: 30000
    });
       </script>
 
