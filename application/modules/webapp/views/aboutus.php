

 <div class="wrapper sub_nav_bg">
	<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="sub_nav">
			<div class="pull-right">
            	<ul class="nav_about">
                    	<li><a href="#" class="active-orage">About Us</a></li>
                    	<li><a href="<?php echo base_url();?>support">Support</a></li>
                        <li><a href="<?php echo base_url();?>useraggree">User Agreement</a></li>
                        <li><a href="<?php echo base_url();?>privacy">Privacy Policy</a></li>
                        <li><a href="<?php echo base_url();?>career">Career</a></li>
                        <li><a href="<?php echo base_url();?>terms">Terms of Use</a></li>
                    </ul>
            	</div>
              </div>
            </div>
        </div>
    </div>
</div>		
        
<div class="wrapper bg_orage">
	<div class="container">
        <div class="about_heading">
        	<h3 class="about_h4">ABOUT US</h3>
        </div>
    </div>
</div>        


<div class="wrapper parivacy-policy">
	<div class="container">
        <div class="row">
            <div class="xm-section">
                     <p><strong>About Us</strong></p>
<p>FOXSKY was founded in 2017 by  entrepreneur  Manish Bhardwaj & Sachin Bhardwaj  based on the vision “TECHNOLOGY BEYOND IMAGINATION ”. </p>
<p>We believe that high-quality products built with cutting-edge technology should be made accessible to everyone. </p>
<p>We create remarkable hardware, software, and Internet services for and with the help of our FOXSKY fans. We incorporate 
their feedback into our product range, which currently includes FOXSKY TVs , MUSIC SYSTEM, POWER BANKS, SMART BANDS, BLUETOOTH SPEAKER,  wearables and other accessories.</p> 
<p>With FOXSKY is expanding its footprint across the nation to become a national brand. <br />

FOXSKY IS OWNED BY BHARDWAJ GROUP WHICH IS ALREADY 30 YEAR OLD COMPANY WHICH HAS AN EXISTENCE IN PETROLEUM, CONSTRUCTONS, LOGISTICS & ELECTRONIC RETAIL STORES.
</p>
                    </div>
                </div>
            </div>
        </div>






<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>
<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			$(".navigation > ul > li > a").hover(function(){
				//alert("ddd");
				if($(this).next(".product_item_view").length > 0){
				$(".logo_row").addClass("show_nav");
				$(this).next(".product_item_view").css({"display":"block"});
				}
				else{
					$(".logo_row").removeClass("show_nav");
					$(".product_item_view").css({"display":"none"});
				}
			});
			$(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
				$(".product_item_view").css({"display":"none"});
			});
		});
		</script>
         <script src="assets/js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    
 	<script src="assets/js/jquery.bxslider.min.js"></script>
	   <script>
        $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 330,
        slideMargin: 0,
        ticker: true,
        speed: 30000
    });
       </script>
 
