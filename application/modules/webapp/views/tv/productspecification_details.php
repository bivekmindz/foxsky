
   <div class="wrapper sub_nav_bg">
  <div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="sub_nav">
            
            <!-- <div class="pull-left">
            <h2>Foxsky LED Smart TV 4A 32</h2>
                <ul class="sub_title">
                      <li><a href="#">|</a></li>
                      <li><a href="#">Mi LED Smart TV 4A 43</a></li>
                        <li><a href="#">|</a></li>
                        <li><a href="#">Mi LED Smart TV 4 32</a></li>
                     </ul>
            </div>
 -->
      <div class="pull-right">
            <ul class="sub_tv">

<?php if($producttab->countOverview>0) { ?>
                      <li><a href="<?php echo base_url();?>product/<?php echo $urlname; ?>" >Overview</a></li>
                      <?php } ?>
                      <?php if($producttab->countspecification>0) { ?>
                      <li><a href="<?php echo base_url();?>productspecification/<?php echo $urlname; ?>" class="active-font">Specs</a></li> <?php } ?>
                       <?php if($producttab->countacces>0) { ?>
                        <li><a href="<?php echo base_url();?>productaccessories/<?php echo $urlname; ?>">Accessories</a></li><?php } ?>
                        <li><a href="<?php echo base_url();?>review/<?php echo $urlname; ?>">Review</a></li>
                       <!--  <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>">F-code</a></li> -->
                         <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>" class="btn-small btn-orange">Buy Now</a></li>
                       
                    </ul>
              </div>
              </div>
            </div>
        </div>
    </div>
</div>    


    <div class="wrapper white_bg">
        <div class="container-fluid">
 <?php if($specificationsdata-> section_heading!='0') { ?>
            <div class="product_dbox sp_box_1">
                <div class="row">
               
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="pro_image">
                                   <img  src="<?php echo base_url('assets/sectionimages/').'/'.$specificationsdata-> banner_img;?>"/>
                            
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="pro_text_box">
                            <div class="pro_title"><?php echo $specificationsdata->section_heading;?></div>
                            <div class="pro_list">
                                <ul>
                                    <li><?php echo $specificationsdata->section_sub_heading;?></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <?php } ?>
<!-- <?php  foreach ($specificationsvalue as $key => $value) {  ?>




            <div class="product_dbox sp_box_2">
                <div class="l_section">
                    <div class="type_name"><?php echo $value->leftvalue;?></div>
                </div>
                <div class="R_section">
                    <div class="w_60">
                        <div class="sub_title_text"></div>
                        <div class="text_section">
                            <p><?php echo $value->feature_value;?></p>
                        </div>
                    </div>
                    <div class="w_40">
                      <?php echo $value-> feature_banner_image; ?>   <img  src="<?php echo base_url('assets/sectionimages/').'/'.$value-> feature_banner_image;?>" class="img_margin"/>
                    </div>
                </div>
            </div>



<?php } ?> -->
<?php  foreach ($specificationsvalue as $key => $value) { ?>
<?php if($value->feature_banner_image!='' && $value->feature_value!='0' ) { ?>
       

             <div class="product_dbox sp_box_2">
                <div class="l_section">
                    <div class="type_name"><?php echo $value->feature_sub_heading;?></div>
                </div>
                <div class="R_section">
                    <div class="w_60">
                          <?php if( $value->feature_heading!='0') { ?>  <div class="sub_title_text"><?php  echo $value->feature_heading; ?></div><?php }?>
                        <div class="text_section">
                            <p><?php echo $value->feature_value;?></p>
                        </div>
                    </div>
                    <?php if( $value->feature_banner_image!='0') { ?> 
                    <div class="w_40">
                         <img  src="<?php echo base_url('assets/sectionimages/').'/'.$value->feature_banner_image;?>" width="150px" height="290px" />
                    </div><?php }?>
                </div>
            </div>



<?php } elseif($value->feature_banner_image!='') { ?>


 <div class="product_dbox sp_box_2">
                <div class="l_section">
                    <div class="type_name"><?php echo $value->feature_sub_heading;?></div>
                </div>
                <div class="R_section">
                 <?php if( $value->feature_heading!='0') { ?>  <div class="sub_title_text"><?php  echo $value->feature_heading; ?></div><?php }?>
                    <?php if( $value->feature_banner_image!='0') { ?>   <div class="pro_sec_img">
                       <img  src="<?php echo base_url('assets/sectionimages/').'/'.$value->feature_banner_image;?>" class="img_margin" />
                    </div><?php }?>
                </div>
            </div>
         <?php }  elseif($value->feature_value!='') { ?>


            <div class="product_dbox sp_box_2">
                <div class="l_section">
                    <div class="type_name"><?php echo $value->feature_sub_heading;?></div>
                </div>
                <div class="R_section">
                  <?php if( $value->feature_heading!='0') { ?>  <div class="sub_title_text"><?php  echo $value->feature_heading; ?></div><?php }?>
                    <div class="text_section">
                        <p class="w_34"><?php echo $value->feature_value;?></p>
                    </div>
                </div>
            </div>

<?php } ?>


<?php } ?>


        </div>
    </div>
    <div class="wrapper">
        <div class="policies_white">
            <div class="container-fluid">
                <div class="row">
                    <div class="footer_policies">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                            <div class="service-policy">
                                <a href="#">
                                    <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                                    <div class="policy_txt">
                                        <strong>Hassle-free replacement</strong>
                                        <br>
                                        <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                                    </div>

                                </a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                            <div class="service-policy pull_space">
                                <a href="#">
                                    <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                                    <div class="policy_txt">
                                        <strong>100% secure payments</strong>
                                        <br>
                                        <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                                    </div>

                                </a>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                            <div class="service-policy pull_right">
                                <a href="#">
                                    <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                                    <div class="policy_txt">
                                        <strong>Vast service network</strong>
                                        <br>
                                        <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                                    </div>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- 
    <script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".navigation > ul > li > a").hover(function () {
                //alert("ddd");
                if ($(this).next(".product_item_view").length > 0) {
                    $(".logo_row").addClass("show_nav");
                    $(this).next(".product_item_view").css({ "display": "block" });
                }
                else {
                    $(".logo_row").removeClass("show_nav");
                    $(".product_item_view").css({ "display": "none" });
                }
            });
            $(".navigation").mouseleave(function () {
                $(".logo_row").removeClass("show_nav");
                $(".product_item_view").css({ "display": "none" });
            });
        });
    </script>
    <script src="assets/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>

    <script src="assets/js/jquery.bxslider.min.js"></script>
    <script>
        $('.bxslider').bxSlider({
            minSlides: 1,
            maxSlides: 8,
            slideWidth: 330,
            slideMargin: 0,
            ticker: true,
            speed: 30000
        });
    </script> -->



