    <div class="wrapper sub_nav_bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sub_nav">

                       
                        <div class="pull-right">
                            <ul class="sub_tv">
                           
<?php if($producttab->countOverview>0) { ?>
                      <li><a href="<?php echo base_url();?>product/<?php echo $urlname; ?>" class="active-font">Overview</a></li>
                      <?php } ?>
                      <?php if($producttab->countspecification>0) { ?>
                      <li><a href="<?php echo base_url();?>productspecification/<?php echo $urlname; ?>">Specs</a></li> <?php } ?>
                       <?php if($producttab->countacces>0) { ?>
                        <li><a href="<?php echo base_url();?>productaccessories/<?php echo $urlname; ?>">Accessories</a></li><?php } ?>
                        <li><a href="<?php echo base_url();?>review/<?php echo $urlname; ?>">Review</a></li>
                       <!--  <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>">F-code</a></li> -->
                         <li><a href="<?php echo base_url();?>productdetails/<?php echo $urlname; ?>" class="btn-small btn-orange">Buy Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper white_bg">
<?php foreach ($accessoriesdata as $key => $value) {?>



        <div class="container-fluid">
         <div class="asses_content_box">
             <div class="asses_title"><?php echo $value->proname; ?></div>
             <div class="asses_dec"><?php echo $value->description; ?></div>
            
         </div>
        </div>
        <div class="asses_img"><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value->prd_main_image; ?>" height="375" width="50"> </div>
        
<div style="text-align: center;padding-top:20px ">

<a href="<?php echo base_url().'productaccessoriesdetails/'.str_replace(' ', '_', $value->proname).'-'.$value->proid.'';?>">

Learn more</a>

</div>

<?php } ?>




       
    </div>
    <div class="wrapper">
        <div class="policies_white">
            <div class="container-fluid">
                <div class="row">
                    <div class="footer_policies">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                            <div class="service-policy">
                                <a href="#">
                                    <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                                    <div class="policy_txt">
                                        <strong>Hassle-free replacement</strong>
                                        <br>
                                        <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                                    </div>

                                </a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                            <div class="service-policy pull_space">
                                <a href="#">
                                    <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                                    <div class="policy_txt">
                                        <strong>100% secure payments</strong>
                                        <br>
                                        <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                                    </div>

                                </a>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                            <div class="service-policy pull_right">
                                <a href="#">
                                    <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                                    <div class="policy_txt">
                                        <strong>Vast service network</strong>
                                        <br>
                                        <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                                    </div>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
