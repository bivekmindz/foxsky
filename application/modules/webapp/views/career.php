<style>
.xm-section p {
	text-align:justify;
}
</style>
 <div class="wrapper sub_nav_bg">
	<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<div class="sub_nav">
			<div class="pull-right">
            	<ul class="nav_about">
                    	<li><a href="<?php echo base_url();?>aboutus">About Us</a></li>
                    	<li><a href="<?php echo base_url();?>support">Support</a></li>
                        <li><a href="<?php echo base_url();?>useraggree">User Agreement</a></li>
                        <li><a href="<?php echo base_url();?>privacy">Privacy Policy</a></li>
                        <li><a href="#" class="active-orage">Career</a></li>
                        <li><a href="<?php echo base_url();?>terms">Terms of Use</a></li>
                    </ul>
            	</div>
              </div>
            </div>
        </div>
    </div>
</div>		
        

<div class="wrapper bg_orage">
	<div class="container">
        <div class="about_heading">
        	<h3 class="about_h4">CAREER</h3>
        </div>
    </div>
</div>

<div class="wrapper parivacy-policy">
	<div class="container">
        <div class="row">
            <div class="xm-section">
                
                   
                        <p><strong>Career with foxskyindia</strong></p>
                        <p>
foxsky is focused on being the most user-centric mobile internet company, and we aim 
to constantly exceed expectations through innovations in software, hardware and services. 
Many of our employees were initially fans of foxsky products, before they decided to join us. Our team is 
not only passionate about technology, but also relentlessly pursues perfection to break tradition and 
push boundaries, all just to ensure that our products remain unique and offer an unparalleled user
experience.</p>
 
<p><strong>Contact</strong></p>

<p>Contact@foxskyindia.com
<br />1800 532 8777
</p>
                   
                
             
            </div>
        </div>
    </div>
</div>


<!-- <div class="wrapper about-2">
	<div class="container">
         <div class="about_main text-center">
              <h3 class="h_02">MAKING QUALITY TECHNOLOGY <br>ACCESSIBLE TO EVERYONE.</h3>
              <p class="p_02">Xiaomi was founded in 2010 by serial entrepreneur Lei Jun based on the vision “innovation for everyone”. <br>We believe that high-quality products built with cutting-edge technology should be made accessible to everyone. <br>We&nbsp;create remarkable hardware, software, and Internet services for and with the help of our Mi fans.&nbsp;We incorporate <br>their feedback into our product range, which currently includes Mi and Redmi smartphones, Mi TVs and set-top boxes, <br>Mi routers, and Mi Ecosystem products including smart home products, wearables and other&nbsp;accessories.&nbsp;<br>With presence in over 30 countries and regions, Xiaomi is expanding its footprint across the world to <br>become a global brand.&nbsp;<br></p>
              
              <span class="line-before"></span>
              
              <div class="review_share_like about-social">
                   <a href="#"><i class="fa fa-facebook"></i></a>
                   <a href="#"><i class="fa fa-twitter"></i></a>
                   
                            
              </div>
              
         </div>
       
    </div>
</div>


<div class="wrapper about-3">
	<div class="container">
		<h3 class="h_03">MAKING QUALITY TECHNOLOGY<br>ACCESSIBLE TO EVERYONE.</h3>
    </div>
</div>


<div class="wrapper about-4">
	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team1.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team2.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team3.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team4.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team5.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
        
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    	<div class="row">
            <div class="team_profile">
                	<img src="assets/images/about/team6.jpg" alt="">
                <div class="team_name">
               		<span class="nick_txt">LEI JUN</span>
                	<span class="name_founder">FOUNDER,CHAIRMAN AND CEO</span>
                </div>
                
                <div class="team_contain">
       			<h4>LEI JUN</h4>
                <p>
                  Lei Jun was part of the founding team of Kingsoft in 1992 and became CEO in 1998. A year later, he founded the IT information service and download website Joyo.com. After Kingsoft successfully completed their IPO, Lei Jun stepped down from his position and became Vice Chairman at Kingsoft. In the early 2000’s, he invested in many successful start-up companies like YY, UC and Vancl as an angel investor, and on April 6, 2010, he founded Xiaomi. In July 2011, he returned to Kingsoft as Chairman of the Board. Lei Jun is currently the Chairman and CEO of Xiaomi.
                </p>
     
                </div>
               
            </div>
        </div>
    </div>
    
    
</div>
         

<div class="wrapper about-5">
	<div class="container">
    	<h3 class="h_05">OUR CULTURE</h3>
        <p class="p_05">"Just for fans" – that's our belief. Our hardcore Mi fans lead every step of the way. In fact, many
Xiaomi employees were first Mi fans before joining the team. As a team, we share the same relentless 
pursuit of perfection, constantly refining and enhancing our products to create the best user experience 
possible. We are also fearless in testing new ideas and pushing our own boundaries. Our dedication 
and belief in innovation, together with the support of Mi fans, are the driving forces behind our 
unique Mi products.</p>
    </div>
</div>


<div class="wrapper about-6">
     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="row">
             <div class="team_profile">
                 <img src="assets/images/about/about_1.jpg" alt="">
                 <h4 class="h_06">OFFICE ENVIRONMENT</h4>
                 <p class="p_06">We are incredibly flat, open, and innovative. No never-ending meetings. No lengthy processes. <br> We provide a friendly and collaborative environment where creativity is encouraged to flourish.</p>
             </div>
         </div>
    </div>
    
    
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="row">
             <div class="team_profile">
                 <img src="assets/images/about/about_2.jpg" alt="">
                 <h4 class="h_06">OFFICE ENVIRONMENT</h4>
                 <p class="p_06">We are incredibly flat, open, and innovative. No never-ending meetings. No lengthy processes. <br> We provide a friendly and collaborative environment where creativity is encouraged to flourish.</p>
             </div>
         </div>
    </div>
    
</div>



<div class="wrapper about-7">
	<div class="container">
         <div class="about_main text-center">
              <h3 class="h_02">CAREERS</h3>
              <p class="p_02">Xiaomi was founded in 2010 by serial entrepreneur Lei Jun based on the vision “innovation for everyone”. <br>We believe that high-quality products built with cutting-edge technology should be made accessible to everyone. <br>We&nbsp;create remarkable hardware, software, and Internet services for and with the help of our Mi fans.&nbsp;We incorporate <br>their feedback into our product range, which currently includes Mi and Redmi smartphones, Mi TVs and set-top boxes, <br>Mi routers, and Mi Ecosystem products including smart home products, wearables and other&nbsp;accessories.&nbsp;<br>With presence in over 30 countries and regions, Xiaomi is expanding its footprint across the world to <br>become a global brand.&nbsp;<br></p>
              
              <span class="line-before"></span>
              
              
              
         </div>
       
    </div>
</div>
 -->


<div class="wrapper">
	<div class="policies_white">
		<div class="container-fluid">
			<div class="row">
        		<div class="footer_policies">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy1.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Hassle-free replacement</strong>
                            <br>
                            <span class="color_gray">10-day easy replacement policy on foxsky.com</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_space">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy2.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>100% secure payments</strong>
                            <br>
                            <span class="color_gray">We support Cards, Wallets, EMI and COD</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
                    
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no_padding">
                      <div class="service-policy pull_right">
                          <a href="#">
                           <div class="policy_pic"><img src="assets/images/policy3.jpg" alt=""></div>
                           <div class="policy_txt">
                            <strong>Vast service network</strong>
                            <br>
                            <span class="color_gray">750 Foxsky service-centers across 452 cities</span>
                           </div>
                                
                            </a>
                        </div>
                    </div>
			</div>
        </div>
	</div>
</div>
</div>
<script type="text/javascript" src="assets/js/jquery-1.12.4.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript">
		
		$(document).ready(function(){
			$(".navigation > ul > li > a").hover(function(){
				//alert("ddd");
				if($(this).next(".product_item_view").length > 0){
				$(".logo_row").addClass("show_nav");
				$(this).next(".product_item_view").css({"display":"block"});
				}
				else{
					$(".logo_row").removeClass("show_nav");
					$(".product_item_view").css({"display":"none"});
				}
			});
			$(".navigation").mouseleave(function(){
            $(".logo_row").removeClass("show_nav");
				$(".product_item_view").css({"display":"none"});
			});
		});
		</script>
         <script src="assets/js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    
 	<script src="assets/js/jquery.bxslider.min.js"></script>
	   <script>
        $('.bxslider').bxSlider({
        minSlides: 1,
        maxSlides: 8,
        slideWidth: 330,
        slideMargin: 0,
        ticker: true,
        speed: 30000
    });
       </script>
 
