﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <div style="float:left;width:700px;height:auto;border:dashed 1px #ccc;padding:10px 10px;font-family:arial, helvetica, sans-serif;">
        <div style="float:left;width:100%;text-align:center;">
            <img src="<?php echo base_url();?>images/logo3.png">
        </div>
        <div style="float:left;width:100%;text-align:center;background:#F1F1F1;padding:8px 0px;font-family:Arial, Helvetica, sans-serif;color: #4FAB29;margin:5px 0px;">
            Your order has been placed
        </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;"><b>Hi <?php echo $mailview[0]['ShippingName']." ".$mailview[0]['ShipLastName'];?>, </b>  </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;">Thank you for your order!  </div>
        <div style="float:left;width:100%;margin-top:10px;font-size:13px;">We will send you another email once the items in your order have been shipped. Meanwhile, you can check the status of your order on FoxSkyindia.com </div>
       <div style="float:left;width:100%;margin-top:5px;font-size:13px;"></div>

        <div style="float:left;width:100%;margin-top:10px;font-size:13px;">Please find below, the summary of your order <span style="color:#4FAB29;"><?php echo $mailview[0]['OrderNumber'];?></span></div>
        
    
      
      
      <table style="border-collapse:collapse;margin:0px;padding:0px;float:left;width:100%;font-size:13px;margin:15px 0px;border-left:solid 1px #ccc;border-top:solid 1px #ccc;"><tr><th style="border-bottom:solid 1px #ccc;padding:7px 0px;width:50px;border-right:solid 1px #ccc;">S.No.</th><th style="border-bottom:solid 1px #ccc;padding:7px 0px;border-right:solid 1px #ccc;">image</th><th style="border-bottom:solid 1px #ccc;padding:7px 0px;border-right:solid 1px #ccc;">Pro. Name</th><th style="border-bottom:solid 1px #ccc;padding:7px 0px;border-right:solid 1px #ccc;">Qty.</th><th style="border-bottom:solid 1px #ccc;padding:7px 0px;border-right:solid 1px #ccc;">Product MRP</th><th style="border-bottom:solid 1px #ccc;padding:7px 0px;border-right:solid 1px #ccc;">Unit Price</th><th style="border-bottom:solid 1px #ccc;padding:7px 0px;border-right:solid 1px #ccc;">Sub Total</th></tr>

      <?php $i=0; foreach ($mailview as $key => $values) { $i++; ?>

              <tr><td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo $i;?></td><td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><img src="http://115.124.98.243/~foxskyindia/images/thumimg/<?php echo $values['ProImage'];?>" style="width:30px;"></td><td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo $values['ProName'];?><?php if(!empty($values['SkuNumber'])){ ?><br><b><?php echo "SKU no. :- ".$values['SkuNumber'];?></b><?php } ?></td><td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;"><?php echo $values['OrdQty'];?></td><td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;">Rs.<?php echo $values['ProPrice'];?></td><td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;">Rs.<?php echo $values['FinalPrice'];?></td><td style="text-align:center;padding:4px 3px;border-bottom:solid 1px #ccc;border-right:solid 1px #ccc;">Rs.<?php echo number_format($values['FinalPrice']*$values['OrdQty'],1,".","");;?></td></tr>       
        
        <?php 
if($values['ordproduct_type']=='combo'){
$totordqty+=$values['ord_comboqty'];
} else {

  $totordqty+=$values['OrdQty']; 
  } 
}
  ?>
        </table>
            
 <?php $shipcharge=$mailview[0]['ShippingAmt'];?>
<div style="float:right;width:100%;background:#F9F9F9;padding:20px 0px; font-size:14px; padding-right:5px;font-family:Arial, Helvetica, sans-serif;color: #000;margin:5px 0px;  border-top:#000 solid 1px;">
<table style="border-collapse:collapse;margin:0px;padding:0px;float:right;width:70%;font-size:13px;margin:-13px 0px;border-left:solid 0px #ccc;border-top:solid 0px #ccc;">
    <tr>
        <th style="border-bottom:solid 0px #ccc;padding:0px 24px;border-right:solid 0px #ccc; text-align:left;">Subtotal</th>
        <td style="text-align:left;padding:4px 16px;border-bottom:solid 0px #ccc;border-right:solid 0px #ccc;">Rs. <?php echo number_format($mailview[0]['subtotal'],1,".","");?></td>
    </tr>

    <?php 
$tax=number_format(($mailview[0]['taxvat']+$mailview[0]['taxcst']+$mailview[0]['taxentry']),1,".","");
?>

 <tr>
        <th style="border-bottom:solid 0px #ccc;padding:0px 24px;border-right:solid 0px #ccc; text-align:left;">TAX</th>
        <td style="text-align:left;padding:4px 16px;border-bottom:solid 0px #ccc;border-right:solid 0px #ccc;">Rs. <?php echo $tax;?></td>
    </tr>

    <tr>
        <th style="border-bottom:solid 0px #ccc;padding:0px 24px;border-right:solid 0px #ccc; text-align:left;">Shipping Charges</th>
        <td style="text-align:left;padding:4px 16px;border-bottom:solid 0px #ccc;border-right:solid 0px #ccc;">Rs. <?php echo number_format($shipcharge,1,".","");?></td>
    </tr>

    <?php if(!empty($mailview[0]['CouponAmt'])){ ?>
    <tr>
        <th style="border-bottom:solid 0px #ccc;padding:0px 24px;border-right:solid 0px #ccc; text-align:left;">Coupon Discount (<?php echo $mailview[0]['CouponCode']; ?>) (-)</th>
        <td style="text-align:left;padding:4px 16px;border-bottom:solid 0px #ccc;border-right:solid 0px #ccc;">Rs. <?php echo number_format($mailview[0]['CouponAmt'],1,".","");?></td>
    </tr>
  <?php } ?>


  <?php if(!empty($mailview[0]['usedwalletamt'])){ ?>
    <tr>
        <th style="border-bottom:solid 0px #ccc;padding:0px 24px;border-right:solid 0px #ccc; text-align:left;">Pay by Wallet Amount (-)</th>
        <td style="text-align:left;padding:4px 16px;border-bottom:solid 0px #ccc;border-right:solid 0px #ccc;">Rs. <?php echo number_format($mailview[0]['usedwalletamt'],1,".","");?></td>
    </tr>
  <?php } ?>
        
    <tr>
        <th style="border-bottom:solid 0px #ccc;padding:0px 24px;border-right:solid 0px #ccc; text-align:left;">Grand Total</th>
        <td style="text-align:left;padding:4px 16px;border-bottom:solid 0px #ccc;border-right:solid 0px #ccc;">Rs. <?php echo number_format($mailview[0]['TotalAmt'],1,".","");?></td>
    </tr>


            
            </table>
  </div>


      <div style="float:left;width:100%;font-size:12px;line-height:20px;padding:5px 0px;">Outstanding Amount Payable on Delivery: <b>Rs. <?php echo number_format($mailview[0]['TotalAmt'],1,".","");?></b></div>

      <div style="float:left;width:100%;font-size:12px;line-height:20px;padding:5px 0px;">Total Order Quantity: <b><?php echo $totordqty;?></b></div>

     <div style="float:left;width:100%;font-size:12px;line-height:20px;padding:5px 0px;">Payment Mode : <?php echo $mailview[0]['PaymentMode']; ?> </div>

     <div style="float:left;width:100%;font-size:12px;line-height:20px;padding:5px 0px;"><?php echo 'Order Remark : '.$mailview[0]['userremark']; ?> </div>

     <span style="float:left;width:100%;font-size:12px;line-height:20px;padding:5px 0px;">Shop Name: <b><?php echo $mailview[0]['companyname'];?></b></span>

      <div style="float:left;width:100%;font-size:13px;line-height:20px;padding-top:20px;">
            DELIVERY ADDRESS <br />
           <span style="float:left;width:100%;font-size:13px;line-height:20px;padding-top:2px; font-weight:bold;"><?php echo $mailview[0]['ShippingName'];?> &nbsp; <?php echo $mailview[0]['ContactNo'];?></span>
           
            <span style="float:left;width:100%;font-size:13px;line-height:20px;padding-top:2px;"> <?php echo $mailview[0]['ShippingAddress'];?><br><?php echo $mailview[0]['statename'];?> - <?php echo $mailview[0]['Pin'];?><br><?php echo $mailview[0]['City'];?></span>
        </div>
        <div style="float:left;width:100%;font-size:12px;line-height:20px;padding:5px 0px;">For any queries please call us on <b>+91 9711000000</b> or mail us on <b>sales@FoxSkyindia.com</b></div>
        <div style="float:left;width:100%;font-size:13px;line-height:20px;font-weight:bold;padding-top:20px;">
            Regards,<br />
            FoxSkyindia Team
        </div>

    </div>

</body>
</html>