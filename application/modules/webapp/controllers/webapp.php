<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Webapp extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
  }

  public function index(){

    $parameter = array('act_mode'=>'mainimage','row_id'=>'0','brandname'=>'0','brandimage'=>'0','0');
    $record['mainimage'] = $this->supper_admin->call_procedure('proc_brand',$parameter);
   
    // pend($record['mainimage']);
  
    $parameter = array('act_mode'=>'viewpress','row_id'=>'0','brandname'=>'0','brandimage'=>'0','0');
    $record['footerbanner'] = $this->supper_admin->call_procedure('proc_brand',$parameter);
    //if($this->session->userdata('bizzgainmember')->id){
    $mainslidepath = api_url().'home/mainslider/format/json/';
    $record['mainslide']  = curlget($mainslidepath);
    $rightbanpath = api_url().'home/rightbanner/format/json/';
    $record['rightban']  = curlget($rightbanpath);
    $selectpath1 = api_url().'home/selectoneimg/format/json/';
    $record['selectimgone']  = curlget($selectpath1);

    $selectpath2 = api_url().'home/selecttwoimg/format/json/';
    $record['selectimgtwo']  = curlget($selectpath2);
    $selectpath3 = api_url().'home/selectthreeimg/format/json/';
    $record['selectimgthree']  = curlget($selectpath3);

    $selectpath2 = api_url().'home/starpro/format/json/';
    $record['starProd']  = curlget($selectpath2);
    
    // p($record['starProd']);
    // exit();
    // $selectpath3 = api_url().'home/selectthreeimg/format/json/';
    // $record['selectimgthree']  = curlget($selectpath3);
    $selectpath4 = api_url().'home/selectfourimg/format/json/';
    $record['selectimgfour']  = curlget($selectpath4);
    $sectionpath = api_url().'home/sectionmaster/format/json/';
    $record['sectionmasterview']  = curlget($sectionpath);
   // pend(  $record['sectionmasterview']);
    $sectiondetpath = api_url().'home/sectiondetail/format/json/';
    $record['sectiondetailview']  = curlget($sectiondetpath);
    $iconpath = api_url().'home/iconimgdata/format/json/';
    $record['iconimg']  = curlget($iconpath);
    $get_video = api_url().'home/homevideo/format/json/';
    $record['home_video']  = curlget($get_video);
    //p($record['home_video']);exit();
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>10);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $newarrivals=api_url().'home/newarrival/format/json/';


   // pend($newarrivals);
     $record['newarrive']  = curlget($newarrivals);


    $para = array('act_mode' => 'video','Panam1'=>'', 'Panam2'=>'', 'Panam3'=>'', 'Panam4'=>'', 'Panam5'=>'', 'Panam6'=>'', 'Panam7'=>'', 'Panam8'=>'',);
 
    $record['video']=$this->supper_admin->call_procedure('proc_videos',$para);

    $parameter           = array('act_mode'=>'get_product_by_category','row_id'=>'0','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'0','catdisplay'=>'','catlevel'=>'');
     $response['vieww']   = $this->supper_admin->call_procedure('proc_category',$parameter);

             $review=array(
            'act_mode'=>'indexpagereview',
            'row_id'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>''
           
          );
         $record['reviewdata'] = $this->supper_admin->call_procedure('proc_ten', $review); 

//p($record['reviewdata']);
      // $this->load->view("helper/nav.php");
      // die;
 $parameter = array(
                             'act_mode'  =>'viewleftbanner',
                             'Param1'    =>'',
                             'Param2' =>'',
                             'Param3'=>'',
                             'Param4'     =>'',
                             'Param5'     =>'',
                             'Param6'     =>'',
                             'Param7'     =>'',
                             'Param8'     =>'',
                             'Param9'     =>'',
                            
                          );
   
    $record['leftbanner'] = $this->supper_admin->call_procedureRow('proc_press',$parameter);


    $this->load->view("helper/head");
    $this->load->view("helper/header",$data);
    $this->load->view("index",$record);
    $this->load->view("helper/footer");
     $this->load->view("helper/foot");
  /*}
  else{
    redirect(base_url());
  }*/
  }

  public function newsletter(){
    $useremail=$this->input->post('newsemails');
    $parameter=array('act_mode'=>'insert',
                     'useremail'=>$useremail,
                     'n_status'=>'A',
                     'rowid'=>''
                    );
    $path = api_url().'userapi/newlettersub/format/json/';
    $data=curlpost($parameter,$path);
    //p($data);exit;
    //$p['mail']=array('useremail' => $useremail);
    $parameter5 =array('act_mode'=>'bizzgainconfig','row_id'=>'');
    $result['bizzgainconfig']= $this->supper_admin->call_procedureRow('proc_siteconfig',$parameter5);
    $emailconfig=$result['bizzgainconfig']->confivalue;
    if($data->lastid>0){
  
      echo 'Newletter Subscription Successfull.';
    }
    else if($data->lastid==0){
      echo 'You are already listed for Newsletter';
    }
    else{
      echo 'Opps! Something went wrong!!';
    }
  }

  public function privacy_policy(){
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>3);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/header",$data);
    $this->load->view("privacypolicy");
    $this->load->view("helper/footer");

  }

  public function contactus(){

   if($this->input->post('c_submit')){
      $c_name=$this->input->post('c_name');
      $c_email=$this->input->post('c_email');
      $c_mobile=$this->input->post('c_mobile');
      $c_enquiry=$this->input->post('c_enquiry');
//p($c_name);exit();
      $bizzmail="support@mindzshop.com";
      $replyemail="anurag.dua@MindzShop.com";
       
    $msg ='<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <div style="float: left; padding: 2px 2px; background: #fff; border: solid 1px #F3F0F0;">
        <div style="padding: 10px 10px; float: left; width: 600px; font-family: sans-serif; font-size: 12px; line-height: 18px; background: #F5F5F5;">
            <div style="float:left;text-align: center;width:100%;">
                <img src="'.base_url().'images/logo3.png" />
            </div>
            <div style="float: left; width: 100%; text-align: center; padding: 10px 0px; font-weight: bold; font-size: 20px; ">Contact Us Enquiry Information</div>
            <div style="float:left;width:100%;font-size:15px;padding:10px 0px;">Dear User</div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Name : <span style="font-weight:100;">'.$c_name.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Email : <span style="font-weight:100;">'.$c_email.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Contact Number : <span style="font-weight:100;">'.$c_mobile.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Enquiry : <span style="font-weight:100;">'.$c_enquiry.'</span></div>
            <div style="float:left;width:100%;font-weight:bold;padding:15px 0px 5px;">
                Regards,<br />
                MindzShop Team
            </div>
        </div>
        <div></div>
    </div>

</body>
</html>
';
         $this->load->library('email');
         $this->email->from($bizzmail, 'MindzShop.com');
         $this->email->to($bizzmail);
         $this->email->cc($replyemail);
         $this->email->subject('Contact Us BIZZGAIN');
         $this->email->message($msg);  
         if($this->email->send()){
          $this->session->set_flashdata("message", "Information sent successfully. we will contact you Shortly.");
          redirect(base_url()."contactus");
        }
        else {
          $this->session->set_flashdata("message", "Information fail to send. Please try again");
          redirect(base_url()."contactus");
        }

    }
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>9);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/header",$data);
    $this->load->view("contactus");
    $this->load->view("helper/footer");

  }

public function advancesearch(){
$myval = $this->input->post('thiavalue');
$cmid=$this->session->userdata('logincity')->cmid;
if(isset($_POST)){

if(!empty($this->input->post('thiavalue'))){

  $parameter = array(
  'act_mode'=>'product',
  'token'=>$this->input->post('thiavalue'), 
  'proName'=>'',
  'category_id'=>'',
  'barand_id'=>'',
  'size'=>'',
  'color'=>'',
  'minprice'=>'',
  'maxprice'=>'',
  'mindiscount'=>'',
  'pageorder'=>'',
  'pid'=>'',
  'perpage'=>'',
  'features'=>'',
  'procityidd'=>$cmid
  
  );

  //$parameter       = array('getval' => $this->input->post('thiavalue'),'act_mode'=>'product');
//p($parameter);
  $path            = api_url().'productlisting/autoadsearch/format/json/';
  $data['product'] = $response = curlpost($parameter, $path);
//p($data['product']);
}
  $this->load->view('searchdesine',$data);
} 
//}
//}
}

public function search1(){
  //p($_GET); die;
  //$this->userfunction->UserValidation();
//p($this->session->userdata('logincity')->cmid);exit;
$search=$this->input->get('token');
$catId=$this->input->get('catids');
$sdata=$this->input->get('sdata');
$newssdata= explode('#',$sdata);
//p($newssdata);exit;

foreach ($newssdata as $key => $value) {
 $newcat= explode(',',$value);
  $sdsa .=$newcat[0].',';
 $brandsdsa .= $newcat[2].',';



if($brandsdsa=='')
{
  $brandsdsa1 =  0;
  
}else{
  
  $brandsdsa1 =  rtrim($brandsdsa,',');
   $brandsdsa1 = implode(',', array_keys(array_flip(explode(',', $brandsdsa1))));

}

}

//echo "ab".$sdsa.'/'.$brandsdsa; exit; 
//p(trim($sdsa,','));

$cattid1=trim($sdsa,',');
//echo "s".$cattid1; exit;

$j=1;

//echo $search; exit;


if(!empty($catId)){
$cattid1=$catId;  
}  
if(!empty($sdata)){
$s_catid=$s_brandid=" ";
$realdata= str_replace("~"," ",$sdata);
$liararray= explode('|',$realdata);
//p($liararray);
foreach($liararray as $key => $value_cat ){
$list= explode(',',$value_cat);
//p($list);
if(trim(strtolower($list[3]))==trim(strtolower($search))){
$sbrand=$list[2];
//p($sbrand);
}
if(trim(strtolower($list[1]))==trim(strtolower($search))){
 $s_token_catid=$list[0]; 
}
if(strpos($s_brandid,$list[2])){
}else{
$s_brandid.=$list[2].',';
}
if($j<5){
if(strpos($s_catid,$list[0])){
}else{
//  $s_catid.=$list[0].',';
$s_catid=$list[0].','.$s_catid;
$j++;
}
}
}
$s_catid = str_replace(" ","",$s_catid);
$s_catid=rtrim($s_catid,',');
//$cattid1=$s_catid;
}

//if(empty($cattid1)){

if($cattid1==''){
  
$myurl=base_url().'searchnorecord';
redirect($myurl);
exit;
}
if(!empty($s_token_catid)){
$cattid1=$s_token_catid;
}
$pa = array('act_mode'=>'catsearch','row_id'=>$cattid1,'');
//p($pa); exit;
$data['catsearch']=$this->supper_admin->call_procedure('proc_productmasterdata',array('act_mode'=>'catsearch','row_id'=>$cattid1,''));
//p($data['catsearch']); exit();

$parameter=array('act_mode'=>'attview','row_id'=>$cattid1,'','','','','');
//p($parameter);exit;
if($this->input->get('show_brand')!=''){
  $brand=$this->input->get('show_brand');
  $brandurl='&show_brand='.$brand;
}
else if(empty($this->input->get('show_brand')) && !empty($this->input->get('sbrand'))){
   $brand=$this->input->get('sbrand');
   $brandurl='&show_brand='.$brand;
}
else if(!empty($this->input->get('show_brand')) && !empty($this->input->get('sbrand')))
{
  $brand=$this->input->get('sbrand');
  $brandurl='&show_brand='.$brand;
}

  
if($this->input->get('price')!=''){
  $price=explode('-',$this->input->get('price'));
  $minprice=$price[0];
  $maxprice=$price[1];
  $priceurl='&price='.$this->input->get('price');
}
else{
  $minprice=0;
  $maxprice=0;
  $priceurl=null; 
}

if($this->input->get('discount')!=''){
  $discount=$this->input->get('discount');
  $discounturl='&discount='.$discount;
  //echo "Hello".$discounturl; exit;
}
else{
  $discount=0;
  $discounturl=null;
}
if($this->input->get('color')!=''){
  $color=$this->input->get('color');
  $colorurl='&color='.$color;
}
else{
  $color=0;
  $colorurl=null;
}
if($this->input->get('size')!=''){
  $size=$this->input->get('size');
  $sizeurl='&size='.$size;
}
else{
  $size=0;
  $sizeurl=null;
}

if($this->input->get('feature')){
  $name=$this->input->get('feature');
  $name2 = str_replace("%20"," ", $name);
  $feature3=str_replace(",","|",$name2);
  $feature =strtolower($feature3);
  $featureurl='&feature='.$name;
}
else{
  $feature=0;
  $featureurl=null;
}

if($this->input->get('feature')){
  $feature=$this->input->get('feature');
  $featureurl='&feature='.$feature;
 }
else{
  $feature="0";
  $featureurl=null;
 }
if($this->input->get('porder')!=''){
  $pageorder=$this->input->get('porder');
  $porderulr='&porder='.$pageorder;
   }
else{
   $pageorder=0;
   $porderulr=null;
}
$totallistoffset   = $_POST['offset'];

if($totallistoffset==0){
  $totallistoffset = 12;
}else{
  $totallistoffset = $_POST['offset'];
  #$totallistoffset = $_POST['offset']+12;
}
$parameter=array('act_mode'=>'discount','','');
$data['fildiscount']=$this->supper_admin->call_procedure('proc_productmasterdata',$parameter);

$parameter =array('act_mode'=>'catatt','row_id'=>'','catid'=>$cattid1);
$data['coloratt'] = $this->supper_admin->call_procedure('proc_product',$parameter);
$excelHeadArr = array();
$excelHeadArrcvc = array();
$arrPart1 = array('Feature1','Feature2');
$allArr = array();
$templevel=0;  
$newkey=0;
$grouparr[$templevel]="";

foreach($data['coloratt'] as $k=>$v){
  $excelHeadArr[] = $v->attname;
  $excelHeadArrcvc[]=$v->id;
  $allArr[] = $v->attname;
  $grouparr[$k] = $v->id;
}
foreach ($excelHeadArrcvc as $key => $value) {
  $parameter=array('act_mode'=>'attvalue','row_id'=>$value,'pvalue'=>$cattid1);
  $data['attData'][$grouparr[$key]]=$this->supper_admin->call_procedure('proc_productmasterdata',$parameter);
  $j=0;
}
$c = 0;
foreach($data['attData'] as $k=>$v){ 
 foreach($v as $kk=>$vv){
 if($k == $vv->attid){
  $mainArr[$allArr[$c]][$vv->attmapid] = $vv->attvalue;}
}
$c++;
}
$data['attrMapData'] = $mainArr;
//p($brand); die;
if(isset($_GET['bysubmit'])){
  $parameterp = array(
  'proName'=>$search,
  'category_id'=>$cattid1,
  'barand_id'=>$brand,
  'size'=>$size,
  'color'=>$color,
  'minprice'=>$minprice,
  'maxprice'=>$maxprice,
  'mindiscount'=>$discount,
  'pageorder'=>$pageorder,
  'pid'=>0,
  'perpage'=>6,
  'features'=>$feature,
  'procityidd'=>$this->session->userdata('logincity')->cmid
  //'token'=>$search,
  //'actmode'=>'check' 
);
//p($parameterp); exit;
$data['resultspage']=$this->supper_admin->call_procedure('proc_Productlistnew',$parameterp);


}else{
$parameterp = array(
  'proName'=>$search,
  'category_id'=>$cattid1,
  'barand_id'=>$brand,
  'size'=>$size,
  'color'=>$color,
  'minprice'=>$minprice,
  'maxprice'=>$maxprice,
  'mindiscount'=>$discount,
  'pageorder'=>$pageorder,
  'pid'=>0,
  'perpage'=>10,
  'features'=>$feature,
  'procityidd'=>$this->session->userdata('logincity')->cmid
  //'token'=>'',
  //'actmode'=>'' 
  );
//p($parameterp); exit;
$data['resultspage']=$this->supper_admin->call_procedure('proc_Productlistnew',$parameterp);
}
//p(count($data['resultspage'])); exit();
//p($data['resultspage']);exit;
if(empty($this->input->get('sbrand')))
{
  $data['sbrand']=$brand;
}
else
{
  $data['sbrand']=$this->input->get('sbrand');
}
//$data['brandsss']=$brand;
 $data['s_data']=$cattid1;
 
$this->load->view("helper/header");
$this->load->view("productlist/search",$data);
$this->load->view("helper/footer");
}


public function search(){
  //p($_GET['token']); 
//$this->userfunction->UserValidation();

$searchkaro=trim($this->input->get('token'));
$search= str_replace("+", " ", $searchkaro);

/****************************** Working for filter ***********************************/

  if(!empty($this->input->get('price')))
  {
    $price1 = $this->input->get('price'); 
    $price = explode('-', $price1);
    $condition =1;
    //p($price);
  }
  else
  {
      $price[0]=0;
      $price[1]=0;
  }
  if(!empty($this->input->get('size')))
  {
    $size = $this->input->get('size');
    $condition =1;
  }
  else
  {
      $size=0;
  }
  if(!empty($this->input->get('show_brand')))
  {
    $brand = $this->input->get('show_brand');
    $condition =1;
  }
  else
  {
      $brand=0;
  }
  if(!empty($this->input->get('discount')))
  {
    $discount = $this->input->get('discount');
    $condition =1;
  }
  else
  {
      $discount=0;
  }
  if(!empty($this->input->get('color')))
  {
    $color = $this->input->get('color');
    $condition =1;
  }
  else
  {
      $color=0;
  }

  if(!empty($this->input->get('weight'))){
  $weight    = $this->input->get('weight');
  
  }
  else{
    $weight    = 0;
    
  }
  if(!empty($this->input->get('food'))){
    $food    = $this->input->get('food');
    
  }
  else{
    $food    = 0;
    
  }


  if(!empty($this->input->get('porder')))
  {
    $porder = $this->input->get('porder');
    $condition =1;
  }
  else
  {
      $porder=0;
  }

/****************************** End Working for filter ***********************************/


  $parameterp = array(
  'act_mode'=>'search',
  'token'=>$search, 
  'proName'=>$condition,
  'category_id'=>'0',
  'barand_id'=>$brand,
  'size'=>$food,
  'color'=>$weight,
  'minprice'=>$price[0],
  'maxprice'=>$price[1],
  'mindiscount'=>$discount,
  'pageorder'=>$porder,
  'pid'=>'0',
  'perpage'=>'6',
  'features'=>'0',
  'procityidd'=>$this->session->userdata('logincity')->cmid );
//p($parameterp); exit;
$data['resultspage']=$this->supper_admin->call_procedure('proc_search',$parameterp);
//p($data['resultspage']);exit;

if(count($data['resultspage'])=='' && $condition!=1)
{ 
    $myurl=base_url().'searchnorecord';
    redirect($myurl);
    exit;
}

$parameterfilter = array(
  'act_mode'=>'searchfilter',
  'token'=>$search, 
  'proName'=>'',
  'category_id'=>'',
  'barand_id'=>'',
  'size'=>'',
  'color'=>'',
  'minprice'=>'',
  'maxprice'=>'',
  'mindiscount'=>'',
  'pageorder'=>'',
  'pid'=>'',
  'perpage'=>'',
  'features'=>'',
  'procityidd'=>''
  
  
);
//p($parameterp); //exit;
$data['filter']=$this->supper_admin->call_procedure('proc_searchfilter',$parameterfilter);
//p($data['filter']);exit();
if(!empty($data['filter']))
{
    foreach ($data['filter'] as $key => $catidlue) 
    {
        $cid[]= $catidlue->CatId;
        $sbrand[]=$catidlue->BrandId;
        $discountserach[]=$catidlue->FDisId;
                
    }
    //p($discountserach);exit;
    $newcid=array_unique($cid);
    $newcidprice= implode(',', $newcid);
    $newbid=array_unique($sbrand);
    $olddiscount=array_unique($discountserach);
    $newdiscount= implode(',', $olddiscount);
    //p($newcidprice);//exit();
    /*foreach ($newcid as $key => $catlue) 
    {
        $catfilter[] = catList2((int)$catlue);
    }  
    //exit();
    $data['catfilter']=$catfilter;*/
    $data['catfilter'] = filtercatList2($newcidprice);
//p($data['catfilter']);
    $sbrands=implode(',', $newbid);
    $catbrand = catBrand2($sbrands);
    $data['catbrand']=$catbrand;
    $catprice = catPricesearch($newcidprice,$search);
    $data['catprice']=$catprice;
    //p($data['catprice']);
}
#$allcat='3,12,13,6';
#p($data['catfilter']);
$data['CatId']=$data['catfilter'];
#p($data['CatId']);
 foreach ($data['catfilter'] as $ke => $va) {
     
        $allcat.=$va['childid'].',';
     
    }

 $catbrand = catBrand(trim($allcat,','));
//p($allcat);
     $parameter=array('act_mode'=>'getcatname','row_id'=>'','catparentid'=>'','c_catname'=>trim($allcat,','));
    $data['CateName']=$this->supper_admin->call_procedure('proc_catname',$parameter);
//p($data['CateName']);
$parameterp=array('act_mode'=>'getparentcatname','row_id'=>'','catparentid'=>'','c_catname'=>trim($allcat,','));
$data['parentCateName']=$this->supper_admin->call_procedure('proc_catname',$parameterp);
//p($data['parentCateName']);
$data['BrandName']= $catbrand;

//p($data['BrandName']);
#$catprice='3,11,12';
$catprice = trim($allcat,',');
$data['$catprice']= catPrice((int)$catprice);

$parameter           = array('act_mode'=>'discount','','');
$data['fildiscount'] = $this->supper_admin->call_procedure('proc_productmasterdata',$parameter);

/*$parameter=array('act_mode'=>'searchdiscount','row_id'=>$newdiscount,'pvalue'=>'');
$data['fildiscount']=$this->supper_admin->call_procedure('proc_productmasterdata',$parameter);*/
//p($data['fildiscount']);exit;

$parameter            = array('act_mode'=>'catatt','row_id'=>'','catid'=>trim($allcat,','));
$data['coloratt']     = $this->supper_admin->call_procedure('proc_product',$parameter);
//p($data['coloratt']);exit();
$excelHeadArr = array();
$excelHeadArrcvc = array();
$arrPart1 = array('Feature1','Feature2');
$allArr = array();
$templevel=0;  
$newkey=0;
$grouparr[$templevel]="";

foreach($data['coloratt'] as $k=>$v){
  $excelHeadArr[] = $v->attname;
  $excelHeadArrcvc[]=$v->id;
  $allArr[] = $v->attname;
  $grouparr[$k] = $v->id;
}

foreach ($excelHeadArrcvc as $key => $value) {
  //$parameter=array('act_mode'=>'searchattvalue','row_id'=>$value,'pvalue'=>$newcidprice);
  $parameteratt = array(
  'act_mode'=>'searchattvalue',
  'token'=>$search, 
  'proName'=>'',
  'category_id'=>$newcidprice,
  'barand_id'=>'',
  'size'=>$value,
  'color'=>'',
  'minprice'=>'',
  'maxprice'=>'',
  'mindiscount'=>'',
  'pageorder'=>'',
  'pid'=>'',
  'perpage'=>'',
  'features'=>'',
  'procityidd'=>''
  
  
);
//p($parameteratt); exit;
  $data['attData'][$grouparr[$key]]=$this->supper_admin->call_procedure('proc_searchfilter',$parameteratt);
  $j=0;
}
//p($data['attData']);exit();
$c = 0;
foreach($data['attData'] as $k=>$v){ 
 foreach($v as $kk=>$vv){
 if($k == $vv->attid){
  $mainArr[$allArr[$c]][$vv->attmapid] = $vv->attvalue;}
}
$c++;
}
$data['attrMapData'] = $mainArr;

 
$this->load->view("helper/header");
$this->load->view("productlist/search",$data);
$this->load->view("helper/footer");

}
public function searchnorecord(){
  //if($this->session->userdata('bizzgainmember')->id){
  $this->load->view("helper/header");

$this->load->view("productlist/no-record");
$this->load->view("helper/footer");
/*}
  else
  {
    redirect(base_url());
  }*/

}

public function success($ordid){
  //p('hi');
  //p($ordid);
  //exit;
  //$ordid=1062;
   $this->cart->destroy();
   $this->session->unset_userdata('toal_cart_disc');
   $this->session->unset_userdata('cart_tax');

  //$path = api_url().'checkoutapi/thankyou/orderId/'.$ordid.'/format/json/';
  //$response['viewthanku'] = curlget($path);

$path = api_url().'checkoutapi/thankyou/orderId/'.$ordid.'/userid/'.$this->session->userdata('foxsky')->id.'/format/json/';
  $response['viewthanku'] = curlget($path);


  //exit;

 $this->load->view("helper/head");
    $this->load->view("helper/header");
    $this->load->view("thankyou",$response);
    $this->load->view("helper/footer");
    $this->load->view("helper/foot");

}

public function landing(){
  
    //if($this->session->userdata('bizzgainmember')->id){
    redirect(base_url()."home");
 /* }
  else{
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>1);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("landingpage",$data);
     $this->load->view("helper/footer");
  }*/
}

public function subscribe()
{
  $useremail=$this->input->post('subs');
  if(!empty($useremail))
  {
    $parameter=array('act_mode'=>'insert',
                     'useremail'=>$useremail,
                     'n_status'=>'A',
                     'rowid'=>''
                    );
    $path = api_url().'userapi/newlettersub/format/json/';
    $data=curlpost($parameter,$path);
  }
    
    //$this->session->unset_userdata('bizzgainmember');
    redirect(base_url().'myaccount');

}


 public function aboutus(){
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>2);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("aboutus");
    $this->load->view("helper/footer");
     $this->load->view("helper/foot");
  }

  public function useraggree(){
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>2);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("useragree");
    $this->load->view("helper/footer");
     $this->load->view("helper/foot");
  }

  public function support(){
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>2);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("support");
    $this->load->view("helper/footer");
     $this->load->view("helper/foot");
  }
  
  public function privacy(){
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>2);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("privacy");
    $this->load->view("helper/footer");
     $this->load->view("helper/foot");
  }

  public function career(){
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>2);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("career");
    $this->load->view("helper/footer");
     $this->load->view("helper/foot");
  }

  public function terms(){
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>2);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("terms");
    $this->load->view("helper/footer");
     $this->load->view("helper/foot");
  }
 public function returnpolicy(){
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>2);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("returnpolicy");
    $this->load->view("helper/footer");
     $this->load->view("helper/foot");
  }
  public function contactusdata(){
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>2);
    $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    $this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("contactus");
    $this->load->view("helper/footer");
     $this->load->view("helper/foot");
  }
  

 public function ajaxsearch(){
 //p($this->uri->segments);
 //p($_POST['brand']); //exit();

  $search=$_POST['token'];

$totallistoffset1   = $_POST['offset'];

if($totallistoffset1==0)
{
    $totallistoffset = 0;
}
else
{
    $totallistoffset = $_POST['offset'];
}


/****************************** Working for filter ***********************************/

  if(!empty($_POST['price']))
  {
    $price1 = $_POST['price']; 
    $price = explode('-', $price1);
    //p($price);
  }
  else
  {
      $price[0]=0;
      $price[1]=0;
  }
  if(!empty($_POST['food']))
  {
    $food = $_POST['food'];
  }
  else
  {
      $food=0;
  }
  if(!empty($_POST['brand']))
  {
    $brand = $_POST['brand'];
  }
  else
  {
      $brand=0;
  }
  if(!empty($_POST['dis']))
  {
    $discount = $_POST['dis'];
  }
  else
  {
      $discount=0;
  }
  if(!empty($_POST['weight']))
  {
    $weight = $_POST['weight'];
  }
  else
  {
      $weight=0;
  }
  if(!empty($_POST['order']))
  {
      $porder = $_POST['order'];
  }
  else
  {
      $porder=0;
  }



/****************************** End Working for filter ***********************************/
  $parameterp = array(
  'act_mode'=>'search',
  'token'=>$search, 
  'proName'=>'',
  'category_id'=>'0',
  'barand_id'=>$brand,
  'size'=>$food,
  'color'=>$weight,
  'minprice'=>$price[0],
  'maxprice'=>$price[1],
  'mindiscount'=>$discount,
  'pageorder'=>$porder,
  'pid'=>$totallistoffset,
  'perpage'=>$_POST['number'],
  'features'=>'0',
  'procityidd'=>$this->session->userdata('logincity')->cmid );
//p($parameterp); //exit;
$data['resultspage']=$this->supper_admin->call_procedure('proc_search',$parameterp);


$data['image_view']=$_POST['seetyp'];
//p($data);//exit();

 $this->load->view('productlist/searchnew',$data);

  }

public function feedback(){

  if($this->input->post('submit')){
    $fd_name=$this->input->post('name');
    $fd_email=$this->input->post('email');
    // $fd_mobile=$this->input->post('fd_mobile');
    $fd_comment=$this->input->post('comment');

    $param = array('act_mode'=>'addfeedback',
                   'row_id'=>'', 
                   'Param3'=>$fd_name,
                   'Param4'=>$fd_email,
                   'Param5'=>'',
                   'Param6'=>'',
                   'Param7'=>'',
                   'Param8'=>$fd_comment,
                   'Param9'=>'',
                   'Param10'=>'');
    $addfeedback=$this->supper_admin->call_procedure('proc_feedback',$param);
    // p($addfeedback);die;

    $res = '<div class="alert alert-success" style="text-align:center">
  <strong>Success!</strong> Feedback added successfully.
</div>';

    $this->session->set_flashdata("message", $res);
    redirect(base_url().'feedback');
  }
    $params = array('act_mode'=>'frontview_feedback',
                   'row_id'=>'', 
                   'Param3'=>'',
                   'Param4'=>'',
                   'Param5'=>'',
                   'Param6'=>'',
                   'Param7'=>'',
                   'Param8'=>'',
                   'Param9'=>'',
                   'Param10'=>'');
    $record['viewfeedback']=$this->supper_admin->call_procedure('proc_feedback',$params);
$this->load->view("helper/head");
  $this->load->view("helper/header");
  $this->load->view("feedback",$record);
  $this->load->view("helper/footer");
  $this->load->view("helper/foot");
}

public function cancel($ordid){
  //p('hi');
  //p($ordid);
  //exit;
  //$ordid=1062;
   //$this->cart->destroy();
   //$this->session->unset_userdata('toal_cart_disc');
  // $this->session->unset_userdata('cart_tax');

  //$path = api_url().'checkoutapi/thankyou/orderId/'.$ordid.'/format/json/';
  //$response['viewthanku'] = curlget($path);

$path = api_url().'checkoutapi/thankyou/orderId/'.$ordid.'/userid/'.$this->session->userdata('foxsky')->id.'/format/json/';
  $response['viewthanku'] = curlget($path);


  //exit;

 $this->load->view("helper/head");
    $this->load->view("helper/header");
    $this->load->view("cancelorder",$response);
    $this->load->view("helper/footer");
    $this->load->view("helper/foot");

}

}//end of class
?>