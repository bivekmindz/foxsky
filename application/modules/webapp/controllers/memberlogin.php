<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Memberlogin extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
    $this->load->library('upload');
    $this->load->library('session');
    $this->load->library('email');

  }

  public function registernew(){

      
      $fname     =$this->input->post('fmem_fname');
      $lname     =$this->input->post('fmem_lname');
      $emailid   =$this->input->post('fmem_email');
      $mobile    =$this->input->post('fmem_mobile');
      $pass      =$this->input->post('fmem_pass');
      $acctype   =$this->input->post('facctype');
      $shipstate =$this->input->post('fmem_shipstate');
      $shipcity  =$this->input->post('fmem_shipcity');
      $cstid     ='';
      $pincode   =$this->input->post('fmem_cstid');
      $cstvalue  ='';
      $dob       =$this->input->post('fmem_cstvalue');
      $tinid     =$this->input->post('fmem_tinid');
      $tinvalue  ='';
      $doa       =$this->input->post('fmem_tinvalue');
      $compname  ='';
      $address   =$this->input->post('fmem_compname');
      $num       =rand(111111, 999999); 
      $unicode   =$acctype.$num;


      $parameter     =array(
       'act_mode' =>'frontmemberinsert',
       'row_id'   =>'',
       'ventype'  =>$acctype,
       'vencode'  =>$unicode,
       'vcomp'    =>$compname,
       'vemail'   =>$emailid,
       'vcontact' =>$mobile,
       'vpass'    =>$pass,
       'vname'    =>$fname,
       'vlast'    =>$lname,
       'vcatid'   =>'',
       'vcountryid'=>'',
       'vstateid' =>$shipstate,
       'vcityid'  =>$shipcity,
       'vaddress' =>$address,
       'vpincode' =>$pincode,
       'vpannumber'=>'',
       'vtinnumber'=>'',
       'accontname'=>'',
       'accnumber'=>'',
       'bankname' =>'',
       'branchname'=>'',
       'ifsccode' =>'',
       'neftdetail'=>$cstid,
       'acctype'  =>$cstvalue,
       'cstnumber'=>$tinid,
       'vatnumber'=>$tinvalue,
       'cstidd'    => '',
       'tinidd'    => '',
       'vatcopy'   => '',
       'cstcopy'   => '',
       'chequecopy'=>  $dob,
       'pancopy'   =>  $doa);
      //p($parameter);exit();
      $memloginapi = api_url().'userapi/memberregister/format/json/';
      $response = curlpost($parameter,$memloginapi);
     // p($response);exit();
      //------------------ city group id -----------------------//
      $parameter1=array(
          'act_mode' =>'citygroupid',
          'row_id' =>$response->city,
          'vusername' =>$response->stateid,
          'vpassword'=>'',
          'vstatus' =>'',
          );
      $path1=api_url().'userapi/citygrouplogin/format/json/'; 
      $record=curlpost($parameter1,$path1);
      //-------------------end-----------------------------------//
     // p($record); exit;
      if($response->id==001){
          echo "existemail";
      } else if($response->id==002) {
          echo "existmobileno";
      } else {
          
          $this->session->set_userdata('bizzgainmember', $response);
          $this->session->set_userdata('sesregister', 'registeruser');//for alert message on home page to complete profile
          //----------------- start default city group ---------------------
          if($record->scalar=='Something Went Wrong'){
            $defaultrecord->cmid=3;
            $this->session->set_userdata('logincity',$defaultrecord);
            //p($this->session->userdata('logincity'));exit;
          } else {
            $this->session->set_userdata('logincity',$record);
          }
          //----------------- end default city group ---------------------
          //$this->session->set_userdata('logincity',$record);
          $loginacctype = $this->session->userdata('bizzgainmember')->vendortype;
          //------------email--------------
          $hmtopemail="support@mindzshop.com";
          $userinfo['userdet']=array('useremail'=>$emailid,'userfname'=>$fname,'userlname'=>$lname,'userpass'=>$pass);
          // $msg = $this->load->view('bizzgainmailer/registermail',$userinfo, true);
          // $this->load->library('email');
          // $this->email->from($hmtopemail, 'MindzShop.com');
          // $this->email->to($emailid); 
          // $this->email->subject('Your Registration Details - ('.$emailid.')');
          // $this->email->message($msg);
          // $this->email->send();
          //------------email--------------
          if($loginacctype == "manufacturer"){
            echo "manufacturer";
          }
      }
 }

  public function register(){


if(isset($_POST['stepfoursubmit'])){
echo "<pre>";
print_r($_POST);

}


/*    if($this->session->userdata('bizzgainmember')->id){
      redirect(base_url()."home");
    }
    
   $path = api_url().'userapi/shipstate/format/json/';
   $record['viewshipstate']  = curlget($path);
   //pend($record['viewshipstate']);

   $this->load->view("helper/head");
   $this->load->view("register",$record);
    $this->load->view("helper/foot");*/
  

  }

  public function forgotpwd(){
  $email=$this->input->post('forgetemail');
    if($this->input->post('submit')=='Submit'){
      // echo "string";
      // exit();
    $parameter=array('act_mode' =>'check_email_id_exit' ,
                    'param1'=>$email,
                    'param2'=>'',
                    'param3'=>'',
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');

    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);
    // p($response);
    // exit();
    if($response->c>0)
    {

      $num = rand(1111, 9999);
      $parameter=array('act_mode' =>'passwordChange' ,
                    'param1'=>$email,
                    'param2'=>base64_encode($num),
                    'param3'=>'',
                    'param4'=>'',
                    'param5'=>'',
                    'param6'=>'',
                    'param7'=>'',
                    'param8'=>'',
                    'param9'=>'',
                    'param10'=>'');
 // p($parameter);
    $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);



    $this->email->from('ankit@mindztechnology.com');
    $this->email->to($email);
    $this->email->set_mailtype("html");
    //$this->email->cc('student@guruq.in'); 
    $this->email->subject('Forgot Password mail');
    #$link = $this->load->view('link',$email,true);
    
    $this->email->message($num);  
    $this->email->send();
$data['message']="Mail send to your mail id";
 //   echo "Mail send to your mail id";

    
    }
    else{
$data['message']=$email."is not registered";
    //  echo $email." is not registered";
    }
    }

    $this->load->view("forgotpwd",$data);
  
  }


  

  public function verifyotp(){


    $smsphnon = $this->input->post('umob');
   // $smsacctype = $this->input->post('acctype');
    $num = rand(1111, 9999);
    $parameter =array('act_mode'=>'chmobileotp',
                      'row_id'=>'',
                      'p_userid'=>'',
                      'p_email'=>'',//$smsacctype,
                      'p_mobilenum'=>$smsphnon,
                      'p_mas'=>'');
    $path1=api_url().'userapi/chmobileotp/format/json/'; 
    $record=curlpost($parameter,$path1);
    //p($record->cont);exit();
    if($record->cont>0){
      echo "Mobile number already exist.";
    } else {
      $smsmsgg = urlencode('Hi User, your OTP Password is '.$num.' for Mobile Number Verification.'); 
      #$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');
      echo $num;
    }
    
  }


  public function verifypincode(){

    $upin = $this->input->post('upin');
    
    $parameter =array('pincode'=>$upin);
    $path1=api_url().'userapi/verifypincode/format/json/'; 
    $record=curlpost($parameter,$path1);
    echo $record->cont;
   
  }

  public function citystate(){
    $stid               = $this->input->post('stid');
    $path = api_url().'userapi/shipcity/stateid/'.$stid.'/format/json/';
    $record['viewshipcity']  = curlget($path);
    if($record['viewshipcity']->scalar!='Something Went Wrong'){
    $str = '';
    #$str .= '<option value="">Select City</option>';
    foreach($record['viewshipcity'] as $k=>$v){
      $str .= "<option value=".$v['cityid'].">".$v['cityname']."</option>";
    }
      echo $str;
    }
  } 
  
  public function citystatecheckoutpanel(){
    $stid               = $this->input->post('stid');
    $path = api_url().'userapi/shipcity/stateid/'.$stid.'/format/json/';
    $record['viewshipcity']  = curlget($path);
    if($record['viewshipcity']->scalar!='Something Went Wrong'){
    $str = '';
    //$str .= '<option value="">Select City</option>';
    foreach($record['viewshipcity'] as $k=>$v){
     $str .= "<option value=".$v['cityname'].'_'.$v['cityid'].">".$v['cityname']."</option>";
    }
      echo $str;
    }
  } 

  public function citystateprofile(){

    $parameters=array(
          'act_mode' =>'meview',
          'row_id' =>$this->session->userdata('bizzgainmember')->id,
          'vusername' =>'',
          'vpassword'=>'',
          'vstatus' =>'',
          );
    $paths=api_url().'userapi/memdetailview/format/json/'; 
    $data['memview']=curlpost($parameters,$paths);
    $cityidprofile=$data['memview']->city;
    $stid               = $this->input->post('stid');
    $path = api_url().'userapi/shipcity/stateid/'.$stid.'/format/json/';
    $record['viewshipcity']  = curlget($path);
    $str = '';

    foreach($record['viewshipcity'] as $k=>$v){
      if($v['cityid']==$cityidprofile){
        $str .= "<option selected value=".$v['cityid'].">".$v['cityname']."</option>";
      } else {
        $str .= "<option value=".$v['cityid'].">".$v['cityname']."</option>";
    }
    }
      echo $str;
  } 

  public function memlogin(){



/*    if($this->session->userdata('bizzgainmember')->id){
      redirect(base_url()."home");
    }*/

      if(isset($_POST['submit'])){

    //     echo "<pre>";
    // print_r($_POST);
    // die();

        $parameter=array(
          'act_mode' =>'memlogin',
          'param1'=>$this->input->post('email'),
          'param2'=>base64_encode($this->input->post('pwd')),
          'param3'=>'',
          'param4'=>'',
          'param5'=>'',
          'param6'=>'',
          'param7'=>'',
          'param8'=>'',
          'param9'=>'',
          'param10'=>'');
  $response=$this->supper_admin->call_procedureRow('proc_1',$parameter);

 // p($parameter);
  // echo"<pre>";
  // print_r($response);
  // die;

      if(!empty($response))
    {
      $this->session->set_userdata('foxsky', $response);
      redirect(base_url()."myaccount");
    }
    else
    {
      $this->session->set_flashdata('message', 'Invalid Login!');
      //redirect(site_url('foxsky/login'));
    }
      
      // redirect(base_url()."home");
  // echo "Successfully Login";
    }
  

     $this->load->view('helper/head');
    $this->load->view("login",$data);
    //$this->load->view('helper/footer');
 
  
  }


 function removeproduct(){
        $wishid=$_GET['wish_id'];
        $url= api_url().'productlisting/removewishlist/wish_id/'.$wishid.'/format/json/';
        $response['deleteproduct'] = curlget($url);
       // p($response['deleteproduct']);
}
function removewishproduct(){
        $wishid=$_GET['wish_id'];
        $uid=$this->session->userdata('bizzgainmember')->id;
        $url= api_url().'productlisting/deletewishlist/wish_id/'.$wishid.'/uid/'.$uid.'/format/json/';
        $response['deleteproduct'] = curlget($url);
        
}


 public function tracking(){
  $trackinguid=$_REQUEST['trackuserid'];
    if($this->session->userdata('bizzgainmember')->id==$trackinguid){
      redirect(base_url().'myaccount');
    } else {
      redirect(base_url());
    }
  }

  public function myaccount(){
    //pend('hghjh');
   
    if($this->session->userdata('bizzgainmember')->id==''){
     // pend('hghgh');
      redirect(base_url());
    }
    else{
     if($this->uri->segment(4)!='')
     {
      $this->session->set_userdata('title',$this->uri->segment(4));
      redirect('myaccount');exit();
     }
    $this->load->view("helper/header");
    //$this->load->view("helper/nav1");
    //$this->load->view("helper/nav2");
    
    $userLoginId = $this->session->userdata('bizzgainmember')->id;
    $usercitygroupid = $this->session->userdata('logincity')->cmid;
    //-------------------- wishlist -----------------------------------//
    $wishlistpath=api_url().'productlisting/wishlist/user_id/'.$userLoginId.'/usercityid/'.$usercitygroupid.'/format/json/'; 
    $data['wishlist']=curlget($wishlistpath);
    //-------------------- end ----------------------------------------//
    //-------------------- start wallet -----------------------------------//
    $walletdrcrpath=api_url().'walletapi/drcrwallet/user_id/'.$userLoginId.'/format/json/'; 
    $data['walletdrcr']=curlget($walletdrcrpath);
    //-------------------- end wallet ----------------------------------------//
    //--------------------orderhist------------------------------------//
    $orderpath= api_url().'userapi/orderdetail/userid/'.$userLoginId.'/format/json/';
    if(!empty($orderpath)){
      $data['order_detail']= curlget($orderpath);
      
    }
    //--------------------end  ----------------------------------------//
    $userid =   $this->session->userdata('bizzgainmember'); 
    $parameter = array('userid' =>$userid->id );
    if($this->input->post('memprofile')){
      $checkimg2 = explode("\\",basename($_FILES['memchequefile']['name']));
      $checkimg = $checkimg2[0];
      
      $panimg2 = explode("\\",basename($_FILES['mempanfile']['name']));
      $panimg = $panimg2[0];
      $tinimg2 = explode("\\",basename($_FILES['memvatfile']['name']));
      $tinimg = $tinimg2[0];
      $cstimg2 = explode("\\",basename($_FILES['memcstfile']['name']));
      $cstimg = $cstimg2[0];
      $field_name= 'memchequefile';

      $Imgdata1 = $this->do_upload($field_name);
      $field_name1= 'mempanfile';
      $Imgdata2 = $this->do_upload($field_name1);
      $field_name2= 'memvatfile';
     // p($field_name2);exit;
      $Imgdata3 = $this->do_upload($field_name2);
      $field_name3= 'memcstfile';
      $Imgdata4 = $this->do_upload($field_name3);

      $mmcstnum=$this->input->post('memcst');
      $mmtinnum=$this->input->post('memvat');
      if(empty($mmcstnum)){
          $mmcstid=4;
      } else {
          $mmcstid=2;
      }
      if(empty($mmtinnum)){
          $mmtinid=6;
      } else {
          $mmtinid=1;
      }

      $parameter=array('act_mode'      =>'memupdate',
                       'row_id'        =>$userid->id,
                       'mname'         =>$this->input->post('memfname'),
                       'mlastname'     =>$this->input->post('memlname'),
                       'memail'        =>$this->input->post('mememail'),
                       'mmobile'       =>$this->input->post('memcontact'),
                       'mstate'        =>$this->input->post('memstate'),
                       'mcity'         =>$this->input->post('memcity'),
                       'mcstnumber'    =>$mmcstnum,
                       'mtinnumber'    =>$mmtinnum,
                       'mpannumber'    =>$this->input->post('mempan'),
                       'mconpanyname'  =>$this->input->post('memcompany'),
                       'maccname'      =>$this->input->post('memaccname'),
                       'maccountnumber'=>$this->input->post('memaccnum'),
                       'mbranch'       =>$this->input->post('membranch'),
                       'mifsccode'     =>$this->input->post('memifsc'),
                       'mneft'         =>$this->input->post('mamneft'),
                       'macctype'      =>$this->input->post('memacctype'),
                       'mvatcopy'      =>time().$tinimg,
                       'mcstcopy'      =>time().$cstimg,
                       'mchequecopy'   =>$this->input->post('memdob'),#time().$checkimg,
                       'mpancopy'      =>$this->input->post('memdoa'),#time().$panimg,
                       'mcstid'        =>$mmcstid,
                       'mtinid'        =>$mmtinid,
                       'maddress'      =>$this->input->post('memaddress'),
                       'mpincode'      =>$this->input->post('mempincode'));
      $path=api_url().'userapi/memprofileupdate/format/json/'; 
      $response=curlpost($parameter,$path);


       //------------------ city group id -----------------------//
      $parameter11=array(
          'act_mode' =>'citygroupid',
          'row_id' =>$response->city,
          'vusername' =>$response->stateid,
          'vpassword'=>'',
          'vstatus' =>'',
          );
     // p($parameter11);//exit();
      $path11=api_url().'userapi/citygrouplogin/format/json/'; 
      $record=curlpost($parameter11,$path11);
     // p($record);//exit();

      //-------------------end-----------------------------------//
      if(!empty($response->id)){
        $this->session->set_userdata('bizzgainmember',$response);
        $this->session->set_userdata('logincity',$record);

        redirect(base_url()."myaccount");
      }
      

    }


    $parameterus=array('loginid'=>$this->session->userdata('bizzgainmember')->id);
    $data['coupon1'] = $this->supper_admin->call_procedure('proc_mycopoun', $parameterus);
    $parameters=array(
          'act_mode' =>'meview',
          'row_id' =>$this->session->userdata('bizzgainmember')->id,
          'vusername' =>'',
          'vpassword'=>'',
          'vstatus' =>'',
          );
    $paths=api_url().'userapi/memdetailview/format/json/'; 
    $data['memview']=curlpost($parameters,$paths);
     
    $path = api_url().'userapi/shipstate/format/json/';
    $data['viewshipstate']  = curlget($path);

    $parameter10=array(
          'act_mode' =>'retailerpaydetail',
          'row_id' =>$this->session->userdata('bizzgainmember')->id,
          'vusername' =>'',
          'vpassword'=>'',
          'vstatus' =>''
          );
    $path10=api_url().'userapi/mempaymentdetailview/format/json/'; 
    $data['orddetail']=curlpost($parameter10,$path10);
    $parameter12=array(
          'act_mode' =>'remtotalretailerpaydetail',
          'row_id' =>$this->session->userdata('bizzgainmember')->id,
          'vusername' =>'',
          'vpassword'=>'',
          'vstatus' =>''
          );
    $path12=api_url().'userapi/mempaymentdetailview/format/json/'; 
    $data['totalrempay']=curlpost($parameter12,$path12);
    //p($data);exit;
    $this->load->view("myaccount",$data);
    $this->load->view("helper/footer");
  }
  
  }



  public function allorder(){

    $userLoginId = $this->session->userdata('bizzgainmember')->id;
    $usercitygroupid = $this->session->userdata('logincity')->cmid;
   
    $orderpath= api_url().'userapi/orderdetail/userid/'.$userLoginId.'/format/json/';
    if(!empty($orderpath)){
      $data['order_detail']= curlget($orderpath);
      
    }
   
    $this->load->view("helper/header");
    $this->load->view("allorder",$data);
    $this->load->view("helper/footer");

  
  }

  
  public function do_upload($field_name){

    //$userLoginId = $this->session->userdata('bizzgainmember')->id;
    $config['upload_path'] = './manufacturer-files/';
    $config['allowed_types'] = 'jpg|jpeg|gif|png|pdf';
    $config['max_size'] = '10000000';
    $config['file_name']  = time().$_FILES[$field_name]['name'];
    
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload($field_name)){
        $data['error'] = array('error' => $this->upload->display_errors());
        //p($data['error']);exit();
        
    } else {
        $data['name'] = array('upload_data' => $this->upload->data());
    }
    return $data;
  }

  public function memberLogout(){
    $data['userdet']=$this->session->userdata('bizzgainmember');
    $path = api_url().'productlisting/sendwishlists/act_mode/get_wishlist/userid/'.$data['userdet']->id.'/cityid/'.$this->session->userdata('logincity')->cmid.'/format/json/';

    $data['mailer']= (array) curlget($path);
    if(!empty($data['mailer'][0]['proname']))
    {
      $name  = $data['userdet']->firstname;
      $email = $data['userdet']->lastname;
      $bizzgainemail="support@mindzshop.com";
      //$bizzgainemail="shibbu@mindztechnology.com";
      $message=$this->load->view('bizzgainmailer/wishlist',$data,true);
      $this->load->library('email');
      $this->email->from($bizzgainemail, 'MindzShop.com');
      $this->email->to($data['userdet']->compemailid); 
      $this->email->bcc($bizzgainemail);
      $this->email->subject('Your Wish List - ('.$data['userdet']->compemailid.')');
      $this->email->message($message);  
      $this->email->send();
      foreach ($data['mailer'] as $value) {
      $path = api_url().'productlisting/updatewishlist/format/json/';
      $paramiter=array('userid'=>$data['userdet']->id,'cityid'=>$value['WishlistID']);
      $data['res']=curlpost($paramiter,$path);
    }
      
      
    }
    
    session_start();
    $this->session->sess_destroy();
    session_destroy();
    unset($_SESSION);
    redirect();
    exit;
      
  }

  function oldPassword(){
    
    $userdata = $this->session->userdata('bizzgainmember');
    $userLoginId= $userdata->id;
    $this->form_validation->set_rules('old_password', 'Old Password', 'required|xss_clean');
    
    if($this->form_validation->run()== true){
            $params = array('userid'=>$userLoginId, 'old_password' => $this->input->post('old_password') ); 
            $getPassword = api_url().'userapi/getoldPassword/format/json/';
            $response['getPassword'] = curlpost($params,$getPassword);
            //p($response['getPassword']); exit;
            echo json_encode($response['getPassword']);
    }
}


function changePassword(){
   $userdata = $this->session->userdata('bizzgainmember');
    $userLoginId= $userdata->id;
    
    $this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
    $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|xss_clean');

    if($this->form_validation->run()== true){
           $params = array('userid'=>$userLoginId,'newpassword' => $this->input->post('new_password') );
        
           $changepass_path = api_url().'userapi/passwordChange/format/json/';
           $response['changepassword']= curlpost($params,$changepass_path);
          
          if(!empty($response['changepassword'])){
              $data['success'] = "success";
              echo json_encode($data);
           }else{
              $data['fail'] ="something went wrong";
              echo json_encode($data);
           }          
        
    }
}
public function change_order_stats()
{  
    $params=array('rowid'=>$_POST['statsid']);
    $getting_path = api_url().'orderapi/get_order_dtls/format/json/';
    $data= curlpost($params,$getting_path);

    $unit_price = get_unit_price($data->ProId, $_POST['cur_qty'], $this->session->userdata('logincity')->cmid);
    
    if($_POST['cur_qty']==$_POST['old_qty'])
    {
      $paramss=array(
                      'act_mode'=>'update_all_qty',
                      'rowid'=>$_POST['statsid'],
                      'promapid'=>$data->OrderproId,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>$_POST['old_stat'],
                      'orderproid'=>$data->ProId,
                      'order_id'=>$data->OrderId,
                      'order_venid'=>$data->VendorID,
                      'order_qty'=>$_POST['cur_qty'],
                      'comnt'=>$_POST['comment'],
                      'old_qty'=>$_POST['old_qty'],
                      'price'=>$data->base_prize

                    );
      
     $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
     $response['or']= curlpost($paramss,$getting_pat);
    }else{
        
            $paramss=array(
                          'act_mode'=>'update_less_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>$_POST['comment'],
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>$data->base_prize

                        );
           $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           $response['or']= curlpost($paramss,$getting_pat);

           $paramss=array(
                          'act_mode'=>'update_remaining_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>$_POST['comment'],
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>get_unit_price($data->ProId, ($_POST['old_qty'] - $_POST['cur_qty']), $this->session->userdata('logincity')->cmid)

                        );
           $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           $response['or']= curlpost($paramss,$getting_pat);

    }

    $msgord='Order number ORD'.$data->OrderId.' is '.$_POST['flag'].' successfully.';

    $this->session->set_flashdata('messageords', $msgord);
}

public function checkbulkmail(){

  $hmtopmail="support@mindzshop.com";
    $msg ='<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <div style="float: left; padding: 2px 2px; background: #fff; border: solid 1px #F3F0F0;">
        <div style="padding: 10px 10px; float: left; width: 600px; font-family: sans-serif; font-size: 12px; line-height: 18px; background: #F5F5F5;">
            <div style="float:left;text-align: center;width:100%;">
                <img src="'.base_url().'images/logo3.png"/>
            </div>
            <div style="float: left; width: 100%; text-align: center; padding: 10px 0px; font-size: 16px; ">Welcome to Bizzgain. padharo mhare desh.
</div>
            <div style="float:left;width:100%;font-weight:bold;padding:15px 0px 5px;">
                Regards,<br />
                MindzShop Team
            </div>
        </div>
        <div></div>
    </div>

</body>
</html>
';
         $this->load->library('email');
         $this->email->from($hmtopmail, 'BIZZGAIN');
         $this->email->to('shibbu@mindztechnology.com'); 
         $this->email->subject('WELCOME BIZZGAIN');
         $this->email->message($msg);
         if($this->email->send()){
          echo "Mail send";
        }
        else {
          echo "Mail not send. Please try again!";
        }  
}

public function repeatorder(){

  //---------------------------create cart data in session start---------------------------
  $reordid=$this->input->post('ordid');
  $getcartdetaildata = curlget(api_url().'productlisting/repeatorder/reordid/'.$reordid.'/format/json/');
//p($getcartdetaildata);exit;
        foreach ($getcartdetaildata as $key => $valuecart) {
          
            $param=array('userid'  =>$this->session->userdata('bizzgainmember')->id,'prodmap_id'=>$valuecart['ProId'],'qty'=>$valuecart['OrdQty'],
            'size'=>$valuecart['OrdSize'],'color'=>$valuecart['OrdColor']);
            $path = api_url().'productlisting/add_catdetails/format/json/';
            $dataqty=curlpost($param,$path);
        }
         $responce = curlget(api_url().'productlisting/loadcartdtls/userid/'.$this->session->userdata('bizzgainmember')->id.'/format/json/');
         //p($responce);
         if($responce->scalar!='Something Went Wrong' && !empty($responce) && isset($responce))
         { 
            foreach ($responce as $value) {
                            
             $Productid = $value['promapid'];
             $qtyvl = $value['qty'];  
             $parameter = array('prodid'=>$Productid);
             $newfinalPrice = '';
            // now calling the api to check the product avalilibility
             $p_cityid=$this->session->userdata('logincity')->cmid;
             $path = api_url().'productlisting/proAvailCheck/name/'.$Productid.'/cityidd/'.$p_cityid.'/format/json/';
             $response = curlget( $path);
                          
             $newqty   =explode('to', $response->qty1);
             $newqty2   =explode('to', $response->qty2);
             $newqty3   =explode('to', $response->qty3);

            if($qtyvl<$newqty[0] ){
              $newfinalPrice = '';
              $newfinalPrice=citybaseprice($response->FinalPrice,$response->cityvalue);
            } 
            if($qtyvl>=$newqty[0] && $qtyvl<=$newqty[1] ){
              $newfinalPrice = '';
              $newfinalPrice=cityqtyprice($response->FinalPrice,$response->cityvalue,$response->price);
            }
            if($qtyvl>=$newqty2[0] && $qtyvl<=$newqty2[1] ){
              $newfinalPrice = '';
              $newfinalPrice=cityqtyprice($response->FinalPrice,$response->cityvalue,$response->price2);
            }

            if($qtyvl>=$newqty3[0] && $qtyvl<='100000' ){
              $newfinalPrice = '';
              $newfinalPrice=cityqtyprice($response->FinalPrice,$response->cityvalue,$response->price3);
            }
              $coloridd = $value['color'];
              
            if(!empty($value['size'])){
              $sizeattr = $value['size'];
            }
            else{
              $sizeattr = NULL;
            }
              $getImg = getImgProdetail($response->image);

              $cart_dtls= array(
                             'id'      => $response->promapid,
                             'qty'     => $qtyvl,
                             'price'   => $newfinalPrice,
                             'name'    => $response->ProName,
                             'VendorID' => $response->manufacturerid,
                             'vendorName'=>$response->CompanyName,
                             'addiscount'=>$response->AdditionDiscount,
                             'options' => array('sizeattr' =>$sizeattr,'color'=>$coloridd),
                             'img'     => proImg_url($getImg['0']),
                             'brandid'  =>$response->brandId,
                             'catid'    =>$response->catid,
                             'prodiscount'=>$response->ProDiscount,
                             'proid'    => $response->proid,
                             'product_type' => 'single',
                             'proqty1'      =>$response->qty1,
                             'proqty2'      =>$response->qty2,
                             'proqty3'      =>$response->qty3,
                             'price1'       =>$response->price,
                             'price2'       =>$response->price2,
                             'price3'       =>$response->price3,
                             'sku'          =>$response->sellersku,
                             'cityval'      =>$response->cityvalue,
                             'baseprice'    =>$response->FinalPrice,
                             'mrpprice'     =>$response->productMRP

                   );
              $this->cart->insert($cart_dtls);
            }
            
         }

         echo "success";
         //redirect(base_url().'cart');
 //-----create cart data in session start---------------------------          


}

public function track_order(){
   
   if($this->session->userdata('bizzgainmember')->id=='')
   {
      redirect(base_url());
   }

    $idds=base64_decode($this->uri->segment(2));
    $data['main']=explode('_', $idds);

    $orderpath= api_url().'userapi/trackorderdetail/orderid/'.$data['main'][0].'/format/json/';
    $data['order_detail']= curlget($orderpath);

    $orderpath= api_url().'userapi/trackorderuniqueid/orderid/'.$data['main'][0].'/format/json/';
    $data['uniqueid']= curlget($orderpath);
    
    $this->load->view("helper/header");
    $this->load->view("trackorder",$data);
    $this->load->view("helper/footer");

  }

public function test_order(){
  //------------email--------------
          $hmtopemail="support@mindzshop.com";
          $userinfo['userdet']=array('useremail'=>$emailid,'userfname'=>$fname,'userlname'=>$lname,'userpass'=>$pass);
          //$msg = $this->load->view('bizzgainmailer/registermail',$userinfo, true);
          $this->load->view('bizzgainmailer/registermail',$userinfo);
          /*$this->load->library('email');
          $this->email->from($hmtopemail, 'MindzShop.com');
          $this->email->to($emailid); 
          $this->email->subject('Your Registration Details - ('.$emailid.')');
          $this->email->message($msg);*/
          //$this->email->send();
          //------------email--------------
}          
    
}//end of class
?>