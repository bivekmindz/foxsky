<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notification extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
  }

public function cartnotify(){
  $param = array('act_mode'=>'get_cartusers','p_rolename'=>'','Param3'=>'','Param4'=>'','Param5'=>'','Param6'=>'');
  $cartuserdata = $this->supper_admin->call_procedure('proc_addrole',$param);
  if(!empty($cartuserdata)){
  foreach ($cartuserdata as $key => $value) {
    $record['cartuser']=$value;
    //------------email--------------
        $hmtopemail="support@mindzshop.com";
        $emailid=$value->compemailid;
        $msg = $this->load->view('bizzgainmailer/cartnotify',$record, true);
        $this->load->library('email');
        $this->email->from($hmtopemail, 'MindzShop.com');
        $this->email->to($emailid); 
        $this->email->subject('Cart Notification');
        $this->email->message($msg);
        
        if($this->email->send()){
          $params=array('act_mode'=>'update_cartsend','p_rolename'=>'','Param3'=>'','Param4'=>'','Param5'=>$value->userid,'Param6'=>'');
          $cartsendupdate = $this->supper_admin->call_procedure('proc_addrole',$params);
          echo "send";
        } else {
          echo "not send";
        }
        
    //------------email--------------
  }
  }
}

}
?>