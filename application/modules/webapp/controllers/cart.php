
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cart extends MX_Controller {
  public function __construct() {
      $this->load->model("supper_admin");
      $this->load->helper("my_helper");
  }

  public function index(){
   
    $data['cart'] = $this->cart->contents();
   
    $this->load->view("helper/head");
    $this->load->view("helper/header");
    $this->load->view('productdetail/cart',$data);
    $this->load->view("helper/footer");

  }

public function checkproavailty(){
//p($_POST); exit('hello');
if(isset($_POST)){
 $this->form_validation->set_rules('proId', 'ProductId', 'trim|required|xss_clean');
  if($this->form_validation->run() == true){
   $Productid = $_POST['proId']; 
   $parameter = array('prodid'=>$Productid);
   $newfinalPrice = '';
// now calling the api to check the product avalilibility
   $p_cityid=$this->session->userdata('logincity')->cmid;
   $path = api_url().'productlisting/proAvailCheck/name/'.$Productid.'/cityidd/'.$p_cityid.'/format/json/';
   $response = curlget( $path);
  
   
   $newqty   =explode('to', $response->qty1);
   $newqty2   =explode('to', $response->qty2);
   $newqty3   =explode('to', $response->qty3);

 if($_POST['qtyval']<$newqty[0] ){
    $newfinalPrice = '';
    $newfinalPrice=citybaseprice($response->FinalPrice,$response->cityvalue);
} 
 if($_POST['qtyval']>=$newqty[0] && $_POST['qtyval']<=$newqty[1] ){
    $newfinalPrice = '';
    $newfinalPrice=cityqtyprice($response->FinalPrice,$response->cityvalue,$response->price);
}
 if($_POST['qtyval']>=$newqty2[0] && $_POST['qtyval']<=$newqty2[1] ){
    $newfinalPrice = '';
    $newfinalPrice=cityqtyprice($response->FinalPrice,$response->cityvalue,$response->price2);
}

 if($_POST['qtyval']>=$newqty3[0] && $_POST['qtyval']<='100000' ){
    $newfinalPrice = '';
    $newfinalPrice=cityqtyprice($response->FinalPrice,$response->cityvalue,$response->price3);
}
    $coloridd = $_POST['coloridd'];
 //if($response->quantityLeft >0 ){
    $data['addtocartno'] = 'success';
// take it to the cart controller
 if(!empty($_POST['sizeattr'])){
    $sizeattr = $_POST['sizeattr'];
 }
 else{
    $sizeattr = NULL;
  }
    $getImg = getImgProdetail($response->image);

    $data['cart'] = array(
                   'id'      => $response->promapid,
                   'qty'     => $_POST['qtyval'],
                   'price'   => $newfinalPrice,
                   'name'    => $response->ProName,
                   'VendorID' => $response->manufacturerid,
                   'vendorName'=>$response->CompanyName,
                   'addiscount'=>$response->AdditionDiscount,
                   'options' => array('sizeattr' =>$sizeattr,'color'=>$coloridd),
                   'img'     => proImg_url($getImg['0']),
                   'brandid'  =>$response->brandId,
                   'catid'    =>$response->catid,
                   'prodiscount'=>$response->ProDiscount,
                   'proid'    => $response->proid,
                   'product_type' => 'single',
                   'proqty1'      =>$response->qty1,
                   'proqty2'      =>$response->qty2,
                   'proqty3'      =>$response->qty3,
                   'price1'       =>$response->price,
                   'price2'       =>$response->price2,
                   'price3'       =>$response->price3,
                   'sku'          =>$response->sellersku,
                   'cityval'      =>$response->cityvalue,
                   'baseprice'    =>$response->FinalPrice,
                   'mrpprice'     =>$response->productMRP,
                   'proqty'       =>$response->proqty

         );
    $this->cart->insert($data);

#===========================================================

if(empty($this->session->userdata('bizzgainmember')))
{
    if(empty($this->session->userdata('guest_cart')))
    {  
        $cartno=time().rand(100000,999999);
        $this->session->set_userdata('guest_cart',$cartno);
        $guest_uniquekey=$this->session->userdata('guest_cart');
    }
    else
    { 
        $guest_uniquekey=$this->session->userdata('guest_cart');
        //$this->session->unset_userdata('guest_cart');
    }
}
else
{
    $guest_uniquekey='';
    $this->session->unset_userdata('guest_cart');
}

#===========================================================
    $param=array('userid'  =>$this->session->userdata('bizzgainmember')->id,'prodmap_id'=>$Productid,'qty'=>$_POST['qtyval'],
    'size'=>$sizeattr,'color'=>$coloridd,'guest_uniquekey'=>$guest_uniquekey);
    $path = api_url().'productlisting/add_catdetails/format/json/';
    $dataqty=curlpost($param,$path);
    $data['total']       = $this->cart->total();
    $data['TotalInCart'] =  count($this->cart->contents()); 
    $data['contentss'] = $this->cart->contents();
    if(empty($this->session->userdata['toal_cart_disc'])) 
    {
          $newdata = array('total_cart_discount' => 0, 'dilivery_charge' => 0, 'coupanid' => 0);
          //$this->session->set_userdata('toal_cart_disc', $newdata);
    }
     echo json_encode($data); 
// take it to the cart controller
   /*}
   else{
      $data['error']="Product Sold";
      }*/
      // now calling the prod to check the product avalibility

  }
  else{
      $data['error'] = "refress";
    }

}else{
  echo "Hello You are Doing it Wrong!!!!";
}

} 


  public function addtocart($datad){
          $this->cart->insert($datad);
          
  } 

  public function cartdisplay(){
          $param =  $this->cart->contents();
          p($param);
  }

  public function deletetocart(){
    $cartid= $this->input->get('cart_id');
    $data = array(
      'rowid' => $cartid,
      'qty'   => 0
    );
    $this->cart->update($data);
    $p     =$this->cart->total();
    $param =  $this->cart->contents(); 
    $q     = count($param);
    echo $p.'  ('.$q.')'; 
    $resdata=$this->cart->contents();
    exit();
  }


  public function deletedocart(){
    $arrayval = $_POST['getarrayid'];
    $data = array(
             'rowid' => $arrayval,
             'qty'   => 0
            );
    $x =  $this->cart->update($data);
    echo json_encode($x);
  }

  public function updatecart(){
    $pid = $this->input->post('productId');
    $parameter = array('act_mode' => 'get_product_details',
                          'row_id' => $pid,
                          'catid'  => '');
    $response = $this->supper_admin->call_procedureRow('proc_product',$parameter);
    // pend($response);
    $data = array('rowid' => $this->input->post('arrayid'),
                  'qty'   => $this->input->post('qnty'),
                  'price'   => $response->productMRP);
    // p($data);exit;
    $this->cart->update($data);
    // echo json_encode($data);
    // die('/');
    $data['total']       = $this->cart->total();
    $data['TotalInCart'] =  count($this->cart->contents()); 
    $data['contentss'] = $this->cart->contents();
    echo json_encode($data);
  }

  public function deletecart(){
    $arrayid = $this->input->post('arrayid');
    $data = array('rowid' => $arrayid,
                  'qty'   => 0,
                  'price' => 0);
    
    $this->cart->update($data);

    $data['total']       = $this->cart->total();
    $data['TotalInCart'] =  count($this->cart->contents()); 
    $data['contentss'] = $this->cart->contents();
    echo json_encode($data);
  }

  public function orderinformation(){

     $memberdata = $this->session->userdata('foxsky');
$data['total'] = $this->cart->total();

$this->session->unset_userdata('applied_coupon');
$this->session->unset_userdata('allow_discount');
$this->session->unset_userdata('discount_amount');
$this->session->unset_userdata('discount_coupon');
$this->session->unset_userdata('discount_id');

     if(empty($memberdata->id)){
      ?>
      <script type="text/javascript">
          window.location = "login"
      </script>
      <?php
      } if($data['total']<=0){ 
      ?>
     <script type="text/javascript">
          window.location = "cart"
      </script>
      <?php
    }
       if($this->input->post('submit')){
     
        $shipname = $this->input->post('shipname');
        $shippincode = $this->input->post('shippincode');
        $shipaddress = $this->input->post('address');
        $landmark = $this->input->post('landmark');
        $shipcity = $this->input->post('shipcity');
        $shipstate = $this->input->post('stateid');
        $shipemailid = $this->input->post('shipemailid');
        $shipcontact = $this->input->post('shipcontact');

        $billname = $shipname;
        $billpincode = $shippincode;
        $billaddress = $shipaddress;
        $billcity = $shipcity;
        $billstate = $shipstate;
        $billemailid = $shipemailid;
        $billcontactno = $shipcontact;

       $parameter = array('act_mode' => 'add_shipping_address',
                        'row_id' => $memberdata->id,
                        'Param1' => $shipname,
                        'Param2'  => $billname,
                        'Param3'  => $shipaddress,
                        'Param4'  => $billaddress,
                        'Param5'  => $shipemailid,
                        'Param6'  => $billemailid,
                        'Param7'  => $shippincode,
                        'Param8'  => $billpincode,
                        'Param9'  => $shipcontact,
                        'Param10'  => $billcontactno,
                        'Param11'  => $shipstate,
                        'Param12'  => $billstate,
                        'Param13'  => $shipcity,
                        'Param14'  => $billcity,
                        'Param15'  => $landmark);
        $record = $this->supper_admin->call_procedure('proc_shipping_address',$parameter);
      //  pend($record);
        if($record[0]->id){
          $data['message'] = "Shipping Address added successfully.";
        }
      }
      $parameter = array('act_mode' => 'get_state',
                          'row_id' => '',
                          'catid'  => '');
      $data['state'] = $this->supper_admin->call_procedure('proc_product',$parameter);
      
      $parameter = array('act_mode' => 'get_shipping_address',
                        'row_id' => $memberdata->id,
                        'Param1' => '',
                        'Param2'  => '',
                        'Param3'  => '',
                        'Param4'  => '',
                        'Param5'  => '',
                        'Param6'  => '',
                        'Param7'  => '',
                        'Param8'  => '',
                        'Param9'  => '',
                        'Param10'  => '',
                        'Param11'  => '',
                        'Param12'  => '',
                        'Param13'  => '',
                        'Param14'  => '',
                        'Param15'  => '');
      $data['record'] = $this->supper_admin->call_procedure('proc_shipping_address',$parameter);
      $data['total'] = $this->cart->total();
      
      $this->load->view("helper/head");
      $this->load->view("helper/header");
      $this->load->view('productdetail/orderinformation',$data);
      $this->load->view("helper/footer");
  }

//----------------------------------------- end cart combo -------------------------------------

  public function checkout(){
     $this->load->view('checkout');
  }

  public function getshipdetails(){
    $ship_id = $this->input->post('ship_id');
    $parameter = array('act_mode' => 'get_shipping_details',
                        'row_id' => $ship_id,
                        'Param1' => '',
                        'Param2'  => '',
                        'Param3'  => '',
                        'Param4'  => '',
                        'Param5'  => '',
                        'Param6'  => '',
                        'Param7'  => '',
                        'Param8'  => '',
                        'Param9'  => '',
                        'Param10'  => '',
                        'Param11'  => '',
                        'Param12'  => '',
                        'Param13'  => '',
                        'Param14'  => '',
                        'Param15'  => '');
    $record = $this->supper_admin->call_procedureRow('proc_shipping_address',$parameter);

    $parameter = array('act_mode' => 'get_state',
                          'row_id' => '',
                          'catid'  => '');
    $data['state'] = $this->supper_admin->call_procedure('proc_product',$parameter);

    $html = '';
    $html .= '<option value="">State</option>';
    foreach($data['state'] as $key => $val){
      $selected = '';
      if($val->stateid == $record->shipstate){
          $selected = 'selected';
      }
      $html .= '<option '.$selected.' value='.$val->stateid.'>'.$val->statename.'</option>';
    }

    $result  = array();

    $result['shipname'] = $record->shipname;
    $result['shipaddress'] = $record->shipaddress;
    $result['shipemailid'] = $record->shipemailid;
    $result['shippincode'] = $record->shippincode;
    $result['shippingid'] = base64_encode($record->shippingid);

    $result['shipstate'] = $html;
    $result['shipcontact'] = $record->shipcontact;
    $result['shipcity'] = $record->shipcity;
    $result['landmark'] = $record->landmark;
    echo json_encode($result);
  }

  function updateshipaddre(){
    $shippingid = $this->input->post('shippingid');
    $shipname = $this->input->post('shipname');
    $shippincode = $this->input->post('shippincode');
    $shipaddress = $this->input->post('address');
    $landmark = $this->input->post('landmark');

    $shipcity = $this->input->post('shipcity');
    $shipstate = $this->input->post('stateid');
    $shipemailid = $this->input->post('shipemailid');
    $shipcontact = $this->input->post('shipcontact');

    $billname = $shipname;
    $billpincode = $shippincode;
    $billaddress = $shipaddress;
    $billcity = $shipcity;
    $billstate = $shipstate;
    $billemailid = $shipemailid;
    $billcontactno = $shipcontact;

    $parameter = array('act_mode' => 'update_shipping_address',
                        'row_id' => base64_decode($shippingid),
                        'Param1' => $shipname,
                        'Param2'  => $billname,
                        'Param3'  => $shipaddress,
                        'Param4'  => $billaddress,
                        'Param5'  => $shipemailid,
                        'Param6'  => $billemailid,
                        'Param7'  => $shippincode,
                        'Param8'  => $billpincode,
                        'Param9'  => $shipcontact,
                        'Param10'  => $billcontactno,
                        'Param11'  => $shipstate,
                        'Param12'  => $billstate,
                        'Param13'  => $shipcity,
                        'Param14'  => $billcity,
                        'Param15'  => $landmark);
    $record = $this->supper_admin->call_procedure('proc_shipping_address',$parameter);
    $message = "Shipping Address updated successfully.";
    $this->session->set_flashdata($message);
    ?>
      <script type="text/javascript">
          window.location = "orderinformation"
      </script>
   <?php }

  function getpassword(){
    echo hash("md5",md5(111));
  }

function deletecartitem()
  {
    curlget(api_url().'productlisting/del_catdetails/userid/'.$this->session->userdata('bizzgainmember')->id.'/prodmap_id/'.$_POST['proid'].'/format/json/');
  }


function validate_coupon2(){ 

    $user_type = $this->session->userdata('foxsky')->vendortype;
    $user_id = $this->session->userdata('foxsky')->id;
    $cpn_code = $_POST['cpn_code'];
        $cartdata = array();
    $cart_contents = $this->cart->contents();
 
    $param = array ('user_type' => $user_type, 'coupon' => $cpn_code);
    $discount_request = api_url().'checkoutapi/discount_detail/format/json/';
    $d_wise = curlpost($param,$discount_request);
    // pend($d_wise->discount_data['id']);
    $amt_qualify_discount = NULL;
    $amt_qualify_msg = "<span style='color:brown;'>Product List: </span>";
    $final_discount_amount = 0.0;
    $cartdata['msg'] = "Coupon Discount is not applicable!"; // default message
    //$cartdata['Test'] = $coupon_detail; //testing code 
    $coupon_detail = array();
    $cpn_match_flag = 0;
//pend($this->cart->total());
    if($this->cart->total() < 10)
    {
        $cartdata['msg'] = "Subtotal amount must have more than 10 before coupon applied";
        $cartdata['allow_discount'] = "false";
        //$cartdata['discount_amount'] = $cartdata['cpn_discount'];
        $this->session->set_userdata('applied_coupon',false);
        $this->session->set_userdata('allow_discount', false);
        $this->session->set_userdata('discount_amount',false);

        $d_wise->discount_data = NULL;  // Not going to execute next if block
    }
   //pend($d_wise->discount_data);
    if($d_wise->discount_data)
    {
        
           $cpn_match_flag = 1;
           $coupon_detail = array(
              'discount_wise' => $d_wise->discount_data['discount_wise'],
              'product_id'    => $d_wise->discount_data['product_id'] ,
              'category_id'    => $d_wise->discount_data['category_id'],
              'brand_id'    => $d_wise->discount_data['brand_id'],
              'coupon'    => $d_wise->discount_data['coupon'],
              'reusable_limit'    => $d_wise->discount_data['reusable_limit'],
              'purchase_amount' => $d_wise->discount_data['purchase_amount'],
              'purchase_amount_max' => $d_wise->discount_data['purchase_amount_max'],
              'discount_value' => $d_wise->discount_data['discount_value'],
              'fixed_discount_price' => $d_wise->discount_data['fixed_discount_price'],
              'discount_type' => $d_wise->discount_data['discount_type'],
              'parent_id'    => $d_wise->discount_data['parent_id']
              );
              $cartdata['coupon_detail'] = $coupon_detail;
         
    }
    else
    {
    
         $avail_for ="";
         $avail_msg = "";
         if($user_type == "consumer")
         {
             $avail_for = "retail";
             $avail_msg = "Retailer";
         }
         else if($user_type == "retail")
         {
             $avail_for = "consumer";
             $avail_msg ="Consumer";
         }
         $param = array ('user_type' => $avail_for, 'coupon' => $cpn_code);
         $discount_request = api_url().'checkoutapi/discount_detail/format/json/';
         $d_wise = curlpost($param,$discount_request);
         if($d_wise->discount_data)
         {
              $cartdata['msg'] = "<span style='color:green;'>This offer is available for ".$avail_msg."</span>";
              $cartdata['allow_discount'] = "false";
              $this->session->set_userdata('discount_amount',"0.0");
              $this->session->set_userdata('applied_coupon',false);
              $this->session->set_userdata('allow_discount', false);
              $this->session->set_userdata('discount_amount',false);
              //echo json_encode($cartdata);
              //die;
             
         }
    }
    // pend($coupon_detail);
//pend($cpn_match_flag);
    if($cpn_match_flag)
    {
        
        $param = array ('UserId' => $user_id, 'CouponCode' => $cpn_code);
        $discount_request = api_url().'checkoutapi/discount_usage_detail/format/json/';
        $cpn_expenses = curlpost($param,$discount_request);
        //pend( $cpn_expenses);
        if(($cpn_expenses->result1['numrows'] + $cpn_expenses->result2['numrows']) <= $coupon_detail['reusable_limit'])
        {
          //   pend($coupon_detail['discount_wise']);
          
      //    echo $coupon_detail['discount_wise'];
           if($coupon_detail['discount_wise'] == 'cart')
             {
                  $subtotal_for_discount = 0.0;
                  foreach($cart_contents as $key => $value)
                  {
                    $subtotal_for_discount += $value['subtotal'];
                  }


//pend($coupon_detail['purchase_amount']);
//echo  $subtotal_for_discount."---".$coupon_detail['purchase_amount']; 

//&& ($subtotal_for_discount<$coupon_detail['purchase_amount_max'])
                  if(($subtotal_for_discount>=$coupon_detail['purchase_amount']) )
                  {
                 //   pend($coupon_detail['discount_type']);
                      if($coupon_detail['discount_type'] == 'fixed')
                      {
                          $final_discount_amount = $coupon_detail['discount_value'];
                          $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                          //$final_discount_amount = ($subtotal_for_discount/$coupon_detail['purchase_amount']) * $coupon_detail['discount_value'];
                        //  p($cartdata['subtotal_for_discount'] );
                      }
                      else if($coupon_detail['discount_type'] == 'percentage')
                      {
                        // $times = ($subtotal_for_discount/$coupon_detail['purchase_amount']);// * $coupon_detail['discount_value'];
                        //$cartdata['Test'] = $times; //testing code 
                        //$final_discount_amount = (($coupon_detail['purchase_amount'] * $coupon_detail['discount_value'])/100);
                        $final_discount_amount = (($subtotal_for_discount * $coupon_detail['discount_value'])/100);
                        $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                     //    p($cartdata['subtotal_for_discount'] );
                    }  
                    $final_discount_amount = number_format($final_discount_amount,1,".",""); 
                  } else if($subtotal_for_discount>$coupon_detail['purchase_amount_max']) {

                        $final_discount_amount = $coupon_detail['fixed_discount_price'];
                        $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                  } else {

                        $amt_qualify_discount = $coupon_detail['purchase_amount'] - $subtotal_for_discount;
                        //$cartdata['msg'] = "Add Rs.".number_format($amt_qualify_discount,1,".","")." more into Cart to avail discount";
                        $cartdata['msg'] = "This coupon code can be availed for subtotal cart amount between Rs. ".number_format($coupon_detail['purchase_amount'],1,".","")." and Rs. ".number_format($coupon_detail['purchase_amount_max'],1,".","")."";
                        $cartdata['allow_discount'] = "false";
                        $cartdata['cart_contents'] = $cart_contents;
                        $this->session->set_userdata('discount_amount',"0.0");
                        $this->session->set_userdata('applied_coupon',false);
                        $this->session->set_userdata('allow_discount', false);
                        $this->session->set_userdata('discount_amount',false);
                        //echo json_encode($cartdata);
                        //die; 
                  }
             

             }
             
          }
           else{
                    $cartdata['msg'] = "You reached maximum limit";
                    $cartdata['allow_discount'] = "false";
                    $cartdata['coupon_expenses'] = $cpn_expenses;
                    $this->session->set_userdata('applied_coupon',false);
                    $this->session->set_userdata('allow_discount', false);
                    $this->session->set_userdata('discount_amount',false);
                }
        } 
        else 
        {
            
            /*$cartdata['msg'] = "Coupon Discount is not applicable!";
            $cartdata['allow_discount'] = "false";
            $cartdata['coupon_expenses'] = $cpn_expenses;
            $this->session->set_userdata('applied_coupon',false);
            $this->session->set_userdata('allow_discount', false);
            $this->session->set_userdata('discount_amount',false); */
            
        }
       if($final_discount_amount <= 0)
       {
                $final_discount_amount = "0.0";
                //$cartdata['msg'] = "Coupon Discount is not applicable.";

                $cartdata['allow_discount'] = "false";
                $this->session->set_userdata('applied_coupon',false);
                $this->session->set_userdata('allow_discount', false);
                $this->session->set_userdata('discount_amount',false);
                
       }
       else
       {
             $cartdata['allow_discount'] = "true";
             $cartdata['msg']  = "Coupon has been applied!";
             $this->session->set_userdata('applied_coupon',true);
             $this->session->set_userdata('allow_discount', true);
             $this->session->set_userdata('discount_amount',$final_discount_amount);
             $this->session->set_userdata('discount_coupon',$cpn_code);
       }

              // print_r(json_encode(array('msg' => "Coupon has been applied", 'coupon_expenses' => $cpn_expenses, 'coupon_detail' => $coupon_detail)));
             //die;
            //-----------------------------
               
            //------------ Tax calculate --------------------
                $cstid=$this->session->userdata('bizzgainmember')->cstid;
                $tinid=$this->session->userdata('bizzgainmember')->tinid;
                $param =  $this->cart->contents();
                $total = count($param);
              foreach ($param as $key => $value) 
              {
                    $bid=$value['brandid'];
                    $cid=$value['catid'];
                    $subtotal=$value['subtotal'];
                    $stateid=$this->session->userdata('bizzgainmember')->stateid;
                    $taxpath = api_url().'checkoutapi/vattax/brandid/'.$bid.'/catid/'.$cid.'/stateid/'.$stateid.'/format/json/';

                $record['viewtex'] = curlget($taxpath); 

                foreach ($record['viewtex'] as $key => $valtax) 
                {  
                    if($cstid== $valtax['taxidd']) //2,4 CST
                    { 
                       $cst_name=$valtax['taxname'];
                       $cst_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                       $cpt[$value['proid']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                       $sub+=number_format($subtotal,1,".","");
                    }
                    if($tinid== $valtax['taxidd']) //1,6 TIN
                    { 
                       $tin_name=$valtax['taxname'];
                       $tin_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                       $tpt[$value['proid']]=number_format(($subtotal)*($valtax['taxprice'])/100.1,".","");

                    }
            
                  if($valtax['taxidd']==5)  //entry
                  { 
                       $entry_name=$valtax['taxname'];
                       $entry_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                       $ept[$value['proid']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                  }
                }  // end foreach
                  //p($record['viewtex']);

                $totaltaxprice = 0;
                 if($value['prostatus'] == 'Yes' || $value['prostatus'] == '') {
                          $totaltaxprice =$totaltaxprice + ($value['price']*$value['qty'] * ($value['ptax'] / 100));

                    }else{
                         $totaltaxprice = $totaltaxprice + 0;
                     }
                     $totaltaxprices += $totaltaxprice;
              }
   
               $tax['cstprowisetax']=$cpt;
               $tax['tinprowisetax']=$tpt;
               $tax['entryprowisetax']=$ept;

                $grandtotal=$cst_value+$tin_value+$entry_value+$sub;
                //echo $cst_name.'**'.$cst_value.'##'.$tin_name.'**'.$tin_value.'##'.$entry_name.'**'.$entry_value.'##'.$sub;
 
                $tax['cart_tax'] = array(
                     'tax_cst'  => 0,
                     'tax_tin'  => 0,
                     'tax_entry'  =>0,
                      //'tax_total'  => $cst_value+$tin_value+$entry_value,
                     'tax_total'  => number_format($totaltaxprices,1,".",""),
                     'shippcharge' => tshippingcharge(0)
                  );
  
                //$this->session->set_userdata($tax);
                $this->session->set_userdata('discount_amount',$final_discount_amount);
                 $this->session->set_userdata('discount_id',$d_wise->discount_data['id']);
             
             
                
              //------------ Tax calculate --------------------
                  $cartdata['grandTotal']  =  number_format($this->cart->total(),1,".",""); 
                 // $cartdata['shippcharge'] = number_format(tshippingcharge($this->cart->total()),1,".","");
                  $cartdata['shippcharge'] = $shipcharge->pin_shipcharge;
                  $cartdata['cpn_discount'] =  $final_discount_amount;
                  //$cartdata['grandTotalwithdilabery'] = number_format(Totalvalincart($cartdata['grandTotal'],$cartdata['shippcharge']),1,".","");  
                  $cartdata['grandTotalwithdilabery'] = Totalvalincart($this->cart->total(),$cartdata['shippcharge'],$_POST['cpn_code'],$cartdata['cpn_discount']);
                  $cartdata['proqty'] = $proqty;
                  $cartdata['unitprice'] = number_format($newfinalPrice,1,".","");
                  $cartdata['totalAmtPerProduct'] = number_format($proqty*$cartdata['unitprice'],1,".",""); 
                  // p($cartdata['totalAmtPerProduct']);exit;
                  $cartdata['tax']=$tax['cart_tax'];
                  $cartdata['TotalInCart']   =  count($this->cart->contents()); 
                  $cartdata['cart_contents'] = $cart_contents;
                  //$cartdata['allow_discount'] = "true";
                  //$cartdata['msg']  = "Coupon has been applied!";
                  //$this->session->set_userdata('applied_coupon',true);

                    $cartdata['total']       =Totalvalincart($this->cart->total(),$cartdata['shippcharge'],$_POST['cpn_code'],$cartdata['cpn_discount']);
    /*$data['TotalInCart'] =  count($this->cart->contents()); */
    $cartdata['contentss'] = $cart_contents;


                  echo json_encode($cartdata);
                  die;
    } 




}
?>