<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search extends MX_Controller {
   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
  }

 public function advancesearch(){

        $myval = $this->input->post('thiavalue');
        if (isset($_POST)) {
            $this->form_validation->set_rules('thiavalue', 'The Search', 'trim|required|xss_clean');
            if ($this->form_validation->run() == true) {
                $len = strlen($this->input->post('thiavalue'));
                if ($len > 0) {
                    if (!empty($this->input->post('thiavalue'))) {
                        $parameter = array(
                            'getval' => $myval,
                            'act_mode' => 'product'
                        );

                        $myval     = trim($myval);
                        $myval=$this->data_modify($myval);
                      
                        $myval     = str_replace(' ', '*+', $myval);  //group=true&group.field=ProId&
                        $myval=$myval.'*';

      
                          //$myurl ='http://115.124.125.148:8983/solr/bizz/select?q='.$myval.'&facet=true&fl=cityid,BrandName,prodtmappid,ProductName,productMRP,FinalPrice,image,ProId&indent=true&start=0&rows=4&wt=json&q.op=AND';
                          
                          $myurl ='http://115.124.125.148:8983/solr/bizz/select?q='.$myval.'&facet=true&fl=cityvalue,cityid,BrandName,prodtmappid,ProductName,productMRP,FinalPrice,image,ProId&indent=true&start=0&rows=4&wt=json&q.op=AND';
//p($myurl);

                        $response = curlget($myurl);
                                          
                        $data['product'] =$response->response['docs'];//$response->grouped->ProId->groups; //$response; //$this->supper_admin->
                      //p($data['product']);
                        $data['q']=str_ireplace('*','', $response->responseHeader->params->q);
         
                    }
              //   p( $data);
 
                      $this->load->view('searchdesine', $data);
               
                }
            }
        } 
  }

  public function index(){ 


    if($this->session->userdata('bizzgainmember')->id==''){
      redirect(base_url());
    } else {
      $search = $this->input->get('token');
       //  $catId  = $this->input->get('catids');
      //  $sdata  = $this->input->get('sdata');
        $sqdata = $this->input->get('sqdata');

     if (!empty($sqdata)) {
                $search = $this->data_modify($sqdata);
            }
        $search=$this->data_modify($search);
        $search= str_replace(' ','*+',$search); 
        $search=$search.'*';
 
if ($this->input->get('catids') != '') {
             $catids    = $this->input->get('catids');
             $catids    =trim($catids);
             $catids    =$this->data_modify($catids);
             $catids    =  str_replace(',','%20OR%20', $catids);
             $catids    =  str_replace('_',' ', $catids);
             $catids    =  str_replace(' ','*', $catids);
             $catids    =  str_replace('+','*', $catids);
             //$cattid1=$catids ;
             $catids='fq=CatName:'.$catids.'&';
        } else {
            $catids ='';
        }

         if ($this->input->get('show_brand') != '') {
            $brand    = $this->input->get('show_brand');
            $brand= $this->data_modify($brand);
            $brand    =  str_replace(',','%20OR%20', $brand);
            $brand    =  str_replace(' ','*', $brand);
      
            $brand='fq=BrandName:'.$brand.'&';
            
        } else {
            $brand    ='';
            $brandurl = null;
        }


if($this->input->get('catids') != ''){
      $fq=$catids.$brand; // CatName:jeans%20OR%20Boy%20Jeans  
    }else{
     // $fq='&q.op=AND';
      $fq='';
    }

    $fq= rtrim($fq,'&');
    
     // $myurl='http://10.0.1.116:8983/solr/royzez/select?q='.$search.'&start=0&rows=0&wt=json&facet.field=CatName&facet.field=SizeVal&facet.field=Color_Val&facet.field=BrandName&facet.field=feature&facet.field=FinalPrice&indent=true&facet=true&group.field=ProId&q.op=AND&fq=proQnty:[1%20TO%20*]&'.$fq;
     //$myurl='http://103.245.118.222:8983/solr/royzez/select?q='.$search.'&start=0&rows=0&wt=json&facet.field=CatName&facet.field=SizeVal&facet.field=Color_Val&facet.field=BrandName&facet.field=feature&facet.field=FinalPrice&indent=true&facet=true&group.field=ProId&q.op=AND&fq=proQnty:[1%20TO%20*]&'.$fq;
      //$myurl='http://115.124.125.148:8983/solr/bizz/select?q='.$search.'&start=0&rows=0&wt=json&facet.field=CatId&facet.field=SizeVal&facet.field=Color_Val&facet.field=FDisId&facet.field=BrandName&facet.field=FinalPrice&indent=true&facet=true&group.field=ProId&q.op=AND&facet.mincount=1&'.$fq;
      $myurl='http://115.124.125.148:8983/solr/bizz/select?q='.$search.'&start=0&rows=0&wt=json&facet.field=CatId&facet.field=SizeVal&facet.field=Color_Val&facet.field=FDisId&facet.field=BrandName&facet.field=FinalPrice&indent=true&facet=true&q.op=AND&facet.mincount=1&'.$fq;
 //p($myurl);
    $response = curlget($myurl);
 //p($response);
 //die();

    $myurlmax='http://115.124.125.148:8983/solr/bizz/select?indent=on&q='.$search.'&wt=json&&sort=FinalPrice%20desc&start=0&rows=1&q.op=AND&fl=FinalPrice';
    $responsemax = curlget($myurlmax);
    //p($myurlmax);
//p($responsemax);
    $myurlmin='http://115.124.125.148:8983/solr/bizz/select?indent=on&q='.$search.'&wt=json&&sort=FinalPrice%20asc&start=0&rows=1&q.op=AND&fl=FinalPrice';
    $responsemin = curlget($myurlmin);
    //p($myurlmin);
//p($responsemin);

 //p($response->facet_counts['facet_fields']['SizeVal']);
    $cat=$response->facet_counts['facet_fields']['CatId'];
    
    $abc=array_chunk($cat, 2);
    $FinalPrice=$response->facet_counts['facet_fields']['FinalPrice'];
    //p($FinalPrice);
    $FinalPrice=array_chunk($FinalPrice, 2);
    $f_price = array();
   foreach ($FinalPrice as $key => $value) {
     if($value[1]<1){
      break;
           }
    array_push($f_price,$value[0]);
   }
/*
   $feature=$response->facet_counts->facet_fields->feature;
   $feature_l=array_chunk($feature, 2);*/

// $data['fildiscount']=$this->supper_admin->call_procedure('proc_FilterDiscount');

    $data['CatId']=$abc;  // array_chunk($response->facet_counts->facet_fields->CatName);
    //p($data['CatId']);
    foreach ($abc as $ke => $va) {
      if($va[1]>0){
        $allcat.=$va[0].',';
      }
    }
    $parameter=array('act_mode'=>'getcatname','row_id'=>'','catparentid'=>'','c_catname'=>trim($allcat,','));
    $data['CateName']=$this->supper_admin->call_procedure('proc_catname',$parameter);

    $parameterp=array('act_mode'=>'getparentcatname','row_id'=>'','catparentid'=>'','c_catname'=>trim($allcat,','));
    $data['parentCateName']=$this->supper_admin->call_procedure('proc_catname',$parameterp);

    $data['CatName_pro']= $this->filterdata_cat_pro_count($response->facet_counts['facet_fields']['CatName']);
    $data['BrandName']= array_chunk($response->facet_counts['facet_fields']['BrandName'],2); //$this->filterdata($response->facet_counts->facet_fields->BrandName);
    $data['Color_Val']= array_chunk($response->facet_counts['facet_fields']['Color_Val'],2);
    $data['SizeVal']= array_chunk($response->facet_counts['facet_fields']['SizeVal'],2);
    $data['fildiscount']= $this->discount_filter(array_chunk($response->facet_counts['facet_fields']['FDisId'],2));
    $data['resultspage']=$response->response->docs;
    $data['f_price']=$f_price;
    $data['maxf_price']=$responsemax->response['docs'][0]['FinalPrice'];
    $data['minf_price']=$responsemin->response['docs'][0]['FinalPrice'];
    $data['s_data']=$cattid1;
    $parametercmid=array('act_mode'=>'getcitygroup','row_id'=>$this->session->userdata('logincity')->cmid,'catparentid'=>'','c_catname'=>'');
    $data['citygrouppercent']=$this->supper_admin->call_procedureRow('proc_catname',$parametercmid);
    
$this->load->view("helper/header");
$this->load->view("helper/nav1");
$this->load->view("helper/nav2");
$this->load->view("productlist/solr1",$data);
$this->load->view("helper/footer");
    
    } // end session check else

  }



public function  discount_filter($variable){

    $a=array();
    foreach ($variable as $val ) {
      array_push($a, $val[0]);
      sort($a);
    }

    $clength=count($a);
    for($x=0;$x<$clength;$x++)
    {
      $aa[]=$a[$x];
    }

    return $aa;
}


 public function ajaxsearch(){

$search = $_REQUEST['token'];

parse_str($_SERVER['REQUEST_URI'], $_REQUEST);

    $totallistoffset   = $_POST['offset'];
    if($totallistoffset==0){
      $totallistoffset = 0;
    }else{
      //$totallistoffset = $_POST['offset']+12;
      $totallistoffset = $_POST['offset'];
    }
   
        
        

        $search=$this->data_modify($search);
       

       // $sdata  = $this->input->get('sdata');
      //  $catId  = $this->input->get('catids');
       // $sqdata = $this->input->get('sqdata');
       // $sqdata =$this->data_modify($sqdata);
  /*    if (!empty($sqdata)) {
            $search = $this->data_modify($sqdata);
        }*/


        $search=$this->data_modify($search);
        $search= str_replace(' ','*+',$search); 
        $search=$search.'*';

    if ($_POST['size'] != '') {
            $size    =  $_POST['size'];
            $size    = str_replace(',','%20OR%20', $size);
           $size    =  str_replace(' ','*', $size);
            $sizeurl = 'fq=SizeVal:'.$size.'&';
            
        } else {
            $size    = 0;
            $sizeurl = null;
        }
        
        if ($_POST['color'] != '') {
            $color    =  $_POST['color'];
            $color    =  str_replace(',','%20OR%20', $color);
            
            $colorurl='fq=Color_Val:'.$color.'&';
        } else {
           
           $colorurl = null;
        }

      if ($_REQUEST['show_brand'] != '') {
            $brand    = $_REQUEST['show_brand'];
            $brand    =$this->data_modify($brand);
            $brand    =  str_replace(',','%20OR%20', $brand);
            $brand    =  str_replace(' ','*', $brand);
      
            $brand='fq=BrandName:'.$brand.'&';
            
        } else {
            $brand    ='';
            $brandurl = null;
        }


$citiid=1;
      if ($citiid != '') {
                        $citiid='fq=cityid:'.$citiid.'&';
            
        } else {
            $citiid    ='';
           // $citiid = null;
        }

//p($_REQUEST['discount']);die();
  if ($_REQUEST['discount'] != '') {
                $discount    = $_REQUEST['discount'];
                $discount=trim($discount);
                 $discount    =  str_replace(',','%20OR%20', $discount);
                $discount    =  str_replace('_','*', $discount);
                $discount    =  str_replace('+','*', $discount);
                $discount    =  str_replace(' ','*', $discount);
      
            $discount='fq=FDisId:'.$discount.'&';




               /* $discount    =  str_replace(',','%20OR%20',$discount);
                $discount    =  str_replace('_','*', $discount);
                $discount    =  str_replace('+','*', $discount);
                $discount    =  str_replace(' ','*', $discount);
*/
           // $discount='fq=FDisId:'.$discount.'&';
            
        } else {
            $discount    ='';
           
        }





           if ($_REQUEST['cat_name'] != '') {
                $catiddss=decrypt($_REQUEST['cat_name']);
                $catids=trim($catiddss);
                //p($catids);exit;
                //$catids=trim($_REQUEST['cat_name']);
                //$catids=$this->data_modify($catids);
                $catids    =  str_replace(',','%20OR%20', $catids);
                $catids    =  str_replace('_','*', $catids);
                $catids    =  str_replace('+','*', $catids);
                $catids    =  str_replace(' ','*', $catids);

            $catids='fq=CatId:'.$catids.'&';
            
        } else {
            $catids    ='';
            $brandurl = null;
        }




if ($_REQUEST['price'] != '') {
            $price    = $_REQUEST['price'];
            $price    =  str_replace('-','%20TO%20', $price);
            $price='fq=FinalPrice:['.$price.']&';
            //fq=FinalPrice:[1%20TO%20*]
        } else {
            $price    ='';
            $brandurl = null;
        }

              if ($_REQUEST['porder'] != '') {
                          $porder    = $_REQUEST['porder'];

                   switch ($porder) {
                          case 1: /// high price  score desc
                           $porder_fl='&sort=FinalPrice%20desc&';
                          //fq=FinalPrice:[1 TO *]
                          //$porder_fl='fq=FinalPrice:[1%20TO%20*]&';  fq=proQnty:[1%20TO%20*]
                          break;
                          case 2: // low price
                              $porder_fl='&sort=FinalPrice%20asc&';
                          //$porder_fl='fq=FinalPrice:[9999%20TO%200]&'; 
                          //$porder_fl='&sort=FinalPrice desc&';
                          break;
                          case 3: // discount FDisId proDiscount
                          //$porder_fl='sort=DiscPrice%20desc&';
                          $porder_fl='sort=ProDiscount%20desc&';
                          break;

                          case 4:  // new product
                          $porder_fl='';
                          //  $catids='&fq=CatName:'.$catids.'&';
                          break;

                      }

              } else {
                          $porder_fl='';
                          
                     }
      
    if($_REQUEST['price'] != ''||$_REQUEST['color'] != '' ||$_REQUEST['size'] != ''||$_REQUEST['show_brand'] != '' ||$_REQUEST['catids'] != '' || $_REQUEST['porder'] != ''||$_REQUEST['feature'] != ''|| $_REQUEST['discount'] != '' || $citiid !='' ){
      $fq=$sizeurl.$colorurl.$brand.$catids.$porder_fl.$feature.$price.$discount.$citiid; // CatName:jeans%20OR%20Boy%20Jeans  
     }else{
      $fq='';
        }

    $fq= rtrim($fq,'&');
    $parameter =array('keyword'=>$fq,
      'category_id'=>$search,
      'brandid'=>'0',
      'size'=>'0',
      'colour'=>'0',
      'minprice'=>'0',
      'maxprice'=>'0',
      'maxdis'=>'0',
      'porder'=>'0',
      'pid'=>$totallistoffset,
      'perpage'=>$_POST['number'],
      'p_Feature'=>0);

   /* p($parameter);
  die('s');*/

     // $myurl='http://10.0.1.116:8983/solr/royzez/select?q='.$search.'&start='.$totallistoffset.'&rows=12&wt=json&q.op=AND&indent=true&group=true&group.field=ProId&fq=proQnty:[1%20TO%20*]&'.$fq;
      //$myurl='http://103.245.118.222:8983/solr/royzez/select?q='.$search.'&start='.$totallistoffset.'&rows=12&wt=json&q.op=AND&indent=true&group=true&group.field=ProId&fq=proQnty:[1%20TO%20*]&'.$fq;
  //$myurl='http://115.124.125.148:8983/solr/bizz/select?q='.$search.'&start='.$totallistoffset.'&rows=12&wt=json&q.op=AND&indent=true&fq=proQnty:[0%20TO%20*]&'.$fq;
     $myurl='http://115.124.125.148:8983/solr/bizz/select?q='.$search.'&start='.$totallistoffset.'&rows=12&wt=json&q.op=AND&indent=true&'.$fq;

//echo $myurl;
     
      $response = curlget($myurl);
  //p($response);
  

      $data['resultspage'] =$response->response['docs'];
      

       $data['image_view']=$_POST['seetyp'];

      //$data['resultspage']=$response->response->docs;
      //p($data['resultspage']);
      $this->load->view('solrlist2',$data);

    }
public function filterdata($data){
                $cat_list= array();
                foreach ($data as $key => $value) {

                if((int)$key%2 == 0){
                 if($value !=1){
                array_push($cat_list, $value);
                }

                }


                if((int)$key%2 == 1){
                // ctype_digit($value) && 
                if(((int)$value==1 || (int)$value==0) ){
                break;
                }
               //  array_push($cat_list1, $value);
                }
              
                }
                array_pop($cat_list);
                return $cat_list;

 }

      public function filterdata_cat_pro_count($data){
                $cat_list = array();
                //$cat_list1=array();
                $i=0;
                foreach ($data as $key => $value) {
                if((int)$key%2 == 0){
                  $i=1;
                  $cat_list[$key]['name']=$value;
                array_push($cat_list, $value);
                }

                if((int)$key%2 == 1){
                // ctype_djigit($value) && 
                  $cat_list[$key-1]['pno']=$value;

                  
                // array_push($cat_list1, $value);
                if(((int)$value==1 || (int)$value==0) ){
                break;
                }
                
                }
                $i=$i++;
                }
                return $cat_list;

}





function feature_filter($feature_l){
         $ffl = array();
         $i=0;
  foreach ($feature_l as $key ) {
            $a= explode(' x99z ', $key[0]);
if($a[0]=='Area of Use'){
$z=$a[1];
            //$ffl[trim($a[0])][trim($z)]=trim($z);
$ffl[trim($a[0])][trim('Drawing room')]='Drawing room';
$ffl[trim($a[0])][trim('Living room')]='Living room';
$ffl[trim($a[0])][trim('Dining room')]='Dining room';
$ffl[trim($a[0])][trim('Bed Room')]='Bed Room';
$ffl[trim($a[0])][trim('Study Room')]='Study Room';
$ffl[trim($a[0])][trim('Kids Room')]='Kids Room';
$ffl[trim($a[0])][trim('Family Room')]='Family Room';
$ffl[trim($a[0])][trim('tair Way')]='tair Way';
}else{
  $z=$a[1];
            $ffl[trim($a[0])][trim($z)]=trim($z);
}
            $i++;
            if($key[1]==1 || $key[1]==0){
              break;
            }
  }

 return $ffl;
}



function re(){
  
   $text='yadav+RAHUL(from2';

    $array_from_to=array('+' => 'Z1',
                         '-' => 'Z2',
                         '&' => 'Z3',
                         '|' => 'Z5',
                         '!' => 'Z6',
                         '(' => 'Z7',
                         ')' => 'Z8',
                         '[' => 'Z9',
                         ']' => 'Zx1',
                         '^' => 'Zx2',
                         '"' => 'Zx3',
                         '*' => 'Zx4',
                         '~' => 'Zx5',
                         '?' => 'Zx6',
                         ':' => 'Zx7',
                         "'" => 'Zx8',
                         "<" => 'Zx9',
                         ">" => 'Zy1',
                         "=" => 'Zy2'
                                     );
                                     
  $text = strtr($text,$array_from_to);
   
   echo $text;

}



function manage_f_price($f_price){
$len=explode(',',$f_price);
$p = array();
              foreach ($len as $value ) {
                         switch ($value) {
                           case '1':
                           array_push($p,0);
                           array_push($p,99);
                             break;
                           
                              case '2':
                            
                           array_push($p,100);
                           array_push($p,199);

                             break;
                                case '3':
                            
                           array_push($p,200);
                           array_push($p,299);
                             break;
                                case '4':
                             
                           array_push($p,300);
                           array_push($p,499);
                             break;
                                case '5':
                             
                           array_push($p,500);
                           array_push($p,999);
                             break;
                                case '6':
                             
                           array_push($p,1000);
                           array_push($p,2499);
                             break;
                                case '7':
                             
                           array_push($p,2500);
                           array_push($p,10000);
                             break;
                         }



             }
 return  min($p).'|'.max($p);
}





function data_modify($search){ 
   
         $array_from_to=array('+' => 'Z1',
                         '-' => 'Z2',
                         '&' => 'Zy3',
                         '|' => 'Z5',
                         '!' => 'Z6',
                         '(' => 'Z7',
                         ')' => 'Z8',
                         '[' => 'Z9',
                         ']' => 'Zx1',
                         '^' => 'Zx2',
                         '"' => 'Zx3',
                         '*' => 'Zx4',
                         '~' => 'Zx5',
                         '?' => 'Zx6',
                         ':' => 'Zx7',
                         "'" => 'Zx8',
                         "<" => 'Zx9',
                         ">" => 'Zy1',
                         "=" => 'Zy2' );

 $search = strtr($search,$array_from_to);
 return $search;
}


public function abc(){
$this->load->library('solr');

$data=$this->solr->most_sugetion();


p($data);
echo 'ff';

}




public function data_onclick(){
 // 103.245.118.222:8983
//$purl='http://10.0.1.116:8983/solr/royzez/select?q=*%3A*&wt=json&indent=true&facet=true&sort=totalproductview%20desc&fq=proQnty:[1%20TO%20*]&group.field=CatId&group=true&start=0&rows=8&fl=CatName,CatId';
 // $purl='http://103.245.118.222:8983/solr/royzez/select?q=*%3A*&wt=json&indent=true&facet=true&sort=totalproductview%20desc&fq=proQnty:[1%20TO%20*]&group.field=CatId&group=true&start=0&rows=8&fl=CatName,CatId';
  $purl='http://52.74.154.27:8983/solr/liveroyzez/select?q=*%3A*&wt=json&indent=true&facet=true&sort=totalproductview%20desc&fq=proQnty:[1%20TO%20*]&group.field=CatId&group=true&start=0&rows=8&fl=CatName,CatId';


$response = curlget($purl);

$data=$response->grouped->CatId->groups;

$object = array();

foreach ($data as $key => $value) {

              $object[$key]->catid=$value->doclist->docs[0]->CatId;
              $object[$key]->CatName=$value->doclist->docs[0]->CatName[0];
}

//p($object);
echo  json_encode($object);
//return $object;
}
 
}
?>