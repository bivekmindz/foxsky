<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Checkout extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->model('rating_model');
    
  }

  public function index(){

  //p($this->session->userdata('logincity')->cmid);
    if($this->session->userdata('bizzgainmember')->id==''){
      redirect(base_url());
    }
    else{
    $userregid = $this->session->userdata('bizzgainmember')->id;
    $ctyidsss= $this->session->userdata('bizzgainmember')->city;

    $statepath = api_url().'checkoutapi/statedetail/format/json/';
    $record['viewstate'] = curlget($statepath);
    $citypath = api_url().'checkoutapi/citydetail/ctid/'.$ctyidsss.'/format/json/';
    $record['viewcityss'] = curlget($citypath);
    //p($record['viewcityss']);exit();
    $pathuser = api_url().'checkoutapi/getaddress/userId/'.$userregid.'/format/json/';
    $record['lastshipdetails'] = curlget($pathuser);

    $pathuserdet = api_url().'checkoutapi/loginuserdetail/userId/'.$userregid.'/format/json/';
    $record['loginuserdetails'] = curlget($pathuserdet);
//p($record['loginuserdetails']);exit();

   // p($this->cart->contents());
      //p($this->session->all_userdata());
  $cst_name;
  $cst_value;
  $tin_name;
  $tin_value;
  $entry_name;
  $entry_value;
  $sub;
  $str ='';

  $cstid=$this->session->userdata('bizzgainmember')->cstid;
  $tinid=$this->session->userdata('bizzgainmember')->tinid;
     $param =  $this->cart->contents();
      $total = count($param);
      foreach ($param as $key => $value) {
        //-------------------------  combo cart --------------------------
        if($value['product_type']=='combo'){

              $pathuser = api_url().'checkoutapi/getcombodetails/comboId/'.$value['id'].'/format/json/';
              $records['combodet'] = curlget($pathuser);

              $newqty    =explode('to', $value['proqty1']);
              $newqty2   =explode('to', $value['proqty2']);
              $newqty3   =explode('to', $value['proqty3']);

              foreach ($records['combodet'] as $key => $comboval) {

                if($value['qty']<$newqty[0] ){
                $newfinalPrice='';  
                $newfinalPrice=citybaseprice($comboval['pro_combo_price'],$value['cityval']);
              //p($newfinalPrice);
                }

                if($value['qty']>=$newqty[0] && $value['qty']<=$newqty[1] ){
                $newfinalPrice='';
               
                  
                $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price1']);
              //p("heloo1");  
              //p($newfinalPrice);//exit;
                }
                if($value['qty']>=$newqty2[0] && $value['qty']<=$newqty2[1] ){
                $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price2']);
              //p("hel2");  
              //p($newfinalPrice);//exit;
                }
                if($value['qty']>=$newqty3[0] && $value['qty']<='100000' ){
                $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price3']);
              //p("he3");  
              //p($newfinalPrice);//exit;
                }
                if($value['qty']==0){
                $newfinalPrice=0;
                } 

                $bid=$comboval['brand_id'];
                $cid=$comboval['sub_catid'];
                $subtotal=$newfinalPrice*$comboval['product_qty']*$value['qty'];
                //p($subtotal);exit;
              $stateid=$this->session->userdata('bizzgainmember')->stateid;
             //$taxpath = api_url().'checkoutapi/vattax/brandid/'.$bid.'/catid/'.str_replace(',', '~', $cid).'/stateid/'.$stateid.'/format/json/';
             $taxpath = api_url().'checkoutapi/vattax/brandid/'.$bid.'/catid/'.$cid.'/stateid/'.$stateid.'/format/json/';
          //p($taxpath);exit;
            $record['viewtex'] = curlget($taxpath); 
          //p($record['viewtex']);//exit;
          foreach ($record['viewtex'] as $key => $valtax) 
          { //p($valtax);//exit;
           // foreach ($record['lastshipdetails'] as $key => $val) 
           // { // get data CST/TIN no

              if($cstid== $valtax['taxidd']) //2,4 CST
              { 
              
                 $cst_name=$valtax['taxname'];
                 $cst_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                 $cpt[$comboval['product_id']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                 $cptpc[$comboval['product_id']]=$valtax['taxprice'];
                 $sub+=number_format($subtotal,1,".","");
              }
              if($tinid== $valtax['taxidd']) //1,6 TIN
              { 
                 $tin_name=$valtax['taxname'];
                 $tin_value+=number_format((($subtotal)*($valtax['taxprice'])/100),1,".","");
                 $tpt[$comboval['product_id']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                 //p($valtax['taxprice']);
                 //p($tpt);
                 $tptpc[$comboval['product_id']]=$valtax['taxprice'];
              }
            //} // end foreach
            //exit;
            if($valtax['taxidd']==5)  //entry
            { 
                 $entry_name=$valtax['taxname'];
                 $entry_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                 $ept[$comboval['product_id']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                 $eptpc[$comboval['product_id']]=$valtax['taxprice'];
            }
          }  // end foreach
        } // end combo foreach  
//----------------------------- end combo ----------------------------------
        } else {

        $bid=$value['brandid'];
        $cid=$value['catid'];
        $subtotal=$value['subtotal'];
      $stateid=$this->session->userdata('bizzgainmember')->stateid;
     $taxpath = api_url().'checkoutapi/vattax/brandid/'.$bid.'/catid/'.$cid.'/stateid/'.$stateid.'/format/json/';

    $record['viewtex'] = curlget($taxpath); 

  foreach ($record['viewtex'] as $key => $valtax) 
  { 
    

      if($cstid== $valtax['taxidd']) //2,4 CST
      { 
         $cst_name=$valtax['taxname'];
         $cst_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
         $cpt[$value['proid']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
         $cptpc[$value['proid']]=$valtax['taxprice'];
         $sub+=number_format($subtotal,1,".","");
      }
      if($tinid== $valtax['taxidd']) //1,6 TIN
      { 
         $tin_name=$valtax['taxname'];
         $tin_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
         $tpt[$value['proid']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
         $tptpc[$value['proid']]=$valtax['taxprice'];
      }
   
    if($valtax['taxidd']==5)  //entry
    { 
         $entry_name=$valtax['taxname'];
         $entry_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
         $ept[$value['proid']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
         $eptpc[$value['proid']]=$valtax['taxprice'];
    }
  }  // end foreach
 } // end product type if                         

    //p($record['viewtex']);
}
    $tax['cstprowisetax']=$cpt;
    $tax['tinprowisetax']=$tpt;
    $tax['entryprowisetax']=$ept;
    $tax['cstprowisepc']=$cptpc;
    $tax['tinprowisepc']=$tptpc;
    $tax['entryprowisepc']=$eptpc;
   $grandtotal=$cst_value+$tin_value+$entry_value+$sub;
 //echo $cst_name.'**'.$cst_value.'##'.$tin_name.'**'.$tin_value.'##'.$entry_name.'**'.$entry_value.'##'.$sub;

  $tax['cart_tax'] = array(
               'tax_cst'  => $cst_value,
               'tax_tin'  => $tin_value,
               'tax_entry'  => $entry_value,
               'tax_total'  => $cst_value+$tin_value+$entry_value,
               'shippcharge' => tshippingcharge(0)
            );
  
  $this->session->set_userdata($tax);

 $str .=$totaltax=$cst_value+$tin_value+$entry_value;
//$str .= '<li>'.$cst_name.'<span>Rs. '.$cst_value.'</span></li>';
//$str .= '<li>'.$tin_name.'<span>Rs. '.$tin_value.'</span></li>';
//$str .= '<li>'.$entry_name.'<span>Rs. '.$entry_value.'</span></li>';
//$str .= '<li>Tax<span>Rs. '.$totaltax.'</span></li>';
//$str .= '<li>Shipping charge <span>Rs. 00</span></li>';
//$str .= '<li>Grand Total<span>Rs. '.$grandtotal.'</span></li>';
 

  $record['str'] =  $str; //exit;
  $parameter4 = array('userid'=>$userregid);
  
  $data_c = $this->supper_admin->call_procedurerow('proc_cart_disc', $parameter4);
  $total =  $this->cart->total();          
 
  if($data_c->PurchaseAmt <= $total){
                    
                    if($data_c->DiscountType == 'percentage') {
                       $discountAmt = ($data_c->DiscountAmt)*($this->cart->total())/100;

                    }
                    else if($data_c->DiscountType == 'fixed') {
                        $discountAmt = $data_c->DiscountAmt;

                    }

                  
                  $cartData = array(
                            
                            'coupanid'        =>  $data_c->copid,
                            'coupondiscount'  =>  $discountAmt, 
                            'PurchaseAmt'     =>  $data_c->PurchaseAmt, 
                            'couponcode'      =>  $cuponcode, 
                            'applyonexitdis'  =>  $data_c->applyonexitdis,
                            'subtotal'        =>  $total 
                          
                          );

            $this->session->set_userdata('toal_cart_disc', $cartData);
      }         
    //------------------- start captcha code ------------------
    $captcha = $this->get_captcha();
    $captchacredit = $this->get_captchacredit();
   //p($this->session->all_userdata());//exit();

    //------------------- end captcha code ------------------
    //-------------------- start wallet -----------------------------------//
    $walletdrcrpath=api_url().'walletapi/drcrwallet/user_id/'.$userregid.'/format/json/'; 
    $record['walletdrcr']=curlget($walletdrcrpath);
    //-------------------- end wallet ----------------------------------------//
    
    
    
    $this->load->view("helper/header");
    /*$this->load->view("helper/nav1");
    $this->load->view("helper/nav2");*/
    $this->load->view("checkout",$record);
    $this->load->view("helper/footer");
  }
  }

  public function get_captcha_ajax(){
    
    unlink("captchaimg/".$this->session->userdata('cap')['time'].".jpg");
    $this->load->helper('captcha');
    $vals = array(
    'img_path'  => './captchaimg/',
    'img_url' => base_url().'captchaimg/',
    'img_width' => '180',
    'img_height' => '50',
    'expiration' => '7200'
    );
    $caprecord = create_captcha($vals);
    $this->session->unset_userdata('cap');
    $this->session->set_userdata('cap', $caprecord);
    print $caprecord['image'];
    
  }
  public function get_captcha(){

    $this->load->helper('captcha');
    $vals = array(
    'img_path'  => './captchaimg/',
    'img_url' => base_url().'captchaimg/',
    'img_width' => '180',
    'img_height' => '50',
    'expiration' => '7200'
    );
    $caprecord = create_captcha($vals);
    $this->session->set_userdata('cap', $caprecord);

  }

  public function get_captcha_ajaxcredit(){
    
    unlink("creditcaptchaimg/".$this->session->userdata('creditcap')['time'].".jpg");
    $this->load->helper('captcha');
    $vals = array(
    'img_path'  => './creditcaptchaimg/',
    'img_url' => base_url().'creditcaptchaimg/',
    'img_width' => '180',
    'img_height' => '50',
    'expiration' => '7200'
    );
    $caprecord = create_captcha($vals);
    $this->session->unset_userdata('creditcap');
    $this->session->set_userdata('creditcap', $caprecord);
    print $caprecord['image'];
    
  }
  public function get_captchacredit(){

    $this->load->helper('captcha');
    $vals = array(
    'img_path'  => './creditcaptchaimg/',
    'img_url' => base_url().'creditcaptchaimg/',
    'img_width' => '180',
    'img_height' => '50',
    'expiration' => '7200'
    );
    $caprecord = create_captcha($vals);
    $this->session->set_userdata('creditcap', $caprecord);

  }

  public function insertshipdetail(){
      
    $citynameid=$this->input->post('shcity');
    $city=explode('_', $citynameid);

    $citynameidb=$this->input->post('bilcity');
    $cityb=explode('_', $citynameidb);

    if(empty($city[1]))
    {
        $ctyidsss= $this->session->userdata('bizzgainmember')->city;
    }
    else
    {
        $ctyidsss= $city[1];
    }
    
 
      $parameter=array(
                      'act_mode'        =>'insertshipdata',
                      'row_id'          =>'',
                      'user_regid'      =>$this->input->post('userregid'),
                      'ship_name'       =>$this->input->post('shname'),
                      'bill_name'       =>$this->input->post('bilname'),
                      'ship_address'    =>$this->input->post('shaddress'),
                      'bill_address'    =>$this->input->post('biladd'),
                      'ship_email'      =>$this->input->post('shemail'),
                      'bill_email'      =>$this->input->post('bilemail'),
                      'ship_pincode'    =>$this->input->post('shpin'),
                      'bill_pincode'    =>$this->input->post('bilpin'),
                      'ship_contact'    =>$this->input->post('shcontact'),
                      'bill_contact'    =>$this->input->post('bilcontact'),
                      'ship_state'      =>$this->input->post('shstate'),
                      'bill_state'      =>$this->input->post('bilstate'),
                      'ship_city'       =>$city[0],#$this->input->post('shcity'),
                      'bill_city'       =>$cityb[0],#$this->input->post('bilcity'),
                      'ship_cst'        =>$this->input->post('shipcst'),
                      'bill_cst'        =>$this->input->post('bilcst'),
                      'ship_tinno'      =>$this->input->post('shiptin'),
                      'bill_tinno'      =>$this->input->post('biltin'),
                      'ship_createdon'  =>$ctyidsss#cityid
                      );

      //p($parameter);exit();
      $path=api_url().'checkoutapi/shipdetail/format/json/'; 
      $response=curlpost($parameter,$path);
      $addressid=array('addressId'=>$response->lastId);
      
       $parameter=array('act_mode'  =>'getmemupdate',
                       'useremail' =>'',
                       'n_status'  =>'',
                       'rowid'     =>$this->input->post('userregid')
                       );

      $response = $this->supper_admin->call_procedureRow('proc_newsletter',$parameter);
   

          //------------------ city group id -----------------------//
      $parameter11=array(
          'act_mode' =>'citygroupid',
          'row_id' =>$response->city,
          'vusername' =>$response->stateid,
          'vpassword'=>'',
          'vstatus' =>'',
          );
     // p($parameter11);//exit();
      $path11=api_url().'userapi/citygrouplogin/format/json/'; 
      $record=curlpost($parameter11,$path11);
     // p($record);//exit();

      //-------------------end-----------------------------------//
      if(!empty($response->id)){
        $this->session->set_userdata('bizzgainmember',$response);
        $this->session->set_userdata('logincity',$record);

        
      }
      
      
      $this->session->set_userdata('addressId',$addressid);
      $address=$this->session->userdata('addressId');
      echo "success";
  }  

  /*public function ckecklogin(){
 
      $parameter=array(
          'act_mode' =>'memlogin',
          'row_id' =>'',
          'vusername' =>$this->input->post('cemail'),
          'vpassword'=>$this->input->post('cpass'),
          'vstatus' =>$this->input->post('cacctype'),
          );

      $path=api_url().'userapi/userlogin/format/json/'; 
      $response=curlpost($parameter,$path);
      if(!empty($response->id)){
        $pathuser = api_url().'checkoutapi/getaddress/userId/'.$response->id.'/format/json/';
        $responseAnother = curlget($pathuser);
        $this->session->set_userdata('bizzgainmember',$response);
        //p($this->session->all_userdata());exit();
        $str ="";
        $str .='<input type="hidden" name="ssid" id="ssid" value="'.$this->session->userdata('bizzgainmember')->id.'">';
        //p($responseAnother);exit();
        if($responseAnother->scalar != "Something Went Wrong") {
        foreach ($responseAnother as $key => $value) {
         
         $str .='<div id="newadd" class="col-lg-3">
        <div class="edit_addr_bg">
        <div class="edit_addr">
        <h3>'.$value['shipname'].'</h3>
        <div class="phone"><i class="fa fa-phone"></i> '.$value['shipcontact'].'</div>
         <div class="address"><i class="fa fa-home"></i>'.$value['shipaddress'].$value['shipcity'].$value['statename'].$value['shippincode'].'</div>
         <div class="edt_button">
         <a class="edit" href="#">Edit</a>
          <a class="remove" href="'.base_url().'webapp/checkout/delshipdetail/'.$value['shippingid'].'">Remove</a></div>
        </div>
        </div>
        </div>';

        }//end foreach

      }//end if
        
        echo $str;
        
      } else {
        echo "fail";
      }
    }*/


  public function delshipdetail($rowid){

    //$rowid = $this->uri->segment(4);
    $delpathuser = api_url().'checkoutapi/delgetaddress/userId/'.$rowid.'/format/json/';
    $delresponseaddress = curlget($delpathuser);
    redirect('checkout');

  }

  public function vieweditshipdetail(){
    $shipid =$this->input->post('shipid');
    //return $shipid;
    //p($shipid);exit();
    $upshipemailid=$this->session->userdata('bizzgainmember')->compemailid;
    $pathuser = api_url().'checkoutapi/getupdateaddress/shippid/'.$shipid.'/format/json/';
    $record = curlget($pathuser);
    $str="";
    $str .='
      <form method="post" action="'.base_url().'webapp/checkout/editshipdetail"> 
      <div class="shipping_detail" id="shipping_detail">
           <div class="col-lg-6">
                <div class="shipping_d">
                    <h3>SHIPPING DETAILS</h3>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Name <span class="red">*</span></div></div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="shipname" name="shipname" type="text" value="'.$record->shipname.'" readonly /></div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Email Address <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="shipemail" name="shipemail" type="text" value="'.$upshipemailid.'" readonly /></div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Mobile No. <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="shipcontact" name="shipcontact" type="text" value="'.$record->shipcontact.'" readonly /></div>
                        </div>
                    </div>
                    
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Address <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><textarea id="shipadd" name="shipadd" rows="2" cols="20">'.$record->shipaddress.'</textarea></div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Pincode <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="shippin" name="shippin" type="text" value="'.$record->shippincode.'" /></div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">State <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil">
                                <input id="shipstates" name="shipstates" type="text" readonly value="'.$record->statename.'" />
                            </div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">City <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="shipcity" name="shipcity" type="text" value="'.$record->shipcity.'" readonly /></div>
                        </div>
                   </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="billing_d">
                    <h3>BILLING DETAILS</h3>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Name <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="billname" name="billname" type="text" value="'.$record->billname.'" /></div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Email Address <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="billemail" name="billemail" type="text" value="'.$upshipemailid.'" readonly /></div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Mobile No. <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="billcontact" name="billcontact" type="text" value="'.$record->billcontactno.'" /></div>
                        </div>
                    </div>
                  
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Address <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><textarea id="billadd" name="billadd" rows="2" cols="20">'.$record->billaddress.'</textarea></div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">Pincode <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="billpin" name="billpin" type="text" value="'.$record->billpincode.'" /></div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">State <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil">
                                <input id="billstates" name="billstates" type="text" readonly value="'.$record->statename.'" />
                              </div>
                        </div>
                    </div>
                    <div class="fild_box">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="text">City <span class="red">*</span></div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="text_fil"><input id="billcity" name="billcity" type="text" value="'.$record->billcity.'" readonly /></div>
                        </div>
                    </div>
                  </div>
            </div>
       <div class="col-lg-12">
            <input type="hidden" id="shippingid" name="shippingid" value="'.$record->shippingid.'" />
           </div>
       </div>
        <div class="col-lg-12">
            <div class="check_btn">
                <input id="Submit1" type="submit" name="editshipsubmit" value="Continue" class="btn continu" />
            </div>
        </div>
        
    </div></form>';
    echo $str;
    
  }

  public function editshipdetail(){
   
    $parameter=array(
                      'act_mode'        =>'updateshipdata',
                      'row_id'          =>$this->input->post('shippingid'),
                      'user_regid'      =>'',
                      'ship_name'       =>$this->input->post('shipname'),
                      'bill_name'       =>$this->input->post('billname'),
                      'ship_address'    =>$this->input->post('shipadd'),
                      'bill_address'    =>$this->input->post('billadd'),
                      'ship_email'      =>'',
                      'bill_email'      =>'',
                      'ship_pincode'    =>$this->input->post('shippin'),
                      'bill_pincode'    =>$this->input->post('billpin'),
                      'ship_contact'    =>$this->input->post('shipcontact'),
                      'bill_contact'    =>$this->input->post('billcontact'),
                      'ship_state'      =>'',
                      'bill_state'      =>'',
                      'ship_city'       =>'',
                      'bill_city'       =>'',
                      'ship_cst'        =>'',
                      'bill_cst'        =>'',
                      'ship_tinno'      =>'',
                      'bill_tinno'      =>'',
                      'ship_createdon'  =>''
                      );

      //p($parameter);exit();
      $path=api_url().'checkoutapi/shipdetail/format/json/'; 
      $response=curlpost($parameter,$path);
      redirect('checkout');

  }
  

public function cod(){
      
    $usercap=trim($this->input->post('usercap'));
    $syscap=trim($this->session->userdata('cap')['word']);
    $citygroupid=$this->session->userdata('logincity')->cmid;
    if($syscap==$usercap){
      unlink("captchaimg/".$this->session->userdata('cap')['time'].".jpg");
    $UserloginDetail= $this->session->userdata('bizzgainmember');
    $oldUserAddressId =$this->input->post('copyshipdetid');
    $tax_cst =$this->session->userdata('cart_tax')['tax_cst'];
    $tax_tin =$this->session->userdata('cart_tax')['tax_tin'];
    $tax_entry =$this->session->userdata('cart_tax')['tax_entry'];
    $tax_total =$this->session->userdata('cart_tax')['tax_total'];
    $shipppamt=$this->session->userdata('cart_tax')['shippcharge'];

  
    if (empty($oldUserAddressId)) {
      $sessUserAddressId  = $this->session->userdata('addressId');
      $UserAddressId  = $sessUserAddressId['addressId'];
    } else {
      $UserAddressId  = $oldUserAddressId;
    }
    $paymentMod     = 'COD';
    $orderNumber    = 'ORD'.$this->randnumber(); 
    $totalamount    = $this->cart->total();
    $userIpaddress  = $_SERVER['REMOTE_ADDR'];
    //$grandtotalcoddiscount = number_format(($totalamount*3)/100,1,".","");
    $grandtotalcoddiscount = 0;
    $grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");
    //$grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","")-$coupon_discount; // -coupon_discount added by manishkr


    //-------------------- start wallet -----------------------------------//
    $walletdrcrpath=api_url().'walletapi/drcrwallet/user_id/'.$UserloginDetail->id.'/format/json/'; 
    $record['walletdrcr']=curlget($walletdrcrpath);
    //p($record['walletdrcr']);exit;
    //-------------------- end wallet ----------------------------------------//
    $totfixwalletamt=0;
    foreach($record['walletdrcr'] as $value){ 
      $totfixwalletamt = number_format($totfixwalletamt,1,".","")+number_format($value['w_amount'],1,".","");
    }
    //p($totfixwalletamt);exit;
    if($this->input->post('userwallamtuse')==1 && $totfixwalletamt!=0 && $totfixwalletamt!=0.0){

      if($this->input->post('usercoupco')==1){
        
        if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }  

        $finalgrandamt=($grandtotal-($coupon_discount+$totfixwalletamt));

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $finalgrandamt,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => $coupon_discount,
                        'couponcode'    => $ccoupon_code,
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>$totfixwalletamt,
                        'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('userremark'));
        //p($parameter);exit();

        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);
        //p($insertProView);exit;
        if($insertProView->OrderId>0){

          //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------

          //-------------- start wallet ---------------------
          if($grandtotalcoddiscount!=0 || $grandtotalcoddiscount!=0.0){
            $parameterwallet=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'credit',
                                   'wa_amt'=>$grandtotalcoddiscount,
                                   'wa_status'=>''
                                   );
            $walletpath = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdata = curlpost($parameterwallet, $walletpath);
          }
          //-------------- end wallet -----------------------

          $showfornow = $this->insertIntoOrderMapwallcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$insertProView->usedwalletamt,$grandtotal);
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
        
          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9711007307, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $hmtopemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($hmtopemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------

          print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;

        }
        
      } else {

        $finalgrandamt=$grandtotal-$totfixwalletamt;

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $finalgrandamt,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => 0.0,
                        'couponcode'    => '',
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>$totfixwalletamt,
                        'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('userremark'));
        
        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);

        if($insertProView->OrderId>0){

          //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------

          //-------------- start wallet ---------------------
          if($grandtotalcoddiscount!=0 || $grandtotalcoddiscount!=0.0){
            $parameterwallet=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'credit',
                                   'wa_amt'=>$grandtotalcoddiscount,
                                   'wa_status'=>''
                                   );
            $walletpath = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdata = curlpost($parameterwallet, $walletpath);
          }
          //-------------- end wallet -----------------------

          $showfornow = $this->insertIntoOrderMapwall($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->usedwalletamt,$grandtotal);
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         
          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9700000000, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------

          print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;
        }
      }

    } else {

      if($this->input->post('usercoupco')==1){

        if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }  

        $finalgrandamt=$grandtotal-$coupon_discount;

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $finalgrandamt,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => $coupon_discount,
                        'couponcode'    => $ccoupon_code,
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>0,
                        'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('userremark'));
        
        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);

        if($insertProView->OrderId>0){
          //-------------- start wallet ---------------------
          if($grandtotalcoddiscount!=0 || $grandtotalcoddiscount!=0.0){
            $parameterwallet=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'credit',
                                   'wa_amt'=>$grandtotalcoddiscount,
                                   'wa_status'=>''
                                   );
            $walletpath = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdata = curlpost($parameterwallet, $walletpath);
          }
          //-------------- end wallet -----------------------

          $showfornow = $this->insertIntoOrderMapcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$grandtotal);
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
        
          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9700000000, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------

          print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;

        }
       } else {
        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $grandtotal,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => 0.0,
                        'couponcode'    => '',
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>0,
                        'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('userremark'));
        
        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);
 
        if($insertProView->OrderId>0){
          //-------------- start wallet ---------------------
          if($grandtotalcoddiscount!=0 || $grandtotalcoddiscount!=0.0){
            $parameterwallet=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'credit',
                                   'wa_amt'=>$grandtotalcoddiscount,
                                   'wa_status'=>''
                                   );
            $walletpath = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdata = curlpost($parameterwallet, $walletpath);
          }
          //-------------- end wallet -----------------------

          $showfornow = $this->insertIntoOrderMap($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod);
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
        
          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9700000000, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------

          print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;
        }
      } // end else coupon not use.
    }// end else wallet not use.
  }// end capcha if
  else{
    print "false";
  }
}


public function paytowallet(){
    
    $citygroupid=$this->session->userdata('logincity')->cmid;
    $UserloginDetail= $this->session->userdata('bizzgainmember');
    $oldUserAddressId =$this->input->post('copyshipdetid');
    $tax_cst =$this->session->userdata('cart_tax')['tax_cst'];
    $tax_tin =$this->session->userdata('cart_tax')['tax_tin'];
    $tax_entry =$this->session->userdata('cart_tax')['tax_entry'];
    $tax_total =$this->session->userdata('cart_tax')['tax_total'];
    $shipppamt=$this->session->userdata('cart_tax')['shippcharge'];
    //p($this->session->userdata('cart_tax'));exit;
    if (empty($oldUserAddressId)) {
      $sessUserAddressId  = $this->session->userdata('addressId');
      $UserAddressId  = $sessUserAddressId['addressId'];
    } else {
      $UserAddressId  = $oldUserAddressId;
    }
    $paymentMod     = 'ONLINE';
    $orderNumber    = 'ORD'.$this->randnumber(); 
    $totalamount    = $this->cart->total();
    $userIpaddress  = $_SERVER['REMOTE_ADDR'];
    $grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");

    if($this->input->post('usercoupco')==1){

        if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }
          $finalgrandamt=$grandtotal-$coupon_discount;

          $parameter = array(
                            'UserId'        => $UserloginDetail->id,
                            'userregid'     => $UserloginDetail->id,
                            'UserAddressId' => $UserAddressId, 
                            'orderNumber'   => $orderNumber,
                            'totalAmount'   => $finalgrandamt,
                            'paymentMod'    => $paymentMod,
                            'shipcharge'    => $shipppamt,
                            'couponamt'     => $coupon_discount,
                            'couponcode'    => $ccoupon_code,
                            'userIpaddress' => $userIpaddress,
                            'p_device'      => $citygroupid,
                            'usedwalletamount'=>$finalgrandamt,
                            'p_usedrewardpoint'=>0,  
                            'subtotal'      =>$totalamount,
                            'orderfrom'     =>'website',
                            'totaltax'      =>$tax_total,
                            'csttax'        =>$tax_cst,
                            'tintax'        =>$tax_tin,
                            'entrytax'      =>$tax_entry,
                            'u_remark'      =>$this->input->post('userremark'));
        //p($parameter);exit();

        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);

        
        //p($insertProView);exit;
        
        if($insertProView->OrderId>0){
          //-------------- start wallet ---------------------
          
            $parameterwallet=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$grandtotal,
                                   'wa_status'=>''
                                   );
            $walletpath = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdata = curlpost($parameterwallet, $walletpath);
          
          //-------------- end wallet -----------------------

          $showfornow = $this->insertIntoOrderMapwallcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$insertProView->usedwalletamt,$grandtotal);
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9700000000, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------

          print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;
        }

    } else {

          $parameter = array(
                            'UserId'        => $UserloginDetail->id,
                            'userregid'     => $UserloginDetail->id,
                            'UserAddressId' => $UserAddressId, 
                            'orderNumber'   => $orderNumber,
                            'totalAmount'   => $grandtotal,
                            'paymentMod'    => $paymentMod,
                            'shipcharge'    => $shipppamt,
                            'couponamt'     => 0.0,
                            'couponcode'    => '',
                            'userIpaddress' => $userIpaddress,
                            'p_device'      => $citygroupid,
                            'usedwalletamount'=>$grandtotal,
                            'p_usedrewardpoint'=>0,  
                            'subtotal'      =>$totalamount,
                            'orderfrom'     =>'website',
                            'totaltax'      =>$tax_total,
                            'csttax'        =>$tax_cst,
                            'tintax'        =>$tax_tin,
                            'entrytax'      =>$tax_entry,
                            'u_remark'      =>$this->input->post('userremark'));
        //p($parameter);exit();

        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);

        
        //p($insertProView);exit;
        
        if($insertProView->OrderId>0){
          //-------------- start wallet ---------------------
          
            $parameterwallet=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$grandtotal,
                                   'wa_status'=>''
                                   );
            $walletpath = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdata = curlpost($parameterwallet, $walletpath);
          
          //-------------- end wallet -----------------------

          $showfornow = $this->insertIntoOrderMapwall($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->usedwalletamt,$grandtotal);
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9700000000, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------

          print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;
        } 
    } // end else coupon not use. 
  } // end function.


  public function credit(){
    
    $usercap=trim($this->input->post('usercap'));
    $syscap=trim($this->session->userdata('creditcap')['word']);
    $citygroupid=$this->session->userdata('logincity')->cmid;
    if($syscap==$usercap){
      unlink("creditcaptchaimg/".$this->session->userdata('creditcap')['time'].".jpg");
    $UserloginDetail= $this->session->userdata('bizzgainmember');
    $oldUserAddressId =$this->input->post('copyshipdetid');
    $tax_cst =$this->session->userdata('cart_tax')['tax_cst'];
    $tax_tin =$this->session->userdata('cart_tax')['tax_tin'];
    $tax_entry =$this->session->userdata('cart_tax')['tax_entry'];
    $tax_total =$this->session->userdata('cart_tax')['tax_total'];
    $shipppamt=$this->session->userdata('cart_tax')['shippcharge'];
    //p($this->session->userdata('cart_tax'));exit;
    if (empty($oldUserAddressId)) {
      $sessUserAddressId  = $this->session->userdata('addressId');
      $UserAddressId  = $sessUserAddressId['addressId'];
    } else {
      $UserAddressId  = $oldUserAddressId;
    }
    $paymentMod     = 'CREDIT';
    $orderNumber    = 'ORD'.$this->randnumber(); 
    $totalamount    = $this->cart->total();
    $userIpaddress  = $_SERVER['REMOTE_ADDR'];
    $grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");

    //-------------------- start wallet -----------------------------------//
    $walletdrcrpath=api_url().'walletapi/drcrwallet/user_id/'.$UserloginDetail->id.'/format/json/'; 
    $record['walletdrcr']=curlget($walletdrcrpath);
    //-------------------- end wallet ----------------------------------------//
    $totfixwalletamt=0;
    foreach($record['walletdrcr'] as $value){ 
      $totfixwalletamt = number_format($totfixwalletamt,1,".","")+number_format($value['w_amount'],1,".","");
    }
    
    if($this->input->post('userwallamtuse')==1 && $totfixwalletamt!=0 && $totfixwalletamt!=0.0){

      if($this->input->post('usercoupco')==1){

        if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }  

        $finalgrandamt=($grandtotal-($coupon_discount+$totfixwalletamt));

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $finalgrandamt,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => $coupon_discount,
                        'couponcode'    => $ccoupon_code,
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>$totfixwalletamt,
                        'p_usedrewardpoint'=>'',  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('userremark'));
        //p($parameter);exit();

        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);
      
        //p($insertProView);exit;
        
        if($insertProView->OrderId>0){
          //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------
          $showfornow = $this->insertIntoOrderMapwallcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$insertProView->usedwalletamt,$grandtotal);
          //p($showfornow);exit();
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9711007307, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------
             //------------------ check null profile --------------   
             $creditpath = api_url().'checkoutapi/checkcredit/chcreditid/'.$UserloginDetail->id.'/format/json/';
             $insercredit = curlget($creditpath);
              if(empty($insercredit->vendoraddress) || empty($insercredit->venpincode) || empty($insercredit->cstvalue) || empty($insercredit->tinvalue) || empty($insercredit->panno) || empty($insercredit->cstmageurl) || empty($insercredit->tinimageurl) || empty($insercredit->chequeimageurl) || empty($insercredit->panimageurl) || empty($insercredit->VenBankName) || empty($insercredit->venbranchname) || empty($insercredit->AccountHolderName) || empty($insercredit->IfscCode)){

                $this->session->set_flashdata("creditmessage", "Please complete your profile along with all the requisite documents. Kindly ignore if it is already completed. You will hear from us very soon.");

             }
              
              //------------------ end check null profile --------------
           print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;
        }
      
      } else {

          $finalgrandamt=$grandtotal-$totfixwalletamt; 

          $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $finalgrandamt,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => 0.0,
                        'couponcode'    => '',
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>$totfixwalletamt,
                        'p_usedrewardpoint'=>'',  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('userremark'));
        //p($parameter);exit();

        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);
      
        //p($insertProView);exit;
        
        if($insertProView->OrderId>0){
          //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------
          $showfornow = $this->insertIntoOrderMapwall($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->usedwalletamt,$grandtotal);
          //p($showfornow);exit();
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9700000000, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------
             //------------------ check null profile --------------   
             $creditpath = api_url().'checkoutapi/checkcredit/chcreditid/'.$UserloginDetail->id.'/format/json/';
             $insercredit = curlget($creditpath);
              if(empty($insercredit->vendoraddress) || empty($insercredit->venpincode) || empty($insercredit->cstvalue) || empty($insercredit->tinvalue) || empty($insercredit->panno) || empty($insercredit->cstmageurl) || empty($insercredit->tinimageurl) || empty($insercredit->chequeimageurl) || empty($insercredit->panimageurl) || empty($insercredit->VenBankName) || empty($insercredit->venbranchname) || empty($insercredit->AccountHolderName) || empty($insercredit->IfscCode)){

                $this->session->set_flashdata("creditmessage", "Please complete your profile along with all the requisite documents. Kindly ignore if it is already completed. You will hear from us very soon.");

             }
              
              //------------------ end check null profile --------------
           print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;
        }

      }
    } else {
      if($this->input->post('usercoupco')==1){

        if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }  

        $finalgrandamt=$grandtotal-$coupon_discount;

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $finalgrandamt,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => $coupon_discount,
                        'couponcode'    => $ccoupon_code,
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>0,
                        'p_usedrewardpoint'=>'',  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('userremark'));
        //p($parameter);exit();

        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);
      
        //p($insertProView);exit;
        
        if($insertProView->OrderId>0){
          $showfornow = $this->insertIntoOrderMapcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$grandtotal);
         //p($showfornow);exit();
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9711007307, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------
             //------------------ check null profile --------------   
             $creditpath = api_url().'checkoutapi/checkcredit/chcreditid/'.$UserloginDetail->id.'/format/json/';
             $insercredit = curlget($creditpath);
              if(empty($insercredit->vendoraddress) || empty($insercredit->venpincode) || empty($insercredit->cstvalue) || empty($insercredit->tinvalue) || empty($insercredit->panno) || empty($insercredit->cstmageurl) || empty($insercredit->tinimageurl) || empty($insercredit->chequeimageurl) || empty($insercredit->panimageurl) || empty($insercredit->VenBankName) || empty($insercredit->venbranchname) || empty($insercredit->AccountHolderName) || empty($insercredit->IfscCode)){

                $this->session->set_flashdata("creditmessage", "Please complete your profile along with all the requisite documents. Kindly ignore if it is already completed. You will hear from us very soon.");

             }
              
              //------------------ end check null profile --------------
           print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;
        }
      
      } else {

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $grandtotal,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => 0.0,
                        'couponcode'    => '',
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>0,
                        'p_usedrewardpoint'=>'',  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('userremark'));
        //p($parameter);exit();

        $path = api_url().'checkoutapi/payment/format/json/'; 
        $insertProView = curlpost($parameter, $path);
      
        //p($insertProView);exit;
        
        if($insertProView->OrderId>0){
          $showfornow = $this->insertIntoOrderMap($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod);
         //p($showfornow);exit();
          
        }
        if($insertProView->OrderId>0){
         $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

          //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9700000000, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------
             //------------------ check null profile --------------   
             $creditpath = api_url().'checkoutapi/checkcredit/chcreditid/'.$UserloginDetail->id.'/format/json/';
             $insercredit = curlget($creditpath);
              if(empty($insercredit->vendoraddress) || empty($insercredit->venpincode) || empty($insercredit->cstvalue) || empty($insercredit->tinvalue) || empty($insercredit->panno) || empty($insercredit->cstmageurl) || empty($insercredit->tinimageurl) || empty($insercredit->chequeimageurl) || empty($insercredit->panimageurl) || empty($insercredit->VenBankName) || empty($insercredit->venbranchname) || empty($insercredit->AccountHolderName) || empty($insercredit->IfscCode)){

                $this->session->set_flashdata("creditmessage", "Please complete your profile along with all the requisite documents. Kindly ignore if it is already completed. You will hear from us very soon.");

             }
              
              //------------------ end check null profile --------------
           print base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber;
        }
      } // end else coupon not use.
    } // end else wallet not use.
  } // end capcha if
  else{
    print "false";
  } // end capcha else.
 } // end function.


  public function ordcheque(){
    
    $citygroupid=$this->session->userdata('logincity')->cmid;
    if($this->input->post('chequesubmit')){

    $UserloginDetail= $this->session->userdata('bizzgainmember');
    $oldUserAddressId =$this->input->post('ch_copyshipdetid');
    $tax_cst =$this->session->userdata('cart_tax')['tax_cst'];
    $tax_tin =$this->session->userdata('cart_tax')['tax_tin'];
    $tax_entry =$this->session->userdata('cart_tax')['tax_entry'];
    $tax_total =$this->session->userdata('cart_tax')['tax_total'];
    $shipppamt=$this->session->userdata('cart_tax')['shippcharge'];
    //p($tax_total);exit();
    //p($oldUserAddressId);exit();
    if (empty($oldUserAddressId)) {
      $sessUserAddressId  = $this->session->userdata('addressId');
      $UserAddressId  = $sessUserAddressId['addressId'];
    } else {
      $UserAddressId  = $oldUserAddressId;
    }
    $paymentMod     = 'CHEQUE';
    $orderNumber    = 'ORD'.$this->randnumber(); 
    $totalamount    = $this->cart->total();
    $userIpaddress  = $_SERVER['REMOTE_ADDR'];
    $grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");

    //-------------------- start wallet -----------------------------------//
    $walletdrcrpath=api_url().'walletapi/drcrwallet/user_id/'.$UserloginDetail->id.'/format/json/'; 
    $record['walletdrcr']=curlget($walletdrcrpath);
    //-------------------- end wallet ----------------------------------------//
    $totfixwalletamt=0;
    foreach($record['walletdrcr'] as $value){ 
      $totfixwalletamt = number_format($totfixwalletamt,1,".","")+number_format($value['w_amount'],1,".","");
    }
    
    if($this->input->post('chequsewallet')==1 && $totfixwalletamt!=0 && $totfixwalletamt!=0.0){

      if($this->input->post('chequsecoupcode')==1){

        if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }  

        $finalgrandamt=($grandtotal-($coupon_discount+$totfixwalletamt));

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $finalgrandamt,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => $coupon_discount,
                        'couponcode'    => $ccoupon_code,
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>$totfixwalletamt,
                        'p_usedrewardpoint'=>'',  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('memchremark'));
                       
    //p($parameter); exit();
    $path = api_url().'checkoutapi/payment/format/json/'; 
    $insertProView = curlpost($parameter, $path);
    //p($insertProView);exit();

    if($insertProView->OrderId>0){

        $param_che = array(
                        'act_mode'          => 'insertcheq',
                        'row_id'            => '',
                        'orderid'           => $insertProView->OrderId,
                        'chequeno'          => $this->input->post('chequeno'),
                        'ifsc'              => $this->input->post('ifsccode'),
                        'bankname'          => $this->input->post('bankname'),
                        'chequedate'        => $this->input->post('chequedate'),
                        'accno'             => $this->input->post('accno'),
                        'chequecollectdate' => $this->input->post('chequecollectdate'),
                        'chequeamount'      => $this->input->post('chequeamount'),
                        'paymentMod'        => $paymentMod,
                        'orderno'           => $insertProView->OrderNumber
                       );

        $path_ch = api_url().'checkoutapi/paymentcheque/format/json/'; 
        $insertProView_ch = curlpost($param_che, $path_ch);

        //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------

          $showfornow = $this->insertIntoOrderMapwallcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$insertProView->usedwalletamt,$grandtotal);
        
        }
        if($insertProView->OrderId>0){
          $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

         //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9711007307, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------


          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------
          redirect('success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
        } // end check order id if for mail. 

      } else {

        $finalgrandamt=$grandtotal-$totfixwalletamt;

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $finalgrandamt,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => 0.0,
                        'couponcode'    => '',
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>$totfixwalletamt,
                        'p_usedrewardpoint'=>'',  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('memchremark'));
                       
    //p($parameter); exit();
    $path = api_url().'checkoutapi/payment/format/json/'; 
    $insertProView = curlpost($parameter, $path);
    //p($insertProView);exit();

    if($insertProView->OrderId>0){

        $param_che = array(
                        'act_mode'          => 'insertcheq',
                        'row_id'            => '',
                        'orderid'           => $insertProView->OrderId,
                        'chequeno'          => $this->input->post('chequeno'),
                        'ifsc'              => $this->input->post('ifsccode'),
                        'bankname'          => $this->input->post('bankname'),
                        'chequedate'        => $this->input->post('chequedate'),
                        'accno'             => $this->input->post('accno'),
                        'chequecollectdate' => $this->input->post('chequecollectdate'),
                        'chequeamount'      => $this->input->post('chequeamount'),
                        'paymentMod'        => $paymentMod,
                        'orderno'           => $insertProView->OrderNumber
                       );

        $path_ch = api_url().'checkoutapi/paymentcheque/format/json/'; 
        $insertProView_ch = curlpost($param_che, $path_ch);

          //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------

          $showfornow = $this->insertIntoOrderMapwall($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->usedwalletamt,$grandtotal);
        
        }
        if($insertProView->OrderId>0){
          $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

         //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9700000000, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------


          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------
          redirect('success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
        } // end check order id if for mail.

      }
    } else {

      if($this->input->post('chequsecoupcode')==1){

        if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }  

        $finalgrandamt=$grandtotal-$coupon_discount;

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $grandtotal,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => 0.0,
                        'couponcode'    => '',
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>0,
                        'p_usedrewardpoint'=>'',  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('memchremark'));
                       
    //p($parameter); exit();
    $path = api_url().'checkoutapi/payment/format/json/'; 
    $insertProView = curlpost($parameter, $path);
    //p($insertProView);exit();

    if($insertProView->OrderId>0){

        $param_che = array(
                        'act_mode'          => 'insertcheq',
                        'row_id'            => '',
                        'orderid'           => $insertProView->OrderId,
                        'chequeno'          => $this->input->post('chequeno'),
                        'ifsc'              => $this->input->post('ifsccode'),
                        'bankname'          => $this->input->post('bankname'),
                        'chequedate'        => $this->input->post('chequedate'),
                        'accno'             => $this->input->post('accno'),
                        'chequecollectdate' => $this->input->post('chequecollectdate'),
                        'chequeamount'      => $this->input->post('chequeamount'),
                        'paymentMod'        => $paymentMod,
                        'orderno'           => $insertProView->OrderNumber
                       );

        $path_ch = api_url().'checkoutapi/paymentcheque/format/json/'; 
        $insertProView_ch = curlpost($param_che, $path_ch);

          $showfornow = $this->insertIntoOrderMapcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$grandtotal);
          
        
        }
        if($insertProView->OrderId>0){
          $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

         //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9711007307, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------


          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------
          redirect('success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
        } // end check order id if for mail.

      } else {

        $parameter = array(
                        'UserId'        => $UserloginDetail->id,
                        'userregid'     => $UserloginDetail->id,
                        'UserAddressId' => $UserAddressId, 
                        'orderNumber'   => $orderNumber,
                        'totalAmount'   => $grandtotal,
                        'paymentMod'    => $paymentMod,
                        'shipcharge'    => $shipppamt,
                        'couponamt'     => 0.0,
                        'couponcode'    => '',
                        'userIpaddress' => $userIpaddress,
                        'p_device'      => $citygroupid,
                        'usedwalletamount'=>0,
                        'p_usedrewardpoint'=>'',  
                        'subtotal'      =>$totalamount,
                        'orderfrom'     =>'website',
                        'totaltax'      =>$tax_total,
                        'csttax'        =>$tax_cst,
                        'tintax'        =>$tax_tin,
                        'entrytax'      =>$tax_entry,
                        'u_remark'      =>$this->input->post('memchremark'));
                       
    //p($parameter); exit();
    $path = api_url().'checkoutapi/payment/format/json/'; 
    $insertProView = curlpost($parameter, $path);
    //p($insertProView);exit();

    if($insertProView->OrderId>0){

        $param_che = array(
                        'act_mode'          => 'insertcheq',
                        'row_id'            => '',
                        'orderid'           => $insertProView->OrderId,
                        'chequeno'          => $this->input->post('chequeno'),
                        'ifsc'              => $this->input->post('ifsccode'),
                        'bankname'          => $this->input->post('bankname'),
                        'chequedate'        => $this->input->post('chequedate'),
                        'accno'             => $this->input->post('accno'),
                        'chequecollectdate' => $this->input->post('chequecollectdate'),
                        'chequeamount'      => $this->input->post('chequeamount'),
                        'paymentMod'        => $paymentMod,
                        'orderno'           => $insertProView->OrderNumber
                       );

        $path_ch = api_url().'checkoutapi/paymentcheque/format/json/'; 
        $insertProView_ch = curlpost($param_che, $path_ch);

          $showfornow = $this->insertIntoOrderMap($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod);
          
        
        }
        if($insertProView->OrderId>0){
          $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);
         //p($userinfo["mailview"][0]['ShippingName']);exit;

         //-------------start SMS API--------------------
              $smsphnon = $insertProView->ContactNo; 
              $smsname = $insertProView->ShippingName;
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9711007307, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderNumber.' '.$smsmsg);
              
              //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------


          //------------email--------------
              $bizzgainemail="support@mindzshop.com";
              
              $emailid=$insertProView->Email;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'support@mindzshop.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
              $this->email->message($msg);
              //$this->email->send();

              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                $userinfomailsend = curlget($path);
              }
          //------------email--------------
          redirect('success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
        } // end check order id if for mail. 

      } // end else coupon not use.
    } // end else wallet not use.
  } // end chequesubmit if.
 } // end funtion.



  public function insertIntoOrderMap($ordertblId,$orderNumber,$paymentMod){
      $x = $this->cart->contents();
      //p($this->session->userdata);exit();
      $cstptax=$this->session->userdata('cstprowisetax');
      $tinptax=$this->session->userdata('tinprowisetax');
      $entryptax=$this->session->userdata('entryprowisetax');
      $cstpc=$this->session->userdata('cstprowisepc');
      $tinpc=$this->session->userdata('tinprowisepc');
      $entrypc=$this->session->userdata('entryprowisepc');
      
      foreach ($x as $key => $value) {
        
        if($value['product_type']=='combo'){

           

            $pathuser = api_url().'checkoutapi/getcombodetails/comboId/'.$value['id'].'/format/json/';
            $records['combodet'] = curlget($pathuser);
           // p($records['combodet']);exit;

            $newqty    =explode('to', $value['proqty1']);
            $newqty2   =explode('to', $value['proqty2']);
            $newqty3   =explode('to', $value['proqty3']);


            foreach ($records['combodet'] as $key => $comboval) {
            if($value['qty']<$newqty[0] ){
            $newfinalPrice='';  
            $newfinalPrice=citybaseprice($comboval['pro_combo_price'],$value['cityval']);
          //p($newfinalPrice);
            }

            if($value['qty']>=$newqty[0] && $value['qty']<=$newqty[1] ){
            $newfinalPrice='';
           
              
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price1']);
          //p("heloo1");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']>=$newqty2[0] && $value['qty']<=$newqty2[1] ){
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price2']);
          //p("hel2");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']>=$newqty3[0] && $value['qty']<='100000' ){
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price3']);
          //p("he3");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']==0){
            $newfinalPrice=0;
            } 
            $parameter = array(
                               'orderId'        => $ordertblId,
                               'proid'          => $comboval['prodtmappid'],
                               'qty'            => $comboval['product_qty']*$value['qty'],
                               'shippingcharge' => 0.0,
                               'proname'        => $comboval['ProductName'],
                               'vendorId'       => $comboval['manufactureId'],
                               'prodSize'       => '',
                               'prodPrice'      => $newfinalPrice,
                               'prodColor'      => '',
                               'coupon_value'   => 0.0,
                               'proimg'         =>'http://192.168.1.30/bizzbizzgain/images/hoverimg/'.$comboval['Image'],
                               'cstvalue'       =>$cstptax[$comboval['product_id']],
                               'tinvalue'       =>$tinptax[$comboval['product_id']],
                               'entryvalue'     =>$entryptax[$comboval['product_id']],
                               'cst_p'          =>$cstpc[$comboval['product_id']],
                               'tin_p'          =>$tinpc[$comboval['product_id']],
                               'entry_p'        =>$entrypc[$comboval['product_id']],
                               'getcatid'       =>$comboval['sub_catid'],
                               'getskuno'       =>$comboval['SKUNO'],
                               'getpromrp'      =>$comboval['pro_seling'],
                               'getcombotype'   =>$value['product_type'],
                               'getcomboid'     =>$value['id'],
                               'getcomboprice'  =>$value['price'],
                               'getcomboqty'    =>$value['qty'],
                               'getcombosellprice'=>$value['mrpprice'],
                               'getcomboname'   =>$value['name'],
                               'getcomboimg'    =>$value['img']
                              );
                //p($parameter);
               $path = api_url().'checkoutapi/saveinordertblcombo/format/json/';
               $insertProView = curlpost($parameter,$path);
            
            }

        } else {

            $parameter = array(
                                 'orderId'        => $ordertblId,
                                 'proid'          => $value['id'],
                                 'qty'            => $value['qty'],
                                 'shippingcharge' => 0.0,
                                 'proname'        => $value['name'],
                                 'vendorId'       => $value['VendorID'],
                                 'prodSize'       => $value['options']['sizeattr'],
                                 'prodPrice'      => $value['price'],
                                 'prodColor'      => $value['options']['color'],
                                 'coupon_value'   => 0.0,
                                 'proimg'         =>$value['img'],
                                 'cstvalue'       =>$cstptax[$value['proid']],
                                 'tinvalue'       =>$tinptax[$value['proid']],
                                 'entryvalue'     =>$entryptax[$value['proid']],
                                 'cst_p'          =>$cstpc[$value['proid']],
                                 'tin_p'          =>$tinpc[$value['proid']],
                                 'entry_p'        =>$entrypc[$value['proid']]
                            );
            //p($parameter);exit;
           $path = api_url().'checkoutapi/saveinordertbl/format/json/';
           $insertProView = curlpost($parameter,$path);
        }
        
           
      }
  
       return $insertProView;
  }

  public function insertIntoOrderMapwallcoup($ordertblId,$orderNumber,$paymentMod,$usercoupamt,$userwallamt,$ordfinalprice){
      $x = $this->cart->contents();
      $carttotalproduct=count($this->cart->contents());
      //p($this->session->userdata);exit();
      $cstptax=$this->session->userdata('cstprowisetax');
      $tinptax=$this->session->userdata('tinprowisetax');
      $entryptax=$this->session->userdata('entryprowisetax');
      $cstpc=$this->session->userdata('cstprowisepc');
      $tinpc=$this->session->userdata('tinprowisepc');
      $entrypc=$this->session->userdata('entryprowisepc');

      //$ordmapcouponper=number_format(($usercoupamt/$carttotalproduct),1,".","");
      //$ordmapwalletper=number_format(($userwallamt/$carttotalproduct),1,".","");
      //$totordmapwallcoup=$usercoupamt+$userwallamt;
      $ordmapcouponper=get_partialdiscountpercent($usercoupamt,$ordfinalprice); 
      $ordmapwalletper=get_partialdiscountpercent($userwallamt,$ordfinalprice);  


      foreach ($x as $key => $value) {
        
        if($value['product_type']=='combo'){

         

           $pathuser = api_url().'checkoutapi/getcombodetails/comboId/'.$value['id'].'/format/json/';
            $records['combodet'] = curlget($pathuser);
           // p($records['combodet']);exit;

            $newqty    =explode('to', $value['proqty1']);
            $newqty2   =explode('to', $value['proqty2']);
            $newqty3   =explode('to', $value['proqty3']);


            foreach ($records['combodet'] as $key => $comboval) {
            if($value['qty']<$newqty[0] ){
            $newfinalPrice='';  
            $newfinalPrice=citybaseprice($comboval['pro_combo_price'],$value['cityval']);
          //p($newfinalPrice);
            }

            if($value['qty']>=$newqty[0] && $value['qty']<=$newqty[1] ){
            $newfinalPrice='';
           
              
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price1']);
          //p("heloo1");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']>=$newqty2[0] && $value['qty']<=$newqty2[1] ){
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price2']);
          //p("hel2");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']>=$newqty3[0] && $value['qty']<='100000' ){
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price3']);
          //p("he3");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']==0){
            $newfinalPrice=0;
            } 
            $parameter = array(
                               'orderId'        => $ordertblId,
                               'proid'          => $comboval['prodtmappid'],
                               'qty'            => $comboval['product_qty']*$value['qty'],
                               'shippingcharge' => $ordmapwalletper,
                               'proname'        => $comboval['ProductName'],
                               'vendorId'       => $comboval['manufactureId'],
                               'prodSize'       => '',
                               'prodPrice'      => $newfinalPrice,
                               'prodColor'      => '',
                               'coupon_value'   => $ordmapcouponper,
                               'proimg'         =>'http://192.168.1.30/bizzbizzgain/images/hoverimg/'.$comboval['Image'],
                               'cstvalue'       =>$cstptax[$comboval['product_id']],
                               'tinvalue'       =>$tinptax[$comboval['product_id']],
                               'entryvalue'     =>$entryptax[$comboval['product_id']],
                               'cst_p'          =>$cstpc[$comboval['product_id']],
                               'tin_p'          =>$tinpc[$comboval['product_id']],
                               'entry_p'        =>$entrypc[$comboval['product_id']],
                               'getcatid'       =>$comboval['sub_catid'],
                               'getskuno'       =>$comboval['SKUNO'],
                               'getpromrp'      =>$comboval['pro_seling'],
                               'getcombotype'   =>$value['product_type'],
                               'getcomboid'     =>$value['id'],
                               'getcomboprice'  =>$value['price'],
                               'getcomboqty'    =>$value['qty'],
                               'getcombosellprice'=>$value['mrpprice'],
                               'getcomboname'   =>$value['name'],
                               'getcomboimg'    =>$value['img']
                              );

                //p($parameter);
               $path = api_url().'checkoutapi/saveinordertblcombo/format/json/';
               $insertProView = curlpost($parameter,$path);
            
            }

        } else{

            $parameter = array(
                                 'orderId'        => $ordertblId,
                                 'proid'          => $value['id'],
                                 'qty'            => $value['qty'],
                                 'shippingcharge' => $ordmapwalletper,
                                 //'shippingcharge' => 0.0,
                                 'proname'        => $value['name'],
                                 'vendorId'       => $value['VendorID'],
                                 'prodSize'       => $value['options']['sizeattr'],
                                 'prodPrice'      => $value['price'],
                                 'prodColor'      => $value['options']['color'],
                                 'coupon_value'   => $ordmapcouponper,
                                 'proimg'         =>$value['img'],
                                 'cstvalue'       =>$cstptax[$value['proid']],
                                 'tinvalue'       =>$tinptax[$value['proid']],
                                 'entryvalue'     =>$entryptax[$value['proid']],
                                 'cst_p'          =>$cstpc[$value['proid']],
                                 'tin_p'          =>$tinpc[$value['proid']],
                                 'entry_p'        =>$entrypc[$value['proid']]
                            );
            
           $path = api_url().'checkoutapi/saveinordertbl/format/json/';
           $insertProView = curlpost($parameter,$path);
      }
           
      }
      
       return $insertProView;
       

  }
  
  public function insertIntoOrderMapwall($ordertblId,$orderNumber,$paymentMod,$userwallamt,$ordfinalprice){
      $x = $this->cart->contents();
      $carttotalproduct=count($this->cart->contents());
      //p($this->session->userdata);exit();
      $cstptax=$this->session->userdata('cstprowisetax');
      $tinptax=$this->session->userdata('tinprowisetax');
      $entryptax=$this->session->userdata('entryprowisetax');
      $cstpc=$this->session->userdata('cstprowisepc');
      $tinpc=$this->session->userdata('tinprowisepc');
      $entrypc=$this->session->userdata('entryprowisepc');

      $ordmapwalletper=get_partialdiscountpercent($userwallamt,$ordfinalprice);

      foreach ($x as $key => $value) {
        
        if($value['product_type']=='combo'){

         

           $pathuser = api_url().'checkoutapi/getcombodetails/comboId/'.$value['id'].'/format/json/';
            $records['combodet'] = curlget($pathuser);
           // p($records['combodet']);exit;

            $newqty    =explode('to', $value['proqty1']);
            $newqty2   =explode('to', $value['proqty2']);
            $newqty3   =explode('to', $value['proqty3']);


            foreach ($records['combodet'] as $key => $comboval) {
            if($value['qty']<$newqty[0] ){
            $newfinalPrice='';  
            $newfinalPrice=citybaseprice($comboval['pro_combo_price'],$value['cityval']);
          //p($newfinalPrice);
            }

            if($value['qty']>=$newqty[0] && $value['qty']<=$newqty[1] ){
            $newfinalPrice='';
           
              
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price1']);
          //p("heloo1");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']>=$newqty2[0] && $value['qty']<=$newqty2[1] ){
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price2']);
          //p("hel2");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']>=$newqty3[0] && $value['qty']<='100000' ){
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price3']);
          //p("he3");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']==0){
            $newfinalPrice=0;
            } 
            $parameter = array(
                               'orderId'        => $ordertblId,
                               'proid'          => $comboval['prodtmappid'],
                               'qty'            => $comboval['product_qty']*$value['qty'],
                               'shippingcharge' => $ordmapwalletper,
                               'proname'        => $comboval['ProductName'],
                               'vendorId'       => $comboval['manufactureId'],
                               'prodSize'       => '',
                               'prodPrice'      => $newfinalPrice,
                               'prodColor'      => '',
                               'coupon_value'   => 0.0,
                               'proimg'         =>'http://192.168.1.30/bizzbizzgain/images/hoverimg/'.$comboval['Image'],
                               'cstvalue'       =>$cstptax[$comboval['product_id']],
                               'tinvalue'       =>$tinptax[$comboval['product_id']],
                               'entryvalue'     =>$entryptax[$comboval['product_id']],
                               'cst_p'          =>$cstpc[$comboval['product_id']],
                               'tin_p'          =>$tinpc[$comboval['product_id']],
                               'entry_p'        =>$entrypc[$comboval['product_id']],
                               'getcatid'       =>$comboval['sub_catid'],
                               'getskuno'       =>$comboval['SKUNO'],
                               'getpromrp'      =>$comboval['pro_seling'],
                               'getcombotype'   =>$value['product_type'],
                               'getcomboid'     =>$value['id'],
                               'getcomboprice'  =>$value['price'],
                               'getcomboqty'    =>$value['qty'],
                               'getcombosellprice'=>$value['mrpprice'],
                               'getcomboname'   =>$value['name'],
                               'getcomboimg'    =>$value['img']
                              );
          
                //p($parameter);
               $path = api_url().'checkoutapi/saveinordertblcombo/format/json/';
               $insertProView = curlpost($parameter,$path);
            
            }

        } else {

            $parameter = array(
                                 'orderId'        => $ordertblId,
                                 'proid'          => $value['id'],
                                 'qty'            => $value['qty'],
                                 'shippingcharge' => $ordmapwalletper,
                                 'proname'        => $value['name'],
                                 'vendorId'       => $value['VendorID'],
                                 'prodSize'       => $value['options']['sizeattr'],
                                 'prodPrice'      => $value['price'],
                                 'prodColor'      => $value['options']['color'],
                                 'coupon_value'   => 0.0,
                                 'proimg'         =>$value['img'],
                                 'cstvalue'       =>$cstptax[$value['proid']],
                                 'tinvalue'       =>$tinptax[$value['proid']],
                                 'entryvalue'     =>$entryptax[$value['proid']],
                                 'cst_p'          =>$cstpc[$value['proid']],
                                 'tin_p'          =>$tinpc[$value['proid']],
                                 'entry_p'        =>$entrypc[$value['proid']]
                            );
            //p($parameter);exit;
           $path = api_url().'checkoutapi/saveinordertbl/format/json/';
           $insertProView = curlpost($parameter,$path);
        }

           
      }
      
       return $insertProView;
  }

  public function insertIntoOrderMapcoup($ordertblId,$orderNumber,$paymentMod,$usercoupamt,$ordfinalprice){
      $x = $this->cart->contents();
      $carttotalproduct=count($this->cart->contents());
      //p($this->session->userdata);exit();
      $cstptax=$this->session->userdata('cstprowisetax');
      $tinptax=$this->session->userdata('tinprowisetax');
      $entryptax=$this->session->userdata('entryprowisetax');
      $cstpc=$this->session->userdata('cstprowisepc');
      $tinpc=$this->session->userdata('tinprowisepc');
      $entrypc=$this->session->userdata('entryprowisepc');

      $ordmapcouponper=get_partialdiscountpercent($usercoupamt,$ordfinalprice);

      foreach ($x as $key => $value) {
        
        if($value['product_type']=='combo'){

            /*$parameter = array(
                                 'orderId'        => $ordertblId,
                                 'proid'          => $value['id'],
                                 'qty'            => $value['qty'],
                                 'shippingcharge' => 0.0,
                                 'proname'        => $value['name'],
                                 'vendorId'       => '',
                                 'prodSize'       => $value['product_type'],
                                 'prodPrice'      => $value['price'],
                                 'prodColor'      => '',
                                 'coupon_value'   => $ordmapcouponper,
                                 'proimg'         => $value['img'],
                                 'cstvalue'       => $cstptax[$value['proid']],
                                 'tinvalue'       => $tinptax[$value['proid']],
                                 'entryvalue'     => $entryptax[$value['proid']],
                                 'cst_p'          => $cstpc[$value['proid']],
                                 'tin_p'          => $tinpc[$value['proid']],
                                 'entry_p'        => $entrypc[$value['proid']]
                            );
            //p($parameter);exit;
           $path = api_url().'checkoutapi/saveinordertbl/format/json/';
           $insertProView = curlpost($parameter,$path);*/

           $pathuser = api_url().'checkoutapi/getcombodetails/comboId/'.$value['id'].'/format/json/';
            $records['combodet'] = curlget($pathuser);
           // p($records['combodet']);exit;

            $newqty    =explode('to', $value['proqty1']);
            $newqty2   =explode('to', $value['proqty2']);
            $newqty3   =explode('to', $value['proqty3']);


            foreach ($records['combodet'] as $key => $comboval) {
            if($value['qty']<$newqty[0] ){
            $newfinalPrice='';  
            $newfinalPrice=citybaseprice($comboval['pro_combo_price'],$value['cityval']);
          //p($newfinalPrice);
            }

            if($value['qty']>=$newqty[0] && $value['qty']<=$newqty[1] ){
            $newfinalPrice='';
           
              
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price1']);
          //p("heloo1");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']>=$newqty2[0] && $value['qty']<=$newqty2[1] ){
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price2']);
          //p("hel2");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']>=$newqty3[0] && $value['qty']<='100000' ){
            $newfinalPrice=cityqtyprice($comboval['pro_combo_price'],$value['cityval'],$value['price3']);
          //p("he3");  
          //p($newfinalPrice);//exit;
            }
            if($value['qty']==0){
            $newfinalPrice=0;
            } 
            $parameter = array(
                               'orderId'        => $ordertblId,
                               'proid'          => $comboval['prodtmappid'],
                               'qty'            => $comboval['product_qty']*$value['qty'],
                               'shippingcharge' => 0.0,
                               'proname'        => $comboval['ProductName'],
                               'vendorId'       => $comboval['manufactureId'],
                               'prodSize'       => '',
                               'prodPrice'      => $newfinalPrice,
                               'prodColor'      => '',
                               'coupon_value'   => $ordmapcouponper,
                               'proimg'         =>'http://192.168.1.30/bizzbizzgain/images/hoverimg/'.$comboval['Image'],
                               'cstvalue'       =>$cstptax[$comboval['product_id']],
                               'tinvalue'       =>$tinptax[$comboval['product_id']],
                               'entryvalue'     =>$entryptax[$comboval['product_id']],
                               'cst_p'          =>$cstpc[$comboval['product_id']],
                               'tin_p'          =>$tinpc[$comboval['product_id']],
                               'entry_p'        =>$entrypc[$comboval['product_id']],
                               'getcatid'       =>$comboval['sub_catid'],
                               'getskuno'       =>$comboval['SKUNO'],
                               'getpromrp'      =>$comboval['pro_seling'],
                               'getcombotype'   =>$value['product_type'],
                               'getcomboid'     =>$value['id'],
                               'getcomboprice'  =>$value['price'],
                               'getcomboqty'    =>$value['qty'],
                               'getcombosellprice'=>$value['mrpprice'],
                               'getcomboname'   =>$value['name'],
                               'getcomboimg'    =>$value['img']
                              );
          
                //p($parameter);
               $path = api_url().'checkoutapi/saveinordertblcombo/format/json/';
               $insertProView = curlpost($parameter,$path);
            
            }

        } else {

            $parameter = array(
                                 'orderId'        => $ordertblId,
                                 'proid'          => $value['id'],
                                 'qty'            => $value['qty'],
                                 'shippingcharge' => 0.0,
                                 'proname'        => $value['name'],
                                 'vendorId'       => $value['VendorID'],
                                 'prodSize'       => $value['options']['sizeattr'],
                                 'prodPrice'      => $value['price'],
                                 'prodColor'      => $value['options']['color'],
                                 'coupon_value'   => $ordmapcouponper,
                                 'proimg'         => $value['img'],
                                 'cstvalue'       => $cstptax[$value['proid']],
                                 'tinvalue'       => $tinptax[$value['proid']],
                                 'entryvalue'     => $entryptax[$value['proid']],
                                 'cst_p'          => $cstpc[$value['proid']],
                                 'tin_p'          => $tinpc[$value['proid']],
                                 'entry_p'        => $entrypc[$value['proid']]
                            );
            //p($parameter);exit;
           $path = api_url().'checkoutapi/saveinordertbl/format/json/';
           $insertProView = curlpost($parameter,$path);
        }

           
      }
      
       return $insertProView;
  }

  public function randnumber(){
    srand ((double) microtime() * 1000000);
    $random5 = rand(10000,99999);
    return $random5;
  }
 

public function online_payment(){
  $this->session->set_userdata('shipcid', $this->input->post('shipcid'));
  $this->session->set_userdata('onlineusewalletsess', $this->input->post('onlineusewallet'));
  $this->session->set_userdata('onlineusecoupcodesess', $this->input->post('onlineusecoupcode'));

//p($_POST);exit;
  if( $this->input->post('payment')==='payumoney'){

    

 $PAYU_BASE_URL=$this->config->item('PAYU_BASE_URL'); 
    
$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
  
  }
}

$hash = '';
// Hash Sequence
 $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
 
  $hashVarsSeq = explode('|', $hashSequence);

    $hash_string = '';  
  foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $this->config->item('SALT');


    $hash = strtolower(hash('sha512', $hash_string)); 
    $action = $PAYU_BASE_URL . '/_payment';
  }



    $data['payu'] = array(

        'action'=>$action,     
        'key' => $this->input->post('key'),  
        'hash' => $hash,
        'txnid' =>  $this->input->post('txnid'),
        'amount' => $this->input->post('amount'),
        'firstname' => $this->input->post('firstname'),
        'email' => $this->input->post('email'),
        'phone' => $this->input->post('phone'),
        'productinfo' => $this->input->post('productinfo'),
        'surl' => $this->input->post('surl'),
        'furl' => $this->input->post('furl'),
        'lastname' => $this->input->post('lastname'),
        'curl' => $this->input->post('curl'),
        'address1' => $this->input->post('address1'),
        'address2' => $this->input->post('address2'),
        'city' => $this->input->post('city'),
        'state' => $this->input->post('state'),
        'country' => $this->input->post('country'),
        'zipcode' => $this->input->post('zipcode')
        

    );
     
      $this->load->view('payumoney',$data);
    }
    else
    {
  set_include_path(FCPATH.'citrus/lib'.PATH_SEPARATOR.get_include_path());
require_once (FCPATH.'citrus/lib/CitrusPay.php');
require_once (FCPATH.'citrus/lib/Zend/Crypt/Hmac.php');
   function generateHmacKey($data, $apiKey=null){
        $hmackey = Zend_Crypt_Hmac::compute($apiKey, "sha1", $data);
        return $hmackey;
    } 


    //Replace this with your secret key from the citrus panel
    $secret_key = "0305b1890601f39aa5cfe4605ded01b26e55aa09";
    
     
    

    CitrusPay::setApiKey($secret_key,'citrus');
    $vanityUrl     = '50anfws9m0';
    $currency      = 'INR';
    $merchantTxnId = uniqid();//Should be OrderId
    $orderAmount   = $this->input->post('amount');
    $tmpdata       = "$vanityUrl$orderAmount$merchantTxnId$currency";
    
    $secSignature  = generateHmacKey($tmpdata,CitrusPay::getApiKey());
    $action        = CitrusPay::getCPBase()."$vanityUrl"; 
    $payment_url   = "https://sandbox.citruspay.com/sslperf/your-vanityUrlPart";

    $accessKey     = '3FBU3FX3I0NYC3O5G2NE';
    $reqtime       = time()*1000;

    $data['citrus'] = array(

        'action'=>$action,     
        'merchantAccessKey' => $accessKey,  
        'merchantTxnId' => $merchantTxnId,
        'addressState' =>  $this->input->post('state'),
        'addressCity' => $this->input->post('city'),
        'addressStreet1' => $this->input->post('address1'),
        'addressCountry' => $this->input->post('country'),
        'addressZip' => $this->input->post('zipcode'),
        'firstName' => $this->input->post('firstname'),
        'lastName' => $this->input->post('lastname'),
        'phoneNumber' => $this->input->post('phone'),
        'email' => $this->input->post('email'),
        'returnUrl' => $config['base_url'].'webapp/checkout/success',
        'notifyUrl' => $config['base_url'].'webapp/checkout/success',
        'orderAmount' => $orderAmount,
        'reqtime' => $reqtime,
        'secSignature' => $secSignature ,
        'currency' => 'INR'
       

    );
    $this->load->view('citrus',$data);
      //echo "please select payumoney";
      //return false;
    }

}

  public function success(){

  if($_REQUEST["status"]==="success")
    {
 
    $citygroupid=$this->session->userdata('logincity')->cmid;
  
    $UserloginDetail= $this->session->userdata('bizzgainmember');
    $oldUserAddressId =$this->session->userdata('shipcid');
    $onlineusewalletsess =$this->session->userdata('onlineusewalletsess');
    $onlineusecoupcodesess =$this->session->userdata('onlineusecoupcodesess');
    $tax_cst =$this->session->userdata('cart_tax')['tax_cst'];
    $tax_tin =$this->session->userdata('cart_tax')['tax_tin'];
    $tax_entry =$this->session->userdata('cart_tax')['tax_entry'];
    $tax_total =$this->session->userdata('cart_tax')['tax_total'];
    $shipppamt=$this->session->userdata('cart_tax')['shippcharge'];
    //p($this->session->userdata('cart_tax'));exit;
    if (empty($oldUserAddressId)) {
      $sessUserAddressId  = $this->session->userdata('addressId');
      $UserAddressId  = $sessUserAddressId['addressId'];
    } else {
      $UserAddressId  = $oldUserAddressId;
    }
    $paymentMod     = 'ONLINE';
    $orderNumber    = 'ORD'.$this->randnumber(); 
    $totalamount    = $this->cart->total();
    $userIpaddress  = $_SERVER['REMOTE_ADDR'];
    $grandtotalcoddiscount = number_format(($totalamount*0)/100,1,".","");
    //$grandtotalcoddiscount = number_format(($totalamount*2)/100,1,".","");
    //$grandtotalafterdiscount = number_format(number_format($totalamount+$tax_total+$shipppamt,1,".","")-$grandtotalcoddiscount,1,".","");
    $grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");

    //-------------------- start wallet -----------------------------------//
    $walletdrcrpath=api_url().'walletapi/drcrwallet/user_id/'.$UserloginDetail->id.'/format/json/'; 
    $record['walletdrcr']=curlget($walletdrcrpath);
    //-------------------- end wallet ----------------------------------------//
    $totfixwalletamt=0;
    foreach($record['walletdrcr'] as $value){ 
      $totfixwalletamt = number_format($totfixwalletamt,1,".","")+number_format($value['w_amount'],1,".","");
    }

    if($onlineusewalletsess==1 && $totfixwalletamt!=0 && $totfixwalletamt!=0.0){

      if($onlineusecoupcodesess==1){

        if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }  

        $finalgrandamt=($grandtotal-($coupon_discount+$totfixwalletamt));


        $parameter = array(
                          'UserId'        => $UserloginDetail->id,
                          'userregid'     => $UserloginDetail->id,
                          'UserAddressId' => $UserAddressId, 
                          'orderNumber'   => $orderNumber,
                          'totalAmount'   => $finalgrandamt,
                          'paymentMod'    => $paymentMod,
                          'shipcharge'    => $shipppamt,
                          'couponamt'     => $coupon_discount,
                          'couponcode'    => $ccoupon_code,
                          'userIpaddress' => $userIpaddress,
                          'p_device'      => $citygroupid,
                          'usedwalletamount'=>$totfixwalletamt,
                          'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                          'subtotal'      =>$totalamount,
                          'orderfrom'     =>'website',
                          'totaltax'      =>$tax_total,
                          'csttax'        =>$tax_cst,
                          'tintax'        =>$tax_tin,
                          'entrytax'      =>$tax_entry,
                          'u_remark'      =>$this->input->post('userremark'));
          // p($parameter);exit();

          $path = api_url().'checkoutapi/payment/format/json/'; 
          $insertProView = curlpost($parameter, $path);
        
         // p($insertProView);exit;
          
          if($insertProView->OrderId>0){

            //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------

            $showfornow = $this->insertIntoOrderMapwallcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$insertProView->usedwalletamt,$grandtotal);
          }
          if($insertProView->OrderId>0){
           $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
           $userinfo["mailview"] =(array) curlget($path);
           //p($userinfo["mailview"][0]['ShippingName']);exit;

            //-------------start SMS API--------------------
                $smsphnon = $insertProView->ContactNo; 
                $smsname = $insertProView->ShippingName;
                
                $smsmsg = " has been placed with us. You will receive an email with your order details shortly. Call us at +91 9711007307, if you need help. Thank You for choosing MindzShop.com ";
                
                $smsmsgg = urlencode('Hi '.$smsname.' your order '.'('.$insertProView->OrderNumber.')'.$smsmsg);
                
               //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

            //-------------end SMS API--------------------

            //------------email--------------
                $bizzgainemail="support@mindzshop.com";
                
                $emailid=$insertProView->Email;
                $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
                $this->load->library('email');
                $this->email->from($bizzgainemail, 'MindzShop.com');
                $this->email->to($emailid); 
                //$this->email->cc($listbizzmail);
                $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
                $this->email->message($msg);
                //$this->email->send();

                if($this->email->send()){
                  $actmodename='ordmailsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                } else {
                  $actmodename='ordmailnotsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                }
            //------------email--------------

            redirect(base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
          }

      } else{

              $finalgrandamt=$grandtotal-$totfixwalletamt;

              $parameter = array(
                          'UserId'        => $UserloginDetail->id,
                          'userregid'     => $UserloginDetail->id,
                          'UserAddressId' => $UserAddressId, 
                          'orderNumber'   => $orderNumber,
                          'totalAmount'   => $finalgrandamt,
                          'paymentMod'    => $paymentMod,
                          'shipcharge'    => $shipppamt,
                          'couponamt'     => 0.0,
                          'couponcode'    => '',
                          'userIpaddress' => $userIpaddress,
                          'p_device'      => $citygroupid,
                          'usedwalletamount'=>$totfixwalletamt,
                          'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                          'subtotal'      =>$totalamount,
                          'orderfrom'     =>'website',
                          'totaltax'      =>$tax_total,
                          'csttax'        =>$tax_cst,
                          'tintax'        =>$tax_tin,
                          'entrytax'      =>$tax_entry,
                          'u_remark'      =>$this->input->post('userremark'));
          // p($parameter);exit();

          $path = api_url().'checkoutapi/payment/format/json/'; 
          $insertProView = curlpost($parameter, $path);
        
         // p($insertProView);exit;
          
          if($insertProView->OrderId>0){

            //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------

            $showfornow = $this->insertIntoOrderMapwall($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->usedwalletamt,$grandtotal);
          }
          if($insertProView->OrderId>0){
           $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
           $userinfo["mailview"] =(array) curlget($path);
           //p($userinfo["mailview"][0]['ShippingName']);exit;

            //-------------start SMS API--------------------
                $smsphnon = $insertProView->ContactNo; 
                $smsname = $insertProView->ShippingName;
                
                $smsmsg = " has been placed with us. You will receive an email with your order details shortly. Call us at +91 9711007307, if you need help. Thank You for choosing MindzShop.com ";
                
                $smsmsgg = urlencode('Hi '.$smsname.' your order '.'('.$insertProView->OrderNumber.')'.$smsmsg);
                
               //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

            //-------------end SMS API--------------------

            //------------email--------------
                $bizzgainemail="support@mindzshop.com";
                
                $emailid=$insertProView->Email;
                $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
                $this->load->library('email');
                $this->email->from($bizzgainemail, 'MindzShop.com');
                $this->email->to($emailid); 
                //$this->email->cc($listbizzmail);
                $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
                $this->email->message($msg);
                //$this->email->send();

                if($this->email->send()){
                  $actmodename='ordmailsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                } else {
                  $actmodename='ordmailnotsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                }
            //------------email--------------

            redirect(base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
          }

      }
    } else {

      if($onlineusecoupcodesess==1){

          if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }  

        $finalgrandamt=$grandtotal-$coupon_discount;

        $parameter = array(
                          'UserId'        => $UserloginDetail->id,
                          'userregid'     => $UserloginDetail->id,
                          'UserAddressId' => $UserAddressId, 
                          'orderNumber'   => $orderNumber,
                          'totalAmount'   => $finalgrandamt,
                          'paymentMod'    => $paymentMod,
                          'shipcharge'    => $shipppamt,
                          'couponamt'     => $coupon_discount,
                          'couponcode'    => $ccoupon_code,
                          'userIpaddress' => $userIpaddress,
                          'p_device'      => $citygroupid,
                          'usedwalletamount'=>0,
                          'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                          'subtotal'      =>$totalamount,
                          'orderfrom'     =>'website',
                          'totaltax'      =>$tax_total,
                          'csttax'        =>$tax_cst,
                          'tintax'        =>$tax_tin,
                          'entrytax'      =>$tax_entry,
                          'u_remark'      =>$this->input->post('userremark'));
          // p($parameter);exit();

          $path = api_url().'checkoutapi/payment/format/json/'; 
          $insertProView = curlpost($parameter, $path);
        
         // p($insertProView);exit;
          
          if($insertProView->OrderId>0){
            $showfornow = $this->insertIntoOrderMapcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$grandtotal);
          }
          if($insertProView->OrderId>0){
           $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
           $userinfo["mailview"] =(array) curlget($path);
           //p($userinfo["mailview"][0]['ShippingName']);exit;

            //-------------start SMS API--------------------
                $smsphnon = $insertProView->ContactNo; 
                $smsname = $insertProView->ShippingName;
                
                $smsmsg = " has been placed with us. You will receive an email with your order details shortly. Call us at +91 9711007307, if you need help. Thank You for choosing MindzShop.com ";
                
                $smsmsgg = urlencode('Hi '.$smsname.' your order '.'('.$insertProView->OrderNumber.')'.$smsmsg);
                
               //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

            //-------------end SMS API--------------------

            //------------email--------------
                $bizzgainemail="support@mindzshop.com";
                
                $emailid=$insertProView->Email;
                $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
                $this->load->library('email');
                $this->email->from($bizzgainemail, 'MindzShop.com');
                $this->email->to($emailid); 
                //$this->email->cc($listbizzmail);
                $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
                $this->email->message($msg);
                //$this->email->send();

                if($this->email->send()){
                  $actmodename='ordmailsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                } else {
                  $actmodename='ordmailnotsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                }
            //------------email--------------

            redirect(base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
          }


      } else{

          $parameter = array(
                          'UserId'        => $UserloginDetail->id,
                          'userregid'     => $UserloginDetail->id,
                          'UserAddressId' => $UserAddressId, 
                          'orderNumber'   => $orderNumber,
                          'totalAmount'   => $grandtotal,
                          'paymentMod'    => $paymentMod,
                          'shipcharge'    => $shipppamt,
                          'couponamt'     => 0.0,
                          'couponcode'    => '',
                          'userIpaddress' => $userIpaddress,
                          'p_device'      => $citygroupid,
                          'usedwalletamount'=>0,
                          'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                          'subtotal'      =>$totalamount,
                          'orderfrom'     =>'website',
                          'totaltax'      =>$tax_total,
                          'csttax'        =>$tax_cst,
                          'tintax'        =>$tax_tin,
                          'entrytax'      =>$tax_entry,
                          'u_remark'      =>$this->input->post('userremark'));
          // p($parameter);exit();

          $path = api_url().'checkoutapi/payment/format/json/'; 
          $insertProView = curlpost($parameter, $path);
        
         // p($insertProView);exit;
          
          if($insertProView->OrderId>0){
            $showfornow = $this->insertIntoOrderMap($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod);
          }
          if($insertProView->OrderId>0){
           $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
           $userinfo["mailview"] =(array) curlget($path);
           //p($userinfo["mailview"][0]['ShippingName']);exit;

            //-------------start SMS API--------------------
                $smsphnon = $insertProView->ContactNo; 
                $smsname = $insertProView->ShippingName;
                
                $smsmsg = " has been placed with us. You will receive an email with your order details shortly. Call us at +91 9711007307, if you need help. Thank You for choosing MindzShop.com ";
                
                $smsmsgg = urlencode('Hi '.$smsname.' your order '.'('.$insertProView->OrderNumber.')'.$smsmsg);
                
               //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

            //-------------end SMS API--------------------

            //------------email--------------
                $bizzgainemail="support@mindzshop.com";
                
                $emailid=$insertProView->Email;
                $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
                $this->load->library('email');
                $this->email->from($bizzgainemail, 'MindzShop.com');
                $this->email->to($emailid); 
                //$this->email->cc($listbizzmail);
                $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
                $this->email->message($msg);
                //$this->email->send();

                if($this->email->send()){
                  $actmodename='ordmailsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                } else {
                  $actmodename='ordmailnotsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                }
            //------------email--------------

            redirect(base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
          }


      } // end else coupon not use
    } // end else wallet not use 

   // echo 'payment success';
    //p($_POST);
  }
  else if ($_REQUEST['TxStatus']==='SUCCESS') {
    $citygroupid=$this->session->userdata('logincity')->cmid;
    
  
    $UserloginDetail= $this->session->userdata('bizzgainmember');
    $oldUserAddressId =$this->session->userdata('shipcid');
    $tax_cst =$this->session->userdata('cart_tax')['tax_cst'];
    $tax_tin =$this->session->userdata('cart_tax')['tax_tin'];
    $tax_entry =$this->session->userdata('cart_tax')['tax_entry'];
    $tax_total =$this->session->userdata('cart_tax')['tax_total'];
    $shipppamt=$this->session->userdata('cart_tax')['shippcharge'];
    //p($this->session->userdata('cart_tax'));exit;
    if (empty($oldUserAddressId)) {
      $sessUserAddressId  = $this->session->userdata('addressId');
      $UserAddressId  = $sessUserAddressId['addressId'];
    } else {
      $UserAddressId  = $oldUserAddressId;
    }
    $paymentMod     = 'ONLINE';
    $orderNumber    = 'ORD'.$this->randnumber(); 
    $totalamount    = $this->cart->total();
    $userIpaddress  = $_SERVER['REMOTE_ADDR'];
    $grandtotalcoddiscount = number_format(($totalamount*2)/100,1,".","");
    //$grandtotalcoddiscount = number_format(($totalamount*3)/100,1,".","");
    //$grandtotalafterdiscount = number_format(number_format($totalamount+$tax_total+$shipppamt,1,".","")-$grandtotalcoddiscount,1,".","");
    $grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");

    //-------------------- start wallet -----------------------------------//
    $walletdrcrpath=api_url().'walletapi/drcrwallet/user_id/'.$UserloginDetail->id.'/format/json/'; 
    $record['walletdrcr']=curlget($walletdrcrpath);
    //-------------------- end wallet ----------------------------------------//
    $totfixwalletamt=0;
    foreach($record['walletdrcr'] as $value){ 
      $totfixwalletamt = number_format($totfixwalletamt,1,".","")+number_format($value['w_amount'],1,".","");
    }

    if($onlineusewalletsess==1 && $totfixwalletamt!=0 && $totfixwalletamt!=0.0){

      if($onlineusecoupcodesess==1){

        if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
        } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
        }  

        $finalgrandamt=($grandtotal-($coupon_discount+$totfixwalletamt));

        $parameter = array(
                              'UserId'        => $UserloginDetail->id,
                              'userregid'     => $UserloginDetail->id,
                              'UserAddressId' => $UserAddressId, 
                              'orderNumber'   => $orderNumber,
                              'totalAmount'   => $finalgrandamt,
                              'paymentMod'    => $paymentMod,
                              'shipcharge'    => $shipppamt,
                              'couponamt'     => $coupon_discount,
                              'couponcode'    => $ccoupon_code,
                              'userIpaddress' => $userIpaddress,
                              'p_device'      => $citygroupid,
                              'usedwalletamount'=>$totfixwalletamt,
                              'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                              'subtotal'      =>$totalamount,
                              'orderfrom'     =>'website',
                              'totaltax'      =>$tax_total,
                              'csttax'        =>$tax_cst,
                              'tintax'        =>$tax_tin,
                              'entrytax'      =>$tax_entry,
                              'u_remark'      =>$this->input->post('userremark'));
         // p($parameter);exit();

          $path = api_url().'checkoutapi/payment/format/json/'; 
          $insertProView = curlpost($parameter, $path);
        
         // p($insertProView);exit;
          
          if($insertProView->OrderId>0){

            //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------

            $showfornow = $this->insertIntoOrderMapwallcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$insertProView->usedwalletamt,$grandtotal);
          }
          if($insertProView->OrderId>0){
           $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
           $userinfo["mailview"] =(array) curlget($path);
           //p($userinfo["mailview"][0]['ShippingName']);exit;

            //-------------start SMS API--------------------
                $smsphnon = $insertProView->ContactNo; 
                $smsname = $insertProView->ShippingName;
                
                $smsmsg = " has been placed with us. You will receive an email with your order details shortly. Call us at +91 9711007307, if you need help. Thank You for choosing MindzShop.com ";
                
                $smsmsgg = urlencode('Hi '.$smsname.' your order '.'('.$insertProView->OrderNumber.')'.$smsmsg);
                
                //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

            //-------------end SMS API--------------------

            //------------email--------------
                $bizzgainemail="support@mindzshop.com";
                
                $emailid=$insertProView->Email;
                $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
                $this->load->library('email');
                $this->email->from($bizzgainemail, 'MindzShop.com');
                $this->email->to($emailid); 
                //$this->email->cc($listbizzmail);
                $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
                $this->email->message($msg);
                //$this->email->send();

                if($this->email->send()){
                  $actmodename='ordmailsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                } else {
                  $actmodename='ordmailnotsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                }
            //------------email--------------

            redirect(base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
          }

      } else{

        $finalgrandamt=$grandtotal-$totfixwalletamt;

        $parameter = array(
                              'UserId'        => $UserloginDetail->id,
                              'userregid'     => $UserloginDetail->id,
                              'UserAddressId' => $UserAddressId, 
                              'orderNumber'   => $orderNumber,
                              'totalAmount'   => $finalgrandamt,
                              'paymentMod'    => $paymentMod,
                              'shipcharge'    => $shipppamt,
                              'couponamt'     => 0.0,
                              'couponcode'    => '',
                              'userIpaddress' => $userIpaddress,
                              'p_device'      => $citygroupid,
                              'usedwalletamount'=>$totfixwalletamt,
                              'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                              'subtotal'      =>$totalamount,
                              'orderfrom'     =>'website',
                              'totaltax'      =>$tax_total,
                              'csttax'        =>$tax_cst,
                              'tintax'        =>$tax_tin,
                              'entrytax'      =>$tax_entry,
                              'u_remark'      =>$this->input->post('userremark'));
         // p($parameter);exit();

          $path = api_url().'checkoutapi/payment/format/json/'; 
          $insertProView = curlpost($parameter, $path);
        
         // p($insertProView);exit;
          
          if($insertProView->OrderId>0){

            //-------------- start wallet ---------------------
      
            $parameterwalletd=array('act_mode'=>'addwalletmoney',
                                   'row_id'=>'',
                                   'wa_orderid'=>$insertProView->OrderId,
                                   'wa_userid'=>$UserloginDetail->id,
                                   'wa_type'=>'debit',
                                   'wa_amt'=>"-".$totfixwalletamt,
                                   'wa_status'=>''
                                   );
            $walletpathd = api_url().'walletapi/addwalletpay/format/json/'; 
            $walletdatad = curlpost($parameterwalletd, $walletpathd);
          
          //-------------- end wallet -----------------------

            $showfornow = $this->insertIntoOrderMapwall($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->usedwalletamt,$grandtotal);
          }
          if($insertProView->OrderId>0){
           $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
           $userinfo["mailview"] =(array) curlget($path);
           //p($userinfo["mailview"][0]['ShippingName']);exit;

            //-------------start SMS API--------------------
                $smsphnon = $insertProView->ContactNo; 
                $smsname = $insertProView->ShippingName;
                
                $smsmsg = " has been placed with us. You will receive an email with your order details shortly. Call us at +91 9711007307, if you need help. Thank You for choosing MindzShop.com ";
                
                $smsmsgg = urlencode('Hi '.$smsname.' your order '.'('.$insertProView->OrderNumber.')'.$smsmsg);
                
                //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

            //-------------end SMS API--------------------

            //------------email--------------
                $bizzgainemail="support@mindzshop.com";
                
                $emailid=$insertProView->Email;
                $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
                $this->load->library('email');
                $this->email->from($bizzgainemail, 'MindzShop.com');
                $this->email->to($emailid); 
                //$this->email->cc($listbizzmail);
                $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
                $this->email->message($msg);
                $this->email->send();

                if($this->email->send()){
                  $actmodename='ordmailsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                } else {
                  $actmodename='ordmailnotsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                }
            //------------email--------------

            redirect(base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
          }

      }
    } else {

      if($onlineusecoupcodesess==1){

          if($this->session->userdata('discount_amount')) {
            $coupon_discount = number_format($this->session->userdata('discount_amount'),1,".","");
            $ccoupon_code = $this->session->userdata('discount_coupon');
          } else { 
            $coupon_discount = "0.0";
            $ccoupon_code = "";
          }  

          $finalgrandamt=$grandtotal-$coupon_discount;

          $parameter = array(
                              'UserId'        => $UserloginDetail->id,
                              'userregid'     => $UserloginDetail->id,
                              'UserAddressId' => $UserAddressId, 
                              'orderNumber'   => $orderNumber,
                              'totalAmount'   => $finalgrandamt,
                              'paymentMod'    => $paymentMod,
                              'shipcharge'    => $shipppamt,
                              'couponamt'     => $coupon_discount,
                              'couponcode'    => $ccoupon_code,
                              'userIpaddress' => $userIpaddress,
                              'p_device'      => $citygroupid,
                              'usedwalletamount'=>0,
                              'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                              'subtotal'      =>$totalamount,
                              'orderfrom'     =>'website',
                              'totaltax'      =>$tax_total,
                              'csttax'        =>$tax_cst,
                              'tintax'        =>$tax_tin,
                              'entrytax'      =>$tax_entry,
                              'u_remark'      =>$this->input->post('userremark'));
         // p($parameter);exit();

          $path = api_url().'checkoutapi/payment/format/json/'; 
          $insertProView = curlpost($parameter, $path);
        
         // p($insertProView);exit;
          
          if($insertProView->OrderId>0){
            $showfornow = $this->insertIntoOrderMapcoup($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod,$insertProView->CouponAmt,$grandtotal);
          }
          if($insertProView->OrderId>0){
           $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
           $userinfo["mailview"] =(array) curlget($path);
           //p($userinfo["mailview"][0]['ShippingName']);exit;

            //-------------start SMS API--------------------
                $smsphnon = $insertProView->ContactNo; 
                $smsname = $insertProView->ShippingName;
                
                $smsmsg = " has been placed with us. You will receive an email with your order details shortly. Call us at +91 9711007307, if you need help. Thank You for choosing MindzShop.com ";
                
                $smsmsgg = urlencode('Hi '.$smsname.' your order '.'('.$insertProView->OrderNumber.')'.$smsmsg);
                
                //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

            //-------------end SMS API--------------------

            //------------email--------------
                $bizzgainemail="support@mindzshop.com";
                
                $emailid=$insertProView->Email;
                $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
                $this->load->library('email');
                $this->email->from($bizzgainemail, 'MindzShop.com');
                $this->email->to($emailid); 
                //$this->email->cc($listbizzmail);
                $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
                $this->email->message($msg);
                //$this->email->send();

                if($this->email->send()){
                  $actmodename='ordmailsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                } else {
                  $actmodename='ordmailnotsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                }
            //------------email--------------

            redirect(base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
          }

      } else{
          
          $parameter = array(
                              'UserId'        => $UserloginDetail->id,
                              'userregid'     => $UserloginDetail->id,
                              'UserAddressId' => $UserAddressId, 
                              'orderNumber'   => $orderNumber,
                              'totalAmount'   => $grandtotal,
                              'paymentMod'    => $paymentMod,
                              'shipcharge'    => $shipppamt,
                              'couponamt'     => 0.0,
                              'couponcode'    => '',
                              'userIpaddress' => $userIpaddress,
                              'p_device'      => $citygroupid,
                              'usedwalletamount'=>0,
                              'p_usedrewardpoint'=>$grandtotalcoddiscount,  
                              'subtotal'      =>$totalamount,
                              'orderfrom'     =>'website',
                              'totaltax'      =>$tax_total,
                              'csttax'        =>$tax_cst,
                              'tintax'        =>$tax_tin,
                              'entrytax'      =>$tax_entry,
                              'u_remark'      =>$this->input->post('userremark'));
         // p($parameter);exit();

          $path = api_url().'checkoutapi/payment/format/json/'; 
          $insertProView = curlpost($parameter, $path);
        
         // p($insertProView);exit;
          
          if($insertProView->OrderId>0){
            $showfornow = $this->insertIntoOrderMap($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod);
          }
          if($insertProView->OrderId>0){
           $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
           $userinfo["mailview"] =(array) curlget($path);
           //p($userinfo["mailview"][0]['ShippingName']);exit;

            //-------------start SMS API--------------------
                $smsphnon = $insertProView->ContactNo; 
                $smsname = $insertProView->ShippingName;
                
                $smsmsg = " has been placed with us. You will receive an email with your order details shortly. Call us at +91 9711007307, if you need help. Thank You for choosing MindzShop.com ";
                
                $smsmsgg = urlencode('Hi '.$smsname.' your order '.'('.$insertProView->OrderNumber.')'.$smsmsg);
                
                //$file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

            //-------------end SMS API--------------------

            //------------email--------------
                $bizzgainemail="support@mindzshop.com";
                
                $emailid=$insertProView->Email;
                $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
                $this->load->library('email');
                $this->email->from($bizzgainemail, 'MindzShop.com');
                $this->email->to($emailid); 
                //$this->email->cc($listbizzmail);
                $this->email->subject('Your order has been placed - ('.$insertProView->OrderNumber.')');
                $this->email->message($msg);
                //$this->email->send();

                if($this->email->send()){
                  $actmodename='ordmailsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                } else {
                  $actmodename='ordmailnotsend';
                  $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$insertProView->OrderId.'/format/json/';
                  $userinfomailsend = curlget($path);
                }
            //------------email--------------

            redirect(base_url().'success/'.$insertProView->OrderId.'/'.$insertProView->OrderNumber);
          }

      } // end else coupon not use.
    } // end else wallet not use.
  } // end else if
  else
  {
  $this->load->view('helper/header');
  $this->load->view('failure');
  $this->load->view('helper/footer');
   // p($_POST);
  }


}

function validate_coupon2(){ //@manish refined version of validate coupon

    $user_type = $this->session->userdata('bizzgainmember')->vendortype;
    $user_id = $this->session->userdata('bizzgainmember')->id;
    $cpn_code = $_POST['cpn_code'];
    $cartdata = array();
    $cart_contents = $this->cart->contents();
    $param = array ('user_type' => $user_type, 'coupon' => $cpn_code);
    $discount_request = api_url().'checkoutapi/discount_detail/format/json/';
    $d_wise = curlpost($param,$discount_request);
    $amt_qualify_discount = NULL;
    $amt_qualify_msg = "<span style='color:brown;'>Product List: </span>";
    $final_discount_amount = 0.0;
    $cartdata['msg'] = "Coupon Discount is not applicable!"; // default message
    //$cartdata['Test'] = $coupon_detail; //testing code 
    $coupon_detail = array();
    $cpn_match_flag = 0;
    if($this->cart->total() < 10)
    {
        $cartdata['msg'] = "Subtotal amount must have more than 10 before coupon applied";
        $cartdata['allow_discount'] = "false";
        //$cartdata['discount_amount'] = $cartdata['cpn_discount'];
        $this->session->set_userdata('applied_coupon',false);
        $this->session->set_userdata('allow_discount', false);
        $this->session->set_userdata('discount_amount',false);

        $d_wise->discount_data = NULL;  // Not going to execute next if block
    }
    if($d_wise->discount_data)
    {
          
           $cpn_match_flag = 1;
           $coupon_detail = array(
              'discount_wise' => $d_wise->discount_data['discount_wise'],
              'product_id'    => $d_wise->discount_data['product_id'] ,
              'category_id'    => $d_wise->discount_data['category_id'],
              'brand_id'    => $d_wise->discount_data['brand_id'],
              'coupon'    => $d_wise->discount_data['coupon'],
              'reusable_limit'    => $d_wise->discount_data['reusable_limit'],
              'purchase_amount' => $d_wise->discount_data['purchase_amount'],
              'purchase_amount_max' => $d_wise->discount_data['purchase_amount_max'],
              'discount_value' => $d_wise->discount_data['discount_value'],
              'fixed_discount_price' => $d_wise->discount_data['fixed_discount_price'],
              'discount_type' => $d_wise->discount_data['discount_type'],
              'parent_id'    => $d_wise->discount_data['parent_id']
              );
              $cartdata['coupon_detail'] = $coupon_detail;
              
    }
    else
    {
       
         $avail_for ="";
         $avail_msg = "";
         if($user_type == "consumer")
         {
             $avail_for = "retail";
             $avail_msg = "Retailer";
         }
         else if($user_type == "retail")
         {
             $avail_for = "consumer";
             $avail_msg ="Consumer";
         }
         $param = array ('user_type' => $avail_for, 'coupon' => $cpn_code);
         $discount_request = api_url().'checkoutapi/discount_detail/format/json/';
         $d_wise = curlpost($param,$discount_request);
         if($d_wise->discount_data)
         {
              $cartdata['msg'] = "<span style='color:green;'>This offer is available for ".$avail_msg."</span>";
              $cartdata['allow_discount'] = "false";
              $this->session->set_userdata('discount_amount',"0.0");
              $this->session->set_userdata('applied_coupon',false);
              $this->session->set_userdata('allow_discount', false);
              $this->session->set_userdata('discount_amount',false);
              //echo json_encode($cartdata);
              //die;
             
         }
    }
    //pend($coupon_detail);
    if($cpn_match_flag)
    {
        
        $param = array ('UserId' => $user_id, 'CouponCode' => $cpn_code);
        $discount_request = api_url().'checkoutapi/discount_usage_detail/format/json/';
        $cpn_expenses = curlpost($param,$discount_request);
        if(($cpn_expenses->result1['numrows'] + $cpn_expenses->result2['numrows']) <= $coupon_detail['reusable_limit'])
        {
             
             if($coupon_detail['discount_wise'] == 'products')
             {
                    $product_array = unserialize($coupon_detail['product_id']);
                    $filter_discount_product = array();
                    $i = 0 ;
                    $subtotal_for_discount  = 0;
                    foreach($product_array as $key => $value) 
                    {
                           foreach($cart_contents as $key2 => $value2)
                           {
                              if($value['promapid'] == $value2['id'] && $value['proid'] == $value2['proid'])
                              {
                                  $filter_discount_product[$i++] = array(
                                      'promapid' => $value2['id'],
                                      'proid' => $value2['proid'],
                                      'subtotal' => $value2['subtotal']
                                      );
                                     $amt_qualify_msg .= $value2['name'].", ";
                                    $subtotal_for_discount += $value2['subtotal'];
                              }
                           }
                      } // bhawna
                      if(($subtotal_for_discount>=$coupon_detail['purchase_amount']) && ($subtotal_for_discount<$coupon_detail['purchase_amount_max']))
                      {
                          if($coupon_detail['discount_type'] == 'fixed')
                          {
                              $final_discount_amount = $coupon_detail['discount_value'];
                              $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                              //$final_discount_amount = ($subtotal_for_discount/$coupon_detail['purchase_amount']) * $coupon_detail['discount_value'];
                              //$cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                          }
                          else if($coupon_detail['discount_type'] == 'percentage')
                          {
                                //$times = ($subtotal_for_discount/$coupon_detail['purchase_amount']);// * $coupon_detail['discount_value'];
                                //$cartdata['Test'] = $times; //testing code 
                                //$final_discount_amount = (($coupon_detail['purchase_amount'] * $coupon_detail['discount_value'])/100);
                                $final_discount_amount = (($subtotal_for_discount * $coupon_detail['discount_value'])/100);
                                $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                          }  
                          $final_discount_amount = number_format($final_discount_amount,1,".","");
                      } else if($subtotal_for_discount>$coupon_detail['purchase_amount_max']) {

                          $final_discount_amount = $coupon_detail['fixed_discount_price'];
                          $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                      } else {

                                $amt_qualify_discount = $coupon_detail['purchase_amount'] - $subtotal_for_discount;
                                if($subtotal_for_discount > 0)  
                                  //$cartdata['msg'] = "Add Rs.".number_format($amt_qualify_discount,1,".","")." more into <span style = 'color:green';>".rtrim($amt_qualify_msg,', ')." </span> to avail discount";
                                  $cartdata['msg'] = "This coupon code can be availed for subtotal cart amount between Rs. ".number_format($coupon_detail['purchase_amount'],1,".","")." and Rs. ".number_format($coupon_detail['purchase_amount_max'],1,".","")."";
                                else
                                   $cartdata['msg'] = "Coupon Discount is not applicable.";

                                $cartdata['allow_discount'] = "false";
                                $cartdata['cart_contents'] = $cart_contents;
                                $this->session->set_userdata('discount_amount',"0.0");
                                $this->session->set_userdata('applied_coupon',false);
                                $this->session->set_userdata('allow_discount', false);
                                $this->session->set_userdata('discount_amount',false);
                                //echo json_encode($cartdata);
                                //die; 
                          }

              }
             else if($coupon_detail['discount_wise'] == 'brands')
             {
                  $brand_list = explode(',', $coupon_detail['brand_id']);

                  $filter_discount_product = array();
                  $subtotal_for_discount = 0.0;
                  $k = 0; 
                  for($i = 0; $i < count($brand_list); $i++)
                  {
                     foreach($cart_contents as $key => $value)
                     {
                        if($brand_list[$i] == $value['brandid'])
                        {
                            $filter_discount_product[$k++] = array(
                              'brandid' => $value['brandid'],
                              'subtotal' => $value['subtotal']
                            );
                            $subtotal_for_discount += $value['subtotal'];
                            $amt_qualify_msg .= $value['name'].", ";
                        }
                     }

                  }
                  if(($subtotal_for_discount>=$coupon_detail['purchase_amount']) && ($subtotal_for_discount<$coupon_detail['purchase_amount_max']))
                  {

                      if($coupon_detail['discount_type'] == 'fixed')
                      {
                          $final_discount_amount = $coupon_detail['discount_value'];
                          $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                          //$final_discount_amount = ($subtotal_for_discount/$coupon_detail['purchase_amount']) * $coupon_detail['discount_value'];
                          //$cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                        //p($final_discount_amount); die;
                      }
                      else if($coupon_detail['discount_type'] == 'percentage')
                      {
                          //$times = ($subtotal_for_discount/$coupon_detail['purchase_amount']);// * $coupon_detail['discount_value'];
                          //$cartdata['Test'] = $times; //testing code 
                          //$final_discount_amount = (($coupon_detail['purchase_amount'] * $coupon_detail['discount_value'])/100);
                          $final_discount_amount = (($subtotal_for_discount * $coupon_detail['discount_value'])/100);
                          $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                      }  
                        
                      
                      $final_discount_amount = number_format($final_discount_amount,1,".","");  
                      //$cartdata['DM'] = $filter_discount_product; //testing code 
                    //print_r($final_discount_amount); die;
                  } else if($subtotal_for_discount>$coupon_detail['purchase_amount_max']) {

                        $final_discount_amount = $coupon_detail['fixed_discount_price'];
                        $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                  } else {

                          $amt_qualify_discount = $coupon_detail['purchase_amount'] - $subtotal_for_discount;
                          if($subtotal_for_discount > 0)  
                              //$cartdata['msg'] = "Add Rs.".number_format($amt_qualify_discount,1,".","")." more into <span style = 'color:green';>".rtrim($amt_qualify_msg,', ')." </span> to avail discount";
                              $cartdata['msg'] = "This coupon code can be availed for subtotal cart amount between Rs. ".number_format($coupon_detail['purchase_amount'],1,".","")." and Rs. ".number_format($coupon_detail['purchase_amount_max'],1,".","")."";
                          else
                                $cartdata['msg'] = "Coupon Discount is not applicable.";
                          

                          $cartdata['allow_discount'] = "false";
                          $cartdata['cart_contents'] = $cart_contents;
                          $this->session->set_userdata('discount_amount',"0.0");
                          $this->session->set_userdata('applied_coupon',false);
                          $this->session->set_userdata('allow_discount', false);
                          $this->session->set_userdata('discount_amount',false);
                          //echo json_encode($cartdata);
                          //die; 
                  }



               }
             else if($coupon_detail['discount_wise'] == 'category')
             {
                  
                  $category_id = explode(',', $coupon_detail['category_id']);
                  $parent_id = $coupon_detail['parent_id'];
                  $filter_discount_product = array();
                  $subtotal_for_discount = 0.0;
                   $k = 0; 
                  for($i = 0; $i < count($category_id); $i++)
                  {
                     foreach($cart_contents as $key => $value)
                     {
                        if($category_id[$i] == $value['catid'])
                        {
                            $filter_discount_product[$k++] = array(
                              'catid' => $value['catid'],
                              'subtotal' => $value['subtotal']
                            );
                            $subtotal_for_discount += $value['subtotal'];
                            $amt_qualify_msg .= $value['name'].", ";
                        }
                     }
                  }
                //p($this->session->userdata('purchase_amount')); die;

                 if(($subtotal_for_discount>=$coupon_detail['purchase_amount']) && ($subtotal_for_discount<$coupon_detail['purchase_amount_max']))
                  {
                      //print_r($this->session->userdata('purchase_amount'));    
                      if($coupon_detail['discount_type'] == 'fixed')
                      {
                          
                          $final_discount_amount = $coupon_detail['discount_value'];
                          $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                          //$final_discount_amount = ($subtotal_for_discount/$coupon_detail['purchase_amount']) * $coupon_detail['discount_value'];
                          //$cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                      }
                      else if($coupon_detail['discount_type'] == 'percentage')
                      {
                          //$times = ($subtotal_for_discount/$coupon_detail['purchase_amount']);// * $coupon_detail['discount_value'];
                          //$cartdata['Test'] = $times; //testing code 
                          //$final_discount_amount = (($coupon_detail['purchase_amount'] * $coupon_detail['discount_value'])/100);
                          $final_discount_amount = (($subtotal_for_discount * $coupon_detail['discount_value'])/100);
                          $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                      }  
                      $final_discount_amount = number_format($final_discount_amount,1,".","");  
                       
                  } else if($subtotal_for_discount>$coupon_detail['purchase_amount_max']) {

                        $final_discount_amount = $coupon_detail['fixed_discount_price'];
                        $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                  } else {

                        $amt_qualify_discount = $coupon_detail['purchase_amount'] - $subtotal_for_discount;
                        if($subtotal_for_discount > 0)  
                              //$cartdata['msg'] = "Add Rs.".number_format($amt_qualify_discount,1,".","")." more into <span style = 'color:green';>".rtrim($amt_qualify_msg,', ')." </span> to avail discount";
                              $cartdata['msg'] = "This coupon code can be availed for subtotal cart amount between Rs. ".number_format($coupon_detail['purchase_amount'],1,".","")." and Rs. ".number_format($coupon_detail['purchase_amount_max'],1,".","")."";
                        else
                              $cartdata['msg'] = "Coupon Discount is not applicable.";

                        
                        $cartdata['allow_discount'] = "false";
                        $cartdata['cart_contents'] = $cart_contents;
                        $this->session->set_userdata('discount_amount',"0.0");
                        $this->session->set_userdata('applied_coupon',false);
                        $this->session->set_userdata('allow_discount', false);
                        $this->session->set_userdata('discount_amount',false);
                        //echo json_encode($cartdata);
                        //die; 
                }

             }
             else if($coupon_detail['discount_wise'] == 'cart')
             {
                  $subtotal_for_discount = 0.0;
                  foreach($cart_contents as $key => $value)
                  {
                    $subtotal_for_discount += $value['subtotal'];
                  }
                  if(($subtotal_for_discount>=$coupon_detail['purchase_amount']) && ($subtotal_for_discount<$coupon_detail['purchase_amount_max']))
                  {
                      if($coupon_detail['discount_type'] == 'fixed')
                      {
                          $final_discount_amount = $coupon_detail['discount_value'];
                          $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                          //$final_discount_amount = ($subtotal_for_discount/$coupon_detail['purchase_amount']) * $coupon_detail['discount_value'];
                          
                      }
                      else if($coupon_detail['discount_type'] == 'percentage')
                      {
                        // $times = ($subtotal_for_discount/$coupon_detail['purchase_amount']);// * $coupon_detail['discount_value'];
                        //$cartdata['Test'] = $times; //testing code 
                        //$final_discount_amount = (($coupon_detail['purchase_amount'] * $coupon_detail['discount_value'])/100);
                        $final_discount_amount = (($subtotal_for_discount * $coupon_detail['discount_value'])/100);
                        $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                    }  
                    $final_discount_amount = number_format($final_discount_amount,1,".",""); 
                  } else if($subtotal_for_discount>$coupon_detail['purchase_amount_max']) {

                        $final_discount_amount = $coupon_detail['fixed_discount_price'];
                        $cartdata['subtotal_for_discount'] = $subtotal_for_discount;
                  } else {

                        $amt_qualify_discount = $coupon_detail['purchase_amount'] - $subtotal_for_discount;
                        //$cartdata['msg'] = "Add Rs.".number_format($amt_qualify_discount,1,".","")." more into Cart to avail discount";
                        $cartdata['msg'] = "This coupon code can be availed for subtotal cart amount between Rs. ".number_format($coupon_detail['purchase_amount'],1,".","")." and Rs. ".number_format($coupon_detail['purchase_amount_max'],1,".","")."";
                        $cartdata['allow_discount'] = "false";
                        $cartdata['cart_contents'] = $cart_contents;
                        $this->session->set_userdata('discount_amount',"0.0");
                        $this->session->set_userdata('applied_coupon',false);
                        $this->session->set_userdata('allow_discount', false);
                        $this->session->set_userdata('discount_amount',false);
                        //echo json_encode($cartdata);
                        //die; 
                  }
             

             }
             
          }
           else{
                    $cartdata['msg'] = "You reached maximum limit";
                    $cartdata['allow_discount'] = "false";
                    $cartdata['coupon_expenses'] = $cpn_expenses;
                    $this->session->set_userdata('applied_coupon',false);
                    $this->session->set_userdata('allow_discount', false);
                    $this->session->set_userdata('discount_amount',false);
                }
        } 
        else 
        {
            
            /*$cartdata['msg'] = "Coupon Discount is not applicable!";
            $cartdata['allow_discount'] = "false";
            $cartdata['coupon_expenses'] = $cpn_expenses;
            $this->session->set_userdata('applied_coupon',false);
            $this->session->set_userdata('allow_discount', false);
            $this->session->set_userdata('discount_amount',false); */
            
        }
       if($final_discount_amount <= 0)
       {
                $final_discount_amount = "0.0";
                //$cartdata['msg'] = "Coupon Discount is not applicable.";

                $cartdata['allow_discount'] = "false";
                $this->session->set_userdata('applied_coupon',false);
                $this->session->set_userdata('allow_discount', false);
                $this->session->set_userdata('discount_amount',false);
                
       }
       else
       {
             $cartdata['allow_discount'] = "true";
             $cartdata['msg']  = "Coupon has been applied!";
             $this->session->set_userdata('applied_coupon',true);
             $this->session->set_userdata('allow_discount', true);
             $this->session->set_userdata('discount_amount',$final_discount_amount);
             $this->session->set_userdata('discount_coupon',$cpn_code);
       }

              // print_r(json_encode(array('msg' => "Coupon has been applied", 'coupon_expenses' => $cpn_expenses, 'coupon_detail' => $coupon_detail)));
             //die;
            //-----------------------------
               
            //------------ Tax calculate --------------------
                $cstid=$this->session->userdata('bizzgainmember')->cstid;
                $tinid=$this->session->userdata('bizzgainmember')->tinid;
                $param =  $this->cart->contents();
                $total = count($param);
              foreach ($param as $key => $value) 
              {
                    $bid=$value['brandid'];
                    $cid=$value['catid'];
                    $subtotal=$value['subtotal'];
                    $stateid=$this->session->userdata('bizzgainmember')->stateid;
                    $taxpath = api_url().'checkoutapi/vattax/brandid/'.$bid.'/catid/'.$cid.'/stateid/'.$stateid.'/format/json/';

                $record['viewtex'] = curlget($taxpath); 

                foreach ($record['viewtex'] as $key => $valtax) 
                {  
                    if($cstid== $valtax['taxidd']) //2,4 CST
                    { 
                       $cst_name=$valtax['taxname'];
                       $cst_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                       $cpt[$value['proid']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                       $sub+=number_format($subtotal,1,".","");
                    }
                    if($tinid== $valtax['taxidd']) //1,6 TIN
                    { 
                       $tin_name=$valtax['taxname'];
                       $tin_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                       $tpt[$value['proid']]=number_format(($subtotal)*($valtax['taxprice'])/100.1,".","");

                    }
            
                  if($valtax['taxidd']==5)  //entry
                  { 
                       $entry_name=$valtax['taxname'];
                       $entry_value+=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                       $ept[$value['proid']]=number_format(($subtotal)*($valtax['taxprice'])/100,1,".","");
                  }
                }  // end foreach
                  //p($record['viewtex']);
              }
   
               $tax['cstprowisetax']=$cpt;
               $tax['tinprowisetax']=$tpt;
               $tax['entryprowisetax']=$ept;

                $grandtotal=$cst_value+$tin_value+$entry_value+$sub;
                //echo $cst_name.'**'.$cst_value.'##'.$tin_name.'**'.$tin_value.'##'.$entry_name.'**'.$entry_value.'##'.$sub;
 
                $tax['cart_tax'] = array(
                       'tax_cst'  => $cst_value,
                       'tax_tin'  => $tin_value,
                       'tax_entry'  => $entry_value,
                       'tax_total'  => $cst_value+$tin_value+$entry_value,
                       'shippcharge' => tshippingcharge(0)
                    );
  
                $this->session->set_userdata($tax);
                $this->session->set_userdata('discount_amount',$final_discount_amount);

              //------------ Tax calculate --------------------
                  $cartdata['grandTotal']  =  number_format($this->cart->total(),1,".",""); 
                  $cartdata['shippcharge'] = number_format(tshippingcharge($this->cart->total()),1,".","");
                  $cartdata['cpn_discount'] =  $final_discount_amount;
                  //$cartdata['grandTotalwithdilabery'] = number_format(Totalvalincart($cartdata['grandTotal'],$cartdata['shippcharge']),1,".","");  
                  $cartdata['grandTotalwithdilabery'] = Totalvalincart($this->cart->total(),$cartdata['shippcharge'])+$tax['cart_tax']['tax_total'] - $cartdata['cpn_discount'];
                  $cartdata['proqty'] = $proqty;
                  $cartdata['unitprice'] = number_format($newfinalPrice,1,".","");
                  $cartdata['totalAmtPerProduct'] = number_format($proqty*$cartdata['unitprice'],1,".",""); 
                  // p($cartdata['totalAmtPerProduct']);exit;
                  $cartdata['tax']=$tax['cart_tax'];
                  $cartdata['TotalInCart']   =  count($this->cart->contents()); 
                  $cartdata['cart_contents'] = $cart_contents;
                  //$cartdata['allow_discount'] = "true";
                  //$cartdata['msg']  = "Coupon has been applied!";
                  //$this->session->set_userdata('applied_coupon',true);
                  echo json_encode($cartdata);
                  die;
    } 

  public function app_paycitrus(){
    set_include_path(FCPATH.'citrus/lib'.PATH_SEPARATOR.get_include_path());
require_once (FCPATH.'citrus/lib/CitrusPay.php');
require_once (FCPATH.'citrus/lib/Zend/Crypt/Hmac.php');
   function generateHmacKey($data, $apiKey=null){
        $hmackey = Zend_Crypt_Hmac::compute($apiKey, "sha1", $data);
        return $hmackey;
    } 
    //Replace this with your secret key from the citrus panel
    $secret_key = "0305b1890601f39aa5cfe4605ded01b26e55aa09";
    CitrusPay::setApiKey($secret_key,'citrus');
    $vanityUrl     = '50anfws9m0';
    $currency      = 'INR';
    $merchantTxnId = uniqid();//Should be OrderId
    $orderAmount   = $this->input->post('amount');
    $tmpdata       = "$vanityUrl$orderAmount$merchantTxnId$currency";
    
    $secSignature  = generateHmacKey($tmpdata,CitrusPay::getApiKey());
    $action        = CitrusPay::getCPBase()."$vanityUrl"; 
    $payment_url   = "https://sandbox.citruspay.com/sslperf/your-vanityUrlPart";

    $accessKey     = '3FBU3FX3I0NYC3O5G2NE';
    $reqtime       = time()*1000;

    $data['citrus'] = array(

        'action'=>$action,     
        'merchantAccessKey' => $accessKey,  
        'merchantTxnId' => $merchantTxnId,
        'addressState' =>  $this->input->post('state'),
        'addressCity' => $this->input->post('city'),
        'addressStreet1' => $this->input->post('address1'),
        'addressCountry' => $this->input->post('country'),
        'addressZip' => $this->input->post('zipcode'),
        'firstName' => $this->input->post('firstname'),
        'lastName' => $this->input->post('lastname'),
        'phoneNumber' => $this->input->post('phone'),
        'email' => $this->input->post('email'),
        'returnUrl' => $config['base_url'].'webapp/checkout/app_paycitruscheck',
        'notifyUrl' => $config['base_url'].'webapp/checkout/app_paycitruscheck',
        'orderAmount' => $orderAmount,
        'reqtime' => $reqtime,
        'secSignature' => $secSignature ,
        'currency' => 'INR'
       
    );
    $this->load->view('citrus',$data);
  }

  public function app_paycitruscheck(){
    if($_REQUEST['TxStatus']==='SUCCESS'){ 
        redirect(base_url().'webapp/checkout/app_citrussuccess');
    } else {
        redirect(base_url().'webapp/checkout/app_citrusfail');
    }
  }
  
  public function app_citrussuccess(){
    echo "paysuccess";
  }
  
  public function app_citrusfail(){
    echo "payfail";
  }
  
}//end of class
?>