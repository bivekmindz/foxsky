<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ccavenue extends MX_Controller {

  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
    $this->load->helper("my_checkout_helper");
  }

  public function randnumber(){
    srand ((double) microtime() * 1000000);
    $random5 = rand(10000,99999);
    return $random5;
  }

 public function codpayment(){
   $shipping_id = $this->input->post('delivery_id1');
    $parameter = array('act_mode' =>'ccavenue_shipping_details',
                       'row_id'  => $shipping_id,
                       'Param3'  =>'',
                       'Param4'  =>'',
                       'Param5'  =>'',
                       'Param6'  =>'',
                       'Param7'  =>'');
    
    $shippingDetails = $this->supper_admin->call_procedureRow('proc_getCCAvenue',$parameter);

    $UserloginDetail = $this->session->userdata('foxsky');
    $paymentMod     = 'COD';
    $totalamount    = $this->cart->total();
    $userIpaddress  = $_SERVER['REMOTE_ADDR'];
   // p($this->session->userdata['discount_amount']);
   // pend(  $this->session->userdata);
   //pend($shippingDetails);     
  $parameter = array('act_mode'      => 'insert_order',
                        'row_id'        => $shipping_id,
                        'UserId'        => $shippingDetails->userregid,
                        'ShippingName'     => $shippingDetails->shipname,
                        'BillName' => $shippingDetails->billname,
                        'ShipLastName'   => '',
                        'ContactNo'   => $shippingDetails->shipcontact,
                        'BillContactNo'    => $shippingDetails->billcontactno,
                        'Email'    => $shippingDetails->shipemailid,
                        'BillEmail' => $shippingDetails->billemailid,
                        'ShippingAddress' => $shippingDetails->shipaddress,
                        'BillAddress'  => $shippingDetails->billaddress,
                        'City'         =>  $shippingDetails->shipcity,
                        'BillCity'     => $shippingDetails->billcity,
                        'State'        => $shippingDetails->statename,
                        'BillState'      => $shippingDetails->statename,
                        'Pin'     => $shippingDetails->shippincode,
                        'BillPin'      => $shippingDetails->billpincode,
                        'subtotal'        => $totalamount,
                        'TotalAmt'        => $this->session->userdata['discount_amount']?($totalamount-$this->session->userdata['discount_amount']):$totalamount,
                        'OrderStatus'      => 'pending',
                        'PaymentMode' => 'COD',  
                        'PaymentStatus'      => 'pending',
                        'ShippingStatus'     => 'pending',
                        'ModifiedBy'      => $UserloginDetail->id);
       
        $insertProView = $this->supper_admin->call_procedureRow('proc_shippingadduser',$parameter);

        $parameter = array('act_mode'      => 'update_order_number',
                        'row_id'        => 'ORD'.$insertProView->OrderId,
                        'UserId'        => $insertProView->OrderId,
                        'ShippingName'     => $this->session->userdata['discount_amount'],
                        'BillName' =>  $this->session->userdata['discount_coupon'],
                        'ShipLastName'   => $this->session->userdata['discount_id'],
                        'ContactNo'   => '',
                        'BillContactNo'    => '',
                        'Email'    => '',
                        'BillEmail' => '',
                        'ShippingAddress' => '',
                        'BillAddress'  => '',
                        'City'         =>  '',
                        'BillCity'     => '',
                        'State'        => '',
                        'BillState'      => '',
                        'Pin'     => '',
                        'BillPin'      => '',
                        'subtotal'        => '',
                        'TotalAmt'        => '',
                        'OrderStatus'      => '',
                        'PaymentMode' => '',  
                        'PaymentStatus'      => '',
                        'ShippingStatus'     => '',
                        'ModifiedBy'      => '');
        
        $this->supper_admin->call_procedureRow('proc_shippingadduser',$parameter);
if($insertProView->OrderId > 0){
          $showfornow = $this->insertIntoOrderMap($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod);
        }
        // echo $insertProView->OrderId;


  $path = api_url().'productlisting/ordermail/orderid/'.$insertProView->OrderId.'/format/json/';
         $userinfo["mailview"] =(array) curlget($path);

  $hmtopemail="ankit@mindztechnology.com";
            
          $emailid= $shippingDetails->shipemailid;
              $msg = $this->load->view('bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($hmtopemail, 'Foxsky.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$insertProView->OrderId.')');
              $this->email->message($msg);
$this->email->send();

        if($insertProView->OrderId > 0){
            $smsphnon = $shippingDetails->shipcontact;
            $smsname  = $shippingDetails->shipname;
             
            $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9711007307, if you need help.";
              
            $smsmsgg = urlencode('Hi '.$smsname.' your order '.$insertProView->OrderId.' '.$smsmsg);
            $file=file_get_contents('http://nimbusit.co.in/api/swsendSingle.asp?username=t1foxskyindia&password=53499390&sender=Foxsky&sendto='.$smsphnon.'&message='.$smsmsgg);
?>
  <script type="text/javascript">
        var order_id = '<?php echo $insertProView->OrderId ;?>';
        window.location = "http://115.124.98.243/~foxskyindia/success/"+order_id+"/ORD"+order_id;
        </script>
  <?php
        }

   }

  public function ccrequesthander(){
    $shipping_id = $this->input->post('delivery_id');
    $parameter = array('act_mode' =>'ccavenue_shipping_details',
                       'row_id'  => $shipping_id,
                       'Param3'  =>'',
                       'Param4'  =>'',
                       'Param5'  =>'',
                       'Param6'  =>'',
                       'Param7'  =>'');
    
    $shippingDetails = $this->supper_admin->call_procedureRow('proc_getCCAvenue',$parameter);
   
    $UserloginDetail = $this->session->userdata('foxsky');
    $paymentMod     = 'ccavenue';
    $totalamount    = $this->cart->total();
    $userIpaddress  = $_SERVER['REMOTE_ADDR'];
   // p($this->session->userdata['discount_amount']);
   // pend(  $this->session->userdata);
   // pend($shippingDetails);      
    $parameter = array('act_mode'      => 'insert_order',
                        'row_id'        => $shipping_id,
                        'UserId'        => $shippingDetails->userregid,
                        'ShippingName'     => $shippingDetails->shipname,
                        'BillName' => $shippingDetails->billname,
                        'ShipLastName'   => '',
                        'ContactNo'   => $shippingDetails->shipcontact,
                        'BillContactNo'    => $shippingDetails->billcontactno,
                        'Email'    => $shippingDetails->shipemailid,
                        'BillEmail' => $shippingDetails->billemailid,
                        'ShippingAddress' => $shippingDetails->shipaddress,
                        'BillAddress'  => $shippingDetails->billaddress,
                        'City'         =>  $shippingDetails->shipcity,
                        'BillCity'     => $shippingDetails->billcity,
                        'State'        => $shippingDetails->statename,
                        'BillState'      => $shippingDetails->statename,
                        'Pin'     => $shippingDetails->shippincode,
                        'BillPin'      => $shippingDetails->billpincode,
                        'subtotal'        => $totalamount,
                        'TotalAmt'        => $this->session->userdata['discount_amount']?($totalamount-$this->session->userdata['discount_amount']):$totalamount,
                        'OrderStatus'      => '',
                        'PaymentMode' => 'CCAVENUES',  
                        'PaymentStatus'      => '',
                        'ShippingStatus'     => 'pending',
                        'ModifiedBy'      => $UserloginDetail->id);
        

        $insertProView = $this->supper_admin->call_procedureRow('proc_shippingadduser',$parameter);

        $parameter = array('act_mode'      => 'update_order_number',
                        'row_id'        => 'ORD'.$insertProView->OrderId,
                        'UserId'        => $insertProView->OrderId,
                        'ShippingName'     => $this->session->userdata['discount_amount'],
                        'BillName' =>  $this->session->userdata['discount_coupon'],
                        'ShipLastName'   => $this->session->userdata['discount_id'],
                        'ContactNo'   => '',
                        'BillContactNo'    => '',
                        'Email'    => '',
                        'BillEmail' => '',
                        'ShippingAddress' => '',
                        'BillAddress'  => '',
                        'City'         =>  '',
                        'BillCity'     => '',
                        'State'        => '',
                        'BillState'      => '',
                        'Pin'     => '',
                        'BillPin'      => '',
                        'subtotal'        => '',
                        'TotalAmt'        => '',
                        'OrderStatus'      => '',
                        'PaymentMode' => '',  
                        'PaymentStatus'      => '',
                        'ShippingStatus'     => '',
                        'ModifiedBy'      => '');
        
        $this->supper_admin->call_procedureRow('proc_shippingadduser',$parameter);

        if($insertProView->OrderId > 0){
          $showfornow = $this->insertIntoOrderMap($insertProView->OrderId,$insertProView->OrderNumber,$paymentMod);
        }
        
        if($insertProView->OrderId > 0){
          //------------email--------------
          $tid = rand(10000,10000000000);
          $dataCCAvenue = array(
                    'tid' => $tid,
                    'merchant_id' => '168753',
                    'order_id' => $insertProView->OrderId,
                    'amount' => $totalamount,
                    'currency' => 'INR',
                    'redirect_url' => base_url().'webapp/ccavenue/ccavresponsehandler',
                    'cancel_url' => base_url().'webapp/ccavenue/ccavresponsehandler',
                   'language' => 'EN',
                    'billing_name' => $shippingDetails->billname,
                    'billing_address' => $shippingDetails->billaddress,
                    'billing_city' => $shippingDetails->billcity,
                    'billing_state' => $shippingDetails->shipstate,
                    'billing_zip' => $shippingDetails->billpincode,
                    'billing_country' => 'India',
                    'billing_tel' => $shippingDetails->billcontactno,
                    'billing_email' => $shippingDetails->billemailid,
                    'delivery_name' => $shippingDetails->shipname,
                    'delivery_address' => $shippingDetails->shipaddress,
                    'delivery_city' => $shippingDetails->billcity,
                    'delivery_state' => $shippingDetails->billstate,
                    'delivery_zip' => $shippingDetails->shippincode,
                    'delivery_country' => 'India',
                    'delivery_tel' => $shippingDetails->shipcontact,
                    'merchant_param1' => 'additional Info.',
                    'merchant_param2' => 'additional Info.',
                    'merchant_param3' => 'additional Info.',
                    'merchant_param4' => 'additional Info.',
                    'merchant_param5' => 'additional Info.',
                    'emi_plan_id' => '',
                    'emi_tenure_id' => '',
                    'card_type' => '',
                     'card_name' => '',
                    'data_accept' => '',
                    'card_number' => '',
                     'expiry_month' => '',
                    'expiry_year' => '',
                    'cvv_number' => '',
                      'cvv_number' => '',
                    'issuing_bank' => '',
                    'mobile_number' => '',
                     'mm_id' => '',
                    'otp' => '',
                    'promo_code' => ''); 
        // p($shippingDetails);
        // pend($dataCCAvenue);
        // redirect('webapp/checkout/ccrequesthanderdata');
        $this->ccrequesthanderdata($dataCCAvenue);
      }
  }

  public function insertIntoOrderMap($orderid,$ordernumber,$paymentmod){
      $x = $this->cart->contents();
      foreach($x as $key => $val){
      //  pend($val);
          $parameter = array(
                         'act_mode' => 'insert_order_map',
                         'Param1'   => $orderid,
                         'Param2'   => $val['id'],
                         'Param3'   => $val['qty'],
                         'Param4'   => '',
                         'Param5'   => $val['name'],
                         'Param6'   => '',
                         'Param7'   => '',
                         'Param8'   => $val['price'],
                         'Param9'   => $val['price']*$val['qty'],
                         'Param10'   => $val['image'],
                         'Param11'   => $val['categoryid']);
          $this->supper_admin->call_procedureRow('proc_addorder_map',$parameter);
      }
      return;
  }

  public function ccrequesthanderdata($dataCCAvenue){
    
     $merchant_data = '';
    $working_key = '9F64809F17BAF7D02736BE6E8AC7B4CB';//Shared by CCAVENUES
    $access_code = 'AVDD77FD61BW94DDWB';//Shared by CCAVENUES
    
    foreach ($dataCCAvenue as $key => $value){
      $merchant_data.=$key.'='.urlencode($value).'&';
    }
    
   $encrypted_data=encrypta($merchant_data,$working_key); // Method for encrypting the data.
    
    ?>
    <form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> 
    <?php
    echo "<input type=hidden name=encRequest value=$encrypted_data>";
    echo "<input type=hidden name=access_code value=$access_code>";
    ?>
    </form>
    <script language='javascript'>document.redirect.submit();</script>
  
 <?php }
  
   public function ccavresponsehandler(){
    
        error_reporting(0);
      
      $workingKey='9F64809F17BAF7D02736BE6E8AC7B4CB';   //Working Key should be provided here.
      $encResponse=$_POST["encResp"];     //This is the response sent by the CCAvenue Server
      $rcvdString=decrypta($encResponse,$workingKey);   //Crypto Decryption used as per the specified working key.
      $order_status="";
      $decryptValues=explode('&', $rcvdString);
      $dataSize=sizeof($decryptValues);
      echo "<center>";
        
      for($i = 0; $i < $dataSize; $i++) 
      {
        $information = explode('=',$decryptValues[$i]);
        if($i==3) $order_status = $information[1];
        if($i==0) $order_id = $information[1];
        
        if($i==1) $cc_tracking_id = $information[1];
            if($i==2) $cc_bank_ref_no = $information[1];
              
            if($i==4) $cc_failure_message = $information[1];
            if($i==5) $cc_payment_mode = $information[1];
              
            if($i==6) $cc_card_name = $information[1];
            if($i==7) $cc_status_code = $information[1];
              
            if($i==8) $cc_status_message = $information[1];
            if($i==9) $cc_currency = $information[1];
              
            if($i==10)  $cc_amount = $information[1];
            
            if($i==19)  $delivery_name = $information[1];
            if($i==25)  $delivery_tel = $information[1];
      }
      
      $parameter = array('act_mode'  =>'update_ccavenue_order_status',
                      'cc_tracking_id'   => $cc_tracking_id,
                      'cc_bank_ref_no' => $cc_bank_ref_no,
                      'cc_failure_message' => $cc_failure_message,
                      'cc_card_name'    => $cc_card_name,
                      'cc_status_code' => $cc_status_code,
                      'cc_status_message'   =>$cc_status_message,
                      'cc_currency'   =>$cc_currency,
                      'cc_amount'    => $cc_amount,
                      'OrderStatus' => $order_status,
                      'order_id'   =>$order_id,
                      'ccavenue_response'   =>$rcvdString);
  
        $res = $this->supper_admin->call_procedureRow('proc_ccavenue_order',$parameter);
        
       
        if($order_status==="Success")
      {
     
          ?>
      <script type="text/javascript">
        var order_id = '<?php echo $order_id?>';
        window.location = "http://115.124.98.243/~foxskyindia/success/"+order_id+"/ORD"+order_id;
        </script>
        <?php
        echo "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";
        
      }
      else if($order_status==="Aborted")
      { 
      $smsphnon = $delivery_tel; 
        $smsname = $delivery_name;
     
      ?>
      <script type="text/javascript">
        var order_id = '<?php echo $order_id?>';
        window.location = "http://115.124.98.243/~foxskyindia/cancel/"+order_id+"/ORD"+order_id;
        </script>
      <?php
        echo "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
      
      }
      else if($order_status==="Failure")
      { 
      $smsphnon = $delivery_tel; 
        $smsname = $delivery_name;
      
      
      
      ?>
      <script type="text/javascript">
        var order_id = '<?php echo $order_id?>';
        window.location = "http://115.124.98.243/~foxskyindia/cancel/"+order_id+"/ORD"+order_id;
        </script>
        <?php
        echo "<br>Thank you for shopping with us.However,the transaction has been declined.";
      }
      else
      { 
        $smsphnon = $delivery_tel; 
        $smsname = $delivery_name;
              
      
      
      ?>
      <script type="text/javascript">
        var order_id = '<?php echo $order_id?>';
        window.location = "http://115.124.98.243/~foxskyindia/cancel/"+order_id+"/ORD"+order_id;
        </script>
        <?php
        echo "<br>Security Error. Illegal access detected";
      }
      echo "<br><br>";
    
      echo "<table cellspacing=4 cellpadding=4>";
      for($i = 0; $i < $dataSize; $i++) 
      {
        $information=explode('=',$decryptValues[$i]);
            echo '<tr><td>'.$information[0].'</td><td>'.$information[1].'</td></tr>';
      }
    
      echo "</table><br>";
      echo "</center>";
    
    }
  
}//end of class
?>