<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Fblogin extends MX_Controller
    {
    public function __construct() 
    {
        $this->load->model("supper_admin");
        $this->load->helper("my_helper");
    }

    public function fbregister() {
        if($this->session->userdata('bizzgainmember')->id){
          redirect(base_url());
        }
        else
        {
          $path = api_url().'userapi/shipstate/format/json/';
          $record['viewshipstate']  = curlget($path);
          $this->load->view("helper/header");
          $this->load->view("helper/nav1");
          $this->load->view("helper/nav2");
          $this->load->view("fbregister",$record);
          $this->load->view("helper/footer");
        }
    }

    public function registernew() {
      $data['mobile']   =$this->input->post('fmem_mobile');
      $data['acctype']  =$this->input->post('facctype');
      $data['shipstate'] =$this->input->post('fmem_shipstate');
      $data['shipcity'] =$this->input->post('fmem_shipcity');
      $data['cstid'] =$this->input->post('fmem_cstid');
      $data['cstvalue'] =$this->input->post('fmem_cstvalue');
      $data['tinid'] =$this->input->post('fmem_tinid');
      $data['tinvalue'] =$this->input->post('fmem_tinvalue');
      $data['compname'] =$this->input->post('fmem_compname');
      $this->session->set_userdata('fbregisterdata',$data);
    }

   public function login()
   {
   //   echo FCPATH; exit;
      require_once FCPATH.'/fb/config.php';
      require_once FCPATH.'/fb/functions.php';
      //destroy facebook session if user clicks reset
    
       
      if(!$fbuser){
        $fbuser = null;
        $loginUrl = $facebook->getLoginUrl(array('redirect_uri'=>$homeurl,'scope'=>$fbPermissions));
        header("Location:".$loginUrl);   
      }else{
      $user_profile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
      $tokenid=$user_profile['id'];
      $fname=$user_profile['first_name'];
      $lname=$user_profile['last_name'];
      $emailid=$user_profile['email'];
      if(@$this->session->userdata('fbregisterdata')) {
          //pend($this->session->userdata('fbregisterdata')['mobile']);
          //===================signup and login==================//
          $mobile = $this->session->userdata('fbregisterdata')['mobile'];
          $pass = rand(111111,999999);
          $acctype = $this->session->userdata('fbregisterdata')['acctype'];
          $shipstate = $this->session->userdata('fbregisterdata')['shipstate'];
          $shipcity = $this->session->userdata('fbregisterdata')['shipcity'];
          $cstid = $this->session->userdata('fbregisterdata')['cstid'];
          $cstvalue = $this->session->userdata('fbregisterdata')['cstvalue'];
          $tinid = $this->session->userdata('fbregisterdata')['tinid'];
          $tinvalue = $this->session->userdata('fbregisterdata')['tinvalue'];
          $compname = $this->session->userdata('fbregisterdata')['compname'];
          $num = rand(111111, 999999); 
          $unicode = $acctype.$num;
          $parameter = array(
              'act_mode' =>'frontmemberinsertfb',
              'row_id'   =>'',
              'ventype'  =>$acctype,
              'vencode'  =>$unicode,
              'vcomp'    =>$compname,
              'vemail'   =>$emailid,
              'vcontact' =>$mobile,
              'vpass'    =>$pass,
              'vname'    =>$fname,
              'vlast'    =>$lname,
              'vcatid'   =>'',
              'vcountryid'=>$tokenid,
              'vstateid' =>$shipstate,
              'vcityid'  =>$shipcity,
              'vaddress' =>'',
              'vpincode' =>'',
              'vpannumber'=>'',
              'vtinnumber'=>'',
              'accontname'=>'',
              'accnumber'=>'',
              'bankname' =>'',
              'branchname'=>'',
              'ifsccode' =>'',
              'neftdetail'=>$cstid,
              'acctype'  =>$cstvalue,
              'cstnumber'=>$tinid,
              'vatnumber'=>$tinvalue,
              'cstidd'    => '',
              'tinidd'    => '',
              'vatcopy'   => '',
              'cstcopy'   => '',
              'chequecopy'=>  '',
              'pancopy'   =>  '');
          //pend( $parameter );
          $memloginapi = api_url().'userapi/memberregister/format/json/';
          $response = curlpost($parameter,$memloginapi);
          //------------------ city group id -----------------------//
          $parameter1=array(
              'act_mode' =>'citygroupid',
              'row_id' =>$response->city,
              'vusername' =>$response->stateid,
              'vpassword'=>'',
              'vstatus' =>'',
              );
          $path1=api_url().'userapi/citygrouplogin/format/json/'; 
          $record=curlpost($parameter1,$path1);
          //-------------------end-----------------------------------//
          if($response->id==001){
              $this->session->set_flashdata('message', 'You are already registered from your facebook account, just click on "LOGIN" button!');
              redirect("fbregister");
          } else {
              $this->session->set_userdata('bizzgainmember', $response);
              $this->session->set_userdata('sesregister', 'registeruser');//for alert message on home page to complete profile
              //----------------- start default city group ---------------------
              if($record->scalar=='Something Went Wrong'){
              $defaultrecord->cmid=3;
              $this->session->set_userdata('logincity',$defaultrecord);
              } else {
              $this->session->set_userdata('logincity',$record);
              }
              //----------------- end default city group ---------------------
              $loginacctype = $this->session->userdata('bizzgainmember')->vendortype;
              //------------email--------------
              $bizzgainemail="support@bizzgain.com";
              $userinfo['userdet']=array('useremail'=>$emailid,'userfname'=>$fname,'userlname'=>$lname,'userpass'=>$pass);
              $msg = $this->load->view('bizzgainmailer/registermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'Bizzgain.com');
              $this->email->to($emailid); 
              $this->email->subject('Your Registration Details - ('.$emailid.')');
              $this->email->message($msg);
              $this->email->send();
              //------------email--------------
              if($loginacctype == "manufacturer"){
              echo "manufacturer";
              }
          }
          $this->session->unset_userdata('fbregisterdata');
          redirect('home');
          //=========================end=========================//
      } else {
          //===================login==================//
          $path = api_url().'userapi/shipstate/format/json/';
          $data['viewshipstate']  = curlget($path);   
          $parameter=array(
          'act_mode' =>'memloginfacebook',
          'row_id' =>$tokenid,
          'vusername' =>$emailid,
          'vpassword'=>'',
          'vstatus' =>'',
          );
          $path=api_url().'userapi/userlogin/format/json/'; 
          $response=curlpost($parameter,$path);
          //------------------ city group id -----------------------//
          $parameter1=array(
          'act_mode' =>'citygroupid',
          'row_id' =>$response->city,
          'vusername' =>$response->stateid,
          'vpassword'=>'',
          'vstatus' =>''
          );
          $path1=api_url().'userapi/citygrouplogin/format/json/'; 
          $record=curlpost($parameter1,$path1);
          //-------------------end-----------------------------------//
          if(!empty($response->id)){
          if($response->vendortype=='consumer')
          {
          $this->session->set_userdata('popupon','on');
          }
          $this->session->set_userdata('bizzgainmember',$response);
          //----------------- start default city group ---------------------
          if($record->scalar=='Something Went Wrong'){
          $defaultrecord->cmid=3;
          $this->session->set_userdata('logincity',$defaultrecord);
          //p($this->session->userdata('logincity'));exit;
          } else {
          $this->session->set_userdata('logincity',$record);
          }
          //----------------- end default city group ---------------------
          //---------------------------create cart data in session start---------------------------
          $responce = curlget(api_url().'productlisting/loadcartdtls/userid/'.$this->session->userdata('bizzgainmember')->id.'/format/json/');
          //p($responce);exit;
          if($responce->scalar!='Something Went Wrong' && !empty($responce) && isset($responce))
          { 
          foreach ($responce as $value) {
          //----------------------------- start combo cart -------------------------------------------   
          //p($value['pro_type']);                       
          if($value['pro_type']=='combo'){
          $Productid = $value['promapid'];
          $qtyvl = $value['qty']; 
          $parameter = array(
          'act_mode'  =>  'cominfo',
          'p_name'  =>  '',
          'p_qty'  =>  '',
          'p_start'  =>  '',
          'p_end'  =>  '',
          'p_ingridients'  => '' ,
          'p_about'  =>  '',
          'p_totalqty'  =>  '',
          'p_comboActPrice'  =>  '',
          'p_disPer'  =>  '',
          'p_comb_sellingprice'  =>  '',
          'p_totalsaving'  =>  '',
          'p_combo_img'   =>  '',
          'p_comboid'   => $Productid,
          'p_cat_id'    =>  '',
          'p_sub_catid' => '',
          'p_product_id'    =>  '',
          'p_product_qty'    =>  '',
          'p_pro_mrp'    =>  '',
          'p_pro_seling'    =>  '',
          'p_pro_combo_price'    =>  ''
          );
          $response = $this->supper_admin->call_procedure('proc_saveCombo', $parameter); 
          $mulcat=array();
          foreach ($response as $key => $value) {
          array_push($mulcat, $value->sub_catid);
          }
          $combomulcat=implode(',', $mulcat);
          $newfinalPrice = '';
          curlget(api_url().'productlisting/update_catdetails/userid/'.$this->session->userdata('bizzgainmember')->id.'/prodmap_id/'.$Productid.'/qty/'.$qtyvl.'/format/json/');
          $newqty   =explode('to', $response[0]->qty);
          $newqty2   =explode('to', $response[0]->qty1);
          $newqty3   =explode('to', $response[0]->qty2);
          if($qtyvl<$newqty[0] ){
          $newfinalPrice = '';
          $newfinalPrice=citybaseprice($response[0]->comboprice,$response[0]->cityvalue);
          } 
          if($qtyvl>=$newqty[0] && $qtyvl<=$newqty[1] ){
          $newfinalPrice = '';
          $newfinalPrice=cityqtyprice($response[0]->comboprice,$response[0]->cityvalue,$response[0]->price);
          }
          if($qtyvl>=$newqty2[0] && $qtyvl<=$newqty2[1] ){
          $newfinalPrice = '';
          $newfinalPrice=cityqtyprice($response[0]->comboprice,$response[0]->cityvalue,$response[0]->price1);
          }
          if($qtyvl>=$newqty3[0] && $qtyvl<='100000' ){
          $newfinalPrice = '';
          $newfinalPrice=cityqtyprice($response[0]->comboprice,$response[0]->cityvalue,$response[0]->price2);
          }
          $cart_dtls = array(
          'id'      => $Productid,
          'qty'     => $qtyvl,
          'price'   => $newfinalPrice,
          'name'    => $response[0]->name,
          'VendorID' => '',
          'vendorName'=>'admin',
          'addiscount'=>$response[0]->p_disPer,
          'options' => '',
          'img'     => base_url().'assets/comboimage/'.$response[0]->combo_img,
          'brandid'  =>$response[0]->brand_id,
          'catid'    =>$combomulcat,
          'prodiscount'=>'',
          'proid'    => $response[0]->ComboId,
          'product_type' => 'combo',
          'proqty1'      =>$response[0]->qty,
          'proqty2'      =>$response[0]->qty1,
          'proqty3'      =>$response[0]->qty2,
          'price1'       =>$response[0]->price,
          'price2'       =>$response[0]->price1,
          'price3'       =>$response[0]->price2,
          'sku'          =>'',
          'cityval'      =>$response[0]->cityvalue,
          'baseprice'    =>$response[0]->comboprice,
          'mrpprice'     =>$response[0]->total_sellingprice,
          'startdate'      =>$response[0]->start_date,
          'enddate'      =>$response[0]->end_date,
          'ingrients'      =>$response[0]->ingridients,
          'about'       =>$response[0]->about,
          'totalqty'       =>$response[0]->totalqty,
          'total_sellingprice'       =>$response[0]->total_sellingprice,
          'p_totalsaving'          =>$response[0]->p_totalsaving,
          'product_qty'      =>$response[0]->product_qty,
          'pro_seling'    =>$response[0]->pro_seling,
          'pro_combo_price'     =>$response[0]->pro_combo_price,
          'proname'     =>$response[0]->proname
          );
          //-------------------------------- end combo cart --------------------------------- 
          } else {
          $Productid = $value['promapid'];
          $qtyvl = $value['qty'];  
          $parameter = array('prodid'=>$Productid);
          $newfinalPrice = '';
          $p_cityid=$this->session->userdata('logincity')->cmid;
          $path = api_url().'productlisting/proAvailCheck/name/'.$Productid.'/cityidd/'.$p_cityid.'/format/json/';
          $response = curlget( $path);
          $newqty   =explode('to', $response->qty1);
          $newqty2   =explode('to', $response->qty2);
          $newqty3   =explode('to', $response->qty3);
          if($qtyvl<$newqty[0] ){
          $newfinalPrice = '';
          $newfinalPrice=citybaseprice($response->FinalPrice,$response->cityvalue);
          } 
          if($qtyvl>=$newqty[0] && $qtyvl<=$newqty[1] ){
          $newfinalPrice = '';
          $newfinalPrice=cityqtyprice($response->FinalPrice,$response->cityvalue,$response->price);
          }
          if($qtyvl>=$newqty2[0] && $qtyvl<=$newqty2[1] ){
          $newfinalPrice = '';
          $newfinalPrice=cityqtyprice($response->FinalPrice,$response->cityvalue,$response->price2);
          }
          if($qtyvl>=$newqty3[0] && $qtyvl<='100000' ){
          $newfinalPrice = '';
          $newfinalPrice=cityqtyprice($response->FinalPrice,$response->cityvalue,$response->price3);
          }
          $coloridd = $value['color'];
          if(!empty($value['size'])){
          $sizeattr = $value['size'];
          }
          else{
          $sizeattr = NULL;
          }
          $getImg = getImgProdetail($response->image);
          $cart_dtls= array(
          'id'      => $response->promapid,
          'qty'     => $qtyvl,
          'price'   => $newfinalPrice,
          'name'    => $response->ProName,
          'VendorID' => $response->manufacturerid,
          'vendorName'=>$response->CompanyName,
          'addiscount'=>$response->AdditionDiscount,
          'options' => array('sizeattr' =>$sizeattr,'color'=>$coloridd),
          'img'     => proImg_url($getImg['0']),
          'brandid'  =>$response->brandId,
          'catid'    =>$response->catid,
          'prodiscount'=>$response->ProDiscount,
          'proid'    => $response->proid,
          'product_type' => 'single',
          'proqty1'      =>$response->qty1,
          'proqty2'      =>$response->qty2,
          'proqty3'      =>$response->qty3,
          'price1'       =>$response->price,
          'price2'       =>$response->price2,
          'price3'       =>$response->price3,
          'sku'          =>$response->sellersku,
          'cityval'      =>$response->cityvalue,
          'baseprice'    =>$response->FinalPrice,
          'mrpprice'     =>$response->productMRP
          );
          } // end else of product type
          $this->cart->insert($cart_dtls);
          }
          }
          //---------------------------create cart data in session start---------------------------          
          redirect('home');
          } else {
            $this->session->set_flashdata('message', 'Invalid login, you need to register first!');
            redirect("fbregister");
          }          
          //====================end===================//
      }

    }     
    echo $output;
  }    
    
}
?>