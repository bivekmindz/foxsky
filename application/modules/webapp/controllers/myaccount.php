<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Myaccount extends MX_Controller {
  public function __construct() {
      $this->load->model("supper_admin");
      $this->load->helper("my_helper");
  }


  public function index()
  {
$user_id = $this->session->userdata('foxsky')->id;
  	$paramm = array(
            'act_mode' =>'get_podercount',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
  // p($paramm );

 	$data['pcount'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);

 	$paramm = array(
            'act_mode' =>'get_dodercount',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
       
             );
  // p($paramm );

 	$data['dcount'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);  

     $user_id = $this->session->userdata('foxsky')->id;
    $paramm = array(
            'act_mode' =>'reviewCount',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
            // p($paramm);
        $data['rcount'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
  
  $user_id = $this->session->userdata('foxsky')->id;
  $paramm = array(
            'act_mode' =>'get_shippingadd',
            'Param1' => $user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>'');

   $data['sadd'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);

	$paramm = array(
            'act_mode' =>'getcust_details',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
            // p($paramm);
	$data['profile'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
 	$this->load->view('helper/head');
    $this->load->view('helper/header');
    // $this->load->view('helper/nav');
  	$this->load->view('myaccount',$data);
  	$this->load->view('helper/footer');
  	$this->load->view('helper/foot');


  }
   
    public function myorder()
  {
$user_id = $this->session->userdata('foxsky')->id;

  	$paramm = array(
            'act_mode' =>'get_product_shipping',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
  // p($paramm );

 	$data['shipping'] = $this->supper_admin->call_procedure('proc_tvoverviewslider',$paramm);
     // p($data['shipping']);exit();

    $paramm = array(
            'act_mode' =>'get_orderdetails',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
  // p($paramm );

 	$data['prodetails'] = $this->supper_admin->call_procedure('proc_tvoverviewslider',$paramm);
  // p($data['prodetails']);die();
 	$paramm = array(
            'act_mode' =>'get_deliveredord',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
  // p($paramm );

 	$data['delivered'] = $this->supper_admin->call_procedure('proc_tvoverviewslider',$paramm);

 	$paramm = array(
            'act_mode' =>'return',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
  // p($paramm );

 	$data['return'] = $this->supper_admin->call_procedure('proc_tvoverviewslider',$paramm);
      // p($data['return']);exit();



     // p($data['delivered']);exit();
  	$this->load->view('helper/head');
    $this->load->view('helper/header');
  	$this->load->view('myorder',$data);
  	$this->load->view('helper/footer');
  	$this->load->view('helper/foot');

  }

 public function myaddress()
  {
     if($this->input->post('submit')){

        $shippingid = $this->input->post('shippingid');
        $shipname = $this->input->post('shipname');
        $shippincode = $this->input->post('shippincode');
        $shipaddress = $this->input->post('address');
        $landmark = $this->input->post('landmark');

        $shipcity = $this->input->post('shipcity');
        $shipstate = $this->input->post('stateid');
        $shipemailid = $this->input->post('shipemailid');
        $shipcontact = $this->input->post('shipcontact');

        $billname = $shipname;
        $billpincode = $shippincode;
        $billaddress = $shipaddress;
        $billcity = $shipcity;
        $billstate = $shipstate;
        $billemailid = $shipemailid;
        $billcontactno = $shipcontact;

       $parameter = array('act_mode' => 'update_shipping_address',
                        'row_id' => base64_decode($shippingid),
                        'Param1' => $shipname,
                        'Param2'  => $billname,
                        'Param3'  => $shipaddress,
                        'Param4'  => $billaddress,
                        'Param5'  => $shipemailid,
                        'Param6'  => $billemailid,
                        'Param7'  => $shippincode,
                        'Param8'  => $billpincode,
                        'Param9'  => $shipcontact,
                        'Param10'  => $billcontactno,
                        'Param11'  => $shipstate,
                        'Param12'  => $billstate,
                        'Param13'  => $shipcity,
                        'Param14'  => $billcity,
                        'Param15'  => $landmark);
        $record = $this->supper_admin->call_procedure('proc_shipping_address',$parameter);
       //  pend($parameter);
       
        $this->session->set_flashdata("message", "Updated successfully.");
       
        redirect('myaddress');
    }
    $parameter = array('act_mode' => 'get_state',
                          'row_id' => '',
                          'catid'  => '');
    $data['state'] = $this->supper_admin->call_procedure('proc_product',$parameter);
      
  	$user_id = $this->session->userdata('foxsky')->id;
  	$paramm = array(
            'act_mode' =>'get_details',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
   // p($paramm );

 	  $data['details'] = $this->supper_admin->call_procedure('proc_tvoverviewslider',$paramm);
 	 // p($data['details']);exit();
  	$this->load->view('helper/head');
    $this->load->view('helper/header');
  	$this->load->view('address',$data);
  	$this->load->view('helper/footer');
  	$this->load->view('helper/foot');
 }

 public function deleteaddress(){
 	$user_id = $this->session->userdata('foxsky')->id;
	if(!empty(($_POST['ship_id']))){

		$paramm = array(
            'act_mode' =>'countaddress',
            'Param1' =>$_POST['ship_id'],
            'Param2' =>$user_id,
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
    // p($paramm ); die;

 	  $result = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
 	  // p(result);
 	  $counts=$result->C; 	
  // p($counts);die();
 	  if($counts>=1){
 	  	//echo 0; die;
 	  	echo "order is shipping stage";
 	  }else{
 	  	$paramm = array(
            'act_mode' =>'deleteShippingAddress',
            'Param1' =>$_POST['ship_id'],
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );

 	  	 // p($paramm ); echo "2222"; die;
   		// p($paramm );
 	  	$result = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
 	  	echo "Successfully Deleted";
 	  }

	}
 	}

	public function myprofile()
	  {
	  	$user_id = $this->session->userdata('foxsky')->id;

	  	if($this->input->post('submit')){
	  		// echo "hiiii";
	  		// die();
  		$name=$this->input->post('name');
  		$gender = $this->input->post('gender');
	  		$param = array(
            'act_mode' =>'insert_details',
            'Param1' =>$name,
            'Param2' =>$gender,
            'Param3' =>$user_id,
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
            // p($paramm);
	  	$data['profiledata'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$param);
	  	}

	  	if($this->input->post('submitpic')){


  		$image = time().str_replace(' ', '', $_FILES["image"]['name']);
   		move_uploaded_file($_FILES["image"]["tmp_name"],"assets/profileimages/".$image);
        

   //


   
/*        echo        $config['upload_path']          = './assets/images/';
                    $config['allowed_types']        = 'gif|jpg|png';


                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());

                      //  $this->load->view('upload_form', $error);
                        print_r($error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());

                       // $this->load->view('upload_success', $data);
                        echo 1;
                }*/
   //     
  		
	  		$paramm = array(
            'act_mode' =>'insert_pic',
            'Param1' =>$image,
            'Param2' =>'',
            'Param3' =>$user_id,
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
           
	  	$data['profilepic'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
	  	}

		$paramm = array(
            'act_mode' =>'getcust_details',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
            // p($paramm);
	  	$data['profile'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
	  	
	  	
		// p($data['profile']);exit();
	 	$this->load->view('helper/head');
	    // $this->load->view('helper/header');
	  	$this->load->view('myprofile',$data);
	    // $this->load->view('helper/footer');
  		// $this->load->view('helper/foot');
 }

	public function security()
	  {
	  	$user_id = $this->session->userdata('foxsky')->id;
	  	if($this->input->post('submit')){
	  	//p($this->session->userdata);
  		$cpassword=$this->input->post('cpassword');
  		$npassword=$this->input->post('npassword');
  		$rpassword=$this->input->post('rpassword');

	  		$param = array(
            'act_mode' =>'reset_pass',
            'Param1' =>base64_encode($cpassword),
            'Param2' =>base64_encode($npassword),
            'Param3' =>$rpassword,
            'Param4' =>$user_id,
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
           // pend($param);
	  	$data['password'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$param);
	  	}


        $paramm = array(
            'act_mode' =>'getcust_details',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
            // p($paramm);
	  	$data['profile'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
 // p($data['profile']);exit();

	  	$this->load->view('security',$data);
	  	
	 }

    public function orderdetails($order_id)
  {
  $order_id = $this->uri->segment(2);
  $pro_id = $this->uri->segment(3);
  $paramm = array(
            'act_mode' =>'orderdetails',
            'Param1' => base64_decode($order_id),
            'Param2' =>$pro_id,
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
   // p($paramm );

  $data['details'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
   // p($data['details']);exit();

  	$this->load->view('helper/head');
    $this->load->view('helper/header');
    $this->load->view('orderdetails',$data);
    $this->load->view('helper/footer');
    $this->load->view('helper/foot');
  }

 public function myreview()
  {
    $user_id = $this->session->userdata('foxsky')->id;
    $paramm = array(
            'act_mode' =>'review',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
            // p($paramm);
        $data['review'] = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
   
// p($data['review']);exit();
   $paramm = array(
            'act_mode' =>'reviewdetails',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
            // p($paramm);
    $data['myreview'] = $this->supper_admin->call_procedure('proc_tvoverviewslider',$paramm);
 // p($data['myreview']);exit();

    $paramm = array(
            'act_mode' =>'managereviewdetails',
            'Param1' =>$user_id,
            'Param2' =>'',
            'Param3' =>'',
            'Param4' =>'',
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
            // p($paramm);
    $data['managemyreview'] = $this->supper_admin->call_procedure('proc_tvoverviewslider',$paramm);

    // p($data['managemyreview']);exit();



    $this->load->view('helper/head');
    $this->load->view('helper/header');
    $this->load->view('myreview',$data);
    $this->load->view('helper/footer');
    $this->load->view('helper/foot');

  }

   public function editreview()
  {


    $user_id = $this->session->userdata('foxsky')->id;
    $proname = $this->uri->segment(2);
    $ordid = $this->uri->segment(3);
    $proid = $this->uri->segment(4);
    $reviewid = $this->uri->segment(5);

    $paramm = array(
            'act_mode' =>'editreview',
            'Param1' =>$user_id,
            'Param2' =>$proid,
            'Param3' =>$reviewid,
            'Param4' =>$ordid,
            'Param5' =>'',
            'Param6' =>'',
            'Param7' =>'',
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
             // p($paramm);
        $data['editreview'] = $this->supper_admin->call_procedure('proc_tvoverviewslider',$paramm);
       // p($data['editreview']);exit;

        
    $data['proname'] = $this->uri->segment(2);
    $data['ordid'] = $this->uri->segment(3);
    $data['proid'] = $this->uri->segment(4);
    $data['reviewid'] = $this->uri->segment(5);


    $this->load->view('helper/head');
    $this->load->view('helper/header');
    $this->load->view('editreview',$data);
    $this->load->view('helper/footer');
    $this->load->view('helper/foot');
    } 

  public function comment($ordid){
  // p($ordid);
    $params=array('rowid'=>$_POST['statsid']);
    // p($params);exit();
    $getting_path = api_url().'orderapi/get_order_dtls/format/json/';
    $data= curlpost($params,$getting_path);

    $unit_price = get_unit_price($data->ProId, $_POST['cur_qty'], $this->session->userdata('logincity')->cmid);
    
    if($_POST['cur_qty']==$_POST['old_qty'])
    {
      $paramss=array(
                      'act_mode'=>'update_all_qty',
                      'rowid'=>$_POST['statsid'],
                      'promapid'=>$data->OrderproId,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>$_POST['old_stat'],
                      'orderproid'=>$data->ProId,
                      'order_id'=>$data->OrderId,
                      // 'order_venid'=>$data->VendorID,
                      'order_qty'=>$_POST['cur_qty'],
                      'comnt'=>$_POST['comment'],
                      'old_qty'=>$_POST['old_qty'],
                      'price'=>$data->base_prize

                    );
      // p($paramss);exit();
      
     $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
     $response['or']= curlpost($paramss,$getting_pat);
           // p($response['or']);exit();

    }else{
        
            $paramss=array(
                          'act_mode'=>'update_less_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>$_POST['comment'],
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>$data->base_prize

                        );
           $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           $response['or']= curlpost($paramss,$getting_pat);

           $paramss=array(
                          'act_mode'=>'update_remaining_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>$_POST['comment'],
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>get_unit_price($data->ProId, ($_POST['old_qty'] - $_POST['cur_qty']), $this->session->userdata('logincity')->cmid)

                        );
           $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           $response['or']= curlpost($paramss,$getting_pat);

    }

    if($_POST['flag']=="cancel"){

        $parameter = array(
                    'act_mode'  => 'get_ord_details',
                    'row_id'    => $data->ProId,
                    'order_id'    => $data->OrderId,
                    'orderstatus' => '',
                    'order_proid' => '');
        $mailData = $this->supper_admin->call_procedureRow('proc_orderEmailData',$parameter);

        $parameter = array(
                    'act_mode'  => 'get_pro_details',
                    'row_id'    => $data->ProId,
                    'order_id'    => $data->OrderId,
                    'orderstatus' => '',
                    'order_proid' => '');
        $mailProductData = $this->supper_admin->call_procedureRow('proc_orderEmailData',$parameter);

        //-------------------- Generate invoice Email -----------------------------
        $bizzmail = "support@honeymoneytop.com";
        $emailid['mmail'] = array('ordernumber' => $mailData->OrderNumber,
                              'orderdate' => $mailData->OrderDate,
                              'shippingaddress' => $mailData->ShippingAddress,
                              'billaddress' => $mailData->BillAddress,
                              'shippingname' => $mailData->ShippingName,
                              'prosku' => $mailProductData->prosku,
                              'proname' => $mailProductData->proname,
                              'description' => $mailProductData->description);

        $emmailidd = $mailData->Email;
        $msg = $this->load->view('bizzgainmailer/cancelmail',$emailid, true);
        $this->load->library('email');
        $this->email->from($bizzmail, 'Honey Money Top');
        $this->email->to($emmailidd); 
       // $this->email->cc($replyemail);
        $this->email->subject('Order Cancelled');
        $this->email->message($msg);  
        $this->email->send();

    }
    
    if($_POST['flag']=="return processing"){

        $parameter = array(
                    'act_mode'  => 'get_ord_details',
                    'row_id'    => $data->ProId,
                    'order_id'    => $data->OrderId,
                    'orderstatus' => '',
                    'order_proid' => '');
        $mailData = $this->supper_admin->call_procedureRow('proc_orderEmailData',$parameter);
        
        $parameter = array(
                    'act_mode'  => 'get_pro_details',
                    'row_id'    => $data->ProId,
                    'order_id'    => $data->OrderId,
                    'orderstatus' => '',
                    'order_proid' => '');
        $mailProductData = $this->supper_admin->call_procedureRow('proc_orderEmailData',$parameter);

        //-------------------- Generate invoice Email -----------------------------
        $bizzmail = "support@foxsky.com";
        $emailid['mmail'] = array('ordernumber' => $mailData->OrderNumber,
                              'orderdate' => $mailData->OrderDate,
                              'shippingaddress' => $mailData->ShippingAddress,
                              'billaddress' => $mailData->BillAddress,
                              'shippingname' => $mailData->ShippingName,
                              'prosku' => $mailProductData->prosku,
                              'proname' => $mailProductData->proname,
                              'description' => $mailProductData->description);

        $emmailidd = $mailData->Email;
        $msg = $this->load->view('bizzgainmailer/returnmail',$emailid, true);
        $this->load->library('email');
        $this->email->from($bizzmail, 'Honey Money Top');
        $this->email->to($emmailidd); 
       // $this->email->cc($replyemail);
        $this->email->subject('Order Return');
        $this->email->message($msg);  
        $this->email->send();

  }
}

public function editreviewupdate(){
// pend($_POST);

        $heading = $this->input->post('tag');        
        $rating = $this->input->post('star');        
        $desc = $this->input->post('review'); 
        $proname = $this->input->post('proname'); 
        $ordid = $this->input->post('ordid'); 
        $proid = $this->input->post('proid'); 
        $reviewid = $this->input->post('reviewid');  

        $paramm = array(
            'act_mode' =>'updatereview',
            'Param1' =>$heading,
            'Param2' =>$rating,
            'Param3' =>$desc,
            'Param4' =>'',
            'Param5' =>$ordid,
            'Param6' =>$proid,
            'Param7' =>$reviewid,
            'Param8' =>'',
            'Param9' =>'',
            'Param10' =>''
        
            );
             // p($paramm);
        $data['updatereview'] = $this->supper_admin->call_procedure('proc_tvoverviewslider',$paramm);
        $this->session->set_flashdata("message", "Updated successfully.");
        redirect('myreview');

    }
}
?>