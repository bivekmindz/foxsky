<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Allproduct extends MX_Controller {
  public function __construct() {
      $this->load->model("supper_admin");
      $this->load->helper("my_helper");
  }

  public function index()
  {


  	     $parameter           = array('act_mode'=>'get_allcat_product',
      'row_id'=>'',
      'catparentid'=>'',
      'cname'=>'',
      'cdesc'=>'',
      'caturl'=>'',
      'catmeta'=>'','
      metadesc'=>'',
      'metakeyword'=>'',
      'catsort'=>'0',
      'catdisplay'=>'',
      'catlevel'=>'');
     //p($parameter);

    $data['allcat'] = $this->supper_admin->call_procedure('proc_category',$parameter);

$this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("allproducts",$data);
    $this->load->view("helper/footer",$data);
     $this->load->view("helper/foot");
  }

    public function category()
  {
$url = array_pop($this->uri->segments);
    //  pend($url);
    if($url!="ajaxprolist"){
      $category  = explode("-",$url);
      $cid       = array_pop($category);
      if($cid<=0){
        redirect();
      }
    }
   // echo $cid;

  	     $parameter           = array('act_mode'=>'get_cat_product',
      'row_id'=>$cid,
      'catparentid'=>'',
      'cname'=>'',
      'cdesc'=>'',
      'caturl'=>'',
      'catmeta'=>'','
      metadesc'=>'',
      'metakeyword'=>'',
      'catsort'=>'0',
      'catdisplay'=>'',
      'catlevel'=>'');
     //p($parameter);

    $data['allcat'] = $this->supper_admin->call_procedure('proc_category',$parameter);
$this->load->view("helper/head",$data);
    $this->load->view("helper/header",$data);
    $this->load->view("allproducts",$data);
    $this->load->view("helper/footer",$data);
     $this->load->view("helper/foot");
  }

   public function newsletterdata()
  {

 $parameter=array('act_mode'=>'insert','useremail'=>$_POST['newsemail'],'n_status'=>'','rowid'=>'');
    $record['newsview']=$this->supper_admin->call_procedurerow('proc_newsletter',$parameter);

echo($record['newsview']->lastid);
  }
}
