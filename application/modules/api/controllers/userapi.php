<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
 class Userapi extends REST_controller{

 
 	public function newlettersub_post(){
	    $parameter=array('act_mode'=>$this->post('act_mode'),
	                     'useremail'=>$this->post('useremail'),
	                     'n_status'=>$this->post('n_status'),
	                     'rowid'=>$this->post('rowid')
	                    );
	    //p($this->response($parameter, 202));exit();
    	$data = $this->model_api->call_procedureRow('proc_newsletter',$parameter);
      	if(!empty($data)){
          //$data   = $this->send_json($data);  
            $this->response($data, 202);
        }else{
          	$this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
   }

  public function memberregister_post(){
      $parameter     =array(
                         'act_mode' =>$this->input->post('act_mode'),
                         'row_id'   =>$this->input->post('row_id'),
                         'ventype'  =>$this->input->post('ventype'),
                         'vencode'  =>$this->input->post('vencode'),
                         'vcomp'    =>$this->input->post('vcomp'),
                         'vemail'   =>$this->input->post('vemail'),
                         'vcontact' =>$this->input->post('vcontact'),
                         'vpass'    =>base64_encode($this->input->post('vpass')),
                         'vname'    =>$this->input->post('vname'),
                         'vlast'    =>$this->input->post('vlast'),
                         'vcatid'   =>$this->input->post('vcatid'),
                         'vcountryid'=>$this->input->post('vcountryid'),
                         'vstateid' =>$this->input->post('vstateid'),
                         'vcityid'  =>$this->input->post('vcityid'),
                         'vaddress' =>$this->input->post('vaddress'),
                         'vpincode' =>$this->input->post('vpincode'),
                         'vpannumber'=>$this->input->post('vpannumber'),
                         'vtinnumber'=>$this->input->post('vtinnumber'),
                         'accontname'=>$this->input->post('accontname'),
                         'accnumber'=>$this->input->post('accnumber'),
                         'bankname' =>$this->input->post('bankname'),
                         'branchname'=>$this->input->post('branchname'),
                         'ifsccode' =>$this->input->post('ifsccode'),
                         'neftdetail' =>$this->input->post('neftdetail'),
                         'acctype'    =>$this->input->post('acctype'),
                         'cstnumber'  =>$this->input->post('cstnumber'),
                         'vatnumber'  =>$this->input->post('vatnumber'),
                         'cstidd'     =>$this->input->post('cstidd'),
                         'tinidd'     =>$this->input->post('tinidd'),
                         'vatcopy'    =>$this->input->post('vatcopy'),
                         'cstcopy'    =>$this->input->post('cstcopy'),
                         'chequecopy' =>$this->input->post('chequecopy'),
                         'pancopy'    =>$this->input->post('pancopy')
                         );
    //$this->response($parameter, 202);exit();
      $data = $this->model_api->call_procedureRow('proc_vendorlogin',$parameter);
        if(!empty($data)){
            $this->response($data, 202);
        }else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
   }

   public function memprofileupdate_post(){
  
    $parameter=array('act_mode'      =>$this->input->post('act_mode'),
                     'row_id'        =>$this->input->post('row_id'),
                     'mname'         =>$this->input->post('mname'),
                     'mlastname'     =>$this->input->post('mlastname'),
                     'memail'        =>$this->input->post('memail'),
                     'mmobile'       =>$this->input->post('mmobile'),
                     'mstate'        =>$this->input->post('mstate'),
                     'mcity'         =>$this->input->post('mcity'),
                     'mcstnumber'    =>$this->input->post('mcstnumber'),
                     'mtinnumber'    =>$this->input->post('mtinnumber'),
                     'mpannumber'    =>$this->input->post('mpannumber'),
                     'mconpanyname'  =>$this->input->post('mconpanyname'),
                     'maccname'      =>$this->input->post('maccname'),
                     'maccountnumber'=>$this->input->post('maccountnumber'),
                     'mbranch'       =>$this->input->post('mbranch'),
                     'mifsccode'     =>$this->input->post('mifsccode'),
                     'mneft'         =>$this->input->post('mneft'),
                     'macctype'      =>$this->input->post('macctype'),
                     'mvatcopy'      =>$this->input->post('mvatcopy'),
                     'mcstcopy'      =>$this->input->post('mcstcopy'),
                     'mchequecopy'   =>$this->input->post('mchequecopy'),
                     'mpancopy'      =>$this->input->post('mpancopy'),
                     'mcstid'        =>$this->input->post('mcstid'),
                     'mtinid'        =>$this->input->post('mtinid'),
                     'maddress'      =>$this->input->post('maddress'),
                     'mpincode'      =>$this->input->post('mpincode'));
                
    $data=$this->model_api->call_procedureRow('proc_memberupdate',$parameter);
    if(!empty($data)){
      $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  public function retailerprofileupdate_post(){
    if($this->input->post('userid')!="" && $this->input->post('first_name')!=""){ 
      if($_POST['vat_copy']!=""){
        $data = $_POST['vat_copy']; //exit;
        $data = str_replace('data:image/png;base64,', '', $data);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);
        $vat_copy = date('Y-m-d_H').'.png';
        $file = FCPATH.'manufacturer-files/'.$vat_copy;
        $success = file_put_contents($file, $data);
      }
      if($_POST['cst_copy']!=""){
        $data = $_POST['cst_copy']; //exit;
        $data = str_replace('data:image/png;base64,', '', $data);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);
        $cst_copy = date('Y-m-d_H').'.png';
        $file = FCPATH.'manufacturer-files/'.$cst_copy;
        $success = file_put_contents($file, $data);
      } 
      if($_POST['cheque_copy']!=""){
        $data = $_POST['cheque_copy']; //exit;
        $data = str_replace('data:image/png;base64,', '', $data);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);
        $cheque_copy = date('Y-m-d_H').'.png';
        $file = FCPATH.'manufacturer-files/'.$cheque_copy;
        $success = file_put_contents($file, $data);
      }
      if($_POST['pan_copy']!=""){
        $data = $_POST['pan_copy']; //exit;
        $data = str_replace('data:image/png;base64,', '', $data);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);
        $pan_copy = date('Y-m-d_H').'.png';
        $file = FCPATH.'manufacturer-files/'.$pan_copy;
        $success = file_put_contents($file, $data); 
      }
      $mmcstnum=$this->post('cst_number');
      $mmtinnum=$this->post('tin_number');
      if(empty($mmcstnum)){
          $mmcstid=4;
      } else {
          $mmcstid=2;
      }
      if(empty($mmtinnum)){
          $mmtinid=6;
      } else {
          $mmtinid=1;
      }
      $parameter=array('act_mode'      =>'memupdateretailer',
                       'row_id'        =>$this->post('userid'),
                       'mname'         =>$this->post('first_name'),
                       'mlastname'     =>$this->post('last_name'),
                       'memail'        =>$this->post('email'),
                       'mmobile'       =>$this->post('mobile'),
                       'mstate'        =>$this->post('state_id'),
                       'mcity'         =>$this->post('city_id'),
                       'mcstnumber'    =>$mmcstnum,
                       'mtinnumber'    =>$mmtinnum,
                       'mpannumber'    =>$this->post('pan_number'),
                       'mconpanyname'  =>$this->post('company_name'),
                       'maccname'      =>$this->post('acc_name'),
                       'maccountnumber'=>$this->post('acc_number'),
                       'mbranch'       =>$this->post('branch'),
                       'mifsccode'     =>$this->post('ifsc_code'),
                       'mneft'         =>$this->post('neft'),
                       'macctype'      =>$this->post('acc_type'),
                       'mvatcopy'      =>@$vat_copy,
                       'mcstcopy'      =>@$cst_copy,
                       'mchequecopy'   =>@$cheque_copy,
                       'mpancopy'      =>@$pan_copy,
                       'mcstid'        =>$mmcstid,
                       'mtinid'        =>$mmtinid,
                       'maddress'      =>$this->post('address'),
                       'mpincode'      =>$this->post('pincode'));                  
      $data=$this->model_api->call_procedureRow('proc_memberupdate',$parameter);
    }else{
      $data=array("response"=>0);
      $this->response($data,400); // 200 being the HTTP response code
    }
  }

  public function consumerprofileupdate_post(){
    if($this->input->post('userid')!="" && $this->input->post('first_name')!=""){  
      $parameter=array('act_mode'=>'memupdateconsumer',
                 'row_id'        =>$this->post('userid'),
                 'mname'         =>$this->post('first_name'),
                 'mlastname'     =>$this->post('last_name'),
                 'memail'        =>$this->post('email'),
                 'mmobile'       =>$this->post('mobile'),
                 'mstate'        =>$this->post('state_id'),
                 'mcity'         =>$this->post('city_id'),
                 'mcstnumber'    =>'',
                 'mtinnumber'    =>'',
                 'mpannumber'    =>'',
                 'mconpanyname'  =>'',
                 'maccname'      =>'',
                 'maccountnumber'=>'',
                 'mbranch'       =>'',
                 'mifsccode'     =>'',
                 'mneft'         =>'',
                 'macctype'      =>'',
                 'mvatcopy'      =>'',
                 'mcstcopy'      =>'',
                 'mchequecopy'   =>'',
                 'mpancopy'      =>'',
                 'mcstid'        =>'',
                 'mtinid'        =>'',
                 'maddress'      =>$this->post('address'),
                 'mpincode'      =>$this->post('pincode'));                
      $data=$this->model_api->call_procedureRow('proc_memberupdate',$parameter);
      $this->response($data,202); 
    }else{
      $data=array("response"=>0);
      $this->response($data,400); // 200 being the HTTP response code
    }
  }

  public function userlogin_post(){
  
    $parameter=array(
                    'act_mode' =>$this->input->post('act_mode'),
                    'row_id' =>$this->input->post('row_id'),
                    'vusername' =>$this->input->post('vusername'),
                    'vpassword'=>base64_encode($this->input->post('vpassword')),
                    'vstatus' =>$this->input->post('vstatus'),
                    );
                
    $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
    if(!empty($data)){
      $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  public function citygrouplogin_post(){
  
    $parameter=array(
                    'act_mode' =>$this->input->post('act_mode'),
                    'row_id' =>$this->input->post('row_id'),
                    'vusername' =>$this->input->post('vusername'),
                    'vpassword'=>'',
                    'vstatus' =>'',
                    );
           $this->response($parameter, 202); exit;     
    $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
    if(!empty($data)){
      $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  public function memdetailview_post(){
  
    $parameter=array(
                    'act_mode' =>$this->input->post('act_mode'),
                    'row_id' =>$this->input->post('row_id'),
                    'vusername' =>$this->input->post('vusername'),
                    'vpassword'=>$this->input->post('vpassword'),
                    'vstatus' =>$this->input->post('vstatus'),
                    );
                
    $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
    if(!empty($data)){
      $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  public function mempaymentdetailview_post(){
  
    $parameter=array(
                    'act_mode' =>$this->input->post('act_mode'),
                    'row_id' =>$this->input->post('row_id'),
                    'vusername' =>$this->input->post('vusername'),
                    'vpassword'=>$this->input->post('vpassword'),
                    'vstatus' =>$this->input->post('vstatus'),
                    );
                
    $data=$this->model_api->call_procedure('proc_memberlogin',$parameter);
    if(!empty($data)){
      $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

public function orderdetail_get(){
  $parameter = array('act_mode'=>'orderview','row_id' =>$this->get('userid') );
  $data = $this->model_api->call_procedure('proc_memorderhistroy',$parameter);
      if(!empty($data)){
        //$data   = $this->send_json($data);  
              $data   =     $this->response($data, 202);
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
}

public function trackorderdetail_get(){
  $parameter = array('act_mode'=>'trackorderview','row_id' =>$this->get('orderid') );
  $data = $this->model_api->call_procedure('proc_memorderhistroy',$parameter);
      if(!empty($data)){
        //$data   = $this->send_json($data);  
              $data   =     $this->response($data, 202);
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
}

public function trackorderuniqueid_get(){
  $parameter = array('act_mode'=>'trackorderuniqueid','row_id' =>$this->get('orderid') );
  $data = $this->model_api->call_procedure('proc_memorderhistroy',$parameter);
      if(!empty($data)){
        //$data   = $this->send_json($data);  
              $data   =     $this->response($data, 202);
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
}

public function orderdetailsview_get(){
  $parameter = array('act_mode'=>'orderdetailv','row_id' =>$this->get('ordid') );
  $data = $this->model_api->call_procedure('proc_memorderhistroy',$parameter);
  if(!empty($data)){
    //$data   = $this->send_json($data);  
    $data = $this->response($data, 202);
  }else{
    $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }
}

public function orderdetailsapp_get(){
    $parameter = array('act_mode'=>'orderview','row_id'=>$this->get('userid'));
    $data = $this->model_api->call_procedure('proc_memorderhistroy',$parameter);
    $res=array();
    foreach($data as $value){
      $response=array();      
      $parameter2 = array('act_mode'=>'orderdetailv','row_id'=>$value->OrderId);
      $data2 = $this->model_api->call_procedure('proc_memorderhistroy',$parameter2);
      $subtotal=$tax=0;
      foreach($data2 as $val) {
        $subtotal+=number_format($val->base_prize*$val->ProQty,1,".","");
        $amt=number_format($val->base_prize*$val->ProQty,1,".","");
        $tin_tax_amt=number_format($amt * $val->protaxvat_p / 100,1,".","");
        $cst_tax_amt=number_format($amt * $val->protaxcst_p / 100,1,".","");
        $entry_tax_amt=number_format($amt * $val->protaxentry_p / 100,1,".","");
        $tax+=$total_tax_amt=$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
        $totproprice=$amt+$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
        $t_coup+=get_partialdiscountpercentamt($totproprice,$val->couponper);
        $t_wall+=get_partialdiscountpercentamt($totproprice,$val->walletper);
      }
      $to_coup=number_format($t_coup,1,".","");
      $to_wall=number_format($t_wall,1,".","");
      $response['OrderId']=$value->OrderId;
      $response['OrderNumber']=$value->OrderNumber;
      $response['PaymentStatus']=$value->PaymentStatus;
      $response['PaymentMode']=$value->PaymentMode;
      $response['OrderDate']=$value->OrderDate;
      $response['ShippingAmt']=number_format($value->ShippingAmt,1,".","");
      $response['subtotal']=number_format($subtotal,1,".","");
      $response['tax']=number_format($tax,1,".","");
      $ordamts=number_format(number_format($subtotal,1,".","")+number_format($tax,1,".","")+number_format($value->ShippingAmt,1,".","")-$to_coup-$to_wall,1,".","");
      if($ordamts==0.0){
        $response['grandtotal']=number_format(number_format($subtotal,1,".","")+number_format($tax,1,".","")+number_format($value->ShippingAmt,1,".","")-$to_coup,1,".","");
      } else {
        $response['grandtotal']=$ordamts;
      }
      $response['products']=$data2;
      array_push($res,$response);  
    }    
    if(!empty($res)){ 
      $this->response($res, 202);
    }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
}

  public function shipstate_get(){
      $parameter=array(
                    'act_mode' =>'countrystate',
                    'row_id' =>6,
                    'counname' =>'',
                    'coucode'=>'',
                    'commid' =>''
                    );
               
    $data=$this->model_api->call_procedure('proc_geographic',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  public function shipcity_get(){
     $parameter=array(
                    'act_mode' =>'citystate',
                    'row_id' =>$this->get('stateid'),
                    'counname' =>'',
                    'coucode'=>'',
                    'commid' =>''
                    );
               
    $data=$this->model_api->call_procedure('proc_geographic',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }


 	public function getoldPassword_post(){
  $parameter = array('userid' =>$this->post('userid'),'old_password' => base64_encode($this->post('old_password')) );
  

   $data = $this->model_api->call_procedureRow('proc_getPassword',$parameter);
      if(!empty($data)){
        $this->response($data, 202);
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
}

public function passwordChange_post(){

    if($this->post('userid')!="" && $this->post('newpassword')!="") {
      $parameter= array('userid' =>$this->post('userid'), 'newpassword' =>base64_encode($this->post('newpassword')));              
      $data = $this->model_api->call_procedureRow('proc_changePassword',$parameter);      
      $this->response($data, 202);
    } else {
      $this->response("failure", 400); // 200 being the HTTP response code
    }
}

function chkuniquemail_post(){
 
 $parameter=array('act_mode'=>$this->post('act_mode'),'row_id'=>$this->post('row_id'),'vusername'=>$this->post('vusername'),'vpassword'=>$this->post('vpassword'),'vstatus'=>$this->post('vstatus'));
 $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
 if(!empty($data)){
  $this->response($data, 202); 
 }else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}

function forgatepassword_post(){
 
 $parameter=array('act_mode'=>$this->post('act_mode'),'row_id'=>$this->post('row_id'),'vusername'=>$this->post('vusername'),'vpassword'=>base64_encode($this->post('vpassword')),'vstatus'=>$this->post('vstatus'));
 $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
 if(!empty($data)){
  $this->response($data, 202); 
 }else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}

public function startbusiness_post(){
      $parameter=array('act_mode'=>$this->input->post('act_mode'),
                     'row_id'=>$this->input->post('row_id'),
                     'b_username'=>$this->input->post('b_username'),
                     'b_email'=>$this->input->post('b_email'),
                     'b_mobilenum'=>$this->input->post('b_mobilenum'),
                     'b_pincode'=>$this->input->post('b_pincode'),
                     'b_address'=>$this->input->post('b_address'),
                     'b_status'=>$this->input->post('b_status')
                    );
      //$this->response($parameter, 202);exit();
      $data = $this->model_api->call_procedureRow('proc_business',$parameter);
        if(!empty($data)){
            $this->response($data, 202);
        }else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
   }

   public function chmobileotp_post(){
      $parameter =array('act_mode'=>$this->input->post('act_mode'),
                        'row_id'=>$this->input->post('row_id'),
                        'p_userid'=>$this->input->post('p_userid'),
                        'p_email'=>$this->input->post('p_email'),
                        'p_mobilenum'=>$this->input->post('p_mobilenum'),
                        'p_mas'=>$this->input->post('p_mas'));
      //$this->response($parameter, 202);exit();
      $data = $this->model_api->call_procedureRow('proc_enquiry',$parameter);
        if(!empty($data)){
            $this->response($data, 202);
        }else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
   }

      public function verifypincode_post(){
      $parameter =array('act_mode'=>'verifypincode',
                        'row_id'=>'',
                        'p_userid'=>'',
                        'p_email'=>$this->input->post('pincode'),
                        'p_mobilenum'=>'',
                        'p_mas'=>'');
      //$this->response($parameter, 202);exit();
      $data = $this->model_api->call_procedureRow('proc_enquiry',$parameter);
        if(!empty($data)){
            $this->response($data, 202);
        }else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
   }

   public function memberregisterapp_post(){
      $num     = rand(111111, 999999); 
      $unicode = $this->input->post('acctype').$num;
      $parameter     =array(
                         'act_mode' =>'frontmemberinsertapp',
                         'row_id'   =>'',
                         'ventype'  =>$this->input->post('acctype'),
                         'vencode'  =>$unicode,
                         'vcomp'    =>$this->input->post('cname'),
                         'vemail'   =>$this->input->post('email'),
                         'vcontact' =>$this->input->post('contact'),
                         'vpass'    =>base64_encode($this->input->post('pass')),
                         'vname'    =>$this->input->post('fname'),
                         'vlast'    =>$this->input->post('lname'),
                         'vcatid'   =>'',
                         'vcountryid'=>'',
                         'vstateid' =>$this->input->post('stateid'),
                         'vcityid'  =>$this->input->post('cityid'),
                         'vaddress' =>'',
                         'vpincode' =>'',
                         'vpannumber'=>'',
                         'vtinnumber'=>'',
                         'accontname'=>'',
                         'accnumber'=>'',
                         'bankname' =>'',
                         'branchname'=>'',
                         'ifsccode' =>'',
                         'neftdetail' =>'',
                         'acctype'    =>'',
                         'cstnumber'  =>'',
                         'vatnumber'  =>'',
                         'cstidd'     =>'',
                         'tinidd'     =>'',
                         'vatcopy'    =>'',
                         'cstcopy'    =>'',
                         'chequecopy' =>'',
                         'pancopy'    =>''
                         );
        $data = $this->model_api->call_procedureRow('proc_vendorlogin',$parameter);
        if(!empty($data)){
            $this->response($data, 202);
        }else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
   }

   public function userloginapp_post(){
  
    $parameter=array(
                    'act_mode' =>'memloginapp',
                    'row_id' =>'',
                    'vusername' =>$this->input->post('email'),
                    'vpassword'=>base64_encode($this->input->post('pass')),
                    'vstatus' =>$this->input->post('acctype'),
                    );
                
    $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
    if(!empty($data)){
      $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  

  /*public function testurl_get(){
      $parameter =array('act_mode'=>'http://192.168.1.30/bizzbizzgain/combo/combolist');
       if(!empty($parameter)){
            $this->response(redirect($parameter['act_mode']) , 202);
        }else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
   }*/

function forgatepasswordapi_post(){
 
 if($this->post('row_id')=='email'){

      $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      $passwordmail= substr(str_shuffle($chars),0,6);

     $parameter=array('act_mode'    =>'forgetpassmem',
                      'row_id'      =>'',
                      'vusername'   =>$this->post('vusername'),
                      'vpassword'   =>base64_encode($passwordmail),
                      'vstatus'     =>$this->post('vstatus'));
     
 //$this->response($parameter, 202);exit;
     $data['userdet']=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
     if(!empty($data['userdet'])){
      //$data['userpass']=$passwordmail;
      $data['userpass']=base64_decode($data['userdet']->password);
      $bizzgainemail="support@bizzgain.com";
      $msg = $this->load->view('webapp/bizzgainmailer/forgotpassword',$data, true);
      $this->load->library('email');
      $this->email->from($bizzgainemail, 'Bizzgain.com');
      $this->email->to($this->post('vusername')); 
      $this->email->subject('Forgot Password - ('.$this->post('vusername').')');
      $this->email->message($msg);
      if($this->email->send())
      { 
        $this->response(array('response'=>"success"), 202);
      } else {
        $this->response("Something Went Wrong", 400);
      }

     }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
 } else {

      $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      $passwordph= substr(str_shuffle($chars),0,6);

    $parameter=array('act_mode'    =>'forgetpassmemmob',
                      'row_id'      =>'',
                      'vusername'   =>$this->post('vusername'),
                      'vpassword'   =>base64_encode($passwordph),
                      'vstatus'     =>$this->post('vstatus'));
     

     $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
     if(!empty($data)){
      $smsname = $data->firstname.' '.$data->lastname;
      $smspass = base64_decode($data->password);
      $smsmsgg = urlencode('Hi '.$smsname.' your new password is '.$smspass.'.');

      $file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$this->post('vusername').'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');
      $this->response(array('response'=>"success"), 202);
     }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
 }



}


public function appmobileotp_get(){

    $num = rand(1111, 9999);
    $acctype=$this->get('acctype');
    $smsphnon=$this->get('mobilenum');
      $parameter =array('act_mode'=>'chmobileotp',
                        'row_id'=>'',
                        'p_userid'=>'',
                        'p_email'=>$acctype,
                        'p_mobilenum'=>$smsphnon,
                        'p_mas'=>'');
      //$this->response($parameter, 202);exit();
      $data = $this->model_api->call_procedureRow('proc_enquiry',$parameter);
    if($data->cont>0){

        $this->response(array("OTP status"=>"Mobile number already exist."), 202); // 200 being the HTTP response code
    } else {
      $smsmsgg = urlencode('Hi User, your OTP Password is '.$num.' for Mobile Number Verification.'); 
      $file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');
      $this->response(array("OTP status"=>"OTP Send","pin"=>$num), 202);
    }
       
  }


public function apporder_account_status_post(){

  $appordstatusid=$this->post('statusid');
  $appordcurrentqty=$this->post('currentqty');
  $appordoldqty=$this->post('oldqty');
  $appordcmid=$this->post('cmid');
  $appordcurrentstatus=$this->post('currentorderstatus'); //flag
  $appordoldstatus=$this->post('oldorderstatus'); 
  $appordcomment=$this->post('comment'); 


  $parameterdet=array(
                  'act_mode'  => 'get_ord_dtls',
                  'row_id'    => $appordstatusid,
                  'orderstatus'=>'',
                  'order_proid'=>'',
                  'order_id'=>'',
                  'order_venid'=>'',
                  'order_qty'=>'',
                  'oldstatus'=>''
                  );
    $record=$this->model_api->call_procedureRow('proc_orderflow',$parameterdet);

    $unit_price = get_unit_price($record->ProId, $appordcurrentqty, $appordcmid);
    
    if($appordcurrentqty==$appordoldqty)
    {
      $paramss=array(
                      'act_mode'    =>'update_all_qty',
                      'row_id'      =>$appordstatusid,
                      'promap_id'   =>$record->OrderproId,
                      'orderstatus' =>$appordcurrentstatus,
                      'order_proid' =>$record->ProId,
                      'order_id'    =>$record->OrderId,
                      'order_venid' =>$record->VendorID,
                      'order_qty'   =>$appordcurrentqty,
                      'oldstatus'   =>$appordoldstatus,
                      'comnt'       =>$appordcomment,
                      'old_qty'     =>$appordoldqty,
                      'price'       =>$record->base_prize

                    );

      $response = $this->model_api->call_procedureRow('proc_update_orderdetils', $paramss); 
          
    }else{
            
            $paramss=array(
                      'act_mode'    =>'update_less_qty',
                      'row_id'      =>$appordstatusid,
                      'promap_id'   =>$record->OrderproId,
                      'orderstatus' =>$appordcurrentstatus,
                      'order_proid' =>$record->ProId,
                      'order_id'    =>$record->OrderId,
                      'order_venid' =>$record->VendorID,
                      'order_qty'   =>$appordcurrentqty,
                      'oldstatus'   =>$appordoldstatus,
                      'comnt'       =>$appordcomment,
                      'old_qty'     =>$appordoldqty,
                      'price'       =>$record->base_prize

                    );
           $response = $this->model_api->call_procedureRow('proc_update_orderdetils', $paramss);
  
           $paramsss=array(
                          'act_mode'    =>'update_remaining_qty',
                          'row_id'      =>$appordstatusid,
                          'promap_id'   =>$record->OrderproId,
                          'orderstatus' =>$appordcurrentstatus,
                          'order_proid' =>$record->ProId,
                          'order_id'    =>$record->OrderId,
                          'order_venid' =>$record->VendorID,
                          'order_qty'   =>$appordcurrentqty,
                          'oldstatus'   =>$appordoldstatus,
                          'comnt'       =>$appordcomment,
                          'old_qty'     =>$appordoldqty,
                          'price'       =>get_unit_price($record->ProId, ($appordoldqty - $appordcurrentqty), $appordcmid)

                        );
           $responses = $this->model_api->call_procedureRow('proc_update_orderdetils', $paramsss);

    }

    if(!empty($record)){
        $msgord='Order number ORD'.$record->OrderId.' is '.$appordcurrentstatus.' successfully.';
        $this->response(array("status"=>$msgord), 202);
    } else {
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

}


public function memretailerupdate_post(){

    $checkimg = str_replace(" ", "_", $_FILES['memchequefile']['name']);
    $panimg = str_replace(" ", "_", $_FILES['mempanfile']['name']);
    $tinimg = str_replace(" ", "_", $_FILES['memvatfile']['name']);
    $cstimg = str_replace(" ", "_", $_FILES['memcstfile']['name']);
    
    move_uploaded_file($_FILES['memchequefile']['tmp_name'], FCPATH.'manufacturer-files/'.time().$checkimg);
    move_uploaded_file($_FILES['mempanfile']['tmp_name'], FCPATH.'manufacturer-files/'.time().$panimg);
    move_uploaded_file($_FILES['memvatfile']['tmp_name'], FCPATH.'manufacturer-files/'.time().$tinimg);
    move_uploaded_file($_FILES['memcstfile']['tmp_name'], FCPATH.'manufacturer-files/'.time().$cstimg);
    $mmcstnum=$this->input->post('memcst');
    $mmtinnum=$this->input->post('memvat');
    if(empty($mmcstnum)){
        $mmcstid=4;
    } else {
        $mmcstid=2;
    }
    if(empty($mmtinnum)){
        $mmtinid=6;
    } else {
        $mmtinid=1;
    }

    $parameter=array('act_mode'      =>'memupdate',
                     'row_id'        =>$this->input->post('userid'),
                     'mname'         =>$this->input->post('mname'),
                     'mlastname'     =>$this->input->post('mlastname'),
                     'memail'        =>$this->input->post('memail'),
                     'mmobile'       =>$this->input->post('mmobile'),
                     'mstate'        =>$this->input->post('mstate'),
                     'mcity'         =>$this->input->post('mcity'),
                     'mcstnumber'    =>$mmcstnum,
                     'mtinnumber'    =>$mmtinnum,
                     'mpannumber'    =>$this->input->post('mpannumber'),
                     'mconpanyname'  =>$this->input->post('mcompanyname'),
                     'maccname'      =>$this->input->post('maccname'),
                     'maccountnumber'=>$this->input->post('maccountnumber'),
                     'mbranch'       =>$this->input->post('mbranch'),
                     'mifsccode'     =>$this->input->post('mifsccode'),
                     'mneft'         =>$this->input->post('mneft'),
                     'macctype'      =>$this->input->post('macctype'),
                     'mvatcopy'      =>time().$tinimg,
                     'mcstcopy'      =>time().$cstimg,
                     'mchequecopy'   =>time().$checkimg,
                     'mpancopy'      =>time().$panimg,
                     'mcstid'        =>$mmcstid,
                     'mtinid'        =>$mmtinid,
                     'maddress'      =>$this->input->post('maddress'),
                     'mpincode'      =>$this->input->post('mpincode'));
                
    $data=$this->model_api->call_procedureRow('proc_memberupdate',$parameter);
    if(!empty($data)){
      $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }


/*public function appmobileforgate_post(){
 
 $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $passwordmail= substr(str_shuffle($chars),0,6);

    $parameter=array('act_mode'=>'forgetpassmemmob','row_id'=>'','vusername'=>$this->post('vusername'),'vpassword'=>base64_encode($passwordmail),'vstatus'=>$this->post('vstatus'));
 $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
 if(!empty($data)){
  $smsphnon = $data->contactnumber;
  $smsname = $data->firstname.' '.$data->lastname;
  $smsmsgg = urlencode('Hi '.$smsname.' your new password is '.$passwordmail.'.');
  $file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');
  $this->response(array("password status"=>"Password Send","password"=>$passwordmail), 202); 
 }else{
  $this->response(array("password status"=>"Enter Valid Mobile number."), 400); // 200 being the HTTP response code
}
       
  } */

public function apiorderstatuschange_post(){
    $parameter=array(
                      'act_mode'  => 'get_ord_dtls',
                      'row_id'    => $this->post('ordstatusid'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>''
                    );
    $data=$this->model_api->call_procedureRow('proc_orderflow',$parameter);


    $unit_price = get_unit_price($data->ProId, $this->post('cur_qty'), $this->post('cmid'));
    
    if($this->post('cur_qty')==$this->post('old_qty'))
    {
      /*$paramss=array(
                      'act_mode'=>'update_all_qty',
                      'rowid'=>$_POST['statsid'],
                      'promapid'=>$data->OrderproId,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>$_POST['old_stat'],
                      'orderproid'=>$data->ProId,
                      'order_id'=>$data->OrderId,
                      'order_venid'=>$data->VendorID,
                      'order_qty'=>$_POST['cur_qty'],
                      'comnt'=>$_POST['comment'],
                      'old_qty'=>$_POST['old_qty'],
                      'price'=>$data->base_prize

                    );*/
      $parameterf=array(
                      'act_mode'    =>'update_all_qty',
                      'row_id'      =>$this->post('ordstatusid'),
                      'promap_id'   =>$data->OrderproId,
                      'orderstatus' =>$this->post('ord_status'),
                      'order_proid' =>$data->ProId,
                      'order_id'    =>$data->OrderId,
                      'order_venid' =>$data->VendorID,
                      'order_qty'   =>$this->post('cur_qty'),
                      'oldstatus'   =>$this->post('old_status'),
                      'comnt'       =>$this->post('comment'),
                      'old_qty'     =>$this->post('old_qty'),
                      'price'       =>$data->base_prize

                    );
    //p($this->response($parameter, 202));exit();
    $response['or']=$this->model_api->call_procedureRow('proc_update_orderdetils', $parameterf);


     //$getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
     //$response['or']= curlpost($paramss,$getting_pat);
    }else{
        
            /*$paramss=array(
                          'act_mode'=>'update_less_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>$_POST['comment'],
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>$data->base_prize

                        );*/

            $parameterfl=array(
                      'act_mode'    =>'update_less_qty',
                      'row_id'      =>$this->post('ordstatusid'),
                      'promap_id'   =>$data->OrderproId,
                      'orderstatus' =>$this->post('ord_status'),
                      'order_proid' =>$data->ProId,
                      'order_id'    =>$data->OrderId,
                      'order_venid' =>$data->VendorID,
                      'order_qty'   =>$this->post('cur_qty'),
                      'oldstatus'   =>$this->post('old_status'),
                      'comnt'       =>$this->post('comment'),
                      'old_qty'     =>$this->post('old_qty'),
                      'price'       =>$data->base_prize

                    );
            $response['or']=$this->model_api->call_procedureRow('proc_update_orderdetils', $parameterfl);

           //$getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           //$response['or']= curlpost($paramss,$getting_pat);

           /*$paramss=array(
                          'act_mode'=>'update_remaining_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>$_POST['comment'],
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>get_unit_price($data->ProId, ($_POST['old_qty'] - $_POST['cur_qty']), $this->session->userdata('logincity')->cmid)

                        );*/

           $parameterflr=array(
                      'act_mode'    =>'update_remaining_qty',
                      'row_id'      =>$this->post('ordstatusid'),
                      'promap_id'   =>$data->OrderproId,
                      'orderstatus' =>$this->post('ord_status'),
                      'order_proid' =>$data->ProId,
                      'order_id'    =>$data->OrderId,
                      'order_venid' =>$data->VendorID,
                      'order_qty'   =>$this->post('cur_qty'),
                      'oldstatus'   =>$this->post('old_status'),
                      'comnt'       =>$this->post('comment'),
                      'old_qty'     =>$this->post('old_qty'),
                      'price'       =>get_unit_price($data->ProId, ($this->post('old_qty') - $this->post('cur_qty')), $this->post('cmid'))

                    );

           $response['or']=$this->model_api->call_procedureRow('proc_update_orderdetils', $parameterflr);
           //$getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           //$response['or']= curlpost($paramss,$getting_pat);

    }


    if(!empty($data)){
      $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }

}

  public function userappdeviceupdate_post(){
  
    $parameter=array(
                    'act_mode' =>'memappupdatedevice',
                    'row_id' =>$this->post('user_id'),
                    'vusername' =>$this->post('device_type'),
                    'vpassword'=>'',
                    'vstatus' =>$this->post('device_id'),
                    );
                
    $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
    if(!empty($data)){
      $this->response($data, 202); 
    }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

}//end class
?>