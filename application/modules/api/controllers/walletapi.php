<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
 class Walletapi extends REST_controller{


 	public function drcrwallet_get(){
    $parameter=array('act_mode'=>'getwalletdrcr',
                     'row_id'=>$this->get('user_id'),
                     'wa_orderid'=>'',
                     'wa_userid'=>'',
                     'wa_type'=>'',
                     'wa_amt'=>'',
                     'wa_status'=>''
                     );
    $data = $this->model_api->call_procedure('proc_wallet',$parameter);
    if(!empty($data)){
      $this->response($data, 202);
    }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  public function drcrwalletapp_get(){
    $parameter=array('act_mode'=>'getwalletdrcr',
                     'row_id'=>$this->get('user_id'),
                     'wa_orderid'=>'',
                     'wa_userid'=>'',
                     'wa_type'=>'',
                     'wa_amt'=>'',
                     'wa_status'=>''
                     );
    $walletdrcr=$this->model_api->call_procedure('proc_wallet',$parameter);
    $totfixwalletamt=0;
    $fixwalletamt=0;
    foreach($walletdrcr as $value){ 
      $totfixwalletamt = number_format($totfixwalletamt,1,".","")+number_format($value->w_amount,1,".","");
      $fixwalletamt = number_format($fixwalletamt,1,".","")+number_format($value->w_amount,1,".","");
      $value->total_amount = number_format($fixwalletamt,1,".","");
      $data[] = $value;
    }
    //$data[]['total_balance']=number_format($totfixwalletamt,1,".","");
    if(!empty($data)){
      $this->response($data, 202);
    }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  public function addwalletpay_post(){
    $parameter=array('act_mode'=>$this->input->post('act_mode'),
                     'row_id'=>$this->input->post('row_id'),
                     'wa_orderid'=>$this->input->post('wa_orderid'),
                     'wa_userid'=>$this->input->post('wa_userid'),
                     'wa_type'=>$this->input->post('wa_type'),
                     'wa_amt'=>$this->input->post('wa_amt'),
                     'wa_status'=>$this->input->post('wa_status')
                     );
    $data = $this->model_api->call_procedure('proc_wallet',$parameter);
    if(!empty($data)){
      $this->response($data, 202);
    }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  public function appwalletpay_post(){

    $parameter=array('act_mode'=>'addwalletmoney',
                     'row_id'=>'',
                     'wa_orderid'=>$this->input->post('orderid'),
                     'wa_userid'=>$this->input->post('userid'),
                     'wa_type'=>'debit',
                     'wa_amt'=>"-".$this->input->post('walletamt'),
                     'wa_status'=>''
                     );
    $data = $this->model_api->call_procedure('proc_wallet',$parameter);
    if(!empty($data)){
      $this->response($data, 202);
    }else{
      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }
	
}//end class
?>