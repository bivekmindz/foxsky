<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
 class Home extends REST_controller{


 	public function newarrival_get()
 	{
 		$parameter=array('act_mode'=>'viewnewarrivals ','blimit'=>'','btype'=>'');
 		//pend($this->response($parameter,200));
 		$data=$this->model_api->call_procedure('proc_image',$parameter);
 		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 

 	}

 
 	public function mainslider_get(){
    	$parameter=array('act_mode'=>'viewslider ','blimit'=>'','btype'=>'mainslider');

        $data = $this->model_api->call_procedure('proc_image',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function rightbanner_get(){
    	$parameter=array('act_mode'=>'viewrightban ','blimit'=>'','btype'=>'rightban');
		
        $data = $this->model_api->call_procedure('proc_image',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function selectoneimg_get(){
    	$parameter=array('act_mode'=>'viewselectimg ','blimit'=>'','btype'=>'selectone');
		
        $data = $this->model_api->call_procedureRow('proc_image',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}


	public function starpro_get(){
	// $img_id = $this->post('img_id');
		$param = array('act_mode'=>'star_prdt','blimit'=>'','btype'=>'');
		$data = $this->model_api->call_procedure('proc_image',$param);

		// p($result);
		// exit();
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 

	}
	 public function selecttwoimg_get(){
     	$parameter=array('act_mode'=>'viewselectimg ','blimit'=>'','btype'=>'selecttwo');
		
        $data = $this->model_api->call_procedureRow('proc_image',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	     }
	     else{
	    $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    } 
 
	 }
	 public function selectthreeimg_get(){
     	$parameter=array('act_mode'=>'viewselectimg ','blimit'=>'','btype'=>'selectthree');
		
         $data = $this->model_api->call_procedureRow('proc_image',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	     }
	     else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	     } 
 
	 }
	public function selectfourimg_get(){
    	$parameter=array('act_mode'=>'viewselectimg ','blimit'=>'','btype'=>'selectfour');
		
        $data = $this->model_api->call_procedureRow('proc_image',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function sectionmaster_get(){
    	$parameter = array(
                        'act_mode' => 'viewmaster',
                        'row_id' => '',
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
        $data = $this->model_api->call_procedure('proc_sectiondetails',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function sectiondetail_get(){
    	$parameter = array(
                        'act_mode' => 'viewdetailmaster',
                        'row_id' => '',
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
        $data = $this->model_api->call_procedure('proc_sectiondetails',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function iconimgdata_get(){
    	$parameter = array(
                        'act_mode' => 'viewiconimg',
                        'row_id' => '',
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
        $data = $this->model_api->call_procedure('proc_sectiondetails',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function homevideo_get(){
    	$param  = array('act_mode'  => 'frontview_video','row_id'    => '','imagename' => '','imageurl'  => '','imgtype'   => '','img_head'  => '','img_shead' => '','img_status'=> '');

        $data = $this->model_api->call_procedureRow('proc_homebanner',$param);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

 	public function products_get(){
    	$parameter = array( 'proId' => 123);
    	$this->response('fddsfds', 202);exit; 
		
		//$this->response($parameter, 202);exit; 
	}

	public function maincat_get(){

		$parameterr = array('act_mode'=>'apiparecatview',
							'row_id'=>'',
							'catparentid'=>'',
							'cname'=>'',
							'cdesc'=>'',
							'caturl'=>'',
							'catmeta'=>'',
							'metadesc'=>'',
							'metakeyword'=>'',
							'catsort'=>'',
							'catdisplay'=>'',
							'catlevel'=>'');
    	$data = $this->model_api->call_procedure('proc_category',$parameterr);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function subcat_get(){

		$parameterr = array('act_mode'=>'apisubcatview',
							'row_id'=>$this->get('catid'),
							'catparentid'=>'',
							'cname'=>'',
							'cdesc'=>'',
							'caturl'=>'',
							'catmeta'=>'',
							'metadesc'=>'',
							'metakeyword'=>'',
							'catsort'=>'',
							'catdisplay'=>'',
							'catlevel'=>'');

		$data = $this->model_api->call_procedure('proc_category',$parameterr);
				if(!empty($data)){
	      $this->response($data, 202);
	    }else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}
//--------------mobile banners api------------------//
	public function mobilebannerimg_get(){
    	$parameter=array('act_mode'=>'mobilebanner ','blimit'=>'','btype'=>'');
		
        $data = $this->model_api->call_procedure('proc_image',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	


	//--------------promotional banners api------------------//
	public function promotionalbannerimg_get(){
    	$parameter=array('act_mode'=>'promotionalbanner ','blimit'=>'','btype'=>'');
		
        $data = $this->model_api->call_procedure('proc_image',$parameter);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	//--------------mobilesubcat api------------------//


		public function mobilesubcat_get(){
    	$parametertoy=array('act_mode'=>'mobsubtoy','blimit'=>1,'btype'=>'');
		
        $data['toy'] = $this->model_api->call_procedure('proc_image',$parametertoy);

        $parameterstationary=array('act_mode'=>'mobsubcatstationary ','blimit'=>2,'btype'=>'');
		
        $data['stationary'] = $this->model_api->call_procedure('proc_image',$parameterstationary);

        $parametercrockery=array('act_mode'=>'mobsubcatcrokry','blimit'=>3,'btype'=>'');
		
        $data['crockery'] = $this->model_api->call_procedure('proc_image',$parametercrockery);

		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	} //end funtion

	//--------------highest discount api------------------//
	public function highestdiscount_get(){
    	$parameter=array('act_mode'=>'highstdis','blimit'=>'','btype'=>'');
		
        $data = $this->model_api->call_procedure('proc_image',$parameter);
        if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
	}//end funtion

	public function highestdiscountall_get(){
    	$parameter=array('act_mode'=>'allhighstdisapp','blimit'=>$this->get('prolimit'),'btype'=>'');
		
        $data = $this->model_api->call_procedure('proc_image',$parameter);
        if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
	}//end funtion

	public function allmobilestationarycat_get(){
    	
    	$parameterstationary=array('act_mode'=>'allmobsubcatstationary ','blimit'=>'','btype'=>'');
		$data = $this->model_api->call_procedure('proc_image',$parameterstationary);

        if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function maincatlist_get(){

		$parameterr = array('act_mode'=>'apiparecatviewtype',
							'row_id'=>$this->get('type'),
							'catparentid'=>'',
							'cname'=>'',
							'cdesc'=>'',
							'caturl'=>'',
							'catmeta'=>'',
							'metadesc'=>'',
							'metakeyword'=>'',
							'catsort'=>'',
							'catdisplay'=>'',
							'catlevel'=>'');
    	$data = $this->model_api->call_procedure('proc_category',$parameterr);
		if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}


	public function tenbrand_get(){
		
		$parameter = array(
                         'act_mode'  =>'appbrandviews',
                         'row_id'    =>'',
                         'brandname' =>'',
                         'brandimage'=>'',
                         'catid'     =>''
                      	  );
        $data = $this->model_api->call_procedure('proc_brand',$parameter);
        $result=array();
        $ct=0;
         foreach ($data as $key => $value) {  
         	if(!empty($value->brandimages)){
         		$filename=FCPATH.'imagefiles/brandimages/'.$value->brandimages;
         		if (file_exists($filename)) {  $ct++;
         		    array_push($result, $value);
				    if($ct==10){
				    	break;
				    }
				}
         	}
         }
        if(!empty($result)){
	      $this->response($result, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function brandlist_get(){

		$parameter = array(
                         'act_mode'  =>'appbrandviews',
                         'row_id'    =>'',
                         'brandname' =>'',
                         'brandimage'=>'',
                         'catid'     =>''
                      	  );
        $data = $this->model_api->call_procedure('proc_brand',$parameter);
        $result=array();
         foreach ($data as $key => $value) {  
         	if(!empty($value->brandimages)){
         		$filename=FCPATH.'imagefiles/brandimages/'.$value->brandimages;
         		if (file_exists($filename)) {  
         		    array_push($result, $value);
				}
         	}
         }
        if(!empty($result)){
	      $this->response($result, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}

	public function manufacturelogin_post(){

	$vendortype   = "manufacturer";
    $comname      = $this->post('man_compname');
    $emailid      = $this->post('man_email');
    $mphn         = $this->post('man_mobile');
    $password     = $this->post('man_pass');
    $num          = rand(111111, 999999); 
    $uniqueCode   = $vendortype.$num;
    $cstid = 4;
    $tinid = 6;
    $prameter     = array(
                         'act_mode'   =>  'insert',
                         'row_id'     =>  '',
                         'ventype'    =>  $vendortype,
                         'vencode'    =>  $uniqueCode,
                         'vcomp'      =>  $comname,
                         'vemail'     =>  $emailid,
                         'vcontact'   =>  $mphn,
                         'vpass'      =>  base64_encode($password),
                         'vname'      =>  '',
                         'vlast'      =>  '',
                         'vcatid'     =>  '',
                         'vcountryid' =>  '',
                         'vstateid'   =>  '',
                         'vcityid'    =>  '',
                         'vaddress'   =>  '',
                         'vpincode'   =>  '',
                         'vpannumber' =>  '',
                         'vtinnumber' =>  '',
                         'accontname' =>  '',
                         'accnumber'  =>  '',
                         'bankname'   =>  '',
                         'branchname' =>  '',
                         'ifsccode'   =>  '',
                         'neftdetail' =>  '',
                         'acctype'    =>  '',
                         'cstnumber'  =>  '',
                         'vatnumber'  =>  '',
                         'cstidd'     =>  $cstid,
                         'tinidd'     =>  $tinid,
                         'vatcopy'    =>  '',
                         'cstcopy'    =>  '',
                         'chequecopy' =>  '',
                         'pancopy'    =>  ''
                         );
    
    $data = $this->model_api->call_procedureRow('proc_vendorlogin',$prameter);
        
        if(!empty($data)){
	      $this->response($data, 202);
	    }
	    else{
	      $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
	    } 
 
	}


	public function checkmanemail_get(){
    $manu_email = $this->get('man_email');
    $prameter     = array('act_mode' => 'manuemailch','row_id' => '','ventype' => '','vencode' => '','vcomp' => '','vemail' => $manu_email,'vcontact' => '','vpass' => '','vname' => '','vlast' => '','vcatid' => '','vcountryid' => '','vstateid' => '','vcityid' => '','vaddress' => '','vpincode' => '','vpannumber' => '','vtinnumber' => '','accontname' => '','accnumber' => '','bankname' => '','branchname' => '','ifsccode' => '','neftdetail' => '','acctype' => '','cstnumber' => '','vatnumber' => '','cstidd' => '','tinidd' => '','vatcopy' => '','cstcopy' => '','chequecopy' =>  '','pancopy' => '');
    
    $data = $this->model_api->call_procedureRow('proc_vendorlogin',$prameter);
    //p($record['chmaildata']->coun);
    if($data->coun==0){
	  $this->response(array('status'=>'email not exist'), 202);
    } else {
      $this->response(array('status'=>'email exist'), 400); // 200 being the HTTP response code
    } 

  }

  public function checkmanmobile_get(){
    $man_mobile = $this->get('man_mobile');
    $prameter     = array('act_mode' => 'manumobch','row_id' => '','ventype' => '','vencode' => '','vcomp' => '','vemail' => '','vcontact' => $man_mobile,'vpass' => '','vname' => '','vlast' => '','vcatid' => '','vcountryid' => '','vstateid' => '','vcityid' => '','vaddress' => '','vpincode' => '','vpannumber' => '','vtinnumber' => '','accontname' => '','accnumber' => '','bankname' => '','branchname' => '','ifsccode' => '','neftdetail' => '','acctype' => '','cstnumber' => '','vatnumber' => '','cstidd' => '','tinidd' => '','vatcopy' => '','cstcopy' => '','chequecopy' =>  '','pancopy' => '');
    
    $data = $this->model_api->call_procedureRow('proc_vendorlogin',$prameter);
    //p($record['chmobdata']);
    if($data->coun==0){
	  $this->response(array('status'=>'mobile not exist'), 202);
    } else {
      $this->response(array('status'=>'mobile exist'), 400); // 200 being the HTTP response code
    }

  }


	public function contactus_post(){

	  $c_name=$this->post('c_name');
      $c_email=$this->post('c_email');
      $c_mobile=$this->post('c_mobile');
      $c_enquiry=$this->post('c_enquiry');
//p($c_name);exit();
      //$bizzmail="support@bizzgain.com";
      //$replyemail="anurag.dua@bizzgain.com";
      $bizzmail="pawan@mindztechnology.com";
      $replyemail="rinkul@mindztechnology.com";
       
    $msg ='<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <div style="float: left; padding: 2px 2px; background: #fff; border: solid 1px #F3F0F0;">
        <div style="padding: 10px 10px; float: left; width: 600px; font-family: sans-serif; font-size: 12px; line-height: 18px; background: #F5F5F5;">
            <div style="float:left;text-align: center;width:100%;">
                <img src="'.base_url().'images/logo3.png" />
            </div>
            <div style="float: left; width: 100%; text-align: center; padding: 10px 0px; font-weight: bold; font-size: 20px; ">Contact Us Enquiry Information</div>
            <div style="float:left;width:100%;font-size:15px;padding:10px 0px;">Dear User</div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Name : <span style="font-weight:100;">'.$c_name.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Email : <span style="font-weight:100;">'.$c_email.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Contact Number : <span style="font-weight:100;">'.$c_mobile.'</span></div>
            <div style="padding:3px 0px;float:left;width:100%;font-size:13px;font-weight:bold;">Enquiry : <span style="font-weight:100;">'.$c_enquiry.'</span></div>
            <div style="float:left;width:100%;font-weight:bold;padding:15px 0px 5px;">
                Regards,<br />
                Bizzgain Team
            </div>
        </div>
        <div></div>
    </div>

</body>
</html>
';
         $this->load->library('email');
         $this->email->from($bizzmail, 'BIZZGAIN.com');
         $this->email->to($bizzmail);
         $this->email->cc($replyemail);
         $this->email->subject('Contact Us BIZZGAIN');
         $this->email->message($msg);  
        if($this->email->send()){
	      $this->response(array('mail_status'=>'Information sent successfully. we will contact you Shortly.'), 202);
	    } else {
	      $this->response(array('mail_status'=>'Information fail to send. Please try again'), 400); // 200 being the HTTP response code
	    } 
 
	}

}//end class
?>

