<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Orderapi extends REST_Controller{
	function ordercodonline_get(){
    $parameter=array('act_mode'=>$this->get('act_mode'));
    $data=$this->model_api->call_procedure('proc_Order',$parameter);
       if(!empty($data)){
            $data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
	}
	function orderbystatus_get(){
    $parameter=array('act_mode'=>$this->get('act_mode'));
    $data=$this->model_api->call_procedure('proc_OrderByStatus',$parameter);
       if(!empty($data)){
            $data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }
  /*function orderdetails_post(){
    $parameter=array('ordid'=>$this->post('ordid'));
    $data=$this->model_api->call_procedure('proc_order_in_account',$parameter); // proc_OrderDetails
      if(!empty($data)){
            $data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        } 
   }*/
   function orderdetails_get(){
    $parameter=array('ordid'=>$this->get('ordid'));
    $data=$this->model_api->call_procedure('proc_order_in_account',$parameter);
      if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        } 
   }




   function order_get(){
    $parameter=array('ordid'=>$this->get('ordid'));
    $data=$this->model_api->call_procedureRow('proc_OrderSingle',$parameter);
      if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        } 
   }
 function OrdersCount_get(){
    $data=$this->model_api->call_procedure('proc_OrderCount');
       if(!empty($data)){
            $data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
          }
  }

function OnlineOrdersCount_get(){
    $data=$this->model_api->call_procedure('proc_OrderCount');
       if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
          }
  }

function CodOrdersCount_get(){
    $data=$this->model_api->call_procedure('proc_OrderCount');
       if(!empty($data)){
            $data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
          }
  }



function orderlistvendor_post(){   // call all product for vendor 
$parameter = array('act_mode' =>$this->post('act_mode') , 'venderid'=>$this->post('id') );
$data = $this->model_api->call_procedure('proc_orderlisting', $parameter);
if(!empty($data)){
        $data   = $this->send_json($data);  
        $this->response($data, 202);
}

else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}


function productdetails_post(){     // for vendor product details //
$parameter = array('act_mode' =>$this->post('act_mode') , 'venderid'=>$this->post('id') ,'bucket_id'=>$this->post('bucket_id') );
$data = $this->model_api->call_procedure('proc_order_product_dtl', $parameter);
if(!empty($data)){
 $data   = $this->send_json($data);  
           $this->response($data, 202);
}
else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}




 function shippingaddressup_post(){
  $parameter = array('orderid' =>$this->post('orderid') , 
                     'state'=>$this->post('state') ,
                     'city'=>$this->post('city'),
                     'pin'=>$this->post('pin'), 
                     'shippingaddress'=>$this->input->post('shippingaddress') );

  $data = $this->model_api->call_procedureRow('proc_shipping_addess_update', $parameter);
  if(!empty($data)){
  $data   = $this->send_json($data);  
           $this->response($data, 202);
}
 else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
   } 
}

function shippingststus_post(){
    $parameter=array('act_mode' =>$this->post('act_mode') , 
                     'id'=>$this->post('id') ,
                     'status_id'=>$this->post('status_id') );

  $data = $this->model_api->call_procedure('proc_allshipingname', $parameter);
  if(!empty($data)){
  $data   = $this->send_json($data);  
            $this->response($data, 202);
}

else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}
}

function getlogigtic_post(){
  $data = $this->model_api->call_procedure('proc_getlogistic');
  if(!empty($data)){
  $data   = $this->send_json($data);  
            $this->response($data, 202);
}

else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}

}


function getuserinfo_post(){
 $parameter=array('userid' =>$this->post('userid')  );
 $data = $this->model_api->call_procedureRow('proc_userinfo' , $parameter);
  if(!empty($data)){
  $data   = $this->send_json($data);  
  $this->response($data, 202);
}

else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}

}

function shippingstatusupdate_post(){
     $parameter = array('awbno'=>$this->post('awbno'),'shipstatus'=>$this->post('shipstatus'));
     $data = $this->model_api->call_procedureRow('proc_UpdateshippingStatus',$parameter);
     if(!empty($parameter)){
     $data   = $this->send_json($parameter);  
     $this->response($data, 202);
     }
     else{
     $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

 function fedexawbapi_post(){
  $param['shipper']=array('personname'=>$this->post('personname'),
                          'compname'=>$this->post('compname'),
                          'mobile'=>$this->post('mobile'),
                          'StreetLines'=>$this->post('StreetLines'),
                          'city'=>$this->post('city'),
                          'state'=>$this->post('state'),
                          'postalcode'=>$this->post('postalcode')
                          );
  $param['addreciep']=array('name'=>$this->post('name'),
                            'cname'=>$this->post('cname'),
                            'contnumber'=>$this->post('contnumber'),
                            'address'=>$this->post('address'),
                            'cuscity'=>$this->post('cuscity'),
                            'statename'=>$this->post('statename'),
                            'pincode'=>$this->post('pincode'),
                            'countrycode'=>$this->post('countrycode'),
                            'countryname'=>$this->post('countryname')
                            );
  $param['specialserv']=array('amount'=>$this->post('amount'),
                              'paytype'=>$this->post('paytype')
                             );
  $param['customdetail']=array('devicetype'=>$this->post('devicetype'));
  $param['ordernum']=array('ordernum'=>$this->post('ordernum'));
  $param['financial']=array('accpersonname'=>$this->post('accpersonname'),
                            'acccompname'=>$this->post('acccompname'),
                            'accphonenumber'=>$this->post('accphonenumber'),
                            'financiladdress'=>$this->post('financiladdress'),
                            'financialcity'=>$this->post('financialcity'),
                            'financialState'=>$this->post('financialState'),
                            'financialpostal'=>$this->post('financialpostal')
                            );
  $data=$this->load->view('logistic/fedexawb',$param,true); 
  echo json_encode($data);
  /*if(!empty($data)){
    $data   =$this->send_json($data);  
    $this->response($data, 202);
  }
  else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }*/
 }
  function fedexawbapitest_post(){
  $param['shipper']=array('personname'=>$this->post('personname'),
                          'compname'=>$this->post('compname'),
                          'mobile'=>$this->post('mobile'),
                          'StreetLines'=>$this->post('StreetLines'),
                          'city'=>$this->post('city'),
                          'state'=>$this->post('state'),
                          'postalcode'=>$this->post('postalcode')
                          );
  $param['addreciep']=array('name'=>$this->post('name'),
                            'cname'=>$this->post('cname'),
                            'contnumber'=>$this->post('contnumber'),
                            'address'=>$this->post('address'),
                            'cuscity'=>$this->post('cuscity'),
                            'statename'=>$this->post('statename'),
                            'pincode'=>$this->post('pincode'),
                            'countrycode'=>$this->post('countrycode'),
                            'countryname'=>$this->post('countryname')
                            );
  $param['specialserv']=array('amount'=>$this->post('amount'),
                             'paytype'=>$this->post('paytype')
                            );
  $param['customdetail']=array('devicetype'=>$this->post('devicetype'));
  $param['ordernum']=array('ordernum'=>$this->post('ordernum'));
  $param['financial']=array('accpersonname'=>$this->post('accpersonname'),
                            'acccompname'=>$this->post('acccompname'),
                            'accphonenumber'=>$this->post('accphonenumber'),
                            'financiladdress'=>$this->post('financiladdress'),
                            'financialcity'=>$this->post('financialcity'),
                            'financialState'=>$this->post('financialState'),
                            'financialpostal'=>$this->post('financialpostal')
                            );
  $data=$this->load->view('logisticnew/fedexawb',$param,true); 
  if(!empty($data)){
    $data   =$this->send_json($data);  
    $this->response($data, 202);
  }
  else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }
 } 
 function orderstatus_get(){
  $data=$this->model_api->call_procedure('proc_OrderStatus');
  if(!empty($data)){
  $data   = $this->send_json($data);  
           $this->response($data, 202);
  }
  else{
  $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
  }
} 

  function ordercount_get(){
    $parameter=array('act_mode'=>$this->get('act_mode'));
    $data=$this->model_api->call_procedure('proc_Order',$parameter);
       if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

  function allorderpegi_post()
  {

    $parameter=array(
                       'act_mode'  => $this->post('act_mode'),
                       'p_page'    => $this->post('p_page'),
                       'p_limit'   => $this->post('p_limit')
                     );
    //p($this->response($parameter, 202));exit();
    $data=$this->model_api->call_procedure('proc_Orderwithpegi',$parameter);
    //p($this->response($data, 202));exit();
       if(!empty($data)){
            echo json_encode($data);  
            //$this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }
    function allonlineorderpegi_post()
    {
      $parameter=array(
                         'act_mode'  => $this->post('act_mode'),
                         'p_page'    => $this->post('p_page'),
                         'p_limit'   => $this->post('p_limit')
                      );
      
      $data=$this->model_api->call_procedure('proc_Orderwithpegi',$parameter);
       /*if(!empty($data)){
            $data   = $this->send_json($data);  
            $this->response($data, 202); 
        }*/
        if(!empty($data))
        {
            echo json_encode($data);  
            //$this->response($data, 202); 
        }
        else{
              $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
    }
    function allcodorderpegi_post()
    {
      $parameter=array(
                       'act_mode'  => $this->post('act_mode'),
                       'p_page'    => $this->post('p_page'),
                       'p_limit'   => $this->post('p_limit')
                      );

      $data=$this->model_api->call_procedure('proc_Orderwithpegi',$parameter);
      
         if(!empty($data))
         {
            echo json_encode($data);  
         }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
    }
   function vendorname_post(){
    $parameter=array('act_mode'  => $this->post('act_mode'),
                     'p_page'    => $this->post('p_page'),
                     'p_limit'   => $this->post('p_limit'));
    $data=$this->model_api->call_procedure('proc_Orderwithpegi',$parameter);
       if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

function allorderbuordnum_post(){
    $parameter=array('act_mode'  => $this->post('act_mode'),
                     'keyword'    => $this->post('keyword')
                     );
    $data=$this->model_api->call_procedure('proc_ordersearching',$parameter);
       if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }
  function searchorderbydate_post(){
    $parameter=array('act_mode'    => "$this->post('act_mode1')",
                     'todate'      => "$this->post('todate1')",
                     'fromdate'    => "$this->post('fromdate1')"
                     );

    $data=$this->model_api->call_procedure('proc_searchbydate',$parameter);
    if(!empty($parameter)){
    $data   = $this->send_json($parameter);  
    $this->response($data, 202); 
    }
    else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

  function allorderbymobnum_post(){
    $parameter=array('act_mode'  => $this->post('act_mode'),
                     'keyword'    => $this->post('keyword')
                     );
    $data=$this->model_api->call_procedure('proc_ordersearching',$parameter);
       if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

  function allorderbyp_post()
  {
    $parameter=array(
                      'act_mode'  => $this->post('act_mode'),
                      'keyword'    => $this->post('keyword')
                    );
    $data=$this->model_api->call_procedure('proc_ordersearching',$parameter);
       if(!empty($data)){
            //$data   = $this->send_json($data);  
            $this->response($data, 202); 
        }
        else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

   function allorderbyvenname_post(){
    $parameter=array('act_mode'  => $this->post('act_mode'),
                     'keyword'    => $this->post('keyword')
                     );
    $data=$this->model_api->call_procedure('proc_ordersearching',$parameter);
    if(!empty($data)){
            //echo json_encode($data);
            $this->response($data, 202);
    }
    else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

function get_order_dtls_post()
{
    $parameter=array(
                      'act_mode'  => 'get_ord_dtls',
                      'row_id'    => $this->post('rowid'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>''

                    );
    $data=$this->model_api->call_procedureRow('proc_orderflow',$parameter);
    if(!empty($data)){
           
            $this->response($data, 202);
    }
    else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
}

function uptodate_order_status_post()
{
  $parameter=array(
                      'act_mode'    =>$this->post('act_mode'),
                      'row_id'      =>$this->post('rowid'),
                      'promap_id'   =>$this->post('promapid'),
                      'orderstatus' =>$this->post('stats'),
                      'order_proid' =>$this->post('orderproid'),
                      'order_id'    =>$this->post('order_id'),
                      'order_venid' =>$this->post('order_venid'),
                      'order_qty'   =>$this->post('order_qty'),
                      'oldstatus'   =>$this->post('oldstats'),
                      'comnt'       =>$this->post('comnt'),
                      'old_qty'     =>$this->post('old_qty'),
                      'price'       =>$this->post('price')

                    );
    //p($this->response($parameter, 202));exit();
    $data=$this->model_api->call_procedureRow('proc_update_orderdetils', $parameter);
                                     
    if(!empty($data)){
           
            $this->response($data, 202);
    }
    else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
}
   public function get_quantity_range_get()
   {
     $parameter=array(
                      'act_mode'    =>$this->get('act'),
                      'row_id'      =>$this->get('cid'),
                      'promap_id'   =>'',
                      'orderstatus' =>'',
                      'order_proid' =>$this->get('pid'),
                      'order_id'    =>'',
                      'order_venid' =>'',
                      'order_qty'   =>'',
                      'oldstatus'   =>'',
                      'comnt'       =>'',
                      'old_qty'     =>'',
                      'price'       =>''

                    );
    //p($this->response($parameter, 202));exit();
    $data=$this->model_api->call_procedureRow('proc_update_orderdetils', $parameter);
                                     
    if(!empty($data)){
           
            $this->response($data, 202);
    }
    else{
           $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

} // end class
?>