<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
 class Checkoutapi extends REST_controller{

 
  public function userlogin_post(){
  
    $parameter=array(
                    'act_mode' =>$this->input->post('act_mode'),
                    'row_id' =>$this->input->post('row_id'),
                    'vusername' =>$this->input->post('vusername'),
                    'vpassword'=>base64_encode($this->input->post('vpassword')),
                    'vstatus' =>$this->input->post('vstatus'),
                    );
                
    $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  public function statedetail_get(){
      $parameter=array(
                    'act_mode' =>'countrystate',
                    'row_id' =>6,
                    'counname' =>'',
                    'coucode'=>'',
                    'commid' =>''
                    );
               
    $data=$this->model_api->call_procedure('proc_geographic',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  public function citydetail_get(){
      $parameter=array(
                    'act_mode' =>'viewcityid',
                    'row_id' =>$this->get('ctid'),
                    'counname' =>'',
                    'coucode'=>'',
                    'commid' =>''
                    );
               
    $data=$this->model_api->call_procedureRow('proc_geographic',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  
  public function shipdetail_post(){
  
    $parameter=array(
                    'act_mode'        =>$this->input->post('act_mode'),
                    'row_id'          =>$this->input->post('row_id'),
                    'user_regid'      =>$this->input->post('user_regid'),
                    'ship_name'       =>$this->input->post('ship_name'),
                    'bill_name'       =>$this->input->post('bill_name'),
                    'ship_address'    =>$this->input->post('ship_address'),
                    'bill_address'    =>$this->input->post('bill_address'),
                    'ship_email'      =>$this->input->post('ship_email'),
                    'bill_email'      =>$this->input->post('bill_email'),
                    'ship_pincode'    =>$this->input->post('ship_pincode'),
                    'bill_pincode'    =>$this->input->post('bill_pincode'),
                    'ship_contact'    =>$this->input->post('ship_contact'),
                    'bill_contact'    =>$this->input->post('bill_contact'),
                    'ship_state'      =>$this->input->post('ship_state'),
                    'bill_state'      =>$this->input->post('bill_state'),
                    'ship_city'       =>$this->input->post('ship_city'),
                    'bill_city'       =>$this->input->post('bill_city'),
                    'ship_cst'        =>$this->input->post('ship_cst'),
                    'bill_cst'        =>$this->input->post('bill_cst'),
                    'ship_tin'        =>$this->input->post('ship_tinno'),
                    'bill_tin'        =>$this->input->post('bill_tinno'),
                    'ship_createdon'  =>$this->input->post('ship_createdon')
                    );
                 //$this->response($parameter, 202); exit;
    $data=$this->model_api->call_procedureRow('proc_usershipping',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  public function getaddress_get(){
      $parameter=array(
                    'act_mode' =>'getuser_shipdetail',
                    'row_id' =>$this->get('userId'),
                    'pvalue' =>''
                    );
               
    $data=$this->model_api->call_procedure('proc_productmasterdata',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  public function getupdateaddress_get(){
      $parameter=array(
                    'act_mode' =>'getsingle_shipdetail',
                    'row_id' =>$this->get('shippid'),
                    'pvalue' =>''
                    );
               
    $data=$this->model_api->call_procedureRow('proc_productmasterdata',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  public function delgetaddress_get(){
      $parameter=array(
                    'act_mode' =>'deluser_shipdetail',
                    'row_id' =>$this->get('userId'),
                    'pvalue' =>''
                    );
               
    $data=$this->model_api->call_procedure('proc_productmasterdata',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

function payment_post(){
 $param = array(
                        'UserId'           =>$this->post('UserId'),
                        'userregid'        =>$this->post('userregid'),
                        'UserAddressId'    =>$this->post('UserAddressId'), 
                        'orderNumber'      =>$this->post('orderNumber'),
                        'totalAmount'      =>$this->post('totalAmount'),
                        'paymentMod'       =>$this->post('paymentMod'),
                        'shipcharge'       =>$this->post('shipcharge'),
                        'couponamt'        =>$this->post('couponamt'),
                        'couponcode'       =>$this->post('couponcode'),
                        'userIpaddress'    =>$this->post('userIpaddress'), 
                        'p_device'         =>$this->post('p_device'),
                        'usedwalletamount' =>$this->post('usedwalletamount'),
                        'p_usedrewardpoint'=>$this->post('p_usedrewardpoint'),
                        'p_subtotal'       =>$this->post('subtotal'),
                        'p_orderfrom'      =>$this->post('orderfrom'),
                        'totaltax'         =>$this->post('totaltax'),
                        'csttax'           =>$this->post('csttax'),
                        'tintax'           =>$this->post('tintax'),
                        'entrytax'         =>$this->post('entrytax'),
                        'u_remark'         =>$this->post('u_remark') );


 //$this->response($param,202); exit;

$data = $this->model_api->call_procedureRow('proc_shippingadduser',$param);
if(!empty($data)){
 $this->response($data,202); 
}
else{
 $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}

  }

function paymentcheque_post(){
          $param = array(
                        'act_mode'          => $this->post('act_mode'),
                        'row_id'            => $this->post('row_id'),
                        'orderid'           => $this->post('orderid'),
                        'chequeno'          => $this->post('chequeno'),
                        'ifsc'              => $this->post('ifsc'),
                        'bankname'          => $this->post('bankname'),
                        'chequedate'        => $this->post('chequedate'),
                        'accno'             => $this->post('accno'),
                        'chequecollectdate' => $this->post('chequecollectdate'),
                        'chequeamount'      => $this->post('chequeamount'),
                        'paymentMod'        => $this->post('paymentMod'),
                        'orderno'           => $this->post('orderno') 
                        );


 //$this->response($param,202); exit;

$data = $this->model_api->call_procedureRow('proc_cheque',$param);
if(!empty($data)){
 $this->response($data,202); 
}
else{
 $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
}

  }



function saveinordertbl_post(){
       $parameter = array(
                            'orderId'        =>$this->post('orderId'),
                            'proid'          =>$this->post('proid'),
                            'qty'            =>$this->post('qty'),
                            'shippingcharge' =>$this->post('shippingcharge'),
                            'proname'        =>$this->post('proname'),
                            'vendorId'       =>$this->post('vendorId'),
                            'prodSize'       =>$this->post('prodSize'),
                            'prodPrice'      =>$this->post('prodPrice'),
                            'coupon_value'   =>$this->post('coupon_value'),
                            'p_color'        =>$this->post('prodColor'),
                            'p_img'          =>str_replace(';','', $this->post('proimg')),
                            'cstvalue'       =>$this->post('cstvalue'),
                            'tinvalue'       =>$this->post('tinvalue'),
                            'entryvalue'     =>$this->post('entryvalue'),
                            'cst_p'         =>$this->post('cst_p'),
                            'tin_p'         =>$this->post('tin_p'),
                            'entry_p'       =>$this->post('entry_p')
                          );
                     //   p($parameter);die;

      $result = $this->model_api->call_procedureRow('proc_orderpromap', $parameter);
      if(!empty($result)){
          //$result   = $this->send_json($result);  
          $this->response($result, 202); 
        }
        else{
            $this->response("Insert successfully", 400); // 200 being the HTTP response code
        }
  }

  /*public function thankyou_get(){
      $parameter=array(
                    'act_mode' =>'thankudetail',
                    'row_id' =>$this->get('orderId'),
                    'pvalue' =>''
                    );
               
    $data=$this->model_api->call_procedure('proc_productmasterdata',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }*/

public function thankyou_get(){
    
      $parameter=array(
                    'act_mode' =>'thankudetail',
                    'row_id' =>$this->get('orderId'),
                    'pvalue' =>$this->get('userid')
                    );
    
    $data=$this->model_api->call_procedure('proc_productmasterdata',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }


public function vattax_get(){

     $parameter=array('act_mode'=>'frontgetvalue',
                      'row_id'=>$this->get('brandid'), // brandid
                      'taxvalue'=>'',
                      //'taxvalue'=>str_replace('~', ',', $this->get('catid')),
                      'countryid'=>'',
                      'stateid'=>$this->get('stateid'), //stateid
                      'baseprice'=>'',
                      'localvat'=>$this->get('catid'), // categoryid
                      //'localvat'=>'', // categoryid
                      'vatcat'=>'',
                      'vatnoncat'=>'',
                      'octorytax'=>''
                      );
  //$this->response($parameter, 202); exit;
    $data=$this->model_api->call_procedure('proc_tax',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  public function update_coupon_usage_post()
  {
      $param = array('act_mode' => 'update_coupon_usage',
                      'discount_name' => '',
                      'discount_value' => '', 
                      'discount_type' => '', 
                      'purchase_amount' => '', 
                      'is_coupon' => $this->input->post('user_id'), 
                      'coupon' => '', 
                      'is_reusable' => $this->input->post('discount_id'), 
                      'reusable_limit' => '', 
                      'valid_from' => '', 
                      'valid_to' => '', 
                      'coupon_user_type' => '',
                      'row_id' => '',
                      'brand_list' => '',
                      'product_type' => '',
                      'parent_id' => ''
                      ); 
      
      $result = $this->model_api->call_procedureRow('proc_discount_cart', $param);

      $result['update_status'] = "1";
      $this->response($result, 202);  
     /* if(!empty($result))
      {
          $this->response($result, 202);  
          die;
      } 
      else 
      {
          $this->response("Something Went Wrong", 400); // 200 being the HTTP response code    
      }
        */
   }
  public function discount_usage_detail_post()
  {
          
      $param = array('act_mode' => 'get_coupon_usage',
                      'discount_name' => '',
                      'discount_value' => '', 
                      'discount_type' =>  '', 
                      'purchase_amount' => '', 
                      'is_coupon' => $this->input->post('UserId'), 
                      'coupon' => $this->input->post('CouponCode'), 
                      'is_reusable' => '', 
                      'reusable_limit' => '', 
                      'valid_from' => '', 
                      'valid_to' => '', 
                      'coupon_user_type' => '',
                      'row_id' => '',
                      'brand_list' => '',
                      'product_type' => '',
                      'parent_id' => ''
                      );   

       $result['result1'] = $this->model_api->call_procedureRow('proc_discount_cart', $param);
        $param2 = array('act_mode' => 'get_coupon_usage_online',
                      'discount_name' => '',
                      'discount_value' => '', 
                      'discount_type' =>  '', 
                      'purchase_amount' => '', 
                      'is_coupon' => $this->input->post('UserId'), 
                      'coupon' => $this->input->post('CouponCode'), 
                      'is_reusable' => '', 
                      'reusable_limit' => '', 
                      'valid_from' => '', 
                      'valid_to' => '', 
                      'coupon_user_type' => '',
                      'row_id' => '',
                      'brand_list' => '',
                      'product_type' => '',
                      'parent_id' => ''
                      );   

       $result['result2'] = $this->model_api->call_procedureRow('proc_discount_cart', $param2);
       if(!empty($result))
       {
          $this->response($result, 202);  
          
       } 
       else 
       {
          $this->response("Something Went Wrong", 400); // 200 being the HTTP response code    
       }
  }
  public function discount_detail_post()
  {
      //$user_type = $this->session->userdata('bizzgainmember')->vendortype;
      //$user_type = $_POST['user_type'];
      
      $current_date = date('Y-m-d');
      $param = array('act_mode' => 'view_current_discount',
                                'discount_name' => '',
                                'discount_value' => '', 
                                'discount_type' => '', 
                                'purchase_amount' => '', 
                                'is_coupon' => '', 
                                'coupon' => $this->post('coupon'), 
                                'is_reusable' => '', 
                                'reusable_limit' => '', 
                                'valid_from' => $current_date, 
                                'valid_to' => '', 
                                'coupon_user_type' => $this->post('user_type'),
                                'row_id' => '',
                                'brand_list' => '',
                                'product_type' => '',
                                'parent_id' => ''
                                );
        //$this->response($param, 202);
        $result = $this->model_api->call_procedureRow('proc_discount_cart', $param);
        if(!empty($result))
        {
             $data['discount_data'] = $result;
             //$data['discount_wise'] = $result->discount_wise;
            $this->response($data, 202);
        }
        else{
              $this->response("Something Went Wrong", 400); // 200 being the HTTP response code       
        }
        
        
         // 200 being the HTTP response code   

        /*if(!empty($result)){

             //$data['discount_wise'] = $result->discount_wise; 
             //$this->response($data, 202);    

              
                 $tbl = NULL;
                if($result->discount_wise == 'cart')
                {
                  $tbl = 'tbl_discount_cart';
                }
                else if($result->discount_wise == 'brands')
                {
                    $tbl = 'tbl_brand_discount';
                }
                else if($result->discount_wise == 'products')
                {
                    $tbl = 'tbl_product_discount';
                }
                  
                else if($result->discount_wise == 'category')  
                  $tbl = 'tbl_category_discount';

              $param = array('act_mode' => 'view_data_by_table',
                                'discount_name' => $tbl,
                                'discount_value' => '', 
                                'discount_type' => '', 
                                'purchase_amount' => '', 
                                'is_coupon' => '', 
                                'coupon' => '', 
                                'is_reusable' => $result->discount_id, 
                                'reusable_limit' => '', 
                                'valid_from' => '', 
                                'valid_to' => '', 
                                'coupon_user_type' => '',
                                'row_id' => '',
                                'brand_list' => '',
                                'product_type' => '',
                                'parent_id' => ''
                                ); 

            $data['discount_data'] = $this->model_api->call_procedureRow('proc_discount_cart', $param);
     
            $data['discount_wise'] =  $result->discount_wise;
            if(!empty($data['discount_data'])){
                $this->response($data, 202);    
            }
            else {
                    $this->response("Something Went Wrong", 400); // 200 being the HTTP response code  
            } 
           
           
        }
        else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        } */
  }


  function shippingcharge_post(){
   //$parameter=array('act_mode'=>$this->post('act_mode'),'p_id'=>$this->post('p_id'));
   $parameter = array('act_mode'    =>$this->post('act_mode'),
                      'row_id'      =>$this->post('row_id'),
                      'orderstatus' =>$this->post('orderstatus'),
                      'order_proid' =>$this->post('order_proid'),
                      'order_id'    =>$this->post('order_id'),
                      'order_venid' =>$this->post('order_venid'),
                      'order_qty'   =>$this->post('order_qty'),
                      'oldstatus'   =>$this->post('oldstatus'));
   $result=$this->model_api->call_procedureRow('proc_orderflow',$parameter);
   if(!empty($result)){
          //$data   = $this->send_json($result);  
          $this->response($result, 202); 
        }
        else{
            $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
        }
  }

public function loginuserdetail_get(){
  
    $parameter=array(
                    'act_mode' =>'viewloginuserdetail',
                    'row_id' =>$this->get('userId'),
                    'vusername' =>'',
                    'vpassword'=>'',
                    'vstatus' =>'',
                    );
                
    $data=$this->model_api->call_procedureRow('proc_memberlogin',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  public function checkcredit_get(){
  
    $parameter= array('act_mode'=>'checkcreditprofile',
                      'row_id'=>$this->get('chcreditid'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>'');
                //p($this->response($parameter, 202));exit;
    $data=$this->model_api->call_procedureRow('proc_orderflow',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  public function getcombodetails_get(){
  
    $parameter= array('act_mode'=>'getcombo_dtls',
                      'row_id'=>$this->get('comboId'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>'');
                //p($this->response($parameter, 202));exit;
    $data=$this->model_api->call_procedure('proc_orderflow',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

function saveinordertblcombo_post(){
       $parameter = array(
                            'orderId'        =>$this->post('orderId'),
                            'proid'          =>$this->post('proid'),
                            'qty'            =>$this->post('qty'),
                            'shippingcharge' =>$this->post('shippingcharge'),
                            'proname'        =>$this->post('proname'),
                            'vendorId'       =>$this->post('vendorId'),
                            'prodSize'       =>$this->post('prodSize'),
                            'prodPrice'      =>$this->post('prodPrice'),
                            'coupon_value'   =>$this->post('coupon_value'),
                            'p_color'        =>$this->post('prodColor'),
                            'p_img'          =>str_replace(';','', $this->post('proimg')),
                            'cstvalue'       =>$this->post('cstvalue'),
                            'tinvalue'       =>$this->post('tinvalue'),
                            'entryvalue'     =>$this->post('entryvalue'),
                            'cst_p'         =>$this->post('cst_p'),
                            'tin_p'         =>$this->post('tin_p'),
                            'entry_p'       =>$this->post('entry_p'),
                            'getcatid'       =>$this->post('getcatid'),
                            'getskuno'       =>$this->post('getskuno'),
                            'getpromrp'      =>$this->post('getpromrp'),
                            'getcombotype'   =>$this->post('getcombotype'),
                            'getcomboid'     =>$this->post('getcomboid'),
                            'getcomboprice'  =>$this->post('getcomboprice'),
                            'getcomboqty'    =>$this->post('getcomboqty'),
                            'getcombosellprice'=>$this->post('getcombosellprice'),
                            'getcomboname'   =>$this->post('getcomboname'),
                            'getcomboimg'    =>$this->post('getcomboimg')
                          );
                     //   p($parameter);die;

      $result = $this->model_api->call_procedureRow('proc_orderpromapcombo', $parameter);
      if(!empty($result)){
          //$result   = $this->send_json($result);  
          $this->response($result, 202); 
        }
        else{
            $this->response("Insert successfully", 400); // 200 being the HTTP response code
        }
  }

  public function delgetaddressapp_get(){
      $parameter=array(
                    'act_mode' =>'deluser_shipdetailapp',
                    'row_id' =>$this->get('shipid'),
                    'pvalue' =>''
                    );
               
    $data=$this->model_api->call_procedureRow('proc_productmasterdata',$parameter);
    if($data->countid==0){
        $this->response(array('status'=>'delete successfully'), 202); 
    }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  }

  public function shipdetailapp_post(){
  
    $parameter=array(
                    'act_mode'        =>'insertshipdata',
                    'row_id'          =>'',
                    'user_regid'      =>$this->input->post('user_regid'),
                    'ship_name'       =>$this->input->post('ship_name'),
                    'bill_name'       =>$this->input->post('bill_name'),
                    'ship_address'    =>$this->input->post('ship_address'),
                    'bill_address'    =>$this->input->post('bill_address'),
                    'ship_email'      =>$this->input->post('ship_email'),
                    'bill_email'      =>$this->input->post('bill_email'),
                    'ship_pincode'    =>$this->input->post('ship_pincode'),
                    'bill_pincode'    =>$this->input->post('bill_pincode'),
                    'ship_contact'    =>$this->input->post('ship_contact'),
                    'bill_contact'    =>$this->input->post('bill_contact'),
                    'ship_state'      =>$this->input->post('ship_state'),
                    'bill_state'      =>$this->input->post('bill_state'),
                    'ship_city'       =>$this->input->post('ship_city'),
                    'bill_city'       =>$this->input->post('bill_city'),
                    'ship_cst'        =>2,
                    'bill_cst'        =>2,
                    'ship_tin'        =>1,
                    'bill_tin'        =>1,
                    'ship_createdon'  =>''
                    );
                 //$this->response($parameter, 202); exit;
    $data=$this->model_api->call_procedureRow('proc_usershipping',$parameter);
    
    if(!empty($data)){
        $this->response($data, 202); 
      }else{
        $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
      }
  }

  function paymentapp_post(){

     $paymentMod     = 'COD'; 
     $orderNumber    = 'ORD'.$this->randnumber();
     $param = array(
                    'UserId'           =>$this->post('UserId'),
                    'userregid'        =>$this->post('UserId'),
                    'UserAddressId'    =>$this->post('UserAddressId'), 
                    'orderNumber'      =>$orderNumber,
                    'totalAmount'      =>$this->post('totalAmount'), // deduct coupon amount and discount 
                    'paymentMod'       =>$paymentMod,
                    'shipcharge'       =>$this->post('shipcharge'),
                    'couponamt'        =>$this->post('couponamt'),
                    'couponcode'       =>$this->post('couponcode'),
                    'userIpaddress'    =>'', 
                    'p_device'         =>$this->post('cmid'), 
                    'usedwalletamount' =>$this->post('usedwalletamount'),
                    'p_usedrewardpoint'=>0,
                    'p_subtotal'       =>$this->post('subtotal'),
                    'p_orderfrom'      =>'mobile',
                    'totaltax'         =>$this->post('totaltax'),
                    'csttax'           =>$this->post('csttax'),
                    'tintax'           =>$this->post('tintax'),
                    'entrytax'         =>$this->post('entrytax'),
                    'u_remark'         =>$this->post('u_remark') );

    $data = $this->model_api->call_procedureRow('proc_shippingadduser',$param);
    //$grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");
    if(!empty($data)){
     $this->response($data,202); 
    }
    else{
     $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  } 

  function paymentmapapp_post(){

    $grandtotal = number_format($this->post('subamount')+$this->post('tax_total')+$this->post('shipamt'),1,".","");

    $ordmapcouponper=get_partialdiscountpercent($this->post('usercoupamt'),$grandtotal); 
    $ordmapwalletper=get_partialdiscountpercent($this->post('userwallamt'),$grandtotal);  

    $parameter = array(
                            'orderId'        =>$this->post('orderId'),
                            'proid'          =>$this->post('proid'),
                            'qty'            =>$this->post('qty'),
                            'shippingcharge' =>$ordmapwalletper,
                            'proname'        =>$this->post('proname'),
                            'vendorId'       =>$this->post('vendorId'),
                            'prodSize'       =>$this->post('prodSize'),
                            'prodPrice'      =>$this->post('prodPrice'),
                            'coupon_value'   =>$ordmapcouponper,
                            'p_color'        =>$this->post('prodColor'),
                            'p_img'          =>base_url().'images/hoverimg/'.$this->post('proimg'),
                            'cstvalue'       =>$this->post('cstvalue'),
                            'tinvalue'       =>$this->post('tinvalue'),
                            'entryvalue'     =>$this->post('entryvalue'),
                            'cst_p'         =>$this->post('cst_p'),
                            'tin_p'         =>$this->post('tin_p'),
                            'entry_p'       =>$this->post('entry_p')
                          );
                     //   p($parameter);die;

    $data = $this->model_api->call_procedureRow('proc_orderpromap',$parameter);
    if(!empty($data)){
     $this->response($data,202); 
    }
    else{
     $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  } 


  function paymentmail_post(){

         $userinfo["mailview"]=array();
         $parameter=array('act_mode'=>'maildetaila','row_id'=>$this->post('orderid'),'vusername'=>'','vpassword'=>'','vstatus'=>'');
         $record = $this->model_api->call_procedure('proc_memberlogin', $parameter);
         foreach ($record as $key => $value) {
          array_push($userinfo["mailview"], (array)$value);
         }
         
          //-------------start SMS API--------------------
              $smsphnon = $userinfo["mailview"][0]['ContactNo']; 
              $smsname = $userinfo["mailview"][0]['ShippingName'];
              
              $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9711007307, if you need help.";
              
              $smsmsgg = urlencode('Hi '.$smsname.' your order '.$userinfo["mailview"][0]['OrderNumber'].' '.$smsmsg);
              
              $file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

          //-------------end SMS API--------------------

          //------------email--------------
              $bizzgainemail="support@bizzgain.com";
              //$bizzgainemailsneha="sneha.bisht@bizzgain.com";
              //$listbizzmail=array("sneha.bisht@bizzgain.com","vishal.girdhar@bizzgain.com");
              $emailid=$userinfo["mailview"][0]['Email'];
              $msg = $this->load->view('webapp/bizzgainmailer/ordermail',$userinfo, true);
              $this->load->library('email');
              $this->email->from($bizzgainemail, 'Bizzgain.com');
              $this->email->to($emailid); 
              //$this->email->cc($listbizzmail);
              $this->email->subject('Your order has been placed - ('.$userinfo["mailview"][0]['OrderNumber'].')');
              $this->email->message($msg);
              
              if($this->email->send()){
                $actmodename='ordmailsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$userinfo["mailview"][0]['OrderId'].'/format/json/';
                $userinfomailsend = curlget($path);
                $this->response(array("status"=>"Email send successfully"),202); 
              } else {
                $actmodename='ordmailnotsend';
                $path = api_url().'productlisting/orderismailsend/modename/'.$actmodename.'/orderid/'.$userinfo["mailview"][0]['OrderId'].'/format/json/';
                $userinfomailsend = curlget($path);
                $this->response(array("status"=>"Email send fail"),400); 
              }
  
  } 


  function creditpaymentapp_post(){

     $paymentMod     = 'CREDIT'; 
     $orderNumber    = 'ORD'.$this->randnumber();
     $param = array(
                    'UserId'           =>$this->post('UserId'),
                    'userregid'        =>$this->post('UserId'),
                    'UserAddressId'    =>$this->post('UserAddressId'), 
                    'orderNumber'      =>$orderNumber,
                    'totalAmount'      =>$this->post('totalAmount'), // deduct coupon amount and discount 
                    'paymentMod'       =>$paymentMod,
                    'shipcharge'       =>$this->post('shipcharge'),
                    'couponamt'        =>$this->post('couponamt'),
                    'couponcode'       =>$this->post('couponcode'),
                    'userIpaddress'    =>'', 
                    'p_device'         =>$this->post('cmid'), 
                    'usedwalletamount' =>$this->post('usedwalletamount'),
                    'p_usedrewardpoint'=>0,
                    'p_subtotal'       =>$this->post('subtotal'),
                    'p_orderfrom'      =>'mobile',
                    'totaltax'         =>$this->post('totaltax'),
                    'csttax'           =>$this->post('csttax'),
                    'tintax'           =>$this->post('tintax'),
                    'entrytax'         =>$this->post('entrytax'),
                    'u_remark'         =>$this->post('u_remark') );

    $data = $this->model_api->call_procedureRow('proc_shippingadduser',$param);
    //$grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");
    if(!empty($data)){
     $this->response($data,202); 
    }
    else{
     $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  } 

  function chequepaymentapp_post(){

     $paymentMod     = 'CHEQUE'; 
     $orderNumber    = 'ORD'.$this->randnumber();
     $param = array(
                    'UserId'           =>$this->post('UserId'),
                    'userregid'        =>$this->post('UserId'),
                    'UserAddressId'    =>$this->post('UserAddressId'), 
                    'orderNumber'      =>$orderNumber,
                    'totalAmount'      =>$this->post('totalAmount'), // deduct coupon amount and discount 
                    'paymentMod'       =>$paymentMod,
                    'shipcharge'       =>$this->post('shipcharge'),
                    'couponamt'        =>$this->post('couponamt'),
                    'couponcode'       =>$this->post('couponcode'),
                    'userIpaddress'    =>'', 
                    'p_device'         =>$this->post('cmid'), 
                    'usedwalletamount' =>$this->post('usedwalletamount'),
                    'p_usedrewardpoint'=>0,
                    'p_subtotal'       =>$this->post('subtotal'),
                    'p_orderfrom'      =>'mobile',
                    'totaltax'         =>$this->post('totaltax'),
                    'csttax'           =>$this->post('csttax'),
                    'tintax'           =>$this->post('tintax'),
                    'entrytax'         =>$this->post('entrytax'),
                    'u_remark'         =>$this->post('u_remark') );

    $data = $this->model_api->call_procedureRow('proc_shippingadduser',$param);
    //$grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");


    if(!empty($data)){

     $param = array(
                    'act_mode'          => 'insertcheq',
                    'row_id'            => '',
                    'orderid'           => $data->OrderId,
                    'chequeno'          => $this->post('chequeno'),
                    'ifsc'              => $this->post('ifsc'),
                    'bankname'          => $this->post('bankname'),
                    'chequedate'        => $this->post('chequedate'),
                    'accno'             => $this->post('accno'),
                    'chequecollectdate' => $this->post('chequecollectdate'),
                    'chequeamount'      => $this->post('chequeamount'),
                    'paymentMod'        => $paymentMod,
                    'orderno'           => $data->OrderNumber 
                    );

     $record = $this->model_api->call_procedureRow('proc_cheque',$param);

     $this->response($data,202); 
    }
    else{
     $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  } 

  function walletpaymentapp_post(){

     $paymentMod     = 'ONLINE'; 
     $orderNumber    = 'ORD'.$this->randnumber();
     $param = array(
                    'UserId'           =>$this->post('UserId'),
                    'userregid'        =>$this->post('UserId'),
                    'UserAddressId'    =>$this->post('UserAddressId'), 
                    'orderNumber'      =>$orderNumber,
                    'totalAmount'      =>$this->post('totalAmount'), // deduct coupon amount and discount 
                    'paymentMod'       =>$paymentMod,
                    'shipcharge'       =>$this->post('shipcharge'),
                    'couponamt'        =>$this->post('couponamt'),
                    'couponcode'       =>$this->post('couponcode'),
                    'userIpaddress'    =>'', 
                    'p_device'         =>$this->post('cmid'), 
                    'usedwalletamount' =>$this->post('usedwalletamount'),
                    'p_usedrewardpoint'=>0,
                    'p_subtotal'       =>$this->post('subtotal'),
                    'p_orderfrom'      =>'mobile',
                    'totaltax'         =>$this->post('totaltax'),
                    'csttax'           =>$this->post('csttax'),
                    'tintax'           =>$this->post('tintax'),
                    'entrytax'         =>$this->post('entrytax'),
                    'u_remark'         =>$this->post('u_remark') );
     
    $data = $this->model_api->call_procedureRow('proc_shippingadduser',$param);
    //$grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");
    if(!empty($data)){
     $this->response($data,202); 
    }
    else{
     $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  }

  function onlinepaymentapp_post(){

     $paymentMod     = 'ONLINE'; 
     $orderNumber    = 'ORD'.$this->randnumber();
     $param = array(
                    'UserId'           =>$this->post('UserId'),
                    'userregid'        =>$this->post('UserId'),
                    'UserAddressId'    =>$this->post('UserAddressId'), 
                    'orderNumber'      =>$orderNumber,
                    'totalAmount'      =>$this->post('totalAmount'), // deduct coupon amount and discount 
                    'paymentMod'       =>$paymentMod,
                    'shipcharge'       =>$this->post('shipcharge'),
                    'couponamt'        =>$this->post('couponamt'),
                    'couponcode'       =>$this->post('couponcode'),
                    'userIpaddress'    =>'', 
                    'p_device'         =>$this->post('cmid'), 
                    'usedwalletamount' =>$this->post('usedwalletamount'),
                    'p_usedrewardpoint'=>0,
                    'p_subtotal'       =>$this->post('subtotal'),
                    'p_orderfrom'      =>'mobile',
                    'totaltax'         =>$this->post('totaltax'),
                    'csttax'           =>$this->post('csttax'),
                    'tintax'           =>$this->post('tintax'),
                    'entrytax'         =>$this->post('entrytax'),
                    'u_remark'         =>$this->post('u_remark') );

    $data = $this->model_api->call_procedureRow('proc_shippingadduser',$param);
    //$grandtotal = number_format($totalamount+$tax_total+$shipppamt,1,".","");
    if(!empty($data)){
     $this->response($data,202); 
    }
    else{
     $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }

  } 

  public function randnumber(){
    srand ((double) microtime() * 1000000);
    $random5 = rand(10000,99999);
    return $random5;
  }   


  public function updateappcart_post(){
    
    $param = array('act_mode' => 'update_appcart_pro',
                      'discount_name' => '',
                      'discount_value' => '', 
                      'discount_type' => '', 
                      'purchase_amount' => '', 
                      'is_coupon' => $this->input->post('quantity'), 
                      'coupon' => '', 
                      'is_reusable' => $this->input->post('productid'), 
                      'reusable_limit' => $this->input->post('userid'), 
                      'valid_from' => '', 
                      'valid_to' => '', 
                      'coupon_user_type' => '',
                      'row_id' => '',
                      'brand_list' => '',
                      'product_type' => '',
                      'parent_id' => ''
                      ); 
      
    $data = $this->model_api->call_procedureRow('proc_discount_cart', $param);

  	if(!empty($data)){
     $this->response(array("status"=>"cart update successfully"),202); 
    }
    else{
     $this->response("Something Went Wrong", 400); // 200 being the HTTP response code
    }
  } 


}//end class
?>