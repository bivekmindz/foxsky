<?php $this->load->view('helper/nav')?> 
<style type="text/css">
.download-left{
    width:150px;
    float: left;
    height: auto;
    margin-left: 10px;

}
.download-left input{
    width: 100%;
    text-align: center;
}
.download-right{
    width:150px;
    float: right;
    height: auto;
        margin-right: 10px;
    
}
.download-right input{
    width: 100%;
    text-align: center;
}
.delete-left{
    float: left;
    width: 24px;
    height: auto;
}
.delete-right{
    float: right;
    
    padding: 0;
    width: 46px;
}
.grid_tbl td {
    border-bottom: 1px solid #d8e8f5;
    border-right: 1px solid #d8e8f5;
    color: #676767;
    font-size: 13px;
    padding: 6px 4px;
}


</style>

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
          <form action="" method="post" enctype="multipart/form-data" >
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Discount List</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can View the  Discount List.</p>
                        </div>
                    </div>

                    <div class="page_box">
                     <form method="post" action="">
                                 <div class="download-left"> <!--<input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel"> -->
                                </div></form>
                      <form method="post" action="<?php echo base_url().'admin/discount/cart_discount'; ?>">
                                 <div class="download-right"> <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Add Cart Discount" name="newsexcel">
                                </div></form>           
                <!-- <table id="filterTable" width="100%" class="grid_tbl">
                    <tr>
                        <td width="22%"><input type="text" name="coupon_code" value="" placeholder="Enter Coupon Code/Name/Type" style="width:97%; padding:8px 2px; border:1px solid #ccc;"></td>
                        <td width="22%"><input type="text" name="start_date" class="datepicker" value="" placeholder="Enter start date" style="width:97%; padding:8px 2px; border:1px solid #ccc;" readonly="true"></td>
                        <td width="22%"><input type="text" name="end_date" class="datepicker" value="" placeholder="Enter end date" style="width:97%; padding:8px 2px; border:1px solid #ccc;" readonly="true"></td>
                        <td width="22%">
                            <select name="Status" style="width:100%;">
                                <option value="">Select Status</option>
                                <option value="A">Active</option>
                                <option value="I">In-Active</option>
                            </select>
                        </td>
                        <td><input type="button" name="filter" value="Filter" onclick="get_filtered_discount();"></td>
                    </tr>
                </table> -->
            </div>
         <div id="title_option">
            <table id="test" class="grid_tbl">
                <thead>
                    <tr>
                        <th scope="col">So.No.</th>
                        <th scope="col">Coupon Code</th>
                        <th scope="col">Coupon User Type</th>
                        <th scope="col">Discount Name</th>
                        <th scope="col">Discount Type</th>
                       <th scope="col">Discount Value</th> 
                        <!-- <th scope="col">Purchase Amount</th> -->
                        <th scope="col">Valid From</th>
                        <th scope="col">Valid to</th>
                        <th scope="col">Reusable Limit</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">
                    <?php
                    $i = 1;
                    foreach ($brands_detail AS $list) {

                     
                        ?>
                        <tr>
                            <td data-rel="so.no"><?php echo $i; ?></td>
                            <td data-rel="so.no"><?php echo $list->coupon; ?></td>
                            <td data-rel="so.no"><?php echo ucfirst(strtolower($list->coupon_user_type)); ?></td>
                            <td data-rel="so.no"><?php echo ucwords(strtolower($list->discount_name)); ?></td>
                            <td data-rel="Pi ID"><?php echo ucfirst(strtolower($list->discount_type)); ?></td>
                            <td data-rel="S.No"><?php echo $list->discount_value; ?></td> 
                            <!-- <td data-rel="Pi Code"><?php echo $list->purchase_amount; ?></td> -->
                            <td data-rel="Supplier Number"><?php echo $list->valid_from; ?></td>
                            <td data-rel="Supplier Number"><?php echo $list->valid_to; ?></td>
                            <td data-rel="Supplier Number"><?php echo $list->reusable_limit; ?></td>
                            
                        <?php 
                       # echo $list->DiscStatus == 'A' ? 'Active' : 'In-Active';

                         ?>


         <!-- <a href="<?php echo base_url(); ?>admin/discount/discount_new_activeinactive/<?=$list->Copid?>/<?php print ($list->DiscStatus=='I') ? 'active':'inactive';?>"
          <?php if($list->DiscStatus=='A'){ ?>class="inactive" <?php } else { ?>class="active_button"  <?php } ?>
          > <?php print ($list->DiscStatus=='I') ? 'Inactive':'Active';?></a> -->





                         </td>
                            <td data-rel="Action">
                                <?php if($list->status == 1) {  ?>
                                <a class="delete-right" title="Delete" href="<?php echo base_url();?>admin/discount/cart_discount_inactive/<?=$list->id?>" onclick="return confirm('Are you sure you want to Inactive this?');" >Active</a>
                                <?php } else if($list->status == 0) { ?>
                                <div class="delete-right"><a class="delete" title="Delete" href="<?php echo base_url();?>admin/discount/cart_discount_active/<?=$list->id?>" onclick="return confirm('Are you sure you want to Active this?');" >Inactive</a></div>
                                <?php } ?>
                                <div class="delete-left"><a class="delete" title="Edit" href="<?php echo base_url();?>admin/discount/cart_discount_edit/<?=$list->id?>"><i class="fa fa-pencil"></i></a>
            </div>                </td>


                        </tr>
    <?php $i++;
} ?>

                </tbody>
            </table>
        </div>
        </div>
</section>
<script>
    $(function () {
        $(".datepicker").datepicker({
            altField: "#alternate",
            altFormat: "DD, d MM, yy",
            dateFormat: "yy-mm-dd"
        });
    });

    function get_filtered_discount() {
        var baseurl = "<?php echo base_url(); ?>";
        var Coupon = $('input[name=coupon_code]').val();
        var sdt = $('input[name=start_date]').val();
        var edt = $('input[name=end_date]').val();
        var status = $('select[name=Status]').val();
        var filter = new Array();
        filter = {'Coupon': Coupon, 'start': sdt, 'end': edt, 'status': status};
        $.ajax({
            url: baseurl + "admin/discount/filterDiscount",
            type: "POST",
            data: {'data': filter},
            success: function (data) {
                //alert(data); return false;
                $('#tblBody').html(data);
            }
        });
    }

</script>