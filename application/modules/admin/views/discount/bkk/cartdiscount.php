<script type="text/javascript" src="<?php echo admin_url();?>js/discount.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> 
<style>
  .coupon{display: none;}
  .username{display: none;} 
</style>
<section class="main_container">  
 <script>
  $(function() {
   $(".datepicker").datepicker({
    altField: "#alternate",
    altFormat: "DD, d MM, yy",
    dateFormat:"yy-mm-dd",
    minDate: 0
   });
  });
 </script>


  <script language="javascript">
        $(document).ready(function () {
            $("#mydate2").datepicker({
               format: "dd/mm/yyyy"
            });
        });
    </script>

<div id="overlay" class="web_dialog_overlay"></div>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
          <form action="" method="post" enctype="multipart/form-data" >
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Discount</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can add various discounts on category, brand and products.</p>
                        </div>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Discount Name</div>
</div>
<div class="col-lg-8">
<div class="tbl_input"><input type="text" name="discountname" value=""  min='1' required class="required" field="Discount Name"> </div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text"></div>
</div>
<div class="col-lg-8">
<div class="submit_tbl"> </div>
</div>
</div>
</div>
</div>


  <div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Discount value</div>
</div>
<div class="col-lg-4">
<div class="tbl_input"> <input type="number" onkeypress="return isNumberKey(event)" name="discount" value=""  min='1' required class="required" field="Discount Amount"></div>
</div>
<div class="col-lg-4">
<div class="tbl_input"><select name="disType">
			<option value="fixed">Fixed</option>
  			<option value="percentage">Percentage</option>
  		</select> </div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Purchase value</div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <input type="number" name="min_purchase" value="" onkeypress="return isNumberKey(event)" required class="required" field="Discount Amount"></div>
</div>
</div>
</div>
</div>


  <div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Date Between</div>
</div>
<div class="col-lg-4">
<div class="tbl_input"><input type="text" name="from" value="" class="datepicker" placeholder="Start Date"  readonly='true'> </div>
</div>
<div class="col-lg-4">
<div class="tbl_input"> <input type="text" name="to" value="" class="datepicker" placeholder="End Date"  readonly='true'></div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Re-Usable</div>
</div>
<div class="col-lg-8">
<div class="tbl_input">  <input type="checkbox" value="1" name="reusable">
       <input type="text" name="usableLimit" value="" placeholder="Reusable Limit" style="width:94%;float:right;" class="required" field="Re-Usable" readonly></div>
</div>
</div>
</div>
</div>


  <div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Coupon</div>
</div>
<div class="col-lg-8">
<div class="tbl_input"><input type="checkbox" name="coupon" value="1" onchange="generateCode($(this));">
        <input type="text" name="coupon_code" value="" readonly style="width:94%;float:right;"> </div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Coupon For User</div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <input type="text" name="CouponUser" value="" > </div>
</div>
</div>
</div>
</div>
<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text"><input type="radio" value="categories" name="apply" onchange="getList($(this));">Categories </div>
</div>
<div class="col-lg-4">
<div class="tbl_text"> <input type="radio" value="brands" name="apply" onchange="getList($(this));">Brand </div>
</div>
<div class="col-lg-4">
<div class="tbl_text"><input type="radio" value="product" name="apply" onchange="getList($(this));">Products </div>
</div>
</div>
</div>


<div class="gridview">
  <div style="float:right; padding:5px;">
  	<input class="hbutton trans mrgnt5" name="save" value="Save" type="button">
  </div>
<table width="95%">
<tr>
<td width="15%"></td>
 <td width="35%">
</td></tr>
  <tr>
  	<td width="15%"></td>
  	<td width="35%">
  		
  	</td>
    <td width="15%"></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>
        
        
    </td>
    <td></td>
    <td>
      
    </td>
  </tr>
  <tr> 
    <!-- <td>
    specific user: 
     <input type="checkbox" name="getuserlist" value="1" onchange="userlistget($(this));">
    </td> -->
  </tr>  
  <tr>
    <td></td>
    <td>
        
    </td>
    <td></td>
    <td>
     
    </td>
  </tr>

  <tr> 
        <td style="text-align:left"> 
          
        </td> 
  </tr> 

<tr> 
        <td style="text-align:left"> 
         
        </td> 
  </tr>

  <tr> 
        <td style="text-align:left"> 
          
        </td> 
  </tr> 

  <tr>
  <td colspan='4'>
  <br>
     <span id="user"></span>
  </td>
  </tr>
   <tr>
      <td id="discountTable" colspan='4'>   

    </tr>
    
    <tr>      
    <td id="subdiscounttable" colspan='4'></td>
    </tr>
    <tr>
        
    <td id="childsubdiscounttable" colspan='4'></td>

    </tr>
   <!--  <tr>
        
    <td id="products" colspan='4'></td>

    </tr> -->
    
</table>
</div>
</div>
</section>

<script>
var baseurl = "<?php echo base_url();?>";
$('input[name=reusable]').on('change', function() {
	if($(this).prop('checked')) {
		$('input[name=usableLimit]').removeAttr('readonly');
	} else {
		$('input[name=usableLimit]').val('');
		$('input[name=usableLimit]').attr('readonly','true');
	}
});

$('input[name=save]').on('click',function() {


var sThisVal = new Array(); 
var sThisValbrandid = new Array(); 

var thisvalproductid = new Array();


   $('input:checkbox[name=subcategorychildname]').each(function () {
           //sThisVal = (this.checked ? $(this).val() : "");
           if(this.checked){
                sThisVal.push($(this).val());
            }
        });

   var categoryid = sThisVal.join(', ');

 $('input:checkbox[name=brandname]').each(function () {
           //sThisVal = (this.checked ? $(this).val() : "");
           if(this.checked){
                sThisValbrandid.push($(this).val());
            }
        });
  var brandproductid = sThisValbrandid.join(', ');


$('input:checkbox[name=products]').each(function () {
           //sThisVal = (this.checked ? $(this).val() : "");
           if(this.checked){
                thisvalproductid.push($(this).val());
            }
        });
  var productid = thisvalproductid.join(',');


 //alert(brandproductid);

if(categoryid=='')
{
  var discoutonval = 'all';
}else{
  var discoutonval = '';
}

	var err = validation();
	if(!err) {
		var disAmt = $('input[name=discount]').val();
		var disTyp = $('select[name=disType]').val();
		var purAmt = $('input[name=min_purchase]').val();
		var startDate = $('input[name=from]').val();
		var endDate = $('input[name=to]').val();
		var coupon = $('input[name=coupon_code]').val();
		var limit = $('input[name=CouponUser]').val();
		var reuse = $('input[name=usableLimit]').val();
		var usableLimit = $('input[name=usableLimit]').val();
    var discountna = $('input[name=discountname]').val();
    
		  $.ajax({
		    url:baseurl+"admin/discount/saveCartDisc",
		    type: "POST",
		    data: {'discname':discountna,'disAmt':disAmt,'disTyp':disTyp,'purAmt':purAmt,'startDate':startDate,'endDate':endDate,'coupon':coupon,'limit':limit,'reuse':reuse,'usableLimit':usableLimit,'categorydiscountid':categoryid,'discountonvalue':discoutonval,'discountbrandproduct':brandproductid,'productid':productid},
		    success:function(data) {
          //alert(data); return false;
          //alertify.success("Discount Add successfully");
		    	window.location.href = baseurl+"admin/discount/discountlist";
		    }
		  });
	}
});

function generateCode(elem) {

  if(elem.prop('checked')) {
    $.ajax({
      url:baseurl+"admin/discount/get_unique_code",
      type: "POST",
      data: {},
      success:function(data) {
        //alert(data);
    	$('input:text[name=coupon_code]').val(data);
      }
    });
  } else {
    $('input:text[name=coupon_code]').val('');
  }
}

function validation() {
	var error = false;
	$('.required').each(function() {
		if(!$(this).val()) {
			alert($(this).attr('field') + ' is Missing!');
			$(this).focus();
			error = true;
			return false;
		}
	});
	return error;
}

function getList(elem) { 
        
        $.ajax({ 
            url: baseurl + "admin/discount/getList", 
            type: "POST", 
            data: {'type' : elem.val()}, 
            success: function (data) { 
                
                $("#products").empty();
                $("#childsubdiscounttable").empty();
                $("#subdiscounttable").empty();
                //alert(data); return false;
                $('#discountTable').html(data); 
                //alert(data);
                /*if (elem.val() == 'product') { 
                    $('#products').html(data); 
                } else {

                    $('#discountTable').html(data); 
                }*/ 
            } 
        }); 
    } 


</script>