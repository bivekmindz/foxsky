<?php $this->load->view('helper/nav')?> 
<style type="text/css">
.download-left{
    width:150px;
    float: left;
    height: auto;
    margin-left: 10px;

}
.download-left input{
    width: 100%;
    text-align: center;
}
.download-right{
    width:150px;
    float: right;
    height: auto;
        margin-right: 10px;
    
}
.download-right input{
    width: 100%;
    text-align: center;
}
.delete-left{
    float: left;
    width: 24px;
    height: auto;
}
.delete-right{
    float: right;
    
    padding: 0;
    width: 46px;
}
.grid_tbl td {
    border-bottom: 1px solid #d8e8f5;
    border-right: 1px solid #d8e8f5;
    color: #676767;
    font-size: 13px;
    padding: 6px 4px;
}


</style>

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
          <!-- <form action="" method="post" enctype="multipart/form-data" > -->
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>First Purchase Offer List</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can View the  Discount List.</p>
                        </div>
                    </div>

                    <div class="page_box">
                         
                     <form method="post" action="">
                                 <div class="download-left"> <!--<input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel"> -->
                                </div></form>
                      <form method="post" action="<?php echo base_url().'admin/discount/first_discount'; ?>">
                                 <div class="download-right"> <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Add First Purchase" name="newsexcel">
                                </div></form>           
                
            
            <div class="col-lg-12 col-md-12" style="padding-top:15px;">
            <table id="search_filter" class="grid_tbl">
                <thead>
                    <tr>
                        <th scope="col">S.No.</th>
                        <th scope="col">Discount Name</th>
                        <th scope="col">Discount Type</th>
                        <th scope="col">Discount Value</th>
                        <th scope="col">Fixed Discount Price</th>
                        <th scope="col">Purchase Amount (Min)</th>
                        <th scope="col">Purchase Amount (Max)</th>
                        <th scope="col">Valid From</th>
                        <th scope="col">Valid to</th>
                        <th scope="col">From Time</th>
                        <th scope="col">To Time</th>
                        <th scope="col">Updated By</th>
                        <th scope="col">Updated Date</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">
                    <?php
                    $i = 1;
                    foreach ($cart_detail AS $list) { #p($list);

                     
                        ?>
                        <tr>
                            <td data-rel="so.no"><?php echo $i; ?></td>
                            <td data-rel="so.no"><?php echo ucwords(strtolower($list->discount_name)); ?></td>
                            <td data-rel="Pi ID"><?php echo ucfirst(strtolower($list->discount_type)); ?></td>
                            <td data-rel="S.No"><?php echo $list->discount_value; ?></td> 
                            <td data-rel="S.No"><?php echo $list->fixed_discount_price; ?></td> 
                            <td data-rel="Pi Code"><?php echo $list->purchase_amount; ?></td>
                            <td data-rel="Pi Code"><?php echo $list->purchase_amount_max; ?></td>
                            <td data-rel="Supplier Number"><?php echo $list->valid_from; ?></td>
                            <td data-rel="Supplier Number"><?php echo $list->valid_to; ?></td>
                            <td data-rel="Supplier Number"><?php echo $list->valid_from_time; ?>:00</td>
                            <td data-rel="Supplier Number"><?php echo $list->valid_to_time; ?>:00</td>
                    <td><?php if(empty($list->modifiedby)){ echo $list->ausername; } else { echo $list->rusername; } ?></td> 
                    <td><?php if(empty($list->modifiedby)){ echo $list->createdon; } else { echo $list->modifiedon; } ?></td>
                       

                         </td>
                            <td data-rel="Action">
                                  <?php if($list->status == 1) {  ?>
                                <a class="delete-right" title="Delete" href="<?php echo base_url();?>admin/discount/first_active_inactive/<?=$list->id?>/0" onclick="return confirm('Are you sure you want to Inactive this?');" >Active</a>
                                <?php } else if($list->status == 0) { ?>
                                <div class="delete-right"><a class="delete" title="Delete" href="<?php echo base_url();?>admin/discount/first_active_inactive/<?=$list->id?>/1" onclick="return confirm('Are you sure you want to Active this?');" >Inactive</a></div>
                                <?php } ?>
                                <div class="delete-left"><a class="delete" title="Edit" href="<?php echo base_url();?>admin/discount/first_discount_edit/<?=$list->id?>"><i class="fa fa-pencil"></i></a>
            </div>                </td>


                        </tr>
    <?php $i++;
} ?>

                </tbody>
            </table>
            </div>
            </div>
        </div>
</section>