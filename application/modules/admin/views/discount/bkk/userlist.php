<div class="table_safe" style="width: 27%;
  height: 290px;
  overflow: auto;">
<table id="user_list">
	<thead><th>action</th><th>user Email</th></thead>
	<tbody>
	<?php foreach ($userlist as $key => $value) { ?>
		<tr>
		<td><input type="checkbox" name="user_select" value="<?php echo $value->UserRegId; ?>" ></td>
		<td><?php echo $value->EmailId ?></td>

	</tr>

	<?php } ?>
	
	</tbody>


</table>
</div>

<script type="text/javascript">

$(document).ready(function() {
  $('#userlist').DataTable( {
    "aoColumnDefs": [
          {'bSortable':false, 'aTargets':[4]}
       ],
    initComplete: function () {
      this.api().columns().every( function () {
           if(this[0]==4) {
            var column = this;
            var select = $('<select style="width:200px;"><option value=""></option></select>')
              .appendTo($('#filterPinCode'))
              .on( 'change', function () {
                var val = $.fn.dataTable.util.escapeRegex(
                  $(this).val()
                );
                //alert(select);
                column.search( val ? '^'+val+'$' : '', true, false ).draw();
              });

            column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' )
            });
        }
      });
    }
  });
});
</script>