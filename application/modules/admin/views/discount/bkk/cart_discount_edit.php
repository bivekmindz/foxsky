<script type="text/javascript" src="<?php echo admin_url(); ?>js/discount.js"></script>

<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> 

<style>
    .coupon{display: none;}
    .username{display: none;} 
</style>
 <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#regdiscount").validate({
        // Specify the validation rules
        rules: {
            
            discountname  : "required",
            discount : "required",
            min_purchase : "required",
            max_purchase : "required",
            fixed_discount_price : "required",
            valid_from : "required",
            valid_to : "required",
            usableLimit : "required",
            coupon_code : "required",
            CouponUser : "required",
            regFrom : "required",
            regTo : "required",
           /* email:{
                required: true,
                email: true
            },

,,,,regTo

            password: {
                required: true,
                minlength: 5
            },
            agree: "required" */
        },
        // Specify the validation error messages
        messages: {
            discountname: "Discount name required",
            discount: "Discount required",
            min_purchase: "Min Purchase Id required",
            max_purchase: "Max Purchase Id required",
            fixed_discount_price: "Fixed discount price is required ",
             valid_from: "From required",
              valid_to: "To required",
            usableLimit   : "Usable required",
            coupon_code   : "Coupon Code required" 
            CouponUser   : "Coupon User  required" 
            regFrom   : "Reg From  required" 
            regTo   : "Reg To Id required" 
            
            /*password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            email: "Please enter a valid email address",
            agree: "Please accept our policy"*/
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
   
  </script>
<section class="main_container">  
    <script>
        $(function () {
            $(".datepicker").datepicker({
                altField: "#alternate",
                altFormat: "DD, d MM, yy",
                dateFormat: "yy-mm-dd",
                minDate: 0
            });
        });
    </script> 

   <?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
          <form action="" id="cartdiscount" method="post" enctype="multipart/form-data" >
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Edit Cart Discount</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add Registration discount with coupon code.</p>
                        </div>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
                    <div class="col-lg-12">
         

      </div>
</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Discount Name <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-8">
<div class="tbl_input"><input type="text" id="discountname" name="discountname" value="<?php echo ucwords(strtolower($detail->discount_name)); ?>" min='1' required class="required" field="Discount Name"></div>
</div>

</div>
</div>

<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Coupon Code <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <input type="checkbox" name="is_coupon" value="1"  <?php if($detail->is_coupon) { ?> checked <?php } ?>  onchange="generateCode($(this));">
                        <input type="text" id="coupon_code" field="Coupon Code" class="required" name="coupon_code" value="<?php if($detail->is_coupon) { echo $detail->coupon; }?>" readonly style="width:94%;float:right"></div>
</div>
</div>
</div>


</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Discount value <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-4">
<div class="tbl_input"><input type="number" onkeypress="return isNumberKey(event);" id="discount" name="discount" value="<?php echo $detail->discount_value; ?>"  min='1' required class="required" field="Discount Amount"></div>
</div>
<div class="col-lg-4"><div class="tbl_input">
  <select name="disType" id="disType">
                            <option value="fixed" <?php if($detail->discount_type == "fixed") { ?> selected <?php  } ?> >Fixed</option>
                            <option value="percentage" <?php if($detail->discount_type == "percentage") { ?> selected <?php  } ?> >Percentage</option>
                        </select>
</div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4"><div class="tbl_text">Maximum Discount Price <span style="color:red;font-weight: bold;">*</span></div></div>
<div class="col-lg-8"><div class="tbl_input">
<input type="number" id="fixed_discount_price" name="fixed_discount_price" onkeypress="return isNumberKey(event);" value="<?php echo $detail->fixed_discount_price; ?>" required class="required" field="Fixed Discount Price">
</div></div>
</div>
</div>
</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4"><div class="tbl_text">Purchase value (Min) <span style="color:red;font-weight: bold;">*</span></div></div>
<div class="col-lg-8"><div class="tbl_input">
<input type="number" id="min_purchase" name="min_purchase" onkeypress="return isNumberKey(event);" value="<?php echo $detail->purchase_amount; ?>" required class="required" field="Discount Amount (Min)">
</div></div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4"><div class="tbl_text">Purchase value (Max) <span style="color:red;font-weight: bold;">*</span></div></div>
<div class="col-lg-8"><div class="tbl_input">
<input type="number" id="max_purchase" name="max_purchase" onkeypress="return isNumberKey(event);" value="<?php echo $detail->purchase_amount_max; ?>" required class="required" field="Discount Amount (Max)">
</div></div>
</div>
</div>
</div>
<?php // p($detail); die; ?>
<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Valid Between <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-4">
<div class="tbl_input"><input type="text" id="valid_from" name="valid_from" value="<?php echo $detail->valid_from; ?>" field="Valid From Date" class="datepicker required" placeholder="Start Date" readonly='true'></div>
</div>
<div class="col-lg-4"><div class="tbl_input">
 <input type="text" id="valid_to" name="valid_to" value="<?php echo $detail->valid_to; ?>" class="datepicker required" field="Valid To Date" placeholder="End Date"  readonly='true'>
</div></div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Re-Usable</div>
</div>
<div class="col-lg-8">
<div class="tbl_input">
<input type="checkbox" value="1" <?php if($detail->is_reusable) { ?> checked <?php } ?> name="is_reusable" >
                        <input type="text" id="usableLimit" name="usableLimit" value="<?php if($detail->is_reusable) { echo $detail->reusable_limit; } ?>" style="width:94%;float:right"  field="Re-Usable" placeholder="Reusable Limit" onkeypress="return isNumberKey(event);" />
</div>
</div>
</div>
</div>
</div>

<div class="sep_box">

<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Valid Between Time<span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-4">
<div class="tbl_input">
<select id="valid_from_time" name="valid_from_time">
    <?php for($i=0;$i<=23;$i++) {  
            if($i < 10)
            {
                $v='0'.$i;
            }
            else
            {
                $v=$i;
            }?>
    <option <?php if($v==$detail->valid_from_time) echo 'selected'; ?> value="<?php echo $v; ?>"><?php echo $v; ?>:00</option>
    <?php } ?>
</select>
</div>
</div>
<div class="col-lg-4"><div class="tbl_input">
<select id="valid_to_time" name="valid_to_time">
    <?php for($i=0;$i<=23;$i++) { 
            if($i < 10)
            {
                $v='0'.$i;
            }
            else
            {
                $v=$i;
            } ?>
    <option <?php if($v==$detail->valid_to_time) echo 'selected'; ?> value="<?php echo $v; ?>"><?php echo $v; ?>:00</option>
    <?php } ?>
</select>

</div></div>
</div>
</div>

<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Coupon For User Type</div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <select name="cpn_user_type" id="cpn_user_type">
                            <option value="retail">Consumer</option>
                        </select></div>
</div>
</div>
</div>
</div>
<!--
<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Registration Between</div>
</div>
<div class="col-lg-4">
<div class="tbl_input"> <input type="text" id="regFrom" name="regFrom" value="" class="datepicker required" field="Registration Start Data"  placeholder="Registration From"  readonly='true'></div>
</div>
<div class="col-lg-4">
<div class="tbl_input"> <input type="text" id="regTo" name="regTo" value="" class="datepicker required" field="Registration Last Data"  placeholder="Registration To"  readonly='true'></div>
</div>
</div>
</div>
</div>
 -->

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text"></div>
</div>
<div class="col-lg-8">
<div class="submit_tbl">  <input class="btn_button sub_btn" id="save" name="save" value="Save" type="button"></div>
</div>
</div>
</div>
</div>



         <!--    <table width="95%">
                <tr>
                    <td width="15%"></td>
                    <td width="35%">
                    </td></tr>
                <tr>
                    <td width="15%"></td>
                    <td width="35%">
                      
                    </td>
                    <td width="15%"></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        
                       
                    </td>
                    <td></td>
                    <td>
                        
                    </td>
                </tr>
                 <tr>
                    <td></td> 
                    <td>
                       
                    </td>
                    <td></td>
                    <td>
                        
                    </td>
                </tr> 
                 <tr>
                    <td>specific user</td>
                    <td>
                        <input type="checkbox" name="getuserlist" value="1" onchange="userlistget($(this));">
                        </td>
                    <td></td>
                    <td>
                    </td>
                </tr> 
                <tr>
                    <td></td>
                    <td>
                       
                       
                    </td>
                   <!--  <td>Region</td>
                    <td>
                        <select name="region">
                            <?php foreach ($cities as $key => $val) { ?>
                                <option value="<?php echo $val->CityId; ?>"><?php echo $val->CityName; ?></option>
                            <?php } ?>
                        </select>
                    </td> -->
              <!--     </tr>
            </table>
         <tr>
  <td colspan='4'>
  <br>
     <span id="user"></span>
  </td>
  </tr>-->
        </div>
    </div>
</section>

<script>





function userlistget(elem){
    if (elem.prop('checked')) {
     var baseurl = "<?php echo base_url(); ?>";
         $.ajax({
         url: baseurl + "admin/discount/getalluser",
            type: "POST",
            data:{"id":"id"},
                 success: function (data) {
                $('#user').html(data);
            }
        });
    }
    else{
         $('#user').html('');
    }
}

    var baseurl = "<?php echo base_url(); ?>";
    $('input[name=is_reusable]').on('change', function () {
        if ($(this).prop('checked')) {
            $('input[name=usableLimit]').removeAttr('readonly');
        } else {
            $('input[name=usableLimit]').val('');
            $('input[name=usableLimit]').attr('readonly', 'true');
        }
    });
    
    $('select[name=disType]').change(function(){
        if($(this).val() == 'percentage') {
            $('input[name=discount]').attr('max', 100);
            var discount = $('input[name=discount]').val();
            if(discount > 100) {
                $('input[name=discount]').val('100');
            }
        }
    });

    $('input[name=save]').on('click', function () {

        var favorite = new Array(); 
            $.each($("input[name='user_select']:checked"), function(){            
                favorite.push($(this).val());
            });

    var t=favorite.length;
    if(t==0){
        favorite=0;
    }

        var err = validation();
       
        //var err = false;
          if (!err) {
            var discount_value = $('input[name=discount]').val();
            var discount_type = $('select[name=disType]').val();
            var purchase_amount = $('input[name=min_purchase]').val();
            var purchase_amount_max = $('input[name=max_purchase]').val();
            var fixed_discount_price = $('input[name=fixed_discount_price]').val();
            var coupon = $('input[name=coupon_code]').val();
            var is_reusable = $('input[name="is_reusable"]:checked').val();
            var is_coupon = $('input[name="is_coupon"]:checked').val();
            var reusable_limit = $('input[name=usableLimit]').val();
            var discount_name = $('input[name=discountname]').val();
            var valid_from = $('input[name=valid_from]').val();
            var valid_to = $('input[name=valid_to]').val();
            var valid_from_time = $('select[name=valid_from_time]').val();
            var valid_to_time = $('select[name=valid_to_time]').val();
            var coupon_user_type = $( "#cpn_user_type option:selected" ).val();
            var cart_discount_id = '<?php echo $this->uri->segment(4,0); ?>';
            
             if(discount_type == "percentage" && (discount_value >=100 || discount_value <= 0))
            {
               alert('Percentage should be between 0 to 100');
               return false;
            }
            if(Date.parse(valid_from) >Date.parse(valid_to))
            {
                alert('Valid from date should be less than or equal to Valid to date');
                return false;   
            } 

            is_reusable = is_reusable ? 1: 0;
            is_coupon = is_coupon ? 1: 0;
            
            $.ajax({
                url: baseurl + "admin/discount/editCartDisc",
                type: "POST",
                data: {
                        'cart_discount_id' : cart_discount_id,
                        'discount_name' : discount_name,
                        'discount_value': discount_value, 
                        'fixed_discount_price': fixed_discount_price,
                        'discount_type': discount_type, 
                        'purchase_amount': purchase_amount,
                        'purchase_amount_max': purchase_amount_max, 
                        'is_coupon': is_coupon, 
                        'coupon': coupon, 
                        'is_reusable': is_reusable, 
                        'reusable_limit': reusable_limit, 
                        'valid_from': valid_from, 
                        'valid_to': valid_to, 
                        'valid_from_time': valid_from_time, 
                        'valid_to_time': valid_to_time, 
                        'coupon_user_type': coupon_user_type, 
                },
                success: function (data) {
                   //alert(data);
                    //console.log(data); return false;
                    //alertify.success("Discount Added successfully");
                    window.location.href = baseurl + "admin/discount/cart_discount_list";
                }
            });
        }
      
    });

    function generateCode(elem) {
        if (elem.prop('checked')) {
            $.ajax({
                url: baseurl + "admin/discount/get_unique_code_bizz",
                type: "POST",
                data: {},
                success: function (data) {
                    $('input:text[name=coupon_code]').val(data);
                }
            });
        } else {
            $('input:text[name=coupon_code]').val('');
        }
    }

    function set_unset_reusable_limit(){
         if (elem.prop('checked')) {
           
        } else {
            $('input:text[name=usableLimit]').val('');
        }
    }

    function validation() {
        var error = false;
        $('.required').each(function () {
            if (!$(this).val()) {
                alert($(this).attr('field') + ' is Missing!');
                $(this).focus();
                error = true;
                return false;
            }
        });
        return error;
    }
    function isNumberKey(evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode;

        // Added to allow decimal, period, or delete
        if (charCode == 110 || charCode == 190 || charCode == 46)
            return true;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>