
<select id="p_category" name="p_category">
	<option value="none">Select any Category</option>
	<?php foreach($list as $key => $val) { ?>
	<option value="<?php echo $val['id']; ?>"><?php echo $val['name']; ?></option>
	<?php } ?>
</select>
<div>
<select id="category" name="category">
	<option value="none">Select any Sub Category</option>
</select>
</div>
<div id="sub_category" name = "sub_category[]">
	<select id="sub_category" name="sub_category">
		<option value="none">Select any Child Sub Category</option>
	</select>
</div>
<script>
	$(document).ready(function(){
		$('#p_category').on('change', function(){
			  $.ajax({ 
            			url: baseurl + "admin/discount/hm_get_sub_category", 
            			type: "POST", 
            			data: {'parent_id' : $('#p_category').val()}, 
            			success: function (data) { 
                			$('#category').html(data); 
                		} 
        			}); 
		});

		$('#category').on('change', function(){
			  $.ajax({ 
            			url: baseurl + "admin/discount/hm_get_subchild_category", 
            			type: "POST", 
            			data: {'parent_id' : $('#category').val()}, 
            			success: function (data) { 
                			$('#sub_category').html(data); 
                		} 
        			}); 
		});
	});
</script> 

