<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
          <form action="" method="post" enctype="multipart/form-data" >
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Discount List</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can View the  Discount List.</p>
                        </div>
                    </div>
                    <div class="page_box">
                     <form method="post" action="">
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
                                </form> 
                <!-- <table id="filterTable" width="100%" class="grid_tbl">
                    <tr>
                        <td width="22%"><input type="text" name="coupon_code" value="" placeholder="Enter Coupon Code/Name/Type" style="width:97%; padding:8px 2px; border:1px solid #ccc;"></td>
                        <td width="22%"><input type="text" name="start_date" class="datepicker" value="" placeholder="Enter start date" style="width:97%; padding:8px 2px; border:1px solid #ccc;" readonly="true"></td>
                        <td width="22%"><input type="text" name="end_date" class="datepicker" value="" placeholder="Enter end date" style="width:97%; padding:8px 2px; border:1px solid #ccc;" readonly="true"></td>
                        <td width="22%">
                            <select name="Status" style="width:100%;">
                                <option value="">Select Status</option>
                                <option value="A">Active</option>
                                <option value="I">In-Active</option>
                            </select>
                        </td>
                        <td><input type="button" name="filter" value="Filter" onclick="get_filtered_discount();"></td>
                    </tr>
                </table> -->
            </div>
            <table id="test" class="grid_tbl">
                <thead>
                    <tr>
                        <th scope="col">So.No.</th>
                        <th scope="col">Discount Name</th>
                        <th scope="col">Discount Type</th>
                        <th scope="col">Coupon Code</th>
                        <th scope="col">Discount On</th>
                        <th scope="col">Start Date</th>
                        <th scope="col">End Date</th>
                        <th scope="col">Discount</th>
                        <th scope="col">Purchase Amount</th>
                        <th>Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">
                    <?php
                    $i = 1;
                    foreach ($result AS $list) {

                        if ($list->DiscountType == 'percentage')
                            $type = '%';
                        else if ($list->DiscountType == 'fixed')
                            $type = 'INR';
                        else
                            $type = '';
                        ?>
                        <tr>
                            <td data-rel="so.no"><?php echo $i; ?></td>
                            <td data-rel="so.no"><?php echo $list->t_disc_name; ?></td>
                            <td data-rel="Pi ID"><?php echo $list->DiscountName; ?></td>
                            <td data-rel="S.No"><?php echo $list->CouponCode; ?></td> 
                            <td data-rel="Pi Code"><?php echo $list->DiscountOn; ?></td>
                            <td data-rel="Supplier Number"><?php echo $list->StartDate; ?></td>
                            <td data-rel="Supplier Number"><?php echo $list->EndDate; ?></td>
                            <td data-rel="Supplier Number"><?php echo $list->DiscountAmt . '(' . $type . ')'; ?></td>
                            <td data-rel="Total"><?php echo $list->PurchaseAmt; ?></td>
                            <td data-rel='Status'>
                        <?php 
                       # echo $list->DiscStatus == 'A' ? 'Active' : 'In-Active';

                         ?>


          <a href="<?php echo base_url(); ?>admin/discount/discount_new_activeinactive/<?=$list->Copid?>/<?php print ($list->DiscStatus=='I') ? 'active':'inactive';?>"
          <?php if($list->DiscStatus=='A'){ ?>class="inactive" <?php } else { ?>class="active_button"  <?php } ?>
          > <?php print ($list->DiscStatus=='I') ? 'Inactive':'Active';?></a>





                         </td>
                            <td data-rel="Action" >
        <a class="view" title="View Details" href="<?php echo base_url().'admin/discount/view/'.$list->Copid?>">
            <i class="fa fa-eye fa-lg"></i></a>
   <a class="delete" title="Delete" href="<?php echo base_url();?>admin/discount/discount_delete/<?=$list->Copid?>"><i class="fa fa-trash fa-lg"></i></a>
                            </td>


                        </tr>
    <?php $i++;
} ?>

                </tbody>
            </table>
        </div>
</section>
<script>
    $(function () {
        $(".datepicker").datepicker({
            altField: "#alternate",
            altFormat: "DD, d MM, yy",
            dateFormat: "yy-mm-dd"
        });
    });

    function get_filtered_discount() {
        var baseurl = "<?php echo base_url(); ?>";
        var Coupon = $('input[name=coupon_code]').val();
        var sdt = $('input[name=start_date]').val();
        var edt = $('input[name=end_date]').val();
        var status = $('select[name=Status]').val();
        var filter = new Array();
        filter = {'Coupon': Coupon, 'start': sdt, 'end': edt, 'status': status};
        $.ajax({
            url: baseurl + "admin/discount/filterDiscount",
            type: "POST",
            data: {'data': filter},
            success: function (data) {
                //alert(data); return false;
                $('#tblBody').html(data);
            }
        });
    }

</script>