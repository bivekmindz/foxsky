<script type="text/javascript" src="<?php echo admin_url(); ?>js/discount.js"></script>

<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> 

<style>
    .coupon{display: none;}
    .username{display: none;} 
</style>
 <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#regdiscount").validate({
        // Specify the validation rules
        rules: {
            
            discountname  : "required",
            discount : "required",
            min_purchase : "required",
            from : "required",
            to : "required",
            usableLimit : "required",
            coupon_code : "required",
            CouponUser : "required",
            regFrom : "required",
            regTo : "required",
           /* email:{
                required: true,
                email: true
            },

,,,,regTo

            password: {
                required: true,
                minlength: 5
            },
            agree: "required" */
        },
        // Specify the validation error messages
        messages: {
            discountname: "Discount name required",
            discount: "Discount required",
            min_purchase: "Min Purchase Id required"
             from: "From required",
              to: "To required",
            usableLimit   : "Usable required",
            coupon_code   : "Coupon Code required" 
            CouponUser   : "Coupon User  required" 
            regFrom   : "Reg From  required" 
            regTo   : "Reg To Id required" 
            
            /*password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            email: "Please enter a valid email address",
            agree: "Please accept our policy"*/
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
   
  </script>
<section class="main_container">  
    <script>
        $(function () {
            $(".datepicker").datepicker({
                altField: "#alternate",
                altFormat: "DD, d MM, yy",
                dateFormat: "yy-mm-dd",
                minDate: 0
            });
        });
    </script> 

   <?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
          <form action="" id="regdiscount" method="post" enctype="multipart/form-data" >
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Registration Discount</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add Registration discount with coupon code.</p>
                        </div>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
                    <div class="col-lg-12">
         

      </div>
</div>
<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Discount Name</div>
</div>
<div class="col-lg-8">
<div class="tbl_input"><input type="text" id="discountname" name="discountname" value="" min='1' required class="required" field="Discount Name"></div>
</div>

</div>
</div>
</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Discount value</div>
</div>
<div class="col-lg-4">
<div class="tbl_input"><input type="number" onkeypress="return isNumberKey(event);" id="discount" name="discount" value=""  min='1' required class="required" field="Discount Amount"></div>
</div>
<div class="col-lg-4"><div class="tbl_input">
  <select name="disType" id="disType">
                            <option value="fixed">Fixed</option>
                            <option value="percentage">Percentage</option>
                        </select>
</div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4"><div class="tbl_text">Purchase value</div></div>
<div class="col-lg-8"><div class="tbl_input">
<input type="number" id="min_purchase" name="min_purchase" onkeypress="return isNumberKey(event);" value="" required class="required" field="Discount Amount">
</div></div>
</div>
</div>
</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Valid Between</div>
</div>
<div class="col-lg-4">
<div class="tbl_input"><input type="text" id="from" name="from" value="" class="datepicker" placeholder="Start Date" readonly='true'></div>
</div>
<div class="col-lg-4"><div class="tbl_input">
 <input type="text" id="to" name="to" value="" class="datepicker" placeholder="End Date"  readonly='true'>
</div></div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Re-Usable</div>
</div>
<div class="col-lg-8">
<div class="tbl_input">
<input type="checkbox" value="1" name="reusable">
                        <input type="text" id="usableLimit" name="usableLimit" value="" style="width:94%;float:right" class="required" field="Re-Usable" placeholder="Reusable Limit" onkeypress="return isNumberKey(event);" readonly/>
</div>
</div>
</div>
</div>
</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Coupon</div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <input type="checkbox" name="coupon33" value="1"   onchange="generateCode($(this));">
                        <input type="text" id="coupon_code" name="coupon_code" value="" readonly style="width:94%;float:right"></div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Coupon For User</div>
</div>
<div class="col-lg-8">
<div class="tbl_input"><input type="text" id="CouponUser" name="CouponUser" required field="Coupon For User" class="required"  value="" onkeypress="return isNumberKey(event);"></div>
</div>
</div>
</div>
</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Registration Between</div>
</div>
<div class="col-lg-4">
<div class="tbl_input"> <input type="text" id="regFrom" name="regFrom" value="" class="datepicker required" field="Registration Start Data"  placeholder="Registration From"  readonly='true'></div>
</div>
<div class="col-lg-4">
<div class="tbl_input"> <input type="text" id="regTo" name="regTo" value="" class="datepicker required" field="Registration Last Data"  placeholder="Registration To"  readonly='true'></div>
</div>
</div>
</div>
</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text"></div>
</div>
<div class="col-lg-8">
<div class="submit_tbl">  <input class="btn_button sub_btn" id="save" name="save" value="Save" type="button"></div>
</div>
</div>
</div>
</div>



         <!--    <table width="95%">
                <tr>
                    <td width="15%"></td>
                    <td width="35%">
                    </td></tr>
                <tr>
                    <td width="15%"></td>
                    <td width="35%">
                      
                    </td>
                    <td width="15%"></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        
                       
                    </td>
                    <td></td>
                    <td>
                        
                    </td>
                </tr>
                 <tr>
                    <td></td> 
                    <td>
                       
                    </td>
                    <td></td>
                    <td>
                        
                    </td>
                </tr> 
                 <tr>
                    <td>specific user</td>
                    <td>
                        <input type="checkbox" name="getuserlist" value="1" onchange="userlistget($(this));">
                        </td>
                    <td></td>
                    <td>
                    </td>
                </tr> 
                <tr>
                    <td></td>
                    <td>
                       
                       
                    </td>
                   <!--  <td>Region</td>
                    <td>
                        <select name="region">
                            <?php foreach ($cities as $key => $val) { ?>
                                <option value="<?php echo $val->CityId; ?>"><?php echo $val->CityName; ?></option>
                            <?php } ?>
                        </select>
                    </td> -->
              <!--     </tr>
            </table>
         <tr>
  <td colspan='4'>
  <br>
     <span id="user"></span>
  </td>
  </tr>-->
        </div>
    </div>
</section>

<script>





function userlistget(elem){
    if (elem.prop('checked')) {
     var baseurl = "<?php echo base_url(); ?>";
         $.ajax({
         url: baseurl + "admin/discount/getalluser",
            type: "POST",
            data:{"id":"id"},
                 success: function (data) {
                $('#user').html(data);
            }
        });
    }
    else{
         $('#user').html('');
    }
}

    var baseurl = "<?php echo base_url(); ?>";
    $('input[name=reusable]').on('change', function () {
        if ($(this).prop('checked')) {
            $('input[name=usableLimit]').removeAttr('readonly');
        } else {
            $('input[name=usableLimit]').val('');
            $('input[name=usableLimit]').attr('readonly', 'true');
        }
    });
    
    $('select[name=disType]').change(function(){
        if($(this).val() == 'percentage') {
            $('input[name=discount]').attr('max', 100);
            var discount = $('input[name=discount]').val();
            if(discount > 100) {
                $('input[name=discount]').val('100');
            }
        }
    });

    $('input[name=save]').on('click', function () {

   //if($("input[name='coupon']:checked:enabled"))
    
   if($("input[name=coupon33]:checked").length == 0){
    alert('Please Generate Coupon Code');
    //alertify.error('Please Generate Coupon Code');
    return false;
   }else {
    
            var favorite = new Array(); 
            $.each($("input[name='user_select']:checked"), function(){            
                favorite.push($(this).val());
            });

    var t=favorite.length;
    if(t==0){
        favorite=0;
    }

        var err = validation();
        //var err = false;
          if (!err) {
            var disAmt = $('input[name=discount]').val();
            var disTyp = $('select[name=disType]').val();
            var purAmt = $('input[name=min_purchase]').val();
            var startDate = $('input[name=from]').val();
            var endDate = $('input[name=to]').val();
            var coupon = $('input[name=coupon_code]').val();
            var limit = $('input[name=CouponUser]').val();
            var reuse = $('input[name="reusable"]:checked').val();
            var usableLimit = $('input[name=usableLimit]').val();
            var discountna = $('input[name=discountname]').val();
            var regFrom = $('input[name=regFrom]').val();
            var regTo = $('input[name=regTo]').val();
            var region = $('select[name=region]').val();
            limit = limit ? limit : 1;
            reuse = reuse ? reuse : 1;

            $.ajax({
                url: baseurl + "admin/discount/saveRegisterDisc",
                type: "POST",
                data: {"userid":favorite, 'discname': discountna, 'disAmt': disAmt, 'disTyp': disTyp, 'purAmt': purAmt, 'startDate': startDate, 'endDate': endDate, 'coupon': coupon, 'limit': limit, 'reuse': reuse, 'usableLimit': usableLimit, 'regFrom': regFrom, 'regTo': regTo, 'region': region},
                success: function (data) {
                   alert(data);
                    //console.log(data); return false;
                    //alertify.success("Discount Added successfully");
                    window.location.href = baseurl + "admin/discount/discountlist";
                }
            });
        }
      } 
    });

    function generateCode(elem) {
        if (elem.prop('checked')) {
            $.ajax({
                url: baseurl + "admin/discount/get_unique_code",
                type: "POST",
                data: {},
                success: function (data) {
                    $('input:text[name=coupon_code]').val(data);
                }
            });
        } else {
            $('input:text[name=coupon_code]').val('');
        }
    }

    function validation() {
        var error = false;
        $('.required').each(function () {
            if (!$(this).val()) {
                alert($(this).attr('field') + ' is Missing!');
                $(this).focus();
                error = true;
                return false;
            }
        });
        return error;
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        // Added to allow decimal, period, or delete
        if (charCode == 110 || charCode == 190 || charCode == 46)
            return true;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>