<script type="text/javascript" src="<?php echo admin_url();?>js/discount.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> 
<style>
  .coupon{display: none;}
  .username{display: none;} 
</style>
<section class="main_container">  
 <script>
  $(function() {
   $(".datepicker").datepicker({
    altField: "#alternate",
    altFormat: "DD, d MM, yy",
    dateFormat:"yy-mm-dd",
    minDate: 0
   });
  });
 </script>


  <script language="javascript">
        $(document).ready(function () {
            $("#mydate2").datepicker({
               format: "dd/mm/yyyy"
            });
        });
    </script>

<div id="overlay" class="web_dialog_overlay"></div>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
          <form action="" method="post" id="product_discount_form" enctype="multipart/form-data" >
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Discount</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can add various discounts on category, brand and products.</p>
                        </div>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Discount Name <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-8">
<div class="tbl_input"><input type="text" id="discountname" name="discountname" value="" min='1' required class="required" field="Discount Name"></div>
</div>
</div>
</div>

<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Coupon Code <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <input type="checkbox" name="is_coupon" value="1"   onchange="generateCode($(this));">
                        <input type="text" id="coupon_code" field="Coupon Code" class="required" name="coupon_code" value="" readonly style="width:94%;float:right"></div>
</div>
</div>
</div>



</div>


  <div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Discount value <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-4">
<div class="tbl_input"><input type="number" onkeypress="return isNumberKey(event);" id="discount" name="discount" value=""  min='1' required class="required" field="Discount Amount"></div>
</div>
<div class="col-lg-4">
<div class="tbl_input"><select name="disType">
            <option value="fixed">Fixed</option>
            <option value="percentage">Percentage</option>
        </select> </div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Maximum Discount Price <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <input type="number" name="fixed_discount_price" value="" onkeypress="return isNumberKey(event)" required class="required" field="Fixed Discount Price"></div>
</div> 
</div>
</div>
</div> 

  <div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Purchase value (Min) <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <input type="number" name="min_purchase" value="" onkeypress="return isNumberKey(event)" required class="required" field="Discount Amount (Min)"></div>
</div> 
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Purchase value (Max) <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <input type="number" name="max_purchase" value="" onkeypress="return isNumberKey(event)" required class="required" field="Discount Amount (Max)"></div>
</div> 
</div>
</div>
</div>

 <div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Valid Between <span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-4">
<div class="tbl_input"><input type="text" id="valid_from" name="valid_from" value="" field="Valid From Date" class="datepicker required" placeholder="Start Date" readonly='true'></div>
</div>
<div class="col-lg-4"><div class="tbl_input">
 <input type="text" id="valid_to" name="valid_to" value="" class="datepicker required" field="Valid To Date" placeholder="End Date"  readonly='true'>
</div></div>
</div>
</div>
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Re-Usable</div>
</div>
<div class="col-lg-8">
<div class="tbl_input">
<input type="checkbox" value="1" name="is_reusable" >
                        <input type="text" id="usableLimit" name="usableLimit" value="" style="width:94%;float:right"  field="Re-Usable" placeholder="Reusable Limit" onkeypress="return isNumberKey(event);" />
</div>
</div>
</div>
</div>
</div>


  <div class="sep_box">

<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Valid Between Time<span style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-4">
<div class="tbl_input">
<select id="valid_from_time" name="valid_from_time">
    <?php for($i=0;$i<=23;$i++) {  
            if($i < 10)
            {
                $v='0'.$i;
            }
            else
            {
                $v=$i;
            }?>
    <option value="<?php echo $v; ?>"><?php echo $v; ?>:00</option>
    <?php } ?>
</select>
</div>
</div>
<div class="col-lg-4"><div class="tbl_input">
<select id="valid_to_time" name="valid_to_time">
    <?php for($i=0;$i<=23;$i++) { 
            if($i < 10)
            {
                $v='0'.$i;
            }
            else
            {
                $v=$i;
            } ?>
    <option value="<?php echo $v; ?>"><?php echo $v; ?>:00</option>
    <?php } ?>
</select>

</div></div>
</div>
</div>

<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Coupon For User Type</div>
</div>
<div class="col-lg-8">
<div class="tbl_input"> <select name="cpn_user_type" id="cpn_user_type">
                            <option value="retail">Consumer</option>
                        </select></div>
</div>
</div>
</div>
</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text"><input type="radio" value="categories" name="apply" onchange="getList($(this));">Categories </div>
</div>
<div class="col-lg-4">
<div class="tbl_text"> <input type="radio" value="brands" name="apply" onchange="getList($(this));">Brand </div>
</div>
<div class="col-lg-4">
<div class="tbl_text"><input type="radio" value="product" name="apply" onchange="getList($(this));">Products </div>
</div>
</div>
</div>


<div class="gridview">
  <div style="float:right; padding:5px;">
    <input class="hbutton trans mrgnt5" name="save" value="Save" type="button">
  </div>
<table width="95%">
<tr>
<td width="15%"></td>
 <td width="35%">
</td></tr>
  <tr>
    <td width="15%"></td>
    <td width="35%">
        
    </td>
    <td width="15%"></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>
        
        
    </td>
    <td></td>
    <td>
      
    </td>
  </tr>
  <tr> 
    <!-- <td>
    specific user: 
     <input type="checkbox" name="getuserlist" value="1" onchange="userlistget($(this));">
    </td> -->
  </tr>  
  <tr>
    <td></td>
    <td>
        
    </td>
    <td></td>
    <td>
     
    </td>
  </tr>

  <tr> 
        <td style="text-align:left"> 
          
        </td> 
  </tr> 

<tr> 
        <td style="text-align:left"> 
         
        </td> 
  </tr>

  <tr> 
        <td style="text-align:left"> 
          
        </td> 
  </tr> 

  <tr>
  <td colspan='4'>
  <br>
     <span id="user"></span>
  </td>
  </tr>
   <tr>
      <td id="discountTable" colspan='4'>   

    </tr>
    
    <tr>      
    <td id="subdiscounttable" colspan='4'></td>
    </tr>
    <tr>
        
    <td id="childsubdiscounttable" colspan='4'></td>

    </tr>
   <!--  <tr>
        
    <td id="products" colspan='4'></td>

    </tr> -->
    
</table>
</div>
</div>
</section>

<script>
    var baseurl = "<?php echo base_url(); ?>";
    $('input[name=is_reusable]').on('change', function () {
        if ($(this).prop('checked')) {
            $('input[name=usableLimit]').removeAttr('readonly');
        } else {
            $('input[name=usableLimit]').val('');
            $('input[name=usableLimit]').attr('readonly', 'true');
        }
    });

 $('input[name=save]').on('click', function () {
    var favorite = new Array(); 
            $.each($("input[name='user_select']:checked"), function(){            
                favorite.push($(this).val());
            });

  var t=favorite.length;
    if(t==0){
        favorite=0;
    }

        var err = validation();
       
        //var err = false;
          if (!err) {
            
            var discount_value = $('input[name=discount]').val();
            var discount_type = $('select[name=disType]').val();
            var purchase_amount = $('input[name=min_purchase]').val();
            var purchase_amount_max = $('input[name=max_purchase]').val();
            var fixed_discount_price = $('input[name=fixed_discount_price]').val();
            var coupon = $('input[name=coupon_code]').val();
            var is_reusable = $('input[name="is_reusable"]:checked').val();
            var is_coupon = $('input[name="is_coupon"]:checked').val();
            var reusable_limit = $('input[name=usableLimit]').val();
            var discount_name = $('input[name=discountname]').val();
            var valid_from = $('input[name=valid_from]').val();
            var valid_to = $('input[name=valid_to]').val();
            var valid_from_time = $('select[name=valid_from_time]').val();
            var valid_to_time = $('select[name=valid_to_time]').val();
            var coupon_user_type = $( "#cpn_user_type option:selected" ).val();

            
            if(discount_type == "percentage" && (discount_value >=100 || discount_value <= 0))
            {
               alert('Percentage should be between 0 to 100');
               return false;
            }
            if(Date.parse(valid_from) >Date.parse(valid_to))
            {
              alert('Valid from date should be less than or equal to Valid to date');
               return false;   
            } 
            
            // post data will vary depend upon selection of radio button
            var apply_type = $('input[name=apply]:checked').val(); // which is selected brand, product or category
            var post_data = { };
            //alert(apply_type);
            if(apply_type == 'brands')
            {

                var brand_list = $('input[name=brandname]:checked').map(function() { return this.value;}).get().join(',');
                is_reusable = is_reusable ? 1: 0;
                is_coupon = is_coupon ? 1: 0;
                post_data = {

                        
                        'discount_name' : discount_name,
                        'discount_value': discount_value, 
                        'discount_type': discount_type, 
                       // 'purchase_amount': purchase_amount, 
                        'is_coupon': is_coupon, 
                        'coupon': coupon, 
                        'is_reusable': is_reusable, 
                        'reusable_limit': reusable_limit, 
                        'valid_from': valid_from, 
                        'valid_to': valid_to, 
                        'valid_from_time': valid_from_time, 
                        'valid_to_time': valid_to_time, 
                        'coupon_user_type': coupon_user_type,
                        'brand_list' : brand_list,
                        'purchase_amount' : purchase_amount,
                        'purchase_amount_max' : purchase_amount_max,
                        'fixed_discount_price' : fixed_discount_price
                     }
                   // console.log(post_data);
            }
            else if(apply_type == 'product')
            {
                var product_list = $('input[name=products]:checked').map(function(){ return this.value;}).get().join(',');
                
                is_reusable = is_reusable ? 1: 0;
                is_coupon = is_coupon ? 1: 0;

                post_data = {
                        'discount_name' : discount_name,
                        'discount_value': discount_value, 
                        'discount_type': discount_type, 
                       // 'purchase_amount': purchase_amount, 
                        'is_coupon': is_coupon, 
                        'coupon': coupon, 
                        'is_reusable': is_reusable, 
                        'reusable_limit': reusable_limit, 
                        'valid_from': valid_from, 
                        'valid_to': valid_to, 
                        'valid_from_time': valid_from_time, 
                        'valid_to_time': valid_to_time, 
                        'coupon_user_type': coupon_user_type,
                        'product_list' : product_list,
                        'purchase_amount' : purchase_amount,
                        'purchase_amount_max' : purchase_amount_max,
                        'fixed_discount_price' : fixed_discount_price
                }

            }
            else if(apply_type == 'categories')
            {
               // var category_list = $('input[name=catname]:checked').map(function(){ return this.value;}).get().join(',');
                var category_list = [];
                    $('#sub_category :selected').each(function(i, selected){ 
                        category_list[i] = $(selected).val(); 
                  });

                var parent_id = $('#category').val();

                is_reusable = is_reusable ? 1: 0;
                is_coupon = is_coupon ? 1: 0;
                post_data = {
                        'discount_name' : discount_name,
                        'discount_value': discount_value, 
                        'discount_type': discount_type, 
                       // 'purchase_amount': purchase_amount, 
                        'is_coupon': is_coupon, 
                        'coupon': coupon, 
                        'is_reusable': is_reusable, 
                        'reusable_limit': reusable_limit, 
                        'valid_from': valid_from, 
                        'valid_to': valid_to, 
                        'valid_from_time': valid_from_time, 
                        'valid_to_time': valid_to_time, 
                        'coupon_user_type': coupon_user_type,
                        'category_list' : category_list.join(","),
                        'parent_id'     : parent_id,
                        'purchase_amount' : purchase_amount,
                        'purchase_amount_max' : purchase_amount_max,
                        'fixed_discount_price' : fixed_discount_price
                }
            } else {

              alert("Please choose any one in Category, Brands and Products for discount.");
              return false;

            }

            
            
            $.ajax({
                url: baseurl + "admin/discount/saveProductDiscount/"+apply_type,
                type: "POST",
                data: post_data,
                success: function (data) {
                    //alert(data);
                    //console.log(data); return false;
                    //alertify.success("Discount Added successfully");
                   window.location.href = baseurl + "admin/discount/all_discount_list/"+apply_type;
                }
            });
        }
      
    });

/*
$('input[name=save]').on('click',function() {


var sThisVal = new Array(); 
var sThisValbrandid = new Array(); 

var thisvalproductid = new Array();


   $('input:checkbox[name=subcategorychildname]').each(function () {
           //sThisVal = (this.checked ? $(this).val() : "");
           if(this.checked){
                sThisVal.push($(this).val());
            }
        });

   var categoryid = sThisVal.join(', ');

 $('input:checkbox[name=brandname]').each(function () {
           //sThisVal = (this.checked ? $(this).val() : "");
           if(this.checked){
                sThisValbrandid.push($(this).val());
            }
        });
  var brandproductid = sThisValbrandid.join(', ');

$('input:checkbox[name=products]').each(function () {
           //sThisVal = (this.checked ? $(this).val() : "");
           if(this.checked){
                thisvalproductid.push($(this).val());
            }
        });
  var productid = thisvalproductid.join(',');


 //alert(brandproductid);

if(categoryid=='')
{
  var discoutonval = 'all';
}else{
  var discoutonval = '';
}

    var err = validation();
    if(!err) {
        var disAmt = $('input[name=discount]').val();
        var disTyp = $('select[name=disType]').val();
        var purAmt = $('input[name=min_purchase]').val();
        var startDate = $('input[name=from]').val();
        var endDate = $('input[name=to]').val();
        var coupon = $('input[name=coupon_code]').val();
        var limit = $('input[name=CouponUser]').val();
        var reuse = $('input[name=usableLimit]').val();
        var usableLimit = $('input[name=usableLimit]').val();
    var discountna = $('input[name=discountname]').val();
    
          $.ajax({
            url:baseurl+"admin/discount/saveCartDisc",
            type: "POST",
            data: {'discname':discountna,'disAmt':disAmt,'disTyp':disTyp,'purAmt':purAmt,'startDate':startDate,'endDate':endDate,'coupon':coupon,'limit':limit,'reuse':reuse,'usableLimit':usableLimit,'categorydiscountid':categoryid,'discountonvalue':discoutonval,'discountbrandproduct':brandproductid,'productid':productid},
            success:function(data) {
          //alert(data); return false;
          //alertify.success("Discount Add successfully");
                window.location.href = baseurl+"admin/discount/discountlist";
            }
          });
    }
}); */

function generateCode(elem) {

  if(elem.prop('checked')) {
    $.ajax({
      url:baseurl+"admin/discount/get_unique_code_bizz",
      type: "POST",
      data: {},
      success:function(data) {
        //alert(data);
        $('input:text[name=coupon_code]').val(data);
      }
    });
  } else {
    $('input:text[name=coupon_code]').val('');
  }
}

function validation() {
    var error = false;
    $('.required').each(function() {
        if(!$(this).val()) {
            alert($(this).attr('field') + ' is Missing!');
            $(this).focus();
            error = true;
            return false;
        }
    });
    return error;
}

function getList(elem) { 
        
        $.ajax({ 
            url: baseurl + "admin/discount/getList", 
            type: "POST", 
            data: {'type' : elem.val()}, 
            success: function (data) { 
                
                $("#products").empty();
                $("#childsubdiscounttable").empty();
                $("#subdiscounttable").empty();
                //alert(data); return false;
                $('#discountTable').html(data); 
                //alert(data);
                /*if (elem.val() == 'product') { 
                    $('#products').html(data); 
                } else {

                    $('#discountTable').html(data); 
                }*/ 
            } 
        }); 
    } 


</script>

 <script>
 /*$("input[name=products]:checked").each(function () {
        var value = $(this).val();
        var array = value.split(',');
        alert(array);
        

        
    });
   var product_list = $('input[name=products]:checked').map(function(){ return this.value;}).get();
   alert(product_list);
   var json_data =  jQuery.parseJSON(product_list);
   alert(json_data);
   return false; */
  </script>