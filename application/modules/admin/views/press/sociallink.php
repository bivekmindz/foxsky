 <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

   
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Social Link</h2>
   
    </div>
        <div class="page_box">
        <div class="sep_box">
                            <div class="col-lg-12">
        <div style="text-align:right;">
           
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
        <form action="" id="addCont" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">facebook<span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-6">
                <div class="tbl_input">

                <input type="text" name="facebook" id="facebook" value="<?php echo $view->facebook; ?>"  required>
                </div>
            </div>
        </div>
        </div>


            <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">twitter<span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-6">
                <div class="tbl_input">
                <input type="text" name="twitter" id="twitter"  value="<?php echo $view->twitter; ?>" required>
                </div>
            </div>
        </div>
        </div>
        </div>


     <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">ping<span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-6">
                <div class="tbl_input">
                <input type="text" name="ping" id="ping" value="<?php echo $view->ping; ?>" required>
                </div>
            </div>
        </div>
        </div>


            <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">googleplus<span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-6">
                <div class="tbl_input">
                <input type="text" name="googleplus" id="googleplus"  value="<?php echo $view->googleplus; ?>"   required>
                </div>
            </div>
        </div>
        </div>
        </div>




     <div class="sep_box">
     
            <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text"></div>
            </div>
            <div class="col-lg-6">
                <div class="submit_tbl">
             <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>
        </div>


          
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>

   