<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Role List</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can Manage All Role Details.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                     <a href="<?php echo base_url();?>admin/user/addrole"><button>Add Role</button></a>
                                </div>
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th>S:NO</th>
                                            <th>Role Name</th>
                                            <th>Updated by</th>
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                           $i=0;
                                           if(!empty($rolelist))
                                           {
                                           foreach ($rolelist as $key => $value) {
                                          $i++;
                                    ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->rolename ;?></td>
                                            <td><?php echo $value->u_createdby; ?></td> 
                                            <td><?php echo $value->created_on; ?></td>
                                            <td>
                                                <a href="<?php echo base_url()?>admin/rollmaster/delrole/<?php echo $value->id; ?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                                <a href="<?php echo base_url(); ?>admin/rollmaster/rolestatus/<?php echo $value->id.'/'.$value->is_status; ?>"><?php if($value->is_status=='D') { ?>Inactive<?php }else { ?>Active<?php } ?> </a> 
                                            </td>
                                        </tr>
                                    <?php } } else { ?>
                                        <tr><td colspan="3">No Record Found.</td></tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>