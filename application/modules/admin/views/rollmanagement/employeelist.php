<?php //p($list); exit();  ?>
<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Employee List</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can Manage All Employee Details.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                     <a href="<?php echo base_url();?>admin/user/employeeregistration"><button>Add Employee</button></a>
                                </div>
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th>S:NO</th>
                                            <th>Employee Name</th>
                                            <th>Employee Role</th>
                                            <th>Email</th>
                                            <th>Contact No</th>
                                            <th>Gender</th>
                                            <th>Password</th>
                                            <th>Change Role<br>Credentials</th>
                                            <th>Updated by</th>
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                           $i=0;
                                           if(!empty($list))
                                           {
                                           foreach ($list as $key => $employeelist) {
                                          $i++;
                                    ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo  $employeelist->F_Name.' '.$employeelist->L_Name; ?></td>
                                            <td><?php $parameters =array(
                                               'act_mode'=>'getemproledetail',
                                               'Param1'=>$employeelist->EmployeeID,
                                               'Param2'=>'',
                                               'Param3'=>'',
                                               'Param4'=>'',
                                               'Param5'=>'',
                                               'Param6'=>'',
                                               'Param7'=>'',
                                               'Param8'=>'',
                                               'Param9'=>'',
                                               'Param10'=>''


                                                );          
                                            $emproledetail=$this->supper_admin->call_procedure('proc_rolluser',$parameters);
                                            if(empty($emproledetail)) echo '-';
                                            else  
                                            echo $emproledetail[0]->rolename; ?></td>
                                            <td><?php echo $employeelist->Email;  ?></td>
                                            <td><?php echo $employeelist->ContactNo;  ?></td>
                                            <td><?php if($employeelist->Gender==1){ echo 'Male'; } if($employeelist->Gender==2){ echo 'Female'; } ?></td>
                                            <td><?php echo base64_decode($employeelist->empSalary); ?></td>
                                            <td style="text-align: center;font-size: 24px;"><a href="<?php echo base_url()?>admin/rollmaster/updateassignrole/<?php echo $employeelist->EmployeeID; ?>"><i class="fa fa-street-view"></i></a></td>
                                            <td><?php if(empty($employeelist->ModifiedBy)){ echo $employeelist->u_createdby; } else { echo $employeelist->u_modifiedby; } ?></td> 
                                            <td><?php if(empty($employeelist->ModifiedBy)){ echo $employeelist->CreatedOn; } else { echo $employeelist->ModifiedOn; } ?></td>
                                            <td>
                                                <a href="<?php echo base_url()?>admin/user/employeelistedit/<?php echo $employeelist->EmployeeID;  ?>"><i class="fa fa-pencil"></i></a> 
                                                <a href="<?php echo base_url(); ?>admin/rollmaster/delemployee/<?php echo $employeelist->EmployeeID; ?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                                 <a href="<?php echo base_url(); ?>admin/rollmaster/employeestatus/<?php echo $employeelist->EmployeeID.'/'.$employeelist->t_status; ?>"><?php if($employeelist->t_status=='Inactive') { 
                                                    ?>Inactive<?php 
                                                }else { 
                                                    ?>Active<?php } 
                                                    ?>
                                                </a> 
                                            </td>
                                        </tr>
                                    <?php } } else { ?>
                                        <tr><td colspan="7">No Record Found.</td></tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>