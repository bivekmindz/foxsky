<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>List of Incentive Slabs</h2>
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this section, admin can view incentive slabs!
                            <a href="<?php echo base_url();?>admin/user/addincentiveslabs"><button type="button" style="float:right;">ADD SLABS</button></a></p>
                        </div>
                    </div>
        <div class="page_box">
        <div class="sep_box">
        <div class="col-lg-12">
            <table id="test" class="grid_tbl">
                <span style="color:red"><?php echo $this->session->flashdata("message"); ?></span>
                <thead>
                    <tr>
                        <th scope="col">S.No.</th>
                        <th scope="col">Sale Range From</th>
                        <th scope="col">Sale Range To</th>
                        <th scope="col">Salary Range From</th>
                        <th scope="col">Salary Range To</th>
                        <th scope="col">Type</th>
                        <th scope="col">Incentive</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="tblBody">
                    <?php $i=0; foreach ($list AS $val)  {    $i++; ?>
                        <tr>
                            <td data-rel="Pi ID" style="align:left" ><?php echo  $i; ?></td>
                            <td data-rel="Pi ID" style="align:left" ><?php echo  $val->range_from; ?></td>
                            <td data-rel="S.No"><?php echo $val->range_to;  ?></td>
                            <td data-rel="S.No"><?php echo $val->range_salaryFrom;  ?></td>
                            <td data-rel="S.No"><?php echo $val->range_salaryTo;  ?></td>
                            <td data-rel="Pi Code"><?php if($val->range_type==1) echo "Percentage"; else echo "Flat";  ?></td>
                            <td data-rel="Supplier Number"><?php echo $val->range_incentive;  ?></td>
                            <td data-rel="Supplier Number">
                            <a href="<?php echo base_url(); ?>admin/user/editincentiveslab/<?php echo $val->range_id;  ?>">Edit</a>
                            <a href="<?php echo base_url(); ?>admin/user/deleteincentiveslab/<?php echo $val->range_id;  ?>">Delete</a>
                            </td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
</section>

</div>
</div>