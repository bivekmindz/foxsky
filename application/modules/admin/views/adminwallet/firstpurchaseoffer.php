<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            "userwallid[]" : "required",
            "userwallamt" : "required",
            "userwallordid" : "required",
            //userwalltype : "required",
        },
        // Specify the validation error messages
        messages: {
            "userwallid[]": "Select user is required",
            "userwallordid": "Reason is required",
            "userwallamt" : "Cashback Amount is required"
            //userwalltype: "Type of wallet is required",
           
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add First Purchase Offer</h2>
                    </div>
                    <div class="page_box">
                         <div class="sep_box">
        <div class="col-lg-12">       
                    <!--  session flash message  -->
                    <div class='flashmsg'>
                        <?php echo validation_errors(); ?> 
                        <?php
                          if($this->session->flashdata('message')){
                            echo $this->session->flashdata('message'); 
                          }
                        ?>
                    </div>
                    </div></div>
                    <form action="" id="addCont" method="post" enctype="multipart/form-data">
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Select User <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                       <div class="tbl_input">
                                        <select name="userwallid[]" id="userwallid" multiple="multiple">
                                        
                                        <?php foreach ($manufacwallet as $value){  ?>
                                          <option value="<?php echo $value->id; ?>"><?php echo $value->firstname.' '.$value->lastname.'  -  '.$value->compemailid; ?></option>                  
                                        <?php } ?>
                                        </select> 

                        
                                        </div> 
                                    </div>
                                </div>
                            </div>
                           </div>
                           <div class="sep_box">

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Cashback Amount <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input type="text" name="userwallamt" id="userwallamt" onKeyPress="return isNumberKey(event);" maxlength="4"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Transfer Cashback Reason<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <textarea name="userwallordid" id="userwallordid" style="resize: none"></textarea>
                                        <input type="hidden" name="userwalltype" id="userwalltype" value="credit">
                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                   </div>
         
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                           
                                            <input id="Submit1" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   </form>
                </div>
            </div>
        </div>
    </div>
    </div>

<script type="text/javascript">  
$(document).ready(function() {
       $('#userwallid').multiselect({ 
            enableClickableOptGroups: true,
            enableFiltering: true,
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
        });
    });


function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode;
if (charCode != 46 && charCode > 31 
&& (charCode < 48 || charCode > 57))
return false;

return true;
}

</script>
    