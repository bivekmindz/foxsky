 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            userwallordid : "required",
            userwalltype : "required",
            userwallid : "required",
            "userwallamt" : "required",
        },
        // Specify the validation error messages
        messages: {
            userwallordid: "Order Id is required",
            userwalltype: "Type of wallet is required",
            userwallid: "Select user is required",
            userwallamt: "Wallet Amount is required"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Wallet</h2>
                    </div>
                    <div class="page_box">
                         <div class="sep_box">
        <div class="col-lg-12">       
                    <!--  session flash message  -->
                    <div class='flashmsg'>
                        <?php echo validation_errors(); ?> 
                        <?php
                          if($this->session->flashdata('message')){
                            echo $this->session->flashdata('message'); 
                          }
                        ?>
                    </div>
                    </div></div>
                    <form action="" id="addCont" method="post" enctype="multipart/form-data">
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Select User <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                       <div class="tbl_input">
                                        <select name="userwallid" id="userwallid" onchange="getorders()">
                                        <option value="">Select User</option>
                                        <?php foreach ($manufacwallet as $value){  ?>
                                          <option value="<?php echo $value->id; ?>"><?php echo $value->firstname.' '.$value->lastname.'  -  '.$value->vendorcode; ?></option>                  
                                        <?php } ?>
                                        </select> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            </div>
                        
<div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Type (+ / -) <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                          <select name="userwalltype" id="userwalltype" >
                                            <option value="">Select Type</option>
                                            <option value="credit">Credit (+)</option>
                                            <option value="debit">Debit (-)</option>
                                          </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Order Id <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <select name="userwallordid" id="userwallordid">
                                          <option value="">Select Order</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

        
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Wallet Amount <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input type="text" name="userwallamt" id="userwallamt" onKeyPress="return isNumberKey(event);"/></div>
                                    </div>
                                </div>
                            </div>

         
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <!-- <input id="Submit1" name="submit" type="submit" value="Submit" class="btn_button sub_btn" onclick="return validate16();" /> -->
                                            <input id="Submit1" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   </form>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script type="text/javascript">
    function getorders(){
      var manid=$('#userwallid').val();
      $.ajax({
        url: '<?php echo base_url()?>admin/wallet/getmanorders',
        type: 'POST',
        data: {'manid': manid},
        success: function(data){
          //console.log(data);
          //alert(data);return false;
        $('#userwallordid').empty();
        $("#userwallordid").append(data);
        }
      });

    }



      function validate16()
{      
    var country_id=document.getElementById('country_id');
    var state_name=document.getElementById('state_name');
    var state_code=document.getElementById('state_code');
    
   
  
if(country_id.value=='')
          {
            
          document.getElementById('country_id').style.border="1px solid red";
          country_id.focus();
          return false; 
          }
          else
          {
          document.getElementById('country_id').style.border="1px solid green";  
          }

if(state_name.value=='')
          {
            
          document.getElementById('state_name').style.border="1px solid red";
          state_name.focus();
          return false; 
          }
          else
          {
          document.getElementById('state_name').style.border="1px solid green";  
          }

if(state_code.value=='')
          {
            
          document.getElementById('state_code').style.border="1px solid red";
          state_code.focus();
          return false; 
          }
          else
          {
          document.getElementById('state_code').style.border="1px solid green";  
          }



                 

        }



function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode;
if (charCode != 46 && charCode > 31 
&& (charCode < 48 || charCode > 57))
return false;

return true;
}
</script>
    