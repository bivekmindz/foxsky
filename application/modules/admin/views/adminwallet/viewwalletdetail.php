<?php //p($newsview);exit;?>
<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>User Wallet details</h2>
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can view the User Wallet details.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">

                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                                <?php  $totfixwalletamt=0;
                                       foreach($userswallet as $value){ 
                                        $totfixwalletamt = number_format($totfixwalletamt,1,".","")+number_format($value->w_amount,1,".","");
                                       }
                                ?>
                                <h3 style="float:right;width:auto;">Total Balance :- Rs. <?php echo number_format($totfixwalletamt,1,".","");?></h3>
                                <h3 style="float:left;width:auto;">Name :- <?php echo ucwords($manufacdetwallet->firstname).' '.ucwords($manufacdetwallet->lastname);?></h3>
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S.NO.</th>
                                            <th>Order Id</th>
                                            <th>Debited Amount (-)</th>
                                            <th>Credited Amount (+)</th>
                                            <th>Total Amount</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; $fixwalletamt=0;
                                    foreach ($userswallet as $key => $value) { $i++;?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo "ORD".$value->w_orderid; ?></td>
                                            <td><?php if($value->w_type=='debit'){ echo number_format($value->w_amount,1,".",""); } else { echo "-"; } ?></td>
                                            <td><?php if($value->w_type=='credit'){ echo number_format($value->w_amount,1,".",""); } else { echo "-"; } ?></td>
                                            <?php $fixwalletamt = number_format($fixwalletamt,1,".","")+number_format($value->w_amount,1,".",""); ?>
                                            <td><?php echo number_format($fixwalletamt,1,".",""); ?></td>
                                            <td><?php echo $value->w_date; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>