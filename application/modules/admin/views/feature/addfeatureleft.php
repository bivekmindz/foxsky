<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Add Featue Left Master</h2>
   
    </div>
        <div class="page_box">
         <div class="sep_box">
        <div class="col-lg-12">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/feature/viewfeaturemaster"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
        <form action="" method="post" enctype="multipart/form-data" id="contactform">
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Feature Type <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="featid" id="featid" required>
                <option value="">Select Feature</option>
                <?php foreach ($vieww as $key => $value) {
                 ?>
                  <option value="<?php echo $value->FeatureID ?>"><?php echo $value->FeatureName ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>
       <!--  <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="catidd" id="catidd" >
                <option>Select Category</option>
                <?php foreach ($parentCatdata as $key => $value) {
                 ?>
                  <option value="<?php echo $value->catid ?>"><?php echo $value->catname ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div> -->
       
        </div>
        <div style='width:30px; float:left; padding:5px; border:1px solid silver; font-weight:bold; cursor:pointer;position: absolute; top: 205px;left: 25px;' id="addMore">(+)</div>

        <div id="attBox" style='padding-bottom:5px;'>
        <div class="sep_box repAttr" style='border-bottom:1px solid #E8E8E8; padding-bottom:5px;'>
         <div class="col-lg-1"></div>
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Left Feature <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input type="text" name="leftvalue[]" id="leftvalue"></div>
            </div>
        </div>
        </div>
        </div>
        </div>
     
   
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
 <script type="text/javascript">

$(document).ready(function(){
       
        $("#addMore").click(function(){

            var remme = "<div style='width:30px; float:left; padding:5px; border:1px solid #FF3C01;color:#FF3C01;text-align: center;cursor:pointer;    position: absolute;left:25px;' class='remMore' onclick='delme($(this));'>(-)</div>";
            $(".repAttr:last-child").clone(true).appendTo("#attBox");
            //alert($(".repAttr:last-child div.remMore").length);

            if($(".repAttr:last-child div.remMore").length == 0){
                $(".repAttr:last-child").prepend(remme);
                return false;
            }
        }); 
    });  

    function delme(vv){
        $(vv).parent().remove();
        return false;    
    }
 </script>

 <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script> 
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script type="text/javascript"> 
    $(document).ready(function() { 
      $("#contactform").validate({ 
        ignore : [],
        rules: { 
          featid : "required",
          "leftvalue[]" : "required"
         }, 
        messages: { 
        featid : "Feature Type is required",
        "leftvalue[]" : "Left Feature is required"
        } 
          }); 
        }); 
  </script> 

   <!-- <style type="text/css"> 
     label.error { width: 250px; display: block; float: left; padding-left: 10px; color:#fff; background:#990000;padding:3px;} 
   
 
      </style>  -->