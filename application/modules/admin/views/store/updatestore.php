<!--   <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script> -->
<script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
    .stp_rmv{text-align: center;}
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore: [],
        rules: {
            sto_vendor              : "required",
            sto_name                : "required",
            sto_address             : "required",
            sto_country             : "required",
            sto_state               : "required",
            sto_city                : "required",
            sto_email               : "required",
            sto_phone               : "required",
            sto_dopen               : "required",
            "stp_person[]"          : "required",
            "stp_person_email[]"    : "required",
            "stp_person_mobile[]"   : "required",
        },
        // Specify the validation error messages
        messages: {
            sto_vendor              : "Vendor is required.",
            sto_name                : "Store Name is required.",
            sto_address             : "Store Address is required.",
            sto_country             : "Country is required.",
            sto_state               : "State is required.",
            sto_city                : "City is required.",
            sto_email               : "Store Email is required.",
            sto_phone               : "Store Phone Number is required.",
            sto_dopen               : "Date of Opening is required.",
            "stp_person[]"          : "Person Name is required.",
            "stp_person_email[]"    : "Person Email is required.",
            "stp_person_mobile[]"   : "Person Mobile is required.",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
            
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Update Store</h2>
                    </div>
                    <div class="page_box" style="overflow: inherit;">
                    <div class="sep_box">
                        <div class="col-lg-12">
                            <div class='flashmsg'>
                                <?php
                                  if($this->session->flashdata('message')){
                                    echo $this->session->flashdata('message'); 
                                  }
                                ?>
                            </div>
                        </div>
                    </div>

        <form id="addCont" action="" method="post" enctype="multipart/form-data" >
            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Vendor <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><select name="sto_vendor" id="sto_vendor">
                                <option value="">Select Vendor</option>
                                <?php foreach ($vendor_list as $key => $value) { ?>
                                    <option <?php if($value->id==$store_detail->st_store_vendor){ echo "selected"; } ?> value="<?php echo $value->id; ?>"><?php echo $value->vendor_name; ?></option>
                                <?php } ?>
                            </select></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Store Code <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input type="text" name="sto_code" id="sto_code" value="<?php echo $store_detail->st_store_code; ?>" readonly></div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="sep_box">    
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Store Name <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input type="text" name="sto_name" id="sto_name" value="<?php echo $store_detail->st_store_name; ?>"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Store Address <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input type="text" name="sto_address" id="sto_address" value="<?php echo $store_detail->st_address; ?>"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Country <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><select name="sto_country" id="sto_country" onclick="countrystate();">
                                <option value="">Select Country</option>
                                <?php foreach ($viewcountry as $key => $value) { ?>
                                    <option <?php if($value->conid==$store_detail->st_country){ echo "selected"; } ?> value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                                <?php } ?>
                            </select></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">State <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><select name="sto_state" id="sto_state" onclick="statecity()">
                                <option value="">Select State</option>
                            </select></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">City <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><select name="sto_city" id="sto_city">
                                <option value="">Select City</option>
                            </select></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Store Email <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input type="text" name="sto_email" id="sto_email" value="<?php echo $store_detail->st_email; ?>"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Store Phone Number <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input type="text" name="sto_phone" id="sto_phone" maxlength="10" value="<?php echo $store_detail->st_mobile; ?>"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Date of Opening <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input type="text" name="sto_dopen" id="sto_dopen" class="valid" value="<?php echo $store_detail->st_date_of_opening; ?>"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="sep_box">
                <div class="col-lg-12">
                <table class="stp_data">
                    <thead>
                        <tr>
                            <th>Person Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <!-- <th><input type="button" name="addmoreperson" id="addmoreperson" value="ADD"></th> -->
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($store_contact as $key => $value) { ?>
                        <tr class="stp_detail">
                            <td><input type="text" name="stp_person<?php echo $key; ?>" id="stp_person" value="<?php echo $value->stm_store_person; ?>"></td>
                            <td><input type="text" name="stp_person_email<?php echo $key; ?>" id="stp_person_email" value="<?php echo $value->stm_person_email; ?>"></td>
                            <td><input type="text" name="stp_person_mobile<?php echo $key; ?>" id="stp_person_mobile" value="<?php echo $value->stm_person_phone; ?>" maxlength="10"></td>
                        </tr>
                    <?php } ?>    
                    </tbody>
                </table>
                </div>
            </div>

            <div class="sep_box">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-12">
                            <div class="submit_tbl">
                                <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div>
</div> 

<script type="text/javascript">

$(document).ready(function () {
    $('#sto_dopen').datepicker({ 
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true 
    });
});

$(document).ready(function(){
$("#addmoreperson").click(function () {
    var $clone = $('table.stp_data tr.stp_detail:first').clone();
    $clone.find(':text').val('');
    $clone.append("<td><div class='stp_rmv' ><i class='fa fa-trash'></i></div></td>");
    $('table.stp_data').append($clone);
});

$('.stp_data').on('click', '.stp_rmv', function () {
     $(this).closest('tr').remove();
});
});

function countrystate(){
     var samagamid="<?php echo $store_detail->st_state ?>";   
     var countryid=$('#sto_country').val();
     $.ajax({
        url: '<?php echo base_url()?>admin/vendor/countrystate',
        type: 'POST',
        //dataType: 'json',
        data: {'countryid': countryid,'samagamid':samagamid},
        success: function(data){

        var  option_brand = '<option value="none">Select State</option>';
        $('#sto_state').empty();
        $("#sto_state").append(option_brand+data);
        statecity();
           
         }
  });
    
   }
   
countrystate();
   
   function statecity(){
    var  stid=$('#sto_state').val();
    var  sam_ide="<?php echo $store_detail->st_city ?>";
    $.ajax({
        url: '<?php echo base_url()?>admin/vendor/citystate',
        type: 'POST',
        //dataType: 'json',
        data: {'stid': stid,'sam_ide':sam_ide},
        success: function(data){
        var  option_brand = '<option value="none">Select City</option>';
        $('#sto_city').empty();
        $("#sto_city").append(option_brand+data);
           
         }
  });
   }

</script>