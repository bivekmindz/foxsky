<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Update Right Feature</h2>
    </div>
        <div class="page_box">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/feature/viewfeaturemasterright"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        <form action="" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Feature Type <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input  type="text" name="featnameT" value="<?php echo $vieww->FeatureName ?>" id="featnameT" readonly/></div>
            </div>
        </div>
        </div>
          <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Left Feature <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input  type="text" name="featnameC" value="<?php echo $vieww->LeftValue ?>" id="featnameC" readonly/></div>
            </div>
        </div>
        </div>
       
        </div>
        <div class="sep_box">
         <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Right Value <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input  type="text" name="featname" value="<?php echo $vieww->RightValue ?>" id="featname" /></div>
            </div>
        </div>
        </div>
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="Submit1" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>