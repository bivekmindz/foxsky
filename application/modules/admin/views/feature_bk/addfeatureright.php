 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            //attrid : "required",
            //catidd : "required",
         /* email:{
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            agree: "required" */
        },
        // Specify the validation error messages
        messages: {
            attrid : "Attribute required",
            catidd : "Category required",
            /*password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            email: "Please enter a valid email address",
            agree: "Please accept our policy"*/
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Add Feature Master</h2>
   
    </div>
        <div class="page_box">
        <div class="sep_box">
        <div class="col-lg-12">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/feature/viewfeaturemasterright"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
        <form action="" id="addCont" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="catidd" id="catidd" onchange="selectleftfeature();" >
                <option>Select Category</option>
                <?php foreach ($parentCatdata as $key => $value) {
                 ?>
                  <option value="<?php echo $value->catid ?>"><?php echo $value->catname ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>
        <div class="col-lg-6"  id="fetaurevalue">
        <div class="row">
            <div class="col-lg-2">
                <div class="tbl_text">Feature Left Value <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select id="feavalueid" name="feavalueid">
                <option value="">Select Left Value</option>
              <!--   <?php  foreach ($leftvalue as $key => $value) { ?>
                   <option value="<?php echo $value->FeatureLeftID ?>"><?php  echo $value->LeftValue ?></option>
             <?php   }   ?> -->

                </select></div>
            </div>
        </div>
        </div>
       
        </div>
     

        <div style='width:30px; float:left; padding:5px; border:1px solid silver; font-weight:bold; cursor:pointer;position: absolute; top: 205px;left: 25px;' id="addMore">(+)</div>

        <div id="attBox" style='padding-bottom:5px;'>
        <div class="sep_box repAttr" style='border-bottom:1px solid #E8E8E8; padding-bottom:5px;'>
         <div class="col-lg-1"></div>
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Right Feature <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input type="text" name="rightvalue[]" id="rightvalue"></div>
            </div>
        </div>
        </div>
        </div>
        </div>

     
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-5">
                <div class="submit_tbl">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
 <script type="text/javascript">
   function selectvalue(){
    var featid=$('#featid').val();
    if(featid==0){
        $('#fetaurevalue').hide();
    }else{
        $('#fetaurevalue').show();
    }
     
    $.ajax({
        url: '<?php echo base_url()?>admin/feature/featurevalue',
        type: 'POST',
        //dataType: 'json',
        data: {'featid': featid},
        success: function(data){
            //alert(data);
            var  option_brand = '<option value="">Select Feature Left value</option>';
            $('#feavalueid').empty();
            $("#feavalueid").append(option_brand+data);
        }
    });
    
   }
   

 </script>


 <script type="text/javascript">

$(document).ready(function(){
       
        $("#addMore").click(function(){

            var remme = "<div style='width:30px; float:left; padding:5px; border:1px solid #FF3C01;color:#FF3C01;text-align: center;cursor:pointer;    position: absolute;left:25px;' class='remMore' onclick='delme($(this));'>(-)</div>";
            $(".repAttr:last-child").clone(true).appendTo("#attBox");
            //alert($(".repAttr:last-child div.remMore").length);

            if($(".repAttr:last-child div.remMore").length == 0){
                $(".repAttr:last-child").prepend(remme);
                return false;
            }
        }); 
    });  

    function delme(vv){
        $(vv).parent().remove();
        return false;    
    }


    function selectleftfeature()
    {
        var catid=$('#catidd').val();
       // alert(catid);
       $.ajax({
        url: '<?php echo base_url()?>admin/feature/featureleftvalue',
        type: 'POST',
        //dataType: 'json',
        data: {'catid': catid},
        success: function(data){
            //alert(data);
             var  option_brand = '<option value="">Select Feature left value</option>';
            $('#feavalueid').empty();
            $("#feavalueid").append(option_brand+data);
        }
    });
    }
 </script>