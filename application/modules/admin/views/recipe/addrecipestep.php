  <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            rep_step_title  : "required",
            rep_desc        : "required",
            rep_image       : "required",
        },
        // Specify the validation error messages
        messages: {
            rep_step_title  : "Title is required",
            rep_desc        : "Description is required",
            rep_image       : "Image is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
            
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Recipe Steps</h2>
                    </div>
                    <div class="page_box">
                        
                        <div class="steps_main">
                            <ul>
                                <li><a href="javascript:void(0);">Step 1</a></li>
                                <li class="active_steps"><a href="javascript:void(0);">Step 2</a></li>
                            </ul>
                        </div> <!--end of steps_main-->

                    <div class="sep_box">
                        <div class="col-lg-12">
                            <div class='flashmsg'>
                                <?php
                                  if($this->session->flashdata('message')){
                                    echo $this->session->flashdata('message'); 
                                  }
                                ?>
                            </div>
                        </div>
                    </div>

        <form id="addCont" action="" method="post" enctype="multipart/form-data" >
            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Recipe step Title <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input id="rep_step_title" name="rep_step_title" type="text" /></div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="sep_box">    
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Recipe step<br>Description <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><textarea name="rep_desc" id="rep_desc"></textarea></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="tbl_text">Recipe step Image <span style="color:red;font-weight: bold;">*</span></div>
                        </div>
                        <div class="col-lg-8">
                            <div class="tbl_input"><input id="rep_image" name="rep_image" type="file" /></div>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-8">
                            <div class="submit_tbl">
                                <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div>
</div> 