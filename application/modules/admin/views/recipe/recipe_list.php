<style type="text/css">
.grid_tbl td a i.fa-list-alt {
    background: #01773c;
    padding: 4px 0px;
    text-align: center;
    color: #fff;
    width: 20px;
    height: 20px;
    border-radius: 70%;
    margin-right: 5px;
}   
</style>
<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Recipies</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                    <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can Manage Recipies.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/recipe/addrecipe"><button>ADD Recipe</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                 <form method="post" action="">
                                 
                                <input type="submit"  name="submit" value="Delete" style="background: #109a55!important;">
                                <table class="grid_tbl" id="search_filter">
                                    <thead>

                                        <tr>
                                           <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span>
                                            <th>S.NO.</th>
                                            <th>Recipe Title</th>
                                            <th>Recipe Ingredient</th>
                                            <th>View Recipe Steps</th>
                                            <th>Updated by</th>
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0;
                                     if(!empty($recipelist))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          } 
                                        foreach ($recipelist as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><input type='checkbox' name='recipedelete[]' class='chkApp' value='<?=$value->r_id?>'> </td> 
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->r_title ;?></td>
                                            <td><?php echo $value->r_ingredient ;?></td>
                                            <td><a href="<?php echo base_url()?>admin/recipe/recipe_steps_list/<?php echo $value->r_id?>" ><i class="fa fa-list-alt"></i></a></td>
                                            <td><?php if(empty($value->r_modifiedby)){ echo $value->u_createdby; } else { echo $value->u_modifiedby; } ?></td> 
                                            <td><?php if(empty($value->r_modifiedby)){ echo $value->r_createdon; } else { echo $value->r_updatedon; } ?></td>
                                             <td><a href="<?php echo base_url()?>admin/recipe/updaterecipe/<?php echo $value->r_id?>" ><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo base_url()?>admin/recipe/deleterecipe/<?php echo $value->r_id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/recipe/statusrecipe/<?php echo $value->r_id.'/'.$value->r_status; ?>"><?php if($value->r_status==1) { ?>Inactive<?php } else { ?>Active<?php } ?></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

<script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
    </script>