<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2> Product accessories</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can view the approved product accessories list.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">

                            <div class="col-lg-12">
                    
                                
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                 <form method="post" action="">
                                 <!-- <input type="submit"  name="submit" value="Delete">  -->
                                <input type="submit"  name="submitstatus" value="Status">&nbsp;&nbsp;&nbsp;

                                 <input type="submit"  name="submitstar" value="Star Status">


                                <table class="grid_tbl" id="search_filter">
                                    <thead>

                                        <tr>
                                          <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span>
                                              <!-- / <br>
                                           <span style='border:0px color:blue; cursor:pointer;' id='DeselAll'>Deselect</span> --></th>
                                            <th>S:NO</th>
                                            <th>accessories Image</th>
                                            
                                            <th>accessories Name</th>
                                          
                                            <th>Approved Date</th>
                                            <th style="width:110px;">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                     if(!empty($attproduct))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                    foreach ($attproduct as $key => $value) {
                                        //p($value);
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><input type='checkbox' name='attdelete[]' class='chkApp' value='<?=$value->proid?>'> </td> 
                                            <td><?php echo $i ;?></td>
                                            <td><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value->prd_main_image; ?>" height="75" width="50"></td>
                                           
                                            <td><?php echo $value->proname ;?></td>
                                          
                                            <td><?php if(empty($value->createdon)){ echo $value->createdon; } else { echo $value->createdon; } ?></td>
                                           
                                            <td>
                                            <a href="<?php echo base_url()?>admin/productdata/editaccessories/<?php echo $value->proid?>/<?php echo $value->prodtmappid?>?<?php echo $_SERVER['QUERY_STRING']; ?>"><i class="fa fa-pencil"></i></a>


                                            <a href="<?php echo base_url(); ?>admin/productdata/approve_status/<?php echo $value->proid.'/'.$value->prostatus; ?>"><?php if($value->prostatus=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                             
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                              <!--   <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div> -->
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>




 <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
    </script>