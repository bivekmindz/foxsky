<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Approve Product</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>  In this Section Admin can view the approved product list.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">

                            <div class="col-lg-12">
                             <!--    <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/feature/addcatfeature"><button>ADD Feature Master</button></a>
                                </div> -->
                                <form method="post" action="">
                                  <input class="xls_download" type="submit" value="Download Excel" name="newsexcel">
                                </form>
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                 <form method="post" action="">
                              
                                 <input type="submit"  name="submitstar" value="Star Status">
<input type="submit"  name="deletestatus" value="Delete Product">

                                <table class="grid_tbl" id="search_filter">
                                    <thead>

                                        <tr>
                                          <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span>
                                              <!-- / <br>
                                           <span style='border:0px color:blue; cursor:pointer;' id='DeselAll'>Deselect</span> --></th>
                                            <th>S:NO</th>
                                            <th>Product Image</th>
                                            <!-- <th>Manufacture</th> -->
                                            <th>Sku Number</th>
                                            <th>Product Name</th>
                                            <th>Category</th>
                                            <!-- <th>Brand</th> -->
                                            <th>Qty</th>
                                            <th>Mrp</th>
                                            <th>Selling Price</th>
                                           <!--  <th>Store Name</th>
                                            <th>Approved By</th> -->
                                            <th>Approved Date</th>
                                            <th style="width:110px;">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                     if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                    foreach ($vieww as $key => $value) {
                                       // p($value);
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><input type='checkbox' name='attdelete[]' class='chkApp' value='<?=$value->prodtmappid?>'> </td> 
                                            <td><?php echo $i ;?></td>
                                            <td><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value->Image; ?>" height="75" width="50"></td>
                                            <!-- <td><?php echo $value->firstname ;?></td> -->
                                            <td><?php echo $value->SKUNO ;?></td>
                                            <td><?php echo $value->ProductName ;?></td>
                                            <td><?php echo $value->CatName ;?></td>
                                           <!--  <td><?php echo $value->BrandName ;?></td> -->
                                            <td><?php echo $value->proQnty ;?></td>
                                            <td><?php echo $value->productMRP ;?></td>
                                            <td><?php echo $value->SellingPrice ;?></td>
                                            <!-- <td><?php echo $value->st_store_name ;?></td>
                                            <td><?php if(empty($value->ModifiedBy)){ echo $value->u_createdby; } else { echo $value->u_modifiedby; } ?></td> --> 
                                            <td><?php if(empty($value->ModifiedBy)){ echo $value->CreatedOn; } else { echo $value->ModifiedOn; } ?></td>
                                            <input type='hidden' name='attstatu[<?php echo $value->id;?>]'  value='<?=$value->ProStatus?>'>
                                            <td>
                                           <!--  <a href="<?php echo base_url()?>admin/attribute/attmapupdate/<?php echo $value->id?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/productdata/deleteappproduct/<?php echo $value->ProId?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>  -->
                                           <a href="<?php echo base_url()?>admin/productdata/editproduct/<?php echo $value->id?>/<?php echo $value->prodtmappid?>?<?php echo $_SERVER['QUERY_STRING']; ?>"><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/productdata/approve_status/<?php echo $value->id.'/'.$value->prodtmappid.'/'.$value->ProStatus; ?>"><?php if($value->ProStatus=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                             
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                              <!--   <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div> -->
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
      <script type="text/javascript">
 /* $(document).ready(function(){
    $('#selAll').click(function(){   
      $('.chkApp').each(function() { //loop through each checkbox
          this.checked = true;  //select all checkboxes with class "checkbox1"               
      });
});

  });
$(document).ready(function(){
    $('#DeselAll').click(function(){   
      $('.chkApp').each(function() { //loop through each checkbox
          this.checked = false;  //select all checkboxes with class "checkbox1"               
      });
});

  });*/


</script>



 <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
    </script>