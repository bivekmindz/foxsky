<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            mcatidd : "required",
            catidd : "required",
            subcatidd : "required",
            userfile : "required",

        },
        // Specify the validation error messages
        messages: {
            mcatidd : "Main Category is required",
            catidd : "Sub-Category is required",
            subcatidd : "Child Sub-Category is required",
            userfile : "Product File is required",

        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2"> <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Bulk Product Upload</h2>
   
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p>In this Section Admin can download and upload excel Category wise to update the products </p>
                        </div>
                    </div>
        <div class="page_box">
        <div class="sep_box">
        <div class="col-lg-12">
  
         <div class='flashmsg'>
            <?php
              if($updatesuccess!=""){
                echo $updatesuccess; 
              }
            ?>
        </div>
        <div style="color: red;" class='flashmsg'>
            <?php
              if($updatefail!=""){
                echo $updatefail; 
              }
            ?>
        </div>
        </div>
        </div>
              
        <form id="addCont" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-12">
        <h3>Choose Category and Upload File</h3></div>
       
       <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Main Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                    <select name="mcatidd" id="mcatidd" onclick="childcategory();" >
                <option value="">Select Category</option>
                <?php foreach ($parentCatdata as $key => $value) {
                 ?>
                  <option value="<?php echo $value->catid ?>"><?php echo $value->catname ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>   
            
        </div>

        <div class="sep_box">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="catidd" id="catidd" onclick="subchildcategory();"  >
                <option value="">Select Category</option></select></div>
            </div>
        </div>
        </div>
        </div>

        <div class="sep_box">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Child Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="subcatidd" id="subcatidd">
                <option value="">Select Category</option></select></div>
            </div>
        </div>
        </div>
        </div>

        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
        <div class="col-lg-4">Select file <span style="color:red;font-weight: bold;">*</span></div>
        <div class="col-lg-8"><input type="file" name="userfile" id="userfile" /></div>
        </div>
        </div>
        
        </div>
          <div class="sep_box">
          <div class="col-lg-6">
          <div class="col-lg-4"></div>
        <div class="col-lg-8">
        <div class="submit_tbl">
        <input type="submit" name="submit" id="submit" value="submit" class="btn_button sub_btn"/></div></div></div>
          </div>
            </form>      
                </div>
            </div>
        </div>
    </div>
    </div>

<script type="text/javascript">

function childcategory(){
    var catid=$('#mcatidd').val();
    
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/catvalue',
        type: 'POST',
        //dataType: 'json',
        data: {'catid': catid},
        success: function(data){
             
            var  option_brand = '<option value="">Select Category</option>';
            $('#catidd').empty();
            $("#catidd").append(option_brand+data);
        }
    });
    
   }

   function subchildcategory(){
        var catid=$('#catidd').val();
            //alert(catid);
         
            $.ajax({
                url: '<?php echo base_url()?>admin/attribute/subbranddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                      //alert(data);
                      var  option_brand = '<option value="">Select Category</option>';
                      $('#subcatidd').empty();
                      $("#subcatidd").append(option_brand+data);  
                     
                }
            });
    }

</script>