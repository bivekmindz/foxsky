<style>
	.mt-10{ margin-top:10px;}
	span.full-width{width: 100%;text-align: center;float: left;}
	span.full-width a{ display: inline-block; }
</style>
<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Rejected Product</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view the Rejected product list.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <table class="grid_tbl" id="search_filter">
                                    <thead>

                                        <tr>
                                            <th>S:NO</th>
                                            <th>Product Image</th>
                                            <th>User Name</th>
                                            <th>Sku Number</th>
                                            <th>Product Name</th>
                                            <th>Category</th>
                                            <th>Brand</th>
                                            <th>Qty</th>
                                            <th>Mrp</th>
                                            <th>Selling Price</th>
                                            <th>Store Name</th>
                                            <th>Rejected By</th>
                                            <th>Rejected On</th>
                                            <th>Reason</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                    if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                       foreach ($vieww as $key => $value) { 
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value->venproimgpath; ?>" height="75" width="50"></td>
                                            <td><?php echo $value->firstname ;?></td>
                                            <td><?php echo $value->skuno ;?></td>
                                            <td><?php echo $value->productname ;?></td>
                                            <td><?php echo $value->catname ;?></td>
                                            <td><?php echo $value->brandname ;?></td>
                                            <td><?php echo $value->ProductQty ;?></td>
                                            <td><?php echo $value->prodmrp ;?></td>
                                            <td><?php echo $value->prodsellingprice ;?></td>
                                            <td><?php echo $value->st_store_name ;?></td>
                                            <td><?php echo $value->u_modifiedby ;?></td>
                                            <td><?php echo $value->modifiedOn ;?></td>
                                            <td><?php echo $value->reject_reason ;?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>