<?php //p($data); ?>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Edit Product</h2>
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add Single Product with details.</p>
                        </div>
                    </div>
        <div class="page_box">
<form action="" method="post" enctype="multipart/form-data">
<div class="form">
<span id="message"></span>

<div class="sep_box">

    <div class="col-lg-6" style="display:none">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Brand Name <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><select name="brandid" id="brandid" required >
            
               <option value="<?php echo  $data[0]->BrandID; ?>"><?php echo  $data[0]->BrandName; ?></option>
             
              
            </select>
           </span></div>
          </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Category Name <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><select name="catid" id="catid" required >
               <option value="<?php echo  $data[0]->CatId; ?>"><?php echo  $data[0]->CatName; ?></option>
              
            </select></div>
          </div>
      </div>
    </div>
    </div>

    <!-- <div id='prodattributes' style="display:none;"></div> --> 

    <div class="sep_box">
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">Color</div></div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><select name="colorname" id="colorname">
              <option value="">Select Color</option>
              <?php
              foreach($viewcolor as $k=>$v){ ?>
    
      <option value="<?php echo $v->id.'#'.$v->attmapname; ?>" <?php if($attr[0]->attributevalue==$v->attmapname){ echo 'selected'; } ?>><?php echo $v->attmapname; ?></option>
 
  <?php } ?>     
      </select>
<input type="hidden" name="attribute[]" value="<?php echo $attr[0]->attmapid; ?>">
      </span></div>
          </div></div>
        </div>
      <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Age
       </div></div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><select name="agename" id="agename">
              <option value="">Select Age</option>
            <?php  foreach($viewage as $k=>$v){ ?>
    
    <option value="<?php echo $v->id.'#'.$v->attmapname; ?>" <?php if($attr[1]->attributevalue==$v->attmapname){ echo 'selected'; } ?>><?php echo $v->attmapname; ?></option>
 
  <?php } ?>
         </select>
   <input type="hidden" name="attribute[]" value="<?php echo $attr[1]->attmapid; ?>">
   </span></div>
          </div></div>
      </div></div>


    <div class="sep_box">
  <div class="col-lg-6">
      <div class="row"><div class="col-lg-4">
          <div class="tbl_text">Product Name <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="pname" value="<?php echo  $data[0]->proname; ?>" id="pname" class="required" field="name" required></span></div></div></div>
      </div>

      <div class="col-lg-6">
      <div class="row"><div class="col-lg-4">
          <div class="tbl_text">Article Number <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="anumber" value="<?php echo  $data[0]->SKUNO; ?>" id="anumber" class="required" field="name" ></span></div></div></div>
      </div>
  </div>
  
  <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">Product SKU <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="psku" value="<?php echo  $data[0]->prosku; ?>" id="psku" class="required" field="name" ></span></div></div></div>
      </div>
      <div class="col-lg-6">
      <div class="row"><div class="col-lg-4">
          <div class="tbl_text">Barcode Number</div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="barcode" value="<?php echo  $data[0]->barcode; ?>" id="barcode" class="required" /></span></div></div></div>
      </div>  
  </div>

  <div class="sep_box">
<div class="col-lg-6">
      <div class="row"><div class="col-lg-4">
          <div class="tbl_text">Product Description</div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="pdes" value="<?php echo  $data[0]->description; ?>" id="pdes" class="required" field="name" ></span></div></div></div>
      </div>
      <div class="col-lg-6">
      <div class="row"><div class="col-lg-4"><div class="tbl_text">Stock QTY <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="qty" value="<?php echo  $data[0]->proQnty; ?>" id="qty" class="required" field="name" ></span></div></div></div>
      </div>  
  </div>

  <div class="sep_box">
<div class="col-lg-6">
      <div class="row"><div class="col-lg-4"><div class="tbl_text">Purchase Price <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8"><div class="tbl_input"><span><input type="text" name="pprice" value="<?php echo  $data[0]->purchaseprice; ?>" id="pprice" class="required" field="name" onblur="profit_margin();"></span></div>
          </div></div>
      </div>
      <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">MRP <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="pmrp" value="<?php echo  $data[0]->productMRP; ?>" id="pmrp" class="required" field="name" onblur="profit_margin();"></span></div></div></div></div>  
  </div>

   <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">Selling Price <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8"><div class="tbl_input"><span><input type="text" name="sprice" value="<?php echo  $data[0]->SellingPrice; ?>" id="sprice" class="required" field="name" onblur="profit_margin();"></span></div></div></div>
      </div>
      <!-- <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">CityGroup-1 percent <span style="color:red;font-weight: bold;">*</span>
</div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="cg1p" value="<?php echo  $city[0]->cityvalue; ?>" id="cg1p" class="required" field="name" ></span></div></div></div>
      </div>
  
  </div>
  <div class="sep_box">
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">CityGroup-2 percent <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="cg2p" value="<?php echo  $city[1]->cityvalue; ?>" id="cg2p" class="required" field="name" ></span></div></div></div></div>
      <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">CityGroup-3 percent <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="cg3p" value="<?php echo  $city[2]->cityvalue; ?>" id="cg3p" class="required" field="name" ></span></div>
          </div></div>
      </div>
  </div>

  <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">ProfitMargin percent
</div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="pmargin" value="<?php echo  $data[0]->profitmargin; ?>" id="pmargin" class="required" field="name" ></span></div></div></div>
      </div>
      <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">Discount percent</div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="discount" value="<?php echo  $data[0]->discount; ?>" id="discount" class="required" field="name" ></span></div></div>
          </div>
      </div>
  </div>
  
  <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">Range1 <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="range1" value="<?php echo  $data[0]->qty1; ?>" id="range1" class="required" field="name" ></span></div>
          </div></div>
  </div>  
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">Range2 <span style="color:red;font-weight: bold;">*</span></div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="range2" value="<?php echo  $data[0]->qty2; ?>" id="range2" class="required" field="name" ></span></div>
        </div>
        </div>
      </div></div>
  
   
<div class="sep_box">

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Range3 <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="range3" value="<?php echo  $data[0]->qty3; ?>" id="range3" class="required" field="name" required></span></div>
          </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Price1 percent <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="price1" value="<?php echo  $data[0]->price; ?>" id="price1" class="required" field="name" ></span></div>
          </div>
      </div>
    </div>
    </div>
    <div class="sep_box">


<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Price2 percent <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="price2" value="<?php echo  $data[0]->price2; ?>" id="price2" class="required" /></span></div>
          </div>
      </div>
    </div>
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Price3 percent <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="price3" value="<?php echo  $data[0]->price3; ?>" id="price3" class="required" field="name" ></span></div>
          </div>
      </div>
    </div>

</div>
<div class="sep_box">

 <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Height</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="hight" value="<?php echo  $data[0]->height; ?>" id="hight" class="required" ></span></div>
          </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Measuring Unit1</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="munit1" value="<?php echo  $data[0]->heighttype; ?>" id="munit1" class="required" field="name" ></span></div>
          </div>
      </div>
    </div>
    </div>
    
    <div class="sep_box">

   
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Breadth</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="breadth" value="<?php echo  $data[0]->width; ?>" id="breadth" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>

     
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Measuring Unit2</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           <span><input type="text" name="munit2" id="munit2" value="<?php echo  $data[0]->widthtype; ?>" class="required" field="name"/></span>
          </div>
          </div>
      </div>
    </div>
      </div>
      <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Length</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="length" value="<?php echo  $data[0]->length; ?>" id="length" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Measuring Unit3</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="munit3" value="<?php echo  $data[0]->lenghttype; ?>" id="munit3" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>
  </div>
   
  <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Weight

</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="weight" value="<?php echo  $data[0]->weight; ?>" id="weight" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Measuring Unit4</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="munit4" value="<?php echo  $data[0]->weighttype; ?>" id="munit4" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>
  </div>
    <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Volumetric Weight_KG</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="volweight" value="<?php echo  $data[0]->volumetricweight; ?>" id="volweight" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Product warranty</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="pwarranty" value="<?php echo  $data[0]->warrenty; ?>" id="pwarranty" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>
  </div>
    <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">MetaTag1</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="metatag1" value="<?php echo  $data[0]->metatitle; ?>" id="metatag1" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">MetaTag2</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="metatag2" value="<?php echo  $data[0]->metakeywords; ?>" id="metatag2" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>
  </div>
    <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">MetaTag3</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="metatag3" value="<?php echo  $data[0]->metadescription; ?>" id="metatag3" class="required" field="name" ></span></div>
          </div>
          </div>
      </div> -->
        <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Product warranty</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="text" name="pwarranty" value="<?php echo  $data[0]->warrenty; ?>" id="pwarranty" class="required" field="name" ></span></div>
          </div>
          </div>
      </div>
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">MainImage <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="file" name="mainimg" value="" id="mainimg" class="required" field="name" >
<input type="hidden" name="old_mainimg" value="<?php echo $data[0]->Image; ?>" >
<input type="hidden" name="mainimg_id" value="<?php echo $image[0]->proimgid; ?>" >
            </span>
 <?php if(!empty($data[0]->Image) && $data[0]->Image!='na') { ?>
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $data[0]->Image; ?>" height="75" width="50">   <?php } ?>
            </div>
          </div>
          </div>
      </div>
  </div>
    <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Image1</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="file" name="img1" value="" id="img1" class="required" field="name" >
<input type="hidden" name="old_img1" value="<?php echo $data[0]->prd_img1; ?>" >
<input type="hidden" name="img1_id" value="<?php echo $data[0]->proimgid; ?>" >
            </span>
            <?php if(!empty($data[0]->prd_img1) && $data[0]->prd_img1!='na') { ?>
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $data[0]->prd_img1; ?>" height="75" width="50">   <?php } ?>

            </div>
          </div>
          </div>
      </div>
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Image2</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="file" name="img2" value="" id="img2" class="required" field="name" >
<input type="hidden" name="old_img2" value="<?php echo $data[0]->prd_img2; ?>" >
<input type="hidden" name="img2_id" value="<?php echo $data[0]->proimgid; ?>" >
            </span>
 <?php if(!empty($data[0]->prd_img2) && $data[0]->prd_img2!='na') { ?>
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $data[0]->prd_img2; ?>" height="75" width="50">   <?php } ?>
            </div>
          </div>
          </div>
      </div>
  </div>
  <div class="sep_box">
<div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Image3
</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
            <span><input type="file" name="img3" value="" id="img3" class="required" field="name" >
<input type="hidden" name="old_img3" value="<?php echo $data[0]->prd_img3; ?>" >
<input type="hidden" name="img3_id" value="<?php echo $data[0]->proimgid; ?>" >
            </span>
 <?php if(!empty($data[0]->prd_img3) && $data[0]->prd_img3!='na') { ?>
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $data[0]->prd_img3; ?>" height="75" width="50">   <?php } ?>
            </div>
          </div>
          </div>
      </div>
  <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4">
          <div class="tbl_text">Image4
</div>
        </div>
        <div class="col-lg-8">
          <div class="tbl_input">
           
            <span><input type="file" name="img4" value="" id="img4" class="required" field="name" >
<input type="hidden" name="old_img4" value="<?php echo $data[0]->prd_img4; ?>" >
<input type="hidden" name="img4_id" value="<?php echo $data[0]->proimgid; ?>" >
            </span>
 <?php if(!empty($data[0]->prd_img4) && $data[0]->prd_img4!='na') { ?>
<img src="<?php echo base_url(); ?>images/thumimg/<?php echo $data[0]->prd_img4; ?>" height="75" width="50">   <?php } ?>

            </div>
          </div>
          </div>
      </div>
  </div>



<div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input type="submit" name="proupdate" class="btn_button sub_btn" value="Update">
                </div>
            </div>
        </div>
        </div>

        </div>


</form> </div>

</div>
</div>
<script>
function profit_margin()
{
  var sp=$('#sprice').val();
  if(sp!="" && $('#pprice').val()!="")
  {
     pm=(parseInt(sp)-parseInt($('#pprice').val()))*100/parseInt($('#pprice').val());
     $('#pmargin').val(pm.toFixed(2));
  }else{
    $('#pmargin').val('');
  }
  if(sp!="" && $('#pmrp').val()!=""){
     dis=(parseInt($('#pmrp').val())-parseInt(sp))*100/parseInt($('#pmrp').val());
     $('#discount').val(dis.toFixed(2));
  }else{
    $('#discount').val(''); 
  }
  
}
</script>