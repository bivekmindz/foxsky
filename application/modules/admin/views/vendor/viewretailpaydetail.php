<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>View Retailer Payment Details</h2>
                      </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view the Retailer Payment Details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <form method="post" action="">
                                      <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
                                    </form>  
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <?php foreach ($totalrempay as $key => $value) {
                                      if($value->PaymentStatus=='unpaid' && $value->remamt==''){
                                            $grtot+=$value->TotalAmt; 
                                        } else {
                                            if($value->PaymentStatus!='paid'){ 
                                              $remaww = $value->TotalAmt-$value->remamt;
                                              $grtot+=$remaww;
                                            }
                                        }
                                    } ?>
                                    <h3 style="float:right;width:auto;">Remaining Balance :- Rs. <?php echo number_format($grtot,1,".",""); ?></h3>
                                    
                                <div style="float:left; width:auto; line-height: 30px;">
                                <p><b>Retaler Name :- </b><?php echo ucwords($vendetail->firstname).' '.ucwords($vendetail->lastname);?></p>
                                <p><b>Retailer Code :- </b><?php echo $vendetail->vendorcode;?></p>
                                </div>
                                <form method="post" action="">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S.NO.</th>
                                            <th>Order Number</th>
                                            <th>Order Date</th>
                                            <th>Order Status</th>
                                            <th>Order Amount</th>
                                            <th>Payment Type</th>
                                            <th>Paid Amount</th>
                                            <!-- <th>Remaining Amount</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; foreach ($orddetail as $key => $value) { 
                                            if($orddetail[$key-1]->OrderNumber!=$value->OrderNumber && $orddetail[$key-1]->OrderNumber!=''){
                                                $rema="";
                                            }
                                            ?>
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->OrderNumber;?></td>
                                            <td><?php echo $value->OrderDate;?></td>
                                            <td><?php echo $value->PaymentStatus;?></td>
                                            <td><?php echo number_format($value->TotalAmt,1,".","");?></td>
                                            <td><?php echo $value->pp_paymentmode;?></td>
                                            <td><?php echo $value->pp_ammount;?></td>
                                            <!-- <td><?php if($value->PaymentStatus=='unpaid' && $value->pp_ammount==''){
                                                    echo number_format($value->TotalAmt,1,".","");
                                                } else {
                                                    if($value->PaymentStatus=='paid'){ 
                                                        echo '0.0'; 
                                                    } else { 
                                                        if($value->OrderId==$value->pp_orderid){
                                                            $rema+=$value->pp_ammount;
                                                            $remain=$value->TotalAmt-$rema;
                                                            echo number_format($remain,1,".",""); 
                                                        } // end if
                                                    } // end paid else
                                                } // end unpaid else 
                                                ?>
                                            </td> -->
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                               </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>