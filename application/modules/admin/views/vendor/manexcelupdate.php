<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">
         
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                    
                        <h2>View Vendor Updated Product Excels</h2>
                       
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>In this Section Admin can view the Vendor updated Product Excel Files list.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                              <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
<!---############################# search section start #####################################################-->
                            <!-- <div class="sep_box">
                            <form method="get" action='<?php echo base_url('admin/vendor/manufactuerexcelupdate'); ?>' id='searchfrm'>
                            <div class="col-lg-12">Filter By Vendor</div>
                            <div class="col-lg-3">
                            <div class="tbl_input">
                            <select id="filter_by" name="filter_by">
                            <option value="">Select Vendor</option>
                            <?php foreach ($viewwman as $key => $value) { ?>
                            <option value="<?php echo $value->id;?>"><?php echo $value->firstname.' '.$value->lastname.' ( '.$value->vendorcode.' )';?></option>
                            <?php } ?>
                            </select>
                            </div>
                            </div>
                            <div class="col-lg-3">
                            <div class="submit_tbl">
                            <input id="validate_frm" type="submit" value="Search" class="btn_button sub_btn">
                            <input type="button" onclick='location.href="<?php echo base_url('admin/vendor/manufactuerexcelupdate'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                            </div>
                            </div>
                            </form>
                            </div> -->
  <!---############################# search section end #####################################################-->
                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th>S.NO.</th>
                                            <th>Vendor Code</th>
                                            <th>Vendor Name</th>
                                            <th>Category Name</th>
                                            <th>Excel Upload Date</th>
                                            <th>Download Excel File</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                          foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->vendorcode;?></td>
                                            <td><?php echo $value->firstname.' '.$value->lastname;?></td>
                                            <td><?php echo $value->catname;?></td>
                                            <td><?php echo $value->mexb_createdon;?></td>
                                            <!-- <td><a href="<?php echo base_url();?>admin/vendor/mandownloadexcelupdate/<?php echo $value->mexb_id;?>"><button>Download Excel</button></a></td> -->
                                            <td><a href="<?php echo base_url().'manbulkproductexcels/'.$value->mexb_filename;?>" download><button>Download Excel</button></a></td>
                                        </tr>    
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

  