<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">
         
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                    
                        <h2>View Vendor Requests list</h2>
                       
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>In this Section Admin can view the Vendor Requests list.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                              <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
 <!---###### search section start -->
                            <!-- <form method="get" action='<?php echo base_url('admin/vendor/manufactuerrequest'); ?>' id='searchfrm'>
                            <div class="col-lg-12">Filter By Vendor</div>
                            <div class="col-lg-3">
                            <div class="tbl_input">
                            <select id="filter_by" name="filter_by">
                            <option value="">Select Vendor</option>
                            <?php foreach ($viewwman as $key => $value) { ?>
                            <option value="<?php echo $value->id;?>"><?php echo $value->firstname.' '.$value->lastname.' ( '.$value->vendorcode.' )';?></option>
                            <?php } ?>
                            </select>
                            </div>
                            </div>
                            <div class="col-lg-3">
                            <div class="submit_tbl">
                            <input id="validate_frm" type="submit" value="Search" class="btn_button sub_btn">
                            <input type="button" onclick='location.href="<?php echo base_url('admin/vendor/manufactuerrequest'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                            </div>
                            </div>
                            </form> -->
                            </div>
                            </div>
                  <!---### search section end -->
                        <form method="post" action="">
                        <div class="sep_box">
                          <div class="col-lg-12">
                          <div class="submit_tbl">
                            <input type="submit" class="btn_button sub_btn" name="apprequest" id="apprequest" value="Approve Request">
                            </div>
                          </div> 
                          </div>
                          <div class="sep_bo">
                          <div class="col-lg-12"> 
                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span></th>
                                            <th>S.NO.</th>
                                            <th>Vendor Code</th>
                                            <th>Vendor Name</th>
                                            <th>Request Summery</th>
                                            <th>Request Status</th>
                                            <th>Request Date</th>
                                            <th>Approved By</th>
                                            <th>Approved Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                    if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                         foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><input type='checkbox' name='reqapp[]' class='chkApp' value='<?=$value->rq_id?>'> </td>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->vendorcode;?></td>
                                            <td><?php echo $value->firstname.' '.$value->lastname;?></td>
                                            <td><?php echo $value->rq_summery;?></td>
                                            <?php if($value->rq_status=='pending'){ ?>
                                            <td style="color: red;"><?php echo $value->rq_status ;?></td>
                                            <?php }else{ ?>
                                            <td style="color: green;"><?php echo $value->rq_status ;?></td>
                                            <?php } ?>
                                            <td><?php echo $value->rq_createdon;?></td>
                                            <td><?php echo $value->u_modifiedby;?></td>
                                            <td><?php echo $value->rq_modifiedon;?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                              <!--    <div class="pagi_nation">
                                <ul class="pagination">
                                <?php //foreach ($links as $link) {
                                //p($link); //exit();
                                //echo "<li class='newli'>". $link."</li>";
                                //} ?>
                                </ul>
                                </div> -->
                            </div>
                            </div>    
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
 <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
    </script>
  