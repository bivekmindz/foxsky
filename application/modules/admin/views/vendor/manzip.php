<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">
         
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                    
                        <h2>View Manufactuer Product Image Zip</h2>
                       
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p>In this Section Admin can view the Manufactuer Product Image Zip list.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                              <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
<!---############################# search section start #####################################################-->
                            <form method="get" action='<?php echo base_url('admin/vendor/manufactuerzip'); ?>' id='searchfrm'>
                            <div class="col-lg-12">Filter By Manufacture</div>
                            <div class="col-lg-3">
                            <div class="tbl_input">
                            <select id="filter_by" name="filter_by">
                            <option value="">Select Manufacture</option>
                            <?php foreach ($viewwman as $key => $value) { ?>
                            <option value="<?php echo $value->id;?>"><?php echo $value->firstname.' '.$value->lastname.' ( '.$value->vendorcode.' )';?></option>
                            <?php } ?>
                            </select>
                            </div>
                            </div>
                            <div class="col-lg-3">
                            <div class="submit_tbl">
                            <input id="validate_frm" type="submit" value="Search" class="btn_button sub_btn">
                            <input type="button" onclick='location.href="<?php echo base_url('admin/vendor/manufactuerzip'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                            </div>
                            </div>
                            </form>
  <!---############################# search section end #####################################################-->
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S.NO.</th>
                                            <th>Manufactuer Code</th>
                                            <th>Manufactuer Name</th>
                                            <th>Brand Name</th>
                                            <th>Zip Upload Date</th>
                                            <th>Download Zip File</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                          foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->vendorcode;?></td>
                                            <td><?php echo $value->firstname.' '.$value->lastname;?></td>
                                            <td><?php echo $value->brandname;?></td>
                                            <td><?php echo $value->mex_createdon;?></td>
                                            <td><a href="<?php echo base_url();?>admin/vendor/mandownloadzip/<?php echo $value->mex_id;?>"><button>Download Zip</button></td>
                                        </tr>    
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

  