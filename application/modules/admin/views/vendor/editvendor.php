<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
  <script src="<?php echo admin_url();?>js/jscolor.js"></script>

<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Edit Vendor</h2>
    </div>
        <div class="page_box">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/vendor/viewvendor"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        <form action="" method="post" id="add_vendor_form" enctype="multipart/form-data" >
        <div class="sep_box">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="tbl_text">First Name <span style="color:red;font-weight: bold;">*</span></div>
                    </div>
                    <div class="col-lg-8">
                        <div class="tbl_input"><input id="fname" type="text" value="<?php echo $vieww->firstname ?>"  name="fname"/></div>
                    </div>
                </div>
                </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="tbl_text">Last Name <span style="color:red;font-weight: bold;">*</span></div>
                    </div>
                    <div class="col-lg-8">
                        <div class="tbl_input"><input id="lasname" type="text" value="<?php echo $vieww->lastname ?>" name="lasname"/></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sep_box">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="tbl_text">Email Id <span style="color:red;font-weight: bold;">*</span></div>
                    </div>
                    <div class="col-lg-8">
                        <div class="tbl_input"><input id="emailid" name="emailid" type="text" value="<?php echo $vieww->compemailid ?>" /></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="tbl_text">Mobile Number <span style="color:red;font-weight: bold;">*</span></div>
                    </div>
                    <div class="col-lg-8">
                        <div class="tbl_input"><input type="text" name="mphn" id="mphn" maxlength="11" value="<?php echo $vieww->contactnumber ?>" onkeypress="return isNumberKey(event);"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sep_box">
            <div class="col-lg-6">
              <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Select Main Category <span style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input"><select name="mcatidd[]" id="mcatidd" multiple="multiple">
                    <?php foreach ($parentCatdata as $key => $value) {
                     ?>
                      <option <?php if(in_array($value->catid, $vendor_maincategory)){ echo 'selected'; } ?> value="<?php echo $value->catid ?>"><?php echo $value->catname ?></option>
                     <?php }?>
                    </select></div>
                </div>
              </div>
            </div>
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
               <div class="tbl_text">Shop/Company Name <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                 
                <input  type="text" name="comname" value="<?php echo $vieww->companyname ?>" id="comname"/></div>
            </div>
        </div>
        </div>
        </div>

        <div class="sep_box" id="childcat">
                        <div class="col-lg-6">
                          <div class="row">
                            <div class="col-lg-4">
                                <div class="tbl_text">Select Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                                <div class="tbl_input">
                                <select name="catidd[]" id="catidd">
                                <option value="">Select Sub-Category</option>
                                </select>
                                 </div>
                            </div>
                        </div>
                        </div>
                        </div>
                        
                        <div class="sep_box" id="subchildcat">
                        <div class="col-lg-6">
                          <div class="row">
                            <div class="col-lg-4">
                                <div class="tbl_text">Select Child Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                                <div class="tbl_input">
                                <select name="subcatidd[]" id="subcatidd">
                                <option value="">Select Child Sub-Category</option>
                                </select>
                                 </div>
                            </div>
                        </div>
                        </div>
                       
                        </div>

        
        <div class="sep_box">
          <div class="col-lg-6">
            <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Country <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select name="countryid" id="countryid" onchange="selectstates();">
                    <option value="none">Select Country</option>
                    <?php foreach ($viewcountry as $key => $value) {
                        
                    ?>
                     <option <?php if ($value->conid==$vieww->countryid){ echo 'selected'; } ?> value="<?php echo $value->conid ?>"><?php echo $value->name ?></option>
                    <?php } ?>
                </select>
                </div>
            </div>
            </div>
            </div>
        <div class="col-lg-6">
        <div class="row">
        <div class="col-lg-4">
        <div class="tbl_text">State <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
        <div class="tbl_input">
        <select name="stateid" id="stateid" onchange="statecountry();">
            <option value="none">Select State</option>
        </select>
        </div>
        </div>
        </div>
        </div>
       
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
        <div class="col-lg-4">
            <div class="tbl_text">City <span style="color:red;font-weight: bold;">*</span></div>
        </div>
        <div class="col-lg-8">
            <div class="tbl_input">
        <select name="cityid" id="cityid">
            <option value="none">Select City</option>
        </select>
            </div>
        </div>
        </div>
        </div>
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Address <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <textarea name="vendoraddress" id="vendoraddress"><?php echo $vieww->vendoraddress ?></textarea>
                </div>
            </div>
        </div>
        </div>
        
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Pin Code <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"> <input id="pincode" value="<?php echo $vieww->venpincode ?>" type="text" name="pincode"/ maxlength="6"  onkeypress="return isNumberKey(event);">
                </div>
            </div>
        </div>
        </div>
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Pan Number</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <input id="pannumber" type="text" name="pannumber" value="<?php echo $vieww->panno ?>" maxlength="16"/>
                </div>
            </div>
        </div>
    </div>
       </div>
       
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Account Holder Name</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
               <input id="accountholder" type="text" name="accountholder" value="<?php echo $vieww->AccountHolderName ?>"/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
    <div class="row">
        <div class="col-lg-4">
            <div class="tbl_text">Account Number</div>
        </div>
        <div class="col-lg-8">
            <div class="tbl_input">
            <input id="accountnumber" type="text" value="<?php echo $vieww->AccountNo ?>" name="accountnumber"/>
          
            </div>
        </div>
    </div>
</div>
       </div>

       <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Bank Name</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <input id="banknumber" type="text" value="<?php echo $vieww->VenBankName ?>" name="banknumber"/>
              
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
    <div class="row">
        <div class="col-lg-4">
            <div class="tbl_text">Branch</div>
        </div>
        <div class="col-lg-8">
            <div class="tbl_input">
        <input id="branchname" type="text" value="<?php echo $vieww->venbranchname ?>" name="branchname"/>
          
            </div>
        </div>
    </div>
</div>
       </div>


 <div class="sep_box">
      <div class="col-lg-6">
    <div class="row">
        <div class="col-lg-4">
            <div class="tbl_text">IFSC Code</div>
        </div>
        <div class="col-lg-8">
            <div class="tbl_input">
           <input type="text" id="IfscCode1"  name="IfscCode1" value="<?php echo $vieww->IfscCode ?>" >
-
            
            </div>
        </div>
    </div>
</div>
  <div class="col-lg-6">
    <div class="row">
    <div class="col-lg-4">
        <div class="tbl_text">NEFT Detail</div>
    </div>
    <div class="col-lg-8">
        <div class="tbl_input">
    <input id="neftdetail" type="text" value="<?php echo $vieww->NeftDetail ?>" name="neftdetail"/>
      
        </div>
    </div>
    </div>
    </div>
       </div>


        <div class="sep_box">
       <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Account Type</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select name="AccountType" id="AccountType" onchange="addfield();">
               
                <option <?php if ($vieww->AcountType=='C'){ echo 'selected'; } ?> value="C">Current</option>
                <option <?php if ($vieww->AcountType=='S'){ echo 'selected'; } ?> value="S">Saving</option>
                </select>
              
                </div>
            </div>
        </div>
        </div>
        <div class="col-lg-6">
    <div class="row">
        <div class="col-lg-4">
            <div class="tbl_text">GST Number</div>
        </div>
        <div class="col-lg-8">
            <div class="tbl_input">
       <input type="text"  name="gstno" id="gstno" value="<?php echo $vieww->cstvalue ?>" class="field_de" />
        <input id="sam_id" name="sam_id" type="hidden" value="<?php echo $vieww->stateid;?>" />  
        <input id="sam_ide" name="sam_ide" type="hidden" value="<?php echo $vieww->city;?>" />  
       
            </div>
        </div>
    </div>
</div>
       </div>
       
    
  <div class="sep_box" id="acountsection" >
                            
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Upload GST Copy</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <span><a href="<?php echo base_url(); ?>manufacturer-files/<?php echo $vieww->cstmageurl;?>" target="_blank"><img src="<?php echo base_url(); ?>manufacturer-files/<?php echo $vieww->cstmageurl;?>" height="50" width="50"></a></span>
                                        <input type="hidden" name="memgstfileold" class="form-control" id="memgstfile" value="<?php echo $vieww->cstmageurl;?>">
                                        <input type="file" name="memgstfile" class="form-control" id="memgstfile" value="">
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Upload Cancelled Cheque Copy</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                             <span><a href="<?php echo base_url(); ?>manufacturer-files/<?php echo $vieww->chequeimageurl;?>" target="_blank"><img src="<?php echo base_url(); ?>manufacturer-files/<?php echo $vieww->chequeimageurl;?> " height="50" width="50"></a></span>
                                             <input type="hidden" name="memchequefileold" class="form-control" id="memchequefile" value="<?php echo $vieww->chequeimageurl;?>">
                                       <input type="file" name="memchequefile" class="form-control" id="memchequefile" value="">
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box" id="acountsection" >
                        <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Upload Pan Card Copy</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                             <span><a href="<?php echo base_url(); ?>manufacturer-files/<?php echo $vieww->panimageurl;?>" target="_blank"><img src="<?php echo base_url(); ?>manufacturer-files/<?php echo $vieww->panimageurl;?> " height="50" width="50"></a></span>
                                             <input type="hidden" name="mempanfileold" class="form-control" id="mempanfile" value="<?php echo $vieww->panimageurl;?>">
                                       <input type="file" name="mempanfile" class="form-control" id="mempanfile" value="">
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="Submit1" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>
       </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php $getvendor_subcategory=implode(',', $vendor_subcategory); ?>
    <script type="text/javascript">
    function selectstates(){
     var samagamid=$('#sam_id').val();   
     var countryid=$('#countryid').val();
     $('#staterid').show();
     $.ajax({
        url: '<?php echo base_url()?>admin/vendor/countrystate',
        type: 'POST',
        //dataType: 'json',
        data: {'countryid': countryid,'samagamid':samagamid},
        success: function(data){

        var  option_brand = '<option value="none">Select State</option>';
        $('#stateid').empty();
        $("#stateid").append(option_brand+data);
        statecountry();
           
         }
  });
    
   }
   
selectstates();
   
   function statecountry(){
    var  stid=$('#stateid').val();
    var  sam_ide=$('#sam_ide').val();
    $.ajax({
        url: '<?php echo base_url()?>admin/vendor/citystate',
        type: 'POST',
        //dataType: 'json',
        data: {'stid': stid,'sam_ide':sam_ide},
        success: function(data){
        var  option_brand = '<option value="none">Select City</option>';
        $('#cityid').empty();
        $("#cityid").append(option_brand+data);
           
         }
  });
   }
    </script>
    <script src="<?php echo base_url().'assets/js/jquery.validate.min.js'; ?>"></script>
 <script>
// When the document is ready
    $(document).ready(function () {

         $.validator.addMethod("valueNotEquals", function(value, element, arg){
            return arg != value;
         }, "Value must not equal arg.");

         $.validator.addMethod("pan", function(value, element)
         {
            return this.optional(element) || /^[A-Z]{5}\d{4}[A-Z]{1}$/.test(value);
         }, "Invalid Pan Number");

       // onkey up converts lower case to uppercase
        $('#pannumber,#vendoraddress').keyup(function(){
            this.value = this.value.toUpperCase();
        });
       //----------------------
        /*$.validator.addMethod("chk_add_field", function(value) {
            
            return value != "";
        }, "Please enter Address");
        
        $.validator.addMethod("check_cat", function (value, element) {
           
            var count = $(element).find('option:selected').length;
            return count > 0;
        });*/
     //validation rules
        $("#add_vendor_form").validate({
            ignore: [],
            rules: {
                        "lasname" : {
                            required : true,
                        },
                        "emailid" : {
                            required: true,
                            email: true
                        },
                        "password" : {
                            required : true,
                            minlength : 6,
                        },
                        "comname" : {
                            required : true,
                        },
                        "mphn" : {
                            required : true,
                            rangelength: [10, 10]
                        },
                          "fname" : {
                            required : true,
                        },
                        "mcatidd[]" : {
                            required : true,
                        },
                        "catidd[]" : {
                            required : true,
                        },
                        "subcatidd[]" : {
                            required : true,
                        },
                        "countryid" : {
                            valueNotEquals: "none",
                        },
                         "stateid" : {
                            valueNotEquals: "none",
                        },
                         "cityid" : {
                            valueNotEquals: "none",
                        },
                        "vendoraddress" : {
                            chk_add_field :'',
                            required: true,
                        },
                        "pincode" : {
                            required : true,
                            rangelength: [6, 6],
                            digits : true,
                        },
                        "pannumber" : {
                            //required : true,
                            pan : $('#pannumber').val(),
                        },
                        
                        
                        
              },
            messages: {
                "lasname": {
                    required: "Last name should not be empty",
                },
                "emailid" : {
                            required: "Email id is required",
                            email: "Invalid Email id",
                },
                "password" : {
                            required : "Password filed should not be empty",
                            minlength : "Must have greater than 6 digits",
                },
                "comname" : {
                            required : "Company name is mandatory",
                },
                "mphn" : {
                            required : "Mobile number is mandatory",
                            rangelength : "Mobile number should be exact 10 digits",
                },
                "fname" : {
                            required : "First name should not be empty",
                },
                "mcatidd[]" : {
                            required : "Please select at least one Category",
                },
                "catidd[]" : {
                            required : "Please select at least one Sub-Category",
                },
                "subcatidd[]": {
                            required : "Please select at least one Child Sub-Category",
                }, 
                "countryid" : {
                            valueNotEquals: "Please select any country",
                },
                "stateid" : {
                            valueNotEquals: "Please select any state",
                }, 
                "cityid" : {
                            valueNotEquals: "Please select any city",
                },
                "vendoraddress" : {
                            chk_add_field : "Address filed should not be empty",
                            required : "Address field should not be empty",
                },
                "pincode" : {
                            required : "Pin code should not be empty",
                            rangelength : "Pin code should be in 6 digits",
                            digits : "Only digits allowed of 6 length",
                },
                  "pannumber" : {
                            //required : "Pan number field is mandatory",
                            pan : "Invalid Pan number",
                        },
                }
        });
    });
</script>

<script type="text/javascript">

    $(document).ready(function() {
       $('#mcatidd').multiselect({ 
            enableClickableOptGroups: true,
            enableFiltering: true,
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
        });
    });

    $("#mcatidd").change(function(){
        var catid=$('#mcatidd').val();
            
            $.ajax({
                url: '<?php echo base_url()?>admin/category/branddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                   
                      $("#catidd").empty();
                      $("#catidd").attr("multiple","multiple");
                      $("#catidd").multiselect('destroy');
                      $("#catidd").append(data);
                      $('#catidd').multiselect({ 
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        enableCaseInsensitiveFiltering: true,
                      });
                }
            });
    });


    $("#catidd").change(function(){
        var catid=$('#catidd').val();
             
            $.ajax({
                url: '<?php echo base_url()?>admin/category/subbranddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                    
                      $("#subcatidd").empty();
                      $("#subcatidd").attr("multiple","multiple");
                      $("#subcatidd").multiselect('destroy');
                      $("#subcatidd").append(data);
                      $('#subcatidd').multiselect({ 
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        enableCaseInsensitiveFiltering: true,
                      });
                }
            });
    });
    
        function childcategory(){
            var catid=$('#mcatidd').val();
            var get_subcatid = '<?php echo $getvendor_subcategory; ?>';  
            $.ajax({
                url: '<?php echo base_url()?>admin/category/getbranddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid,'get_subcatid': get_subcatid },
                success: function(data){
               
                      $("#catidd").empty();
                      $("#catidd").attr("multiple","multiple");
                      $("#catidd").multiselect('destroy');
                      $("#catidd").append(data);
                      $('#catidd').multiselect({ 
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        enableCaseInsensitiveFiltering: true,
                      });
                      subchildcategory();
                }
            });
            
           }

           childcategory();

           function subchildcategory(){
                var catid=$('#catidd').val();
                var get_subcatid = '<?php echo $vieww->catid; ?>';
                
                $.ajax({
                    url: '<?php echo base_url()?>admin/category/getsubbranddcatvalue',
                    type: 'POST',
                    //dataType: 'json',
                    data: {'catid': catid,'get_subcatid': get_subcatid},
                    success: function(data){
                        //alert(data);
                        //console.log(data);return false;
                          $("#subcatidd").empty();
                          $("#subcatidd").attr("multiple","multiple");
                          $("#subcatidd").multiselect('destroy');
                          $("#subcatidd").append(data);
                          $('#subcatidd').multiselect({ 
                            enableClickableOptGroups: true,
                            enableFiltering: true,
                            includeSelectAllOption: true,
                            enableCaseInsensitiveFiltering: true,
                          });
                    }
                });
           }
   
    </script>  