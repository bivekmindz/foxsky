<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Vendor</h2>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
        <div class="col-lg-12">
                    <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/vendor/viewvendor"><button>CANCEL</button></a>
             
           </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div></div></div>
            <form action="" method="post" id="add_vendor_form" enctype="multipart/form-data" >
                        
                        <div class="sep_box">
                            
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">First Name <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="fname" type="text"  name="fname"/></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Last Name <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="lasname" type="text" name="lasname"/></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="sep_box">

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <span id="validcheck" style='font-size:11px; color:red;'></span>
                                        <div class="tbl_text">Email Id <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="emailid" onblur="checkemail();" name="emailid" type="text" /></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Password <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="password" type="password" name="password" /></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="sep_box">

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <span id="validphn" style='font-size:11px; color:red;'></span>
                                        <div class="tbl_text">Mobile Number <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input type="text" onblur="checkphn();" name="mphn" id="mphn" maxlength="10" onkeypress="return isNumberKey(event);"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Shop/Company Name <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <input type="text" name="comname" id="comname">
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        
                        </div>

                        <div class="sep_box">
                        <div class="col-lg-6">
                          <div class="row">
                            <div class="col-lg-4">
                                <div class="tbl_text">Select Main Category <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                                <div class="tbl_input"><select name="mcatidd[]" id="mcatidd" multiple="multiple">
                                <?php foreach ($parentCatdata as $key => $value) {
                                 ?>
                                  <option value="<?php echo $value->catid ?>"><?php echo $value->catname ?></option>
                                 <?php }?>
                                </select></div>
                            </div>
                        </div>
                        </div>
                        </div>
                       
                        <div class="sep_box" id="childcat">
                        <div class="col-lg-6">
                          <div class="row">
                            <div class="col-lg-4">
                                <div class="tbl_text">Select Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                                <div class="tbl_input">
                                <select name="catidd[]" id="catidd">
                                <option value="">Select Sub-Category</option>
                                </select>
                                 </div>
                            </div>
                        </div>
                        </div>
                        </div>
                        
                        <div class="sep_box" id="subchildcat">
                        <div class="col-lg-6">
                          <div class="row">
                            <div class="col-lg-4">
                                <div class="tbl_text">Select Child Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                                <div class="tbl_input">
                                <select name="subcatidd[]" id="subcatidd">
                                <option value="">Select Child Sub-Category</option>
                                </select>
                                 </div>
                            </div>
                        </div>
                        </div>
                       
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Country <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <select name="countryid" id="countryid" onchange="selectstates();">
                                            <option value="none">Select Country</option>
                                            <?php foreach ($viewcountry as $key => $value) {
                                                
                                            ?>
                                             <option value="<?php echo $value->conid ?>"><?php echo $value->name ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">State <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <select name="stateid" id="stateid" onchange="statecountry();">
                                            <option value="none">Select State</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">City <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                   <select name="cityid" id="cityid">
                                            <option value="none">Select City</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Address <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <textarea name="vendoraddress" id="vendoraddress"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Pincode <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input" >
                                         <input id="pincode" type="text" name="pincode"/ maxlength="6"  onkeypress="return isNumberKey(event);">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Pan Number</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <input id="pannumber" type="text" name="pannumber" maxlength="10"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                        <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Account Holder Name</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                       <input id="accountholder" type="text" name="accountholder"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                           <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Account Number</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <input id="accountnumber" type="text" name="accountnumber"/>
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                         <div class="sep_box">
                          <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Bank Name</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <input id="banknumber" type="text" name="banknumber"/>
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Branch</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                    <input id="branchname" type="text" name="branchname"/>
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                          <div class="sep_box">
                          <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">IFSC Code</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                       <input type="text" id="IfscCode1"  name="IfscCode1" >
-
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">NEFT Detail</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                   <input id="neftdetail" type="text" name="neftdetail"/>
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                       
                        <div class="sep_box">
                        <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Account Type</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <select name="AccountType" id="AccountType" >
                                        <option value="">Select Type</option>
                                        <option value="C">Current</option>
                                        <option value="S">Saving</option>
                                        </select>
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">GST Number</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                   <input type="text"  name="gstno" id="gstno" class="field_de" />
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            
                        <div class="sep_box" id="acountsection" >
                            
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Upload GST Copy</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <input type="file" name="memgstfile" class="form-control" id="memgstfile" value="">
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Upload Cancelled Cheque Copy</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                       <input type="file" name="memchequefile" class="form-control" id="memchequefile" value="">
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box" id="acountsection" >
                        <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Upload Pan Card Copy</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                       <input type="file" name="mempanfile" class="form-control" id="mempanfile" value="">
                                      
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

  <!--  ---------------------------------------------- Child category  -------------------------------->




  <!--  ---------------------------------------------- child category  -------------------------------->



                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                </form>
                    </div>
                   
                </div>
            </div>
            <!---------------------- view category ---------------------------------------->
          
                   
<!---------------------- view category ---------------------------------------->

        </div>
 </div>
    </div> 
    
<script type="text/javascript">
function ValidateAlpha12(evt)
{
    var keyCode = (evt.which) ? evt.which : evt.keyCode
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32 && keyCode != 8)
    return false;
    return true;
  
}

function isNumberKey12(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode;
if (charCode != 48 ) 
return false;

return true;
}

function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode;
if (charCode != 46 && charCode > 31 
&& (charCode < 48 || charCode > 57))
return false;

return true;
}
    
function selectstates(){
var countryid=$('#countryid').val();
$('#staterid').show();
$.ajax({
 url: '<?php echo base_url()?>admin/geographic/countrystate',
type: 'POST',
data: {'countryid': countryid},
success: function(data){
var  option_brand = '<option value="none">Select State</option>';
$('#stateid').empty();
$("#stateid").append(option_brand+data);
 }
});
}

function statecountry(){
var  stid=$('#stateid').val();
$.ajax({
 url: '<?php echo base_url()?>admin/vendor/citystate',
type: 'POST',
data: {'stid': stid},
success: function(data){
var  option_brand = '<option value="none">Select City</option>';
$('#cityid').empty();
$("#cityid").append(option_brand+data);
 }
});
}


function checkemail(){
var vendore_email=$('#emailid').val();
var ven_typeid='manufacturer';

$.ajax({
 url: '<?php echo base_url(); ?>admin/vendor/emilcheck',
 type: 'POST',
 //dataType:'json',
 data: {'email':vendore_email,'ven_typeid':ven_typeid},
  success:function(data){
  // alert(data);
  if(data=='true'){
$("#validcheck").html("Email Already Exists");
  $("#emailid").val('');
  return false; 
      }else{
     
}
     
 }

 });
}


function checkphn(){
var mphn=$('#mphn').val();
var ven_typeid='manufacturer';

$.ajax({
 url: '<?php echo base_url(); ?>admin/vendor/phncheck',
 type: 'POST',
 //dataType:'json',
 data: {'mphn':mphn,'ven_typeid':ven_typeid},
  success:function(data){
   
  if(data=='false'){
  }else{
  $("#validphn").html("Mobile No. Already Exists");
  $("#mphn").val('');
  return false;    
}
     
 }

 });
}
</script>
<script src="<?php echo base_url().'assets/js/jquery.validate.min.js'; ?>"></script>
<style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>
 <script>
// When the document is ready
    $(document).ready(function () {

         $.validator.addMethod("valueNotEquals", function(value, element, arg){
            return arg != value;
         }, "Value must not equal arg.");

         $.validator.addMethod("pan", function(value, element)
         {
            return this.optional(element) || /^[A-Z]{5}\d{4}[A-Z]{1}$/.test(value);
         }, "Invalid Pan Number");

       // onkey up converts lower case to uppercase
        $('#pannumber,#vendoraddress').keyup(function(){
            this.value = this.value.toUpperCase();
        });
       //----------------------
        $.validator.addMethod("chk_add_field", function(value) {
            
            return value != "";
        }, "Please enter Address");
        
        /*$.validator.addMethod("check_cat", function (value, element) {
           
            var count = $(element).find('option:selected').length;
            return count > 0;
        });*/
     //validation rules
        $("#add_vendor_form").validate({
            ignore: [],
            rules: {
                        "lasname" : {
                            required : true,
                        },
                        "emailid" : {
                            required: true,
                            email: true
                        },
                        "password" : {
                            required : true,
                            minlength : 6,
                        },
                        "comname" : {
                            required : true,
                        },
                        "mphn" : {
                            required : true,
                            rangelength: [10, 10]
                        },
                          "fname" : {
                            required : true,
                        },
                        "mcatidd[]" : {
                            required : true,
                        },
                        "catidd[]" : {
                            required : true,
                        },
                        "subcatidd[]" : {
                            required : true,
                        },
                        "countryid" : {
                            valueNotEquals: "none",
                        },
                         "stateid" : {
                            valueNotEquals: "none",
                        },
                         "cityid" : {
                            valueNotEquals: "none",
                        },
                        "vendoraddress" : {
                            chk_add_field :'',
                            required: true,
                        },
                        "pincode" : {
                            required : true,
                            rangelength: [6, 6],
                            digits : true,
                        },
                        "pannumber" : {
                            //required : true,
                            pan : $('#pannumber').val(),
                        },
                        
                        
                        
              },
            messages: {
                "lasname": {
                    required: "Last name should not be empty",
                },
                "emailid" : {
                            required: "Email id is required",
                            email: "Invalid Email id",
                },
                "password" : {
                            required : "Password filed should not be empty",
                            minlength : "Must have greater than 6 digits",
                },
                "comname" : {
                            required : "Company name is mandatory",
                },
                "mphn" : {
                            required : "Mobile number is mandatory",
                            rangelength : "Mobile number should be exact 10 digits",
                },
                "fname" : {
                            required : "First name should not be empty",
                },
                "mcatidd[]" : {
                            required : "Please select at least one Category",
                },
                "catidd[]" : {
                            required : "Please select at least one Sub-Category",
                },
                "subcatidd[]": {
                            required : "Please select at least one Child Sub-Category",
                }, 
                "countryid" : {
                            valueNotEquals: "Please select any country",
                },
                "stateid" : {
                            valueNotEquals: "Please select any state",
                }, 
                "cityid" : {
                            valueNotEquals: "Please select any city",
                },
                "vendoraddress" : {
                            chk_add_field : "Address filed should not be empty",
                            required : "Address field should not be empty",
                },
                "pincode" : {
                            required : "Pin code should not be empty",
                            rangelength : "Pin code should be in 6 digits",
                            digits : "Only digits allowed of 6 length",
                },
                  "pannumber" : {
                            //required : "Pan number field is mandatory",
                            pan : "Invalid Pan number",
                        },
                }
        });
    });
</script>
<script type="text/javascript">

    $(document).ready(function() {
       $('#mcatidd').multiselect({ 
            enableClickableOptGroups: true,
            enableFiltering: true,
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
        });
    });

    $("#mcatidd").change(function(){
        var catid=$('#mcatidd').val();
              
            $.ajax({
                url: '<?php echo base_url()?>admin/category/branddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                   
                      $("#catidd").empty();
                      $("#catidd").attr("multiple","multiple");
                      $("#catidd").multiselect('destroy');
                      $("#catidd").append(data);
                      $('#catidd').multiselect({ 
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        enableCaseInsensitiveFiltering: true,
                      });
                }
            });
    });


    $("#catidd").change(function(){
        var catid=$('#catidd').val();
              
            $.ajax({
                url: '<?php echo base_url()?>admin/category/subbranddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                    
                      $("#subcatidd").empty();
                      $("#subcatidd").attr("multiple","multiple");
                      $("#subcatidd").multiselect('destroy');
                      $("#subcatidd").append(data);
                      $('#subcatidd').multiselect({ 
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        enableCaseInsensitiveFiltering: true,
                      });
                }
            });
    });
    
   
    </script>  