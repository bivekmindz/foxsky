 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            sec_name        : "required",
            sec_cat         : "required",
            //"sec_subcat[]"  : "required",
            "slideimg[]"    : "required",
            "banimg[]"     : "required",
        },
        // Specify the validation error messages
        messages: {
            sec_name        : "Section Name is required",
            sec_cat         : "Section Category is required",
            //"sec_subcat[]"  : "Section Sub-Category is required",
            "slideimg[]"    : "Center Banner Image is required",
            "banimg[]"      : "Banner Image is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>
<div class="wrapper">
    <?php $this->load->view('helper/nav')?> 

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <form id="addCont" action="" method="post" enctype="multipart/form-data" >
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Add Home Section Banners</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this Section Admin can add Home Section Banners of home page.</p>
                            </div>
                        </div>
                                                <div class="page_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div style="text-align:right;">
                                        <a href="#"><button>CANCLE</button></a>
                                    </div>
                                    <div style="float: left;width: 100%;margin-left: 14px;"><h4>Create New Section</h4></div>
                                    <div class='flashmsg'>
                                        <?php
                                        if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message'); 
                                        }
                                        ?>
                                    </div>
                                </div></div>
                                <div class="sep_box">
                                    <div class="col-lg-4">
                                        <div class="row">
                                            <div class="col-lg-11">
                                                <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                                <div class="tbl_input">
                                                    <select name="sec_cat" id="sec_cat" onchange="selectsubcat();">
                                                        <option value="">Select Type</option>
                                                        <?php foreach ($parentcat as $key => $value) { ?> 

                                                        <option <?php if($value->catid==$section_cat->catid) echo 'selected'; ?> 
                                                        value="<?php echo $value->catid;?>"><?php echo $value->catname;?>
                                                            
                                                        </option>
                                                        <?php } ?>
                                                    </select></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-11">
                                                    <div class="tbl_text">Select Product Name<span style="color:red;font-weight: bold;">*</span></div>
                                                    <div class="tbl_input">
                                                        <select name="pro_cat" id="pro_cat" >
                                                            <option value="">Select Type</option>
                                                            <?php foreach ($ProductName as $key => $value) { ?>
                                                            <option <?php if($value->proid==$section_cat->Proid) echo 'selected'; ?> value="<?php echo $value->proid;?>">
                                                                <?php echo $value->proname;?>
                                                            </option>
                                                            <?php } ?>
                                                        </select></div>
                                                    </div>
                                                </div>
                                            </div>
                                             </div>
                                        <div style="float: left;width: 95%;margin-left: 14px; border-top:1px solid #E4E4E4;"><h4>Customize Overview</h4></div>
                                        
                                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                                            <div class="col-lg-4">
                                                <input type="hidden" name="count[]" value="1">
                                                <div class="row"> 
                                                    <div class="col-lg-12">
                                                       <div class="tbl_text">Image <span style="color:red;font-weight: bold;">*</span></div>
                                                       
                                                       <div class="tbl_input">
                                                        <input id="image1" name="image1" type="file" />
                                                        <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Img1; ?>"  height="75" width="50"
                                                        >
                                                        <input type="hidden" name="oldimage1" value="<?php echo $section_cat->Img1; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            <!-- <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text">Overview Url </div>
                                        <div class="tbl_input"><input type="text" name="slideurl_1" id="slideurl_1" /></div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">Heading </div>
                                        <div class="tbl_input"><input id="head1" type="text" name="head1" value="<?php echo $section_cat->Head1; ?>" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">Overview Sub-Heading </div>
                                        <div class="tbl_input"><input id="subhead1" type="text"  name="subhead1" value="<?php echo $section_cat->Subhead1; ?>" /></div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                            <div class="col-lg-6">
                                <div class=row>
                                    <div class="col-lg-12">
                                        <div class="videos_adds">
                                            <div class="tbl_text">First Video <span style="color:red;font-weight: bold;">*</span></div>         
                                            
                                            <div class="tbl_input">
                                            <input id="vedio1" name="vedio1" type="file" <?php echo $section_cat->Vedio1; ?> />
                                            <input type="text" name="oldvedio1" value="<?php echo $section_cat->Vedio1; ?>" readonly></div>        
                                            
                                        </div>      
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class=row>
                                    <div class="col-lg-12">
                                        <div class="videos_adds">
                                            <div class="tbl_text">Second Video <span style="color:red;font-weight: bold;">*</span></div>         
                                            
                                            <div class="tbl_input"><input id="vedio2" name="vedio2" type="file" />
                                            <input type="text" name="oldvedio2" value="<?php echo $section_cat->Vedio2; ?>" readonly></div>        
                                            
                                        </div>           
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                           <div class="col-lg-4">
                            <input type="hidden" name="count[]" value="1">
                            <div class="row"> 
                                <div class="col-lg-12">
                                   <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                                   <div class="tbl_input"><input id="image2" name="image2" type="file" />
                                    <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image2; ?>"  height="75" width="50"
                                    >
                                    <input type="hidden" name="oldimage2" value="<?php echo $section_cat->Image2; ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row"> 
                                <div class="col-lg-12">
                                    <div class="tbl_text"> Image Heading </div>
                                    <div class="tbl_input"><input type="text" name="head2" id="head2" value="<?php echo $section_cat->Head2; ?>" /></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row"> 
                                <div class="col-lg-12">
                                    <div class="tbl_text"> Image Sub-heading</div>
                                    <div class="tbl_input"><input type="text" id="subhead2" type="text" name="subhead2" value="<?php echo $section_cat->Subhead2; ?>" /></div>
                                </div>
                            </div>
                        </div>
                        </div>
                    <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                       <div class="col-lg-4">
                        <input type="hidden" name="count[]" value="1">
                        <div class="row"> 
                            <div class="col-lg-12">
                               <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                               <div class="tbl_input"><input id="image3" name="image3" type="file" />
                                <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image3; ?>"  height="75" width="50"
                                >
                                <input type="hidden" name="oldimage3" value="<?php echo $section_cat->Image3; ?>"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="row"> 
                            <div class="col-lg-12">
                                <div class="tbl_text"> Image Heading </div>
                                <div class="tbl_input"><input type="text" name="head3" id="head3" value="<?php echo $section_cat->Head3; ?>" /></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="row"> 
                            <div class="col-lg-12">
                                <div class="tbl_text"> Image Sub-heading</div>
                                <div class="tbl_input"><input type="text" id="subhead3" type="text" name="subhead3" value="<?php echo $section_cat->Subhead3; ?>"/></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                   <div class="col-lg-4">
                    <input type="hidden" name="count[]" value="1">
                    <div class="row"> 
                        <div class="col-lg-12">
                           <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                           <div class="tbl_input"><input id="image4" name="image4" type="file" /><img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image4; ?>"  height="75" width="50"
                            >
                            <input type="hidden" name="oldimage4" value="<?php echo $section_cat->Image4; ?>"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row"> 
                        <div class="col-lg-12">
                            <div class="tbl_text"> Image Heading </div>
                            <div class="tbl_input"><input type="text" name="head4" id="head4" value="<?php echo $section_cat->Head4; ?>"/></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row"> 
                        <div class="col-lg-12">
                            <div class="tbl_text"> Image Sub-heading</div>
                            <div class="tbl_input"><input type="text" id="subhead4" type="text" name="subhead4" value="<?php echo $section_cat->Subhead4; ?>" /></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sep_box">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                          <div class="tbl_text">Image1</div>
                      </div>
                      <div class="col-lg-8">
                        <div class="tbl_input"><span><input type="file" name="image5"  id="image5">
                            <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image5; ?>"  height="75" width="50"
                            >
                            <input type="hidden" name="oldimage5" value="<?php echo $section_cat->Image5; ?>"></span></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                      <div class="col-lg-4"><div class="tbl_text">Image2</div></div>
                      <div class="col-lg-8">
                        <div class="tbl_input"><span><input type="file" name="image6"  id="image6">
                            <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image6; ?>"  height="75" width="50"
                            >
                            <input type="hidden" name="oldimage6" value="<?php echo $section_cat->Image6; ?>"></span></div></div>
                        </div>
                    </div>
                </div>
                <div class="sep_box">
                  <div class="col-lg-6">
                    <div class="row">
                      <div class="col-lg-4"><div class="tbl_text">Image3</div></div>
                      <div class="col-lg-8">
                        <div class="tbl_input">
                          <span><input type="file" name="image7"  id="image7"  >
                              <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image7; ?>"  height="75" width="50"
                              >
                              <input type="hidden" name="oldimage7" value="<?php echo $section_cat->Image7; ?>"></span></div>
                          </div></div>
                      </div> 
                      <div class="col-lg-6">
                        <div class="row">
                          <div class="col-lg-4">
                              <div class="tbl_text">Image4</div>
                          </div>
                          <div class="col-lg-8">
                            <div class="tbl_input"><span><input type="file" name="image8"  id="image8">
                                <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image8; ?>"  height="75" width="50"
                                >
                                <input type="hidden" name="oldimage8" value="<?php echo $section_cat->Image8; ?>"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                   <div class="col-lg-4">
                    <input type="hidden" name="count[]" value="1">
                    <div class="row"> 
                        <div class="col-lg-12">
                           <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                           <div class="tbl_input"><input id="image9" name="image9" type="file" />
                            <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image9; ?>"  height="75" width="50"
                            >
                            <input type="hidden" name="oldimage9" value="<?php echo $section_cat->Image9; ?>"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row"> 
                        <div class="col-lg-12">
                            <div class="tbl_text"> Image Heading </div>
                            <div class="tbl_input"><input type="text" name="head9" id="head9" value="<?php echo $section_cat->Head9; ?>"/></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row"> 
                        <div class="col-lg-12">
                            <div class="tbl_text"> Image Sub-heading</div>
                            <div class="tbl_input"><input type="text" id="subhead9" type="text" name="subhead9" value="<?php echo $section_cat->Subhead9; ?>"/></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
               <div class="col-lg-4">
                <input type="hidden" name="count[]" value="1">
                <div class="row"> 
                    <div class="col-lg-12">
                       <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                       <div class="tbl_input"><input id="image10" name="image10" type="file" />
                        <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image10; ?>"  height="75" width="50"
                        >
                        <input type="hidden" name="oldimage10" value="<?php echo $section_cat->Image10; ?>"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row"> 
                    <div class="col-lg-12">
                        <div class="tbl_text"> Image Heading </div>
                        <div class="tbl_input"><input type="text" name="head10" id="head10" value="<?php echo $section_cat->Head10; ?>" /></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row"> 
                    <div class="col-lg-12">
                        <div class="tbl_text"> Image Sub-heading</div>
                        <div class="tbl_input"><input type="text" id="subhead10" type="text" name="subhead10" value="<?php echo $section_cat->Subhead10; ?>"/></div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="sep_box">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-4">
                      <div class="tbl_text">Image1</div>
                  </div>
                  <div class="col-lg-8">
                    <div class="tbl_input"><span><input type="file" name="image11"  id="image11">
                        <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image11; ?>"  height="75" width="50"
                        >
                        <input type="hidden" name="oldimage11" value="<?php echo $section_cat->Image11; ?>">
                        <input type="hidden" name="oldimage11" value="<?php echo $section_cat->Image11; ?>"></span></div>
                    </div>
                    <div class="col-lg-4">
                      <div class="tbl_text">Image1 heading</div>
                  </div>
                  <div class="col-lg-8">
                    <div class="tbl_input"><span><input type="text" name="head11"  id="head11" value="<?php echo $section_cat->Head11; ?>"></span></div>
                </div>

            </div>
        </div>
        <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-4"><div class="tbl_text">Image2</div></div>
              <div class="col-lg-8">
                <div class="tbl_input"><span><input type="file" name="image12"  id="image12">
                    <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image12; ?>"  height="75" width="50"
                    >
                    <input type="hidden" name="oldimage12" value="<?php echo $section_cat->Image12; ?>">
                    <input type="hidden" name="oldimage12" value="<?php echo $section_cat->Image12; ?>"></span></div></div>
                </div>
                <div class="col-lg-4">
                  <div class="tbl_text">Image2 heading</div>
              </div>
              <div class="col-lg-8">
                <div class="tbl_input"><span><input type="text" name="head12"  id="head12"
                    value="<?php echo $section_cat->Head12; ?>">
                </span></div>
            </div>
        </div>
    </div>

    <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4"><div class="tbl_text">Image3</div></div>
                <div class="col-lg-8">

                <div class="tbl_input"><span><input type="file" name="image13"  id="image13">
                    <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image13; ?>"  height="75" width="50">
                    <input type="hidden" name="oldimage13" value="<?php echo $section_cat->Image13; ?>">
                    <input type="hidden" name="oldimage13" value="<?php echo $section_cat->Image13; ?>"></span></div>
                    </div>
                    <div class="col-lg-4">
                      <div class="tbl_text">Image3 heading</div>
                  </div>
                  <div class="col-lg-8">
                    <div class="tbl_input"><span><input type="text" name="head13"  id="head13" value="<?php echo $section_cat->Head13; ?>"></span></div>
                </div>
                
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-4"><div class="tbl_text">Image4</div></div>
              <div class="col-lg-8">
                <div class="tbl_input"><span><input type="file" name="image14"  id="image14">
                    <img src="<?php echo base_url(); ?>assets/overviewimages/<?php echo $section_cat->Image14; ?>"  height="75" width="50"
                    >
                    <input type="hidden" name="oldimage14" value="<?php echo $section_cat->Image14; ?>">
                    <input type="hidden" name="oldimage14" value="<?php echo $section_cat->Image14; ?>"></span></div>
                    </div>
                </div>
                <div class="col-lg-4">
                  <div class="tbl_text">Image4 heading</div>
              </div>
              <div class="col-lg-8">
                <div class="tbl_input"><span><input type="text" name="head14"  id="head14" value="<?php echo $section_cat->Head14; ?>"></span></div>
            </div>
        </div>
        <div class="col-lg-6">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Main heading</div>
                                        <div class="tbl_input"><input type="text" name="mainhead" id="mainhead" value="<?php echo $section_cat->Mainhead; ?>" /></div>
                                    </div>
                                </div>
                            </div>
                             <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                         <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text"> Heading1 </div>
                                        <div class="tbl_input"><input type="text" name="head15" id="head15" value="<?php echo $section_cat->Head15; ?>" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> Heading2 </div>
                                        <div class="tbl_input"><input type="text" name="head16" id="head16" value="<?php echo $section_cat->Head16; ?>"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Heading3</div>
                                        <div class="tbl_input"><input type="text" name="head17" id="head17" value="<?php echo $section_cat->Head17; ?>"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text"> Heading4 </div>
                                        <div class="tbl_input"><input type="text" name="head18" id="head18" value="<?php echo $section_cat->Head18; ?>"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> Heading5 </div>
                                        <div class="tbl_input"><input type="text" name="head19" id="head19" value="<?php echo $section_cat->Head19; ?>" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Heading6 </div>
                                        <div class="tbl_input"><input type="text" name="head20" id="head20" value="<?php echo $section_cat->Head20; ?>"/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>

    <?php 
    $i=0; 
    
    foreach($gettopslide as $key => $value){

    $i++;
    ?>
     <div class="col-lg-3">
                            <!-- <input type="hidden" name="countoverview[]" value="1"> -->
                            <div class="row"> 
                                <div class="col-lg-12">
                                   <div class="tbl_text">Overview Image <span style="color:red;font-weight: bold;">*</span></div>
                                   <div class="tbl_input"><img height="50px" width="50px" src="<?php echo base_url()?>assets/overviewimages/<?php echo $value->imgname ;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-lg-4">
                            <div class="row"> 
                                <div class="col-lg-12">
                                    <div class="tbl_text">Overview Heading </div>
                                    <div class="tbl_input"><label> <?php echo $value->imghead; ?></label></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row"> 
                                <div class="col-lg-12">
                                    <div class="tbl_text">Overview Sub-Heading </div>
                                    <div class="tbl_input"><label> <?php echo $value->imgsubhead; ?></label></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <div class="row"> 
                                <div class="col-lg-12">
                                    <div class="tbl_input"><a href="<?php echo base_url()?>admin/product/deleteoverview/<?php echo $value->did; ?>/<?php echo $value->sid; ?>" class="btn_button sub_btn" />Delete</a></div>
                                </div>
                            </div>
                        </div>
    <?php }
    ?> 


    <div class="sep_box append2" style="width: 98%;"">
        <input type="hidden" name="countoverview[]" value="0">
                                
    </div>

                        <div class="sep_box" style="border-bottom: 1px solid #ddd;">
                            <div class="col-lg-6">
                                <div class="row">
                                    <button type="button" class="addMore"><i class="glyphicon glyphicon-plus"></i> Add More</button>
                                </div>
                            </div>
                        </div>

    <?php 
    $i=0; 
    
    foreach($getbottomslide as $key => $value){

    $i++;
    ?>

    <div class="col-lg-3">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                       <div class="tbl_text">botom Overview Image <span style="color:red;font-weight: bold;">*</span></div>
                                       <div class="tbl_input">
                                            <img height="50px" width="50px" src="<?php echo base_url()?>assets/overviewimages/<?php echo $value->bottomImagename ;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">botom OverviewHeading </div>
                                        <div class="tbl_input"><label><?php echo $value->bottomImagehead; ?></label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">botom Overview Sub-Heading </div>
                                        <div class="tbl_input"><label><?php echo $value->bottomImagesubhead; ?></label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            <div class="row"> 
                                <div class="col-lg-12">
                                    <div class="tbl_input"><a href="<?php echo base_url()?>admin/product/deletebottomoverview/<?php echo $value->did; ?>/<?php echo $value->sid; ?>" class="btn_button sub_btn" />Delete</a></div>
                                </div>
                            </div>
                        </div>
    <?php }
    ?> 

    <div class="sep_box append3" style="width: 98%; border-bottom: 1px solid #ddd;">
       <input type="hidden" name="countoverviewbottom[]" value="0">     
       </div>           
                            <!-- <div class="col-lg-4">
                                <input type="hidden" name="countoverviewbottom[]" value="1">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                       <div class="tbl_text">botom Overview Image <span style="color:red;font-weight: bold;">*</span></div>
                                       <div class="tbl_input"><input id="bottomslideimg_1" name="bottomslideimg_1" type="file" />
                                        <div style="margin-top: 25px" height="50px" width="50px"> 
                                            <img height="50px" width="50px" src="<?php echo base_url()?>assets/overviewimages/<?php echo $section_cat->bottomImagename ;?>"
                                            <input type="hidden" name="oldbottomImagename" value="<?php echo $section_cat->bottomImagename; ?>"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">botom OverviewHeading </div>
                                        <div class="tbl_input"><input id="bottomslidehead_1" type="text" name="bottomslidehead_1" value="<?php echo $section_cat->bottomImagehead; ?>"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">botom Overview Sub-Heading </div>
                                        <div class="tbl_input"><input id="bottomslidesubhead_1" type="text"  name="bottomslidesubhead_1" value="<?php echo $section_cat->bottomImagesubhead; ?>"/></div>
                                    </div>
                                </div>
                            </div> -->
                        <!-- </div> -->
                        
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <button type="button" class="addMore1"><i class="glyphicon glyphicon-plus"></i> Add More</button>
                                </div>
                            </div>
                            
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">

    $(document).ready(function(){
      var maxField = 100; 
      var z = 0; 
      var wrapper1 = $('.append2'); 
      $(".addMore").click(function(){ 


        if(z < maxField){
            z++;
            $(".append2").append(' <div class="sep_box"><input type="hidden" name="countoverview[]" value="'+z+'"><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">Overview Image <span style="color:red;font-weight: bold;">*</span></div><div class="tbl_input"><input id="slideimg_'+z+'" name="slideimg_'+z+'" type="file" /></div></div></div></div><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">OverviewHeading </div><div class="tbl_input"><input id="slidehead_'+z+'" type="text" name="slidehead_'+z+'" /></div></div></div></div><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">Overview Sub-Heading </div><div class="tbl_input"><input id="slidesubhead_'+z+'" type="text"  name="slidesubhead_'+z+'"/></div> </div></div> </div><div class="col-lg-3"><button type="button" class="remove_row"><i class="glyphicon glyphicon-minus"></i></button></div></div>');
        }
    });
      $(wrapper1).on('click', '.remove_row', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          z--;
      });
  });


    $(document).ready(function(){
      var maxField = 100; 
      var y = 0; 
      var wrapper2 = $('.append3'); 
      $(".addMore1").click(function(){ 


        if(y < maxField){
            y++;
            $(".append3").append(' <div class="sep_box"><input type="hidden" name="countoverviewbottom[]" value="'+y+'"><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">botom Overview Image <span style="color:red;font-weight: bold;">*</span></div><div class="tbl_input"><input id="bottomslideimg_'+y+'" name="bottomslideimg_'+y+'" type="file" /></div></div></div></div><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">botom OverviewHeading </div><div class="tbl_input"><input id="bottomslidehead_'+y+'" type="text" name="bottomslidehead_'+y+'" /></div></div></div></div><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">botom Overview Sub-Heading </div><div class="tbl_input"><input id="bottomslidesubhead_'+y+'" type="text"  name="bottomslidesubhead_'+y+'"/></div></div></div></div></div>  </div><div class="col-lg-3"><button type="button" class="remove_row1"><i class="glyphicon glyphicon-minus"></i></button></div></div>');
        }
    });
      $(wrapper2).on('click', '.remove_row1', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          y--;
      });
  });



    function selectsubcat(){
        var ids= $('#sec_cat').val();
     alert(ids);
     $.ajax({
        url: '<?php echo base_url(); ?>admin/overview/productName',
        type:'POST',
        data:{'ids':ids},
        success: function(proData) {
                // alert(proData);
                $('#pro_cat').html(proData);
            }

        });
 }




</script>    