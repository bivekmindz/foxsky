 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            sec_name        : "required",
            sec_cat         : "required",
            //"sec_subcat[]"  : "required",
            "slideimg[]"    : "required",
            "banimg[]"     : "required",
        },
        // Specify the validation error messages
        messages: {
            sec_name        : "Section Name is required",
            sec_cat         : "Section Category is required",
            //"sec_subcat[]"  : "Section Sub-Category is required",
            "slideimg[]"    : "Center Banner Image is required",
            "banimg[]"      : "Banner Image is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
           
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Overview</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Admin can add Overview Section of home page.</p>
                        </div>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
        <div class="col-lg-12">
                    <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/overview/index"><button>CANCLE</button></a>
           </div>
           <div style="float: left;width: 100%;margin-left: 14px;"><h4>Create New Section</h4></div>
        <!-- <div class='flashmsg'>
            <?php
               if($this->session->flashdata('message')){
               echo $this->session->flashdata('message'); 
              }
            ?> 
        </div>  -->
        </div></div>
         <form id="addCont" action="" method="post" enctype="multipart/form-data" >
                        <div class="sep_box">
                            <!-- <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Section Name <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="sec_name" name="sec_name" type="text" /></div>
                                    </div>
                                </div>
                            </div>      -->   
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="sec_cat" id="sec_cat" onchange="selectsubcat();">
                                                <option value="">Select Type</option>
                                                <?php foreach ($parentcat as $key => $value) { ?>
                                                <option value="<?php echo $value->catid;?>"><?php echo $value->catname;?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Product Name<span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="pro_cat" id="pro_cat" >
                                                <option value="">Select Type</option>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <div style="float: left;width: 95%;margin-left: 14px; border-top:1px solid #E4E4E4;"><h4>Customize Overview</h4></div>
                        
                        <!-- .................. NEW SLIDER 1 .................. -->



                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                            <div class="col-lg-4">
                            <input type="hidden" name="count[]" value="1">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text">Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="image1" name="image1" type="file" /><span>Image size should be 2560x1400</span>
                                         </div>

                                    </div>
                                     <span class='flashmsg'>
                                         <?php
                                          if($this->session->flashdata('message1')){
                                            echo $this->session->flashdata('message1'); 
                                          }
                                        ?></span>
                                </div>
                            </div>
                            
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">Heading </div>
                                        <div class="tbl_input"><input id="head1" type="text" name="head1" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">Overview Sub-Heading </div>
                                        <div class="tbl_input"><input id="subhead1" type="text"  name="subhead1"/></div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                        <div class="col-lg-6">
                        <div class=row>
                        <div class="col-lg-12">
                        <div class="videos_adds">
                            <div class="tbl_text">First Video <span style="color:red;font-weight: bold;">*</span></div>         
                            
                            <div class="tbl_input"><input id="vedio1" name="vedio1" type="file" />
                            <span>File size should be 1260x600</span></div>        
                                        
                        </div>      
                        </div>
                        </div>
                        </div>
                        <div class="col-lg-6">
                        <div class=row>
                        <div class="col-lg-12">
                        <div class="videos_adds">
                            <div class="tbl_text">Second Video <span style="color:red;font-weight: bold;">*</span></div>         
                            
                            <div class="tbl_input"><input id="vedio2" name="vedio2" type="file" />
                            <span>File size should be 1260x600</span></div>        
                                        
                        </div>           
                        </div>
                        </div>
                        </div>
                        </div>
                    
                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                         <div class="col-lg-4">
                            <input type="hidden" name="count[]" value="1">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="image2" name="image2" type="file" /><span>Image size should be 1502x964</span></div>
                                    </div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message2')){
                                            echo $this->session->flashdata('message2'); 
                                          }
                                        ?>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> Image Heading </div>
                                        <div class="tbl_input"><input type="text" name="head2" id="head2" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Image Sub-heading</div>
                                        <div class="tbl_input"><input type="text" id="subhead2" type="text" name="subhead2" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                         <div class="col-lg-4">
                            <input type="hidden" name="count[]" value="1">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="image3" name="image3" type="file" /><span>Image size should be 2560x1300</span></div>
                                        <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message3')){
                                            echo $this->session->flashdata('message3'); 
                                          }
                                        ?>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> Image Heading </div>
                                        <div class="tbl_input"><input type="text" name="head3" id="head3" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Image Sub-heading</div>
                                        <div class="tbl_input"><input type="text" id="subhead3" type="text" name="subhead3" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                         <div class="col-lg-4">
                            <input type="hidden" name="count[]" value="1">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="image4" name="image4" type="file" /><span>Image size should be 2560x1200</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message4')){
                                            echo $this->session->flashdata('message4'); 
                                          }
                                        ?>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> Image Heading </div>
                                        <div class="tbl_input"><input type="text" name="head4" id="head4" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Image Sub-heading</div>
                                        <div class="tbl_input"><input type="text" id="subhead4" type="text" name="subhead4" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                       <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Image1</div>
                                     </div>
                                      <div class="col-lg-8">
                                        <div class="tbl_input"><span><input type="file" name="image5"  id="image5"></span><span>Image size should be 700x420</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message5')){
                                            echo $this->session->flashdata('message5'); 
                                          }
                                        ?>
                                    </span>
                                      </div>
                                </div>
                            </div>
                                <div class="col-lg-6">
                                <div class="row">
                                  <div class="col-lg-4"><div class="tbl_text">Image2</div></div>
                                  <div class="col-lg-8">
                                    <div class="tbl_input"><span><input type="file" name="image6"  id="image6"></span><span>Image size should be 700x420</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message6')){
                                            echo $this->session->flashdata('message6'); 
                                          }
                                        ?>
                                    </span></div>
                                    </div>
                                </div>
                        </div>
                            <div class="sep_box">
                          <div class="col-lg-6">
                                <div class="row">
                                  <div class="col-lg-4"><div class="tbl_text">Image3</div></div>
                                  <div class="col-lg-8">
                                    <div class="tbl_input">
                                      <span><input type="file" name="image7"  id="image7"  ></span><span>Image size should be 700x420</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message7')){
                                            echo $this->session->flashdata('message7'); 
                                          }
                                        ?>
                                    </span>
                                    </div></div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="row">
                                  <div class="col-lg-4">
                                  <div class="tbl_text">Image4</div>
                                  </div>
                                  <div class="col-lg-8">
                                    <div class="tbl_input"><span><input type="file" name="image8"  id="image8"></span><span>Image size should be 700x420</span></div>
                                  <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message8')){
                                            echo $this->session->flashdata('message8'); 
                                          }
                                        ?>
                                    </span></div>
                                </div>
                            </div>
                            </div> 
                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                         <div class="col-lg-4">
                            <input type="hidden" name="count[]" value="1">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="image9" name="image9" type="file" /><span>Image size should be 2560x1420</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message9')){
                                            echo $this->session->flashdata('message9'); 
                                          }
                                        ?>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> Image Heading </div>
                                        <div class="tbl_input"><input type="text" name="head9" id="head9" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Image Sub-heading</div>
                                        <div class="tbl_input"><input type="text" id="subhead9" type="text" name="subhead9" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                         <div class="col-lg-4">
                            <input type="hidden" name="count[]" value="1">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text"> Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="image10" name="image10" type="file" /><span>Image size should be 1440x500</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message10')){
                                            echo $this->session->flashdata('message10'); 
                                          }
                                        ?>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> Image Heading </div>
                                        <div class="tbl_input"><input type="text" name="head10" id="head10" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Image Sub-heading</div>
                                        <div class="tbl_input"><input type="text" id="subhead10" type="text" name="subhead10" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Image1</div>
                                     </div>
                                      <div class="col-lg-8">
                                        <div class="tbl_input"><span><input type="file" name="image11"  id="image11"></span><span>Image size should be 700x400</span></div>
                                        <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message11')){
                                            echo $this->session->flashdata('message11'); 
                                          }
                                        ?>
                                    </span>
                                      </div>
                                      <div class="col-lg-4">
                                      <div class="tbl_text">Image1 heading</div>
                                     </div>
                                      <div class="col-lg-8">
                                        <div class="tbl_input"><span><input type="text" name="head11"  id="head11"></span></div>
                                      </div>

                                </div>
                            </div>
                                <div class="col-lg-6">
                                <div class="row">
                                  <div class="col-lg-4"><div class="tbl_text">Image2</div></div>
                                  <div class="col-lg-8">
                                    <div class="tbl_input"><span><input type="file" name="image12"  id="image12"></span><span>Image size should be 700x400</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message12')){
                                            echo $this->session->flashdata('message12'); 
                                          }
                                        ?>
                                    </span>
                                    </div>
                                    </div>
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Image2 heading</div>
                                     </div>
                                      <div class="col-lg-8">
                                        <div class="tbl_input"><span><input type="text" name="head12"  id="head12"></span></div>
                                      </div>
                                </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Image3</div>
                                     </div>
                                      <div class="col-lg-8">
                                        <div class="tbl_input"><span><input type="file" name="image13"  id="image13"></span><span>Image size should be 700x650</span></div>
                                        <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message13')){
                                            echo $this->session->flashdata('message13'); 
                                          }
                                        ?>
                                    </span>
                                      </div>
                                      <div class="col-lg-4">
                                      <div class="tbl_text">Image3 heading</div>
                                     </div>
                                      <div class="col-lg-8">
                                        <div class="tbl_input"><span><input type="text" name="head13"  id="head13"></span></div>
                                      </div>
                                    
                                </div>
                            </div>
                                <div class="col-lg-6">
                                <div class="row">
                                  <div class="col-lg-4"><div class="tbl_text">Image4</div></div>
                                  <div class="col-lg-8">
                                    <div class="tbl_input"><span><input type="file" name="image14"  id="image14"></span><span>Image size should be 700x650</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('message14')){
                                            echo $this->session->flashdata('message14'); 
                                          }
                                        ?>
                                    </span></div>
                                    </div>
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Image4 heading</div>
                                     </div>
                                      <div class="col-lg-8">
                                        <div class="tbl_input"><span><input type="text" name="head14"  id="head14"></span></div>
                                      </div>
                                </div>
                                <div class="col-lg-6">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Main heading</div>
                                        <div class="tbl_input"><input type="text" id="mainhead" type="text" name="mainhead" /></div>
                                    </div>
                                </div>
                            </div>
                             <div class="sep_box append" style="width: 98%; border-bottom: 1px solid #ddd;">
                         <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text"> Heading1 </div>
                                        <div class="tbl_input"><input type="text" name="head15" id="head15" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> Heading2 </div>
                                        <div class="tbl_input"><input type="text" name="head16" id="head16" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Heading3</div>
                                        <div class="tbl_input"><input type="text" name="head17" id="head17" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text"> Heading4 </div>
                                        <div class="tbl_input"><input type="text" name="head18" id="head18" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> Heading5 </div>
                                        <div class="tbl_input"><input type="text" name="head19" id="head19" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text"> Heading6 </div>
                                        <div class="tbl_input"><input type="text" name="head20" id="head20" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="sep_box append2" style="width: 98%; border-bottom: 1px solid #ddd;">
                            <div class="col-lg-4">
                            <input type="hidden" name="countoverview[]" value="1">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text">Overview Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="slideimg_1" name="slideimg_1" type="file" /><span>Image size should be 2560x1200</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('messagetop')){
                                            echo $this->session->flashdata('messagetop'); 
                                          }
                                        ?>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        
                           
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">Overview Heading </div>
                                        <div class="tbl_input"><input id="slidehead_1" type="text" name="slidehead_1" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">Overview Sub-Heading </div>
                                        <div class="tbl_input"><input id="slidesubhead_1" type="text"  name="slidesubhead_1"/></div>
                                    </div>
                                </div>
                            </div>
                             
                            
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <button type="button" class="addMore"><i class="glyphicon glyphicon-plus"></i> Add More</button>
                                </div>
                            </div>
                        </div>
                        
                        <div class="sep_box append3" style="width: 98%; border-bottom: 1px solid #ddd;">
                        
                            <div class="col-lg-4">
                            <input type="hidden" name="countoverviewbottom[]" value="1">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                     <div class="tbl_text">botom Overview Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="bottomslideimg_1" name="bottomslideimg_1" type="file" /><span>Image size should be 2560x1200</span></div>
                                    <span class='flashmsg'>
                                        <?php
                                          if($this->session->flashdata('messagebottom')){
                                            echo $this->session->flashdata('messagebottom'); 
                                          }
                                        ?>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        
                           
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">botom OverviewHeading </div>
                                        <div class="tbl_input"><input id="bottomslidehead_1" type="text" name="bottomslidehead_1" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="tbl_text">botom Overview Sub-Heading </div>
                                        <div class="tbl_input"><input id="bottomslidesubhead_1" type="text"  name="bottomslidesubhead_1"/></div>
                                    </div>
                                </div>
                            </div>
                             </div>
                         <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <button type="button" class="addMore1"><i class="glyphicon glyphicon-plus"></i> Add More</button>
                                </div>
                            </div>
                        
                        </div>
                        
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div> 

<script type="text/javascript">

$(document).ready(function(){
      var maxField = 100; 
      var z = 1; 
      var wrapper1 = $('.append2'); 
      $(".addMore").click(function(){ 


        if(z < maxField){
            z++;
              $(".append2").append(' <div class="sep_box"><div class="col-lg-4"><input type="hidden" name="countoverview[]" value="'+z+'"><div class="row"><div class="col-lg-12"><div class="tbl_text">Overview Image <span style="color:red;font-weight: bold;">*</span></div><div class="tbl_input"><input id="slideimg_'+z+'" name="slideimg_'+z+'" type="file" /></div></div></div></div><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">OverviewHeading </div><div class="tbl_input"><input id="slidehead_'+z+'" type="text" name="slidehead_'+z+'" /></div></div></div></div><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">Overview Sub-Heading </div><div class="tbl_input"><input id="slidesubhead_'+z+'" type="text"  name="slidesubhead_'+z+'"/></div> </div></div> </div><div class="col-lg-3"><button type="button" class="remove_row"><i class="glyphicon glyphicon-minus"></i></button></div></div>');
        }
      });
      $(wrapper1).on('click', '.remove_row', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          z--;
      });
    });


$(document).ready(function(){
      var maxField = 100; 
      var y = 1; 
      var wrapper2 = $('.append3'); 
      $(".addMore1").click(function(){ 


        if(y < maxField){
            y++;
              $(".append3").append(' <div class="sep_box"><div class="col-lg-4"><input type="hidden" name="countoverviewbottom[]" value="'+y+'"><div class="row"><div class="col-lg-12"><div class="tbl_text">botom Overview Image <span style="color:red;font-weight: bold;">*</span></div><div class="tbl_input"><input id="bottomslideimg_'+y+'" name="bottomslideimg_'+y+'" type="file" /></div></div></div></div><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">botom OverviewHeading </div><div class="tbl_input"><input id="bottomslidehead_'+y+'" type="text" name="bottomslidehead_'+y+'" /></div></div></div></div><div class="col-lg-4"><div class="row"><div class="col-lg-12"><div class="tbl_text">botom Overview Sub-Heading </div><div class="tbl_input"><input id="bottomslidesubhead_'+y+'" type="text"  name="bottomslidesubhead_'+y+'"/></div></div></div></div></div>  </div><div class="col-lg-3"><button type="button" class="remove_row1"><i class="glyphicon glyphicon-minus"></i></button></div></div>');
        }
      });
      $(wrapper2).on('click', '.remove_row1', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          y--;
      });
    });



   function selectsubcat(){
    var ids= $('#sec_cat').val();
     //alert(ids);
     $.ajax({
            url: '<?php echo base_url(); ?>admin/overview/productName',
            type:'POST',
            data:{'ids':ids},
            success: function(proData) {
                // alert(proData);
                $('#pro_cat').html(proData);
            }

        });
}




</script>    

    