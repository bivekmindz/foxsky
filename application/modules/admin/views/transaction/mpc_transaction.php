<?php //pend($amountdetails);?><!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#example').DataTable();
        });
    </script>   
     <!--start of popup_mod-->
    <script type="text/javascript">
        $(document).ready(function(){
            $(".up_exal").click(function(){
              $(".popup_mod").show();
            });
            $(".close_bbb").click(function(){
              $(".popup_mod").hide();
            });
        });
    </script>  
    <style type="text/css">
    .popup_mod
    {
      width:100%;
      float:left;
      position: fixed;
      top:0;
      height: 100vh;
      background: rgba(0, 0, 0, 0.72);
      z-index: 99;
      display: none;
    }
    .browse_div
    {
      width: 300px;
    height: 200px;
    margin: auto;
    background: #fff;
    border-radius: 3px;
    position: fixed;
    top: 10%;
    left: 50%;
    transform: translatex(-50%);
    }
    .browse_inpt
    {
      width:100%;
      float:left;
      padding: 10px 48px;
      margin-top: 40px;
      position: relative;
    }
    .browse_inpt input
    {
      width:100%;
      padding: 5px 5px;
      float:left;
    }
    .close_bbb
    {
    position: absolute;
    right: 11px;
    top: 5px;
    color: #d01111;
    font-size: 20px;
    cursor: pointer;
    }
   .browse_inpt .button_sbt_b input
    {
    border: none;
    background: #c10202;
    color: #fff;
    border-radius: 3px;
    width: 154px;
    margin-top: 13px;
    float: left;
    margin-left: 15px;
    }
     .up_exal
    {
    float: right;
    padding: 5px 16px;
    background: #2196f3;
    color: #fff;
    margin-right: 3px;
    cursor: pointer;
    border-radius: 5px;
    font-weight: 700;
    }

    .down_exal
    {
    float: right;
    padding: 5px 16px;
    background: #2196f3;
    color: #fff;
    margin-right: 3px;
    cursor: pointer;
    border-radius: 5px;
    font-weight: 700;
    }
    </style> 

     <!--end of popup_mod-->

    <div class="wrapper">

     <!--start of popup_mod-->
      <div class="popup_mod">
       <div class="browse_div">
         <span class="close_bbb">X</span>
         <div class="browse_inpt">
            <input type="file" name="uploadexcel" id="excel">
            <span class="button_sbt_b" style="display:none;"><button id="upload">Upload</button></span>
             <span id="upload_msg"></span>
         </div>
       </div>
      </div> 
      <!--end of popup_mod-->
    <?php  $this->load->view('helper/nav'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Settled Transactions</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this section, you can see the list of transactions!</p>
                                <p style="color:green;text-align:center;">
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            </div>
                        </div> 
                        <?php #if($this->session->userdata('popcoin_login')->s_admin_id==1)
                        {?> 
                 <form method="post" action="">
<input class="down_exal" style="border: none;color: #fff;padding: 5px 10px;font-size: 13px; float:right;  margin-bottom: 10px;" type="submit" value="Download Excel" name="pay_payment">
</form> 

      
<span class="up_exal">Upload Excel</span>
                <?php } ?>        
                        <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl">
                                        <thead>
                                            <tr>
                                               <?php if($this->session->userdata('popcoin_login')->s_admin_id==1){?> <th bgcolor='red'>S.No.</th>
                                               <?php } ?>
                                               <!--  <th bgcolor='red'>Date From & To (Settlement Cycle)</th>
                                                <th bgcolor='red'>Bill Pay Date</th> -->
                                                <th bgcolor='red'>Retailer/Store ID</th>
                                                <th bgcolor='red'>Company/Store Name</th>
                    
                                               <!--  <th bgcolor='red'>No. of Stores</th> -->                                               
                                                <!-- <th bgcolor='red'>Amount Receivable</th> -->
                                                <th bgcolor='red'>Amount Payable</th>
                                               <!--  <th bgcolor='red'>Arears</th>
                                                <th bgcolor='red'>Interest Amount</th>
                                                <th bgcolor='red'>ADJUST BALANCE</th> -->
                                                <th bgcolor='red'>Net Amount</th>
                                              
                                                <!-- <th bgcolor='red'>Bill Generate Date</th> -->    
                                                <th bgcolor='red'>Status</th>                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            //$curdate='2017-01-08';
                                             $i=0;
                                            foreach ($amountdetails as $amount) { $i++; //p($amount->retailer_id);
                                                if($amount->curr_status==1){  ?>
                                        <tr>
                                            <?php if($this->session->userdata('popcoin_login')->s_admin_id==1){?>
                                            <td><?php echo $i; ?></td>
                                            <?php } ?>
                                           <!--  <td><?php echo date("d-m-Y", strtotime($amount->from_date)).' To '.date("d-m-Y", strtotime($amount->to_date)); ?></td>
                                            <td><?php echo date("d-m-Y", strtotime($amount->pay_date)); ?></td> -->
                                            <td><!-- <a href="<?php echo base_url('admin/tax/storelist').'/'.$amount->txn_id.'/'.$amount->cycle_id.'/'.$amount->retailer_id; ?>" target="_blank"><?php echo $amount->s_name; ?></a> -->
                                            <?php echo $amount->s_name; ?></td>
                                            <td><?php echo $amount->company_name; ?></td>
                                            
                                      <!-- <td> --><?php //echo $amount->cc; 

                                      $ab=explode('#', $amount->cc);
                                      //p($ab);
                                      $rec=0;
                                      $pay=0;
                                      $rec_in=0;
                                      $pay_in=0;
                                      $str=0;
                                      $interest1='';

                                      foreach ($ab as $key => $value) {
                                          //p($value);
                                           $ab1=explode(',', $value);
                                      /*p($ab1[0]);
                                      p($ab1[1]);
                                      p($ab1[2]);
                                      p($ab1[3]);
                                      p($ab1[4]);
                                      p($ab1[5]);
                                      p($ab1[6]);*/

                                      $cycle_id=$ab1[6];
                                       $store_idd=$ab1[11];
                                      $str=$ab1[0];
                                      //$str=$str+$ab1[0];

                                      $mpc_interset=$ab1[9];
                                      $pay_permission=$ab1[10];

                                        $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[2];
                                        $pdate=$ab1[4];
                                        $date1 = new DateTime($pdate);
                                        $date2 = new DateTime($curdate);
                                        $diff = $date2->diff($date1)->format("%a");
                             //echo $diff;
                                        if($curdate<=$pdate)
                                        {
                                            $intamt='0';
                                            $interest1 =$interest1.$intamt.'#';
                                           
                                        }
                                        else
                                        {
                                            $intamt1=($ab1[2]*$mpc_interset)/100;
                                            $intamt=round((($intamt1*$diff)/365),4);
                                            $interest1 =$interest1.$intamt.'#';
                                           
                                        }



                                      if($ab1[3]=='Receivables')
                                      {
                                          /*p($ab1[0]);
                                          p($ab1[1]);
                                          p($ab1[2]);
                                          p($ab1[3]);
                                          p($ab1[4]);
                                          p($ab1[5]);
                                          p($ab1[6]);*/
                                          $newamtrec =$ab1[2];
                                          $newrec_ref =$ab1[5];
                                          
                                          $mpc_intersetr=$ab1[9];

                                $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[2];
                                $pdate=$ab1[1];
                                $date1 = new DateTime($pdate);
                                $date2 = new DateTime($curdate);
                                $diff = $date2->diff($date1)->format("%a");
                     // echo $diff;
                                if($curdate<=$pdate)
                                {
                                    $intamt='0';
                                    $rec_in=$rec_in+$intamt;
                                    $rec =$rec+$ab1[2];
                                }
                                else
                                {
                                    $intamt1=($ab1[2]*$mpc_intersetr)/100;
                                    $intamt=round((($intamt1*$diff)/365),4);

                                    //echo $intamt1;
                                    $rec_in=$rec_in+$intamt;
                                    $rec =$rec+$ab1[2];
                                }
                                      }
                                      if($ab1[3]=='Payables')
                                      {
                                          /*p($ab1[0]);
                                          p($ab1[1]);
                                          p($ab1[2]);
                                          p($ab1[3]);
                                          p($ab1[4]);
                                          p($ab1[5]);
                                          p($ab1[6]);*/
                                           $newamtpay =$ab1[2];
                                           $newpay_ref =$ab1[5];
                                           $mpc_intersetp=$ab1[9];
                                         

                                $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[2];
                                $pdate=$ab1[1];
                                $date1 = new DateTime($pdate);
                                $date2 = new DateTime($curdate);
                                $diff = $date2->diff($date1)->format("%a");
                     
                                if($curdate<=$pdate)
                                {
                                    $intamt='0';
                                    $pay_in=$pay_in+$intamt;
                                    $pay =$pay+$ab1[2];
                                }
                                else
                                {
                                    $intamt1=($ab1[2]*$mpc_intersetp)/100;
                                    $intamt=round((($intamt1*$diff)/365),4);
                                    //echo $intamt;
                                    $pay_in=$pay_in+$intamt;
                                    $pay =$pay+$ab1[2];
                                }


                                      }
                                      }  //echo $ab1[11].'___';
#echo $str; // store count

$m=date("m", strtotime($amount->t_createdon));
$y=date("Y", strtotime($amount->t_createdon));


                                      ?><!-- </td> -->
                                    <!--   <td><?php if(empty($newamtrec)) $newamtrec='0'; else $newamtrec=$newamtrec;
                                          echo number_format($newamtrec,2);?></td> -->
                                            <td>
                                            <?php if($ab1[3]=='Payables')
                                             {
                                            if(empty($newamtpay)) $newamtpay='0'; else $newamtpay=$newamtpay;
                                            //echo ' <a href="'.base_url('admin/transaction/details_pay').'/'.$amount->retailer_id.'/'.$newrec_ref.'/Payables/'.$cycle_id.'/'.$m.'/'.$y.'/'.$store_idd.'" target="_blank">-'.number_format($newamtpay,2).'</a>';
                                            echo '-'.number_format($newamtpay,2);
                                            //echo $pay; // Payables
                                           }else{ echo '0.00';}  ?></td>
                                            <!-- <td><?php $ret=$rec+$rec_in;
                                                        $pet=$pay+$pay_in;

                                                        $apa=round($pay-$newamtpay,4);
                                                        $are=round($rec-$newamtrec,4)-$apa;
                                                      
                                             echo number_format($are,2); //Arears ?></td>
                     <td><?php
                          $int=$rec_in-$pay_in; 
                     echo number_format($int,2);//interest
                    
                        ?></td>
                                   
                                                <td>

                                                  <?php 
        if($amount->pay_permission==1)
        {
            $act_mode='remaining_balance';
            $idd=$amount->retailer_id;
        }
        else
        {
            $act_mode='remaining_balance_store';
            $idd=$store_idd;
        }

        $param = array(
                  'act_mode' => $act_mode,
                  'row_id' => $idd,
                  'param1' => '',
                  'param2' => '',
                  'param3' => '',
                  'param4' => '',
                  'param5' => '',
                  'param6' => '',
                  'param7' => '',
                  'param8' => '',
                  'param9' => '',
                  'param10' => '',
                  'param11' => '',
                  'param12' => '',
                  'param13' => '',
                  'param14' => '',
                  'param15' => '',
                  'param16' => '',
                  'param17' => '',
                  'param18' => '',
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $remaining_balance = $this->supper_admin->call_procedureRow('proc_manualpayment', $param);
            //echo '-';
            $rem_amt=round($remaining_balance->rem_bal,2);
            if($rem_amt=='')
            {
              echo '0.00';
            } 
            else
            {
              echo '-'.$rem_amt;
            }
                                                  ?>

                                                </td> -->

                                                <td><?php //echo $newamtrec-$newamtpay.'___';
                                            $amt=($newamtrec-$newamtpay)+$are+$int; 
                                             //echo number_format($amt,2); 
                                              echo number_format($amt-$rem_amt,2); 
                                           ?></td>
                                            <td>


                                                <?php // echo ' <a href="'.base_url('admin/tax/storelistdetails').'/'.$amount->txn_id.'/'.$amount->cycle_id.'/'.$amount->retailer_id.'" target="_blank">View</a>';
                                               // echo $amount->pay_type;

                                                

if($ret > $pet)
{
    echo ' Receivables';

    if($amount->pay_permission==1)
{
    echo '<input type="hidden" class="form-control" id="payment_'.$amount->retailer_id.'" value="1"><a style="background: #2196F3;padding: 5px 10px;margin-left: 10px;color: #fff;border-radius: 5px;font-weight: 700;" href="'.base_url().'admin/transaction/manual_retailer_paymentprocess/'.$amount->retailer_id.'" target="_blank" onclick="return fieldValidate('.$amount->retailer_id.');">Manual Payment</a>';
}
else
{
  echo '<input type="hidden" class="form-control" id="payment_'.$amount->retailer_id.'" value="1"><a style="background: #2196F3;padding: 5px 10px;margin-left: 10px;color: #fff;border-radius: 5px;font-weight: 700;" href="'.base_url().'admin/transaction/manual_store_paymentprocess/'.$amount->retailer_id.'/'.$store_idd.'" target="_blank" onclick="return fieldValidate('.$amount->retailer_id.');">Manual Payment</a>';
}
}
else if($ret < $pet)
{
    echo ' Payables';
    ?>
    <!-- <form action="" method="post">
    <input type="text" class="form-control" name="payment" id="payment_<?php echo $amount->t_id; ?>" value="<?php echo substr($amt,1); ?>" size="10" readonly>
     <input type="hidden" name="arears" value="<?php echo $are; ?>">
    <input type="hidden" name="all" value="<?php echo $amount->cc; ?>">
    <input type="hidden" name="intamt" value="<?php echo $interest1; ?>">
    
    <input type="submit" name="save_data" value="Pay" class="form-control1 pay" onclick="return fieldValidate('<?php echo $amount->t_id; ?>');" />
    </form> -->
    <?php
}
else if($ret == $pet)
{
    //echo $ab1[11];
    echo ' Settled';
    if($amount->remaining_amt!='0') {  
      if($ab1[11]==0){
                                               
     ?>
    <form action="" method="post">
    <input type="hidden" class="form-control" name="payment" id="payment_<?php echo $amount->t_id; ?>" value="<?php echo $amt; ?>" size="10" readonly>
     <input type="hidden" name="arears" value="<?php echo $are; ?>">
    <input type="hidden" name="all" value="<?php echo $amount->cc; ?>">
   <input type="hidden" name="intamt" value="<?php echo $interest1; ?>">
    
    <input type="submit" name="settled_data" value="Settled" class="form-control1 pay" onclick="return fieldValidate('<?php echo $amount->t_id; ?>');" />
    </form>
    <?php
} }
}
                                                

                                               /* if($amount->remaining_amt=='0') {  
                                                if($amount->interest_amt!='' && $amount->remain_intamt=='0') { 
                                                
                                                echo '</br> Confirmed'; 
                                                if($this->session->userdata('popcoin_login')->s_usertype==2){
                                                    echo '</br> <a href="'.base_url('admin/tax/generateinvoice').'/'.$amount->txn_id.'/'.$amount->cycle_id.'/'.$amount->retailer_id.'" target="_blank">Invoice</a>';
                                                }



                                            }
                                            else { #1

                                                if($amount->remaining_amt=='0') { // pay
                                                echo '</br> Confirmed'; 
                                                if($this->session->userdata('popcoin_login')->s_usertype==2){
                                                    echo '</br> <a href="'.base_url('admin/tax/generateinvoice').'/'.$amount->txn_id.'/'.$amount->cycle_id.'/'.$amount->retailer_id.'" target="_blank">Invoice</a>';
                                                } 
                                            }
                                            else // pay
                                            {
                                            
                                                if($this->session->userdata('popcoin_login')->s_admin_id!=1){ #11
                                                 if($amount->pay_type=='Receivables'){ #2 
                                                  if($amount->pay_amt>=0){ #3 ?>
                                                <form action="" method="post">
                                                <input type="text" class="form-control" name="payment" id="payment_<?php echo $amount->t_id; ?>" value="<?php if($amount->remaining_amt=="") echo $amount->pay_amt+$intamt+$amount->interest_amt; else echo $amount->remaining_amt+$intamt+$amount->interest_amt; ?>" size="10">
                                                <input type="hidden" name="t_id" value="<?php echo $amount->t_id; ?>">
                                <input type="hidden" name="paydate" value="<?php echo $amount->pay_date; ?>">

                                                <input type="hidden" name="intamt" value="<?php echo $intamt; ?>">
                                                <input type="hidden" name="intamtbefore" value="<?php echo $amount->interest_amt; ?>">
                                    <input type="hidden" name="pay_amt" value="<?php if($amount->remaining_amt=='') {echo $amount->pay_amt; } else{ echo $amount->remaining_amt; } ?>">
                                               <input type="hidden" name="pay_amt1" value="0">
                                                <input type="hidden" name="txn_id" value="<?php echo $amount->txn_id; ?>">
                                            
                                                <input type="submit" name="save_data" value="Pay" class="form-control" onclick="return fieldValidate('<?php echo $amount->t_id; ?>');" />
                                                </form>
                                                <?php }#2 
                                                } #3
                                            }#11

                                            }
                                        }//pay

                                        }


                                                else { #1
                                            
                                                if($this->session->userdata('popcoin_login')->s_admin_id!=1){ #11
                                                 if($amount->pay_type=='Receivables'){ #2 
                                                  if($amount->pay_amt>=0){ #3 ?>
                                                <form action="" method="post">
                                                <input type="text" class="form-control" name="payment" id="payment_<?php echo $amount->t_id; ?>" value="<?php if($amount->remaining_amt=="") echo $amount->pay_amt+$intamt+$amount->interest_amt; else echo $amount->remaining_amt+$intamt+$amount->interest_amt; ?>" size="10">
                                                <input type="hidden" name="t_id" value="<?php echo $amount->t_id; ?>">
                                <input type="hidden" name="paydate" value="<?php echo $amount->pay_date; ?>">

                                                <input type="hidden" name="intamt" value="<?php echo $intamt; ?>">
                                                <input type="hidden" name="intamtbefore" value="<?php echo $amount->interest_amt; ?>">
                                    <input type="hidden" name="pay_amt" value="<?php if($amount->remaining_amt=='') {echo $amount->pay_amt; } else{ echo $amount->remaining_amt; } ?>">
                                               <input type="hidden" name="pay_amt1" value="0">
                                                <input type="hidden" name="txn_id" value="<?php echo $amount->txn_id; ?>">
                                            
                                                <input type="submit" name="save_data" value="Pay" class="form-control" onclick="return fieldValidate('<?php echo $amount->t_id; ?>');" />
                                                </form>
                                                <?php }#2 
                                                } #3
                                            }#11
                                               
                                            }*/#1  ?>
                                                <?php //if($this->session->userdata('popcoin_login')->s_admin_id==1)
 ?>
                                            </td>
                                        </tr>
                                        <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
            
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>

<script type="text/javascript">
function schange(ids) {
  var st=$('#status'+ids).val();
  //alert(st);
  $.ajax({
    url:'<?php echo base_url()?>admin/receipts/updatereceipts',
    type:'post',
    data:{'uid':ids,'ust':st},
    success: function(data){
      $('#msg'+ids).html('Successfully Updated!').css('color','green');
    }

  });
}

function showdealdetailstr(id)
{
  
  $('.dealdetailstr').closest('tr').addClass('str');
  $('#sho_'+id).closest('tr').removeClass('str');

}
</script>

<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#filterForm").validate({
          rules: {            
              month : "required",
              store_id : "required"          
          },
          messages: {
              month : "This field is required!",
              store_id : "This field is required!"           
          },
          submitHandler: function(form) {
            form.submit();
          }
      });
   });
</script>
<script type="text/javascript">
function fieldValidate(value) {
    var value=$('#payment_'+value).val();
    if(value=="") {
        alert("Please input an amount!"); return false;
    } else {

       
        var r = confirm("Confirmed this amount!");
        if (r == true) {
           
        } else {
           
            return false;
        }
       
    }
}
</script>
<style type="text/css">

.pay{background: #16A12B;
    padding: 5px 10px;
    margin-left: 78px;
    color: #fff;
    border-radius: 5px;
    font-weight: 700;
    border: none;
    width: 33%;
    margin-top: 5px;
    box-shadow: 0px 5px 5px gray;}
    
.deal_detail{
    float:left;
    width:100%;
    height:auto;
    background:#f1f1f1;
    padding:10px 10px;
}
.deal_left{
    float:left;
    width:25%;
}
.d_image{
    float:left;
    width:100%;
    background: #fff;
    padding:6px 6px;
}
.d_image img{
    width:100%;
}
.deal_right{
    float:left;
    width: 75%;
    padding: 0px 20px;
}
.deal_row{
    float:left;
    width:100%;
    padding:7px 0px;
}
.deal_d2
{
    float:left;
    width:50%;
    padding:0px 10px;
}
.deal_d1
{
    float:left;
    width:100%;
    padding:0px 10px;
}
.deal_text
{
    float:left;
    width:100%;
    font-weight:600;
}
.deal_text_b
{
    float:left;
    width:100%;
    font-weight:400;
    padding:8px 0;
}
.deal_text span{
    font-weight:400!important;
    margin-left:15px;
}
.approve_btn
{
    float:right;
    padding:8px 12px;
    background: green;
    color:#fff!important;
}
.issue_btn
{
    float:right;
    padding:8px 12px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.t_area{
    float:left;
    width:60%;
}
.sbt_btn{
    float:left;
    padding:12px 15px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.issuediv{
    display:none;
}

.d_image img {
    width: 200px;
    height: 252px;
    text-align: center;
}

 </style>

  <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>-->
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
<script type="text/javascript">
$(document).ready(function()
{   
    $(".monthPicker").datepicker({
        dateFormat: 'm-yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('m-yy', new Date(year, month, 1)));
        }
    });

    $(".monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
});




$('#upload').on('click', function() {
    var file_data = $('#excel').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    //alert(form_data);                             
    $.ajax({
          url: '<?php echo base_url(); ?>admin/transaction/uploadfile', // point to server-side PHP script 
          dataType: 'text',  // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,                         
          type: 'post',
          beforeSend: function () {
                    $("#upload_msg").html("Uploading, please wait....");
                    $('#upload_msg').css('color','red');
                },
          success: function(php_script_response){
              //alert(php_script_response); // display response from the PHP script, if any
              $("#upload_msg").html(php_script_response);
              $('#upload_msg').css('color','green');
              $("#excel").val('');
              setTimeout(function(){
                  location.reload();
              }, 5000);
          }
     });
});

$(document).ready(function () { 
  $('#excel').change(function () { 
    var val = $(this).val().toLowerCase(); 
    var regex = new RegExp("(.*?)\.(xls)$"); 
    if(!(regex.test(val))) { 
      $(this).val(''); 
      $('.button_sbt_b').css('display','none');
      alert('Unsupported file, only excel(xls) files are allowed!'); 
    } 
    else
    {
      $('.button_sbt_b').css('display','block');
    }
 
  }); 

    
    }); 
</script>
