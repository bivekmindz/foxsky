<?php //pend($amountdetails);?><!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#example').DataTable();
        });
    </script>   
    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Arears Transactions Details</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this section, you can see the list of transactions!</p>
                                <p style="color:green;text-align:center;">
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            </div>
                        </div>
                              
                        <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl">
                                        <thead>
                                            <tr>
                                               <?php if($this->session->userdata('popcoin_login')->s_admin_id==1){?> <th bgcolor='red'>S.No.</th>
                                               <?php } ?>
                                                <th bgcolor='red'>Date From & To (Settlement Cycle)</th>
                                                <th bgcolor='red'>Bill Pay Date</th>
                                                <th bgcolor='red'>Retailer ID</th>
                                                <th bgcolor='red'>Company Name</th>
                    
                                                <th bgcolor='red'>No. of Stores</th>                                               
                                                <th bgcolor='red'>Amount Receivable</th>
                                                <th bgcolor='red'>Amount Payable</th>
                                                <th bgcolor='red'>Arears</th>
                                                <th bgcolor='red'>Interest Amount</th>
                                                <th bgcolor='red'>Net Amount</th>
                                                <th bgcolor='red'>Reference ID</th>
                                                <th bgcolor='red'>Bill Generate Date</th>   
                                                                                     
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            //$curdate='2017-01-08';
                                             $i=0;
                                            foreach ($amountdetails as $amount) { $i++; //p($amount->t_createdon);
                                                if($amount->curr_status==1){  ?>
                                        <tr>
                                            <?php if($this->session->userdata('popcoin_login')->s_admin_id==1){?>
                                            <td><?php echo $i; ?></td>
                                            <?php } ?>
                                            <td><?php echo date("d-m-Y", strtotime($amount->from_date)).' To '.date("d-m-Y", strtotime($amount->to_date)); ?></td>
                                            <td><?php echo date("d-m-Y", strtotime($amount->pay_date)); ?></td>
                                            <td><?php echo $amount->s_name; ?></td>
                                            <td><?php echo $amount->company_name; ?></td>
                                            <td><?php echo $amount->store_count; ?></td>
                                      
                                           
                                       
                                            <td><?php if($amount->pay_type=='Receivables')
                                                     echo $amount->update_amt; 
                                                    else
                                                     echo '0';  ?></td>
                                                  <td><?php if($amount->pay_type=='Receivables')
                                                   echo '0';  
                                                    else
                                                        echo '-'.$amount->update_amt;
                                                     ?></td>
                                            <td><?php echo $amount->remaining_amt; ?></td>
                     <td><?php  @$intval=$interest->i_value;
                                $curdate=date('Y-m-d h:i:s');
                                $pdate=$amount->pay_date;
                                //$pdate=$amount->t_createdon;
                                $date1 = new DateTime($pdate);
                                $date2 = new DateTime($curdate);
                                $diff = $date2->diff($date1)->format("%a");
                     
                        if($curdate<=$pdate)
                        {
                            echo $intamt='0';
                        }
                        else
                        {
                            $intamt1=($amount->update_amt*$intval)/100;
                            $intamt=round((($intamt1*$diff)/365),4);
                            echo $intamt;
                        }
                        ?></td>
                                            <td><?php if($amount->pay_type=='Receivables')
                                                    echo '+'.round($amount->update_amt,4);
                                                    else
                                                     echo '-'.$amount->update_amt;  ?></td>
                                         
                                             <td><!--<a href="<?php echo base_url();?>admin/tax/viewpaymentdetails/<?php echo $amount->txn_id; ?>">--><?php echo $amount->txn_id; ?></td>
                                                <td><?php echo date("d-m-Y h:i:s", strtotime($amount->t_createdon)); ?></td>

                                            
                                        </tr>
                                        <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
            
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>

<script type="text/javascript">
function schange(ids) {
  var st=$('#status'+ids).val();
  //alert(st);
  $.ajax({
    url:'<?php echo base_url()?>admin/receipts/updatereceipts',
    type:'post',
    data:{'uid':ids,'ust':st},
    success: function(data){
      $('#msg'+ids).html('Successfully Updated!').css('color','green');
    }

  });
}

function showdealdetailstr(id)
{
  
  $('.dealdetailstr').closest('tr').addClass('str');
  $('#sho_'+id).closest('tr').removeClass('str');

}
</script>

<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#filterForm").validate({
          rules: {            
              month : "required",
              store_id : "required"          
          },
          messages: {
              month : "This field is required!",
              store_id : "This field is required!"           
          },
          submitHandler: function(form) {
            form.submit();
          }
      });
   });
</script>
<script type="text/javascript">
function fieldValidate(value) {
    var value=$('#payment_'+value).val();
    if(value=="") {
        alert("Please input an amount!"); return false;
    } else {

       
        var r = confirm("Confirmed this amount!");
        if (r == true) {
           
        } else {
           
            return false;
        }
       
    }
}
</script>
<style type="text/css">
.deal_detail{
    float:left;
    width:100%;
    height:auto;
    background:#f1f1f1;
    padding:10px 10px;
}
.deal_left{
    float:left;
    width:25%;
}
.d_image{
    float:left;
    width:100%;
    background: #fff;
    padding:6px 6px;
}
.d_image img{
    width:100%;
}
.deal_right{
    float:left;
    width: 75%;
    padding: 0px 20px;
}
.deal_row{
    float:left;
    width:100%;
    padding:7px 0px;
}
.deal_d2
{
    float:left;
    width:50%;
    padding:0px 10px;
}
.deal_d1
{
    float:left;
    width:100%;
    padding:0px 10px;
}
.deal_text
{
    float:left;
    width:100%;
    font-weight:600;
}
.deal_text_b
{
    float:left;
    width:100%;
    font-weight:400;
    padding:8px 0;
}
.deal_text span{
    font-weight:400!important;
    margin-left:15px;
}
.approve_btn
{
    float:right;
    padding:8px 12px;
    background: green;
    color:#fff!important;
}
.issue_btn
{
    float:right;
    padding:8px 12px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.t_area{
    float:left;
    width:60%;
}
.sbt_btn{
    float:left;
    padding:12px 15px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.issuediv{
    display:none;
}

.d_image img {
    width: 200px;
    height: 252px;
    text-align: center;
}

 </style>

  <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>-->
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
<script type="text/javascript">
$(document).ready(function()
{   
    $(".monthPicker").datepicker({
        dateFormat: 'm-yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,

        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).val($.datepicker.formatDate('m-yy', new Date(year, month, 1)));
        }
    });

    $(".monthPicker").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
});
</script>
<!-- 
<label for="month">Month: </label>
<input type="text" id="month" name="month" class="monthPicker" /> -->