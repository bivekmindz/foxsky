<?php 
//echo 'dd';
//pend($amountdetails);?><!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#example').DataTable();
        });
    </script>   
    <div class="wrapper">
    <?php  $this->load->view('helper/nav'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Manual Settle Transaction</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this section, you can see the list of manual transactions!</p>
                                <p style="color:green;text-align:center;">
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            </div>
                        </div> 
                            
                        <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">

                                    <table class="grid_tbl">
                                        <thead>
                                            <tr>
                                               <!--  <th bgcolor='red'>S.No.</th>
                                               
                                               <th bgcolor='red'>Date From & To (Settlement Cycle)</th>
                                                <th bgcolor='red'>Bill Pay Date</th> -->
                                                <th bgcolor='red'>Retailer/Store ID</th>
                                                <th bgcolor='red'>Company/Store Name</th>
                    
                                                <th bgcolor='red'>No. of Stores</th>                                               
                                                <th bgcolor='red'>Amount Receivable</th>
                                                <th bgcolor='red'>Amount Payable</th>
                                                <th bgcolor='red'>Arrears</th>
                                                <th bgcolor='red'>Interest Amount</th>
                                                <th bgcolor='red'>Net Amount</th>
                                               <!--  <th bgcolor='red'>Reference ID</th> 
                                             <th bgcolor='red'>Action</th>-->
                                                 <th bgcolor='red'>Adjust Balance(-)</th>
                                                <th bgcolor='red'>Net Amount Receivables/Payables</th>   
                                                                                         
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            //$curdate='2017-01-08';
                                             $i=0;
                                            foreach ($amountdetails as $amount) { $i++; //p($amount);
                                                if($amount->curr_status==1){  ?>
                                        <tr>
                                             <!-- 
                                            <td><?php echo $i; ?></td>
                                            
                                           <td><?php echo date("d-m-Y", strtotime($amount->from_date)).' To '.date("d-m-Y", strtotime($amount->to_date)); ?></td>
                                            <td><?php echo date("d-m-Y", strtotime($amount->pay_date)); ?></td> -->
                                            <td><!-- <a href="<?php echo base_url('admin/tax/storelist').'/'.$amount->txn_id.'/'.$amount->cycle_id.'/'.$amount->retailer_id; ?>" target="_blank"><?php echo $amount->s_name; ?></a> -->
                                            <?php echo $amount->s_name; ?></td>
                                            <td><?php echo $amount->company_name; ?></td>
                                            
                                      <td><?php //p($amount);//
                                      //echo $amount->cc; 

                                      $ab=explode('#', $amount->cc);
                                      //p($ab);
                                      $rec=0;
                                      $pay=0;
                                      $rec_in=0;
                                      $pay_in=0;
                                      $str=0;
                                      $interest1='';
                                      
                                      foreach ($ab as $key => $value) {
                                          //p($value);
                                           $ab1=explode(',', $value);
                                      /*p($ab1[0]);
                                      p($ab1[1]);
                                      p($ab1[2]);
                                      p($ab1[3]);
                                      p($ab1[4]);
                                      p($ab1[5]);
                                      p($ab1[6]);*/
                                      $tid=$ab1[7];
                                      $tid_amt=$ab1[2];
                                      $str=$ab1[0];
                                      $cycle_id=$ab1[6];
                                      //$str=$str+$ab1[0];
                                       $mpc_interset=$ab1[8];
                                      $pay_permission=$ab1[9];
                                       $store_idd=$ab1[11];

                                        $curdate=date('Y-m-d h:i:s');
                                $pdate=$ab1[1];
                                        //$pdate=$ab1[4];
                                        $date1 = new DateTime($pdate);
                                        $date2 = new DateTime($curdate);
                                        $diff = $date2->diff($date1)->format("%a");
                             //echo $diff;
                                        if($curdate<=$pdate)
                                        {
                                            $intamt='0';
                                            $interest1 =$interest1.$intamt.'#';
                                           
                                        }
                                        else
                                        {
                                            $intamt1=($ab1[2]*$mpc_interset)/100;
                                            $intamt=round((($intamt1*$diff)/365),4);
                                            $interest1 =$interest1.$intamt.'#';
                                            //$interest1 =$intamt.'#';
                                           
                                        }



                                      if($ab1[3]=='Receivables')
                                      {
                                          /*p($ab1[0]);
                                          p($ab1[1]);
                                          p($ab1[2]);
                                          p($ab1[3]);
                                          p($ab1[4]);
                                          p($ab1[5]);
                                          p($ab1[6]);*/
                                          $newamtrec =$ab1[2];
                                          //$newamtrec =$ab1[10];
                                          $newrec_ref =$ab1[5];
                                          $mpc_intersetr=$ab1[8];
                                          

                                $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[4];
                                $pdate=$ab1[1];
                                $date1 = new DateTime($pdate);
                                $date2 = new DateTime($curdate);
                                $diff = $date2->diff($date1)->format("%a");
                     // echo $diff;
                                if($curdate<=$pdate)
                                {
                                    $intamt='0';
                                    $rec_in=$rec_in+$intamt;
                                    $rec =$rec+$ab1[2];
                                }
                                else
                                {
                                    $intamt1=($ab1[2]*$mpc_intersetr)/100;
                                    $intamt=round((($intamt1*$diff)/365),4);

                                    //echo $intamt1;
                                    $rec_in=$rec_in+$intamt;
                                    $rec =$rec+$ab1[2];
                                }
                                      }
                                      if($ab1[3]=='Payables')
                                      {
                                          /*p($ab1[0]);
                                          p($ab1[1]);
                                          p($ab1[2]);
                                          p($ab1[3]);
                                          p($ab1[4]);
                                          p($ab1[5]);
                                          p($ab1[6]);*/
                                           $newamtpay =$ab1[2];
                                           //$newamtpay =$ab1[10];
                                           $newpay_ref =$ab1[5];
                                           $mpc_intersetp=$ab1[8];
                                         

                                $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[2];
                                $pdate=$ab1[1];
                                $date1 = new DateTime($pdate);
                                $date2 = new DateTime($curdate);
                                $diff = $date2->diff($date1)->format("%a");
                     
                                if($curdate<=$pdate)
                                {
                                    $intamt='0';
                                    $pay_in=$pay_in+$intamt;
                                    $pay =$pay+$ab1[2];
                                }
                                else
                                {
                                    $intamt1=($ab1[2]*$mpc_intersetp)/100;
                                    $intamt=round((($intamt1*$diff)/365),4);
                                    //echo $intamt;
                                    $pay_in=$pay_in+$intamt;
                                    $pay =$pay+$ab1[2];
                                }


                                      }
                                      }  //echo $amount->cycle_id.'___';
echo $str; // store count

$m=date("m", strtotime($amount->t_createdon));
$y=date("Y", strtotime($amount->t_createdon));

                                      ?></td>
                                      
                                      <td><?php if(empty($newamtrec)) $newamtrec='0'; else $newamtrec=$newamtrec;
                                      //echo ' <a href="'.base_url('admin/transaction/details_rec').'/'.$amount->retailer_id.'/'.$newrec_ref.'/Receivables/'.$cycle_id.'/'.$m.'/'.$y.'/'.$store_idd.'" target="_blank">+'.$newamtrec.'</a>';
                                      //echo $rec;

                                         echo number_format($newamtrec,2);

                                       // Receivables?></td>
                                        <td><?php if(empty($newamtpay)) $newamtpay='0'; else $newamtpay=$newamtpay;
                                       //echo ' <a href="'.base_url('admin/transaction/details_pay').'/'.$amount->retailer_id.'/'.$newrec_ref.'/Payables/'.$cycle_id.'/'.$m.'/'.$y.'/'.$store_idd.'" target="_blank">-'.$newamtpay.'</a>';
                                            //echo $pay; // Payables
                                        echo number_format($newamtpay,2);
                                             ?></td>
                                           
                                            <td><?php $ret=$rec+$rec_in;
                                                        $pet=$pay+$pay_in;

                                                        $apa=round($pay-$newamtpay,4);
                                                        //$are=round($rec-$newamtrec,4)-$apa;
                                                        //$are=$apa-round($rec-$newamtrec,4);
                                                         $are=round($rec-$newamtrec,4)-$apa;
                                                      
                                             echo number_format($are,2); //Arears ?></td>
                     <td><?php //echo $rec_in.'__'.$pay_in.'__'.$interest1;
                     //echo $int=round($pay_in-$rec_in+$interest1,3); //interest
                   // echo $int=$pay_in-$rec_in; //interest
                     $int=$rec_in-$pay_in;
                     echo number_format($int,2);
                    
                        ?></td>
                                            <td><?php //echo $newamtpay-$newamtrec.'___';
                                            //echo $amt=($newamtrec-$newamtpay)+$are+$int; 
                                            //echo $amt=($newamtpay-$newamtrec)+($are+$int); 
                                             $amt=($newamtrec-$newamtpay)+$are+$int; 
                                             $amt1=$newamtrec-$newamtpay;
                                           echo number_format($amt,2);
                                           //p($amt);
                                             ?></td>
                                         <!-- 
                                             <td><?php echo $amount->txn_id; ?></td>
                                                <td><?php echo date("d-m-Y h:i:s", strtotime($amount->t_createdon)); ?></td> -->
                                                <td><?php $rem_bal=number_format($remaining_balance->rem_bal,2);
            if($rem_bal=='0.00')
            {
              echo '0.00';
            } 
            else
            {
              echo '-'.$rem_bal;
            } ?></td>
                                            <td style="padding: 10px 0px;width: 100%;">


                                                <?php $v=str_replace('-', '', $amt);
                                                
                                                if($remaining_balance->rem_bal!=0)
                                                {
                                                    echo $a_mt=round($v,2);
                                                }
                                                else
                                                {
                                                    echo $a_mt=round($v-$rem_bal,2);
                                                }
                                                
                                                
                                                //?>
                                                

                                            
                                            </td>
                                            
                                        </tr>
                                        <?php } } ?>
                                        </tbody>
                                    </table>
                                
                                     <table class="grid_tbl" style="margin-top:10px;">
                                        <thead>
                                            <tr>
                                              
                    
                                                <!-- <th bgcolor='red'>Amount Paid</th> -->                                               
                                                <th bgcolor='red'>Mode of Payment</th>
                                                <th bgcolor='red'>Reference Id</th>
                                                <th bgcolor='red'>Receive Date</th>
                                                 <th bgcolor='red'>Discount Amount</th>
                                                <th bgcolor='red'>Amount Paid</th>                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <form action="" method="post">
                                          <tr>
                                            
                                            <td><select name="mode" id="mode" class="form-control">
                                              <option value="">Select</option>
                                              <option value="Cash">Cash</option>
                                              <option value="Cheque">Cheque</option>
                                              <option value="NEFT/RTGS">NEFT/RTGS</option>
                                            </select></td>
                                            <td><input type="text" name="refeid" class="form-control"/></td>
                                            <td>

 <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat: 'dd-mm-yy'});
  } );
  </script>
<input type="text" id="datepicker" class="form-control" name="date" value="<?php echo date('d-m-Y'); ?>">
 </td>
 <td><input type="text" name="discount" class="form-control" value="0"/><input type="hidden" name="paid" class="form-control" value="0"/></td>
<td>
  
<?php if($amount->remaining_amt=="") 
{ 
  //$amount_f=(str_replace('-', '', number_format($amt,2)))-$rem_bal;
  $amount_f=$a_mt;
}
else 
{ 
  //$amount_f=(number_format($amount->remaining_amt,2)+number_format($int,2))-$rem_bal;
  $v1=($amount->remaining_amt+$int)-$rem_bal;
  $amount_f=round($v1,2);
 } ?>
   <!--  <input type="text" class="form-control" name="payment" id="payment_<?php echo $amount->t_id; ?>" value="<?php echo substr($amt,1); ?>" size="10"> -->
    <input type="hidden" name="arears" id="arears" value="<?php echo str_replace('-', '', round($are,2)) ;//substr($are,1); ?>">
    <input type="hidden" name="all" value="<?php echo $amount->cc; ?>">
    <input type="hidden" name="intamt1" value="<?php echo $interest1; ?>">

<input type="hidden" name="str_id" value="<?php echo $store_idd; ?>">
<input type="text" class="form-control" name="payment" id="payment" value="<?php echo $amount_f; ?>" size="10">
<input type="hidden" name="t_id" value="<?php echo $tid;//$amount->t_id; ?>">
<!-- <input type="hidden" name="paydate" value="<?php echo $amount->pay_date; ?>"> -->
<input type="hidden" name="intamt" id="intamt" value="<?php echo str_replace('-', '', round($int,2));//substr($int,1);//$intamt; ?>">
<input type="hidden" name="pay_amt" id="pay_amt" value="<?php echo round($tid_amt,2); ?>">
<input type="hidden" name="txn_id" value="<?php echo $amount->txn_id; ?>">
    <input type="submit" name="save_data" value="Update" class="form-control pay" onclick="return fieldValidate();" />

  </td></tr></form>
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </div>
            
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>

<script type="text/javascript">
function fieldValidate() {
    var value=$('#payment').val();
    var intamt=$('#intamt').val();
    var pay_amt=$('#pay_amt').val();
    var arr=$('#arears').val();
    //var to_amt1=(parseFloat(intamt)+parseFloat(pay_amt)+parseFloat(arr)).toFixed(0);
    var to_amt11='<?php echo $amount_f; ?>';
    var to_amt1=parseFloat(to_amt11).toFixed(0);
    var to_amt=(parseInt(((to_amt1*10)/100).toFixed(0))+parseFloat(arr));
    //alert(value);
    if($('#mode').val()=="") {
        alert("Please select mode of payment!"); return false;
    } 
    if(value=="") {
        alert("Please input an amount!"); return false;
    } else {
            //if(parseFloat(intamt) <= parseFloat(value))
           // {
                if(to_amt <= parseFloat(value))
                {
                    var r = confirm("Confirmed this amount!");
                    if (r == true) 
                    {

                      $.ajax({
                      url:'<?php echo base_url()?>admin/transaction/update_payment_status',
                      type:'post',
                      data:{},
                      success: function(data){
                       }
                      });
                       
                    } 
                    else 
                    {
                       
                        return false;
                    }
                }
                /*else
                {
                    alert("Please input less than or equal this ("+to_amt+") amount!"); return false;
                }
            }*/
            else
            {
               //alert("Please input greater than or equal this ("+to_amt+") amount!"); return false;
               alert("Please input minimum ("+to_amt+") amount!"); return false;
            }
       

        
       
    }
}  
</script>

<style type="text/css">

.pay{background: #2196F3;
    padding: 5px 10px;
    color: #fff;
    border-radius: 5px;
    font-weight: 700;
    border: none;
    /*width: 33%;
    margin-left: 78px;*/
    margin-top: 5px;}

.deal_detail{
    float:left;
    width:100%;
    height:auto;
    background:#f1f1f1;
    padding:10px 10px;
}
.deal_left{
    float:left;
    width:25%;
}
.d_image{
    float:left;
    width:100%;
    background: #fff;
    padding:6px 6px;
}
.d_image img{
    width:100%;
}
.deal_right{
    float:left;
    width: 75%;
    padding: 0px 20px;
}
.deal_row{
    float:left;
    width:100%;
    padding:7px 0px;
}
.deal_d2
{
    float:left;
    width:50%;
    padding:0px 10px;
}
.deal_d1
{
    float:left;
    width:100%;
    padding:0px 10px;
}
.deal_text
{
    float:left;
    width:100%;
    font-weight:600;
}
.deal_text_b
{
    float:left;
    width:100%;
    font-weight:400;
    padding:8px 0;
}
.deal_text span{
    font-weight:400!important;
    margin-left:15px;
}
.approve_btn
{
    float:right;
    padding:8px 12px;
    background: green;
    color:#fff!important;
}
.issue_btn
{
    float:right;
    padding:8px 12px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.t_area{
    float:left;
    width:60%;
}
.sbt_btn{
    float:left;
    padding:12px 15px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.issuediv{
    display:none;
}

.d_image img {
    width: 200px;
    height: 252px;
    text-align: center;
}

 </style>

  