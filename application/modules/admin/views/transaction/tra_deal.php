<!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>


   <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('#example').DataTable();
                                    } );
                                </script>    



    <div class="wrapper">
    <?php  $this->load->view('helper/nav'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Deal and Transfer PopCoins Details</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this section, you can see the list of Details!</p>
                                <p style="color:green;text-align:center;">
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            </div>
                        </div>


 <form method="post" action="">
<input style="background:#79400D;border: none;color: #fff;padding: 5px 10px;font-size: 13px; float:right;  margin-bottom: 10px;" type="submit" value="Download Excel" name="receipts">
</form>  


                        <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Settlement Cycle</th>
                                                <th bgcolor='red'>Deal Coupon Code / Transaction ID</th>
                                                <th bgcolor='red'>Store ID</th>
                                                <th bgcolor='red'>Store Name</th>
                                                <th bgcolor='red'>Location</th>
                                                <th bgcolor='red'>User Name</th>
                                                <th bgcolor='red'>Date & Time</th>
                                                <th bgcolor='red'>Deal ID</th>
                                                <th bgcolor='red'>Redeemed Popcoins / Transferred Popcoins</th>
                                                <th bgcolor='red'>MPC Margin %</th>
                                                <th bgcolor='red'>Net Popcoins Redeemed</th>
                                               
                                                                                          
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $i=0;
                                        $b_amt=0;
                                            foreach ($view as $value) {
                                            $i++;
                                           
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            
                                            <td><?php echo date("d-m-Y", strtotime($value->from_date)).' To '.date("d-m-Y", strtotime($value->to_date)).' </br>Pay Date </br>'.date("d-m-Y", strtotime($value->pay_date)); ?></td>
                                            <td><?php echo $value->dealcoupan_code; ?></td>
                                            <td><?php echo $value->storeuid; ?></td>
                                           
                                            <td><?php echo $value->storename; ?></td> 
                                            <td><?php echo $value->storeaddress; ?></td>
                                            
                                            <td><?php echo $value->customername;?></td> 
                                            <td><?php echo date("d-m-Y h:i:s", strtotime($value->date_of_purchase)); ?></td>
 
                                            <td><?php if($value->deal_id=='NA')
                                                        echo $value->deal_id; 
                                                        else
                                                        echo 'DEA('.$value->deal_id.')POP';     ?></td>
                                            <td><?php echo $value->amount; ?></td>
                                            <td><?php if($value->tax_amt=='')
                                                        echo 'NA'; 
                                                        else
                                                        echo $value->tax_amt.'%';
                                            //echo $value->tax_amt.' %'; ?></td>
                                            <td><?php echo $bamt=$value->pay_amt; ?></td>
                                            
                                           
                                    
                                    <?php $b_amt +=$bamt; } ?>

                                        </tbody>
                                    </table>
                                    Total Amount <?php echo ' :'.$b_amt; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>

