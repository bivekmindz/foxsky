<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Review </h2>
    </div>
        <div class="page_box">
       
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        <form action="" id="addCont" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-3">
                <div class="tbl_text">Product Name </div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><?php echo $reviewdata->prodname; ?></div>
            </div>
        </div>
        </div>

            <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-3">
                <div class="tbl_text">Order Id </div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><?php echo $reviewdata->r_ordid; ?></div>
            </div>
        </div>
        </div>
 </div>



 <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-3">
                <div class="tbl_text">User Id </div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><?php echo $reviewdata->username; ?></div>
            </div>
        </div>
        </div>

            <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-3">
                <div class="tbl_text">Review Name </div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><?php echo $reviewdata->headline; ?></div>
            </div>
        </div>
        </div>
 </div>


 <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-3">
                <div class="tbl_text">Desc </div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><?php echo $reviewdata->review_des; ?></div>
            </div>
        </div>
        </div>

            <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-3">
                <div class="tbl_text">Rating </div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><?php echo ($reviewdata->rating/2); ?></div>
            </div>
        </div>
        </div>
 </div>



        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>