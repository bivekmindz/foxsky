<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Purcahse Report</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view All Sales Report details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <form method="post" action="">
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
                                </form>
                                <div style="width:100%;overflow-x:auto;">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <th>PO No.</th>
                                          <th>Order no</th>
                                          <th>Order Date</th>
                                          <th>Product Description</th>
                                          <th>Qty</th>
                                          <th>Item Code</th>
                                          <th>Unit Price</th>
                                          <th>Amount</th>
                                          <th>Vat/Vat</th>
                                          <th>Vat/CST</th>
                                          <th>Vat/entry</th>
                                          <th>Tax Total</th>
                                          <th>Return Date</th>
                                          <th>Mod Of Return </th>
                                          <th>Retun Reason</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; 
                                        if(!empty($record))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                        foreach ($record as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->OrderNumber;?></td>
                                            <td><?php echo $value->OrderDate;?></td>
                                            <td><?php echo $value->description;?></td>
                                             <td><?php echo $value->ProQty;?></td>
                                            <td><?php echo $value->SkuNumber; ?></td>
                                            <td><?php echo $value->base_prize;?></td>
                                            <td><?php echo $value->amount;?></td>
                                            <td><?php echo $value->protaxvat_p;?></td>
                                            <td><?php echo $value->protaxcst_p;?></td>
                                             <td><?php echo $value->protaxentry_p;?></td>
                                             <td><?php echo $value->taxtotals;?></td>
                                             <td><?php echo $value->CreatedOn; ?></td>
                                              <td><?php echo $value->PaymentMode; ?></td>
                                                <td><?php echo $value->Remark; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>