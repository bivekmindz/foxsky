<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Purchase Report</h2>
                    </div>
                    <div class="page_box">
                      <div class="col-lg-12">
                          <p> In this section, admin can view all purchase details.</p>
                      </div>
                    </div>
                    <form action="" method="get" id="dateForm">
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-lg-4">
                                            <input type="text" class="date form-control" placeholder="Date From" id="start_date" name="start_date" value="<?php echo $_REQUEST['start_date'];?>" readonly />
                                    </div>
                                    <div class="col-lg-4">
                                            <input type="text" class="date form-control" placeholder="Date To" id="end_date" name="end_date" value="<?php echo $_REQUEST['end_date'];?>" readonly />
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="submit_tbl">
                                            <input type="submit" value="Search" name="submit" class="btn_button sub_btn" />
                                            <input type="button" onclick='location.href="<?php echo base_url('admin/report/purchasereport'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <form method="post" action="">
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
                                </form>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                <table class="grid_tbl">
                                    <thead>
                                       <tr>
                                          <th>S.No.</th>
                                          <th>Purchase Order number</th>
                                          <th>Supplier Name</th>
                                          <th>Supplier Invoice number</th>
                                          <th>Discount (If any)</th>
                                          <th>Net Amount Exclusive of VAT/CST</th>
                                          <th>VAT/CST @ 2%/5%/12.5%</th>
                                          <th>Total Amount Inclusive of VAT/CST</th>
                                          <th>Date of Purchase</th>
                                          <th>Supplier Invoice Image</th>
                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; 
                                        if(!empty($viewporeport))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                        foreach ($viewporeport as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->pono;?></td>
                                            <td><?php echo $value->firstname.' '.$value->lastname;?></td>
                                            <td><?php echo $value->poveninvoiceno;?></td>
                                            <td><?php echo $value->podiscount;?></td>
                                            <td><?php echo $value->ponetamt; ?></td>
                                            <td><?php echo $value->potax;?></td>
                                            <td><?php echo $value->pototalamt;?></td>
                                            <td><?php echo $value->porecreatedon;?></td>
                                            <td>
                                            <?php if($value->poinvoiceimg==''){ ?>
                                            Bill Not found.
                                            <?php } else { ?>
                                            <a href="<?php echo base_url();?>assets/supplierinvoice/<?php echo $value->poinvoiceimg;?>" target="_blank"><img height="50" width="50" src="<?php echo base_url();?>assets/supplierinvoice/<?php echo $value->poinvoiceimg;?>"></a>
                                            <?php } ?>
                                            </td>
                                            
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />
<script type="text/javascript">
    $('.date').datepicker({
    'format': 'yyyy-mm-dd',
    'autoclose': true,
    todayHighlight: true
    });
</script>  
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#dateForm").validate({
          // Specify the validation rules
          rules: {
              start_date : "required",
              end_date : "required"
          },
          
          messages: {
              start_date : "This field is required!",
              end_date : "This field is required!"
          },
          
          submitHandler: function(form) {
            form.submit();
          }
      });
   });
</script>