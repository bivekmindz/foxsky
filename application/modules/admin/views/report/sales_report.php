<div class="wrapper">
<?php 

p($record); exit;

$this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Sales Report</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view All Sales Report details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <form method="post" action="">
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
                                </form>
                                <div style="width:100%;overflow-x:auto;">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <th>S.NO.</th>
                                          <th>Product name</th>
                                          <th>Retailer</th>
                                          <th>Retailer Id</th>
                                          <th>Invoice No</th>
                                          <th>Invoice Date</th>
                                          <th>Item Code</th>
                                          <th>Qty</th>
                                          <th>Unit Price</th>
                                          <th>Amount</th>
                                          <th>OrderDate</th>
                                          <th>Discount</th>
                                          <th>Tax/Vat</th>
                                          <th>Tax/CST</th>
                                          <th>Tax/Entry</th>
                                          <th>Total Tax</th>
                                          <th>Mode Of Delivery</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; 
                                        if(!empty($record))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                        foreach ($record as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->ProName;?></td>
                                            <td><?php echo $value->ShippingName;?></td>
                                            <td><?php echo $value->Email;?></td>
                                            <td><?php echo $value->OrderNumber;?></td>
                                            <td><?php echo $value->OrderDate;?></td>
                                            <td><?php echo $value->SkuNumber;?></td>
                                            <td><?php echo $value->OrdQty;?></td>
                                            <td><?php echo $value->FinalPrice;?></td>
                                            <td><?php echo $value->amount;?></td>
                                            <td><?php echo $value->CreatedOn;?></td>
                                            <td><?php echo $value->TotalDiscount;?></td>
                                            <td><?php echo $value->protaxvat_p;?></td>
                                            <td><?php echo $value->protaxcst_p;?></td>
                                            <td><?php echo $value->protaxentry_p;?></td>
                                            <td><?php echo $value->taxtotal;?></td>
                                            <td><?php echo $value->PaymentMode;?></td>


                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>