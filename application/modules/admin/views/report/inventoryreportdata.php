<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Inventory Report</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this section, admin can view all Inventory details.</p>
                        </div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box">
<!---############################# search section start #####################################################-->
                            <form method="get" action='<?php echo base_url('admin/report/inventoryreport'); ?>' id='searchfrm'>
                            <div class="col-lg-3">
                            <div class="tbl_input">
                            <select id="filter_by" name="filter_by">
                            <option value="">Filter BY</option>
                            <option value="brand_name" <?php echo ($_GET['filter_by']=='brand_name')?'selected':'';?>>Brand Name</option>
                            <option value="categ_name"<?php echo ($_GET['filter_by']=='categ_name')?'selected':'';?>>Category Name</option>
                            <option value="zeroqty_pro"<?php echo ($_GET['filter_by']=='zeroqty_pro')?'selected':'';?>>All Zero Quantity Products</option>
                            </select>
                            </div>
                            </div>
                            <div class="col-lg-3" id="input_div">
                            <div class="tbl_input">
                            <input type="text" name="searchname" value="" id="searchname" class="valid input_cls">
                            </div>
                            </div>
                            <div class="col-lg-3">
                            <div class="submit_tbl">
                            <input id="validate_frm" type="submit" value="Search" class="btn_button sub_btn">
                            <input type="button" onclick='location.href="<?php echo base_url('admin/report/inventoryreport'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                            </div>
                            </div>
                            </form>
  <!---############################# search section end #####################################################-->
                            </div>
                            </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                               <form method="post" action="">
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
                                </form>
                               <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                 <form method="post" action="">
                                <table class="grid_tbl">
                                    <thead>

                                        <tr>
                                            <th>S:NO</th>
                                            <th>Product Image</th>
                                            <!-- <th>Manufacture</th> -->
                                            <th>Sku Number</th>
                                            <th>Product Name</th>
                                            <th>Category</th>
                                            <th>Brand</th>
                                            <th>Qty</th>
                                            <th>Mrp</th>
                                            <th>Selling Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; 
                                     if(!empty($vieww))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }
                                    foreach ($vieww as $key => $value) {
                                        //p($value);
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value->Image; ?>" height="75" width="50"></td>
                                            <!-- <td><?php echo $value->firstname ;?></td> -->
                                            <td><?php echo $value->SKUNO ;?></td>
                                            <td><?php echo $value->ProductName ;?></td>
                                            <td><?php echo $value->CatName ;?></td>
                                            <td><?php echo $value->BrandName ;?></td>
                                            <td><?php echo $value->proQnty ;?></td>
                                            <td><?php echo $value->productMRP ;?></td>
                                            <td><?php echo $value->SellingPrice ;?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>