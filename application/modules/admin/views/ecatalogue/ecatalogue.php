 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  
  <!-- jQuery Form Validation code -->
   <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
   </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            ecatalogue         : "required",
            desc   : "required",
            
            stype    : "required",
            image1    : "required",
            image2   : "required",
            image3    : "required",
            image4    : "required",
            
            
           /* email:{
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            agree: "required" */
        },
        // Specify the validation error messages
        messages: {
            ecatalogue         : "Required",
            desc   : "Required",
            
            stype      : "Required",
            image1    : "Required",
            image2    : "Required",
            image3    : "Required",
            image4    : "Required",
            
           
            /*password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            email: "Please enter a valid email address",
            agree: "Please accept our policy"*/
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add New Ecatalogue</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add Ecatalogues.</p>
                        </div>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
        <div class="col-lg-12">
                    <div style="text-align:right;">
            <a href="<?php echo base_url() ?>admin/ecatalogue/ecataloguelist"><button>Cancel</button></a>
           </div>
           <!-- <div style="float: left;width: 100%;margin-left: 14px;"><h4>Create New Catalogue</h4></div> -->
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>


         <!-- <div class="sep_box">
          -->                   
            <form id="addCont" action="" method="post" enctype="multipart/form-data" >

                       <!--  </div> -->
                        <div   class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Ecatalogue Name <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">  
                                        <input type="text" name="ecatalogue" value="" id="ecatalogue" />
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                    

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-11">
                                         <div class="tbl_text">Description <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                        <input type="text" name="desc" value="" id="desc" />
                                          </div>
                                    </div>
                                </div>
                            </div>
                           <!--  <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Brands <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                  <select name='brandid' class="brandid" id="mcatidd" onchange="barndcategory($(this));" >
                                  <option value="">Select Brand</option>
                                  <?php foreach ($branddata as $key => $value) {
                                  ?>
                                  <option value="<?php echo $value->id ?>"><?php echo $value->brandname; ?></option>
                                  <?php }?>
                                  </select>

                                          </div>
                                    </div>
                                </div>
                            </div>  -->
                            
                           <!--  <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="sec_cat" id="sec_cat" onchange="">
                                                
                                            </select></div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Ecatalogue Type <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="stype" id="stype" onchange="">
                                              <option value="">Select Type</option>
                                                <option value="1">Type 1 </option>
                                                <option value="2">Type 2 </option>  
                                                
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Image 1 <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <input type="file" name="image1" value="" />
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Image 2 <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <input type="file" name="image2" value="" />
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Image 3 <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <input type="file" name="image3" value="" />
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Image 4 <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <input type="file" name="image4" value="" />
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="row">
                                   <!--  <div class="col-lg-4"></div> -->
                                    <div class="col-lg-12">
                                        <div class="submit_tbl" style="margin-top:10px;">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            
                            <div id="mainidiv">
                            
                        </div>
                        </div>
                       



                       

                       
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">


   $(document).ready(function(){
$("#selectall").click(function(){
        //alert("just for check");
        if(this.checked){
            $('.checkboxall').each(function(){
                this.checked = true;
            })
        }else{
            $('.checkboxall').each(function(){
                this.checked = false;
            })
        }
    });
});
    function selectsubcat(){
     var parentcat=$('#sec_cat').val();
     $.ajax({
        url: '<?php echo base_url()?>admin/ecatalogue/subcategory',
        type: 'POST',
        data: {'parentcat': parentcat},
        success: function(data){
           //alert(data); //return false;
            $('#sec_sub_cat').empty();
            $("#sec_sub_cat").html(data);
           // $('#sec_subcat').multiselect({ 
               // enableClickableOptGroups: true,
               // enableFiltering: true,
              //  includeSelectAllOption: true,

           // });
              
         }
  });
    
   }
   function selectproduct(){
     var parentcat=$('#sec_sub_cat').val();
    // alert(parentcat); return false;
     $.ajax({
        url: '<?php echo base_url()?>admin/ecatalogue/getproduct',
        type: 'POST',
        data: {'parentcat': parentcat},
        success: function(data){
           //alert(data); return false;
            $('#mainidiv').empty();
            $("#mainidiv").html(data);
            //$('#sec_product').multiselect({ 
                //enableClickableOptGroups: true,
                //enableFiltering: true,
               //includeSelectAllOption: true,

            //});
              
         }
  });
    
   }

   function barndcategory(elem){

    //var catid=$('#mcatidd').val();
     //alert(catid);
     var brandid = elem.val();
    
    
     
    $.ajax({
        url: '<?php echo base_url()?>admin/ecatalogue/get_maincategory',
        type: 'POST',
        //dataType: 'json',
        data: {'brandids': brandid},
        success: function(data){
                var option="<option value=''>Select Category</option>";
            $('#sec_cat').empty();
            $("#sec_cat").html(option+data);
        }
    });
    
   }
</script>    

    