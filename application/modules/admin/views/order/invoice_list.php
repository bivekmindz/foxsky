<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Invoice List</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view All Invoice details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                <form method="post" id="delete">
                                 <input type="button" value="Delete" class="del_btn" id="ev_del">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                        <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span></th>
                                          <th>S.NO.</th>
                                          <th>Invoice No.</th>
                                          <th>Order No.</th>
                                          <th>AWB No.</th>
                                          <th>Customer Name</th>
                                          <th>Email</th>
                                          <th>Mobile</th>
                                          <th>PaymentMode</th>
                                          <th>OrderDate</th>
                                          <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; 
                                        if(!empty($invoicedata))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          
                                        foreach ($invoicedata as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><input type='checkbox' name='attdelete[]' class='chkApp' value='<?=$value->id?>'> </td> 
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->invoice_no;?></td>
                                            <td><?php echo $value->OrderNumber;?></td>
                                            <td><?php echo $value->awb_no;?></td>
                                            <td><?php echo ucwords($value->BillName);?></td>
                                            <td><?php echo $value->BillEmail;?></td>
                                            <td><?php echo $value->BillContactNo;?></td>
                                            <td><?php echo $value->PaymentMode;?></td>
                                            <td><?php echo $value->created;?></td>
                                            <td><a href="<?php echo base_url().'admin/order/print_invoice/'.$value->id.'/'.$value->invoice_no;?>" target="_blank"><i class="fa fa-ban"></i></a></td>
                                            
                                        </tr>
                                        <?php } }else {?>
                                      <tr><td colspan="11">Record Not Found</td></tr>
                                      <?php } ?>
                                    </tbody>
                                    
                                </table>
                                </form>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<script type="text/javascript">
    $("#selecctall").click(function () {
      $('.chkApp').each(function() { //loop through each checkbox
        $(this).prop('checked',$('#selecctall').prop("checked"));  //select all checkboxes with class "checkbox1"               
      });
    });

$('#ev_del').click(function(){
  var sub=false;
    $('.chkApp').each(function() { 
          if(this.checked)
           sub=true;
        });
if(sub==true)
  $('#delete').submit();
else
  alert('Please select atleast one');
});
</script>