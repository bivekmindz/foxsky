<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage Dispatch Orders</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view All Dispatch order details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                          <th>S.NO.</th>
                                          <th>Order No.</th>
                                          <th>Customer Name</th>
                                          <th>Email</th>
                                          <th>Mobile</th>
                                          <th>Payment<br>Mode</th>
                                          <th>Payment<br>Status</th>
                                          <!-- <th>ShippingStatus</th> -->
                                          <th>Order Process Date</th>
                                          <th>Action</th>
                                          <!-- <th style="width: 100px;">Revised Mail</th>
                                          <th>Revised Message</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; 
                                        if(!empty($penview))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          
                                        foreach ($penview as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->OrderNumber;?></td>
                                            <td><?php echo $value->ShippingName;?></td>
                                            <td><?php echo $value->Email;?></td>
                                            <td><?php echo $value->ContactNo;?></td>
                                            <td><?php echo $value->PaymentMode;?></td>
                                            <td><?php echo $value->PaymentStatus;?></td>
                                            <!-- <td><?php //echo $value->ShipStatus;?></td> -->
                                            <td><?php echo $value->orderprocessdate;?></td>
                                        <!--     <td><?php echo $value->vendorname;?></td>
 -->                                        <td><a href="<?php echo base_url().'admin/order/orddispatchdetail/'.$value->OrderId;?>"><i class="fa fa-eye"></i></a></td>
                                            <!-- <td><a href="<?php echo base_url().'admin/wallet/ordrevisemail/'.$value->OrderId;?>"><button class="btn_button sub_btn">click here to Send</button></td>
                                            <td><a href="<?php echo base_url().'admin/wallet/ordrevisemobilemsg/'.$value->OrderId;?>"><i style="font-size: 50px;" class="fa fa-mobile"></i></td> -->
                                        </tr>
                                        <?php } }else {?>
                                      <tr><td colspan="11">Record Not Found</td></tr>
                                      <?php } ?>
                                    </tbody>
                                    
                                </table>
                                <!-- <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div> -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>