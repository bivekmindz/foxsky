<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

<script type="text/javascript">
   function  open_div(val)
  {
    if(val=='order'){
      $('#custm_nm').val('');
      $('#select_div').addClass('hide');
      $('#dt_div').addClass('hide');
      $('#input_div').removeClass('hide');
      $('.input_cls').attr({id:"ord_no",name:"ord_no",placeholder:"Enter Order No"});
    }else if(val=='phone'){
       $('#custm_nm').val('');
       $('#dt_div').addClass('hide');
       $('#select_div').addClass('hide');
       $('#input_div').removeClass('hide');
       $('.input_cls').attr({id:"phn_no",name:"phn_no",placeholder:"Enter Phone No",onkeypress:"return isNumberKey(event)"});
    }else if(val=='customer'){
       $('#input_div').addClass('hide');
       $('#dt_div').addClass('hide');
       $('#select_div').removeClass('hide');
    }else if(val=='date'){
       $('#custm_nm').val('');
       $('#input_div').addClass('hide');
       $('#select_div').addClass('hide');
       $('#dt_div').removeClass('hide');
    }
  }
</script>
<?php if(!empty($customer_range)){ foreach ($customer_range as $value) { $value=(object)$value; $customer[]=$value->ShippingName." ".$value->ShipLastName; }} ?>
 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>All Order</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Manufacturer can view All order details.</p>
                        </div>
                    </div>
                     <div class="page_box">
                      <div class="sep_box">

    <div class="col-lg-12">

    <div style="text-align:right;">
    <!--   <form method="post" action="">
        <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
      </form>  -->
    </div>
     <form method="get" action='<?php //echo base_url('admin/order/allorder'); ?>' onsubmit="return searching_frm();">
      <div class="row">
        <div class="col-lg-3">
          <div class="tbl_input">
            <select id="filter_by" name="filter_by" onchange="open_div(this.value)">
            <option value="">Filter BY</option>
            <option value="order" <?php echo ($_GET['filter_by']=='order')?'selected':'';?>>Order NO</option>
            <option value="date"<?php echo ($_GET['filter_by']=='date')?'selected':'';?>>Date</option>
            </select>
            </div>
          </div>
          <div class="col-lg-3 <?php //echo ($_GET['custm_nm']!='')?'':'hide';?>" id="select_div">
          <div class="tbl_input">
            <select id="custm_nm" name="custm_nm">
            <option value="">Select name</option>
            <?php //foreach (array_unique($customer) as $value) { ?>
              <option value="<?php //echo $value;?>"<?php //echo ($_GET['custm_nm']==$value)?'selected':''; ?>><?php //echo $value; ?></option> 
           <?php   //} ?>
            </select>
          </div>
          </div>
          <div class="col-lg-3 hide" id="input_div">
          <div class="tbl_input">
          <input type="text" name="" value="" class="valid input_cls">
          </div>
          </div>
          <div class="hide" id="dt_div">
          <div class="col-lg-3">
          <div class="tbl_input">
          <input name="fromdate" id="fromdate" value="" class="valid">
          </div>
          </div>
          <div class="col-lg-3">
          <div class="tbl_input">
          <input name="todate" id="todate" value="" class="valid" min="">
          </div>
          </div>
          </div>
           <div class="col-lg-3">
        <div class="submit_tbl">
                    <input id="validate_frm" type="submit" value="Search" class="btn_button sub_btn">
                    <input type="button" onclick='location.href="<?php //echo base_url('admin/order/allorder'); ?>"' value="Reset" class="btn_button sub_btn" style="margin-left:3px;">
                </div>
          </div>
      </div>
      </form>
    </div>

  
    </div>
                    </div> 
                    
    <div class="page_box">
    <div class="sep_box1">
    <div class="col-lg-12">
    <div class="gridview">
    <!-- <table id="test" class="grid_tbl"> -->
    <table id="search_filter" class="grid_tbl">
    <thead>
    <tr>
      <th bgcolor='red'>S.NO.</th>
      <th bgcolor='red'>Order No.</th>
      <th bgcolor='red'>Customer Name</th>
      <th bgcolor='red'>Email</th>
      <th bgcolor='red'>Mobile</th>
      <th bgcolor='red'>Payment Mode</th>
      <th bgcolor='red'>Payment Status</th>
    
      <th bgcolor='red'>Amount</th>
      <th bgcolor='red'>OrderDate</th>
      <th bgcolor='red'>Action</th>
    </tr>
    </thead>
    <tbody>
       <?php 
        if(count($record)>0){ 
        $i=1;
        if(!empty($record))
        {
          if($_GET['page'])
          {
          $page = $_GET['page']-1; 
          $i=$page*10+1; 
          }
        }
          
        $j=0;
        foreach ($record as $value) {
            $value=(object)$value;
         ?>
        <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $value->OrderNumber;?></td>
        <td><?php echo $value->ShippingName;?></td>
        <td><?php echo $value->Email;?></td>
        <td><?php echo $value->ContactNo;?></td> 
        <td><?php echo $value->PaymentMode;?></td>
        <td><?php echo $value->PaymentStatus;?></td>
      
        <td>
          <?php $ordamts=number_format($current_amt[$j]+number_format($value->ShippingAmt,1,".","")-number_format($value->CouponAmt,1,".","")-number_format($value->usedwalletamt,1,".",""),1,".","");
              if($ordamts==0.0){ 
                echo number_format($current_amt[$j]+number_format($value->ShippingAmt,1,".","")-number_format($value->CouponAmt,1,".",""),1,".","");
              } else {
                echo $ordamts;
              }
            ?>
        </td>
        <td><?php echo $value->OrderDate;?></td>    
        <td>
          <a href="<?php echo base_url().'admin/order/allorderdetail/'.$value->OrderId.'/'.$value->OrderNumber;?>"><i class="fa fa-eye"></i></a>
        </td>
        </tr>      
        <?php $i++;$j++;} ?>
        <?php } else {?>
        <tr><td colspan="11">Record Not Found</td></tr>
        <?php } ?>

    </tbody>
   </table>
   
    <!-- <div class="pagi_nation">
    <ul class="pagination">
    <?php //foreach ($links as $link) {
    //echo "<li class='newli'>". $link."</li>";
    //} ?>
    </ul>
    </div> -->
  </div>
  </div>
  </div>
  </div>
<script>
$(document).ready(function () {

    function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
                'href': csvData,
                'target': '_blank'
        });
    }

    // This must be a hyperlink
    $("#exp").on('click', function (event) {
        // CSV
        exportTableToCSV.apply(this, [$('table'), 'AllOrder.csv']);
        
        // IF CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
    });
});
</script>

  <script>
 
   $(document).ready(function () {
        
        $("#fromdate").datepicker({
            dateFormat: "yy-mm-d",
            //minDate: 0,
            onSelect: function (date) {
                var date2 = $('#fromdate').datepicker('getDate');
                 $('#todate').datepicker('option', 'minDate', date2);
            }
        });

        $('#todate').datepicker({ dateFormat: "yy-mm-d" });

        
       
        
    });
         function searching_frm(){
          var error=0;
          var field=$('#filter_by').val();
          if(field==""){
            error++
            $('#filter_by').css('border-color','red');
          }else{
            $('#filter_by').css('border-color','');
          }
          if(field=='order'){
            if($('#ord_no').val().search(/\S/) == -1){
              $('#ord_no').css('border-color','red');
               error++
            }else{
              $('#ord_no').css('border-color','');
            }
          }else if(field=='phone'){
            if($('#phn_no').val().search(/\S/) == -1){
              $('#phn_no').css('border-color','red');
               error++
            }else{
              $('#phn_no').css('border-color','');
            }
          }else if(field=='customer'){
            if($('#custm_nm').val()==''){
              $('#custm_nm').css('border-color','red');
               error++;
            }else{
              $('#custm_nm').css('border-color','');
            }
          }else if(field=='date'){
            if($('#fromdate').val()==''){
              $('#fromdate').css('border-color','red');
              error++
            }else{
              $('#fromdate').css('border-color','');
            }
            if($('#todate').val()==''){
              $('#todate').css('border-color','red');
              error++
            }else{
              $('#todate').css('border-color','');
            }
          }
           //alert(error);
          if(error==0){
           return true;
          }else{
            return false;
          }
        }
function isNumberKey(evt)
 {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
  return true;
 }
   
</script>