<?php //p($detialrecord);?>
<style>
  .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;
    overflow-x: hidden;
}
  * html .ui-autocomplete {
  height: 100px;
  }
  </style>
<?php 
if(!empty($detialrecord))
{
    $coupanc= $detialrecord[0]->CouponCode;
    $coupana= $detialrecord[0]->CouponAmt;
    if(!empty($coupanc))
    {
       $coupcode='<p style="color:green">'.$coupanc.'</p>';
    }
    else
    {
        $coupcode='<p style="color:red"> No Coupon </p>';
    }
    if( !empty($coupana))
    {
         $coupamnt='<p style="color:green">'.$coupana.'</p>';
      
    }
    else
    {
        $coupamnt='<p style="color:red"> 0.00 </p>';
    }
}
else
{ 
    $coupanc='';
    $coupanp='';
    $coupamnt='';
    $coupcode='';
}
?>
<!-- <div class="hide" id="add_comnt_div">
<div class="add_coment_canc">
<h3>Add Comment Here</h3>
<form>

<textarea id="comnt"></textarea>

<a href="javascript:void(0);" class="comen_su" onclick="return update_order_pro();">Submit</a>
<a href="javascript:void(0);" class="comen_ca" onclick="window.open('<?php echo base_url(uri_string()); ?>','_self')">Cancel</a>
</form>
</div>
<div class="overlay"></div> 
</div> -->
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">
          <form action="" method="post" enctype="multipart/form-data" id="form_id"> 
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                    <h2>View Order Detail</h2>
                    </div>
                    <div class="page_box">
                    <div class='flashmsg'>
                        <?php echo validation_errors(); ?>
                        <?php
                          if($this->session->flashdata('message')){
                            echo $this->session->flashdata('message'); 
                          }
                        ?>
                    </div>
                    
                    <div id="qtymsg"></div>
                    <div class="col-lg-12">
                    <h4>Order Number : <?php echo $record->OrderNumber;?></h4>
                   <table id="test" class="grid_tbl">
                   <thead>
                    <tr>
                  <!--     <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span></th> 
                       -->
                      <th bgcolor='red'>S.NO.</th>
                      <th bgcolor='red'>Order<br>Status</th>
                        <th bgcolor='red'>Product Image</th>
                      <th bgcolor='red'>Product Name</th>
                      <th bgcolor='red'>Quantity</th>
        
                      <th bgcolor='red'>MRP</th>
                      <th bgcolor='red'>Unit Price</th>
                      <th bgcolor='red' colspan="5">Amount</th>
                      <!-- <th bgcolor='red'>Delete</th> -->
                    </tr>
                    </thead>
                    <tbody>
                   <?php $i=1; 
                     if(isset($detialrecord) && count($detialrecord)>0){
                     foreach ($detialrecord as $value) { ;                      
                      $row_id=$value->OrderproId;
                      $pro_id=$value->ProId;
                       ?>
                        <tr id="<?php echo $row_id.','.$pro_id; ?>">	
                        <!--  <td><input type='checkbox' name='proApp[]' class='chkApp' value='<?=$value->OrderproId?>'> </td> -->  
                                          
                        <td><?php echo $i;?></td>
                        <td><?php echo ucfirst($value->ShipStatus);?></td>
                        
                        <td><img src="<?php echo base_url();?>images/thumimg/<?php echo $value->ProImage;?>" style="vertical-align:middle; width:80px;"></td> 
                        <!-- <td><a href="<?php echo base_url().'product/'.str_replace(' ', '-', $value->ProName).'-'.$value->ProId; ?>.html" target="_blank"><?php echo $value->ProName;?></a<input type="hidden" name="proid[<?php //echo $value->OrderproId;?>]" value="<?php //echo $value->ProId;?>"></td> -->
                        <td><?php echo $value->ProName;?></td>
                        <td>
                    
                      <?php echo $value->ProQty;?>
                      </td>
                                        
                        <?php $amt=number_format($value->base_prize*$value->ProQty,1,".",""); ?>
                    
                        <?php $tin_tax_amt=number_format($amt * $value->protaxvat_p / 100,1,".","");
                        $cst_tax_amt=number_format($amt * $value->protaxcst_p / 100,1,".","");
                        $entry_tax_amt=number_format($amt * $value->protaxentry_p / 100,1,".","");?></td>
                        <td><?php echo number_format($value->ProPrice,1,".","");?></td>
                            
                          <?php 
                          $subtotal+=number_format($value->base_prize*$value->ProQty,1,".","");
                          $tax+=$total_tax_amt=number_format($tin_tax_amt+$cst_tax_amt+$entry_tax_amt,1,".",""); 
                          $totproprice=$amt+$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
                          //$t_coup[]=get_partialdiscountpercentamt($totproprice,$value->couponper);
                          //$t_wall[]=get_partialdiscountpercentamt($totproprice,$value->walletper);
                          ?>

                        <td>
                         <!--  <input type="text" readonly ordproid="<?=$value->OrderproId?>"  ordid="<?=$value->OrderId?>" catid="<?=$value->CatId?>" brandid="<?=$value->brandId?>" cityid="<?=$value->citygroupid?>" proid="<?=$value->ProId?>" onchange="return updatepri($(this))" value="<?php echo $value->VenSellPrice;?>" id="unitprice<?=$value->OrderproId?>"  name="unitprice[<?=$value->OrderproId?>]" style="width:50px;"> -->
                         <?php echo number_format($value->base_prize,1,".","");?>
                        </td>
                        <td colspan="5"><?php echo number_format(($value->base_prize * $value->ProQty),1,".","");?></td>
                        <!-- <td><a href="javascript:void(0);" onclick="delete_prod(<?php echo $row_id;?>,<?php echo (($value->TotalAmt) - ($value->VenSellPrice * $value->OrdQty));?>);">Delete</a>
                        </td> -->
                        </tr>

                        <input type="hidden" name="oldstatus[<?=$value->OrderproId?>]" value="<?php echo $value->OrdShipping;?>">
                        <input type="hidden" name="proid[<?=$value->OrderproId?>]" value="<?php echo $value->ProId;?>">
                        <input type="hidden" name="venid[<?=$value->OrderproId?>]"  value="<?php echo $value->VendorID;?>">
                        <input type="hidden" name="totalstock[<?=$value->OrderproId?>]" value="<?php echo $value->pomq;?>">

                        <?php $i++;} }?>
                        <?php   
                        //p($t_coup);
                        //p($t_wall);
                        //$to_coup=number_format(array_sum($t_coup),1,".","");
                        //$to_wall=number_format(array_sum($t_wall),1,".",""); 
                          $to_coup=number_format($value->CouponAmt,1,".","");
                          $to_wall=number_format($value->usedwalletamt,1,".","");
                        ?>
                        <tr>
                        <td colspan="3">&nbsp;</td>
                        <td colspan="5" style="border-right:solid 0px #ccc;"><b>Sub Total :</b></td>
                        <td colspan="4"><?php echo number_format($subtotal,1,".","");?></td></tr>
   <tr>
                        <td colspan="3">&nbsp;</td>
                        <td colspan="5" style="border-right:solid 0px #ccc;"><b>Coupon :</b></td>
                        <td colspan="4"><?php echo number_format( $to_coup,1,".","");?></td></tr>



                       <!--  <tr><td colspan="9">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Shipping :</b></td>
                        <td colspan="4"><?php echo number_format($value->ShippingAmt,1,".","");?></td></tr>
                        <tr><td colspan="9">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Tax :</b></td>
                        <td colspan="4"><?php echo number_format($tax,1,".",""); ?></td></tr>
                        <?php if(!empty($value->CouponAmt)){ ?>
                        <tr><td colspan="9">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Coupon Amount (-) :</b></td>
                        <td colspan="4"><?php echo $to_coup; ?></td></tr>
                        <?php } ?>
                        <?php if(!empty($value->usedwalletamt) && $value->usedwalletamt!=0.0){ ?>
                        <tr><td colspan="9">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Wallet Pay (-) :</b></td>
                        <td colspan="4"><?php echo $to_wall; ?></td></tr>
                        <?php } ?> -->
                        <tr><td colspan="3">&nbsp;</td><td colspan="5" style="border-right:solid 0px #ccc;"><b>Final Total :</b></td>
                        <td colspan="4"><?php $ordamts=number_format(number_format($subtotal,1,".","")+number_format($value->ShippingAmt,1,".","")+number_format($tax,1,".","")-$to_coup-$to_wall,1,".","");
                        if($ordamts<=0.0){
                          echo number_format(number_format($subtotal,1,".","")+number_format($value->ShippingAmt,1,".","")+number_format($tax,1,".","")-$to_coup,1,".",""); }
                        else {
                          echo $ordamts;
                          } ?></td></tr>
     
    </tbody>
   </table>
</form>
</div>
  </div>
  
  <!-- <button id="add_more">Add More Product</button> -->
  <div id="pro_serch_div" class="hide">
  <form id="add_pro_form">
  <input type="hidden" name="city" value="<?php echo $value->BillCity; ?>">
  <input type="hidden" name="ord_id" value="<?php echo $value->OrderId; ?>">
  <input type="hidden" name="state" value="<?php echo $value->BillState; ?>">
     <select name="prod" id="prod" onchange="get_color(this.value);get_img(this.value);">
    <?php if(!empty($product_list))
    { 
      echo "<option value=''></option>";
      foreach ($product_list as $value) {
        echo "<option value='".$value->proid."'>".$value->proname."</option>";
      }
     
    } ?>
     
    </select>
        <div id="pro_img_data" class="hide" style="display:inline; margin-left:20px;"></div>

        <div id="pro_feature_div" class="hide">
          <select id="color" name="color" onchange="get_color_age(this.value);" class="hide">
            <option value="">Choose Color</option>
          </select>
          <select id="pro_size" name="pro_size" class="hide">
           <option value="">Choose Size</option>
          </select>
          
          <input id="proqty" name="proqty">
        
        </div>
     <input type="button" id="add_pro_sub" value="Add">
  
  </form>  
  </div>
  <!-- <button style="float: right;" id="invoice1232" onclick="window.open('<?php echo base_url(uri_string()); ?>/invoice','_blank')">invoice print</button> -->
  
   <div class="heading">
    <h3>Shipping Details:-</h3>
    <?php if(!empty($record->userremark)){ ?>
    <h5 style="color: red;">Remark:- <?php echo $record->userremark;?></h5>
    <?php } ?>                
    </div>
        <form method="post" action="">
    <div class="form">
    <div class="formCol">
    <label>Name</label>
    <span>: <?php echo $record->ShippingName;?></span>
    </div>
    <div class="formCol">
    <label>Mobile</label>
    <span>: <?php echo $record->ContactNo;?></span>
    </div>
    <div class="formCol">
    <label>Email</label>
    <span>: <?php echo $record->Email;?></span>
    </div>
    <div class="formCol">
    <label>Shipping Address</label>
    <span>: <?php echo $record->ShippingAddress;?></span>
    </div>
    <div class="formCol">
    <label>Billing Address</label>
    <span>: <?php echo $record->BillAddress.', '.$record->BillCity.', '.$record->state_name.' = '.$record->BillPin;?></span>
    </div>
    <div class="formCol">
    <label>City</label>
    <span>: <?php echo $record->City;?></span>
    </div>
    <div class="formCol">
    <label>State</label>
    <span>: <?php echo $record->shipstate_name;?></span>
    </div>
    <div class="formCol">
    <label>Pin Code</label>
    <span>: <?php echo $record->Pin;?></span>
    </div>
    <div class="formCol">
    <label>Payment Mode</label>
    <span>: <?php echo $record->PaymentMode;?></span>
    </div>
    <div class="formCol">
    <label>Payment Status</label>
    <span>: <?php echo $record->PaymentStatus;?></span>
    </div>
    <div class="formCol">
    <label> Order Date</label>
    <span>: <?php echo date('d m Y h:i:s A',strtotime($record->OrderDate));?></span>
    </div>
    <div>
    </div>
   </div>
   </form>
</section>
<script>
 $('.update_product' ).on("keyup",function() {

   var val = this.value; 
   var name = this.name;
   var State_id="<?php echo $record->BillState;?>";
   var city="<?php echo $record->BillCity;?>";
   var order_id="<?php echo  $record->OrderId; ?>";
   var url="<?php echo base_url(); ?>";
   var pro_id = $(this).closest('tr').attr('id').split(',');
    if(val>0 && isNaN(val)==false)
   {
     $.ajax({
                type:"post",
                url:url+"admin/order/update_product_val",
                data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id,city_vl:city},
                success:function(data){
                    //alert(data);
                    window.location.href=window.location.href;
                }
        });
   }
   
 });
 function delete_prod(id,pz)
 { 
   var order_id="<?php echo  $record->OrderId; ?>";
    $.ajax({
                type:"post",
                url:site_url+"admin/order/delete_product/"+id+"/"+pz+"/"+order_id,
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data);return false;
                    window.location.href=window.location.href;
                }
        });
 }
 $('#add_more').on("click",function(){
   //$( this ).text('Remove');
   //$(this).attr("id","remove")
  $('#pro_serch_div').removeClass('hide');
 
return false;
    $("#prod").select2({
      placeholder: "Select Product",
      allowClear: true
    });
  
 });
 /*$('#remove').on('click',function(){

   //$( this ).text('Add More Product');
   //$(this).attr("id","add_more")
  
 
 });*/
function get_color(val)
{
   $.ajax({
                type:"post",
                url:site_url+"admin/order/get_color/"+val,
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data.trim());
                    if(data.trim()==0)
                    { 
                      
                      $('#pro_feature_div').removeClass('hide');
                      $('#color').addClass('hide');
                      $('#pro_size').addClass('hide');
                    }else{
                      $('#pro_feature_div').removeClass('hide');
                      $('#color').removeClass('hide');
                      $('#color').html(data);
                      $('#pro_size').addClass('hide');
                      
                      
                    }
                    
                }
        });
}


function get_img(val)
{
   $.ajax({
                type:"post",
                url:site_url+"admin/order/get_img/"+val,
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data);
                    if(data==0)
                    { 
                      $('#pro_img_data').addClass('hide');
                    }else{
                      $('#pro_img_data').removeClass('hide');
                      $('#pro_img_data').html(data);

                    }
                    
                }
        });
}

function get_color_age(val)
{

  $.ajax({
                type:"post",
                url:site_url+"admin/order/get_color_age/"+val.split(',')[1],
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data.trim());
                    if(data.trim()==0)
                    { 
                      $('#pro_size').addClass('hide');
                    }else{
                       $('#pro_size').removeClass('hide');
                       $('#pro_size').html(data.split('#')[1]);
                    }
                    
                }
        });
}
$('#add_pro_sub').on("click",function(){
  var err=0;
  if($('#prod').val()=="")
  { 
    err++;
    $('#prod').css("border-color","red");
  }
  if($('#color').attr('class')=="")
  { 
    if($('#color').val()=="")
    {
      err++;
     $('#color').css("border-color","red");
    }else{
      $('#color').css("border-color","");
    }
    
  }
  if($('#pro_size').attr('class')=="")
  { 
    if($('#pro_size').val()=="")
    {
      err++;
     $('#pro_size').css("border-color","red");
    }else{
      $('#pro_size').css("border-color","");
    }
  }
    if($('#proqty').val()=="" || isNaN($('#proqty').val())==true || $('#proqty').val()<1)
    {
      err++;
     $('#proqty').css("border-color","red");
    }else{
      $('#proqty').css("border-color","");
    }
  
  if(err==0)
  { 
    var order_id="<?php echo  $record->OrderId; ?>";
    var formdata=$("#add_pro_form").serialize();
    //alert(formdata); return false;
    
    $.ajax({
      type:"post",
      url:site_url+"admin/order/add_product/"+order_id,
      data:formdata,
      success:function(rep)
      { 
         //console.log(rep);
         //alert(rep);return false;
         window.location.href=window.location.href;
      }

    });
  }
  
  
})
</script>
<script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
    </script>
<script type="text/javascript">
function updateqty(val){
var qnty       = val.val();
var productid  = val.attr('proid');
var cityid  = val.attr('cityid');
var catid   =val.attr('catid');
var brndid  =val.attr('brandid');
var stateid =$('#stateid').val();
var cstid =$('#cstid').val();
var tinid =$('#tinid').val();
var ordid =val.attr('ordid');
var ordproid =val.attr('ordproid');
var unitprc    = $('#unitprice'+ordproid).val();
//alert(unitprc);
//alert(qnty);
//return false;
$("#qtymsg").hide();
if($.isNumeric(qnty)==false){
 $("#qtymsg").show();
 $("#qtymsg").html('<span class="qtyinvalid">Invalid quantity entered!</span>');
 return false;  
}
$.ajax({
   url:'<?php echo base_url();?>admin/order/updateorderqty',
   type: 'POST',
   data: {qnty:qnty,proid:productid,cityid:cityid,catid:catid,brndid:brndid,stateid:stateid,cstid:cstid,tinid:tinid,ordid:ordid,ordproid:ordproid,unitprc:unitprc},
   success:function(data){
    console.log(data);
    //alert(data);return false;
    window.location.href=window.location.href;
    return false;
    }
  });
}


function updatepri(val){
var unitprc    = val.val();
var productid  = val.attr('proid');
var cityid  = val.attr('cityid');
var catid   =val.attr('catid');
var brndid  =val.attr('brandid');
var stateid =$('#stateid').val();
var cstid =$('#cstid').val();
var tinid =$('#tinid').val();
var ordid =val.attr('ordid');
var ordproid =val.attr('ordproid');
var qnty      = $('#OrdQty'+ordproid).val();
//alert(qnty);
//alert(unitprc); return false;

$("#qtymsg").hide();
if($.isNumeric(qnty)==false){
 $("#qtymsg").show();
 $("#qtymsg").html('<span class="qtyinvalid">Invalid quantity entered!</span>');
 return false;  
}
$.ajax({
   url:'<?php echo base_url();?>admin/order/updateorderqty',
   type: 'POST',
   data: {qnty:qnty,proid:productid,cityid:cityid,catid:catid,brndid:brndid,stateid:stateid,cstid:cstid,tinid:tinid,ordid:ordid,ordproid:ordproid,unitprc:unitprc},
   success:function(data){
    //console.log(data);
    //alert(data);return false;
    window.location.href=window.location.href;
    return false;
    }
  });

}
$(document).ready(function(){
  $('#submitmanu').click(function(){
     var ttl_check=0;
     var ttl_uncheck=0;
    $('.chkApp').each(function()
    { 
        ttl_check++;
        if($(this).prop("checked") == false)
        {
           ttl_uncheck++;
        }
        
    })
    if(ttl_check==ttl_uncheck)
    {
        alert('Please check atleast one Product');
    }else if($('#chstatus').val()==""){
      $('#chstatus').css('border-color','red');
         
    }else{
      $("#form_id").submit();
    }
  
  })

});

</script>