<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
        
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage Pending Order details</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view single Pending order details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                              <form method="post" id="form_id">
                              <h3 style="float:right;width:auto;">Order Number :- <?php echo $pendetailview[0]->OrderNumber; ?></h3>
                               <div style="float:left;width:auto;"> <select style="margin-right: 20px;padding:3px 5px;" id="chstatus" name="chstatus">
                                          <option value="">select status</option>
                                          <option value="onprocess">On Process</option>
                                          <option value="dispatch">Dispatch</option>
                                          <option value="hold">Hold</option>
                                          <option value="cancel">Cancel</option>
                                           <option value="delivered">Delivered</option>
                                        </select>
                                      <input type="button" name="submitmanu" id="submitmanu" value="Submit" class="btn_button sub_btn" style="border:none;padding:4px 10px;" />
                                      <i class="fa fa-spinner fa-spin hide" style="font-size: 21px;"></i>
                                </div> 
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <?php if(!empty($pendetailview[0]->userremark)){ ?>
                                <div><p style="font-size: 15px;padding: 3px 0px 6px 0px;"><span style="font-weight: bold;">Order Remark :- </span><span style="color: #186BB2;font-weight: bold;"><?php echo $pendetailview[0]->userremark; ?></span></p></div>
                                <?php } ?>
                                <div style="width:100%;overflow-x:auto;">
                                
                                <!-- <div style="margin-bottom: 5px;"><input type="button" onclick="del_multipro()" name="delmultipro" id="delmultipro" value="Delete Multiple Products" class="btn_button sub_btn" style="border:none;padding:4px 10px;" /></div> -->
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                           <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span></th> 
                                          <th>S.NO.</th>
                                          <th>Order Status</th>
                                       
                                          <th>Product Image</th>
                                          <th>Product Name</th>
                                          <th>Quantity</th>
                                         
                                       
                                          <th>MRP</th>
                                          <th>Unit Price</th>
                                          <th colspan="5">Amount</th>
                                          <!-- <th>Delete</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=0; //p($pendetailview);
                                      $allproid=array(); 
                                      if(!empty($pendetailview)){
                                         foreach ($pendetailview as $key => $value) { 
                                        $i++;
                                        $row_id=$value->StatusOrderId; 
                                        //p($value->productid);
                                        array_push($allproid, $value->productid);
                                        ?>
                                        <tr>
                                             <td><input type='checkbox' name='proApp' class='chkApp<?=$value->OrderId?>' data-proid='<?=$value->ProId?>' data-qty='<?=$value->ProQty?>' value='<?=$value->StatusOrderId?>'></td>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->ShipStatus;?></td>
                                          
                                            <td><img src="<?php echo base_url(); ?>images/thumimg/<?php echo $value->ProImage;?>" style="vertical-align:middle; width:80px;"></td>
                                            <td><?php echo $value->ProName;?></td>
                                            <td><?php echo $value->ProQty;?>
                                            <!-- <?php echo $value->OrdQty;?> -->
                                               <input type="hidden" id="stateid" value="<?=$value->State?>">
							                                 <input type="hidden" id="cstid" value="<?=$value->cstid?>">
							                                 <input type="hidden" id="tinid" value="<?=$value->tinid?>">
                                            <input type="hidden" ordproid="<?=$value->StatusOrderId?>" ordid="<?=$value->OrderId?>" catid="<?=$value->CatId?>" brandid="<?=$value->brandId?>" cityid="<?=$value->citygroupid?>" proid="<?=$value->ProId?>" value="<?php echo $value->ProQty;?>" class="update_product11" name="OrdQty" id="qty<?=$value->StatusOrderId?>" style="width:50px;" min="1" oldqy="<?php echo $value->ProQty;?>" ordstatus="<?=$value->ShipStatus?>" stock="<?php echo $value->pomq; ?>"> 
                                            </td>
                                          
                                             <?php $amt=number_format($value->base_prize*$value->ProQty,1,".",""); ?>
                                           <!--  <td><?php echo $tin_tax_amt=number_format($amt * $value->protaxvat_p / 100,1,".","");?></td>
                                            <td><?php echo $cst_tax_amt=number_format($amt * $value->protaxcst_p / 100,1,".","");?></td>
                                            <td><?php echo $entry_tax_amt=number_format($amt * $value->protaxentry_p / 100,1,".","");?></td> -->
                                             <?php $tin_tax_amt=number_format($amt * $value->protaxvat_p / 100,1,".","");
                                             $cst_tax_amt=number_format($amt * $value->protaxcst_p / 100,1,".","");
                                             $entry_tax_amt=number_format($amt * $value->protaxentry_p / 100,1,".","");?></td>
                                            <td><?php echo number_format($value->ProPrice,1,".","");?></td>
                                            <td><?php echo number_format($value->base_prize,1,".","");?>
                                            <!-- <input type="text" ordproid="<?=$value->StatusOrderId?>"  promapid="<?=$value->OrderproId?>" catid="<?=$value->CatId?>" brandid="<?=$value->brandId?>" cityid="<?=$value->citygroupid?>" proid="<?=$value->ProId?>" onkeyup="return updatepri($(this))" onkeypress="return isNumberKey(event)" value="<?php echo number_format($value->base_prize,1,".","");?>" id="<?=$value->StatusOrderId?>"  name="unitprice[<?=$value->StatusOrderId?>]" style="width:50px;"> -->
                                            </td>
                                            <td colspan="5"><?php echo number_format($value->base_prize*$value->ProQty,1,".","");?></td>
                                            <?php 
                                              $t_subtotal[]=number_format($value->base_prize*$value->ProQty,1,".","");
                                              $t_tax[]=number_format($tin_tax_amt+$cst_tax_amt+$entry_tax_amt,1,".","");
                                              $totproprice=$amt+$tin_tax_amt+$cst_tax_amt+$entry_tax_amt;
                                              //$t_coup[]=get_partialdiscountpercentamt($totproprice,$value->couponper);
                                              //$t_wall[]=get_partialdiscountpercentamt($totproprice,$value->walletper);
                                            ?>
                                            <!-- <td><a href="javascript:void(0);" onclick="delete_prod(<?php echo $value->StatusOrderId.','.$value->ProId.','.$value->ProQty;?>);">Delete</a></td> -->
                                        </tr>
                                        <!-- <input type="hidden" value="<?php echo $value->OrdQty;?>" name="OrdQty[<?=$value->StatusOrderId?>]"> -->
                                        <input type="hidden" name="oldstatus[<?=$value->StatusOrderId?>]" value="<?php echo $value->ShipStatus;?>">
                                        <input type="hidden" name="proid[<?=$value->StatusOrderId?>]" value="<?php echo $value->ProId;?>">
                                        <input type="hidden" name="venid[<?=$value->StatusOrderId?>]"  value="<?php echo $value->VendorID;?>">
                                        <input type="hidden" name="totalstock[<?=$value->StatusOrderId?>]" value="<?php echo $value->pomq;?>">

                                        <?php } ?>
                                        <tr>
                                        <?php
                                            $ship=number_format($value->ShippingAmt,1,".","");
                                            $to_subtotal=array_sum($t_subtotal);
                                            $to_tax=array_sum($t_tax);
                                            //$to_coup=number_format(array_sum($t_coup),1,".","");
                                            //$to_wall=number_format(array_sum($t_wall),1,".","");
                                            $to_coup=number_format($value->CouponAmt,1,".","");
                                            $to_wall=number_format($value->usedwalletamt,1,".","");

                                        ?>
                                      <td colspan="3">&nbsp;</td><td colspan="6" style="border-right:solid 0px #ccc;"><b>Sub Total :</b></td>
                                      <td colspan="4"><?php echo number_format($to_subtotal,1,".","");?></td></tr>
                                        <tr>
                        <td colspan="3">&nbsp;</td>
                        <td colspan="5" style="border-right:solid 0px #ccc;"><b>Coupon :</b></td>
                        <td colspan="4"><?php echo number_format( $to_coup,1,".","");?></td></tr>
                                     <!--  <tr><td colspan="9">&nbsp;</td><td colspan="6" style="border-right:solid 0px #ccc;"><b>Shipping :</b></td><td colspan="4"><?php echo $ship;?></td></tr>
                                      <tr><td colspan="9">&nbsp;</td><td colspan="6" style="border-right:solid 0px #ccc;"><b>Tax :</b></td><td><?php echo number_format($to_tax,1,".",""); ?></td></tr>
                                      <?php if(!empty($value->CouponAmt)){ ?>
                                      <tr><td colspan="9">&nbsp;</td><td colspan="6" style="border-right:solid 0px #ccc;"><b>Coupon Amount (-) :</b></td>
                                      <td colspan="4"><?php echo $to_coup; ?></td></tr>
                                      <?php } ?>
                                      <?php if(!empty($value->usedwalletamt) && $value->usedwalletamt!=0.0){ ?>
                                      <tr><td colspan="9">&nbsp;</td><td colspan="6" style="border-right:solid 0px #ccc;"><b>Wallet Pay (-) :</b></td>
                                      <td colspan="4"><?php echo $to_wall; ?></td></tr>
                                      <?php } ?> -->
                                      <tr><td colspan="3">&nbsp;</td><td colspan="6" style="border-right:solid 0px #ccc;"><b>Final Total :</b></td><td colspan="4"><?php $ordamts=number_format(number_format($to_subtotal,1,".","")+$ship+number_format($to_tax,1,".","")-$to_coup-$to_wall,1,".","");
                                      if($ordamts<=0.0){
                                        echo number_format(number_format($to_subtotal,1,".","")+$ship+number_format($to_tax,1,".","")-$to_coup,1,".",""); }
                                      else {
                                        echo $ordamts;
                                        } ?></td></tr>
                                         <?php } else {?>
                                        <tr><td colspan="18">Record Not Found</td></tr>
                                        <?php } ?>
                                    </tbody>
                                    
                                </table>
                                </form>
                                </div>
                            </div>
                        </div>

                    </div>
     <!--  add more product section -->
      <input type="hidden" id="allproids" class="allproids" value="<?php print_r($allproid);?>" > 
                    <!-- <button id="add_more">Add More Product</button> -->
                    <div id="pro_serch_div" class="hide">
                    <div id="alreadypro" style="color: red;text-align: center;margin: 10px;"></div>
                    <form id="add_pro_form">
                    <input type="hidden" name="city" value="<?php echo $user_dtls->BillCity; ?>">
                    <input type="hidden" name="ord_id" value="<?php echo $user_dtls->OrderId; ?>">
                    <input type="hidden" name="state" value="<?php echo $user_dtls->BillState; ?>">
                    <input type="hidden" name="userid" value="<?php echo $user_dtls->UserId; ?>">
                       <select name="prod" id="prod" onchange="get_color(this.value);get_img(this.value);" class="product_list">
                      <?php if(!empty($product_list))
                      { 
                        echo "<option value=''></option>";
                        foreach ($product_list as $value) {
                          echo "<option value='".$value->proid.'_'.$value->catid."'>".$value->proname."</option>";
                        }
                       
                      } ?>
                       
                      </select>
                          <div id="pro_img_data" class="hide" style="display:inline; margin-left:20px;"></div>

                          <div id="pro_feature_div" class="hide">
                            <select id="color" name="color" onchange="get_color_age(this.value);" class="hide">
                              <option value="">Choose Color</option>
                            </select>
                            <select id="pro_size" name="pro_size" class="hide">
                             <option value="">Choose Age</option>
                            </select>
                            
                            <input type="number" id="proqty" name="proqty" min="1" value="1">
                          
                          </div>
                       <input type="button" id="add_pro_sub" value="Add">

                    </form>  
                    </div>
    <!--  add more product section -->
                </div>
            </div>
            
        </div>
    </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
      var ordid="<?php echo $pendetailview[0]->OrderId; ?>";
        if(this.checked) { // check select status
            $('.chkApp'+ordid).each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp'+ordid).each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});


$(document).ready(function(){
  $('#submitmanu').click(function(){
     var ordid="<?php echo $pendetailview[0]->OrderId; ?>";
     var ttl_check=0;
     var ttl_uncheck=0;
     var counter=0;
     var countrow=0;
     var ttl_ids=[];
    $('.chkApp'+ordid).each(function()
    { 
        ttl_check++;
        if($(this).prop("checked") == false)
        {
           ttl_uncheck++;
        }
        
    })
    if(ttl_check==ttl_uncheck)
    {
        alert('Please check atleast one Product');
    }else if($('#chstatus').val()==""){
      $('#chstatus').css('border-color','red');
         
    }else{
            $('#submitmanu').hide();
            $('.fa-spin').removeClass('hide');
            $('.chkApp'+ordid).each(function(){ 

            if($(this).prop("checked") == true)
            {
               countrow++;
            }
            })
              
         $('.chkApp'+ordid).each(function(){ 
                       
                        if($(this).prop("checked") == true)
                        { 
                            
                            //alert(counter);
                           var id=$(this).attr('value');
                           var current_qty=$('#qty'+id).val();
                           var old_qty=$('#qty'+id).attr('oldqy');
                           var old_stat=$('#qty'+id).attr('ordstatus');
                           var total_stock=$('#qty'+id).attr('stock');
                         //  alert(id;);
                           $.ajax({ 
                            type:'post',
                            url:'<?php echo base_url()?>admin/order/uptodate_order_satatus',
                            data:{'statsid':id,'flag':$('#chstatus').val(),'cur_qty':current_qty,'old_qty':old_qty,'old_stat':old_stat,'total_stock':total_stock},
                            success:function(rep)
                            { 
                              //alert(rep.trim())
                                counter++;
                               if(rep.trim()!=0 && rep.trim()!='')
                               { 
                                  ttl_ids.push(rep.trim());
                               }
                               if(parseInt(counter)==parseInt(countrow) && ttl_ids.length>0)
                                { 
                                   //insert_invoice_data(ttl_ids);
                                   document.location.href=window.location.href;
                              //    alert("ddddddd");
                                }if(parseInt(counter)==parseInt(countrow) && ttl_ids.length==0)
                                { 
                                   //  alert("sssssssss");
                                   document.location.href=window.location.href;
                                }
                              
                            }

                           });
                           
                        }

                    }) 
    }
  
  })

   $('#add_more').on("click",function(){
   $('#pro_serch_div').removeClass('hide');
      $(".product_list").select2({
      placeholder: "Select a Product",
      allowClear: true
})
  
 });

});
function insert_invoice_data(arr)
{ 
  var orderid="<?php echo $this->uri->segment(4); ?>";
  $.ajax({ 
          type:'post',
          url:'<?php echo base_url()?>admin/order/insert_invoice',
          data:{'statsid':arr,order_id:orderid},
          success:function(rep)
          { 
             //alert(rep.trim())
             document.location.href=window.location.href;
          }

         });
}
function updateqty(val){
var qnty       = val.val();
var productid  = val.attr('proid');
var cityid  = val.attr('cityid');
var catid   =val.attr('catid');
var brndid  =val.attr('brandid');
var stateid =$('#stateid').val();
var cstid =$('#cstid').val();
var tinid =$('#tinid').val();
var ordid =val.attr('ordid');
var ordproid =val.attr('ordproid');
var unitprc    = $('#unitprice'+ordproid).val();

$("#qtymsg").hide();
if($.isNumeric(qnty)==false){
 $("#qtymsg").show();
 $("#qtymsg").html('<span class="qtyinvalid">Invalid quantity entered!</span>');
 return false;  
}
$.ajax({
   url:'<?php echo base_url();?>admin/order/updateorderqty',
   type: 'POST',
   data: {qnty:qnty,proid:productid,cityid:cityid,catid:catid,brndid:brndid,stateid:stateid,cstid:cstid,tinid:tinid,ordid:ordid,ordproid:ordproid,unitprc:unitprc},
   success:function(data){
    console.log(data);
    //alert(data);return false;
    window.location.href=window.location.href;
    return false;
    }
  });
}


function updatepri(val){
var newprice = val.val();
var statsid  = val.attr('id');
var promapid =val.attr('promapid');
  if(newprice.search(/\S/) == -1 || newprice==0)
  {
    $('#'+statsid).css('border-color','red');
  }else{
    $('#'+statsid).css('border-color','');
    $.ajax({
     url:'<?php echo base_url();?>admin/order/update_base_prc',
     type: 'POST',
     data: {newprice:newprice,statsid:statsid},
     success:function(data){
      //console.log(data);
      //alert(data);return false;
      window.location.href=window.location.href;
      //return false;
      }
    });
  }
}
function get_color(val)
{ 
  $('#color').val("");
  $('#pro_size').val("");
   $.ajax({
                type:"post",
                url:site_url+"admin/order/get_color/"+val,
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data.trim());
                    if(data.trim()==0)
                    { 
                      
                      $('#pro_feature_div').removeClass('hide');
                      $('#color').addClass('hide');
                      $('#pro_size').addClass('hide');
                    }else{
                      $('#pro_feature_div').removeClass('hide');
                      $('#color').removeClass('hide');
                      $('#color').html(data);
                      $('#pro_size').addClass('hide');
                      
                      
                    }
                    
                }
        });
}

function get_img(val)
{
    $.ajax({
                type:"post",
                url:site_url+"admin/order/get_img/"+val,
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data);
                    if(data==0)
                    { 
                      $('#pro_img_data').addClass('hide');
                    }else{
                      $('#pro_img_data').removeClass('hide');
                      $('#pro_img_data').html(data);

                    }
                    
                }
        });
}

function get_color_age(val)
{

  $.ajax({
                type:"post",
                url:site_url+"admin/order/get_color_age/"+val.split(',')[1],
                //data:{qty:val,prod_id:pro_id,Stateid:State_id,orderid:order_id},
                success:function(data){
                    //alert(data.trim());
                    if(data.trim()==0)
                    { 
                      $('#pro_size').addClass('hide');
                    }else{
                       $('#pro_size').removeClass('hide');
                       $('#pro_size').html(data.split('#')[1]);
                    }
                    
                }
        });
}
$('#add_pro_sub').on("click",function(){
  var err=0;
  var allproid=$('#allproids').val();
  var chprodid=$('#prod').val();
  var residd = chprodid.split("_");
  var aaproidd = allproid.indexOf(residd[0]);

  if($('#prod').val()=="")
  { 
    err++;
    $('#prod').css("border-color","red");
  }
  if($('#color').attr('class')=="")
  { 
    if($('#color').val()=="")
    {
      err++;
     $('#color').css("border-color","red");
    }else{
      $('#color').css("border-color","");
    }
    
  }
  if($('#pro_size').attr('class')=="")
  { 
    if($('#pro_size').val()=="")
    {
      err++;
     $('#pro_size').css("border-color","red");
    }else{
      $('#pro_size').css("border-color","");
    }
  }
    if($('#proqty').val()=="" || isNaN($('#proqty').val())==true || $('#proqty').val()<1)
    {
      err++;
     $('#proqty').css("border-color","red");
    }else{
      $('#proqty').css("border-color","");
    }
  
  if(err==0)
  { 
     /*alert(allproid);
     alert(chprodid);
     alert(residd[0]);
     alert(aaproidd);*/
     if(aaproidd==-1){
      
        var order_id="<?php echo  $user_dtls->OrderId; ?>";
        var formdata=$("#add_pro_form").serialize();
        //alert(formdata); return false;
        
        $.ajax({
          type:"post",
          url:site_url+"admin/order/add_product/"+order_id,
          data:formdata,
          success:function(rep)
          { 
             //alert(rep);
             //console.log(rep);
             //return false;
             window.location.href=window.location.href;
          }

        });
     } else {
        $('#alreadypro').html('Product Allready Exist!');
     }
     
  }
  
  
})
function delete_prod(id,proid,qty)
 { 
         $.ajax({
                type:"post",
                url:site_url+"admin/order/delete_product/"+id+"/"+proid+"/"+qty,
                success:function(data){
                  //alert(data);
                    window.location.href=window.location.href;
                }
        });
 }

function del_multipro()
 { 
    var ordid="<?php echo $pendetailview[0]->OrderId; ?>";
    var val = [];
    var valproid = [];
    var valqty = [];
      $('.chkApp'+ordid).each(function(i){
        if($(this).prop("checked") == true)
        { 
          val[i] = $(this).val();
          valproid[i] = $(this).attr('data-proid');
          valqty[i] = $(this).attr('data-qty');
        }
      });
    
      if(val==''){
        alert('Please check atleast one Product');
      } else {
        $.ajax({
            type:"post",
            url:site_url+"admin/order/delmultipro",
            data:{statusordid:val,productid:valproid,quatity:valqty},
            success:function(data){
              //alert(data);
              //console.log(data);
              window.location.href=window.location.href;
            }
        });
      }
 }

 function isNumberKey(evt)
 {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
  return true;
 }
</script>