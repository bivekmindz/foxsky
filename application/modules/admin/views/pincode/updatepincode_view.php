<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Update Pincode</h2>
   
    </div>
        <div class="page_box">
        <div class="sep_box">
                            <div class="col-lg-12">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/pincode/listPincode"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
        <form action="" method="post" enctype="multipart/form-data" onsubmit="return pinvalidate();">
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Enter Pincode<span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input type="text" name="pincodename" id="pincodename" value="<?php echo $pinrow->pin_pincode;?>" maxlength="6"><span class="pin_msg"></span>
                </div>
            </div>
        </div>
        </div>
           <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>
      </div>
          
    </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
    function pinvalidate()
      {
        pin = $('#pincodename').val();
       if(pin=="")
       {
       $(".pin_msg").html("Please enter pincode.").css({ 'color': 'red' });
       return false;
       } else {
      $(".pin_msg").html(" ").css({ 'color': 'red' });
      
      }

      }  
  

    </script>
   