<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

<!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            brandid      : "required",
            "mcatidd[]"  : "required",
           /// "catidd[]"  : "required",
            //"subcatidd[]"  : "required",     
        },
        // Specify the validation error messages
        messages: {
            brandid  : "Brand is required",
            "mcatidd[]"  : "Main Category is required",
           // "catidd[]"  : "Sub-Category is required",
            //"subcatidd[]"  : "Child Sub-Category is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Add Brand Master</h2>
   
    </div>
        <div class="page_box" style="overflow: inherit;">
         <div class="sep_box">
                            <div class="col-lg-12">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/category/viewbrandmaster"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
        <form action="" id="addCont" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Brand <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select name="brandid" id="brandid" >
                <option value="">Select Brand</option>
                <?php foreach ($vieww as $key => $value) {
                 ?>
                  <option value="<?php echo $value->id ?>"><?php echo $value->brandname ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>
        </div>
        
        <div class="sep_box">
        <div class="col-lg-6">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">select Main Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><select name="mcatidd[]" id="mcatidd" multiple="multiple">
                <?php foreach ($parentCatdata as $key => $value) {
                 ?>
                  <option value="<?php echo $value->catid ?>"><?php echo $value->catname ?></option>
                 <?php }?>
                </select></div>
            </div>
        </div>
        </div>
        </div>
        
      <!--   <div class="sep_box"> 
        <div class="col-lg-6" style="display:none" id="childcat">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Select Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select name="catidd[]" id="catidd" multiple="multiple">
                </select>
                 </div>
            </div>
        </div>
        </div>
        </div> -->

       <!--  <div class="sep_box">
        <div class="col-lg-6" style="display:none" id="subchildcat">
          <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Select Child Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select name="subcatidd[]" id="subcatidd" multiple="multiple">
                </select>
                 </div>
            </div>
        </div>
        </div>
       
        </div> -->
     
   
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">

    $(document).ready(function() {
       $('#mcatidd').multiselect({ 
            enableClickableOptGroups: true,
            enableFiltering: true,
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
        });
    });

    $("#mcatidd").change(function(){
        var catid=$('#mcatidd').val();
            //alert(catid);

            if(catid==0){
                $('#childcat').hide();
            }else{
                $('#childcat').show();
            }
             
            $.ajax({
                url: '<?php echo base_url()?>admin/category/branddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                   
                      $("#catidd").empty();
                      $("#catidd").attr("multiple","multiple");
                      $("#catidd").multiselect('destroy');
                      $("#catidd").append(data);
                      $('#catidd').multiselect({ 
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        enableCaseInsensitiveFiltering: true,
                      });
                }
            });
    });


    $("#catidd").change(function(){
        var catid=$('#catidd').val();
            //alert(catid);

            if(catid==0){
                $('#subchildcat').hide();
            }else{
                $('#subchildcat').show();
            }
             
            $.ajax({
                url: '<?php echo base_url()?>admin/category/subbranddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                    
                      $("#subcatidd").empty();
                      $("#subcatidd").attr("multiple","multiple");
                      $("#subcatidd").multiselect('destroy');
                      $("#subcatidd").append(data);
                      $('#subcatidd').multiselect({ 
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        enableCaseInsensitiveFiltering: true,
                      });
                }
            });
    });
    
        function childcategory(){
            var catid=$('#mcatidd').val();
            //alert(catid);

            if(mcatidd==0){
                $('#childcat').hide();
            }else{
                $('#childcat').show();
            }
             
            $.ajax({
                url: '<?php echo base_url()?>admin/category/branddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                     //alert(data);return false;
                    //var  option_brand = '<option value="">Select Category</option>';
                    //$('#catidd').empty();
                    //$("#catidd").append(data);
                    /*$('#catidd').multiselect({
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                    });*/

                      $("#catidd").empty();
                      $("#catidd").attr("multiple","multiple");
                      $("#catidd").multiselect('destroy');
                      $("#catidd").append(data);
                      $('#catidd').multiselect({ 
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        enableCaseInsensitiveFiltering: true,
                      });
                }
            });
            
           }
   
    </script>
 