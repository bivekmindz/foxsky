<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
 <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            bname         : "required",
        },
        // Specify the validation error messages
        messages: {
            bname         : "Brand Name is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>
<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Brand</h2>
   
    </div>
        <div class="page_box">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/category/viewbrand"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        <form action="" id="addCont" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Brand Name <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input type="text" name="bname" id="bname" value="<?php echo $vieww->brandname?>"></div>
            </div>
        </div>
        </div>
         <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Image</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <input type="hidden" name="oldimage" value="<?php echo $vieww->brandimages;?>">
             <img height="50" width="50" src="<?php echo base_url()?>imagefiles/brandimages/<?php echo $vieww->brandimages;?>"></td>

                <input id="video_img" name="video_img" type="file" />    
                </div>
            </div>
        </div>
        </div>
       
        </div>
          
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
   