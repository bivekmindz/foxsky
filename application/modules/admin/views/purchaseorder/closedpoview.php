<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Closed Purchase Orders</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view All closed Purchase order details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                            <div style="text-align:right;">
                                <form method="post" action="">
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
                                </form>  
                            </div>
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <th>S.NO.</th>
                                          <th>PO Number</th>
                                          <th>Vendor Name</th>
                                          <th>Total Product</th>
                                          <th>Created On</th>
                                          <th>Modified On</th>
                                          <th>View Detail</th>
                                          <th>Print PO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0;
                                         if(!empty($record))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }   
                                        foreach ($record as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->ponumber;?></td>
                                            <td><?php echo $value->firstname." ".$value->lastname;?></td>
                                            <td><?php echo $value->total;?></td>
                                            <td><?php echo $value->po_date;?></td>
                                            <td><?php echo $value->po_modify;?></td>
                                            <td><a href="<?php echo base_url().'admin/poorder/closepolistdetail/'.$value->poid;?>"><i class="fa fa-print"></i></a></td>
                                            <td><a href="<?php echo base_url();?>admin/order/printpo/<?php echo $value->poid;?>" target="_blank"><i class="fa fa-print"></i></a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>