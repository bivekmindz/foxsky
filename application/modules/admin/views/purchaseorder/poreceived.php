<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Update Purchase Order Received</h2>
    </div>
      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can Update Purchase Order Received details.</p>
                        </div>
                    </div>
        <div class="page_box">
<form method="post" enctype="multipart/form-data">
<div class="form">
<div class="col-lg-12">
    <p style="font-weight: bold;font-size: 15px;">Manufacturer Name : <?php echo $porecdata->firstname.' '.$porecdata->lastname;?></p>
    <p style="font-weight: bold;font-size: 15px;">PO Number : <?php echo $porecdata->ponumber;?></p>
</div>
<span id="message"></span>

  <div class="sep_box">
    <div class="col-lg-6">
      <div class="row"><div class="col-lg-4">
          <div class="tbl_text">Supplier Invoice Number</div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="pospinvoiceno" id="pospinvoiceno" /></span></div></div></div>
      </div>
  
    <div class="col-lg-6">
      <div class="row"><div class="col-lg-4">
          <div class="tbl_text">Discount (If any)</div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="podiscount" id="podiscount" /></span></div></div></div>
      </div>
  </div>    
<div class="sep_box">      
      <div class="col-lg-6">
      <div class="row"><div class="col-lg-4"><div class="tbl_text">Net Amount Exclusive of VAT/CST</div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="ponetamt" id="ponetamt" /></span></div></div></div>
      </div>  
  
    <div class="col-lg-6">
      <div class="row"><div class="col-lg-4"><div class="tbl_text">CST/VAT@ 2%/5%/12.5%</div></div>
        <div class="col-lg-8"><div class="tbl_input"><span><input type="text" name="pocst" id="pocst" /></span></div>
          </div></div>
      </div>
</div>      
<div class="sep_box">      
      <div class="col-lg-6">
      <div class="row">
        <div class="col-lg-4"><div class="tbl_text">Total Amount inclusive of VAT/CST</div></div>
        <div class="col-lg-8">
          <div class="tbl_input"><span><input type="text" name="pototamt" id="pototamt" /></span></div></div></div></div>  
      <div class="col-lg-6">
        <div class="row"><div class="col-lg-4"><div class="tbl_text">Supplier Invoice Image</div></div>
          <div class="col-lg-8"><div class="tbl_input"><span><input type="file" name="pospbillimg" id="pospbillimg" /></span></div>
            </div></div>
      </div>
  </div>

<div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input type="submit" name="posubmit" id="posubmit" class="btn_button sub_btn" value="Submit">
                </div>
            </div>
        </div>
        </div>

        </div>


</form> </div>

</div>
</div>

