<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Closed Purchase Order details</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view single closed Purchase order details according to manufacturer.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div id="div1" style="width:100%;overflow-x:auto;">
                                <form method="post">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <th>S.No.</th>
                                          <th>Vendor</th>
                                          <th>SKUNumber</th>
                                          <th>Product Image</th>
                                          <th>Product Name</th>
                                          <th>updated purchase Quantity</th>
                                          <th>Total Stock</th>
                                          <th>Size</th>
                                          <th>Color</th>
                                          <th>Product MRP</th>
                                          <th>old Manufacture Price</th>
                                          <th>New Manufacture Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; foreach ($record as $key => $value) {  //p($value);?>
                                        <tr>
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo $value->firstname." ".$value->lastname;?></td>
                                            <td><?php echo $value->po_skuno;?></td>
                                            <td><img src="<?php echo $value->po_proimg;?>" style="vertical-align:middle; width:80px;"></td>
                                            <td><?php echo $value->po_proname;?></td>
                                            <td><?php echo $record2[$i]->postock; ?></td>
                                            <td><?php echo $value->pomq; ?></td>
                                            <td><?php echo $value->po_ordsize;?></td>
                                            <td><?php echo $value->po_ordcolor;?></td>
                                            <td><?php echo number_format($value->prodmrp,1,".","");?></td>
                                            <td><?php echo $value->po_finalprice;?></td>
                                            <td><?php echo $record2[$i]->poprice;?></td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                    
                                </table>
                                </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>    