<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage Purchase Order details</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view single Purchase order details according to manufacturer.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div id="div1" style="width:100%;overflow-x:auto;">
                                <form method="post">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select All</span></th>
                                          
                                          <th>Vendor</th>
                                          <th>SKUNumber</th>
                                          <th>Product Image</th>
                                          <th>Product Name</th>
                                          <th>Total Order Quantity</th>
                                          <th>Total Stock</th>
                                          <th>Size</th>
                                          <th>Color</th>
                                          <th>Manufacture Price</th>
                                          <!-- <th>Manufacture Price</th>
                                          <th>Manufacture Total Price</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($vieww)){ $i=0; foreach ($vieww as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                        
                                            <td><input type='checkbox' name='proApp[]' class='chkApp' value='<?=$value->po_proid?>'></td>
                                            
                                            
                                            <td><select name="vendid<?=$value->po_proid?>" id="vendid<?=$value->po_proid?>">
                                            <?php foreach ($manuvieww as $k => $val) { ?>
                                                <option value="<?php echo $val->id;?>" <?php if($value->po_vendorid==$val->id){ echo "selected"; } ?>><?php echo $val->firstname." ".$val->lastname;?></option>
                                            <?php } ?>    
                                            </select></td>
                                            <td><?php echo $value->po_skuno;?></td>
                                            <td><img src="<?php echo $value->po_proimg;?>" style="vertical-align:middle; width:80px;"></td>
                                            <td><?php echo $value->po_proname;?></td>
                                            <td><?php echo $value->rqty;?></td>
                                            <td><?php echo $value->pomq; ?></td>
                                            <td><?php echo $value->po_ordsize;?></td>
                                            <td><?php echo $value->po_ordcolor;?></td>
                                            <td><?php echo $value->po_finalprice;?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                        <td colspan="2">&nbsp;</td>
                                        
                                        <td><input type="submit" name="submitpo" id="submitpo" value="Generate PO" /></td>
                                        
                                        </tr>
                                        <?php } else { ?>
                                        <tr><td colspan="11" style="text-align: center;">No Record Found</td></tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                    
                                </table>
                                </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

     <script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});

        


        function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
}
    </script>