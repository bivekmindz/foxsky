<div class="wrapper">
<?php $this->load->view('helper/nav');?> 
  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage Purchase Orders</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can view All Purchase order details.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                 <form method="post" action="">
                            <div style="text-align:right;">
                                  <input style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" type="submit" value="Download Excel" name="newsexcel">
                                 
                            </div>
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <div style="width:100%;overflow-x:auto;">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                          <th><span style='border:0px color:blue; cursor:pointer;' id='selAll'><input type="checkbox" id="selecctall"/></br>Select for<br>PO excel</span></th>
                                          <th>S.NO.</th>
                                          <th>PO Number</th>
                                          <th>Vendor Name</th>
                                          <th>Total<br>Product</th>
                                          <th>Created On</th>
                                          <th>Modified On</th>
                                          <th>Action</th>
                                          <th>Print PO</th>
                                          <th>PO Received</th>
                                          <th>Closed PO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0;
                                         if(!empty($record))
                                           {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $i=$page*50; 
                                            }
                                          }   
                                        foreach ($record as $key => $value) { $i++; //p($value);?>
                                        <tr>
                                            <td><input type='checkbox' name='proApp[]' class='chkApp' value='<?=$value->poid?>'></td>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->ponumber;?></td>
                                            <td><?php echo $value->firstname." ".$value->lastname;?></td>
                                            <td><?php echo $value->total;?></td>
                                            <td><?php echo $value->po_date;?></td>
                                            <td><?php echo $value->po_modify;?></td>
                                            <td><a href="<?php echo base_url().'admin/order/polistdetail/'.$value->poid;?>"><i class="fa fa-pencil"></i></a></td>
                                            <td><a href="<?php echo base_url();?>admin/order/printpo/<?php echo $value->poid;?>" target="_blank"><i style="font-size: 20px;" class="fa fa-print"></i></a></td>
                                            <td>
                                            <a href="<?php echo base_url().'admin/poorder/poreceived/'.$value->poid;?>"><i style="font-size: 20px;" class="fa fa-pencil-square-o"></i></a>
                                            <!-- <?php if($value->po_updateqtystatus==0){ ?>    
                                            <a href="<?php echo base_url().'admin/poorder/poupdateqty/'.$value->poid;?>"><input style="color: #676767;" type="button" value="Update Inventory" /></i></a>
                                            <?php } else { ?>
                                            Received
                                            <?php } ?> -->
                                            </td>
                                            <!-- <td><a href="<?php echo base_url();?>admin/poorder/closedpo/<?php echo $value->poid;?>"><button>Close PO</button></a></td> -->
                                            <td><input type="button" style="background: #1870BB;border: none;color: #fff;padding: 5px 10px;font-size: 13px;" value="Close PO" onClick="javascript:location.href = '<?php echo base_url();?>admin/poorder/closedpo/<?php echo $value->poid;?>';" /></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    
                                </table>
                                <div class="pagi_nation">
                                <ul class="pagination">
                                <?php foreach ($links as $link) {
                                //p($link); //exit();
                                echo "<li class='newli'>". $link."</li>";
                                } ?>
                                </ul>
                                </div>
                                </div>
                                </form> 
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<script type="text/javascript">
        $(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click 
        if(this.checked) { // check select status
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.chkApp').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
    
});
</script>