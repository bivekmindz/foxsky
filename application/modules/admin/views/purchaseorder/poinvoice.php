<?php //p($record);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo admin_url();?>fontawsome/css/font-awesome.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
body{
	margin:0; 
	padding:0; 
	-webkit-print-color-adjust: exact;
	-moz-print-color-adjust: exact;
	-khtml-print-color-adjust: exact;
	-o-print-color-adjust: exact;
}
.container{max-width:800px; margin:auto; border: dashed 1px #ccc; float:left; padding:10px 10px;}
.header{width:100%; height:40px; background:#F1F1F1;}
.wrapper{width:100%; margin:auto;}
.section{width:100%;}
.top_header{width:100%; padding: 10px 0px; float: left;}
.logo_left{width:44%; float:left;}
.invoice_right{width:44%; float:right; padding: 0 42px;}
.invoice_right h1{font-size:26px; color:#000; text-align:right; font-family: Arial, Helvetica, sans-serif;}
.top_header_bottom{width: 100%;
    padding: 20px 0px;
    float: left;}
.invoice_to{width:48%; float:left;}
.invoice_to p{font-size:15px; margin-bottom: -14px; margin-left: 6px;}
.invoice_to_right{width:52%; float:left; background:#058ED0; margin-top: 17px;}
.date{width:165px;float:left; margin-left: 16px; padding: 5px 0px;}
.date img{width:26px;}
.date b{color:#fff; font-size:16px;}
.invoice_no{width:154px;float:left; padding: 5px 0px;}
.invoice_no b{color:#fff; font-size:16px;}
.payment{width:40%;  background:#058ED0; }
.payment h1{font-size:16px; text-align:left; color:#fff; padding:5px 5px;}
.invoice_too {width: 53%;float: left; margin-top: -11px;}
.invoice_too p{font-size:15px; margin-bottom: -14px; margin-left: 6px;}
.thanku{padding: 43px 8px; font-size:15px; color:#000;}
.invoice_too_right{width:45%; float:left; /*height:74px;*/ margin-top: -41px;}
.payment-right{width: 260px;
    background: #058ED0;
    float: right;
    margin: 0px 0px;}
.payment-right h3{font-size:14px; text-align:left; color:#fff; padding:5px 21px;}
.signature{width:200px; float:right;  margin: 58px 53px;}
.signature h1{font-size:15px; color:#000;text-align:center;}
.signature p{font-size:14px; color:#000; text-align:center;}
.footer_head{width:100%; height:30px; background:#058ED0; float:left; }
.footer{width:100%;  background:#F1F1F1; float:left; }
.left-text{width:520px; float:left; font-size:14px; color:#fff; padding:5px 5px;}
.right-text{width:200px; float:right; font-size:14px; color:#fff; padding:5px 5px;}
.left-text p{color:#000; font-size:13px;margin-left: 1px;}
.right-text p{color:#000; font-size:13px; }
button {
    background: #1870BB;
    border: none;
    color: #fff;
    padding: 5px 10px;
    font-size: 13px;
    position: absolute;
    left: 822px;
    top: 0;
}
.date i{color:#fff; font-size: 15px; padding: 0px 13px;}
.invoice_no i{color:#fff; font-size: 15px; padding: 0px 13px;}
.top_header_bottom1{width:100%;}

.product-category table {
    width: 100%;
    margin: 0 auto !important;
    font-size: 12px;
    font-family: arial;
    padding: 0px;
    margin: 0px;
    cellpadding: 0px;
}
.product-category{
	width: 100%;
    float: left;
    height: auto;
    padding: 0px;
    margin: 0px;}
.product-category table tr {
    padding: 5px 0px !important;
}
.product-category table tr th {
    padding: 8px 0px !important;
    font-size: 12px !important;
    margin: 0px !important;
    text-align: center !important;
    width: 10%;
}
.product-category  table tr td {
    padding: 5px 0px !important;
    text-align: center !important;
}	
</style>

</head>

<body>
<button id="prinvoice">print PO</button>
<div class="container">
<div class="header"></div>
<div class="wrapper">
<div class="section">
<div class="top_header">
<div class="logo_left"><img src="<?php echo base_url();?>images/logo3.png" alt="logo"/></div><!--end of logo-->
<div class="invoice_right"><h1>Purchase Order</h1></div><!--end of invoice-->
</div><!-- end of top header -->

<div class="top_header_bottom">
<?php //p($record);?>
<div class="invoice_to">
<p>PURCHASE ORDER TO :</p>
<p><b><?php echo ucwords($record3->firstname)." ".ucwords($record3->lastname);?></b></p>
<p>Email : <?php echo $record3->compemailid;?> <br/>Contact no. : <?php echo $record3->contactnumber;?></p>
</div><!-- end of invoice to -->

<div class="invoice_to_right">
<div style="width:360px; float:left;  margin-left: 62px;">
<span class="date"><i class="fa fa-calendar" aria-hidden="true"></i><br />
<b>DATE</b><br /><span style="color:#fff; font-size:13px;"><?php echo $record3->po_date;?></span></span>

<span class="invoice_no"><i class="fa fa-file-text-o" aria-hidden="true"></i><br />
<b>PO NO.</b><br /><span style="color:#fff; font-size:13px;"><?php echo $record3->ponumber;?></span></span>
</div>
<!-- <span class="invoice_no"><i class="fa fa-inr" aria-hidden="true"></i><br />
<b>TOTAL PRICE</b><br /><span style="color:#fff; font-size:13px;">Rs. <?php echo round($record->TotalAmt); ?></span></span> -->
</div><!-- end of invoice right -->
</div><!-- end of header-bottom -->

<div class="product-category">
<table  cellpadding="0" cellspacing="0">
<tbody><tr style="background: #058ED0;color: #fff;"><th >S.No.</th><th style="padding:10px 5px;width:300px; text-align:left!important;width:300px;font-size: 16px;">Product Name</th><th style="padding:7px 0px; font-size: 16px;">SKU No.</th><th style="padding:7px 0px; font-size: 16px;">Quantity</th><th style="padding:7px 0px;font-size: 16px;">Product MRP</th><th style="padding:7px 0px;font-size: 16px;">Unit Price</th><th style="padding:7px 0px;font-size: 16px;">Total</th></tr>

<?php $j=1; $i=0; foreach ($record as $key => $value) {  //p($value);?>

<tr style="background: #<?php echo ($i%2==0)?'dedede':'ccc';?>;color: #000;"><td style="padding:21px 5px;width:300px; text-align:left;"><?php echo $j++;?><td style="padding:21px 5px;width:300px; text-align:left!important;"><?php echo $value->po_proname;?></td><td style="padding:7px 0px; text-align:center;"><?php echo $value->po_skuno;?></td><td style="padding:7px 0px; text-align:center;"><?php echo $record2[$i]->postock;?></td><td style="padding:7px 0px; text-align:center;"><?php echo number_format($value->prodmrp,1,".","");?></td><td style="padding:7px 0px; text-align:center;">Rs. <?php 
                                                if($record2[$i]->poprice==0){ 
                                                    echo $value->po_finalprice; 
                                                } else {
                                                    echo $record2[$i]->poprice;
                                                }
                                            ?></td><td style="padding:7px 0px; text-align:center;">Rs. <?php 
                                                if($record2[$i]->poprice==0){
                                                    $grandtotal1[]= $value->po_finalprice*$record2[$i]->postock;
                                                    $grandtotal2[]=0;
                                                    echo number_format($value->po_finalprice*$record2[$i]->postock,1,".",""); 
                                                } else {
                                                    $grandtotal2[]= $record2[$i]->poprice*$record2[$i]->postock;
                                                    $grandtotal1[]=0;
                                                    echo number_format($record2[$i]->poprice*$record2[$i]->postock,1,".","");
                                                }
                                            ?></td></tr>




<?php $i++; } ?>
<?php //p($grandtotal1);  p($grandtotal2);?>
<tr style="background: #058ED0;color: #fff;"><td colspan="7" style="padding:2px !important;"></td></tr>
</tbody>
</table>
</div>

<div class="top_header_bottom1">
 <div style="width:100%; float:right;">
<!-- <div class="invoice_too">
<div class="thanku">Thank You for business with Us !</div>
</div>

<div class="invoice_too_right">
<table style="border-collapse:collapse;margin:0px; height:40px;float:left;width:400px;font-size:13px;">
<tbody>
<tr style="color: #000;"><td style="padding:10px 5px;width:300px; text-align:center;width:300px;">SUB TOTAL</td><td style="padding:7px 0px; text-align: left;">Rs. <?php echo round($value->subtotal);?></td></tr>

<tr style="color: #000;"><td style="padding:10px 5px;width:300px; text-align:center;width:300px;">SHIPPING</td><td style="padding:7px 0px; text-align: left;">Rs. <?php echo round($value->ShippingAmt);?></td></tr>

<tr style="color: #000;"><td style="padding:10px 5px;width:300px; text-align:center;width:300px;">TAX</td><td style="padding:7px 0px; text-align: left;">Rs. <?php echo round($value->Tax);?></td></tr>
</tbody>
</table>

</div> -->

<div class="payment-right"><h3>Final Total<span style="float:right; margin-top:0px; color:#fff; font-size:14px; padding:0px 36px;">Rs. <?php if(array_sum($grandtotal1)==0) { echo number_format(array_sum($grandtotal2),1,".",""); } else { echo number_format(array_sum($grandtotal1),1,".",""); }?></span></h3></div>
</div>  <!--paymentright-->
<div style="width:100%; float:left;">
<div class="signature">
<h1>MindzShop.com</h1>
<p>Signature</p>
</div>
</div>

</div><!-- end of section -->

<!--<div class="footer_head">
<div class="left-text">TERMS</div>-->
<!-- <div class="right-text">ADDRESS</div> -->
</div>
<!--<div class="footer">
<div class="left-text"><p>1. Goods once sold will not be returned or exchange in any condition.<br>                    
2. Interest @ 18%p.a. will be charged if amount not paid in due date.<br>                    
3. All Disputes Subject to Delhi jurisdiction.<br>                   
4. Warranty: standard as per the Manufacturer. </p></div>
<!-- <div class="right-text"><p style="margin-left:0px;">D-56 New Delhi.</p></div> -->
</div>--><!--end of footer -->

</div><!--end of wrapper -->
</div><!--end of container -->

<script type="text/javascript">
$(document).ready(function() {
    $("#prinvoice").click(function() {
        $("#prinvoice").css('display', 'none');
        window.print();
    });
});
</script>

</body>
</html>

