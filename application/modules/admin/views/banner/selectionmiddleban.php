<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Home Middle Banners</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add, edit and delete Middle banners.</p>
                        </div>
                    </div>
                   
                    <div class="page_box">
                    <div class="flashmsg">  
                           <?php  echo $this->session->flashdata('message1'); ?>
                    </div>
                        <div class="sep_box1">
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                <form action="<?php echo base_url();?>admin/bannergallery/Middlebanner" method="post" enctype="multipart/form-data">
                                    <thead>
                                        <tr>
                                            <th>Banner Image</th>
                                            <th>Image Url</th>
                                            <th>Banner Heading</th>
                                            <th>Banner Sub-Heading</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><div style="margin-top: 15px;"><input id="banimg1" name="banimg1" type="file" /></div>
                                            <img style="margin: 10px 0px 5px 5px;" height="60px" width="60px" src="<?php echo base_url();?>assets/webapp/images/<?php echo $banone->t_imgname;?>">
                                            </td>
                                            <td><input id="imgurl1" name="imgurl1" type="url" value="<?php echo $banone->t_path;?>" /></td>
                                            <td><input id="imghead1" name="imghead1" type="text" value="<?php echo $banone->t_heading;?>" /></td>
                                            <td><input id="imgshead1" name="imgshead1" type="text" value="<?php echo $banone->t_subheading;?>" /></td>
                                            <td><input id="submitone" name="submitone" type="submit" value="Save" class="btn_button sub_btn xls_download" /><br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </form>
                                </table>
                            </div>
                        </div>                      
                    </div>



                    <div class="page_box">
                    <div class="flashmsg">  
                           <?php  echo $this->session->flashdata('message2'); ?>
                    </div>
                        <div class="sep_box1">
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                <form action="<?php echo base_url();?>admin/bannergallery/Middlebanner" method="post" enctype="multipart/form-data">
                                    <thead>
                                        <tr>
                                            <th>Banner Image</th>
                                            <th>Image Url</th>
                                            <th>Banner Heading</th>
                                            <th>Banner Sub-Heading</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><div style="margin-top: 15px;"><input id="banimg2" name="banimg2" type="file" /></div>
                                            <img style="margin: 10px 0px 5px 5px;" height="60px" width="60px" src="<?php echo base_url();?>assets/webapp/images/<?php echo $bantwo->t_imgname;?>">
                                            </td>
                                            <td><input id="imgurl2" name="imgurl2" type="url" value="<?php echo $bantwo->t_path;?>" /></td>
                                            <td><input id="imghead2" name="imghead2" type="text" value="<?php echo $bantwo->t_heading;?>" /></td>
                                            <td><input id="imgshead2" name="imgshead2" type="text" value="<?php echo $bantwo->t_subheading;?>" /></td>
                                            <td><input id="submittwo" name="submittwo" type="submit" value="Save" class="btn_button sub_btn xls_download" /><br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </form>
                                </table>
                            </div>
                        </div>                      
                    </div>




                    <div class="page_box">
                    <div class="flashmsg">  
                           <?php  echo $this->session->flashdata('message3'); ?>
                    </div>
                        <div class="sep_box1">
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                <form action="<?php echo base_url();?>admin/bannergallery/Middlebanner" method="post" enctype="multipart/form-data">
                                    <thead>
                                        <tr>
                                            <th>Banner Image</th>
                                            <th>Image Url</th>
                                            <th>Banner Heading</th>
                                            <th>Banner Sub-Heading</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><div style="margin-top: 15px;"><input id="banimg3" name="banimg3" type="file" /></div>
                                            <img style="margin: 10px 0px 5px 5px;" height="60px" width="60px" src="<?php echo base_url();?>assets/webapp/images/<?php echo $banthree->t_imgname;?>">
                                            </td>
                                            <td><input id="imgurl3" name="imgurl3" type="url" value="<?php echo $banthree->t_path;?>" /></td>
                                            <td><input id="imghead3" name="imghead3" type="text" value="<?php echo $banthree->t_heading;?>" /></td>
                                            <td><input id="imgshead3" name="imgshead3" type="text" value="<?php echo $banthree->t_subheading;?>" /></td>
                                            <td><input id="submitthree" name="submitthree" type="submit" value="Save" class="btn_button sub_btn xls_download" /><br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </form>
                                </table>
                            </div>
                        </div>                      
                    </div>




                    <!-- <div class="page_box">
                    <div class="flashmsg">  
                           <?php  //echo $this->session->flashdata('message4'); ?>
                    </div>
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                <form action="<?php //echo base_url();?>admin/bannergallery/Middlebanner" method="post" enctype="multipart/form-data">
                                    <thead>
                                        <tr>
                                            <th>Banner Image</th>
                                            <th>Image Url</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><div style="margin-top: 15px;"><input id="banimg4" name="banimg4" type="file" /></div>
                                            <img style="margin: 10px 0px 5px 5px;" height="60px" width="60px" src="<?php //echo base_url();?>assets/webapp/images/<?php //echo $banfour->t_imgname;?>">
                                            </td>
                                            <td><input id="imgurl4" name="imgurl4" type="url" value="<?php //echo $banfour->t_path;?>" /></td>
                                            <td><input id="submitfour" name="submitfour" type="submit" value="Save" class="btn_button sub_btn" /><br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </form>
                                </table>
                            </div>
                        </div>                      
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>


