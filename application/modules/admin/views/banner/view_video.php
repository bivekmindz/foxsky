<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

<!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
    .submit_tbll { width: 100%; float: left; text-align: left; }
    .submit_tbll input { border: none;padding: 9px 14px;  }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            h_video  : "required",
        },
        // Specify the validation error messages
        messages: {
            h_video  : "Video file is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>
<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Home Video</h2>
                        <?php echo validation_errors(); ?> 
    
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add, edit and delete Home Video.</p>
                        </div>
                    </div>
                    <form method="post" id="addCont" enctype="multipart/form-data">
                    <div class="page_box">
                    <div class='flashmsg'><?php echo $this->session->flashdata('messag'); ?></div>
                    <div style="text-align: center;color: red;"><?php echo $this->session->flashdata('er_msg'); ?></div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Add Video File <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="h_video" name="h_video" type="file" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="submit_tbll">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    </form>



                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Video</th>
                                            <th>Updated by</th>
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   <?php $i=0; foreach ($view_video as $key => $value) { $i++; ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><a href="<?php echo base_url().'assets/webapp/img/'.$value->v_filename; ?>" target="_blank"><?php echo $value->v_filename;?></a></td>
                                            <td><?php if(empty($value->v_modifiedby)){ echo $value->u_createdby; } else { echo $value->u_modifiedby; } ?></td> 
                                            <td><?php if(empty($value->v_modifiedby)){ echo $value->v_createdon; } else { echo $value->v_modifiedon; } ?></td>
                                            <td><a href="<?php echo base_url()?>admin/bannergallery/updatevideo/<?php echo $value->v_id?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/bannergallery/deletevideo/<?php echo $value->v_id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/bannergallery/statusvideo/<?php echo $value->v_id.'/'.$value->v_status; ?>"><?php if($value->v_status==0) { ?>Inactive<?php } if($value->v_status==1) { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                    <?php } ?> 
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


