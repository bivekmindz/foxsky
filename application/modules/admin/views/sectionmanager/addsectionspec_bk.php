 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
	.sepbox{ width:100%; float:left;}
	.mar-top{ margin-top:33px;}
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        ignore : [],
        rules: {
            // sec_name        : "required",
            sec_cat         : "required",
            //"sec_subcat[]"  : "required",
            "slideimg[]"    : "required",
            "banimg[]"     : "required",
        },
        // Specify the validation error messages
        messages: {
            sec_name        : "Section Name is required",
            sec_cat         : "Section Category is required",
            //"sec_subcat[]"  : "Section Sub-Category is required",
            "slideimg[]"    : "Center Banner Image is required",
            "banimg[]"      : "Banner Image is required",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
            <form id="addCont" action="" method="post" enctype="multipart/form-data" >
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Specification Section</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add Product specification</p>
                        </div>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
        <div class="col-lg-12">
                    <div style="text-align:right;">
            <a href="#"><button>CANCEL</button></a>
           </div>
           <div style="float: left;width: 100%;margin-left: 14px;"><h4>Add Section</h4></div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
                        <div class="sep_box">
                           <!--  <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Section Name <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="sec_name" name="sec_name" type="text" /></div>
                                    </div>
                                </div>
                            </div>  -->       
                           <!--  <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Section Category <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="sec_cat" id="sec_cat" onchange="selectsubcat();">
                                                <option value="">Select Type</option>
                                                <?php foreach ($parentcat as $key => $value) { ?>
                                                <option value="<?php echo $value->catid;?>"><?php echo $value->catname;?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Section Category <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="sec_cat" id="sec_cat" onchange="selectsubcat();">
                                                <option value="">Select Type</option>
                                                <?php foreach ($parentcat as $key => $value) { ?>
                                                <option value="<?php echo $value->catid;?>"><?php echo $value->catname;?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>

                             <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Product Name<span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="pro_cat" id="pro_name" onchange="selectsubcat1();">
                                                <option value="">Select Type</option>
                                            </select></div>
                                    </div>
                                </div>
                            </div>

                              <!-- <div class="sep_box" style="width: 98%; border-bottom: 1px solid #ddd;"> -->

                              <div style="float: left;width: 100%;margin-left: 14px;"><h4>Top Section</h4></div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                     <div class="tbl_text">Center Banner Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="slideimg<?=$i?>" name="slideimg[]" type="file" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                    <div class="tbl_text">Heading</div>
                                        <div class="tbl_input"><input type="text" name="slideurl[]" id="slideurl<?=$i?>" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Sub-Heading</div>
                                        <div class="tbl_input"><input id="slidehead<?=$i?>" type="text" name="slidehead[]" /></div>
                                    </div>
                                </div>
                            </div>
                            <!-- </div> -->


                         
                        <div style="float: left;width: 95%;margin-left: 14px; border-top:1px solid #E4E4E4;"><h4>Customize Center Banner</h4></div>
                        <div class="col-lg-4" id="sub_cat">
                                <!-- <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Left Value<span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select style="height:38px;" name="sec_subcat[]" id="sec_subcat">
                                            </select>
                                        </div>
                                    </div>
                                </div> -->
                                
                                 <div class="row">
                                 <div style="float: left;width: 100%;margin-left: 14px;"><h4>Bottom Section</h4></div>
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Feature Left Value<span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="left_val" id="left_value" onchange="selectsubcat2();" >
                                                <option value="">Select Type</option>
                                            </select></div>
                                    </div>
                                </div>
                                 </div>
                        <div class="appenddiv"> 
                            <div class="sepbox">           
                            <div class="col-lg-5" id="sub_cat">
                               
                                    
                                        <div class="tbl_text">Feature Right Value<span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="right_val" id="right_value" onchange="text">
                                                <option value="">Select Type</option>
                                            </select></div>
                                  
                               

                            </div>

                            <div class="col-lg-5" id="sub_cat">
                                
                                    <div class="col-lg-12">
                                    <div class="tbl_text"> value</div>
                                        <div class="tbl_input"><input type="text" name="text" id="rvalue<?=$i?>" /></div>
                                    </div>
                                
                            </div>
                            </div>
                            
                        </div>

                         <div class="sep_box">
                            <div class="col-lg-6">
                               
                                    <button type="button" class="addMore"><i class="glyphicon glyphicon-plus"></i> Add More</button>
                                
                            </div>
                        </div>
                        <!-- <?php
                            for ($i=0; $i < 1; $i++) {                                
                                $j = $i+1;
                        ?> -->
                        <!-- .................. NEW SLIDER 1 .................. -->
                        <div class="sep_box" style="width: 98%; border-bottom: 1px solid #ddd;">
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                     <div class="tbl_text">Center Banner Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="slideimg<?=$i?>" name="slideimg[]" type="file" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                    <div class="tbl_text">Heading</div>
                                        <div class="tbl_input"><input type="text" name="slideurl[]" id="slideurl<?=$i?>" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Sub-Heading</div>
                                        <div class="tbl_input"><input id="slidehead<?=$i?>" type="text" name="slidehead[]" /></div>
                                    </div>
                                </div>
                            </div>
                            

                        
                        <!-- <?php } ?> -->

     <!-- <div style="float: left;width: 95%;margin-left: 14px;"><h4>Customize Banners</h4></div>

                        <?php
                        $j = 0;
                            for ($i=0; $i < 4; $i++) {
                                $j = $i+1;
                        ?> -->
                        <!-- .................. NEW Banner <?=$i?> .................. -->
                      <!--   <div class="sep_box" style="width: 98%; border-bottom: 1px solid #ddd;">
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                    <div class="tbl_text">Banner Image <?=$j?> <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input id="banimg<?=$i?>" name="banimg[]" type="file" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="tbl_text">Banner Url <?=$j?> </div>
                                        <div class="tbl_input"><input type="text" name="banurl[]" id="banurl<?=$i?>" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="tbl_text">Banner Heading <?=$j?> </div>
                                        <div class="tbl_input"><input id="banhead<?=$i?>" type="text" name="banhead[]" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="tbl_text">Banner Sub-Heading <?=$j?> </div>
                                        <div class="tbl_input"><input id="bansubhead<?=$i?>" type="text"  name="bansubhead[]"/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <?php } ?>



 -->
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">
/*    function selectsubcat(){
     var parentcat=$('#sec_cat').val();
     var sec_catid=""; 
     $.ajax({
        url: '<?php echo base_url()?>admin/sectionmanager/hmsubcategory',
        type: 'POST',
        data: {'parentcat': parentcat,'sec_catid':sec_catid},
        success: function(data){
           //alert(data); //return false;
           $('#sub_cat').css('display','block');
            $('#sec_subcat').empty();
            var option = "<option value=''>Select Sub-Category</option>";
            $("#sec_subcat").html(option+data);
          
              
         }
  });
    
   }
*/
$(document).ready(function(){
      var maxField = 100; 
      var z = 1; 
      var wrapper1 = $('.appenddiv'); 
      $(".addMore").click(function(){ 
        if(z < maxField){
            z++;

            var ids = $('#left_value').val();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/feature/featureRight',
                type:'POST',
                data:{'ids':ids},
                success: function(proData) {
                    // alert(proData);
                    $('#right_value'+z+'').html(proData);
                }

            });
            $(".appenddiv").append(' <div class="sepbox"><div class="col-lg-5" id="sub_cat"><div class="col-lg-12s "><div class="tbl_text">Feature Right Value<span style="color:red;font-weight: bold;">*</span></div><div class="tbl_input"><select name="right_val" id="right_value'+z+'" onchange="text"><option value="">Select Type</option></select></div></div></div><div class="col-lg-5" id="sub_cat"><div class="col-lg-12"><div class="tbl_text"> value</div><div class="tbl_input"><input type="text" name="text" id="rvalue<?=$i?>" /></div></div></div><div class="col-lg-2"><button type="button" class="mar-top remove_row"><i class="glyphicon glyphicon-minus"></i></button></div></div>');
        }
      });
      $(wrapper1).on('click', '.remove_row', function(e){
          e.preventDefault();
          $(this).parent('div').parent('div').remove();
          z--;
      });
    });
  


   function selectsubcat(){
    var ids= $('#sec_cat').val();
     //alert(ids);
     $.ajax({
            url: '<?php echo base_url(); ?>admin/sectionmanager/productName',
            type:'POST',
            data:{'ids':ids},
            success: function(proData) {
                //alert(proData);
                $('#pro_name').html(proData);
            }

        });
}


 function selectsubcat1(){
    var ids= $('#sec_cat').val();
     //alert(ids);
     $.ajax({
            url: '<?php echo base_url(); ?>admin/feature/featureLeft',
            type:'POST',
            data:{'ids':ids},
            success: function(proData) {
              //   alert(proData);
                $('#left_value').html(proData);
            }

        });
}


    function selectsubcat2(){
        var ids= $('#left_value').val();
         // alert(ids);
         $.ajax({
                url: '<?php echo base_url(); ?>admin/feature/featureRight',
                type:'POST',
                data:{'ids':ids},
                success: function(proData) {
                    // alert(proData);
                    $('#right_value').html(proData);
                }

            });
    }

</script>    

    