 <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <link type="text/css" rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/jquery-te-1.4.0.css">


<script type="text/javascript" src="<?php echo base_url()?>assets/admin/css/jquery-te-1.4.0.min.js" charset="utf-8"></script>

  <!-- jQuery Form Validation code -->
  <style>
.sectionsubhead_tb{height: 24px;
    border-bottom: 1px solid rgb(141, 185, 0);
    display: none;}
    .rvalue_tb{height: 24px;
    border-bottom: 1px solid rgb(141, 185, 0);
    display: none;}
    
    #sectionsubhead_container { border-top: 1px solid rgb(141, 185, 0);}
     #rvalue_container { border-top: 1px solid rgb(141, 185, 0);}
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
	.sepbox{ width:100%; float:left;}
	.mar-top{ margin-top:33px;}
	.appendleftdiv{ width:100%; float:left; padding:5px 0px; }
  </style>
<?php 
for ($x = 1; $x <= 100; $x++) {
   // echo "The number is: $x <br>";
   
   ?>
   <style>
        .rvalue<?php echo $x; ?>_tb{height: 24px;
    border-bottom: 1px solid rgb(141, 185, 0);
    display: none;}
    #rvalue<?php echo $x; ?>_container { border-top: 1px solid rgb(141, 185, 0);}
   </style>
   <?php
} 
?>
    <script language='javascript'>
      $(document).ready(function() {
         $("#addCont").validate({
            ignore : [],
            rules: {
                "pro_cat" : "required",
                "sec_cat" : "required",
                "slideimg" : "required",
                "sectionhead" : "required",
                "sectionsubhead" : "required",
            },
            // Specify the validation error messages
            messages: {
                "pro_cat" : "Product Name is required",
                "sec_cat" : "Section Category is required",
                "slideimg" : "Section Banner Image is required",
                "sectionhead" : "Section Head is required",
                "sectionsubhead" : "Section Sub Head is required",
                "specificationsubhead" : "Specification Sub Heading is required",
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
      });
    </script>

<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
            <form id="addCont" action="" method="post" enctype="multipart/form-data" >
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Specification Section</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section Admin can add Product specification</p>
                        </div>
                    </div>
                    <div class="page_box">
                    <div class="sep_box">
        <div class="col-lg-12">
                    <div style="text-align:right;">
            <a href="#"><button>CANCEL</button></a>
           </div>
           <div style="float: left;width: 100%;margin-left: 14px;"><h4>Add Section</h4></div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div>    <script language="Javascript" src="<?php echo base_url()?>assets/admin/js/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script language="Javascript" src="<?php echo base_url()?>assets/admin/js/htmlbox.min.js" type="text/javascript"></script>


</div>
                        <div class="sep_box">
                            <div class="col-lg-6">

                   <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Section Category <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="sec_cat" id="sec_cat" onchange="selectsubcat();" required>
                                                <option value="">Select Type</option>
                                                <?php foreach ($parentcat as $key => $value) { ?>
                                                <option value="<?php echo $value->catid;?>"><?php echo $value->catname;?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>

                             <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Select Product Name<span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input">
                                            <select name="pro_cat" id="pro_cat" onchange="selectsubcat1();" required>
                                                <option value="">Select Type</option>
                                            </select></div>
                                    </div>
                                </div>
                            </div>

                            <div style="float: left;width: 100%;margin-left: 14px;"><h4>Top Section</h4></div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                     <div class="tbl_text">Center Banner Image <span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input name="slideimg" id="slideimg" type="file" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                    <div class="tbl_text">Heading<span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input type="text" name="sectionhead" id="sectionhead"/></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Sub-Heading<span style="color:red;font-weight: bold;">*</span></div>
                                        <div class="tbl_input"><input type="text" name="sectionsubhead" id="sectionsubhead"   class="hb"/></div>
                                    </div>
                                </div>

                            </div>
                            <!-- </div> -->
                         
                        <div style="float: left;width: 95%;margin-left: 14px; border-top:1px solid #E4E4E4; margin-top:15px;"><h4>Customize Center Banner</h4></div> <div style="float: left;width: 100%;margin-left: 14px;"><h4>Bottom Section</h4></div>
                        
                        
                        <div class="appendleftdiv">
                        <div class="row">
                        <div class="col-lg-11" id="sub_cat">
                                      <div class="col-lg-4">
                                
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Feature Left Value</div>
                                        <div class="tbl_input"><input type="text" name="specificationsubhead[]" id="specificationsubhead" /></div>
                                    </div>
                                
                            </div>
                                <!-- 
                                    <div class="col-lg-11">
                                        <div class="tbl_text">Feature Left Value</div>
                                        <div class="tbl_input">
                                            <select name="left_val[]" id="left_value" onchange="selectsubcat2(0);" >
                                                <option value="">Select Type</option>
                                            </select>
                                        </div>
                                    </div> -->
                                
                            </div>
                            
                           <!-- 
                            <div class="col-lg-6" id="sub_cat">
                            <div class="col-lg-11">
                                    <div class="tbl_text">Feature Right Value</div>
                                        <div class="tbl_input">
                                            <select name="right_val[]" id="right_value" onchange="text">
                                                <option value="">Select Type</option>
                                            </select>
                                        </div>
                                        </div>
                                </div> -->
                            
                        </div>
                            
                                     
                                
                                <div class="row">
                                <div class="col-lg-6" id="sub_cat">
                                    <div class="col-lg-11">
                                        <div class="tbl_text"> value</div>
                                            <div class="tbl_input"><input type="text" name="value[]" id="rvalue" /></div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                <div class="row"> 
                                    <div class="col-lg-11">
                                     <div class="tbl_text">Center Banner Image</div>
                                        <div class="tbl_input"><input name="banimg[]" type="file" id="banimg"/></div>
                                    </div>
                                </div>
                            </div>
                                </div>

                                
                             <div class="row">
                             <div class="col-lg-4">
                                
                                    <div class="col-lg-11">
                                    <div class="tbl_text">Heading</div>
                                        <div class="tbl_input"><input type="text" name="specificationhead[]" id="specificationhead"/></div>
                                    </div>
                               
                            </div>
                            
                      
                             
                             </div>
                            
                            
                            
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <button type="button" class="addMore"><i class="glyphicon glyphicon-plus"></i> Add More</button>
                            </div>
                        </div>
                       
                        <div class="sep_box" style="width: 98%; border-bottom: 1px solid #ddd;">
                          <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">

$(document).ready(function(){
      var maxField = 100; 
      var z = 1; 
      var wrapper1 = $('.appendleftdiv'); 
      $(".addMore").click(function(){ 
        if(z < maxField){
            z++;
            var id = $('#sec_cat').val();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/feature/featureLeft',
                type:'POST',
                data:{'ids':id},
                success: function(proData) {
                  //   alert(proData);
                    $('#left_value'+z+'').html(proData);
                    
                    
                                var rvalue = $("#rvalue"+z+"").htmlbox({
    buttons:[
         ["separator_dots","bold","italic","underline"]
    ],
    icons:"default",
    skin:"green"
});


                }

            });

            var ids = $('#left_value').val();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/feature/featureRight',
                type:'POST',
                data:{'ids':ids},
                success: function(proData) {
                   $('#right_value'+z+'').html(proData);
                }
            });
   
            $(".appendleftdiv").append('<div id="newDiv'+z+'"><div class="col-lg-4" id="sub_cat"><div class="row"><div class="col-lg-11"><div class="tbl_text">Feature Left Value<span style="color:red;font-weight: bold;">*</span></div><div class="tbl_input"><input type="text" name="specificationsubhead[]" id="specificationsubhead"/></div></div></div></div><div class="sepbox"><div class="col-lg-5" id="sub_cat"></div><div class="col-lg-5" id="sub_cat"><div class="col-lg-12"><div class="tbl_input"><input type="text" name="value[]" id="rvalue'+z+'" /></div><div class="tbl_text">Center Banner Image</div><div class="tbl_input"><input name="banimg[]" type="file" /></div><div class="tbl_text">Heading</div><div class="tbl_input"><input type="text" name="specificationhead[]" id="specificationhead"/></div></div></div><div class="col-lg-2"><button type="button" onClick="removeRow('+z+');" class="mar-top remove_row"><i class="glyphicon glyphicon-minus"></i></button></div></div></div>');
        }
      });
    });
    
 
    function removeRow(z){
       $("#newDiv"+z).remove(); 
    }

   function selectsubcat(){
     var ids= $('#sec_cat').val();
  //   alert(ids);
     $.ajax({
            url: '<?php echo base_url(); ?>admin/sectionmanager/productName',
            type:'POST',
            data:{'ids':ids},
            success: function(proData) {
                //alert(proData);
                $('#pro_cat').html(proData);
            }

        });
    }


    function selectsubcat1(){
        var ids= $('#sec_cat').val();
        $.ajax({
            url: '<?php echo base_url(); ?>admin/feature/featureLeft',
            type:'POST',
            data:{'ids':ids},
            success: function(proData) {
              //   alert(proData);
                $('#left_value').html(proData);
            }
        });
    }


    function selectsubcat2(z){
        if(z==0){
          var ids= $('#left_value').val();
        }else{
           var ids= $('#left_value'+z).val();
        }
      
        $.ajax({
            url: '<?php echo base_url(); ?>admin/feature/featureRight',
            type:'POST',
            data:{'ids':ids},
            success: function(proData) {
                if(z==0){
                    $('#right_value').html(proData);
                }else{
                    $('#right_value'+z).html(proData);

                }
            }
        });
    }
</script>    
<script>
    $('.jqte-test').jqte();
    
    // settings of status
    var jqteStatus = true;
    $(".status").click(function()
    {
        jqteStatus = jqteStatus ? false : true;
        $('.jqte-test').jqte({"status" : jqteStatus})
    });
</script>
    <script language="Javascript" type="text/javascript">
var hb = $(".hb").htmlbox({
    buttons:[
         ["separator_dots","bold","italic","underline"]
    ],
    icons:"default",
    skin:"green"
});
</script>

   <script language="Javascript" type="text/javascript">
var rvalue = $("#rvalue").htmlbox({
    buttons:[
         ["separator_dots","bold","italic","underline"]
    ],
    icons:"default",
    skin:"green"
});
</script>
