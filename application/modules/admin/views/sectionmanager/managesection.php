<div class="wrapper">

<?php
      $this->load->view('helper/nav')
?> 

<div class="col-lg-10 col-lg-push-2">
        <div class="row">
         
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                       <h2>Manage Product Specification </h2>
                    </div>
                    <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this Section , Admin can add manage product specification and can delete also.</p>
                        </div>
                    </div>
            
                    <div class="page_box">
                        <div class="sep_box1">
                            <div class="col-lg-12">
                            <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/sectionmanager/addsection"><button>Add Section</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>

                                <table class="grid_tbl" id="search_filter">
                                    <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Section Heading</th>
                                               <th>Product</th>
                                            <th>Section Sub Heading</th>
                                            <th>Image</th>
                                            <th>Updated Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                         $i=0; 
                                        if(count($recs) > 0){
                                         foreach ($recs as $key => $value) {
                                         
                                         $i++;
                                    ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->section_heading ;?></td>
                                              <td><?php echo $value->prname; ?></td>
                                            <td><?php echo $value->section_sub_heading ;?></td>    
                                            <td><?php if($value->banner_img) { ?>
                                                <img style="width:50px;height:75px;" src="<?php echo base_url('assets/sectionimages/').'/'.$value-> banner_img;?>"/>
                                                <?php } else { ?>
                                                <img style="width:50px;height:75px;" src="<?php echo base_url('assets').'/'.'noimage.png';?>"/>
                                                <?php } ?>
                                            </td> 
                                            <td><?php if(empty($value->modifiedby)){ echo $value->created_at; } ?></td>   
                                            <td>
                                            <input type='hidden' name='attstatu[<?php echo $value->specification_id;?>]'  value='<?=$value->sectionstatus?>'>
                                            <a href="<?php echo base_url()?>admin/sectionmanager/updatesection/<?php echo $value->specification_id?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/sectionmanager/deletesection/<?php echo $value->specification_id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/sectionmanager/statussection/<?php echo $value->specification_id.'/'.$value->status; ?>"><?php if($value->status=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                            <?php 
                                    } 

                                }//END IF
                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
