<div class="wrapper">
<?php $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Update Mobile Slider</h2>
   
    </div>
        <div class="page_box">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/add_mob_banner/addbannerimg"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        <form method="post" action="" enctype="multipart/form-data">
                    
        <div class="sep_box">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="tbl_text">Mobile Slider Image</div>
                    </div>
                    <div class="col-lg-8">
                        <div class="tbl_input"><input id="imgslide" name="imgslide" type="file" /></div>
                        <div><img style="margin: 10px 0px 5px 5px;" height="60" width="60" src="<?php echo base_url()?>assets/mobilebanners/<?php echo $view->b_img;?>"></td></div>
                    </div>
                </div>
            </div>
        </div>    
  

        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
   