<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    
    <h2>Add Tax</h2>
   
    </div>
        <div class="page_box">
          <div class="sep_box">
        <div class="col-lg-12">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/category/viewtax"><button>CANCEL</button></a>
        </div>
         <div class='flashmsg'>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
        <form action="" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Tax Type</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select id="typeid" onchange="categoryvalue();" >
                <option value="sele">Select Type</option>
                <option value="pro">Product</option>
                <option value="catg">Category</option>
                <option value="bran">Brand</option>
                </select> 
                </div>
            </div>
        </div>
        </div>
            
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Country</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select name="countryid" id="countryid" onclick="selectstates();" >
                <option value="">Select  Country</option>
                <?php foreach ($viewcountry as $value){  ?>  
                <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>                  
                <?php } ?>
                </select> 
                </div>
            </div>
        </div>
        </div>
         <div class="col-lg-6" style='display:none' id="staterid">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">State</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
               <select id="stateid" name="stateid" value="stateid" multiple="">
                    <option value="">Select State</option>
             </select> 
                </div>
            </div>
        </div>
        </div>
       
        </div>
         <div class="sep_box">
        <div class="col-lg-6" style='display:none' id="childid">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Category</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select name="catidd" id="catidd" >
                <option>Select Category</option>
                <?php foreach ($parentCatdata as $key => $value) {
                 ?>
                  <option value="<?php echo $value->catid ?>"><?php echo $value->catname ?></option>
                 <?php }?>
                </select>
                </div>
            </div>
        </div>
        </div>

        
        <div class="col-lg-6" style='display:none' id="prodid">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Product</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <input type="text" name="pname">
                </div>
            </div>
        </div>
        </div>
        
        <div class="col-lg-6"  id="bradid" style='display:none'>
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Brand</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                <select name="brandid" id="brandid" >
                <option value="0">Select Brand</option>
                <?php foreach ($vieww as $key => $value) {
                 ?>
                  <option value="<?php echo $value->id ?>"><?php echo $value->brandname ?></option>
                 <?php }?>
                </select>
                </div>
            </div>
        </div>
        </div>
       
        </div>
          <div class="sep_box" id="viewpricetax" style='display:none'>
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>Base Price</th>
                                            <th>Vat Cst</th>
                                            <th>Vat Non Cst</th>
                                            <th>Octory Tax</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="text" name="bprice" id="bprice"></td>
                                            <td><input type="text" name="vcst"   id="vcst"></td>
                                            <td><input type="text" name="nvcst"  id="nvcst"></td>
                                            <td><input type="text" name="octax"  id="octax"></td>
                                            
                                        </tr>
                                      
                                     
                                      
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
          
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8">
                <div class="submit_tbl">
                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>


                  
        </div>
        </div>
        </div>
    </div>
    </div>
   <script type="text/javascript">
   function categoryvalue(){
        var id=$('#typeid').val();
        
        if($.trim(id) == 'pro'){
            $('#bprice').val('');
            $('#vcst').val('');
            $('#nvcst').val('');
            $('#octax').val('');
            $('#prodid').show();
            $('#childid').hide();
            $('#bradid').hide();
            $('#staterid').hide();
            $('#viewpricetax').show();
        }
        if($.trim(id) == 'catg'){
            $('#bprice').val('');
            $('#vcst').val('');
            $('#nvcst').val('');
            $('#octax').val('');
            $('#countryid').val('');
            $('#prodid').hide();
            $('#childid').show();
            $('#bradid').hide();
            $('#staterid').hide();
            $('#viewpricetax').show();
        }
        if($.trim(id) == 'bran'){
            $('#bprice').val('');
            $('#vcst').val('');
            $('#nvcst').val('');
            $('#octax').val('');
            $('#countryid').val('');
            $('#prodid').hide();
            $('#childid').hide();
            $('#bradid').show();
            $('#staterid').hide();
            $('#viewpricetax').show();
        }
        if($.trim(id) == 'sele'){
            $('#countryid').val('');
            $('#prodid').hide();
            $('#childid').hide();
            $('#bradid').hide();
            $('#staterid').hide();
            $('#viewpricetax').hide();
        }
    }


   function selectstates(){
     var countryid=$('#countryid').val();
     $('#staterid').show();
     $.ajax({
        url: '<?php echo base_url()?>admin/geographic/countrystate',
        type: 'POST',
        //dataType: 'json',
        data: {'countryid': countryid},
        success: function(data){
        var  option_brand = '<option value="">Select State</option>';
        $('#stateid').empty();
        $("#stateid").append(option_brand+data);
           
         }
  });
    
   }
   </script>