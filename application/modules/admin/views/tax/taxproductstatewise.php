<style type="text/css">
  .page_box
{
   overflow-x: inherit!important;
}    

</style>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo admin_url();?>bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo admin_url();?>bootstrap/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?php echo admin_url();?>bootstrap/css/bootstrap-multiselect.css" type="text/css"/>  
     <div class="wrapper">
       <?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                   
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Tax State Wise</h2>
                    </div>
                    <div class="page_box">
             <form method="post" id="add_vendor_form" action="" enctype="multipart/form-data">
                     <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                        <div class="sep_box">
                             <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Category <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <select id="mcatid" name="mcatid" onchange="childcategory();">
                                        <option value="">Select Category</option>
                                        <?php foreach ($viewwcattid as $key => $value) { 
                                        ?>
                                        <option value="<?php echo $value->catid; ?>"><?php echo $value->catname; ?></option>
                                        <?php }?>
                                        </select> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Country <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <select name="countryid" id="countryid" onchange="selectstates();">
                                        <option value="">Select  Country</option>
                                        <?php foreach ($viewcountry as $value){  ?>  
                                        <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>                  
                                        <?php } ?>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                          <div class="sep_box">
                            <div class="col-lg-6">
                            <div class="row">
                            <div class="col-lg-4">
                            <div class="tbl_text">Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                            <div class="tbl_input">
                            <select id="msubcatid" name="msubcatid">
                            <option value="">Select Sub-Category</option>
                            </select> 
                            </div>
                            </div>
                            </div>
                            </div>

                            <div class="col-lg-6">
                            <div class="row">
                            <div class="col-lg-4">
                            <div class="tbl_text">State <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                            <div class="tbl_input">
                            <select id="stateid" name="stateid[]">
                            <option value="">Select State</option>
                            </select> 
                            </div>
                            </div>
                            </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                            <div class="row">
                            <div class="col-lg-4">
                            <div class="tbl_text">Child Sub-Category <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                            <div class="tbl_input">
                            <select id="subcatid" name="subcatid[]">
                            <option value="">Select Child Sub-Category</option>
                            </select> 
                            </div>
                            </div>
                            </div>
                            </div>
                        </div>            

                  <div class="sep_box">
                  <?php foreach ($vieww as $key => $value) { ?>
        <div class="col-lg-6">
        <div class="row">
        <div class="col-lg-4"><?php echo $value->taxname ;?> <span style="color:red;font-weight: bold;">*</span></div>
        <div class="col-lg-8"> <div class="tbl_input">
        <input type="hidden" name="taxid[]" id="taxid" value="<?php echo $value->id ;?>"/>
        <div class="form-group">
   <div class="input-group">
      <div class="input-group-addon">GST</div>
       <input type="text" name="taxvalue[<?php echo $value->id ;?>]" id="taxvalue" class="numeric form-control" maxlength="4" placeholder="Value"/ >
      <div class="input-group-addon">%</div>
    </div>
  </div>

        </div>
        </div></div>
        </div>
        <?php } ?>
        </div>
          <div class="sep_box">
          <div class="col-lg-6">
          <div class="col-lg-4"></div>
        <div class="col-lg-8">
        <div class="submit_tbl">
        <input type="submit" name="submit" class="btn_button sub_btn"/></div></div></div>
          </div>
        
                   
                   
                </div>
                                </form>
            </div>
        </div>
    </div>
    </div>
<script type="text/javascript">

function selectstates(){
     var countryid=$('#countryid').val();
     $('#staterdid').show();
     $.ajax({
        url: '<?php echo base_url()?>admin/geographic/countrystate',
        type: 'POST',
        //dataType: 'json',
        data: {'countryid': countryid},
        success: function(data){
        
            $("#stateid").empty();
            $("#stateid").attr("multiple","multiple");
            $("#stateid").multiselect('destroy');
            $("#stateid").append(data);
            $('#stateid').multiselect({ 
                enableClickableOptGroups: true,
                enableFiltering: true,
                includeSelectAllOption: true,
                enableCaseInsensitiveFiltering: true,
            });
           
        }
  });
    
   }

   function childcategory(){
    var catid=$('#mcatid').val();
     
    $.ajax({
        url: '<?php echo base_url()?>admin/attribute/catvalue',
        type: 'POST',
        //dataType: 'json',
        data: {'catid': catid},
        success: function(data){
             
            $('#msubcatid').empty();
            var option ="<option value=''>Select Sub-Category</option>";
            $("#msubcatid").append(option+data);
        }
    });
    
   }

</script>

<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
    <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(function () {
            $(".numeric").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode == 46) || specialKeys.indexOf(keyCode) != -1);
                $(".error1").css("display", ret ? "none" : "inline");
                return ret;
            });
            $(".numeric").bind("paste", function (e) {
                return false;
            });
            $(".numeric").bind("drop", function (e) {
                return false;
            });
        });


    </script>

    <script src="<?php echo base_url().'assets/js/jquery.validate.min.js'; ?>"></script>
     <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
  </style>
 <script>
// When the document is ready
    $(document).ready(function () {

         /*$.validator.addMethod("valueNotEquals", function(value, element, arg){
            return arg != value;
         }, "Value must not equal arg.");

         $.validator.addMethod("check_cat", function (value, element) {
           
            var count = $(element).find('option:selected').length;
            return count > 0;
        });*/
     //validation rules
        $("#add_vendor_form").validate({

            ignore:[],
            rules: {
                        "mcatid" : {
                            required : true,
                        },
                        "msubcatid" : {
                            required : true,
                        },
                        "subcatid[]" : {
                            required : true,
                        },
                         "countryid" : {
                            required : true,
                        },
                         "stateid[]" : {
                            required : true,
                        },
                        "taxvalue[5]" :{
                            required : true,
                        },
                       
              },
            messages: {
                "mcatid": {
                            required: "Please select any Category.",
                },  
                "msubcatid": {
                            required : "Please select at least one Sub-Category",
                },
                "subcatid[]" : {
                            required : 'Please select at least one Child Sub-Category',
                        },
                "countryid" : {
                            required: "Please select any country",
                },
                "stateid[]" : {
                            required: "Please select at least one State",
                }, 
                "taxvalue[5]" : {
                            required: "Please Add GST value",
                },
                }
        });
    });

    $("#msubcatid").change(function(){
        var catid=$('#msubcatid').val();
            
            $.ajax({
                url: '<?php echo base_url()?>admin/attribute/subbranddcatvalue',
                type: 'POST',
                //dataType: 'json',
                data: {'catid': catid},
                success: function(data){
                   
                      $("#subcatid").empty();
                      $("#subcatid").attr("multiple","multiple");
                      $("#subcatid").multiselect('destroy');
                      $("#subcatid").append(data);
                      $('#subcatid').multiselect({ 
                        enableClickableOptGroups: true,
                        enableFiltering: true,
                        includeSelectAllOption: true,
                        enableCaseInsensitiveFiltering: true,
                      });
                }
            });
    });
</script>