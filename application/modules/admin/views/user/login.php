<!DOCTYPE HTML>
<html>
<head>
    <title>Admin</title>
    <link rel='shortcut icon' href="<?php echo base_url();?>assets/fevicon.png"/>
    <link href="<?php echo base_url()?>assets/webapp/css/bizzgain4.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/webapp/css/pages.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/webapp/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    <link href="fontawsome/css/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
    <div class="loginPageMain">
	    <div class="loginForm">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        		<img src="<?php echo base_url()?>assets/admin/images/logo3.png" class="adminLogo">
    		    <div class="form-group">
                <form action="" method="post">
                    <label style="font-weight:normal;">E-mail:</label>
                    <input  type="text" placeholder="User Email" class="form-control space2" name="email" value="<?php echo set_value('email');?>" />
                    <label style="font-weight:normal;">Password:</label>
                    <input type="password" class="form-control" placeholder="User Password" name="password" value="" />
                    <input type="submit" name="submit" value="Login" class="submitBtn_new button" style="background:#2171a0!important;" >
                    <div><?php echo validation_errors();  if(isset($result) && $result!=''){echo ($result);}?></div>
                </form>
            	</div>
            </div>
        </div>
    </div>
</body>
</html>