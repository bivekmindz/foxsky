<div class="wrapper">
<?php $this->load->view('helper/nav')?> 

 <div class="col-lg-10">
        <div class="row">
          <form action="" method="post" enctype="multipart/form-data" >
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Top Banner</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Banner Image</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input type="file" name="file"  class="file"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">

                                        <div class="tbl_text">Image URL</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <input type="hidden" name="type" value="banner" >
                                        <input type="url" name="imgurl" calss="file" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="Submit1" type="submit" value="Submit" name="submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Banner Image</th>
                                            <th>Banner Url</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php $i=0; foreach ($banner as $key => $value) {
                                          $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><img src="<?php echo base_url(); ?>imagefiles/HomeBanner/<?php echo $value->t_imgname; ?>" height="60" width="60"    ></td>
                                            <td><?php echo $value->t_path;?></td>
                                            <td>
                                            <a href="<?php echo base_url()?>admin/home/topbannerupdate/<?php echo $value->n_imgid?>" ><i class="fa fa-pencil"></i></a>
                                            <a href="<?php echo base_url()?>admin/home/topdelete/<?php echo $value->n_imgid?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/home/topstatus/<?php echo $value->id.'/'.$value->status; ?>"><?php if($value->status=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>
                                            </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
            
            </form>
        </div>
    </div>
    </div>