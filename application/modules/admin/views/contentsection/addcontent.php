<div class="wrapper">
<?php  $this->load->view('helper/nav')?> 
<div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Update <?php foreach ($updatemenuview as $value){  
                        if($value->id==$cmsid->title){ echo $value->title; } } ?> Detail</h2>
                    </div>
                    <div class="page_box">
                    <div class="">  
                           <?php  echo $this->session->flashdata('message'); ?>
                    </div>
                    <form action="" method="post" enctype="multipart/form-data">
                       
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Menu Category</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                        <?php if($cmsid->title!=''){ ?>
                                        <input id="con_title" name="con_title" type="hidden" value="<?php echo $cmsid->title;?>"/>
                                        <?php foreach ($updatemenuview as $value){  
                                        if($value->id==$cmsid->title){ echo "<p style='font-weight:bold;font-size: medium;padding-top: 9px;'>".$value->title."</p>"; 
                                                }               
                                                 }
                                             } else { ?>
                                           <select name="con_title" id="con_title" required>
                                            <option value="">Select Menu Category</option>
                                            <?php foreach ($menuview as $value){  ?>  
                                         <option value="<?php echo $value->id; ?>"><?php echo $value->title; ?></option>                  
                                                <?php } ?>
                                            </select>
                                                <?php } ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Content Description</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><textarea name="con_description" id="content" required>
                                        <?php echo $cmsid->description;?></textarea>
                                        <?php echo display_ckeditor($ckeditor); ?>


                                                                

                                                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>   

                        <!-- <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Meta Title</div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="tbl_input"><input id="metatitle" name="metatitle" type="text" value="<?php echo $cmsid->metatitle;?>" /></div>  
                                </div>
                            </div>
                        </div></div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Meta Keywords</div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="tbl_input"><input id="metakeywords" name="metakeywords" type="text" value="<?php echo $cmsid->metakeywords;?>" /></div>  
                                </div>
                            </div>
                        </div></div> 
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Meta Description</div>
                                    </div>
                                    <div class="col-lg-8">
                                      <div class="tbl_input"><input id="metadescription" name="metadescription" type="text" value="<?php echo $cmsid->metadescription;?>" /></div>  
                                </div>
                            </div>
                        </div></div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">URL Slug</div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input"><input id="urlslug" name="urlslug" type="text" value="<?php echo $cmsid->urlslug;?>" /></div>  
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submit" name="submit" type="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <a href="<?php echo base_url(); ?>admin/ckeditor/cmsview" class="btn_button sub_btn">Back Listing</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                  </form>
               </div>
            </div>
        </div>
    </div>
    