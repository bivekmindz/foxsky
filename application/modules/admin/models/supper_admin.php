<?php
class Supper_admin extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	public function call_procedure($procedure, $parameter = array()) {
		$param         = $this->userfunction->paramiter($parameter);
		$key=(array_keys($parameter));
		for ($i=0; $i < count($key) ; $i++) { 
		if(empty($parameter[$key[$i]]) ){ $parameter[$key[$i]]=0;} 
		}
		$query         = $this->db->query("call $procedure($param)", $parameter);
		$close         = $this->db->close();
		return $result = $query->result();
	}
	public function call_procedureRow($procedure, $parameter = array()) {
		$param         = $this->userfunction->paramiter($parameter);
		$key=(array_keys($parameter));
		for ($i=0; $i < count($key) ; $i++) { 
		if(empty($parameter[$key[$i]]) ){ $parameter[$key[$i]]=0;} 
		}
		$query         = $this->db->query("call $procedure($param)", $parameter);
		$close         = $this->db->close();
		return $result = $query->row();
	}
	public function userdata($array) {
		$recorddata = array(
			'admin_id'  => $array[0]->ad_id,
			'username'  => $array[0]->username,
			'validated' => true);
		$this->session->set_userdata($recorddata);

		main_menu('menuaccess');
	}

} //  end class 

/*begin
if(p_coupon='')then
set p_coupon=null;
end if;

if(p_start='')then
set p_start=null;
end if;

if(p_end='')then
set p_end=null;
end if;

if(p_status='')then
set p_status=null;
end if;

select td.copid,td.t_disc_name,td.CopId,td.DiscountName,td.OfferType,td.PurchaseAmt,td.DiscountAmt,td.CouponCode,td.DiscountOn,
td.StartDate,td.EndDate,td.CreatedOn, td.DiscStatus,td.DiscountType
from tbldiscount td where td.DiscStatus!='D'
and (td.CouponCode REGEXP p_coupon or p_coupon is null)
and (td.StartDate = p_start or p_start ='0000-00-00')
and (td.EndDate = p_end or p_end ='0000-00-00')
and (td.DiscStatus = p_status or p_status is null)
or (td.t_disc_name REGEXP p_coupon or p_coupon is null)
or (td.DiscountName REGEXP p_coupon or p_coupon is null);
end*/

?>
