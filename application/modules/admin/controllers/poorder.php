<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Poorder extends MX_Controller{
  public function __construct(){
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('upload');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->load->library('form_validation');
    $this->userfunction->loginAdminvalidation();
  }

//------------------start purchase Order-----------------------

public function managepolist(){
 $parameter = array('act_mode'=>'viewpolist','row_id'=>'','orderstatus'=>'onprocess','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/managepolist?";
     $config['total_rows']       = count($data['record']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $data["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'viewpolist','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

     //----------------  end pagination setting ------------------------// 
 $this->load->view('helper/header');
 $this->load->view('purchaseorder/managepo',$data); 
}
public function podetail($venid){

	$parameterwwq        = array('act_mode'=>'viewallmanufac','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['manuvieww'] = $this->supper_admin->call_procedure('proc_orderflow',$parameterwwq);

 $parameter        = array('act_mode'=>'viewpodetail','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>$venid,'order_qty'=>'','oldstatus'=>'');
 $data['vieww'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 
 if($this->input->post('submitpo')) {

  $ponumber = 'PO'.$this->randnumber();
  $totpro=count($this->input->post('proApp'));
  $parameter        = array('act_mode'=>'poinsertT','row_id'=>$venid,'orderstatus'=>$ponumber,'order_proid'=>$totpro,'order_id'=>'','order_venid'=>$venid,'order_qty'=>'','oldstatus'=>'');
  $insertProView    = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter); 
  
  if($insertProView->lastid>0){
  foreach ($this->input->post('proApp') as $key => $value) {
    $vennnid = $this->input->post('vendid'.$value);
    //$qty       =$this->input->post('qty');
    $parameter1q   =array('act_mode'=>'pomapinsertdata','row_id'=>$insertProView->lastid,'ponumber'=>'','po_total'=>'','po_proid'=>$value,'po_manuid'=>$vennnid,'po_qty'=>'','po_price'=>'');
    //p($parameter1q);
    $inser = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameter1q);
   }
   //exit;
  }
  redirect('admin/poorder/podetail/'.$venid);
  
 }
 
  
 $this->load->view('helper/header');
 $this->load->view('purchaseorder/managepodetails',$data);
   
}

public function polist(){
 $parameter = array('act_mode'=>'polistdata','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 

 //----------------  Download PO Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
             if($this->input->post('proApp')!=''){
//p($this->input->post('proApp'));exit;
            $excelpoid=implode(',', $this->input->post('proApp'));
            $parameterexc  =array('act_mode'=>'excelmiltipowise','row_id'=>'','ponumber'=>$excelpoid,'po_total'=>'','po_proid'=>'','po_manuid'=>'','po_qty'=>'','po_price'=>'');            
//p($parameterexc);exit;
/*$parameterexc  = array('act_mode'=>'excelpo','row_id'=>'','ponumber'=>'','po_total'=>'','po_proid'=>'','po_manuid'=>'','po_qty'=>'','po_price'=>'');*/
            $recordexc = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameterexc);
            
            $finalExcelArr = array('S.NO.','PO NUMBER','VENDOR NAME','TOTAL PRODUCT','CREATED ON','MODIFIED ON','SKU NUMBER','PRODUCT NAME','UPDATED PURCHASE QUANTITY','TOTAL STOCK','SIZE','COLOR','PRODUCT MRP','OLD MANUFACTURE PRICE','NEW MANUFACTURE PRICE','MANUFACTURE TOTAL PRICE');

           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Purchase Order Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
            $k=1;
            foreach ($recordexc as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $k);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->ponumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->firstname." ".$value->lastname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->total);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->po_date);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->po_modify);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->po_skuno);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->po_proname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->postock);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->pomq);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->po_ordsize);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->po_ordcolor);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, number_format($value->prodmrp,1,".",""));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->po_finalprice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $value->poprice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->poprice*$value->postock);


            $k++;
            }
          }

          $filename='Purchase Orders Listing.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
       
        }else {
          $this->session->set_flashdata('message', 'Please check atleast one Purchase Order.');
          redirect('admin/poorder/polist');
        }

      }
      //----------------  End Download PO Excel ------------------------// 

//----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/polist?";
     $config['total_rows']       = count($data['record']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $data["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'polistdata','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

     //----------------  end pagination setting ------------------------// 
 $this->load->view('helper/header');
 $this->load->view('purchaseorder/poview',$data); 
}


/*public function poupdateqty($poid){
 
 $parameter1 = array('act_mode'=>'polistdetail','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter1);
 $parameter2 = array('act_mode'=>'polistdetail2','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record2'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter2);
 $i=0;
 foreach ($data['record'] as $key => $value) {

 	if($data['record2'][$i]->poprice==0){ 
	    $unitpri = $value->po_finalprice;
	    $totpri = $value->po_finalprice*$data['record2'][$i]->postock; 
	} else {
	    $unitpri = $data['record2'][$i]->poprice;
	    $totpri = $data['record2'][$i]->poprice*$data['record2'][$i]->postock;
	}
   $parameter1q   =array('act_mode'=>'poupdateqty','row_id'=>$poid,'ponumber'=>'','po_total'=>$totpri,'po_proid'=>$data['record2'][$i]->poproid,'po_manuid'=>$data['record2'][$i]->pomanuid,'po_qty'=>$data['record2'][$i]->postock,'po_price'=>$unitpri);
 	$inser = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameter1q);
    $i++;
 }
 
 redirect('admin/order/polist/');
 
}*/

public function polistdetail($poid){

 $parameter = array('act_mode'=>'polistdetail','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 $parameter = array('act_mode'=>'polistdetail2','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record2'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
//p($data['record']);exit();
 if($this->input->post('submitpo')) { 
 	foreach ($data['record2'] as $key => $value) {
 	//p($value->pomapid);
 		$unitpri = $this->input->post('sinamt');
 		$totpri = $this->input->post('sinamttotal');
 		$poqty = $this->input->post('qty');
	 	$parametersub = array('act_mode'=>'updatepolist','row_id'=>$value->pomapid,'orderstatus'=>'','order_proid'=>$poid,'order_id'=>$unitpri[$value->pomapid],'order_venid'=>$totpri[$value->pomapid],'order_qty'=>$poqty[$value->pomapid],'oldstatus'=>'');
	 	//p($parametersub);exit();
	 	$record = $this->supper_admin->call_procedure('proc_orderflow',$parametersub);
 	}
 	redirect('admin/order/polistdetail/'.$poid);
 	//exit();
 }

 $this->load->view('helper/header');
 $this->load->view('purchaseorder/podetails',$data); 
}

public function printpo($poid){
	//p($poid);exit();
 $parameter1 = array('act_mode'=>'polistdetail','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter1);
 $parameter2 = array('act_mode'=>'polistdetail2','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record2'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter2);
 $parameter3 = array('act_mode'=>'printpodet','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record3'] = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter3);
  $this->load->view('purchaseorder/poinvoice',$data);
}

//------------------end purchase Order-----------------------


public function randnumber(){
    srand ((double) microtime() * 1000000);
    $random5 = rand(10000,99999);
    return $random5;
}


public function poreceived($poid){
	//p("hello ".$poid);
	$parameter   =array('act_mode'=>'porecdata','row_id'=>$poid,'ponumber'=>'','po_total'=>'','po_proid'=>'','po_manuid'=>'','po_qty'=>'','po_price'=>'');
 	$record['porecdata'] = $this->supper_admin->call_procedureRow('proc_orderpoinsert',$parameter);
//p($record['porecdata']);exit;
	
	$totqty = $record['porecdata']->totqty;
	$venid = $record['porecdata']->pomanufid;
	$po_number = $record['porecdata']->ponumber;

	if($this->input->post('posubmit')) { 

		//--------------- update Quantity ----------------------------

		$parameter1 = array('act_mode'=>'polistdetail','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
		 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter1);
		 $parameter2 = array('act_mode'=>'polistdetail2','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
		 $data['record2'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter2);
		 $i=0;
		 foreach ($data['record'] as $key => $value) {

		 	if($data['record2'][$i]->poprice==0){ 
			    $unitpri = $value->po_finalprice;
			    $totpri = $value->po_finalprice*$data['record2'][$i]->postock; 
			} else {
			    $unitpri = $data['record2'][$i]->poprice;
			    $totpri = $data['record2'][$i]->poprice*$data['record2'][$i]->postock;
			}
		   $parameter1q   =array('act_mode'=>'poupdateqty','row_id'=>$poid,'ponumber'=>'','po_total'=>$totpri,'po_proid'=>$data['record2'][$i]->poproid,'po_manuid'=>$data['record2'][$i]->pomanuid,'po_qty'=>$data['record2'][$i]->postock,'po_price'=>$unitpri);
		 	$inser = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameter1q);
		    $i++;
		 }
		//--------------- end upadte quantity --------------------------



		//$pochallan  	= $this->input->post('pochallan');
		$pospinvoiceno 	= $this->input->post('pospinvoiceno');
		//$pobillrate 	= $this->input->post('pobillrate');
		//$pobillamt 		= $this->input->post('pobillamt');
		$podiscount 	= $this->input->post('podiscount');
		$ponetamt 		= $this->input->post('ponetamt');
		$pocst 			= $this->input->post('pocst');
		$pototamt 		= $this->input->post('pototamt');
		$pospbillimg 	= $_FILES['pospbillimg']['name'];

     $field_name_img    = 'pospbillimg';
     $img_file          = $this->image_upload($field_name_img);

     $allowedExts       = array("gif","jpeg","jpg","png");
     $temp              = explode(".",$pospbillimg);
     $extension         = end($temp);
     //echo $extension;exit();
     if (in_array($extension, $allowedExts)){

		$parameterpo   	= array('act_mode'=>'porecinsert',
								'row_id'=>$poid,
								'por_number'=>$po_number,
								'por_challan'=>'',
								'por_venid'=>$venid,
								'por_veninvoice'=>$pospinvoiceno,
								'por_totqty'=>$totqty,
								'por_billrate'=>'',
								'por_billamt'=>'',
								'por_billdiscount'=>$podiscount,
								'por_netamt'=>$ponetamt,
								'por_tax'=>$pocst,
								'por_totamt'=>$pototamt,
								'por_invoiceimg'=>time().$pospbillimg
								);

		$records['porecinsert'] = $this->supper_admin->call_procedureRow('proc_orderporeceive',$parameterpo);
		$this->session->set_flashdata('message', 'Your information was successfully Saved.');
		redirect('admin/poorder/polist');
		
	 } else {

      $parameterpo    = array('act_mode'=>'porecinsert',
                'row_id'=>$poid,
                'por_number'=>$po_number,
                'por_challan'=>'',
                'por_venid'=>$venid,
                'por_veninvoice'=>$pospinvoiceno,
                'por_totqty'=>$totqty,
                'por_billrate'=>'',
                'por_billamt'=>'',
                'por_billdiscount'=>$podiscount,
                'por_netamt'=>$ponetamt,
                'por_tax'=>$pocst,
                'por_totamt'=>$pototamt,
                'por_invoiceimg'=>''
                );
//p($parameterpo);exit;
    $records['porecinsert'] = $this->supper_admin->call_procedureRow('proc_orderporeceive',$parameterpo);
    $this->session->set_flashdata('message', 'Your information was successfully Saved.');
    redirect('admin/poorder/polist');

   }

	}


	$this->load->view('helper/header');
 	$this->load->view('purchaseorder/poreceived',$record); 
}


 public function image_upload($field_name){

    $config['upload_path']   = './assets/supplierinvoice/';
    $config['allowed_types'] = 'jpg|jpeg|gif|png';
    $config['max_size']      = '10000000';
    $config['file_name']     = time().$_FILES[$field_name]['name'];
    
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload($field_name)){
        $data['error']  = array('error' => $this->upload->display_errors());
    } else {
        $data['name']   = array('upload_data' => $this->upload->data());
    }
    return $data;
  }
  

  public function purchasereport(){

  	    $parameterpo   	= array('act_mode'=>'porecinsert',
								'row_id'=>$poid,
								'por_number'=>$po_number,
								'por_challan'=>'',
								'por_venid'=>$venid,
								'por_veninvoice'=>$pospinvoiceno,
								'por_totqty'=>$totqty,
								'por_billrate'=>'',
								'por_billamt'=>'',
								'por_billdiscount'=>$podiscount,
								'por_netamt'=>$ponetamt,
								'por_tax'=>$pocst,
								'por_totamt'=>$pototamt,
								'por_invoiceimg'=>time().$pospbillimg
								);

		$records['porecinsert'] = $this->supper_admin->call_procedureRow('proc_orderporeceive',$parameterpo);
    
  }


public function closedpo($poid){
  $parameter  =array('act_mode'=>'updatepoclosed','row_id'=>$poid,'ponumber'=>'','po_total'=>'','po_proid'=>'','po_manuid'=>'','po_qty'=>'','po_price'=>'');
  $record = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameter);
  $this->session->set_flashdata('message', 'Purchase Order Closed Successfully.');
  redirect('admin/poorder/polist');
}


public function closedpolist(){
  $parameter  =array('act_mode'=>'poclosed','row_id'=>'','ponumber'=>'','po_total'=>'','po_proid'=>'','po_manuid'=>'','po_qty'=>'','po_price'=>'');
  $data['record'] = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameter);

  //----------------  Download PO Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
            $parameterexc  =array('act_mode'=>'excelpoclosed','row_id'=>'','ponumber'=>'','po_total'=>'','po_proid'=>'','po_manuid'=>'','po_qty'=>'','po_price'=>'');
            $recordexc = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameterexc);

            $finalExcelArr = array('S.NO.','PO NUMBER','VENDOR NAME','TOTAL PRODUCT','CREATED ON','MODIFIED ON','SKU NUMBER','PRODUCT NAME','UPDATED PURCHASE QUANTITY','TOTAL STOCK','SIZE','COLOR','PRODUCT MRP','OLD MANUFACTURE PRICE','NEW MANUFACTURE PRICE','MANUFACTURE TOTAL PRICE');

           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Closed Purchase Order Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
            $k=1;
            foreach ($recordexc as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $k);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->ponumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->firstname." ".$value->lastname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->total);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->po_date);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->po_modify);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->po_skuno);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->po_proname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->postock);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->pomq);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->po_ordsize);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->po_ordcolor);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, number_format($value->prodmrp,1,".",""));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->po_finalprice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $value->poprice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->poprice*$value->postock);


            $k++;
            }
          }

          $filename='Closed Purchase Orders Listing.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
      }
      //----------------  End Download PO Excel ------------------------// 



  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/poorder/closedpolist?";
     $config['total_rows']       = count($data['record']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $data["links"]  = explode('&nbsp;',$str_links );
     $parameter  =array('act_mode'=>'poclosed','row_id'=>$page,'ponumber'=>'','po_total'=>'','po_proid'=>$second,'po_manuid'=>'','po_qty'=>'','po_price'=>'');
  $data['record'] = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameter);

     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('purchaseorder/closedpoview',$data);
}

public function closepolistdetail($poid){

 $parameter = array('act_mode'=>'polistdetail','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 $parameter = array('act_mode'=>'polistdetail2','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record2'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 $this->load->view('helper/header');
 $this->load->view('purchaseorder/closedpodetails',$data); 
}

}// class
?>