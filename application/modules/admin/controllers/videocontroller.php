<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class videocontroller extends MX_Controller{
  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }

  //............. View VIDEO ............... //
public function index(){
   $userid = $this->session->userdata('bizzadmin')->LoginID;
  //----------------------multiple delete -------------------------------//
  $this->userfunction->loginAdminvalidation();
    if($this->input->post('submit'))
    {

     foreach ($this->input->post( 'attdelete') as $key => $value) {

      $parameter          = array(
      'act_mode'=>'attdelete',
      'Param1'=>$value,
      'Param2'=>$userid,
      'Param3'=>'',
      'Param4'=>'',
      'Param5'=>'',
      'Param6'=>'',
      'Param7'=>'',
      'Param8'=>''
      );
      //print_r($parameter);
      $response['vieww']   = $this->supper_admin->call_procedure('proc_videos',$parameter); 
     }


     $this->session->set_flashdata("message", "Your video successfully deleted.");
     redirect("admin/videocontroller/index");
    }
   //---------------------------end Delete---------------------------------//
   //--------------------------Change ststus ------------------------------//

    if($this->input->post('submitstatus')){
      
      foreach ($this->input->post( 'attdelete') as $key => $value) {

        $status    = $this->input->post('attstatu')[$value];
        $act_mode  = $status=='1'?'inactivevideo':'activevideo';

        $parameter = array(
          'act_mode'=>$act_mode,
          'Param1'=>$value,
          'Param2'=>$userid,
          'Param3'=>'',
          'Param4'=>'',
          'Param5'=>'',
          'Param6'=>'',
          'Param7'=>'',
          'Param8'=>''
          );
        $response['vieww'] = $this->supper_admin->call_procedure('proc_videos',$parameter);
      }

      print_r($parameter);
      
      $this->session->set_flashdata("message", "Your video successfully deleted.");
      redirect("admin/videocontroller/index");
    }
    
    //-------------------------------end ----------------------------------------//

    $parameter          = array(
      'act_mode'=>'get_all_videos',
      'Param1'=>'',
      'Param2'=>'',
      'Param3'=>'',
      'Param4'=>'',
      'Param5'=>'',
      'Param6'=>'',
      'Param7'=>'',
      'Param8'=>''
      );
     $response['vieww']   = $this->supper_admin->call_procedure('proc_videos',$parameter);
           
     //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/videocontroller/index?";
     $config['total_rows']       = count($response['vieww']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

     //----------------  end pagination setting ------------------------// 
    
    $this->load->view('helper/header');
    $this->load->view('videos/viewvideos',$response);

    }

  function addvideo()
  {
    $this->load->view('helper/header');
    $this->load->view('videos/addvideo',$response);
  }

  function upload_video()
  {
                echo $config['upload_path']          = site_url().'videos/';
                $config['allowed_types']        = 'GIF|mp4|3gp';// |webm|mkv |flv|3gp|avi|mp4
                $config['max_size']             = 1000000;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('add_video'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        print_r($error); exit;
                        //$this->session->set_flashdata("error", "Something wromg! Please try again");
                       // redirect("admin/videocontroller/addvideo");                  

                }
                else
                {
                        $userid = $this->session->userdata('bizzadmin')->LoginID;
                        $video_name = $_FILES['add_video']['name'];
                        $parameter = array(
                          'act_mode'=>'insert',
                          'Param1'=>$video_name,
                          'Param2'=>$userid,
                          'Param3'=>'',
                          'Param4'=>'',
                          'Param5'=>'',
                          'Param6'=>'',
                          'Param7'=>'',
                          'Param8'=>''
                          );
                         $response['vieww']   = $this->supper_admin->call_procedure('proc_videos',$parameter);

                        $data = array('upload_data' => $this->upload->data());
                        $this->session->set_flashdata("message", "Your Video successfully Updated.");
                        redirect("admin/videocontroller/index");
                }
    }

}// end class
?>