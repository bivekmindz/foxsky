<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Bannergallery extends MX_Controller{

//............. Default Construct function ............... //
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('upload');
    $this->userfunction->loginAdminvalidation();
    
  
  }// end function.

//............. Slider Manager ............... //
  public function addslider(){

    $parameters = array(
                    'act_mode'  => 'viewslider',
                    'row_id'    => '',
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => 'mainslider',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );

    $record['viewslide'] = $this->supper_admin->call_procedure('proc_homebanner',$parameters);

    if($this->input->post('submit')){
     $userid = $this->session->userdata('bizzadmin')->LoginID; 
     $imgurl            = $this->input->post('imgurl');
     $imgslide          = str_replace(' ', '', $_FILES['imgslide']['name']);
          
     $field_name_img    = 'imgslide';
     $img_file          = $this->image_upload($field_name_img);

     $allowedExts       = array("gif","jpeg","jpg","png");
     $temp              = explode(".",$imgslide);
     $extension         = end($temp);
     //echo $extension;exit();
     if (in_array($extension, $allowedExts)){

        $parameter      = array(
                            'act_mode'  => 'insertslider',
                            'row_id'    => '',
                            'imagename' => time().$imgslide,
                            'imageurl'  => $imgurl,
                            'imgtype'   => 'mainslider',
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status'=> $userid
                          );

        $record = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
        $this->session->set_flashdata('messag', 'Your information was successfully Saved.');
        //echo "<script>location.href=".base_url()."admin/bannergallery/addslider</script>";
        redirect('admin/bannergallery/addslider','refresh');
     }
     else{        
        $this->session->set_flashdata('messag', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
        redirect('admin/bannergallery/addslider');
     }
    }

    $this->load->view('helper/header');
    $this->load->view('banner/addimgslider', $record);
  
  }// end function.

//............. Update Slider ............... //
public function updateslider($id) {
    $parameters     = array(
                        'act_mode'   => 'viewsingleslider',
                        'row_id'     => $id,
                        'imagename'  => '',
                        'imageurl'   => '',
                        'imgtype'    => '',
                        'img_head'   => '',
                        'img_shead'  => '',
                        'img_status' => ''
                        );
    $record['view'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameters);

    if($this->input->post('submit')){
     $userid = $this->session->userdata('bizzadmin')->LoginID; 
     $imgurl            = $this->input->post('imgurl');
     $imgslide          = str_replace(' ', '', $_FILES['imgslide']['name']);
          
     $field_name_img    = 'imgslide';
     $img_file          = $this->image_upload($field_name_img);

     $allowedExts       = array("gif","jpeg","jpg","png");
     $temp              = explode(".",$imgslide);
     $extension         = end($temp);
     //echo $extension;exit();

     if(!empty($imgslide)){
         if (in_array($extension, $allowedExts)){

            unlink(FCPATH."assets/webapp/img/".$record['view']->t_imgname);
            $parameter      = array(
                                'act_mode'  =>'updatemidbanbottom',
                                'row_id'    =>$id,
                                'imagename'  =>time().$imgslide,
                                'imageurl' =>$imgurl,
                                'imgtype'   =>'',
                                'img_head'   =>'',
                                'img_shead'   =>'',
                                'img_status'   =>$userid
                                );
            $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/addslider');
            
         }
         else{            
            $this->session->set_flashdata('message2', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            redirect('admin/bannergallery/addslider');
         }
        } 
        else{
            $parameter      = array(
                                'act_mode'  => 'updatemidbanbottom',
                                'row_id'    => $id,
                                'imagename' => $record['view']->t_imgname,
                                'imageurl'  => $imgurl,
                                'imgtype'   => '',
                                'img_head'  => '',
                                'img_shead' => '',
                                'img_status'=> $userid
                              );

            $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/addslider');
        }     
    }

    $this->load->view('helper/header');
    $this->load->view('banner/updateimgslider',$record);
  }

//............. Update Slider ............... //
  public function image_upload($field_name){

    $config['upload_path']   = './assets/webapp/img/';
    $config['allowed_types'] = 'jpg|jpeg|gif|png';
    $config['max_size']      = '10000000';
    $config['file_name']     = time().str_replace(' ', '', $_FILES[$field_name]['name']);
    
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload($field_name)){
        $data['error']  = array('error' => $this->upload->display_errors());
    } else {
        $data['name']   = array('upload_data' => $this->upload->data());
    }
    return $data;
  }

//............. Delete Slider ............... //
  public function deleteslider($id) {
    
    $parameters     = array(
                        'act_mode'  => 'viewsingleslider',
                        'row_id'    => $id,
                        'imagename' => '',
                        'imageurl'  => '',
                        'imgtype'   => '',
                        'img_head'  => '',
                        'img_shead' => '',
                        'img_status'=> ''
                      );
    $record['view'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameters);
    unlink(FCPATH."assets/webapp/img/".$record['view']->t_imgname);

    $parameter      = array(
                    'act_mode'   => 'delslider',
                    'row_id'     => $id,
                    'imagename'  => '',
                    'imageurl'   => '',
                    'imgtype'    => '',
                    'img_head'   => '',
                    'img_shead'  => '',
                    'img_status' => ''
                    );

    $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect('admin/bannergallery/addslider');
  }

//............. Slider Status ............... //
  public function statusslider(){
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='A'?'banactive':'baninactive';
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $parameter     = array(
                    'act_mode'  => $act_mode,
                    'row_id'    => $rowid,
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> $userid
                    );
    $record        = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
    redirect(base_url().'admin/bannergallery/addslider');
  }  

//............. End Slider Manager ............... //

//............. Right Banner Manager ............... //
public function addrightbanner(){

    $parameters     = array(
                        'act_mode'  =>'viewslider',
                        'row_id'    =>'',
                        'imagename'  =>'',
                        'imageurl' =>'',
                        'imgtype'   =>'rightban',
                        'img_head'   =>'',
                        'img_shead'   =>'',
                        'img_status'   =>''
                      );
    $record['viewban'] = $this->supper_admin->call_procedure('proc_homebanner',$parameters);

    if($this->input->post('submit')){
     $userid = $this->session->userdata('bizzadmin')->LoginID; 
     $banurl        = $this->input->post('banurl');
     $banimg        = str_replace(' ', '', $_FILES['banimg']['name']);
          
     $field_name_img = 'banimg';
     $img_file       = $this->img_upload($field_name_img);

     $allowedExts    = array("gif","jpeg","jpg","png");
     $temp           = explode(".",$banimg);
     $extension      = end($temp);
     //echo $extension;exit();
     if (in_array($extension, $allowedExts)){

        $parameter   = array(
                        'act_mode'   => 'insertslider',
                        'row_id'     => '',
                        'imagename'  => time().$banimg,
                        'imageurl'   => $banurl,
                        'imgtype'    => 'rightban',
                        'img_head'   => '',
                        'img_shead'  => '',
                        'img_status' => $userid
                       );

        $record = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
        $this->session->set_flashdata('messag', 'Your information was successfully Saved.');
        redirect('admin/bannergallery/addrightbanner');
        
     } 
     else{
        
        $this->session->set_flashdata('messag', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
        redirect('admin/bannergallery/addrightbanner');
     }
    }

    $this->load->view('helper/header');
    $this->load->view('banner/addrightban',$record);
  
  }// end function.
 
//............. Update Right Banner ............... //
  public function updaterightbanner($id) {
    $parameters     = array(
                        'act_mode'   => 'viewsingleslider',
                        'row_id'     => $id,
                        'imagename'  => '',
                        'imageurl'   => '',
                        'imgtype'    => '',
                        'img_head'   => '',
                        'img_shead'  => '',
                        'img_status' => ''
                      );
    $record['view'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameters);

    if($this->input->post('submit')){
     $userid = $this->session->userdata('bizzadmin')->LoginID; 
     $imgurl          = $this->input->post('imgurl');
     $imgslide        = str_replace(' ', '', $_FILES['imgslide']['name']);
          
     $field_name_img  ='imgslide';
     $img_file        = $this->img_upload($field_name_img);

     $allowedExts     = array("gif","jpeg","jpg","png");
     $temp            = explode(".",$imgslide);
     $extension       = end($temp);
     //echo $extension;exit();
     if(!empty($imgslide)){

         if (in_array($extension, $allowedExts)){

            unlink(FCPATH."assets/webapp/images/".$record['view']->t_imgname);
            $parameter  = array(
                            'act_mode'   => 'updatemidbanbottom',
                            'row_id'     => $id,
                            'imagename'  => time().$imgslide,
                            'imageurl'   => $imgurl,
                            'imgtype'    => '',
                            'img_head'   => '',
                            'img_shead'  => '',
                            'img_status' => $userid
                          );
            $record     = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/addrightbanner');
            
         } 
         else{   
            $this->session->set_flashdata('message2', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            redirect('admin/bannergallery/addrightbanner');
         }
        } 
        else{
            $parameter = array(
                            'act_mode'  => 'updatemidbanbottom',
                            'row_id'    => $id,
                            'imagename' => $record['view']->t_imgname,
                            'imageurl'  => $imgurl,
                            'imgtype'   => '',
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status' => $userid
                            );
            $record    = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/addrightbanner');
        }     
    }

    $this->load->view('helper/header');
    $this->load->view('banner/updaterightban',$record);
  }

//............. Delete Right Banner ............... //
  public function deleterightban($id) {
    
    $parameters     = array(
                        'act_mode'  => 'viewsingleslider',
                        'row_id'    => $id,
                        'imagename' => '',
                        'imageurl'  => '',
                        'imgtype'   => '',
                        'img_head'  => '',
                        'img_shead' => '',
                        'img_status'=> ''
                        );
    $record['view'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameters);
    unlink(FCPATH."assets/webapp/images/".$record['view']->t_imgname);

    $parameter      = array(
                        'act_mode'  => 'delslider',
                        'row_id'    => $id,
                        'imagename' => '',
                        'imageurl'  => '',
                        'imgtype'   => '',
                        'img_head'  => '',
                        'img_shead' => '',
                        'img_status'=> ''
                        );
    $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect('admin/bannergallery/addrightbanner');
  }

//............. Right Banner Status Change ............... //
  public function statusrightban(){
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='A'?'banactive':'baninactive';
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $parameter     = array(
                        'act_mode'  => $act_mode,
                        'row_id'    => $rowid,
                        'imagename' => '',
                        'imageurl'  => '',
                        'imgtype'   => '',
                        'img_head'  => '',
                        'img_shead' => '',
                        'img_status'=> $userid
                    );
    $record        = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
    redirect(base_url().'admin/bannergallery/addrightbanner');
  }
//............. End Right Banner Manager ............... //


//............. Bizzgain Selection Banner Manager ............... //
public function Middlebanner(){
    $parameter1 = array(
                    'act_mode'  => 'viewslider',
                    'row_id'    => '',
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => 'selectone',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );
    $record['banone'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameter1);


    //p($record['banone']->t_imgname);exit();
    $parameter2 = array(
                    'act_mode'  => 'viewslider',
                    'row_id'    => '',
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => 'selecttwo',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );
    $record['bantwo'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameter2);
    $parameter3       = array(
                            'act_mode'  => 'viewslider',
                            'row_id'    => '',
                            'imagename' => '',
                            'imageurl'  => '',
                            'imgtype'   => 'selectthree',
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status'=> ''
                            );
    $record['banthree'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameter3);
    $parameter4         = array(
                            'act_mode'  => 'viewslider',
                            'row_id'    => '',
                            'imagename' => '',
                            'imageurl'  => '',
                            'imgtype'   => 'selectfour',
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status'=> ''
                            );
    $record['banfour'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameter4);


    //----------- Banner first -----------------

    if($this->input->post('submitone')){

     $imghead1       = $this->input->post('imghead1');
     $imgshead1      = $this->input->post('imgshead1');
     $imgurl1        = $this->input->post('imgurl1');
     $banimg1        = str_replace(' ', '', $_FILES['banimg1']['name']);
          
     $field_name_img1  = 'banimg1';
     $img_file1        = $this->img_upload($field_name_img1);

     $allowedExts   = array("gif","jpeg","jpg","png");
     $temp          = explode(".",$banimg1);
     $extension     = end($temp);
     //echo $extension;exit();
     if(!empty($banimg1)){

         if (in_array($extension, $allowedExts)){

            unlink(FCPATH."assets/webapp/images/".$record['banone']->t_imgname);
            $parameter = array(
                            'act_mode'  => 'updatemidban',
                            'row_id'    => $record['banone']->n_imgid,
                            'imagename' => time().$banimg1,
                            'imageurl'  => $imgurl1,
                            'imgtype'   => '',
                            'img_head'  => $imghead1,
                            'img_shead' => $imgshead1,
                            'img_status'=> ''
                        );

          //  pend(  $parameter);
            $record     = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message1', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/Middlebanner');
            
         } 
         else{
            
            $this->session->set_flashdata('message1', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            redirect('admin/bannergallery/Middlebanner');
         }
        } 
        else{

            $parameter = array(
                            'act_mode'  => 'updatemidban',
                            'row_id'    => $record['banone']->n_imgid,
                            'imagename' => $record['banone']->t_imgname,
                            'imageurl'  => $imgurl1,
                            'imgtype'   => '',
                            'img_head'  => $imghead1,
                            'img_shead' => $imgshead1,
                            'img_status'=> ''
                        );
            $record    = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message1', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/Middlebanner');

        }
    }

    if($this->input->post('submittwo')){

     $imghead2       = $this->input->post('imghead2');
     $imgshead2      = $this->input->post('imgshead2');
     $imgurl2        = $this->input->post('imgurl2');
     $banimg2        = str_replace(' ', '', $_FILES['banimg2']['name']);
          
     $field_name_img2 ='banimg2';
     $img_file2       = $this->img_upload($field_name_img2);

     $allowedExts     = array("gif","jpeg","jpg","png");
     $temp            = explode(".",$banimg2);
     $extension       = end($temp);
     //echo $extension;exit();
     if(!empty($banimg2)){

         if (in_array($extension, $allowedExts)){

            unlink(FCPATH."assets/webapp/images/".$record['bantwo']->t_imgname);
            $parameter = array(
                            'act_mode'  =>'updatemidban',
                            'row_id'    =>$record['bantwo']->n_imgid,
                            'imagename'  =>time().$banimg2,
                            'imageurl' =>$imgurl2,
                            'imgtype'   =>'',
                            'img_head'   =>$imghead2,
                            'img_shead'   =>$imgshead2,
                            'img_status'   =>''
                          );
            $record    = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message2', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/Middlebanner');
            
         } 
         else{
            
            $this->session->set_flashdata('message2', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            redirect('admin/bannergallery/Middlebanner');
         }
        } 
        else{

            $parameter = array(
                        'act_mode'  => 'updatemidban',
                        'row_id'    => $record['bantwo']->n_imgid,
                        'imagename' => $record['bantwo']->t_imgname,
                        'imageurl'  => $imgurl2,
                        'imgtype'   => '',
                        'img_head'  => $imghead2,
                        'img_shead' => $imgshead2,
                        'img_status'=> ''
                        );
            $record    = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message2', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/Middlebanner');

        }     
    }

    //----------- End Banner second -----------------

    //----------- Banner third -----------------

    if($this->input->post('submitthree')){

     $imghead3       = $this->input->post('imghead3');
     $imgshead3      = $this->input->post('imgshead3');
     $imgurl3        = $this->input->post('imgurl3');
     $banimg3        = str_replace(' ', '', $_FILES['banimg3']['name']);
          
     $field_name_img3  = 'banimg3';
     $img_file3        = $this->img_upload($field_name_img3);

     $allowedExts    = array("gif","jpeg","jpg","png");
     $temp           = explode(".",$banimg3);
     $extension      = end($temp);
     //echo $extension;exit();
     if(!empty($banimg3)){

         if (in_array($extension, $allowedExts)){

            unlink(FCPATH."assets/webapp/images/".$record['banthree']->t_imgname);
            $parameter = array(
                            'act_mode'  => 'updatemidban',
                            'row_id'    => $record['banthree']->n_imgid,
                            'imagename' => time().$banimg3,
                            'imageurl'  => $imgurl3,
                            'imgtype'   => '',
                            'img_head'  => $imghead3,
                            'img_shead' => $imgshead3,
                            'img_status'=> ''
                        );
            $record = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message3', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/Middlebanner');
            
         } 
         else{
            
            $this->session->set_flashdata('message3', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            redirect('admin/bannergallery/Middlebanner');
         }
        } 
        else{

            $parameter = array(
                            'act_mode'  => 'updatemidban',
                            'row_id'    => $record['banthree']->n_imgid,
                            'imagename' => $record['banthree']->t_imgname,
                            'imageurl'  => $imgurl3,
                            'imgtype'   => '',
                            'img_head'  => $imghead3,
                            'img_shead' => $imgshead3,
                            'img_status'=> ''
                        );
            $record    = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message3', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/Middlebanner');

        }     
    }

    //----------- End Banner third -----------------

    //----------- Banner fourth -----------------

    if($this->input->post('submitfour')){

     $imgurl4        = $this->input->post('imgurl4');
     $banimg4        = str_replace(' ', '', $_FILES['banimg4']['name']);
          
     $field_name_img4='banimg4';
     $img_file4      = $this->img_upload($field_name_img4);

     $allowedExts    = array("gif","jpeg","jpg","png");
     $temp           = explode(".",$banimg4);
     $extension      = end($temp);
     //echo $extension;exit();
     if(!empty($banimg4)){

         if (in_array($extension, $allowedExts)){

            unlink(FCPATH."assets/webapp/images/".$record['banfour']->t_imgname);
            $parameter = array(
                            'act_mode'  => 'updatemidbanbottom',
                            'row_id'    => $record['banfour']->n_imgid,
                            'imagename' => time().$banimg4,
                            'imageurl'  => $imgurl4,
                            'imgtype'   => '',
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status'=> ''
                        );
            $record = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message4', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/Middlebanner');
            
         } else {
            
            $this->session->set_flashdata('message4', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            redirect('admin/bannergallery/Middlebanner');
         }
        } else {

            $parameter = array(
                            'act_mode'   => 'updatemidbanbottom',
                            'row_id'     => $record['banfour']->n_imgid,
                            'imagename'  => $record['banfour']->t_imgname,
                            'imageurl'   => $imgurl4,
                            'imgtype'    => '',
                            'img_head'   => '',
                            'img_shead'  => '',
                            'img_status' => ''
                        );
            $record     = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message4', 'Your information was successfully Updated.');
            redirect('admin/bannergallery/Middlebanner');

        }     
    }
    
    //----------- End Banner fourth -----------------

    $this->load->view('helper/header');
    $this->load->view('banner/selectionmiddleban',$record);
  
  }// end function.



//............. End Bizzgain Selection Banner Manager ............... //


//............. Image Upload ............... //
  public function img_upload($field_name){

    $config['upload_path']   = './assets/webapp/images/';
    $config['allowed_types'] = 'jpg|jpeg|gif|png';
    $config['max_size']      = '10000000';
    $config['file_name']     = time().str_replace(' ', '', $_FILES[$field_name]['name']);
    
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload($field_name)){
        $data['error']       = array('error' => $this->upload->display_errors());
    } else {
        $data['name']        = array('upload_data' => $this->upload->data());
    }
    return $data;
  }


  public function view_video_list(){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    if($this->input->post('submit')){
      $h_video=time().str_replace(' ', '', $_FILES['h_video']['name']);
      $videoFileType = pathinfo($h_video,PATHINFO_EXTENSION);
      /*p($videoFileType);
      p(FCPATH.'assets/webapp/img/'.$h_video);
      p($_FILES);exit;*/

      if($videoFileType=="wmv" || $videoFileType=="3gp" || $videoFileType=="mp4" || $videoFileType=="avi" || $videoFileType=="MP4") {
        $parameters = array('act_mode'  => 'addvideo',
                            'row_id'    => $userid,
                            'imagename' => $h_video,
                            'imageurl'  => '',
                            'imgtype'   => '',
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status'=> ''
                            );
        //p($parameters);exit;
        $insertvideo = $this->supper_admin->call_procedure('proc_homebanner',$parameters);
        move_uploaded_file($_FILES['h_video']['tmp_name'], FCPATH.'assets/webapp/img/'.$h_video);
        $this->session->set_flashdata('messag', 'Your information was successfully inserted.');
        redirect('admin/bannergallery/view_video_list');
      } else {
        $this->session->set_flashdata('er_msg', 'Please upload only wmv, mp4 & avi files.');
        redirect('admin/bannergallery/view_video_list');
      }
    }

    $param  = array('act_mode'  => 'viewvideo',
                    'row_id'    => '',
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );
    $record['view_video'] = $this->supper_admin->call_procedure('proc_homebanner',$param);

    $this->load->view('helper/header');
    $this->load->view('banner/view_video',$record);
  }

  public function deletevideo($id){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $param  = array('act_mode'  => 'del_video',
                    'row_id'    => $id,
                    'imagename' => '',
                    'imageurl'  => $userid,
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );
    $record['view_video'] = $this->supper_admin->call_procedure('proc_homebanner',$param);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect('admin/bannergallery/view_video_list');
  }

  public function statusvideo(){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='0'?'active_video':'inactive_video';
    $param  = array('act_mode'  => $act_mode,
                    'row_id'    => $rowid,
                    'imagename' => '',
                    'imageurl'  => $userid,
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );
    $record['view_video'] = $this->supper_admin->call_procedure('proc_homebanner',$param);
    $this->session->set_flashdata('message', 'Your status was successfully updated.');
    redirect('admin/bannergallery/view_video_list');
  }

  public function updatevideo($id){
    $userid = $this->session->userdata('bizzadmin')->LoginID;
    $param  = array('act_mode'  => 'view_singlevideo',
                    'row_id'    => $id,
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );
    $record['view_svideo'] = $this->supper_admin->call_procedureRow('proc_homebanner',$param);

    if($this->input->post('submit')){
      $h_video=time().str_replace(' ', '', $_FILES['h_video']['name']);
      $videoFileType = pathinfo($h_video,PATHINFO_EXTENSION);
      //p(FCPATH."assets/webapp/img/".$record['view_svideo']->v_filename);exit;
      if($videoFileType=="wmv" || $videoFileType=="3gp" || $videoFileType=="mp4" || $videoFileType=="avi" || $videoFileType=="MP4") {
        unlink(FCPATH."assets/webapp/img/".$record['view_svideo']->v_filename);
        $parameters = array('act_mode'  => 'updatevideo',
                            'row_id'    => $id,
                            'imagename' => $h_video,
                            'imageurl'  => $userid,
                            'imgtype'   => '',
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status'=> ''
                            );
        //p($parameters);exit;
        $insertvideo = $this->supper_admin->call_procedure('proc_homebanner',$parameters);
        move_uploaded_file($_FILES['h_video']['tmp_name'], FCPATH.'assets/webapp/img/'.$h_video);
        $this->session->set_flashdata('message', 'Your information was successfully updated.');
        redirect('admin/bannergallery/view_video_list');
      } else {
        $this->session->set_flashdata('er_msg', 'Please upload only wmv, mp4 & avi files.');
        redirect('admin/bannergallery/updatevideo/'.$id);
      }
    }

    $this->load->view('helper/header');
    $this->load->view('banner/update_video',$record);
  }


 }//end class


?>