<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Weight extends MX_Controller{
  public function __construct(){
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }

 
//---------------------- Add/View Weight  -------------------------//

  public function viewweight(){
    $this->load->library('pagination'); // load pagination library.
    $this->userfunction->loginAdminvalidation();

  if($this->input->post('submit')){
   
       $title      = $this->input->post('title');
       $unit       = $this->input->post('unit');
       $value      = $this->input->post('value');
       $parameter = array('act_mode'=>'weightinsert','row_id'=>'','title'=>$title,'unit'=>$unit, 'value'=>$value);
       $response  = $this->supper_admin->call_procedure('proc_weight_des',$parameter);
       $this->session->set_flashdata('message', 'Your information was successfully Saved.');
       redirect(base_url().'admin/weight/viewweight'); 
  }//end if

    $parameter = array('act_mode'=>'showweight','row_id'=>'','title'=>'','unit'=>'', 'value'=>'');
    $response['vieww'] = $this->supper_admin->call_procedure('proc_weight_des',$parameter);

    //----------------  Download Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Title','Unit','Value');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Weight  Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($response['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

                      

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->title);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->unit);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->value);
            
            }
          }

          $filename='Weight.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------// 
    
    $this->load->view('helper/header');
    $this->load->view('weight/viewweight',$response);

  }

//---------------------- Weight Update -------------------------//
public function weightupdate($id){

  if($this->input->post('submit')){
   
       $title      = $this->input->post('title');
       $unit       = $this->input->post('unit');
       $value      = $this->input->post('value');

       $parameter = array('act_mode'=>'weightupdate','row_id'=>$id,'title'=>$title,'unit'=>$unit,'value'=>$value);
       $response  = $this->supper_admin->call_procedure('proc_weight_des',$parameter);
     $this->session->set_flashdata('message1', 'Your information was successfully Update.');
     redirect(base_url().'admin/weight/viewweight');
   }
  

  $parameter         = array('act_mode'=>'viewweightid','row_id'=>$id,'title'=>'','unit'=>'', 'value'=>'');
  $response['vieww'] = $this->supper_admin->call_procedureRow('proc_weight_des',$parameter);
  $this->load->view('helper/header');
  $this->load->view('weight/updateweight',$response);
}

//---------------------- Weight Delete  -------------------------//
public function weightdelete($id){
    $parameter = array('act_mode'=>'deleteweight','row_id'=>$id,'title'=>'','unit'=>'', 'value'=>'');
    $response  = $this->supper_admin->call_procedure('proc_weight_des',$parameter);
    $this->session->set_flashdata('message1', 'Your information was successfully deleted.');
    redirect(base_url().'admin/weight/viewweight');
}

//---------------------- Weight Status  -------------------------//
public function weightstatus (){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='A'?'activeandinactive':'inactiveandinactive';
  $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'title'=>'','unit'=>'', 'value'=>'');
  $response      = $this->supper_admin->call_procedure('proc_weight_des',$parameter);
  $this->session->set_flashdata('message1', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/weight/viewweight');
}

}// class

?>