<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Partialpay extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    
  }
  
  public function index($ordid){

    $parameter=array('act_mode'=>'orderdetail','row_id'=>$ordid,'ppy_orderid'=>'','ppy_userid'=>'','ppy_paymentmode'=>$modetype,'ppy_amt'=>'','ppy_status'=>'');
    $record['orddetail'] = $this->supper_admin->call_procedureRow('proc_orderpartialpayment',$parameter);
    //p($record['orddetail']);exit;

    $recordcurrent_amt = number_format($this->userfunction->get_current_total_amt_partialpay($record['orddetail']->OrderId)-number_format($record['orddetail']->CouponAmt,1,".","")-number_format($record['orddetail']->usedwalletamt,1,".",""),1,".","");
      if ($recordcurrent_amt==0.0) {
        $record['current_amt'] = number_format($this->userfunction->get_current_total_amt_partialpay($record['orddetail']->OrderId),1,".","");
      } else {
        $record['current_amt'] = $recordcurrent_amt;
      }
    

    if($this->input->post('submit')){

      if($this->input->post('pp_paymode')=='CASH'){
          $parameteradd=array('act_mode'=>'addpartpay',
                              'row_id'=>'',
                              'ppy_orderid'=>$ordid,
                              'ppy_userid'=>$record['orddetail']->UserId,
                              'ppy_paymentmode'=>$this->input->post('pp_paymode'),
                              'ppy_amt'=>$this->input->post('pp_partamt'),
                              'ppy_status'=>''
                              );
          $addpartialpay = $this->supper_admin->call_procedureRow('proc_orderpartialpayment',$parameteradd);
          $this->session->set_flashdata("message", "Your information was successfully Saved");
          redirect("admin/partialpay/index/".$ordid);
      
      } else {

          $parameteradd=array('act_mode'=>'addpartpay',
                              'row_id'=>'',
                              'ppy_orderid'=>$ordid,
                              'ppy_userid'=>$record['orddetail']->UserId,
                              'ppy_paymentmode'=>$this->input->post('pp_paymode'),
                              'ppy_amt'=>$this->input->post('pp_partamt'),
                              'ppy_status'=>''
                              );
          $chaddpartialpay = $this->supper_admin->call_procedureRow('proc_orderpartialpayment',$parameteradd);

          $param = array(
                        'act_mode'          => 'pp_insertcheq',
                        'row_id'            => $chaddpartialpay->pp_id,
                        'orderid'           => $ordid,
                        'chequeno'          => $this->input->post('pp_chno'),
                        'ifsc'              => '',
                        'bankname'          => $this->input->post('pp_chbankname'),
                        'chequedate'        => $this->input->post('pp_chdate'),
                        'accno'             => $this->input->post('pp_chacno'),
                        'chequecollectdate' => '',
                        'chequeamount'      => $this->input->post('pp_partamt'),
                        'paymentMod'        => $this->input->post('pp_paymode'),
                        'orderno'           => 'ORD'.$ordid 
                        );

          $addpartialpaychdetail = $this->supper_admin->call_procedureRow('proc_cheque',$param);
          $this->session->set_flashdata("message", "Your information was successfully Saved");
          redirect("admin/partialpay/index/".$ordid);

      }

    }

    $parameterdet=array('act_mode'=>'partpaydetail','row_id'=>$ordid,'ppy_orderid'=>'','ppy_userid'=>'','ppy_paymentmode'=>$modetype,'ppy_amt'=>'','ppy_status'=>'');
    $record['ordpartpaydetail'] = $this->supper_admin->call_procedure('proc_orderpartialpayment',$parameterdet);
    
    $this->load->view('helper/header');
    $this->load->view('order/addpartialpay',$record);
  }

  public function viewpaymentdetail($ordid){

    $parameter=array('act_mode'=>'orderdetail','row_id'=>$ordid,'ppy_orderid'=>'','ppy_userid'=>'','ppy_paymentmode'=>$modetype,'ppy_amt'=>'','ppy_status'=>'');
    $record['orddetail'] = $this->supper_admin->call_procedureRow('proc_orderpartialpayment',$parameter);
    $parameterdet=array('act_mode'=>'partpaydetail','row_id'=>$ordid,'ppy_orderid'=>'','ppy_userid'=>'','ppy_paymentmode'=>$modetype,'ppy_amt'=>'','ppy_status'=>'');
    $record['ordpartpaydetail'] = $this->supper_admin->call_procedure('proc_orderpartialpayment',$parameterdet);
    $this->load->view('helper/header');
    $this->load->view('order/viewpartpaydetail',$record);
  }

public function orderpayments(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'payorderview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/partialpay/orderpayments?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'payorderview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 foreach ($responce['penview'] as $value) {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt_partialpay($value->OrderId);
            }  
     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/orderpayment',$responce);
}


public function ordprodetail($ordid){
 $parameter        = array('act_mode'=>'payorderdetail','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/orderpaymentprodetail',$responce);
 //p($responce['penview']);exit;
}

public function codpaymentupdate($ordid,$pay){
  
  if($pay=='COD'){
     $parameter = array('act_mode'=>'updatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/partialpay/orderpayments');
  }

  if($pay=='CHEQUE'){
     $parameter = array('act_mode'=>'updatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/partialpay/orderpayments');
  }

  if($pay=='CREDIT'){
     $parameter = array('act_mode'=>'updatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/partialpay/orderpayments');
  }
 
}

public function paymentreupdate($ordid,$pay){
  
  if($pay=='COD'){
     $parameter = array('act_mode'=>'reupdatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/partialpay/orderpayments');
  }

  if($pay=='CHEQUE'){
     $parameter = array('act_mode'=>'reupdatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/partialpay/orderpayments');
  }

  if($pay=='CREDIT'){
     $parameter = array('act_mode'=>'reupdatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/partialpay/orderpayments');
  }
 
}
  
}//end class
?>