<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Ecatalogue extends MX_Controller{

//............. Default Construct function ............... //
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    //$this->userfunction->loginAdminvalidation();
    
  }// end function.




  public function newecatalogue(){
    //p("hello"); die;
    $this->userfunction->loginAdminvalidation();
    $parameterr  = array('act_mode'=>'brandlisting',
                      'row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $record['branddata'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 
    $parameter = array('act_mode'=>'viewparentcat','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort' =>'','catdisplay'=>'','catlevel'=>'');
    $record['parentcat']  = $this->supper_admin->call_procedure('proc_category',$parameter);

    if($this->input->post('submit')){
          if(!empty($_FILES['image1']['name'])) {
                          $upload_name = $docid[$i].'-'.str_replace(" ", "-", strtolower($_FILES['image1']['name']));
                          $filePath = FCPATH.'assets/ecatalogue/'.$upload_name;
                          $tmpFilePath = $_FILES['image1']['tmp_name'];
                          if(move_uploaded_file($tmpFilePath, $filePath)) {
                            $image1 = $upload_name;
                          } else {
                            $image1 = "Noimage";
                          } 
                      }
          if(!empty($_FILES['image2']['name'])) {
                          $upload_name = $docid[$i].'-'.str_replace(" ", "-", strtolower($_FILES['image2']['name']));
                          $filePath = FCPATH.'assets/ecatalogue/'.$upload_name;
                          $tmpFilePath = $_FILES['image2']['tmp_name'];
                          if(move_uploaded_file($tmpFilePath, $filePath)) {
                            $image2 = $upload_name;
                          } else {
                            $image2 = "Noimage";
                          } 
                      }
           if(!empty($_FILES['image3']['name'])) {
                          $upload_name = $docid[$i].'-'.str_replace(" ", "-", strtolower($_FILES['image3']['name']));
                          $filePath = FCPATH.'assets/ecatalogue/'.$upload_name;
                          $tmpFilePath = $_FILES['image3']['tmp_name'];
                          if(move_uploaded_file($tmpFilePath, $filePath)) {
                            $image3 = $upload_name;
                          } else {
                            $image3 = "Noimage";
                          } 
                      }
           if(!empty($_FILES['image4']['name'])) {
                          $upload_name = $docid[$i].'-'.str_replace(" ", "-", strtolower($_FILES['image4']['name']));
                          $filePath = FCPATH.'assets/ecatalogue/'.$upload_name;
                          $tmpFilePath = $_FILES['image4']['tmp_name'];
                          if(move_uploaded_file($tmpFilePath, $filePath)) {
                            $image4 = $upload_name;
                          } else {
                            $image4 = "Noimage";
                          } 
                      }                                  
     
     

          $param = array(
                        'act_mode' => 'insertcata',
                        'row_id' => $this->input->post('sec_cat'),
                        'cat_id' => $this->input->post('stype'),
                        'name' => $this->input->post('ecatalogue'),
                        'sec_order' => '1',
                        'media_type' => $this->input->post('desc'),
                        'img_name' => $image1,
                        'img_url' => $image2,
                        'img_head' => $image3,
                        'imgshead' => $image4
                        );
         // p($param); die;
         $record['lastsecid']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);

     
         
     

     
     $this->session->set_flashdata('message', 'Your information was successfully Saved.');
     redirect('admin/ecatalogue/ecataloguelist');
    
    }


    $this->load->view('helper/header');
    $this->load->view('ecatalogue/ecatalogue',$record);
  
  }// end function.
//............. Add Section Manager ............... //
  //............. Manage Section Master ............... //
  public function ecataloguelist(){
   
   $param = array(
                        'act_mode' => 'getecat',
                        'row_id' => '',
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
   $record['ecatlist']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);
   $this->load->view('helper/header');
   $this->load->view('ecatalogue/managecatalogue',$record); 
  }//end function.


  public function addecatalogue(){
    //p("hello"); die;
    $this->userfunction->loginAdminvalidation();
      $parameter               = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
      $record['viewcountry']   = $this->supper_admin->call_procedure('proc_geographic',$parameter);


     $parameterr  = array('act_mode'=>'brandlisting',
                      'row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $record['branddata'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 

    $parameter = array('act_mode'=>'viewparentcat','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort' =>'','catdisplay'=>'','catlevel'=>'');
    $record['parentcat']  = $this->supper_admin->call_procedure('proc_category',$parameter);
    $param = array(
                        'act_mode' => 'getecatfordropdown',
                        'row_id' => '',
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
    $record['ecatlist']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);



    if($this->input->post('submit')){
        
     


     $val =   $this->input->post('product');  
    
     for($k=0; $k< count($val); $k++){
     
        $param = array(
                        'act_mode' => 'insercataproduct',
                        'row_id' => $this->input->post('ecat'),
                        'cat_id' => $this->input->post('sec_cat'),
                        'name' => $this->input->post('sec_sub_cat'),
                        'sec_order' => $this->input->post('product')[$k],
                        'media_type' => $this->input->post('cityid'),
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
         $record['subcatdetail']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);
          
     }    
         
     $this->session->set_flashdata('message', 'Your information was successfully Saved.');
     redirect('admin/ecatalogue/createpdf');
    
    }


    $this->load->view('helper/header');
    $this->load->view('ecatalogue/addecatalogue',$record);
  
  }// end function.


//............. Manage Section Master ............... //
  public function createpdf(){
    $this->load->view('helper/header');
    $this->userfunction->loginAdminvalidation();
    $param = array(
                        'act_mode' => 'getecatmap',
                        'row_id' => '',
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
         $record['catamaplist']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);
    $this->load->view('ecatalogue/managecataloguemap',$record);
  
  }//end function.
  public function deleteecatalogue($id){
    $this->load->view('helper/header');
    $this->userfunction->loginAdminvalidation();
    $param = array(
                        'act_mode' => 'deleteecatalogue',
                        'row_id' => $id,
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
         $record['catamaplist']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);
   redirect('admin/ecatalogue/ecataloguelist');
  
  }
  
  
  public function downloadpdf($id){
    

    $txt = file_get_contents(base_url().'admin/ecatalogue/gethtmlhome/'.$id);
    
    //p($txt);
        $pdfFilePath = "Ecatalogue.pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
         $this->m_pdf->pdf->WriteHTML($txt);

 
        //download it.
      $a = $this->m_pdf->pdf->Output($pdfFilePath, "D");
      //p($a); die();
  }

  
  public function gethtmlhome($id){
     //p($id); die;
     $param = array(
                        'act_mode' => 'catahomepage',
                        'row_id' => $id,
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
    
            );
     //p($param); die;           
    $record['catahomepage']  = $this->supper_admin->call_procedureRow('proc_ecatalogue',$param);
    $param = array(
                        'act_mode' => 'catainnerpage',
                        'row_id' => $id,
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
    
            );
               
    $record['catainnerpage']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);
    //p($record['catainnerpage']); die;
    //p($record['catainnerpage']); die;
    $this->load->view('ecatalogue/ecataloghome',$record);

  }
  

  public function deletesection($id){
    $this->userfunction->loginAdminvalidation();
    $parameterimg     = array('row_id'=>$id,'catparentid'=>'','actmode'=>'viewsectionimg');
    $dataimg  = $this->supper_admin->call_procedure('proc_sectionmanager',$parameterimg);
    foreach ($dataimg as $key => $value) {
        unlink($_SERVER["DOCUMENT_ROOT"]."/bizzgain.com/assets/sectionimages/".$value->imgname);
    }

    $parameter     = array('row_id'=>$id,'catparentid'=>'','actmode'=>'deletesection');
    $data  = $this->supper_admin->call_procedure('proc_sectionmanager',$parameter);
    redirect('admin/sectionmanager/managesection');

  }


  public function subcategory(){
    $parentcatid=$this->input->post('parentcat');
    //echo $parentcatid;exit();
    $parameter= array('act_mode'=>'viewsubcatdata','row_id'=>$parentcatid,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort' =>'','catdisplay'=>'','catlevel'=>'');
    $record['subcat']  = $this->supper_admin->call_procedure('proc_category',$parameter);
    $str = '';
    foreach($record['subcat'] as $k=>$v){
      
        $str .= "<option value=".$v->catid.">".$v->catname."</option>";
   
    }
    echo $str;
  
  }

  public function get_maincategory() {
       $brandids                    = $this->input->post('brandids');
      //$parameterr                = array('act_mode'=>'brandcategory','row_id'=>$brandids,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
      $parameterr                = array('act_mode'=>'brandcategorycombo','row_id'=>$brandids,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
       $responce['brandcategory']  = $this->supper_admin->call_procedure('proc_category',$parameterr);  
        
        
        foreach($responce['brandcategory'] as $k=>$v){   
              $str .= '<option value="'.$v->catid.'">'.$v->catname.'</option>';
      }
        
        echo $str;
    }

    public function getproduct(){

    $parentcatid=$this->input->post('parentcat');
    
    $cityid=$this->input->post('city');
    //echo $cityid;exit();
    $param = array(
                        'act_mode' => 'citygroup',
                        'row_id' => $cityid,
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
    $recordcmid  = $this->supper_admin->call_procedureRow('proc_ecatalogue',$param);
   // p($record['citygroup']); exit;
    if(empty($recordcmid->cmid)){
      $recordcmid=3;
    }
    $param = array(
                        'act_mode' => 'getproduct',
                        'row_id' => $parentcatid,
                        'cat_id' =>'' ,
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
    
    $record['subcatdetail']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);
    
    $str = '';
    foreach($record['subcatdetail'] as $k=>$v){
        $param = array(
                        'act_mode' => 'getcitypercent',
                        'row_id' => $v->prodtmappid,
                        'cat_id' => $recordcmid->cmid,
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
       
        $getcitypercent  = $this->supper_admin->call_procedureRow('proc_ecatalogue',$param);
        //p($getcitypercent); exit;
        $cityprice =citybaseprice($v->FinalPrice,$getcitypercent->cityvalue);
        $str .= '<div class="col-lg-4"><div class="row"><div class="col-lg-11"><div style="width: 100%;float: left;
    text-align: center;"><img src="'.base_url().'/images/thumimg/'.$v->Image.'" alt="Smiley face" height="50" width="50"></div><div style="    width: 100%;
    height: auto;
    float: left;
    text-align: center;margin: 10px 0px;"><input type="checkbox" class="checkboxall" name="product[]" style="margin: 5px 5px;" value="'.$v->prodtmappid.'">'.$v->ProductName.'</br>Selling Price:-'.$cityprice.'</br>MRP:-'.$v->productMRP.'<br></div></div></div></div>';
   
    }
    echo $str;
  
  }

  public function selectproductbycat(){

    $parentcatid=$this->input->post('parentcat');
    
    $cityid=$this->input->post('city');
    //echo $cityid;exit();
    $param = array(
                        'act_mode' => 'citygroup',
                        'row_id' => $cityid,
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
    $recordcmid  = $this->supper_admin->call_procedureRow('proc_ecatalogue',$param);
   // p($record['citygroup']); exit;
    if(empty($recordcmid->cmid)){
      $recordcmid=3;
    }
    $param = array(
                        'act_mode' => 'selectproductbycat',
                        'row_id' => $parentcatid,
                        'cat_id' =>'' ,
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
    
    $record['subcatdetail']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);
    
    $str = '';
    foreach($record['subcatdetail'] as $k=>$v){
        $param = array(
                        'act_mode' => 'getcitypercent',
                        'row_id' => $v->prodtmappid,
                        'cat_id' => $recordcmid->cmid,
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
       
        $getcitypercent  = $this->supper_admin->call_procedureRow('proc_ecatalogue',$param);
        //p($getcitypercent); exit;
        $cityprice =citybaseprice($v->FinalPrice,$getcitypercent->cityvalue);
        $str .= '<div class="col-lg-4"><div class="row"><div class="col-lg-11"><div style="width: 100%;float: left;
    text-align: center;"><img src="'.base_url().'/images/thumimg/'.$v->Image.'" alt="Smiley face" height="50" width="50"></div><div style="    width: 100%;
    height: auto;
    float: left;
    text-align: center;margin: 10px 0px;"><input type="checkbox" class="checkboxall" name="product[]" style="margin: 5px 5px;" value="'.$v->prodtmappid.'">'.$v->ProductName.'</br>Selling Price:-'.$cityprice.'</br>MRP:-'.$v->productMRP.'<br></div></div></div></div>';
   
    }
    echo $str;
  
  }

  public function getproductbybrand(){

    
    $brandid=$this->input->post('brandid');
    $cityid=$this->input->post('city');
    //echo $cityid;exit();
    $param = array(
                        'act_mode' => 'citygroup',
                        'row_id' => $cityid,
                        'cat_id' => '',
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
    $recordcmid  = $this->supper_admin->call_procedureRow('proc_ecatalogue',$param);
   // p($record['citygroup']); exit;
    if(empty($recordcmid->cmid)){
      $recordcmid=3;
    }
    $param = array(
                        'act_mode' => 'getproductbybrand',
                        'row_id' => '',
                        'cat_id' => $brandid,
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
    
    $record['subcatdetail']  = $this->supper_admin->call_procedure('proc_ecatalogue',$param);
    
    $str = '';
    foreach($record['subcatdetail'] as $k=>$v){
        $param = array(
                        'act_mode' => 'getcitypercent',
                        'row_id' => $v->prodtmappid,
                        'cat_id' => $recordcmid->cmid,
                        'name' => '',
                        'sec_order' => '',
                        'media_type' => '',
                        'img_name' => '',
                        'img_url' => '',
                        'img_head' => '',
                        'imgshead' => ''
                        );
       
        $getcitypercent  = $this->supper_admin->call_procedureRow('proc_ecatalogue',$param);
        //p($getcitypercent); exit;
        $cityprice =citybaseprice($v->FinalPrice,$getcitypercent->cityvalue);
        $str .= '<div class="col-lg-4"><div class="row"><div class="col-lg-11"><div style="width: 100%;float: left;
    text-align: center;"><img src="'.base_url().'/images/thumimg/'.$v->Image.'" alt="Smiley face" height="50" width="50"></div><div style="    width: 100%;
    height: auto;
    float: left;
    text-align: center;margin: 10px 0px;"><input type="checkbox" class="checkboxall" name="product[]" style="margin: 5px 5px;" value="'.$v->prodtmappid.'">'.$v->ProductName.'</br>Selling Price:-'.$cityprice.'</br>MRP:-'.$v->productMRP.'<br></div></div></div></div>';
   
    }
    echo $str;
  
  }


public function changecategory(){

    //$chsku=array('CHICCO-159','LO-150','LO-151','LO-152','LO-153','LO-154','MAT-DFR37','MAT-P6873','MS1021B','MS1021C','MS1021E','MS1021F','MS1021G','MS1021H','MS1021J','MS1100A','MS1100B','MS1200A','MS1700L','MS1700M','MS2300L','MS2300M','MS2521A','VTA022');

$chsku=array('CT-109A');
    
    foreach ($chsku as $key => $value) {
        $parameter=array('act_mode'=>'chcatapppro','Ordid'=>'','coments'=>$value,'empid'=>'');
        $record['subcat']  = $this->supper_admin->call_procedure('proc_Commnents',$parameter);
    }
        echo 'success';
  }  


} 
?>