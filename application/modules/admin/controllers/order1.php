<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends MX_Controller{
  public function __construct(){
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->helper('string');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->load->library('form_validation');
    $this->userfunction->loginAdminvalidation();

  }

  public function allorder()
  {
     if(isset($_GET['filter_by']) && !empty($_GET['filter_by']))
    { 

            //----------------  Download Order Excel ----------------------------//

                if(!empty($_GET['newsexcel']))
                    {

                      if(isset($_GET['filter_by']) && !empty($_GET['filter_by']))
                      { 
                        $act=$ord_no=$phone=$cust_name=$fromdate=$todate='';
                        if($_GET['filter_by']=='order')
                        {
                          $ord_no=$_GET['ord_no'];
                          $act='By_order';
                        }elseif($_GET['filter_by']=='phone'){
                          $phone=$_GET['phn_no'];
                          $act='By_phone';
                        }elseif($_GET['filter_by']=='customer'){
                          $cust_name=$_GET['custm_nm'];
                          $act='By_cutomer';
                        }elseif($_GET['filter_by']=='date'){
                          $fromdate=$_GET['fromdate'];
                          $todate=$_GET['todate'];
                          $act='By_date';

                        }
                          $parameter = array('act_mode'=>$act,'row_id'=>'','orderstatus'=>'pending','order_proid'=>'','order_id'=>$ord_no,'order_venid'=>'','order_qty'=>'','oldstatus'=>'','phone'=>$phone,'customer'=>$cust_name,'s_date'=>$fromdate,'e_date'=>$todate);
                         $data['recordfil'] = $this->supper_admin->call_procedure('proc_allorder_search',$parameter); 
                     
                    $finalExcelArr = array('Order Number','Customer Name','Customer Address','Email','Mobile','Payment Mode','Payment Status','Order Type','Amount','Order Date & Time','Vendor Name');
                     $objPHPExcel = new PHPExcel();
                     $objPHPExcel->setActiveSheetIndex(0);
                     $objPHPExcel->getActiveSheet()->setTitle('Order Worksheet');
                     $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
                      $j=2;
                      
                      //For freezing top heading row.
                      $objPHPExcel->getActiveSheet()->freezePane('A2');

                      //Set height for column head.
                      $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                                  
                     for($i=0;$i<count($finalExcelArr);$i++){
                      
                      //Set width for column head.
                      $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

                      //Set background color for heading column.
                      $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                          array(
                              'fill' => array(
                                  'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                  'color' => array('rgb' => '71B8FF')
                              ),
                                'font'  => array(
                                'bold'  => false,
                                'size'  => 15,
                                )
                          )
                      );

                      $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

                      foreach ($data['recordfil'] as $key => $value) {
                       
                      $newvar = $j+$key;

                      //Set height for all rows.
                      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
                      
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->OrderNumber);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->ShippingName." ".$value->ShipLastName);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->ShippingAddress.", ".$value->City.", ".$value->statename."-".$value->Pin);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->Email);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->ContactNo);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->PaymentMode);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->PaymentStatus);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->order_type);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $this->userfunction->get_current_total_amt($value->OrderId)+$value->ShippingAmt);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->OrderDate);
                      $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->vendorname);
                      }
                    }

                    $filename='Orders Listing.xls';
                    header('Content-Type: application/vnd.ms-excel'); //mime type
                    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                    header('Cache-Control: max-age=0'); //no cache
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                    ob_end_clean();
                    ob_start();  
                    $objWriter->save('php://output');

                  } else {

                    $this->session->set_flashdata("message", "Please select any filter by !");
                    redirect('admin/order/allorder');

                  }

                   
            }
                //----------------  End Download Order Excel ------------------------// 
            else {

                $act=$ord_no=$phone=$cust_name=$fromdate=$todate='';
                if($_GET['filter_by']=='order')
                {
                  $ord_no=$_GET['ord_no'];
                  $act='By_order';
                }elseif($_GET['filter_by']=='phone'){
                  $phone=$_GET['phn_no'];
                  $act='By_phone';
                }elseif($_GET['filter_by']=='customer'){
                  $cust_name=$_GET['custm_nm'];
                  $act='By_cutomer';
                }elseif($_GET['filter_by']=='date'){
                  $fromdate=$_GET['fromdate'];
                  $todate=$_GET['todate'];
                  $act='By_date';

                }
                  $parameter = array('act_mode'=>$act,'row_id'=>'','orderstatus'=>'pending','order_proid'=>'','order_id'=>$ord_no,'order_venid'=>'','order_qty'=>'','oldstatus'=>'','phone'=>$phone,'customer'=>$cust_name,'s_date'=>$fromdate,'e_date'=>$todate);
                 $data['record'] = $this->supper_admin->call_procedure('proc_allorder_search',$parameter); 
                 //p($data['record']);exit();
                    //----------------  start pagination setting ------------------------// 

                   $config['base_url']         = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
                   $config['total_rows']       = count($data['record']);
                   $config['per_page']         = 50;
                   $config['use_page_numbers'] = TRUE;
                   //p($config);exit();
                   $this->pagination->initialize($config);
                   if($_GET['page']){
                     $page         = $_GET['page']-1 ;
                     $page         = ($page*50);
                     $second       = $config['per_page'];
                   }
                   else{
                     $page         = 0;
                     $second       = $config['per_page'];
                   }
                   
                   $str_links = $this->pagination->create_links();
                   $data["links"]  = explode('&nbsp;',$str_links );

                   $parameter = array('act_mode'=>$act,'row_id'=>$page,'orderstatus'=>'pending','order_proid'=>$second,'order_id'=>$ord_no,'order_venid'=>'','order_qty'=>'','oldstatus'=>'','phone'=>$phone,'customer'=>$cust_name,'s_date'=>$fromdate,'e_date'=>$todate);
                 $data['record'] = $this->supper_admin->call_procedure('proc_allorder_search',$parameter); 
                  //p($parameter);exit();
                   //----------------  end pagination setting ------------------------// 
               }
    
    }else{
    
          $parameter= array('act_mode'=>'allorder','row_id'=>'','orderstatus'=>'pending','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
           $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

              //----------------  start pagination setting ------------------------// 

         $config['base_url']         = base_url()."admin/order/allorder?";
         $config['total_rows']       = count($data['record']);
         $config['per_page']         = 50;
         $config['use_page_numbers'] = TRUE;

         $this->pagination->initialize($config);
         if($_GET['page']){
           $page         = $_GET['page']-1 ;
           $page         = ($page*50);
           $second       = $config['per_page'];
         }
         else{
           $page         = 0;
           $second       = $config['per_page'];
         }
         
         $str_links = $this->pagination->create_links();
         $data["links"]  = explode('&nbsp;',$str_links );

         $parameter = array('act_mode'=>'allorder','row_id'=>$page,'orderstatus'=>'pending','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
           //$data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

           
         
         //----------------  end pagination setting ------------------------// 
    }

        foreach ($data['record'] as $value) {
                  $data['current_amt'][]=$this->userfunction->get_current_total_amt($value->OrderId);
            } 

       $parameter= array('act_mode'=>'allorder','row_id'=>'','orderstatus'=>'pending','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $data['customer_range'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

        $parametercus= array('act_mode'=>'allordercustomer','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $recordallordcustomername = $this->supper_admin->call_procedure('proc_orderflow',$parametercus);
        $arrcust=array();

        foreach ($recordallordcustomername as $key => $value) {
          if($value->vendortype=='retail'){
            array_push($arrcust, $value->companyname);
          } else {
            array_push($arrcust, $value->ShippingName);
          }
        }
        natcasesort($arrcust);
        $data['allordcustomername']=$arrcust;
       //p($data);exit;
       $this->load->view('helper/header');
       $this->load->view('order/allorder',$data);
  }


public function allorderdetail($ordid){
      if(!empty($_POST)){
        
       foreach ($this->input->post('proApp') as $key => $value) {
        if($this->input->post('chstatus')=='onprocess'){
          if($this->input->post('OrdQty')[$value]<=$this->input->post('totalstock')[$value]){
           $this->session->set_flashdata("message", "Your Stock Quantity is already available.");
          }else{
           
          	$parameterrs = array('act_mode'=>'insertpodata','row_id'=>$value,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
            $responce['podata'] = $this->supper_admin->call_procedure('proc_orderflow',$parameterrs);


            $parameter = array('act_mode'=>'update','row_id'=>$value,'orderstatus'=>$this->input->post('chstatus'),'order_proid'=>$this->input->post('proid')[$value],'order_id'=>$ordid,'order_venid'=>$this->input->post('venid')[$value],'order_qty'=>$this->input->post('OrdQty')[$value],'oldstatus'=>$this->input->post('oldstatus')[$value]);
            $responce['pende'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
          }

        }
        else{
          
        $parameter = array('act_mode'=>'update','row_id'=>$value,'orderstatus'=>$this->input->post('chstatus'),'order_proid'=>$this->input->post('proid')[$value],'order_id'=>$ordid,'order_venid'=>$this->input->post('venid')[$value],'order_qty'=>$this->input->post('OrdQty')[$value],'oldstatus'=>$this->input->post('oldstatus')[$value]);
        $responce['pende'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
      }
        }
        //p($responce['pende']);exit();
        redirect('admin/order/allorderdetail/'.$ordid);
       }
      
        $parameter      = array('act_mode'=>'singrecod','row_id'=>'','orderstatus'=>'pending','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $data['record'] = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter); 
        $parameter2     = array('act_mode'=>'admin','row_id'=>'','orderstatus'=>'pending','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $data['detialrecord'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter2); 
        $parameter3     = array('act_mode'=>'get_pro_list','row_id'=>'','orderstatus'=>'pending','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $data['product_list'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter3);
       
          //p($data);exit();
          $this->load->view('helper/header');
          $this->load->view('order/vieworder',$data);
       
       
    
}


/*public function onprocess(){
 $parameter        = array('act_mode'=>'onprocessview','row_id'=>'','orderstatus'=>'onprocess','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/onprocess',$data); 
}
public function onprocessdetail($venid){
 if($this->input->post('submitpo')) {

  $ponumber = 'PO'.$this->randnumber(); 
  $parameter        = array('act_mode'=>'poinsertT','row_id'=>$venid,'orderstatus'=>$ponumber,'order_proid'=>'','order_id'=>'','order_venid'=>$venid,'order_qty'=>'','oldstatus'=>'');
  $insertProView    = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter); 
   if($insertProView->lastid>0){
   foreach ($this->input->post('proApp') as $key => $value) {
    $singprice =$this->input->post('sinamt');
    $totalprice=$this->input->post('sinamttotal');
    $qty       =$this->input->post('qty');
    $parameter   =array('act_mode'=>'pomapinsert','row_id'=>$insertProView->lastid,'ponumber'=>'','po_total'=>$totalprice[$value],'po_proid'=>$value,'po_manuid'=>$venid,'po_qty'=>$qty[$value],'po_price'=>$singprice[$value]);
    $inser = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameter);
   //p($parameter);
   }
   //redirect('admin/order/onprocessdetail/'.$venid);
  }
//exit;
  
 }
 $parameter        = array('act_mode'=>'onprodetail','row_id'=>'','orderstatus'=>'onprocess','order_proid'=>'','order_id'=>'','order_venid'=>$venid,'order_qty'=>'','oldstatus'=>'');
 $data['vieww'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/onprocedetails',$data);
   
}*/



public function onprocess(){
 $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'onprocesssview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $data['onproview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/onprocess?";
     $config['total_rows']       = count($data['onproview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $data["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'onprocesssview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  #$data['onproview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

     //----------------  end pagination setting ------------------------// 
  //p($responce['onproview']);exit;
 $this->load->view('helper/header');
 $this->load->view('order/onprocess',$data); 
}

public function onprocessdetail($ordid){
 $parameter = array('act_mode'=>'onprocesssdetail','row_id'=>'','orderstatus'=>'onprocess','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 if(!empty($_POST)){
  //p($_POST);exit();
       foreach ($this->input->post('proApp') as $key => $value) {
                 
        $parameter = array('act_mode'=>'update','row_id'=>$value,'orderstatus'=>$this->input->post('chstatus'),'order_proid'=>$this->input->post('proid')[$value],'order_id'=>$ordid,'order_venid'=>$this->input->post('venid')[$value],'order_qty'=>$this->input->post('OrdQty')[$value],'oldstatus'=>$this->input->post('oldstatus')[$value]);
        //p($parameter);exit();
        $responce['pende'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
       }
        //p($responce['pende']);exit();
        redirect('admin/order/onprocessdetail/'.$ordid);
    }
    //p($data);exit();
 $this->load->view('helper/header');
 $this->load->view('order/onprocedetails',$data);
   
}


//------------------start purchase Order-----------------------

public function managepolist(){
 $parameter = array('act_mode'=>'viewpolist','row_id'=>'','orderstatus'=>'onprocess','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/managepolist?";
     $config['total_rows']       = count($data['record']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $data["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'viewpolist','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

     //----------------  end pagination setting ------------------------// 
 $this->load->view('helper/header');
 $this->load->view('purchaseorder/managepo',$data); 
}
public function podetail($venid){

	$parameterwwq        = array('act_mode'=>'viewallmanufac','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['manuvieww'] = $this->supper_admin->call_procedure('proc_orderflow',$parameterwwq);

 $parameter        = array('act_mode'=>'viewpodetail','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>$venid,'order_qty'=>'','oldstatus'=>'');
 $data['vieww'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 
 if($this->input->post('submitpo')) {

  if($this->input->post('proApp')){
      
      $ponumber = 'PO'.$this->randnumber();
      $totpro=count($this->input->post('proApp'));
      $parameter        = array('act_mode'=>'poinsertT','row_id'=>$venid,'orderstatus'=>$ponumber,'order_proid'=>$totpro,'order_id'=>'','order_venid'=>$venid,'order_qty'=>'','oldstatus'=>'');
      $insertProView    = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter); 
      
      if($insertProView->lastid>0){
      foreach ($this->input->post('proApp') as $key => $value) {
        $vennnid = $this->input->post('vendid'.$value);
        //$qty       =$this->input->post('qty');
        $parameter1q   =array('act_mode'=>'pomapinsertdata','row_id'=>$insertProView->lastid,'ponumber'=>'','po_total'=>'','po_proid'=>$value,'po_manuid'=>$vennnid,'po_qty'=>'','po_price'=>'');
        //p($parameter1q);
        $inser = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameter1q);
       }
       //exit;
      }
      redirect('admin/order/podetail/'.$venid);
  } else {
    $this->session->set_flashdata('message', 'Please Select any product for generate Purchase order.');
    redirect('admin/order/podetail/'.$venid);
  } // end else count
 } // end if submitpo
 
  
 $this->load->view('helper/header');
 $this->load->view('purchaseorder/managepodetails',$data);
   
}

public function polist(){
 $parameter = array('act_mode'=>'polistdata','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
//----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/polist?";
     $config['total_rows']       = count($data['record']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $data["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'polistdata','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

     //----------------  end pagination setting ------------------------// 
 $this->load->view('helper/header');
 $this->load->view('purchaseorder/poview',$data); 
}

public function poupdateqty($poid){
 
 $parameter1 = array('act_mode'=>'polistdetail','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter1);
 $parameter2 = array('act_mode'=>'polistdetail2','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record2'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter2);
 $i=0;
 foreach ($data['record'] as $key => $value) {

 	if($data['record2'][$i]->poprice==0){ 
	    $unitpri = $value->po_finalprice;
	    $totpri = $value->po_finalprice*$data['record2'][$i]->postock; 
	} else {
	    $unitpri = $data['record2'][$i]->poprice;
	    $totpri = $data['record2'][$i]->poprice*$data['record2'][$i]->postock;
	}
   $parameter1q   =array('act_mode'=>'poupdateqty','row_id'=>$poid,'ponumber'=>'','po_total'=>$totpri,'po_proid'=>$data['record2'][$i]->poproid,'po_manuid'=>$data['record2'][$i]->pomanuid,'po_qty'=>$data['record2'][$i]->postock,'po_price'=>$unitpri);
 	$inser = $this->supper_admin->call_procedure('proc_orderpoinsert',$parameter1q);
    $i++;
 }
 
 redirect('admin/order/polist/');
 
}

public function polistdetail($poid){

 $parameter = array('act_mode'=>'polistdetail','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 $parameter = array('act_mode'=>'polistdetail2','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record2'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
//p($data['record']);exit();
 if($this->input->post('submitpo')) { 
 	foreach ($data['record2'] as $key => $value) {
 	//p($value->pomapid);
 		$unitpri = $this->input->post('sinamt');
 		$totpri = $this->input->post('sinamttotal');
 		$poqty = $this->input->post('qty');

    $parametersub   =array('act_mode'=>'updatepolist','row_id'=>$value->pomapid,'ponumber'=>'','po_total'=>$totpri[$value->pomapid],'po_proid'=>$poid,'po_manuid'=>'','po_qty'=>$poqty[$value->pomapid],'po_price'=>$unitpri[$value->pomapid]);
    //p($parametersub);exit();
	 	$record = $this->supper_admin->call_procedure('proc_orderpoinsert',$parametersub);
 	}
 	//redirect('admin/order/polistdetail/'.$poid);
$this->session->set_flashdata('message', 'Your information was successfully Saved.');
redirect('admin/poorder/polist');
 	//exit();
 }

 $this->load->view('helper/header');
 $this->load->view('purchaseorder/podetails',$data); 
}

public function printpo($poid){
	//p($poid);exit();
 $parameter1 = array('act_mode'=>'polistdetail','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter1);
 $parameter2 = array('act_mode'=>'polistdetail2','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record2'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter2);
 $parameter3 = array('act_mode'=>'printpodet','row_id'=>$poid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $data['record3'] = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter3);
  $this->load->view('purchaseorder/poinvoice',$data);
}

//------------------end purchase Order-----------------------





function randnumber(){
    srand ((double) microtime() * 1000000);
    $random5 = rand(10000,99999);
    return $random5;
}

public function orderdelete()
{

  $OrdId=$this->uri->segment(4);
  $Ordno=$this->uri->segment(5); 
  $parameter=array('orderid'=>(int)$OrdId);
  $parameter1=array('orderid'=>(int)$OrdId,'act_mode'=>'admin','venid'=>'');
  $data['detialrecord']=$this->supper_admin->call_procedure('proc_OrderDetails',$parameter1);
  foreach ($data['detialrecord'] as $value) {
    $proid=$value->ProId;
    $proQty=$value->OrdQty;
    $parameter1=array('act_mode'=>'qtyupdatebeforedelord','row_id'=>$proid,'qty'=>$proQty,'pay_status'=>'');
    $data['updateqty']=$this->supper_admin->call_procedure('proc_orderflow',$parameter1);
  }
  $parameter2=array('act_mode'=>'deleteordernow','row_id'=>$OrdId,'qty'=>'','pay_status'=>'');
  $data['deleteord']=$this->supper_admin->call_procedure('proc_orderflow',$parameter2);
 redirect('admin/order/allorder');
  
}

  
  
public function addremarks()
{
    

    if($this->input->post('updateinventory'))
    {

          $parameter = array(
              'act_mode'  =>  'update',
              'Row_id'    =>  $this->input->post('quantyupdate')
            );

          
          $this->supper_admin->call_procedure('proc_inventoryupdate' , $parameter);

          redirect('admin/order/returnorder');


    }else{


    $parameter = array('act_mode' => 'insertremark',
                    'p_userid' =>$this->input->post('userid'),
                    'p_invoiceid'=>'',
                    'p_orderid'=>'',
                    'p_productid'=>'',
                    'p_reasonoforderss'=>'',
                    'p_reason'=>'',
                    'p_return_qty'=>'',
                    'p_vendorid'=>'',
                    'p_productname'=>'',
                    'p_Remarks' =>  $this->input->post('Remarks'),
                    'p_complain_id' => ''

                  );

   $datas['returndetails'] = $this->supper_admin->call_procedure('proc_insertreturnord',$parameter);
   redirect('admin/order/returnorder');

}

}



 public function update_product_val()
 {
   //p($_POST);exit();
         $prd_qty=$_POST['qty'];

         $parameter  = array(
          'act_mode'=>'get_pre_ordr_dtl',
          'product_id'=>'',
          'pro_qty'=>'',
          'state_id' =>'',
          'ordertbl_id'=>'',
          'ordrmaptbl_id'=>$_POST['prod_id'][0],
          'price'=>'',
          'city'=>'',
          'tax_amt'=>'',
          'shipping_amt'=>'',
          'p_taxvat'=>'',
          'p_taxcst'=>'',
          'p_taxentry'=>''
          );
     
        $old_order = $this->supper_admin->call_procedureRow('proc_update_order_details',$parameter);
        //p($old_order);exit();
          $parameter  = array(
          'act_mode'=>'get_pro_details',
          'product_id'=>$_POST['prod_id'][1],
          'pro_qty'=>'',
          'state_id' =>'',
          'ordertbl_id'=>'',
          'ordrmaptbl_id'=>'',
          'price'=>'',
          'city'=>'',
          'tax_amt'=>'',
          'shipping_amt'=>'',
          'p_taxvat'=>'',
          'p_taxcst'=>'',
          'p_taxentry'=>''
          );

        $pro_dtls = $this->supper_admin->call_procedureRow('proc_update_order_details',$parameter);
        //p($pro_dtls);exit();


         $parameter  = array(
            'act_mode'=>'get_tax_dtls',
            'product_id'=>$pro_dtls->proid,
            'pro_qty'=>'',
            'state_id' =>$_POST['Stateid'],
            'ordertbl_id'=>$_POST['orderid'],
            'ordrmaptbl_id'=>'',
            'price'=>'',
            'city'=>'',
            'tax_amt'=>'',
            'shipping_amt'=>'',
            'p_taxvat'=>'',
            'p_taxcst'=>'',
            'p_taxentry'=>''
            );
        //p($parameter);exit();
            $data=$this->supper_admin->call_procedure('proc_update_order_details',$parameter);
            //p($data);exit();
            $tin = $cst = $entry ="";
            foreach ($data as $value) {
             if(($value->taxidd==1 || $value->taxidd==6) && ($value->entrytax_id==5))
             { 
              $tin = $value->taxprice;
              $entry = $value->entry_pz;
             }
             if($value->taxidd==2 || $value->taxidd==4)
             {
               $cst = $value->taxprice;
             }
              
            }

          $parameter  = array(
          'act_mode'=>'get_city_prize',
          'product_id'=>$_POST['prod_id'][1],
          'pro_qty'=>'',
          'state_id' =>$_POST['Stateid'],
          'ordertbl_id'=>'',
          'ordrmaptbl_id'=>'',
          'price'=>'',
          'city'=>$_POST['city_vl'],
          'tax_amt'=>'',
          'shipping_amt'=>'',
          'p_taxvat'=>'',
          'p_taxcst'=>'',
          'p_taxentry'=>''
          );

        $data = $this->supper_admin->call_procedureRow('proc_update_order_details',$parameter);
        
        $parameter  = array(
        'act_mode'=>'get_range',
        'product_id'=>$_POST['prod_id'][1],
        'pro_qty'=>'',
        'state_id' =>$_POST['Stateid'],
        'ordertbl_id'=>'',
        'ordrmaptbl_id'=>'',
        'price'=>'',
        'city'=>$_POST['city_vl'],
        'tax_amt'=>'',
        'shipping_amt'=>'',
        'p_taxvat'=>'',
        'p_taxcst'=>'',
        'p_taxentry'=>''
        );


        $price_range = $this->supper_admin->call_procedure('proc_update_order_details',$parameter);
        
         if(!empty($price_range))
         { 
            
            foreach ($price_range[0] as $key => $range) 
           { 

              if($key=='qty1')
              { 
                  $min=explode('-',$range)[0];
                  $max=explode('-',$range)[1];
                  if(($min <= $prd_qty) && ($prd_qty <= $max))
                  {
                     $price=$price_range[0]->price;
                  }
              }else if($key=='qty2')
              {
                  $min=explode('-',$range)[0];
                  $max=explode('-',$range)[1];
                  if(($min <= $prd_qty) && ($prd_qty <= $max))
                  {
                     $price=$price_range[0]->price2;
                  }
              }else if($key=='qty3')
              {
                  $min=explode('-',$range)[0];
                  $max=explode('-',$range)[1];
                  if(($min <= $prd_qty) && ($prd_qty <= $max))
                  {
                      $price=$price_range[0]->price3;
                  }
              }
              
           }
          
          
         }
   
          if(!empty($price) && !empty($data->cityvalue))
          {
            $sale_pz=($old_order->ProPrice) - (($old_order->ProPrice * $price / 100) + ($old_order->ProPrice * $data->cityvalue / 100));
          }else if (!empty($price) && empty($data->cityvalue))
          {
            $sale_pz=($old_order->ProPrice) - ($old_order->ProPrice * $price / 100);
          }
          else if (empty($price) && !empty($data->cityvalue))
          {
            $sale_pz=($old_order->ProPrice) - ($old_order->ProPrice * $data->cityvalue / 100);
          }else{
            $sale_pz=$old_order->ProPrice;
          }
          
          $add_pro_arr = array( 
          'act_mode'  =>'add_more_prod',
          'p_orderId' =>$old_order->OrderId,
          'p_proid'   =>$old_order->ProId,
          'p_CatId'   =>$old_order->CatId,
          'p_SkuNumber'=>$old_order->SkuNumber,
          'p_proname'  =>$old_order->ProName,
          'p_prodPrice'=>$old_order->ProPrice,
          'p_VenSellPrice'=>ceil($sale_pz),
          'p_FinalPrice'=>ceil($sale_pz),
          'p_OrdSize'=>$old_order->OrdSize,
          'p_OrdQty'=>$_POST['qty'],
          'p_shippingcharge' =>'',
          'p_vendorId'=> $old_order->VendorID,
          'p_OrdColor'=>$old_order->OrdColor,
          'p_ProImage'=>$old_order->ProImage,
          'p_OrdShipping'=>'pending',
          'p_protaxvat'=>ceil(((ceil($sale_pz) * $_POST['qty']) * ($tin))/100),
          'p_protaxcst'=>ceil(((ceil($sale_pz) * $_POST['qty']) * ($cst))/100),
          'p_protaxentry'=>ceil(((ceil($sale_pz) * $_POST['qty']) * ($entry))/100),
          );
          $add_pro_arr['p_protax']=$add_pro_arr['p_protaxvat']+$add_pro_arr['p_protaxcst']+$add_pro_arr['p_protaxentry'];
        //p($add_pro_arr);exit();
      //$this->supper_admin->call_procedureRow('proc_add_more_prod',$parameter);
      /*$pz=(($old_order->subtotal) - ($old_order->VenSellPrice * $old_order->OrdQty));
      $priz=(($pz) + (ceil($sale_pz) * $_POST['qty']));*/

      //$this->delete_product($_POST['prod_id'][0],$priz,$old_order->OrderId);
          $parameter  = array(
          'act_mode'=>'update_order_tbl',
          'product_id'=>'',
          'pro_qty'=>'',
          'state_id' =>'',
          'ordertbl_id'=>$id,
          'ordrmaptbl_id'=>'',
          'price'=>(($old_order->subtotal) - ($old_order->VenSellPrice * $old_order->OrdQty)) + ($add_pro_arr['p_VenSellPrice'] * $add_pro_arr['p_OrdQty']),
          'city'=>'',
          'tax_amt'=>($add_pro_arr['p_protax']+$old_order->Tax)- ($old_order->protax),
          'shipping_amt'=>'',
          'p_taxvat'=>($old_order->taxvat+$add_pro_arr['p_protaxvat']) - ($old_order->protaxvat),
          'p_taxcst'=>($old_order->taxcst+$add_pro_arr['p_protaxcst']) - ($old_order->protaxcst),
          'p_taxentry'=>($old_order->taxentry+$add_pro_arr['p_protaxentry']) - ($old_order->protaxentry)
          );
     //p($parameter);exit();
     //$this->supper_admin->call_procedureRow('proc_update_order_details',$parameter);

/*
    $parameter  = array(
                          'act_mode'=>'update',
                          'product_id'=>'',
                          'pro_qty'=>$prd_qty,
                          'state_id' =>'',
                          'ordertbl_id'=>'',
                          'ordrmaptbl_id'=>$_POST['prod_id'][0],
                          'price'=>$price,
                          'city'=>''
                        );
   

   $data = $this->supper_admin->call_procedure('proc_update_order_details',$parameter);

    $parameter  = array(
                          'act_mode'=>'update_order_tbl',
                          'product_id'=>'',
                          'pro_qty'=>$_POST['pro_val'],
                          'state_id' =>'',
                          'ordertbl_id'=>$_POST['orderid'],
                          'ordrmaptbl_id'=>'',
                          'price'=>'',
                          'city'=>''
                        );
   

   $data = $this->supper_admin->call_procedure('proc_update_order_details',$parameter);*/
   
   
 }
 public function delete_product($id,$proid,$qty)
 { 
      $parameter = array(
                          'act_mode'=>'delete_row',
                          'OrdProMap_Id'=>'',
                          'ord_id'=>$id,
                          'Pro_Id'=>$proid,
                          'Vendor_Id'=>'',
                          'baseprize'=>'',
                          'Qty'=>$qty,
                          'Ship_Status'=>'',''
                        );
   $this->supper_admin->call_procedure('proc_status_tbl_updt',$parameter);
   
 }
 public function get_attibute_opt($id)
 { 
    $parameter  = array(
                          'act_mode'=>'get_pro_attr',
                          'product_id'=>$id,
                          'pro_qty'=>'',
                          'state_id' =>'',
                          'ordertbl_id'=>'',
                          'ordrmaptbl_id'=>'',
                          'price'=>'',
                          'city'=>'',
                          'tax_amt'=>'',
                          'shipping_amt'=>'',
                          'p_taxvat'=>'',
                          'p_taxcst'=>'',
                          'p_taxentry'=>''
                        );
    $data['data']=$this->supper_admin->call_procedure('proc_update_order_details',$parameter);
    //p($data['data']);
    if(!empty($data['data']))
    { 
       echo "<option value=''>Choose Colour</option>";
       $this->load->view('order/get_color',$data);
    }else{
      echo 0;
    }
    
 }
 public function get_color($id)
 { 
    $parameter  = array(
                          'act_mode'=>'get_pro_color',
                          'product_id'=>$id,
                          'pro_qty'=>'',
                          'state_id' =>'',
                          'ordertbl_id'=>'',
                          'ordrmaptbl_id'=>'',
                          'price'=>'',
                          'city'=>'',
                          'tax_amt'=>'',
                          'shipping_amt'=>'',
                          'p_taxvat'=>'',
                          'p_taxcst'=>'',
                          'p_taxentry'=>''
                        );
    $data['data']=$this->supper_admin->call_procedure('proc_update_order_details',$parameter);
    //p($data['data']);
    if(!empty($data['data']))
    { 
       echo "<option value=''>Choose Colour</option>";
       $this->load->view('order/get_color',$data);
    }else{
      echo 0;
    }
    
 }

 public function get_img($id)
 { 
    $parameter  = array(
                          'act_mode'=>'get_pro_img',
                          'product_id'=>$id,
                          'pro_qty'=>'',
                          'state_id' =>'',
                          'ordertbl_id'=>'',
                          'ordrmaptbl_id'=>'',
                          'price'=>'',
                          'city'=>'',
                          'tax_amt'=>'',
                          'shipping_amt'=>'',
                          'p_taxvat'=>'',
                          'p_taxcst'=>'',
                          'p_taxentry'=>''
                        );
    $data=$this->supper_admin->call_procedureRow('proc_update_order_details',$parameter);
    
    if(!empty($data))
    { 
      if(file_exists(FCPATH.'images/hoverimg/'.$data->imgpath))
      {
        echo "<img height='80px' width='80px' src='".base_url()."images/hoverimg/$data->imgpath'>";
      }else{
        echo "<img height='80px' width='80px' src='".base_url()."images/no_img.jpg'>";
      }
       
    }else{
      echo 0;
    }
    
 }

 public function get_color_age($id)
 { 
    $parameter  = array(
                          'act_mode'=>'get_pro_color_age',
                          'product_id'=>$id,
                          'pro_qty'=>'',
                          'state_id' =>'',
                          'ordertbl_id'=>'',
                          'ordrmaptbl_id'=>'',
                          'price'=>'',
                          'city'=>'',
                          'tax_amt'=>'',
                          'shipping_amt'=>'',
                          'p_taxvat'=>'',
                          'p_taxcst'=>'',
                          'p_taxentry'=>''
                        );
    $parameter2  = array(
                          'act_mode'=>'get_pro_color_size',
                          'product_id'=>$id,
                          'pro_qty'=>'',
                          'state_id' =>'',
                          'ordertbl_id'=>'',
                          'ordrmaptbl_id'=>'',
                          'price'=>'',
                          'city'=>'',
                          'tax_amt'=>'',
                          'shipping_amt'=>'',
                          'p_taxvat'=>'',
                          'p_taxcst'=>'',
                          'p_taxentry'=>''
                        );
    $data['data']=$this->supper_admin->call_procedure('proc_update_order_details',$parameter);
    $data['data2']=$this->supper_admin->call_procedure('proc_update_order_details',$parameter2);
    //p($data['data']);exit();
      if(!empty($data['data2']) || !empty($data['data']))
      {


          if(!empty($data['data2']))
          {  
              echo "<option value=''>Choose Quantity</option>";
              foreach (range(1,$data['data2'][0]->proqty) as $value) {
              echo "<option value='".$value."'>".$value."</option>";
              }
              echo '#';
             echo "<option value=''>Choose Age</option>";
             foreach ($data['data2'] as $value) {
               echo "<option value='".$value->attvalue."'>".$value->attmapname."</option>";

             }
             
             
          }
          if(!empty($data['data']))
          {  
              echo "<option value=''>Choose Quantity</option>";
              foreach (range(1,$data['data'][0]->proqty) as $value) {
              echo "<option value='".$value."'>".$value."</option>";
              }
             echo '#';
             echo "<option value=''>Choose Age</option>";
             foreach ($data['data'] as $value) {
               echo "<option value='".$value->attvalue."'>".$value->attmapname."</option>";

             }
          }
      }else{
        echo 0;
      }
    
 }
public function add_product($id)
{
   $prd_qty=$_POST['proqty'];

            $parameter = array('vendor_dtls',$_POST['userid'],'','','','','','');
            $responce = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
            
            $parameter = array('tax_details',$_POST['state'],'',explode('_',$_POST['prod'])[1],'','','','');
            $tax_data = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
            //pend($tax_data);
            $tin = $cst = $entry ="";
            foreach ($tax_data as $value) {
              if($value->taxidd==$responce->tinid)
              {
                $tin=$value->taxprice;
              }elseif ($value->taxidd==$responce->cstid) {
                $cst=$value->taxprice;
              }elseif ($value->taxidd==5) {
                $entry=$value->taxprice;
              }
            }
                      
    if($_POST['color']=="")
    { 
        
            $parameter  = array(
            'act_mode'=>'get_only_pro_dtls',
            'product_id'=>$_POST['prod'],
            'pro_qty'=>$_POST['proqty'],
            'state_id' =>$_POST['state'],
            'ordertbl_id'=>'',
            'ordrmaptbl_id'=>'',
            'price'=>'',
            'city'=>$_POST['city'],
            'tax_amt'=>'',
            'shipping_amt'=>'',
            'p_taxvat'=>'',
            'p_taxcst'=>'',
            'p_taxentry'=>''
            );

            $data=$this->supper_admin->call_procedureRow('proc_update_order_details',$parameter);
            //pend($data);
            
            if(!empty($data->qty1) || !empty($data->qty2) || !empty($data->qty3))
            { 

                foreach ($data as $key => $range) 
                { 

                  if($key=='qty1')
                  { 
                      $min=explode('to',$range)[0];
                      $max=explode('to',$range)[1];
                      if(($min <= $prd_qty) && ($prd_qty <= $max))
                      {
                         $qty_disc=$data->price;
                      }
                  }else if($key=='qty2')
                  {
                      $min=explode('to',$range)[0];
                      $max=explode('to',$range)[1];
                      if(($min <= $prd_qty) && ($prd_qty <= $max))
                      {
                         $qty_disc=$data->price2;
                      }
                  }else if($key=='qty3')
                  {
                      $min=explode('to',$range)[0];
                      $max=explode('to',$range)[1];
                      if(($min <= $prd_qty) && ($prd_qty <= $max))
                      {
                          $qty_disc=$data->price3;
                      }
                  }
                  
                }

            }

            if(!empty($qty_disc) && !empty($data->cityvalue))
            {
            $sale_pz=number_format($data->prodsellingprice,1,".","") + number_format((number_format($data->prodsellingprice,1,".","") * $data->cityvalue / 100),1,".","");
            $sale_pz=number_format($data->prodsellingprice,1,".","") - number_format((number_format($data->prodsellingprice,1,".","") * $qty_disc / 100),1,".","");
            }else if (!empty($qty_disc) && empty($data->cityvalue))
            {
            $sale_pz=number_format($data->prodsellingprice,1,".","") - number_format((number_format($data->prodsellingprice,1,".","") * $qty_disc / 100),1,".","");
            }
            else if (empty($qty_disc) && !empty($data->cityvalue))
            {
            $sale_pz=number_format($data->prodsellingprice,1,".","") + number_format((number_format($data->prodsellingprice,1,".","") * $data->cityvalue / 100),1,".","");
            }else{
            $sale_pz=number_format($data->prodsellingprice,1,".","");
            }

            $parameter = array( 
            'act_mode'  =>'add_more_prod',
            'p_orderId' => $id,
            'p_proid'   => $data->promapid,
            'p_CatId'   =>$data->catid,
            'p_SkuNumber'=>$data->sellersku,
            'p_proname'  => $data->proname,
            'p_prodPrice'=> number_format($data->prodmrp,1,".",""),
            'p_VenSellPrice'=>number_format($sale_pz,1,".",""),
            'p_FinalPrice'=>number_format($sale_pz,1,".",""),
            'p_OrdSize'=>'',
            'p_OrdQty'=>$_POST['proqty'],
            'p_shippingcharge' => 0,
            'p_vendorId'=> $data->manufactureid,
            'p_OrdColor'=>'',
            'p_ProImage'=>base_url().'images/hoverimg/'.$data->imgpath,
            'p_OrdShipping'=>'pending',
            'p_protax'=>'',
            'p_protaxvat'=>number_format((((number_format($sale_pz,1,".","") * $_POST['proqty']) * ($tin))/100),1,".",""),
            'p_protaxcst'=>'',
            'p_protaxentry'=>'',
            'vat_p'=>$tin,
            'cst_p'=>$cst,
            'entry'=>$entry
            
            );
           //pend($parameter);
            $this->supper_admin->call_procedureRow('proc_add_more_prod',$parameter);
// Start push notification for app 
      /*$parameterapp = array('act_mode'=>'getappaddorderdeviceid','row_id'=>$id,'catid'=>'');
      $appdeviceids  = $this->supper_admin->call_procedureRow('proc_product',$parameterapp);
      $appdeviceiddata = array($appdeviceids->appdeviceid);
      $appmsgdevice=array('message'=>'One Products are Added in your existing order. OrderId is '.$appdeviceids->OrderNumber,'title'=>'Add Products');
      $appnotification='2';
      send_andriod_notification($appdeviceiddata,$appmsgdevice,$appnotification);*/
// End push notification for app 
    }
    else{
           //pend('byee');
            $parameter  = array(
            'act_mode'=>'get_pro_dtls',
            'product_id'=>explode(',', $_POST['color'])[1],
            'pro_qty'=>'',
            'state_id' =>$_POST['state'],
            'ordertbl_id'=>$_POST['OrderId'],
            'ordrmaptbl_id'=>'',
            'price'=>'',
            'city'=>$_POST['city'],
            'tax_amt'=>'',
            'shipping_amt'=>'',
            'p_taxvat'=>'',
            'p_taxcst'=>'',
            'p_taxentry'=>''
            );

          $data=$this->supper_admin->call_procedureRow('proc_update_order_details',$parameter);
          //p($data);exit();

            $parameter  = array(
            'act_mode'=>'get_range',
            'product_id'=>$data->promapid,
            'pro_qty'=>'',
            'state_id' =>'',
            'ordertbl_id'=>'',
            'ordrmaptbl_id'=>'',
            'price'=>'',
            'city'=>'',
            'tax_amt'=>'',
            'shipping_amt'=>'',
            'p_taxvat'=>'',
            'p_taxcst'=>'',
            'p_taxentry'=>''
            );
     
          $price_range = $this->supper_admin->call_procedure('proc_update_order_details',$parameter);
         //p($price_range);exit();
          if(!empty($price_range))
          { 
            
            foreach ($price_range[0] as $key => $range) 
           { 

              if($key=='qty1')
              { 
                  $min=explode('to',$range)[0];
                  $max=explode('to',$range)[1];
                  if(($min <= $prd_qty) && ($prd_qty <= $max))
                  {
                     $qty_disc=$price_range[0]->price;
                  }
              }else if($key=='qty2')
              {
                  $min=explode('to',$range)[0];
                  $max=explode('to',$range)[1];
                  if(($min <= $prd_qty) && ($prd_qty <= $max))
                  {
                     $qty_disc=$price_range[0]->price2;
                  }
              }else if($key=='qty3')
              {
                  $min=explode('to',$range)[0];
                  if($prd_qty >= $min)
                  {
                      $qty_disc=$price_range[0]->price3;
                  }
              }
              
           }
           
          }
          
           if(!empty($qty_disc) && !empty($data->cityvalue))
            {
            $sale_pz=number_format($data->prodsellingprice,1,".","") + number_format((number_format($data->prodsellingprice,1,".","") * $data->cityvalue / 100),1,".","");
            $sale_pz=number_format($data->prodsellingprice,1,".","") - number_format((number_format($data->prodsellingprice,1,".","") * $qty_disc / 100),1,".","");
            }else if (!empty($qty_disc) && empty($data->cityvalue))
            {
            $sale_pz=number_format($data->prodsellingprice,1,".","") - number_format((number_format($data->prodsellingprice,1,".","") * $qty_disc / 100),1,".","");
            }
            else if (empty($qty_disc) && !empty($data->cityvalue))
            {
            $sale_pz=number_format($data->prodsellingprice,1,".","") + number_format((number_format($data->prodsellingprice,1,".","") * $data->cityvalue / 100),1,".","");
            }else{
            $sale_pz=number_format($data->prodsellingprice,1,".","");
            }

         $add_pro_arr = array( 
          'act_mode'  =>'add_more_prod',
          'p_orderId' => $id,
          'p_proid'   => $data->promapid,
          'p_CatId'   =>$data->catid,
          'p_SkuNumber'=>$data->sellersku,
          'p_proname'  => $data->proname,
          'p_prodPrice'=> number_format($data->prodmrp,1,".",""),
          'p_VenSellPrice'=>number_format($sale_pz,1,".",""),
          'p_FinalPrice'=>number_format($sale_pz,1,".",""),
          'p_OrdSize'=>$_POST['pro_size'],
          'p_OrdQty'=>$_POST['proqty'],
          'p_shippingcharge' => 0,
          'p_vendorId'=>$data->manufactureid,
          'p_OrdColor'=>explode(',',$_POST['color'])[0],
          'p_ProImage'=>base_url().'images/hoverimg/'.$data->imgpath,
          'p_OrdShipping'=>'pending',
          //'p_protax'=>'',
          'p_protaxvat'=>number_format((((number_format($sale_pz,1,".","") * $_POST['proqty']) * ($tin))/100),1,".",""),
          'p_protaxcst'=>number_format((((number_format($sale_pz,1,".","") * $_POST['proqty']) * ($cst))/100),1,".",""),
          'p_protaxentry'=>number_format((((number_format($sale_pz,1,".","") * $_POST['proqty']) * ($entry))/100),1,".",""),
          
          );
          $add_pro_arr['p_protax']=$add_pro_arr['p_protaxvat']+$add_pro_arr['p_protaxcst']+$add_pro_arr['p_protaxentry'];
          $add_pro_arr['vat_p']=$tin;
          $add_pro_arr['cst_p']=$cst;
          $add_pro_arr['entry']=$entry;
           //p($add_pro_arr);exit();
          
          $this->supper_admin->call_procedureRow('proc_add_more_prod',$add_pro_arr);
// Start push notification for app 
      /*$parameterapp = array('act_mode'=>'getappaddorderdeviceid','row_id'=>$id,'catid'=>'');
      $appdeviceids  = $this->supper_admin->call_procedureRow('proc_product',$parameterapp);
      $appdeviceiddata = array($appdeviceids->appdeviceid);
      $appmsgdevice=array('message'=>'One Products are Added in your existing order. OrderId is '.$appdeviceids->OrderNumber,'title'=>'Add Products');
      $appnotification='2';
      send_andriod_notification($appdeviceiddata,$appmsgdevice,$appnotification);*/
// End push notification for app 
       }
  
     
      
}

//-----------------------------------------new order flow ------------------------------------------//
public function pending(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'pendingorder','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
  //p($responce);exit();
  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/pending?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'pendingorder','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  //$responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

     //----------------  end pagination setting ------------------------//
     //p($responce);exit(); 
  $this->load->view('helper/header');
  $this->load->view('order/pending',$responce);
 }


public function ordpendingdetail($ordid){
 $parameter = array('user_details','','','',$ordid,'','','');
 $responce['user_dtls'] = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
 //pend($responce);
 if(!empty($_POST)){
 	//p($_POST);exit();
  
       foreach ($this->input->post('proApp') as $key => $value) 
       {
        if($this->input->post('chstatus')=='onprocess'){
          $test++;
          if($this->input->post('OrdQty')[$value]<=$this->input->post('totalstock')[$value]){
            $test++;
           $this->session->set_flashdata("message", "Your Stock Quantity is already available.");
          }else{
           
            $parameterrs = array('act_mode'=>'insertpodata','row_id'=>$value,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
            $responce['podata'] = $this->supper_admin->call_procedure('proc_orderflow',$parameterrs);
            
            $parameter = array('act_mode'=>'update','row_id'=>$value,'orderstatus'=>$this->input->post('chstatus'),'order_proid'=>$this->input->post('proid')[$value],'order_id'=>$ordid,'order_venid'=>$this->input->post('venid')[$value],'order_qty'=>$this->input->post('OrdQty')[$value],'oldstatus'=>$this->input->post('oldstatus')[$value]);
            //p($parameter);exit();
            $responce['pende'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
            ///p($responce['pende']);exit(); 
          }

        }
        else{
          
        $parameter = array('act_mode'=>'update','row_id'=>$value,'orderstatus'=>$this->input->post('chstatus'),'order_proid'=>$this->input->post('proid')[$value],'order_id'=>$ordid,'order_venid'=>$this->input->post('venid')[$value],'order_qty'=>$this->input->post('OrdQty')[$value],'oldstatus'=>$this->input->post('oldstatus')[$value]);
        //p($parameter);exit();
        $responce['pende'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
      }
    }
        //p($responce['pende']);exit();

        redirect('admin/order/ordpendingdetail/'.$ordid);
    }

 $parameter = array('act_mode'=>'pendingdetail','row_id'=>$ordid,'orderstatus'=>'pending','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 $parameter= array(
  'act_mode'=>'get_pro_list',
  'row_id'=>'',
  'orderstatus'=>'pending',
  'order_proid'=>'',
  'order_id'=>'',
  'order_venid'=>'',
  'order_qty'=>'',
  'oldstatus'=>''
  );
 $responce['product_list'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 //p($responce['pendetailview']);exit();
 $this->load->view('helper/header');
 $this->load->view('order/pendingdetails',$responce);
 //p($responce['penview']);exit;
}

public function holdorder(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'holdview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
   //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/holdorder?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'holdview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  #$responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/holdview',$responce);
 }
public function dispatchorder(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'dispatchview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/dispatchorder?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'dispatchview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  #$responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/dispatchview',$responce);
 }
 public function deliveredorder(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'deliveredview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/deliveredorder?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'deliveredview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  #$responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/deliveredview',$responce);
 } 
 public function cancelorder(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'canclview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/cancelorder?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'canclview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  #$responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  foreach ($responce['penview'] as $value) {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt_cancelord($value->OrderId);
            }  

     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/cancelview',$responce);
 }

 public function ordcanceldetail($ordid){
 $parameter        = array('act_mode'=>'canceldetail','row_id'=>'','orderstatus'=>'cancel','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 
 if(!empty($_POST)){
  //p($_POST);exit();
       foreach ($this->input->post('proApp') as $key => $value) {
                 
        $parameter = array('act_mode'=>'update','row_id'=>$value,'orderstatus'=>$this->input->post('chstatus'),'order_proid'=>$this->input->post('proid')[$value],'order_id'=>$ordid,'order_venid'=>$this->input->post('venid')[$value],'order_qty'=>$this->input->post('OrdQty')[$value],'oldstatus'=>$this->input->post('oldstatus')[$value]);
        //p($parameter);exit();
        $responce['pende'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
       }
        //p($responce['pende']);exit();
        redirect('admin/order/orddispatchdetail/'.$ordid);
    }
  //p($_POST);exit();
 $this->load->view('helper/header');
 $this->load->view('order/canceldetails',$responce);

}
public function returnorder(){
  $this->userfunction->loginAdminvalidation();

  if(!empty($_GET['Searchrtl'])){
      $parameter        = array('act_mode'=>'returncompletesearchview','row_id'=>'','orderstatus'=>'%'.$_GET['searchboxretail'].'%','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
         //----------------  start pagination setting ------------------------// 

           $config['base_url'] = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
           $config['total_rows']       = count($responce['penview']);
           $config['per_page']         = 50;
           $config['use_page_numbers'] = TRUE;

           $this->pagination->initialize($config);
           if($_GET['page']){
             $page         = $_GET['page']-1 ;
             $page         = ($page*50);
             $second       = $config['per_page'];
           }
           else{
             $page         = 0;
             $second       = $config['per_page'];
           }
           
           $str_links = $this->pagination->create_links();
           $responce["links"]  = explode('&nbsp;',$str_links );
          $parameter        = array('act_mode'=>'returncompletesearchview','row_id'=>$page,'orderstatus'=>'%'.$_GET['searchboxretail'].'%','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  } else {

        $parameter        = array('act_mode'=>'returncompleteview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
         //----------------  start pagination setting ------------------------// 

           $config['base_url']         = base_url()."admin/order/returnorder?";
           $config['total_rows']       = count($responce['penview']);
           $config['per_page']         = 50;
           $config['use_page_numbers'] = TRUE;

           $this->pagination->initialize($config);
           if($_GET['page']){
             $page         = $_GET['page']-1 ;
             $page         = ($page*50);
             $second       = $config['per_page'];
           }
           else{
             $page         = 0;
             $second       = $config['per_page'];
           }
           
           $str_links = $this->pagination->create_links();
           $responce["links"]  = explode('&nbsp;',$str_links );
          $parameter        = array('act_mode'=>'returncompleteview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        #$responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

}

  /*foreach ($responce['penview'] as $value) {
                if (substr($value->Remark, 0, 17)=="Order Refund Case") {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt_returnord($value->OrderId,'Order Refund Case');
                } else {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt_returnord($value->OrderId,'Order Replacement Case');
                }  
            }  */

     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/returnview',$responce);
 }

 public function ordreturndetail($ordid){
 $parameter        = array('act_mode'=>'returncompletedetail','row_id'=>'','orderstatus'=>'return','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>$_GET['ordcase'].'%');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/returndetails',$responce);

}

 public function ordholddetail($ordid){
 $parameter        = array('act_mode'=>'holddetail','row_id'=>'','orderstatus'=>'hold','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 

 if(!empty($_POST)){
  //p($_POST);exit();
       foreach ($this->input->post('proApp') as $key => $value) {
        if($this->input->post('chstatus')=='onprocess'){
          if($this->input->post('OrdQty')[$value]<=$this->input->post('totalstock')[$value]){
           $this->session->set_flashdata("message", "Your Stock Quantity is already available.");
          }else{
           
            $parameterrs = array('act_mode'=>'insertpodata','row_id'=>$value,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
            //p($parameterrs);exit();
            $responce['podata'] = $this->supper_admin->call_procedure('proc_orderflow',$parameterrs);


            $parameter = array('act_mode'=>'update','row_id'=>$value,'orderstatus'=>$this->input->post('chstatus'),'order_proid'=>$this->input->post('proid')[$value],'order_id'=>$ordid,'order_venid'=>$this->input->post('venid')[$value],'order_qty'=>$this->input->post('OrdQty')[$value],'oldstatus'=>$this->input->post('oldstatus')[$value]);
            //p($parameter);exit();
            $responce['pende'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
          }

        }
        else{
          
        $parameter = array('act_mode'=>'update','row_id'=>$value,'orderstatus'=>$this->input->post('chstatus'),'order_proid'=>$this->input->post('proid')[$value],'order_id'=>$ordid,'order_venid'=>$this->input->post('venid')[$value],'order_qty'=>$this->input->post('OrdQty')[$value],'oldstatus'=>$this->input->post('oldstatus')[$value]);
        //p($parameter);exit();
        $responce['pende'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
      }
        }
        //p($responce['pende']);exit();
        redirect('admin/order/ordholddetail/'.$ordid);
    }


 $this->load->view('helper/header');
 $this->load->view('order/holddetails',$responce);

}

public function orddispatchdetail($ordid){
 $parameter        = array('act_mode'=>'dispatchdetail','row_id'=>'','orderstatus'=>'dispatch','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 if(!empty($_POST)){
  //p($_POST);exit();
       foreach ($this->input->post('proApp') as $key => $value) {
                 
        $parameter = array('act_mode'=>'update','row_id'=>$value,'orderstatus'=>$this->input->post('chstatus'),'order_proid'=>$this->input->post('proid')[$value],'order_id'=>$ordid,'order_venid'=>$this->input->post('venid')[$value],'order_qty'=>$this->input->post('OrdQty')[$value],'oldstatus'=>$this->input->post('oldstatus')[$value]);
        //p($parameter);exit();
        $responce['pende'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
       }
        //p($responce['pende']);exit();
        redirect('admin/order/orddispatchdetail/'.$ordid);
    }

    //--------------------------invoice page---------------------
  $parametersin      = array('act_mode'=>'singrecod','row_id'=>'','orderstatus'=>'dispatch','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $data['record'] = $this->supper_admin->call_procedureRow('proc_orderflow',$parametersin); 
        $parameteradm     = array('act_mode'=>'admin','row_id'=>'','orderstatus'=>'dispatch','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $data['detialrecord'] = $this->supper_admin->call_procedure('proc_orderflow',$parameteradm);
//p($this->uri->segment(5));exit();
        if($this->uri->segment(5)=='invoice')
        {
         $this->load->view('order/invoice',$data);
        }else{
          $this->load->view('helper/header');
 		  $this->load->view('order/dispatchdetails',$responce);
        } 

//-------------------------end invoice page---------------------------

}

public function orddliverdetail($ordid){
 $parameter        = array('act_mode'=>'deliverdetail','row_id'=>'','orderstatus'=>'delivered','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/deliverdetails',$responce);

}

public function onlineorder(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'onlineview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/onlineorder?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'onlineview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
   foreach ($responce['penview'] as $value) {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt($value->OrderId);
            }  
     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/onlineview',$responce);
 }

 public function codorder(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'codview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/codorder?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'codview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 foreach ($responce['penview'] as $value) {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt($value->OrderId);
            }  
     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/codview',$responce);
 }

 public function chequeorder(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'chequeview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/chequeorder?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'chequeview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
  foreach ($responce['penview'] as $value) {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt($value->OrderId);
            } 
     //----------------  end pagination setting ------------------------// 

  $this->load->view('helper/header');
  $this->load->view('order/chequeview',$responce);
 }

 public function creditorder(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'creditview','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/creditorder?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'creditview','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
  foreach ($responce['penview'] as $value) {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt($value->OrderId);
            } 
     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/creditview',$responce);
 }

 public function ordcoddetail($ordid){
 $parameter        = array('act_mode'=>'coddetail','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/coddetails',$responce);
 //p($responce['penview']);exit;
}

public function ordonlinedetail($ordid){
 $parameter        = array('act_mode'=>'onlinedetail','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/onlinedetails',$responce);
 //p($responce['penview']);exit;
}
public function ordchequedetail($ordid){
 $parameter        = array('act_mode'=>'chequedetail','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/chequedetails',$responce);
 //p($responce['penview']);exit;
}
public function ordcreditdetail($ordid){
 $parameter        = array('act_mode'=>'creditdetail','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/creditdetails',$responce);
 //p($responce['penview']);exit;
}

public function codpaymentupdate($ordid,$pay){
  
  if($pay=='COD'){
     $parameter = array('act_mode'=>'updatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/order/codorder');
  }

  if($pay=='CHEQUE'){
     $parameter = array('act_mode'=>'updatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/order/chequeorder');
  }

  if($pay=='CREDIT'){
     $parameter = array('act_mode'=>'updatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/order/creditorder');
  }
 
}

public function paymentreupdate($ordid,$pay){
  
  if($pay=='COD'){
     $parameter = array('act_mode'=>'reupdatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/order/codorder');
  }

  if($pay=='CHEQUE'){
     $parameter = array('act_mode'=>'reupdatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/order/chequeorder');
  }

  if($pay=='CREDIT'){
     $parameter = array('act_mode'=>'reupdatepaymentstatus','row_id'=>$ordid,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
     $this->session->set_flashdata("message", "Your information was successfully Saved");
     redirect('admin/order/creditorder');
  }
 
}



public function updateorderqty(){
  $proid=$this->input->post('proid');
  $proqty=$this->input->post('qnty');
  $cityid=$this->input->post('cityid');
  $cattid=$this->input->post('catid');
  $brandid=$this->input->post('brndid');
  $stateidd=$this->input->post('stateid');
  $cstid=$this->input->post('cstid');
  $tinid=$this->input->post('tinid');
  $ordid=$this->input->post('ordid');
  $ordproid=$this->input->post('ordproid');
  $unitpricee=$this->input->post('unitprc');

  $param=array('proid'  =>(int)$proid,
               'cityidd'=>$cityid);
  $path = api_url().'productlisting/procheckqty/format/json/';
  $dataqty=curlpost($param,$path);

  $cartdata['promaxmsg']="<span class='qtymsg'>The product <b>'$dataqty->ProName'</b> have been updated successfully</span>"; 
  
  $newqty    =explode('to', $dataqty->qty1);
  $newqty2   =explode('to', $dataqty->qty2);
  $newqty3   =explode('to', $dataqty->qty3);

  if($this->input->post('qnty')<$newqty[0] ){
  $newfinalPrice='';  

  /*$newfinalPrice=citybaseprice($unitpricee,$dataqty->cityvalue);*/
  $newfinalPrice=ceil($unitpricee);
  }
 if($this->input->post('qnty')>=$newqty[0] && $this->input->post('qnty')<=$newqty[1] ){
  $newfinalPrice='';
  $newfinalPrice=ceil($unitpricee-($unitpricee*$dataqty->price/100));
  /*$newfinalPrice=cityqtyprice($unitpricee,$dataqty->cityvalue,$dataqty->price);*/
  }
  if($this->input->post('qnty')>=$newqty2[0] && $this->input->post('qnty')<=$newqty2[1] ){
  /*$newfinalPrice=cityqtyprice($unitpricee,$dataqty->cityvalue,$dataqty->price2);*/
  $newfinalPrice=ceil($unitpricee-($unitpricee*$dataqty->price2/100));
  }
  if($this->input->post('qnty')>=$newqty3[0] && $this->input->post('qnty')<='100000' ){
  /*$newfinalPrice=cityqtyprice($unitpricee,$dataqty->cityvalue,$dataqty->price3);*/
  $newfinalPrice=ceil($unitpricee-($unitpricee*$dataqty->price3/100));
  //p($newfinalPrice);exit;
  }
  if($this->input->post('qnty')==0){
  $newfinalPrice=0;
  } 

  $taxpath = api_url().'checkoutapi/vattax/brandid/'.$brandid.'/catid/'.$cattid.'/stateid/'.$stateidd.'/format/json/';
  $record['viewtex'] = curlget($taxpath);
  foreach ($record['viewtex'] as $key => $valtax) 
  { 
     if($cstid== $valtax['taxidd']) //2,4 CST
      { 
         $cst_name=$valtax['taxname'];
         $cst_value+=ceil((ceil($newfinalPrice*$proqty))*($valtax['taxprice'])/100);
         $cpt=ceil((ceil($newfinalPrice*$proqty))*($valtax['taxprice'])/100);
         //p($cpt);exit();
         
      }
      if($tinid== $valtax['taxidd']) //1,6 TIN
      { 
         $tin_name=$valtax['taxname'];
         $tin_value+=ceil((ceil($newfinalPrice*$proqty))*($valtax['taxprice'])/100);
         $tpt=ceil((ceil($newfinalPrice*$proqty))*($valtax['taxprice'])/100);
      }
   
    if($valtax['taxidd']==5)  //entry
    { 
         $entry_name=$valtax['taxname'];
         $entry_value+=ceil((ceil($newfinalPrice*$proqty))*($valtax['taxprice'])/100);
         $ept=ceil((ceil($newfinalPrice*$proqty))*($valtax['taxprice'])/100);
     }
  } 

 
$parameter11 = array('act_mode'=>'invqtyupdate','row_id'=>$ordid,'orde_mproid'=>$ordproid,'ord_qty'=>$proqty,'ord_price'=>ceil($newfinalPrice),'ord_total'=>'','ord_taxtotals'=>'','ord_vat'=>$tpt,'ord_cst'=>$cpt,'ord_entry'=>$ept);
  //p($parameter11);exit;
$insert = $this->supper_admin->call_procedure('proc_invqtyupdate',$parameter11);
//p($insert);exit;
/*
<pre>Array
(
    [0] => stdClass Object
        (
            [FinalPrice] => 235.00
            [OrdQty] => 2
            [protaxvat] => 29
            [protaxcst] => 38
            [protaxentry] => 15
        )

    [1] => stdClass Object
        (
            [FinalPrice] => 539.00
            [OrdQty] => 1
            [protaxvat] => 54
            [protaxcst] => 65
            [protaxentry] => 54
        )

)
</pre>
*/
foreach ($insert as $key => $value) {
  
  $subtotal[]=$value->FinalPrice*$value->OrdQty;
  $totaltax[]=$value->protaxvat+$value->protaxcst+$value->protaxentry;
  $totaltaxvat[]=$value->protaxvat;
  $totaltaxcst[]=$value->protaxcst;
  $totaltaxentry[]=$value->protaxentry;
  $totalamt[]=$subtotal+$totaltax;
//p($subtotal);
//p($totaltax);
//p($totalamt);
}

$t_subtotal=array_sum($subtotal);
$t_totaltax=array_sum($totaltax);
$t_totaltaxvat=array_sum($totaltaxvat);
$t_totaltaxcst=array_sum($totaltaxcst);
$t_totaltaxentry=array_sum($totaltaxentry);
$t_totalamt=($t_subtotal+$t_totaltax);

//$t_totalamt
//p($t_subtotal);
//p($t_totalamt);
//p($t_totaltax);
$parameters1 = array('act_mode'=>'inqtyorder','row_id'=>$ordid,'orde_mproid'=>'','ord_qty'=>'','ord_price'=>$t_subtotal,'ord_total'=>$t_totalamt,'ord_taxtotals'=>$t_totaltax,'ord_vat'=>$t_totaltaxvat,'ord_cst'=>$t_totaltaxcst,'ord_entry'=>$t_totaltaxentry);
  //p($parameters1);exit;
$insertord = $this->supper_admin->call_procedure('proc_invqtyupdate',$parameters1);
//p($totalamt);
 //exit;
return;

}
public function update_base_prc()
{
   $parameter = array(
                          'act_mode'=>'uptodate_baseprc',
                          'OrdProMap_Id'=>'',
                          'ord_id'=>$_POST['statsid'],
                          'Pro_Id'=>'',
                          'Vendor_Id'=>'',
                          'baseprize'=>$_POST['newprice'],
                          'Qty'=>'',
                          'Ship_Status'=>'',''
                        );
   $this->supper_admin->call_procedure('proc_status_tbl_updt',$parameter);

 }

 public function uptodate_order_satatus()
 { 
  //p($_POST);exit();
  $ids=0;
  if($_POST['flag']=="onprocess"){
    if($this->input->post('cur_qty')<=$this->input->post('total_stock')){
          $this->session->set_flashdata("message", "Your Quantity is in stock.");
    }else{
      
        $parameter=array(
                      'act_mode'  => 'get_ord_dtls',
                      'row_id'    => $this->input->post('statsid'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>''

                    );
   
    $data=$this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    //p($data);exit();
   
    if($_POST['cur_qty']==$_POST['old_qty'])
    {
      $paramss=array(
                      'act_mode'=>'update_all_qty',
                      'rowid'=>$_POST['statsid'],
                      'promapid'=>$data->OrderproId,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>$_POST['old_stat'],
                      'orderproid'=>$data->ProId,
                      'order_id'=>$data->OrderId,
                      'order_venid'=>$data->VendorID,
                      'order_qty'=>$_POST['cur_qty'],
                      'comnt'=>'',
                      'old_qty'=>$_POST['old_qty'],
                      'price'=>$data->base_prize

                    );
      
      $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
      $response['or']= curlpost($paramss,$getting_pat);
    }else if($_POST['cur_qty'] < $_POST['old_qty']){
        
            $paramss=array(
                          'act_mode'=>'update_less_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>'',
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>get_unit_price($data->ProId,$_POST['cur_qty'], $data->citygroupid)

                        );
           $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           $response['or']= curlpost($paramss,$getting_pat);

            $paramss=array(
                          'act_mode'=>'update_remaining_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>$_POST['comment'],
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>get_unit_price($data->ProId, ($_POST['old_qty'] - $_POST['cur_qty']), $data->citygroupid)

                        );
            //p($paramss);exit();
           $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           $response['or']= curlpost($paramss,$getting_pat);


    }else{
             $paramss=array(
                              'act_mode'=>'update_increserd_qty',
                              'rowid'=>$_POST['statsid'],
                              'promapid'=>$data->OrderproId,
                              'stats'=>$_POST['flag'],
                              'oldstats'=>$_POST['old_stat'],
                              'orderproid'=>$data->ProId,
                              'order_id'=>$data->OrderId,
                              'order_venid'=>$data->VendorID,
                              'order_qty'=>$_POST['cur_qty'],
                              'comnt'=>'',
                              'old_qty'=>$_POST['old_qty'],
                              'price'=>get_unit_price($data->ProId, $_POST['cur_qty'], $data->citygroupid)

                            );
             
               $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
               $response['or']= curlpost($paramss,$getting_pat);
    }
    $parameter=array(
                      'act_mode'  => 'get_last_changed_dtls',
                      'row_id'    => '',
                      'orderstatus'=>$_POST['flag'],
                      'order_proid'=>$data->OrderproId,
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>$_POST['cur_qty'],
                      'oldstatus'=>''
                    );
   
      $data=$this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
      $parameterrs = array('act_mode'=>'insertpodata','row_id'=>$data->StatusOrderId,'orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
        $responce['podata'] = $this->supper_admin->call_procedure('proc_orderflow',$parameterrs);
  }
  }else if($_POST['flag']=="cancel" || $_POST['flag']=="hold"){
    if($this->input->post('cur_qty') > $this->input->post('old_qty')){
          $this->session->set_flashdata("message", "Invalide Quantity.");
    }else{ 
             $parameter=array(
                      'act_mode'  => 'get_ord_dtls',
                      'row_id'    => $this->input->post('statsid'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>''

                    );
   
         $data=$this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
      if($_POST['cur_qty']==$_POST['old_qty'])
    {
      $paramss=array(
                      'act_mode'=>'update_all_qty',
                      'rowid'=>$_POST['statsid'],
                      'promapid'=>$data->OrderproId,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>$_POST['old_stat'],
                      'orderproid'=>$data->ProId,
                      'order_id'=>$data->OrderId,
                      'order_venid'=>$data->VendorID,
                      'order_qty'=>$_POST['cur_qty'],
                      'comnt'=>'',
                      'old_qty'=>$_POST['old_qty'],
                      'price'=>$data->base_prize

                    );
      
      $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
      $response['or']= curlpost($paramss,$getting_pat);
    }else if($_POST['cur_qty'] < $_POST['old_qty']){
          
              $paramss=array(
                            'act_mode'=>'update_less_qty',
                            'rowid'=>$_POST['statsid'],
                            'promapid'=>$data->OrderproId,
                            'stats'=>$_POST['flag'],
                            'oldstats'=>$_POST['old_stat'],
                            'orderproid'=>$data->ProId,
                            'order_id'=>$data->OrderId,
                            'order_venid'=>$data->VendorID,
                            'order_qty'=>$_POST['cur_qty'],
                            'comnt'=>'',
                            'old_qty'=>$_POST['old_qty'],
                            'price'=>$data->base_prize

                          );
             $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
             $response['or']= curlpost($paramss,$getting_pat);

              $paramss=array(
                            'act_mode'=>'update_remaining_qty',
                            'rowid'=>$_POST['statsid'],
                            'promapid'=>$data->OrderproId,
                            'stats'=>$_POST['flag'],
                            'oldstats'=>$_POST['old_stat'],
                            'orderproid'=>$data->ProId,
                            'order_id'=>$data->OrderId,
                            'order_venid'=>$data->VendorID,
                            'order_qty'=>$_POST['cur_qty'],
                            'comnt'=>$_POST['comment'],
                            'old_qty'=>$_POST['old_qty'],
                            'price'=>get_unit_price($data->ProId, ($_POST['old_qty'] - $_POST['cur_qty']), $data->citygroupid)

                          );
             $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
             $response['or']= curlpost($paramss,$getting_pat);


      }
    }
         
  }else if($_POST['flag']=="pending"){
    $parameter=array(
                      'act_mode'  => 'get_ord_dtls',
                      'row_id'    => $this->input->post('statsid'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>''

                    );
    $data=$this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    $paramss=array(
                      'act_mode'=>'update_all_qty',
                      'rowid'=>$_POST['statsid'],
                      'promapid'=>$data->OrderproId,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>$_POST['old_stat'],
                      'orderproid'=>$data->ProId,
                      'order_id'=>$data->OrderId,
                      'order_venid'=>$data->VendorID,
                      'order_qty'=>$_POST['cur_qty'],
                      'comnt'=>'',
                      'old_qty'=>$_POST['old_qty'],
                      'price'=>$data->base_prize

                    );
      
      $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
      $response['or']= curlpost($paramss,$getting_pat);
  }else if($_POST['flag']=="dpending"){

    $parameter=array(
                      'act_mode'  => 'get_ord_dtls',
                      'row_id'    => $this->input->post('statsid'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>''

                    );
    $data=$this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    $paramss=array(
                      'act_mode'=>'update_dispatchtopending',
                      'rowid'=>$_POST['statsid'],
                      'promapid'=>$data->OrderproId,
                      'stats'=>'pending',
                      'oldstats'=>$_POST['old_stat'],
                      'orderproid'=>$data->ProId,
                      'order_id'=>$data->OrderId,
                      'order_venid'=>$data->VendorID,
                      'order_qty'=>$_POST['cur_qty'],
                      'comnt'=>'',
                      'old_qty'=>$_POST['old_qty'],
                      'price'=>$data->base_prize

                    );
      
      $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
      $response['or']= curlpost($paramss,$getting_pat);
  }else if($_POST['flag']=="dispatch"){
    if($this->input->post('cur_qty') > $this->input->post('total_stock')){
          $this->session->set_flashdata("message", "Your quantity is out of stock.");
          
    }else{ 
             $parameter=array(
                      'act_mode'  => 'get_ord_dtls',
                      'row_id'    => $this->input->post('statsid'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>''

                    );
   
            $data=$this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
      if($_POST['cur_qty']==$_POST['old_qty'])
    {
      $paramss=array(
                      'act_mode'=>'update_all_qty',
                      'rowid'=>$_POST['statsid'],
                      'promapid'=>$data->OrderproId,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>$_POST['old_stat'],
                      'orderproid'=>$data->ProId,
                      'order_id'=>$data->OrderId,
                      'order_venid'=>$data->VendorID,
                      'order_qty'=>$_POST['cur_qty'],
                      'comnt'=>'',
                      'old_qty'=>$_POST['old_qty'],
                      'price'=>$data->base_prize

                    );
      
      $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
      $response['or']= curlpost($paramss,$getting_pat);
      if(!empty($response['or']->lastid)){ $ids=$response['or']->lastid; }
    }else if($_POST['cur_qty'] < $_POST['old_qty']){
          
              $paramss=array(
                            'act_mode'=>'update_less_qty',
                            'rowid'=>$_POST['statsid'],
                            'promapid'=>$data->OrderproId,
                            'stats'=>$_POST['flag'],
                            'oldstats'=>$_POST['old_stat'],
                            'orderproid'=>$data->ProId,
                            'order_id'=>$data->OrderId,
                            'order_venid'=>$data->VendorID,
                            'order_qty'=>$_POST['cur_qty'],
                            'comnt'=>'',
                            'old_qty'=>$_POST['old_qty'],
                            'price'=>get_unit_price($data->ProId,$_POST['cur_qty'], $data->citygroupid)

                          );
             $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
             $response['or']= curlpost($paramss,$getting_pat);
             if(!empty($response['or']->lastid)){ $ids=$response['or']->lastid; }
              $paramss=array(
                            'act_mode'=>'update_remaining_qty',
                            'rowid'=>$_POST['statsid'],
                            'promapid'=>$data->OrderproId,
                            'stats'=>$_POST['flag'],
                            'oldstats'=>$_POST['old_stat'],
                            'orderproid'=>$data->ProId,
                            'order_id'=>$data->OrderId,
                            'order_venid'=>$data->VendorID,
                            'order_qty'=>$_POST['cur_qty'],
                            'comnt'=>$_POST['comment'],
                            'old_qty'=>$_POST['old_qty'],
                            'price'=>get_unit_price($data->ProId, ($_POST['old_qty'] - $_POST['cur_qty']), $data->citygroupid)

                          );
             $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
             $response['or']= curlpost($paramss,$getting_pat);


      }else{
             $paramss=array(
                              'act_mode'=>'update_increserd_qty',
                              'rowid'=>$_POST['statsid'],
                              'promapid'=>$data->OrderproId,
                              'stats'=>$_POST['flag'],
                              'oldstats'=>$_POST['old_stat'],
                              'orderproid'=>$data->ProId,
                              'order_id'=>$data->OrderId,
                              'order_venid'=>$data->VendorID,
                              'order_qty'=>$_POST['cur_qty'],
                              'comnt'=>'',
                              'old_qty'=>$_POST['old_qty'],
                              'price'=>get_unit_price($data->ProId, $_POST['cur_qty'], $data->citygroupid)

                            );
               $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
               $response['or']= curlpost($paramss,$getting_pat);
               if(!empty($response['or']->lastid)){ $ids=$response['or']->lastid; }
      }
    }
         
  }else{ //die("hiiii");bhawna
    $parameter=array(
                      'act_mode'  => 'get_ord_dtls',
                      'row_id'    => $this->input->post('statsid'),
                      'orderstatus'=>'',
                      'order_proid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'oldstatus'=>''

                    );
   
    $data=$this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    //p($data);exit();
   
    if($_POST['cur_qty']==$_POST['old_qty'])
    {
      $paramss=array(
                      'act_mode'=>'update_all_qty',
                      'rowid'=>$_POST['statsid'],
                      'promapid'=>$data->OrderproId,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>$_POST['old_stat'],
                      'orderproid'=>$data->ProId,
                      'order_id'=>$data->OrderId,
                      'order_venid'=>$data->VendorID,
                      'order_qty'=>$_POST['cur_qty'],
                      'comnt'=>'',
                      'old_qty'=>$_POST['old_qty'],
                      'price'=>$data->base_prize

                    );
      
      $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
      $response['or']= curlpost($paramss,$getting_pat);
      //p($response['or']);exit;

      if($_POST['flag']=="return" || $_POST['flag']=="return cancel"){
        $paramssa=array(
                      'act_mode'=>'update_return_comment',
                      'rowid'=>'',
                      //'promapid'=>$data->OrderproId,
                      'promapid'=>$response['or']->stlastid,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>'',
                      'orderproid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'comnt'=>$data->Remark,
                      'old_qty'=>'',
                      'price'=>''

                    );
        //p($paramssa);exit;

        $datas=$this->supper_admin->call_procedure('proc_update_orderdetils',$paramssa);
      }


    }else if($_POST['cur_qty'] < $_POST['old_qty']){
        
            $paramss=array(
                          'act_mode'=>'update_less_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>'',
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>get_unit_price($data->ProId,$_POST['cur_qty'], $data->citygroupid)

                        );
           $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           $response['or']= curlpost($paramss,$getting_pat);

            $paramss=array(
                          'act_mode'=>'update_remaining_qty',
                          'rowid'=>$_POST['statsid'],
                          'promapid'=>$data->OrderproId,
                          'stats'=>$_POST['flag'],
                          'oldstats'=>$_POST['old_stat'],
                          'orderproid'=>$data->ProId,
                          'order_id'=>$data->OrderId,
                          'order_venid'=>$data->VendorID,
                          'order_qty'=>$_POST['cur_qty'],
                          'comnt'=>$_POST['comment'],
                          'old_qty'=>$_POST['old_qty'],
                          'price'=>get_unit_price($data->ProId, ($_POST['old_qty'] - $_POST['cur_qty']), $data->citygroupid)

                        );
           $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
           $response['or']= curlpost($paramss,$getting_pat);

           if($_POST['flag']=="return" || $_POST['flag']=="return cancel"){
        $paramssa=array(
                      'act_mode'=>'update_return_comment',
                      'rowid'=>'',
                      //'promapid'=>$data->OrderproId,
                      'promapid'=>$response['or']->stlastid,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>'',
                      'orderproid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'comnt'=>$data->Remark,
                      'old_qty'=>'',
                      'price'=>''

                    );

        $datas=$this->supper_admin->call_procedure('proc_status_tbl_updt',$paramssa);
      }


    }else{
            $paramss=array(
                              'act_mode'=>'update_increserd_qty',
                              'rowid'=>$_POST['statsid'],
                              'promapid'=>$data->OrderproId,
                              'stats'=>$_POST['flag'],
                              'oldstats'=>$_POST['old_stat'],
                              'orderproid'=>$data->ProId,
                              'order_id'=>$data->OrderId,
                              'order_venid'=>$data->VendorID,
                              'order_qty'=>$_POST['cur_qty'],
                              'comnt'=>'',
                              'old_qty'=>$_POST['old_qty'],
                              'price'=>get_unit_price($data->ProId, $_POST['cur_qty'], $data->citygroupid)

                            );
               $getting_pat = api_url().'orderapi/uptodate_order_status/format/json/';
               $response['or']= curlpost($paramss,$getting_pat);

        if($_POST['flag']=="return" || $_POST['flag']=="return cancel"){
        $paramssa=array(
                      'act_mode'=>'update_return_comment',
                      'rowid'=>'',
                      //'promapid'=>$data->OrderproId,
                      'promapid'=>$response['or']->stlastid,
                      'stats'=>$_POST['flag'],
                      'oldstats'=>'',
                      'orderproid'=>'',
                      'order_id'=>'',
                      'order_venid'=>'',
                      'order_qty'=>'',
                      'comnt'=>$data->Remark,
                      'old_qty'=>'',
                      'price'=>''

                    );

        $datas=$this->supper_admin->call_procedure('proc_status_tbl_updt',$paramssa);
      }
    }
  }
   
   echo $ids; 
 }

 function insert_invoice()
 { 
    //pend($_POST);
    $parameter = array(
                          'act_mode'=>'get_orders_dtl',
                          'OrdProMap_Id'=>'',
                          'ord_id'=>$_POST['order_id'],
                          'Pro_Id'=>'',
                          'Vendor_Id'=>'',
                          'baseprize'=>'',
                          'Qty'=>'',
                          'Ship_Status'=>'',''
                        );

   $dataord=$this->supper_admin->call_procedureRow('proc_status_tbl_updt',$parameter);
  
    $parameter = array(
                          'act_mode'=>'insert_invoice',
                          'OrdProMap_Id'=>$dataord->panno,
                          'ord_id'=>$_POST['order_id'],
                          'Pro_Id'=>$dataord->cstvalue,
                          'Vendor_Id'=>$dataord->tinvalue,
                          'baseprize'=>$dataord->ShippingAmt,
                          'Qty'=>random_string('numeric',6),
                          'Ship_Status'=>get_invoice_num(),''
                        );
    
   $datas=$this->supper_admin->call_procedureRow('proc_status_tbl_updt',$parameter);

   $i=0;
   foreach ($_POST['statsid'] as $value) {

    $parameter = array(
                          'act_mode'=>'get_pro_dtls',
                          'OrdProMap_Id'=>'',
                          'ord_id'=>$value,
                          'Pro_Id'=>'',
                          'Vendor_Id'=>'',
                          'baseprize'=>'',
                          'Qty'=>'',
                          'Ship_Status'=>'',''
                        );
  //$kat[]=$parameter;
  $data=$this->supper_admin->call_procedureRow('proc_status_tbl_updt',$parameter);
  $amt=number_format(($data->base_prize*$data->ProQty),1,".","");
  $tin_tax_amt=number_format(($amt * $data->protaxvat_p / 100),1,".","");
  $cst_tax_amt=number_format(($amt * $data->protaxcst_p / 100),1,".","");
  $entry_tax_amt=number_format(($amt * $data->protaxentry_p / 100),1,".","");
    $parameter = array(
                          'act_mode'=>'insert_invoice_data',
                          'OrdProMap_Id'=>number_format(($tin_tax_amt+$cst_tax_amt+$entry_tax_amt),1,".",""),
                          'ord_id'=>$datas->lastid,
                          'Pro_Id'=>$value,
                          'Vendor_Id'=>$data->skuno,
                          'baseprize'=>$data->base_prize,
                          'Qty'=>$data->ProQty,
                          'Ship_Status'=>$data->proname,
                          'mrps'=>$_POST['mrp'][$i]
                        );
    
   $this->supper_admin->call_procedure('proc_status_tbl_updt',$parameter);
   $this->session->set_flashdata("message", "Your invoice created successfully.");
   $i++;
   }



 }  

function invoice_list()
{
  if(!empty($_POST))
  {
     $parameter = array('act_mode'=>'delete_inv','row_id'=>implode(',',$_POST['attdelete']),'');
     $responce=$this->supper_admin->call_procedure('proc_productmasterdata',$parameter);
     $this->session->set_flashdata("message", "Your invoice deleted successfully.");
     redirect(uri_string());
  }
   $parameter = array(
                          'act_mode'=>'get_order_tbl',
                          'OrdProMap_Id'=>'',
                          'ord_id'=>'',
                          'Pro_Id'=>'',
                          'Vendor_Id'=>'',
                          'baseprize'=>'',
                          'Qty'=>'',
                          'Ship_Status'=>'',''
                        );
   $responce['invoicedata']=$this->supper_admin->call_procedure('proc_status_tbl_updt',$parameter);
  $this->load->view('helper/header');
  $this->load->view('order/invoice_list',$responce);
}
public function print_invoice($id)
{ 
  $parameter = array(
                          'act_mode'=>'get_bill_dtls',
                          'OrdProMap_Id'=>'',
                          'ord_id'=>$id,
                          'Pro_Id'=>'',
                          'Vendor_Id'=>'',
                          'baseprize'=>'',
                          'Qty'=>'',
                          'Ship_Status'=>'',''
                        );
   $responce['record']=$this->supper_admin->call_procedureRow('proc_status_tbl_updt',$parameter);
   $parameter = array(
                          'act_mode'=>'get_total_dtls',
                          'OrdProMap_Id'=>'',
                          'ord_id'=>$id,
                          'Pro_Id'=>'',
                          'Vendor_Id'=>'',
                          'baseprize'=>'',
                          'Qty'=>'',
                          'Ship_Status'=>'',''
                        );
   $responce['detialrecord']=$this->supper_admin->call_procedure('proc_status_tbl_updt',$parameter);
   //p($responce);exit();
  $this->load->view('order/invoice',$responce);
}

public function offline_order()
{ 

	$parameter=array('act_mode'=>'vendor_typ','','');
	$responce['vendor_type'] = $this->supper_admin->call_procedure('proc_report',$parameter);

	$parameter= array('act_mode'=>'get_pro_list','row_id'=>'','orderstatus'=>'pending','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
	$responce['product_list'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
	$parameter = array('user_details','','','',$ordid,'','','');
	$responce['user_dtls'] = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
	$this->load->view('helper/header');
	$this->load->view('order/add_offline_ord',$responce);
}
  public function get_vendor_list()
  {
    $parameter = array('vendor_list','',$_GET['v_type'],'','','','','');
    $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
    echo '<option value="">Select Vendor</option>';   
    foreach ($responce as $value) {
      echo '<option value="'.$value->id.'">'.ucwords($value->vendor_name).'</option>';   
    }
    
  }

  public function get_order_row()
  { 
    
    $parameter = array('vendor_dtls',$_POST['vendoid'],'','','','','','');
    $responce = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    
    $parameter=array('act_mode' =>'citygroupid','row_id' =>$responce->city,'vusername' =>$responce->stateid,'','');
    $data=$this->supper_admin->call_procedureRow('proc_memberlogin',$parameter);

     if(empty($_POST['colr']))
    {
       $parameter = array('product_details',$_POST['productid'],'','','','','','');
       $pro_data = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    }else{
      $parameter = array('product_dtls',explode(',', $_POST['colr'])[1],'','','','','','');
      $pro_data = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    }
    
    $unit_pz=number_format(get_unit_price($pro_data->promapid,$_POST['qty'],$data->cmid),1,".","");
    
    $parameter = array('tax_details',$responce->stateid,'',$pro_data->catid,'','','','');
    $tax_data = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
    //pend($tax_data);
    foreach ($tax_data as $value) {
     if($value->taxidd==$responce->tinid)
     {
       $tin=$value->taxprice;
     }elseif ($value->taxidd==$responce->cstid) {
        $cst=$value->taxprice;
     }elseif ($value->taxidd==5) {
        $entry=$value->taxprice;
     }
    }
    if(empty($_POST['colr']))
    {
     $style="background-color:#";
     $clr="";
    }else{
      $clr=explode(',', $_POST['colr'])[0];
     if(explode(',', $_POST['colr'])[0]=='multicolor')
    { 
      $urlmulti=base_url().'images/imgpsh_fullsize.jpg';
      $style='background-image:url('.$urlmulti.')';

    }else{
      $style="background-color:#".explode(',', $_POST['colr'])[0];
    }

    }
    $tin_t=number_format((($unit_pz*$_POST['qty'])*$tin/100),1,".","");
    $cst_t=number_format((($unit_pz*$_POST['qty'])*$cst/100),1,".","");
    $enrty_t=number_format((($unit_pz*$_POST['qty'])*$entry/100),1,".","");
    $subtotal=number_format(($unit_pz*$_POST['qty']),1,".","");
    $shipping_charge=number_format(tshippingcharge($unit_pz*$_POST['qty']+$_POST['amount']),1,".","");
    $final_total=number_format(($unit_pz*$_POST['qty']),1,".","");
    if(!empty($_POST)){ 
      
      $str='<tr>';
      $str.='<td class="sno">'.($_POST['ttl_row']+1).'</td>';
      $str.='<td class="sku">'.$pro_data->skuno.'</td>';
      $str.='<td><img class="img" src="'.$_POST['pro_img'].'" style="vertical-align:middle; width:80px;"></td>';
      $str.='<td class="pro_name" pid="'.$pro_data->promapid.'" catid="'.$pro_data->catid.'">'.$_POST['proname'].'</td>';
      $str.='<td class="qty">'.$_POST['qty'].'</td>';
      $str.='<td class="age">'.$_POST['age'].'</td>';
      $str.='<td>';

      $str.='<font class="colr" clr="'.$clr.'" style="'.$style.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><br></td>';
      $str.='<td class="tin" tx="'.$tin.'">'.number_format((($unit_pz*$_POST['qty'])*$tin/100),1,".","").'</td>';
      $str.='<td class="cst" tx="'.$cst.'">'.number_format((($unit_pz*$_POST['qty'])*$cst/100),1,".","").'</td>';
      $str.='<td class="entry" tx="'.$entry.'">'.number_format((($unit_pz*$_POST['qty'])*$entry/100),1,".","").'</td>';
      $str.='<td class="unit">'.$unit_pz.'</td>';
      $str.='<td class="total">'.number_format(($unit_pz*$_POST['qty']),1,".","").'</td>';
      $str.="<td onclick='delete_row($(this))' style='cursor: pointer;color: blue'>Delete</td>";
      $str.='</tr>';
    }
    echo $str.'**'.$tin_t.'**'.$cst_t.'**'.$enrty_t.'**'.$subtotal.'**'.$shipping_charge.'**'.$final_total;
    
  }


function get_vend_address_details($id)
{
    $parameter = array('vendor_address',$id,'','','','','','');
    $responce = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
    //pend($responce);
    if(!empty($responce))
    { 
      ?>
       <div class="col-lg-12">
        <div class="ad_address">
      <?php
      foreach ($responce as $value) {
      ?>
         
      <div class="col-lg-3 col-md-3">
        <div class="edit_addr_bg">
          <div class="edit_addr">
            <div style="cursor: pointer;">
              <h3><?php echo ucwords($value->shipname); ?></h3>
              <div class="phone"><i class="fa fa-phone"></i><?php echo $value->shipcontact; ?></div>
              <div class="address"><i class="fa fa-home"></i><?php echo $value->shipaddress; ?></div>
              <div class="deli_h"><input type="checkbox" name="del_addr[]" value="<?php echo $value->shippingid; ?>" onclick="assign_val(this)"> Deliver Here</div>
            </div> 
            <div class="edt_button">
            <a class="edit" href="javascript:void(0);" onclick="return editshipdetail(<?php echo $value->shippingid; ?>,<?php echo $id; ?>)">Edit</a>
             <a class="remove" href="javascript:void(0);" onclick="delete_shipp_addr(<?php echo $value->shippingid; ?>,<?php echo $id; ?>)">Remove</a>
           </div>
         </div>
       </div>
     </div>
             
           
      <?php
    }
    ?>
      <div class="col-lg-3 col-md-3">
      <a href="javascript:void(0);" class="ad_new_add" onclick="add_back_address(<?php echo $id; ?>)">Add New Address</a>
      </div>
      </div>
      </div>
    <?php
    }else{
      ?>
      <div class="col-lg-12">
        <div class="ad_address">
          <div class="col-lg-3 col-md-3">
           <a href="javascript:void(0);" class="ad_new_add" onclick="add_back_address(<?php echo $id; ?>)">Add New Address</a>
          </div>
        </div>
      </div>
      <?php
    }
}

function get_ven_account_details($id)
{
    $parameter = array('vendor_dtls',$id,'','','','','','');
    $responce = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);
    //pend($responce);
    if(!empty($responce))
    {
      ?>
      <div class="col-lg-12">


       <div class="shipping_detail">
        <div id="shiperror" style="display: none;color: #FF001F;margin: 0 auto;width: 180px;margin-bottom: 10px;">All Fields Are compulsory</div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="shipping_d">
            <h3>SHIPPING DETAILS</h3>
            <div class="fild_box">
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="row">
                  <div class="text">Name <span class="red">*</span></div></div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7">
                <div class="text_fil"><input id="shipname" name="shipname" type="text" value="<?php echo ucfirst($responce->firstname).' '.ucfirst($responce->lastname); ?>" readonly=""></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Email Address <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-5 col-xs-12">
                  <div class="text_fil"><input id="shipemail" name="shipemail" type="text" value="<?php echo $responce->compemailid ;?>" readonly=""></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Mobile No. <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="shipcontact" maxlength="10" name="shipcontact" type="text" value="<?php echo $responce->contactnumber ;?>" readonly=""></div>
                </div>
              </div>

              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Address <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><textarea id="shipadd" name="shipadd" rows="2" cols="20"><?php echo $responce->vendoraddress ;?></textarea></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Pincode <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="shippin" name="shippin" onkeypress="return isNumberKey(event);" type="text" maxlength="6" value="<?php echo $responce->venpincode ;?>"></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">State <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil">

                    <input id="shipstate" name="shipstate" type="hidden" readonly="" value="<?php echo $responce->stateid ;?>">
                    <input id="shipstates" name="shipstates" type="text" readonly="" value="<?php echo $responce->statename ;?>">



                  </div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">City <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil">
                  <input id="shipcity" name="shipcity" type="text" value="<?php echo $responce->cityname ;?>" readonly>
                  <input id="shipcitys" name="shipcitys" type="hidden" value="<?php echo $responce->city;?>">

                  </div>
                </div>
              </div>


            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="billing_d">
              <h3>BILLING DETAILS</h3>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Name <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="billname" name="billname" type="text"></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Email Address <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="billemail" name="billemail" type="text" value="<?php echo $responce->compemailid ;?>" readonly=""></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Mobile No. <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="billcontact" name="billcontact" onkeypress="return isNumberKey(event);" type="text" maxlength="10"></div>
                </div>
              </div>

              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Address <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><textarea id="billadd" name="billadd" rows="2" cols="20"></textarea></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Pincode <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="billpin" name="billpin" onkeypress="return isNumberKey(event);" type="text" maxlength="6"></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">State <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil">

                    <input id="billstate" name="billstate" type="hidden" readonly="" value="<?php echo $responce->stateid ;?>">
                    <input id="billstates" name="billstates" type="text" readonly="" value="<?php echo $responce->statename ;?>">


                  </div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">City <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil">
                  <input id="billcity" name="billcity" type="text" value="<?php echo $responce->cityname ;?>" readonly>
                  <input id="billcitys" name="billcitys" type="hidden" value="<?php echo $responce->city ;?>">
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="text_1"><input onclick="return auto_fill();" type="checkbox">My billing address same as my shipping address
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="check_btn">
            <input type="button" onclick="shipdetail_fill(<?=$id?>);" value="Save Address" class="btn continu">
            <a href="javascript:void(0);" class="btn back_addr" onclick="open_address(<?=$id?>);">Back to Address</a>
          </div>
        </div>
      </div>
    </div>
      <?php
    }
}
function save_vender_addr($id)
{ 
    $parameter = array('vendor_dtls',$id,'','','','','','');
    $responce = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);

  $parameter=array(
                    'act_mode'        =>"insertshipdata",
                    'row_id'          =>'',
                    'user_regid'      =>$id,
                    'ship_name'       =>$this->input->post('shipname'),
                    'bill_name'       =>$this->input->post('billname'),
                    'ship_address'    =>$this->input->post('shipadd'),
                    'bill_address'    =>$this->input->post('billadd'),
                    'ship_email'      =>$this->input->post('shipemail'),
                    'bill_email'      =>$this->input->post('billemail'),
                    'ship_pincode'    =>$this->input->post('shippin'),
                    'bill_pincode'    =>$this->input->post('billpin'),
                    'ship_contact'    =>$this->input->post('shipcontact'),
                    'bill_contact'    =>$this->input->post('billcontact'),
                    'ship_state'      =>$this->input->post('shipstate'),
                    'bill_state'      =>$this->input->post('billstate'),
                    'ship_city'       =>$this->input->post('shipcity'),
                    'bill_city'       =>$this->input->post('billcity'),
                    'ship_cst'        =>$responce->cstid,
                    'bill_cst'        =>$responce->cstid,
                    'ship_tin'        =>$responce->tinid,
                    'bill_tin'        =>$responce->tinid,
                    'ship_createdon'  =>''
                    );
                 
    $data=$this->supper_admin->call_procedureRow('proc_usershipping',$parameter);

}
function delshipdetail($aid)
{
  $parameter=array(
                    'act_mode' =>'deluser_shipdetail',
                    'row_id' =>$aid,
                    'pvalue' =>''
                    );
  $data=$this->supper_admin->call_procedure('proc_productmasterdata',$parameter);
}

function get_vendor_ship_details($aid,$id)
{
  $parameter=array(
                    'act_mode' =>'get_vendor_shipp_addrs',
                    'row_id' =>$aid,
                    'pvalue' =>''
                    );
  $responce=$this->supper_admin->call_procedureRow('proc_productmasterdata',$parameter);
  if(!empty($responce))
    {
      ?>
      <div class="col-lg-12">


       <div class="shipping_detail">
        <div id="shiperror" style="display: none;color: #FF001F;margin: 0 auto;width: 180px;margin-bottom: 10px;">All Fields Are compulsory</div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="shipping_d">
            <h3>SHIPPING DETAILS</h3>
            <div class="fild_box">
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="row">
                  <div class="text">Name <span class="red">*</span></div></div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7">
                <div class="text_fil"><input id="shipname" name="shipname" type="text" value="<?php echo ucwords($responce->shipname); ?>" readonly=""></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Email Address <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-5 col-xs-12">
                  <div class="text_fil"><input id="shipemail" name="shipemail" type="text" value="<?php echo $responce->shipemailid ;?>" readonly=""></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Mobile No. <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="shipcontact" maxlength="10" name="shipcontact" type="text" value="<?php echo $responce->shipcontact ;?>" readonly=""></div>
                </div>
              </div>

              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Address <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><textarea id="shipadd" name="shipadd" rows="2" cols="20"><?php echo $responce->shipaddress ;?></textarea></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Pincode <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="shippin" name="shippin" onkeypress="return isNumberKey(event);" type="text" maxlength="6" value="<?php echo $responce->shippincode ;?>"></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">State <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil">

                    <input id="shipstate" name="shipstate" type="hidden" readonly="" value="<?php echo $responce->shipstate ;?>">
                    <input id="shipstates" name="shipstates" type="text" readonly="" value="<?php echo $responce->statename ;?>">



                  </div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">City <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil">
                  <input id="shipcity" name="shipcity" type="text" value="<?php echo $responce->shipcity ;?>" readonly="">
                  <input id="shipcitys" name="shipcitys" type="hidden" value="<?php echo $responce->shipcity;?>">

                  </div>
                </div>
              </div>


            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="billing_d">
              <h3>BILLING DETAILS</h3>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Name <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="billname" name="billname" type="text" value="<?php echo ucwords($responce->billname) ;?>"></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Email Address <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="billemail" name="billemail" type="text" value="<?php echo $responce->billemailid ;?>" readonly=""></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Mobile No. <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="billcontact" name="billcontact" onkeypress="return isNumberKey(event);" type="text" maxlength="10" value="<?php echo $responce->billcontactno ;?>"></div>
                </div>
              </div>

              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Address <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><textarea id="billadd" name="billadd" rows="2" cols="20"><?php echo $responce->billaddress ;?></textarea></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">Pincode <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil"><input id="billpin" name="billpin" onkeypress="return isNumberKey(event);" type="text" maxlength="6" value="<?php echo $responce->billpincode ;?>"></div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">State <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil">

                    <input id="billstate" name="billstate" type="hidden" readonly="" value="<?php echo $responce->billstate ;?>">
                    <input id="billstates" name="billstates" type="text" readonly="" value="<?php echo $responce->statename ;?>">


                  </div>
                </div>
              </div>
              <div class="fild_box">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="text">City <span class="red">*</span></div>
                  </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                  <div class="text_fil">
                  <input id="billcity" name="billcity" type="text" value="<?php echo $responce->billcity ;?>" readonly>
                  <input id="billcitys" name="billcitys" type="hidden" value="<?php echo $responce->billcity ;?>"></div>
                </div>
              </div>

            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="text_1"><input onclick="return auto_fill();" type="checkbox">My billing address same as my shipping address
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="check_btn">
            <input type="button" onclick="shipdetail_update(<?=$aid?>,<?=$id?>);" value="Update Address" class="btn continu">
            <a href="javascript:void(0);" class="btn back_addr" onclick="open_address(<?=$id?>);">Back to Address</a>
          </div>
        </div>
      </div>
    </div>
      <?php
    }
}
function update_vender_addr($aid)
{
  $parameter=array(
                    'act_mode'        =>"update_vend_ship_add",
                    'row_id'          =>$aid,
                    'user_regid'      =>'',
                    'ship_name'       =>'',
                    'bill_name'       =>$this->input->post('billname'),
                    'ship_address'    =>$this->input->post('shipadd'),
                    'bill_address'    =>$this->input->post('billadd'),
                    'ship_email'      =>'',
                    'bill_email'      =>'',
                    'ship_pincode'    =>$this->input->post('shippin'),
                    'bill_pincode'    =>$this->input->post('billpin'),
                    'ship_contact'    =>'',
                    'bill_contact'    =>$this->input->post('billcontact'),
                    'ship_state'      =>'',
                    'bill_state'      =>'',
                    'ship_city'       =>'',
                    'bill_city'       =>'',
                    'ship_cst'        =>'',
                    'bill_cst'        =>'',
                    'ship_tin'        =>'',
                    'bill_tin'        =>'',
                    'ship_createdon'  =>''
                    );
    //pend($parameter);           
    $data=$this->supper_admin->call_procedureRow('proc_usershipping',$parameter);
}
public function add_order_by_admin()
{ 
    //pend($_POST);
    $parameter = array('vendor_dtls',$_POST['vendoid'],'','','','','','');
    $responce = $this->supper_admin->call_procedureRow('proc_orderflow',$parameter);

    $parameter=array('act_mode' =>'citygroupid','row_id' =>$responce->city,'vusername' =>$responce->stateid,'','');
    $data=$this->supper_admin->call_procedureRow('proc_memberlogin',$parameter);
    $grandtotalcoddiscount='';
    if($_POST['paymentmode']=='COD')
    {
      $grandtotalcoddiscount = ceil(($_POST['subtotal']*0)/100);
    }
   
   $param = array(
                        'UserId'           =>$_POST['vendoid'],
                        'userregid'        =>$_POST['vendoid'],
                        'UserAddressId'    =>$_POST['ship_id'], 
                        'orderNumber'      =>'',
                        'totalAmount'      =>$_POST['final_total'],
                        'paymentMod'       =>$_POST['paymentmode'],
                        'shipcharge'       =>$_POST['shipping_charge'],
                        'couponamt'        =>'',
                        'couponcode'       =>'',
                        'userIpaddress'    =>'', 
                        'p_device'         =>$data->cmid,
                        'usedwalletamount' =>'',
                        'p_usedrewardpoint'=>$grandtotalcoddiscount,
                        'p_subtotal'       =>$_POST['subtotal'],
                        'p_orderfrom'      =>'offline',
                        'totaltax'         =>$_POST['total_tax'],
                        'csttax'           =>$_POST['tcst_tax'],
                        'tintax'           =>$_POST['ttin_tax'],
                        'entrytax'         =>$_POST['tentry_tax'],
                        'u_remark'         =>$_POST['remark'] 
                );
     
     $order_rep = $this->supper_admin->call_procedureRow('proc_offlineordr',$param);
    if($_POST['paymentmode']=='CHEQUE' && !empty($order_rep))
    {
     $param = array(
                        'act_mode'          => 'insertcheq',
                        'row_id'            => '',
                        'orderid'           => $order_rep->OrderId,
                        'chequeno'          => $this->input->post('cheque_no'),
                        'ifsc'              => $this->input->post('ifsc_code'),
                        'bankname'          => $this->input->post('bank_name'),
                        'chequedate'        => $this->input->post('cheque_date'),
                        'accno'             => $this->input->post('acc_no'),
                        'chequecollectdate' => $this->input->post('chequecollect_date'),
                        'chequeamount'      => $this->input->post('cheque_amount'),
                        'paymentMod'        => $this->input->post('paymentmode'),
                        'orderno'           => $order_rep->OrderNumber 
                        );
     $data = $this->supper_admin->call_procedureRow('proc_cheque',$param);
    }
        
    if(!empty($order_rep))
    { 
      
      for($i=0;$i<count($_POST['pro_qty']);$i++){
       $parameter = array(
                            'orderId'        =>$order_rep->OrderId,
                            'proid'          =>$_POST['promap_id'][$i],
                            'qty'            =>$_POST['pro_qty'][$i],
                            'shippingcharge' =>0,
                            'proname'        =>$_POST['proname'][$i],
                            'vendorId'       =>get_product_details($_POST['promap_id'][$i])->manufactureid,
                            'prodSize'       =>$_POST['pro_age'][$i],
                            'prodPrice'      =>$_POST['sale_pz'][$i],
                            'coupon_value'   =>'',
                            'p_color'        =>$_POST['pro_color'][$i],
                            'p_img'          =>$_POST['image'][$i],
                            'cstvalue'       =>$_POST['cst_tax'][$i],
                            'tinvalue'       =>$_POST['tin_tax'][$i],
                            'entryvalue'     =>$_POST['entry_tax'][$i],
                            'cst_p'         =>$_POST['cst_taxp'][$i],
                            'tin_p'         =>$_POST['tin_taxp'][$i],
                            'entry_p'       =>$_POST['entry_taxp'][$i]
                          );
       $result = $this->supper_admin->call_procedureRow('proc_orderpromap', $parameter);              
      }
      
      
      
    }
    if($result->success=='success')
    {  
       $parameter=array('act_mode'=>'maildetaila','row_id'=>$order_rep->OrderId,'vusername'=>'','vpassword'=>'','vstatus'=>'');
       $userinfo =$this->supper_admin->call_procedure('proc_memberlogin', $parameter);
       foreach ($userinfo as $value) {
         $val=(array)$value;
         $mail_arr['mailview'][]=$val;
       }

        //-------------start SMS API--------------------
          $smsphnon = $order_rep->ContactNo; 
          $smsname = $order_rep->ShippingName;
          
          $smsmsg = "has been placed with us. You will receive an email with your order details shortly. Call Us at +91 9711007307, if you need help.";
          
          $smsmsgg = urlencode('Hi '.$smsname.' your order '.$order_rep->OrderNumber.' '.$smsmsg);
          
          $file= file_get_contents('http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to='.$smsphnon.'&msg='.$smsmsgg.'&msg_type=TEXT&userid=2000157303&auth_scheme=plain&password=qYhWPsXxi&v=1.1&format=text');

      //-------------end SMS API--------------------

      //------------email--------------
          $bizzgainemail="support@mindzshop.com";
          $emailid=$order_rep->Email;
          $employe_email=$this->session->userdata['bizzadmin']->UserName;
          //$operation_mageger_email='bishwajeet410@gmail.com';
          $listbizzmail=array($employe_email,);
          $msg = $this->load->view('webapp/bizzgainmailer/ordermail',$mail_arr, true);
          $this->load->library('email');
          $this->email->from($bizzgainemail, 'MindzShop.com');
          $this->email->to($emailid);
          $this->email->cc($listbizzmail); 
          $this->email->subject('Your order has been placed - ('.$order_rep->OrderNumber.')');
          $this->email->message($msg);
          $this->email->send();
      //------------email--------------
      $this->session->set_flashdata("success", "Your offline Order Added successfully.");
    }else{
      $this->session->set_flashdata("error", "Something Went Wrong.");
    } 
    
}
public function offline_order_list()
{

  $parameter        = array('act_mode'=>'offline_view','row_id'=>'','orderstatus'=>'offline','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['details'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
       //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/offline_order_list?";
     $config['total_rows']       = count($responce['details']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter = array('act_mode'=>'offline_view','row_id'=>$page,'orderstatus'=>'offline','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
     $responce['details'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/offline_view',$responce);
}
 public function offline_order_details($ordid){
 $parameter = array('act_mode'=>'offline_details','row_id'=>'','orderstatus'=>'return','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['details'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
 //pend($responce); 
 $this->load->view('helper/header');
 $this->load->view('order/offline_order_details',$responce);

}

function get_shipping_charge($amt)
{
   echo tshippingcharge($amt);
}

public function update_order_paymode(){
  $modetype=$this->input->post('modetype');
  $ordid=$this->input->post('ordid');
  $parameter=array('act_mode'=>'changefullpaymode','row_id'=>$ordid,'ppy_orderid'=>'','ppy_userid'=>'','ppy_paymentmode'=>$modetype,'ppy_amt'=>'','ppy_status'=>'');
  $record = $this->supper_admin->call_procedure('proc_orderpartialpayment',$parameter);
  $this->session->set_flashdata("message", "Payment Mode was successfully Changed.");
}

public function delmultipro()
{
    $statusordid=$this->input->post('statusordid');
    $productid=$this->input->post('productid');
    $quatity=$this->input->post('quatity');
    for($i=0;$i<count($statusordid);$i++) {
      $parameter = array(
                      'act_mode'=>'delete_row',
                      'OrdProMap_Id'=>'',
                      'ord_id'=>$statusordid[$i],
                      'Pro_Id'=>$productid[$i],
                      'Vendor_Id'=>'',
                      'baseprize'=>'',
                      'Qty'=>$quatity[$i],
                      'Ship_Status'=>'',''
                        );
      $this->supper_admin->call_procedure('proc_status_tbl_updt',$parameter);
    }

// Start push notification for app 
      /*$addprocount=count($statusordid);
      $parameterapp = array('act_mode'=>'getappdelorderdeviceid','row_id'=>$statusordid,'catid'=>'');
      $appdeviceids  = $this->supper_admin->call_procedureRow('proc_product',$parameterapp);
      $appdeviceiddata = array($appdeviceids->appdeviceid);
      $appmsgdevice=array('message'=>$addprocount.' Products are Remove in your existing order. OrderId is '.$appdeviceids->OrderNumber,'title'=>'Remove Products');
      $appnotification='2';
      send_andriod_notification($appdeviceiddata,$appmsgdevice,$appnotification);*/
// End push notification for app

}

public function addwalletorderamt(){

     $parameterwall=array('act_mode'=>'checkaddwalletmoney',
                           'row_id'=>'',
                           'wa_orderid'=>$this->input->post('wallordid'),
                           'wa_userid'=>$this->input->post('walluserid'),
                           'wa_type'=>'credit',
                           'wa_amt'=>$this->input->post('wallordamt'),
                           'wa_status'=>$this->input->post('ordercase')
                           );
    $checkwalletdata = $this->supper_admin->call_procedureRow('proc_wallet',$parameterwall);

    if($checkwalletdata->countid==0){

      $parameterwalldel=array('act_mode'=>'delprevwalletmoney',
                           'row_id'=>'',
                           'wa_orderid'=>$this->input->post('wallordid'),
                           'wa_userid'=>$this->input->post('walluserid'),
                           'wa_type'=>'credit',
                           'wa_amt'=>'',
                           'wa_status'=>$this->input->post('ordercase')
                           );
    $delwalletdata = $this->supper_admin->call_procedureRow('proc_wallet',$parameterwalldel);

      $parameterwallet=array('act_mode'=>'addwalletmoney',
                             'row_id'=>'',
                             'wa_orderid'=>$this->input->post('wallordid'),
                             'wa_userid'=>$this->input->post('walluserid'),
                             'wa_type'=>'credit',
                             'wa_amt'=>$this->input->post('wallordamt'),
                             'wa_status'=>$this->input->post('ordercase')
                             );
      
      $walletdata = $this->supper_admin->call_procedureRow('proc_wallet',$parameterwallet);
      $this->session->set_flashdata("message", "Payment add in wallet successfully.");
      echo "success";
    } else {
      $this->session->set_flashdata("message", "Payment already add in wallet.");
      echo "fail";
    }

}

public function returnorderprocess(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'returnviewprocess','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
   //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/returnorderprocess?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'returnviewprocess','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  #$responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  /*foreach ($responce['penview'] as $value) {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt_returnord($value->OrderId);
            }  */

     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/returnviewprocess',$responce);
 } 

 public function ordreturndetailprocess($ordid){
 $parameter        = array('act_mode'=>'returndetailprocess','row_id'=>'','orderstatus'=>'return processing','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>$_GET['ordcase'].'%');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/returndetailsprocess',$responce);

}

public function returnordercancel(){
  $this->userfunction->loginAdminvalidation();
  $parameter        = array('act_mode'=>'returnviewcancel','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  $responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);
   //----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/order/returnordercancel?";
     $config['total_rows']       = count($responce['penview']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );
    $parameter        = array('act_mode'=>'returnviewcancel','row_id'=>$page,'orderstatus'=>'','order_proid'=>$second,'order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
  #$responce['penview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  /*foreach ($responce['penview'] as $value) {
                  $responce['current_amt'][]=$this->userfunction->get_current_total_amt_returnord($value->OrderId);
            }  */

     //----------------  end pagination setting ------------------------// 
  $this->load->view('helper/header');
  $this->load->view('order/returnviewcancel',$responce);
 } 

 public function ordreturndetailcancel($ordid){
 $parameter        = array('act_mode'=>'returndetailcancel','row_id'=>'','orderstatus'=>'return cancel','order_proid'=>'','order_id'=>$ordid,'order_venid'=>'','order_qty'=>'','oldstatus'=>'');
 $responce['pendetailview'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter); 
 $this->load->view('helper/header');
 $this->load->view('order/returndetailscancel',$responce);

}

}// class
?>