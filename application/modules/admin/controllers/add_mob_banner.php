<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Add_mob_banner extends MX_Controller{

//............. Default Construct function ............... //
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('upload');
    $this->userfunction->loginAdminvalidation();
    
  
  }// end function.

//............. Slider Manager ............... //
  public function addbannerimg(){
//p('hello ');exit();
    $parameters = array(
                    'act_mode'  => 'viewsmobbanner',
                    'row_id'    => '',
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );

    $record['viewslide'] = $this->supper_admin->call_procedure('proc_homebanner',$parameters);
//p($record['viewslide']);exit();
    if($this->input->post('submit')){

     $imgurl            = $this->input->post('imgurl');
     $imgslide          = preg_replace('/\s+/', '', $_FILES['imgslide']['name']);
          
     $field_name_img    = 'imgslide';
     $img_file          = $this->image_uploads($field_name_img);

     $allowedExts       = array("gif","jpeg","jpg","png");
     $temp              = explode(".",$imgslide);
     $extension         = end($temp);
     //echo $extension;exit();
     if (in_array($extension, $allowedExts)){

        $parameter      = array(
                            'act_mode'  => 'insertmob_banner',
                            'row_id'    => '',
                            'imagename' => time().$imgslide,
                            'imageurl'  => '',
                            'imgtype'   => 'mainslider',
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status'=> ''
                          );

        $record = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
        $this->session->set_flashdata('messag', 'Your information was successfully Saved.');
        redirect('admin/add_mob_banner/addbannerimg');
     }
     else{        
        $this->session->set_flashdata('messag', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
        redirect('admin/add_mob_banner/addbannerimg');
     }
    }

    $this->load->view('helper/header');
    $this->load->view('mobilebanner/add_mob_banner', $record);
  
    }
public function updatesmobbanner($id) {
   //p(FCPATH);exit();

    $parameters     = array(
                        'act_mode'   => 'viewsmob_banner',
                        'row_id'     => $id,
                        'imagename'  => '',
                        'imageurl'   => '',
                        'imgtype'    => '',
                        'img_head'   => '',
                        'img_shead'  => '',
                        'img_status' => ''
                        );
    $record['view'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameters);

    if($this->input->post('submit')){

     $imgurl            = $this->input->post('imgurl');
     $imgslide          = preg_replace('/\s+/', '', $_FILES['imgslide']['name']);
          
     $field_name_img    = 'imgslide';
     $img_file          = $this->image_uploads($field_name_img);

     $allowedExts       = array("gif","jpeg","jpg","png");
     $temp              = explode(".",$imgslide);
     $extension         = end($temp);
     //echo $extension;exit();

     if(!empty($imgslide)){
         if (in_array($extension, $allowedExts)){

            unlink(FCPATH."assets/mobilebanners/".$record['view']->b_img);
            $parameter      = array(
                                'act_mode'  =>'update_mob_banner',
                                'row_id'    =>$id,
                                'imagename'  =>time().$imgslide,
                                'imageurl' =>'',
                                'imgtype'   =>'',
                                'img_head'   =>'',
                                'img_shead'   =>'',
                                'img_status'   =>''
                                );
            $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/add_mob_banner/addbannerimg');
            
         }
         else{            
            $this->session->set_flashdata('message2', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            //redirect('admin/bannergallery/addslider');
         }
        } 
        else{
            $parameter      = array(
                                'act_mode'  => 'updatemobilebanner',
                                'row_id'    => $id,
                                'imagename' => $record['view']->b_img,
                                'imageurl'  => '',
                                'imgtype'   => '',
                                'img_head'  => '',
                                'img_shead' => '',
                                'img_status'=> ''
                              );

            $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/add_mob_banner/addbannerimg');
        }     
    }

    $this->load->view('helper/header');
    $this->load->view('mobilebanner/update_mobbanner',$record);
  }
//............. Slider Status ............... //
  public function statusmobileslider(){
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='A'?'banactives':'baninactives';
    
    $parameter     = array(
                    'act_mode'  => $act_mode,
                    'row_id'    => $rowid,
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );
    //p($parameter);exit();
    $record        = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
    redirect('admin/add_mob_banner/addbannerimg');
  }  

//............. End Slider Manager ............... //


//............. Delete Slider ............... //
  public function deletemobileslider($id) {


    $parameter      = array(
                    'act_mode'   => 'delmobslider',
                    'row_id'     => $id,
                    'imagename'  => '',
                    'imageurl'   => '',
                    'imgtype'    => '',
                    'img_head'   => '',
                    'img_shead'  => '',
                    'img_status' => ''
                    );

    $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect('admin/add_mob_banner/addbannerimg');
  }

//............. Slider Status ............... //

    // end function.
//............. Update Slider ............... //
  public function image_uploads($field_name){

    $config['upload_path']   = './assets/mobilebanners/';
    $config['allowed_types'] = 'jpg|jpeg|gif|png';
    $config['max_size']      = '10000000';
    $config['file_name']     = time().preg_replace('/\s+/', '', $_FILES[$field_name]['name']);
    
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload($field_name)){
        $data['error']  = array('error' => $this->upload->display_errors());
    } else {
        $data['name']   = array('upload_data' => $this->upload->data());
    }
    return $data;
  }

//............. Delete Slider ............... //


// promotional banner......//
  
    public function addpromotionalbanner(){
//p('hello ');exit();
    $parameters = array(
                    'act_mode'  => 'viewspromotionalbanner',
                    'row_id'    => '',
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );

    $record['viewslide'] = $this->supper_admin->call_procedure('proc_homebanner',$parameters);
//p($record['viewslide']);exit();
    if($this->input->post('submit')){

     $imgurl            = $this->input->post('imgurl');
     $imgslide          = preg_replace('/\s+/', '', $_FILES['imgslide']['name']);
          
     $field_name_img    = 'imgslide';
     $img_file          = $this->image_uploads($field_name_img);

     $allowedExts       = array("gif","jpeg","jpg","png");
     $temp              = explode(".",$imgslide);
     $extension         = end($temp);
     //echo $extension;exit();
     if (in_array($extension, $allowedExts)){

        $parameter      = array(
                            'act_mode'  => 'insertpromotional_banner',
                            'row_id'    => '',
                            'imagename' => time().$imgslide,
                            'imageurl'  => '',
                            'imgtype'   => 'mainslider',
                            'img_head'  => '',
                            'img_shead' => '',
                            'img_status'=> ''
                          );

        $record = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
        $this->session->set_flashdata('messag', 'Your information was successfully Saved.');
        redirect('admin/add_mob_banner/addpromotionalbanner');
     }
     else{        
        $this->session->set_flashdata('messag', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
        redirect('admin/add_mob_banner/addpromotionalbanner');
     }
    }

    $this->load->view('helper/header');
    $this->load->view('mobilebanner/add_promotional_banner', $record);
  
    }
 
// end promotional banner..//  
// update promotional banner..//

 public function updatespromotionalbanner($id) {
   //p(FCPATH);exit();

    $parameters     = array(
                        'act_mode'   => 'viewspromotional_banner',
                        'row_id'     => $id,
                        'imagename'  => '',
                        'imageurl'   => '',
                        'imgtype'    => '',
                        'img_head'   => '',
                        'img_shead'  => '',
                        'img_status' => ''
                        );
    $record['view'] = $this->supper_admin->call_procedureRow('proc_homebanner',$parameters);

    if($this->input->post('submit')){

     $imgurl            = $this->input->post('imgurl');
     $imgslide          = preg_replace('/\s+/', '', $_FILES['imgslide']['name']);
          
     $field_name_img    = 'imgslide';
     $img_file          = $this->image_uploads($field_name_img);

     $allowedExts       = array("gif","jpeg","jpg","png");
     $temp              = explode(".",$imgslide);
     $extension         = end($temp);
     //echo $extension;exit();

     if(!empty($imgslide)){
         if (in_array($extension, $allowedExts)){

            unlink(FCPATH."assets/mobilebanners/".$record['view']->b_img);
            $parameter      = array(
                                'act_mode'  =>'update_promotional_banner',
                                'row_id'    =>$id,
                                'imagename'  =>time().$imgslide,
                                'imageurl' =>'',
                                'imgtype'   =>'',
                                'img_head'   =>'',
                                'img_shead'   =>'',
                                'img_status'   =>''
                                );
            $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/add_mob_banner/addpromotionalbanner');
            
         }
         else{            
            $this->session->set_flashdata('message2', 'Please Upload Image in GIF, JPEG, JPG and PNG Format only');
            //redirect('admin/bannergallery/addslider');
         }
        } 
        else{
            $parameter      = array(
                                'act_mode'  => 'updatepromotionalbanner',
                                'row_id'    => $id,
                                'imagename' => $record['view']->b_img,
                                'imageurl'  => '',
                                'imgtype'   => '',
                                'img_head'  => '',
                                'img_shead' => '',
                                'img_status'=> ''
                              );

            $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
            $this->session->set_flashdata('message', 'Your information was successfully Updated.');
            redirect('admin/add_mob_banner/addpromotionalbanner');
        }     
    }

    $this->load->view('helper/header');
    $this->load->view('mobilebanner/update_promotionalbanner',$record);
  }

 // end update promotional banner //
  
// delete promotional banner//
 
  public function deletepromotionalbanner($id) {


    $parameter      = array(
                    'act_mode'   => 'delpromotionalbanner',
                    'row_id'     => $id,
                    'imagename'  => '',
                    'imageurl'   => '',
                    'imgtype'    => '',
                    'img_head'   => '',
                    'img_shead'  => '',
                    'img_status' => ''
                    );

    $record         = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect('admin/add_mob_banner/addpromotionalbanner');
  }


// end delete promotional banner  //

// status promotional banner //
 
  public function statuspromotionalbanner(){
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='A'?'banactivespromo':'baninactivespromo';
    
    $parameter     = array(
                    'act_mode'  => $act_mode,
                    'row_id'    => $rowid,
                    'imagename' => '',
                    'imageurl'  => '',
                    'imgtype'   => '',
                    'img_head'  => '',
                    'img_shead' => '',
                    'img_status'=> ''
                    );
    //p($parameter);exit();
    $record        = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
    $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
    redirect('admin/add_mob_banner/addpromotionalbanner');
  } 

 // end status promtoional banner // 
  }
?>