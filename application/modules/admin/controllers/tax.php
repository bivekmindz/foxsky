<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Tax extends MX_Controller{

public function __construct() {
	$this->load->model("supper_admin");
	$this->load->helper('my_helper');
	$this->load->library('PHPExcel');
	$this->load->library('PHPExcel_IOFactory');
        $this->userfunction->loginAdminvalidation();
}

public function addtax(){
  $this->userfunction->loginAdminvalidation();
  if($this->input->post('submitcat')){
   $brand=$this->input->post('brandid');
   $catid=$this->input->post('catid'); 
   $parameter=array('act_mode'=>'selprod','row_id'=>$brand,'taxvalue'=>'','countryid'=>$catid,'stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
   $record['prvieww']=$this->supper_admin->call_procedure('proc_tax',$parameter);
}

 if(!empty($this->input->post('Download')))
  {
    $k=0;
 foreach ($this->input->post('proid') as $key => $value) {
  $parameter[]=array(
     'proid'   =>$value,
     'proname' =>$this->input->post('proname')[$value],
     'prosku'  =>$this->input->post('prosku')[$value],
     'prosell' =>$this->input->post('prosellin')[$value]);
  $k++;
}
 $parameter3=array('act_mode'=>'vetax','row_id'=>'','taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
 $record['proctax']=$this->supper_admin->call_procedure('proc_tax',$parameter3);

 foreach ($record['proctax'] as $key => $value) {
   $taxname[]=$value->taxname;
}
  $finalExcel = array('ProductID','ProductName','ProductSku','Selling Price');
  $finalExcelArr=array_merge($finalExcel,$taxname);
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->setActiveSheetIndex(0);
  $objPHPExcel->getActiveSheet()->setTitle('tax#'.$this->input->post('procat').'');
  $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
  $j=2;

  for($i=0;$i<count($finalExcelArr);$i++){
   $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

   foreach ($parameter as $key => $value) {
    $newvar = $j+$key;
    $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value['proid']);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value['proname']);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value['prosku']);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value['prosell']);

  }
}
    $filename='ProductTax.xls';
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    ob_end_clean();
    ob_start();  
    $objWriter->save('php://output');
   
    }
 //-------------------------------tax upload-----------------------------------------------//   
 
 if($this->input->post('submit')){
	$count=$this->input->post('countryid');
	$stat =$this->input->post('stateid');
	$fileName    = $_FILES['userfile']['tmp_name'];
	$objPHPExcel = PHPExcel_IOFactory::load($fileName);
	$rows        = $objPHPExcel->getActiveSheet()->toArray(); 
	$sheetName   = $objPHPExcel->getActiveSheet()->getTitle();
	$parameter3=array('act_mode'=>'vetax','row_id'=>'','taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
	$record['proctax']=$this->supper_admin->call_procedure('proc_tax',$parameter3);
 foreach ($record['proctax'] as $key => $value) { 
	$taxname[]=$value->taxname;
	$tcatIds[$value->taxname] = $value->id;
 }
 
for($i=0;$i<count($rows);$i++){

 for($j=0;$j<count($rows[$i]);$j++){

 }
$headArr[] = $rows[$i];
}
$taxsql    ='CREATE temporary TABLE tbltaxvalue ( id INT NOT NULL AUTO_INCREMENT Primary key ,taxid VARCHAR(250) NOT NULL , taxprice VARCHAR(250) NOT NULL)';   
$taxinsert ='insert into tbltaxvalue(taxid,taxprice)';	
$countColumn  = count($headArr[0]);
$counttax     = count($taxname);
$totaltax     =$countColumn-$counttax;
$tottaxlimit  =$totaltax+$counttax;

for($k=0;$k<count($headArr);$k++){
 if(count($headArr[$k])>2){
  if(gettype($headArr[$k]) == 'array'){
   if($k > 0){
//-------------------feature value ------------------//
    $taxsql4 = 'values ';
    $uu=0;
for($ir=$totaltax;$ir<$tottaxlimit; $ir++){
   $taxVals[$taxname[$uu]] = $headArr[$k][$ir];//create attribute values array
   $uu++;
}

$taxsql4_4='';
foreach($taxVals as $c=>$d){
  $taxsql4_4[] = "('".$tcatIds[$c]."','".$d."')";
}

$alltaxVals = implode(",", $taxsql4_4);
$finaltax = $taxsql4.$alltaxVals;
$taxnewsql5=$taxinsert;
$taxnewsql5.=$finaltax;
$taxnewsql5.=";";

//---------------------end ------------------------------//

$parameter=array('tcountryid'=>$count,'tstateid'=>$stat,'tproid'=>$headArr[$k][0],'tbaseprice'=>$headArr[$k][3],'taxsql'=>$taxsql,'taxsql2'=>$taxnewsql5);
$responce['upload']=$this->supper_admin->call_procedure('proc_inserttax',$parameter);

}

} 
}// divif end
}



//exit;



 
} 
  

//--------------------------------- end tax upload------------------------------------------//
  $parameter = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
  $record['viewcountry']   = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  $parameterr              = array('act_mode'=>'catview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $record['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr);
  $parameter               = array('act_mode'=>'brmview','row_id'=>'','brandname'=>'','brandimage'=>'','');
  $record['vieww']         = $this->supper_admin->call_procedure('proc_brand',$parameter); 	
  $this->load->view('helper/header');
  $this->load->view('tax/taxproduct',$record);
 } 

public function viewtaxprod(){
  if($this->input->post('submitcat')){
   $countryid=$this->input->post('countryid');
   $stateid=$this->input->post('stateid'); 
   $parameter=array('act_mode'=>'vptax','row_id'=>$countryid,'taxvalue'=>'','countryid'=>$stateid,'stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
   $record['prvieww']=$this->supper_admin->call_procedure('proc_tax',$parameter);
     
}
  $parameter = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
  $record['viewcountry']   = $this->supper_admin->call_procedure('proc_geographic',$parameter); 
  $this->load->view('helper/header');
  $this->load->view('tax/viewtaxproduct',$record);
}



 public function taxadd(){
 $this->userfunction->loginAdminvalidation();	
  if($this->input->post('submit')){
  	$parameter=array('act_mode'=>'inserttax','row_id'=>'','taxvalue'=>$this->input->post('taxname'),'countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
  	$this->supper_admin->call_procedure('proc_tax',$parameter);
  	redirect("admin/tax/viewtax");
  } 
  $this->load->view('helper/header');
  $this->load->view('tax/addtax');
 } 

public function viewtax(){
	$this->userfunction->loginAdminvalidation();
	$parameter=array('act_mode'=>'view','row_id'=>'','taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
	$data['vieww']=$this->supper_admin->call_procedure('proc_tax',$parameter);

  //----------------  Download Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Tax Name');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Tax Type Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($data['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->taxname);
            
            }
          }

          $filename='Tax Type.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------// 

	$this->load->view('helper/header');
	$this->load->view('tax/viewtax',$data);

  } 

public function taxupdate($id){
	if($this->input->post('submit')){
	 $parameter=array('act_mode'=>'updtax','row_id'=>$id,'taxvalue'=>$this->input->post('taxname'),'countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
	 $this->supper_admin->call_procedure('proc_tax',$parameter);
  	 redirect("admin/tax/viewtax");	
	}
    $parameter=array('act_mode'=>'idview','row_id'=>$id,'taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
  	$data['vieww']=$this->supper_admin->call_procedureROW('proc_tax',$parameter);

  	$this->load->view('helper/header');
  	$this->load->view('tax/edittax',$data);	
} 

public function taxdelete($id){
$parameter=array('act_mode'=>'delettax','row_id'=>$id,'taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
$this->supper_admin->call_procedure('proc_tax',$parameter);
redirect("admin/tax/viewtax");		
} 

public function taxstatus(){
	$rowid         = $this->uri->segment(4);
	$status        = $this->uri->segment(5);
	$act_mode      = $status=='A'?'activetax':'inactivetax';
	$parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
	$response      = $this->supper_admin->call_procedure('proc_tax',$parameter);

	$this->session->set_flashdata('message', 'Your Status was successfully Updated.');
	redirect("admin/tax/viewtax");
}




/************************************** Tax State Wise *********************************************/

/*
public function addtaxstatewise()
{
    $this->userfunction->loginAdminvalidation();
    if($this->input->post('submit'))
    {
        $brandid   =$this->input->post('brandid');
        $catid     =$this->input->post('catid');
        $countryid =$this->input->post('countryid');
        $stateid   =$this->input->post('stateid');
        $taxid     =$this->input->post('taxid');
        $taxvalue  =$this->input->post('taxvalue');  

        $parametercheck =array('act_mode' =>'taxcheckentry',
                          'row_id'   =>'',
                          'taxvalue' =>'',
                          'countryid'=>$countryid,
                          'stateid'  =>$stateid,
                          'baseprice'=>$brandid,
                          'localvat' =>$catid,
                          'vatcat'   =>'',
                          'vatnoncat'=>'',
                          'octorytax'=>''
                          );
        
        $record['check']=$this->supper_admin->call_procedureROW('proc_tax',$parametercheck);
        
        if($brandid==$record['check']->brandid || $catid==$record['check']->catidd || $countryid==$record['check']->countryid || $stateid==$record['check']->stateid)
        {
            $this->session->set_flashdata("message", "This state vat already exists");
            redirect("admin/tax/addtaxstatewise");
        }
        else
        {
            foreach ($this->input->post('taxid') as $key => $value) 
            {
                $parameter =array('act_mode' =>'ctaxinsert',
                          'row_id'   =>$value,
                          'taxvalue' =>'',
                          'countryid'=>$countryid,
                          'stateid'  =>$stateid,
                          'baseprice'=>$brandid,
                          'localvat' =>$catid,
                          'vatcat'   =>$this->input->post('taxvalue')[$value],
                          'vatnoncat'=>'',
                          'octorytax'=>''
                          );
        
                $record['prvieww']=$this->supper_admin->call_procedure('proc_tax',$parameter);
       
            }
            $this->session->set_flashdata("message", "Your information was successfully save");
            redirect("admin/tax/viewtaxstatewise");
        }
  }

    $parameter = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $record['viewcountry']= $this->supper_admin->call_procedure('proc_geographic',$parameter);
    $parameter1 = array('act_mode'=>'brmview','row_id'=>'','brandname'=>'','brandimage'=>'','');
    $record['viewwbrand']= $this->supper_admin->call_procedure('proc_brand',$parameter1); 
    $parameter=array('act_mode'=>'view','row_id'=>'','taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
    $record['vieww']=$this->supper_admin->call_procedure('proc_tax',$parameter);

    $this->load->view('helper/header');
    $this->load->view('tax/taxproductstatewise',$record);
}
*/


/*
public function addtaxstatewise()
{  
    $this->userfunction->loginAdminvalidation();
    if($this->input->post('submit'))
    {
        //$brandid   =$this->input->post('brandid');
        $catid     =$this->input->post('catid');
        $countryid =$this->input->post('countryid');
        $stateid   =$this->input->post('stateid');
        $taxid     =$this->input->post('taxid');
        $taxvalue  =$this->input->post('taxvalue');  

        $parametercheck =array('act_mode' =>'taxcheckentry',
                          'row_id'   =>'',
                          'taxvalue' =>'',
                          'countryid'=>$countryid,
                          'stateid'  =>$stateid,
                          //'baseprice'=>$brandid,
                          'baseprice'=>'',
                          'localvat' =>$catid,
                          'vatcat'   =>'',
                          'vatnoncat'=>'',
                          'octorytax'=>''
                          );
        
        $record['check']=$this->supper_admin->call_procedureROW('proc_tax',$parametercheck);
        
        //if($brandid==$record['check']->brandid || $catid==$record['check']->catidd || $countryid==$record['check']->countryid || $stateid==$record['check']->stateid)
        if($catid==$record['check']->catidd || $countryid==$record['check']->countryid || $stateid==$record['check']->stateid)
        {
            $this->session->set_flashdata("message", "This state vat already exists");
            redirect("admin/tax/addtaxstatewise");
        }
        else
        {
            foreach ($this->input->post('taxid') as $key => $value) 
            {
                $parameter =array('act_mode' =>'ctaxinsert',
                          'row_id'   =>$value,
                          'taxvalue' =>'',
                          'countryid'=>$countryid,
                          'stateid'  =>$stateid,
                          //'baseprice'=>$brandid,
                          'baseprice'=>'',
                          'localvat' =>$catid,
                          'vatcat'   =>$this->input->post('taxvalue')[$value],
                          'vatnoncat'=>'',
                          'octorytax'=>''
                          );
        
                $record['prvieww']=$this->supper_admin->call_procedure('proc_tax',$parameter);
       
            }
            $this->session->set_flashdata("message", "Your information was successfully save");
            redirect("admin/tax/viewtaxstatewise");
        }
  }

    $parameter = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $record['viewcountry']= $this->supper_admin->call_procedure('proc_geographic',$parameter);
    //$parameter1 = array('act_mode'=>'brmview','row_id'=>'','brandname'=>'','brandimage'=>'','');
    //$record['viewwbrand']= $this->supper_admin->call_procedure('proc_brand',$parameter1); 
    $parameter1=array('act_mode'=>'viewcatdata','row_id'=>'','taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
    $record['viewwcattid']=$this->supper_admin->call_procedure('proc_tax',$parameter1);
    $parameter2=array('act_mode'=>'view','row_id'=>'','taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
    $record['vieww']=$this->supper_admin->call_procedure('proc_tax',$parameter2);

    $this->load->view('helper/header');
    $this->load->view('tax/taxproductstatewise',$record);
}
*/


















public function addtaxstatewise()
{  
    $this->userfunction->loginAdminvalidation();
    if($this->input->post('submit'))
    {
      //p($_POST);exit;
        $mcatid     =$this->input->post('mcatid');
        $msubcatid     =$this->input->post('msubcatid');
        $subcatid     =$this->input->post('subcatid');
        $countryid =$this->input->post('countryid');
        $stateid   =$this->input->post('stateid');
        $taxid     =$this->input->post('taxid');
        $taxvalue  =$this->input->post('taxvalue');



        foreach ($stateid as $key => $value) {

          foreach ($subcatid as $kcat => $catval) {
            
            $parametch =array('act_mode' =>'taxvalidcheck',
                            'row_id'   =>$catval,
                            'taxvalue' =>'',
                            'countryid'=>$countryid,
                            'stateid'  =>$value,
                            'baseprice'=>'',
                            'localvat' =>'',
                            'vatcat'   =>'',
                            'vatnoncat'=>'',
                            'octorytax'=>''
                            );
            $record['prviewwch']=$this->supper_admin->call_procedureRow('proc_tax',$parametch);
            //p($record['prviewwch']->retax);
            if($record['prviewwch']->retax==0){
            
            $paramet =array('act_mode' =>'inttax',
                            'row_id'   =>$catval,
                            'taxvalue' =>'',
                            'countryid'=>$countryid,
                            'stateid'  =>$value,
                            'baseprice'=>0,
                            'localvat' =>'',
                            'vatcat'   =>'',
                            'vatnoncat'=>'',
                            'octorytax'=>''
                            );
            $record['prvieww']=$this->supper_admin->call_procedureRow('proc_tax',$paramet);
            //p($record['prvieww']->masid);exit;
            //p($paramet);exit;
            foreach ($taxvalue as $ktax => $taxval) {
              

                  $paramettax =array('act_mode' =>'intstax',
                                     'row_id'   =>$record['prvieww']->masid,
                                     'taxvalue' =>'',
                                     'countryid'=>'',
                                     'stateid'  =>$ktax,
                                     'baseprice'=>$taxval,
                                     'localvat' =>'',
                                     'vatcat'   =>'',
                                     'vatnoncat'=>'',
                                     'octorytax'=>''
                                     );
                  //p($paramettax);
                  $record['prviewwdet']=$this->supper_admin->call_procedure('proc_tax',$paramettax);

            } // end taxvalue
            } else {
              
              $parametch =array('act_mode' =>'updatetaxvalidcheck',
                            'row_id'   =>$catval,
                            'taxvalue' =>'',
                            'countryid'=>$countryid,
                            'stateid'  =>$value,
                            'baseprice'=>'',
                            'localvat' =>'',
                            'vatcat'   =>'',
                            'vatnoncat'=>'',
                            'octorytax'=>''
                            );
            $record['updateprviewwch']=$this->supper_admin->call_procedureRow('proc_tax',$parametch);

              $paramet =array('act_mode' =>'inttax',
                            'row_id'   =>$catval,
                            'taxvalue' =>'',
                            'countryid'=>$countryid,
                            'stateid'  =>$value,
                            'baseprice'=>0,
                            'localvat' =>'',
                            'vatcat'   =>'',
                            'vatnoncat'=>'',
                            'octorytax'=>''
                            );
            $record['prvieww']=$this->supper_admin->call_procedureRow('proc_tax',$paramet);
            //p($record['prvieww']->masid);exit;
            //p($paramet);exit;
            foreach ($taxvalue as $ktax => $taxval) {
              

                  $paramettax =array('act_mode' =>'intstax',
                                     'row_id'   =>$record['prvieww']->masid,
                                     'taxvalue' =>'',
                                     'countryid'=>'',
                                     'stateid'  =>$ktax,
                                     'baseprice'=>$taxval,
                                     'localvat' =>'',
                                     'vatcat'   =>'',
                                     'vatnoncat'=>'',
                                     'octorytax'=>''
                                     );
                  //p($paramettax);
                  $record['prviewwdet']=$this->supper_admin->call_procedure('proc_tax',$paramettax);

            } // end taxvalue



            }
          } // end subcatid
        } // end stateid
        $this->session->set_flashdata("message", "Your information was successfully save");
        redirect("admin/tax/addtaxstatewise");
    }

    $parameter = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $record['viewcountry']= $this->supper_admin->call_procedure('proc_geographic',$parameter);
    
    $parameterr                = array('act_mode'=>'parecatview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $record['viewwcattid'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 

    $parameter2=array('act_mode'=>'view','row_id'=>'','taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
    $record['vieww']=$this->supper_admin->call_procedure('proc_tax',$parameter2);

    $this->load->view('helper/header');
    $this->load->view('tax/taxproductstatewise',$record);
}


public function viewtaxstatewise(){
  $this->userfunction->loginAdminvalidation();

  //--------------------------multiple ststus ------------------------------//
    if($this->input->post('submitstatus')){
     foreach ($this->input->post( 'attstatus') as $key => $value) {
        $status            = $this->input->post('attstatu')[$value];
        $act_mode          = $status == '1'?'activetaxstatewise':'inactivetaxstatewise';
        $parameter2        = array('act_mode'=>$act_mode,'row_id'=>$value,'taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
        //p($parameter2);exit;
        $responce['vieww'] = $this->supper_admin->call_procedure('proc_tax',$parameter2);
      }
      
     $this->session->set_flashdata("message", "Your Status was successfully Updated.");
     redirect("admin/tax/viewtaxstatewise");
    }

    //--------------------------multiple Update ------------------------------//
    if($this->input->post('updatetax')){
     foreach ($this->input->post( 'attstatus') as $key => $value) {
        $taxvalue            = $this->input->post('taxvalue')[$value];
        $parameter3        = array('act_mode'=>'edittax','row_id'=>$value,'taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>$taxvalue,'vatnoncat'=>'','octorytax'=>'');
        //p($parameter3);exit;
        $responce['vieww'] = $this->supper_admin->call_procedure('proc_tax',$parameter3);
      }
      
     $this->session->set_flashdata("message", "Your Tax Value was successfully Updated.");
     redirect("admin/tax/viewtaxstatewise");
    }

  $parameter=array('act_mode'=>'totalviewtbltax','row_id'=>'','taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
  $data['vieww']=$this->supper_admin->call_procedure('proc_tax',$parameter);


  //----------------  Download Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
            if(empty($this->input->post('tax_xlsstate'))){
                
                $this->session->set_flashdata("message", "Please select any state for Download Excel.");
                redirect("admin/tax/viewtaxstatewise");

            } else {

                $parameter=array('act_mode'=>'viewxlstbltax','row_id'=>$this->input->post('tax_xlsstate'),'taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
                $data['xlsvieww']=$this->supper_admin->call_procedure('proc_tax',$parameter);
               
               $finalExcelArr = array('S.No.','CATEGORY','COUNTRY','STATE','GST','Tax Status');
               $objPHPExcel = new PHPExcel();
               $objPHPExcel->setActiveSheetIndex(0);
               $objPHPExcel->getActiveSheet()->setTitle('State Wise Tax Worksheet');
               $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
                $j=2;
                
                //For freezing top heading row.
                $objPHPExcel->getActiveSheet()->freezePane('A2');

                //Set height for column head.
                $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                            
               for($i=0;$i<count($finalExcelArr);$i++){
                
                //Set width for column head.
                $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

                //Set background color for heading column.
                $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '71B8FF')
                        ),
                          'font'  => array(
                          'bold'  => false,
                          'size'  => 15,
                          )
                    )
                );

                $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
                $us=1;
                foreach ($data['xlsvieww'] as $key => $value) {
                 
                $newvar = $j+$key;

                //Set height for all rows.
                $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);

                $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $us);
                $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->catname);
                $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->name);
                $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->statename);
                $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->octroientry);
                if($value->statuss=='A'){
                  $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, "Active");
                } else {
                  $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, "Inactive");
                }
                $us++;
                }
              }

              $filename='State Wise Tax.xls';
              header('Content-Type: application/vnd.ms-excel'); //mime type
              header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
              header('Cache-Control: max-age=0'); //no cache
              $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
              ob_end_clean();
              ob_start();  
              $objWriter->save('php://output');
            
            }//end empty state else  
         
          }// end excel if
      //----------------  End Download Excel ------------------------//

      if(!empty($_GET['filterby']))
      { 
        //echo "hello"; exit;
          $parameter=array('tcountryid'=>$_GET['tax_searchchildcat'],'tstateid'=>$_GET['tax_searchstate'],'tproid'=>'','tbaseprice'=>'','taxsql'=>'','taxsql2'=>'viewfiltertax');
          $data['vieww']=$this->supper_admin->call_procedure('proc_inserttax',$parameter); 

          //----------------  start pagination configuration ------------------------// 

           $config['base_url']         = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
           $config['total_rows']       = count($data['vieww']);
           $config['per_page']         = 50;
           $config['use_page_numbers'] = TRUE;

           $this->pagination->initialize($config);
           if($_GET['page']){
             $page         = $_GET['page']-1 ;
             $page         = ($page*50);
             $second       = $config['per_page'];
           }
           else{
             $page         = 0;
             $second       = $config['per_page'];
           }
           
           $str_links = $this->pagination->create_links();
           $data["links"]  = explode('&nbsp;',$str_links );

          //----------------  end pagination configuration ------------------------// 

          $parameter=array('tcountryid'=>$_GET['tax_searchchildcat'],'tstateid'=>$_GET['tax_searchstate'],'tproid'=>$page,'tbaseprice'=>$second,'taxsql'=>'','taxsql2'=>'viewfiltertax');
          $data['vieww']=$this->supper_admin->call_procedure('proc_inserttax',$parameter);

      } else {
          //----------------  start pagination ------------------------// 

          $config['base_url']         = base_url()."admin/tax/viewtaxstatewise?";
          $config['total_rows']       = count($data['vieww']);
          $config['per_page']         = 50;
          $config['use_page_numbers'] = TRUE;

         $this->pagination->initialize($config);
         if($_GET['page']){
           $page         = $_GET['page']-1 ;
           $page         = ($page*50);
           $second       = $config['per_page'];
         }
         else{
           $page         = 0;
           $second       = $config['per_page'];
         }
         
         $str_links = $this->pagination->create_links();
         $data["links"]  = explode('&nbsp;',$str_links );

        $parameter=array('act_mode'=>'totalviewtbltax','row_id'=>$page,'taxvalue'=>'','countryid'=>$second,'stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
        $data['vieww']=$this->supper_admin->call_procedure('proc_tax',$parameter);

    }
    //----------------  end pagination ------------------------//  

    $parameterr              = array('act_mode'=>'catview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $data['viewwcattid'] = $this->supper_admin->call_procedure('proc_category',$parameterr);
    $parameterst = array('act_mode'=>'countrystate','row_id'=>6,'counname'=>'','coucode'=>'','commid'=>'');
    $data['stvieww']  = $this->supper_admin->call_procedure('proc_geographic',$parameterst);


  $this->load->view('helper/header');
  $this->load->view('tax/viewtaxstatewise',$data);

  } 

public function taxstatusstatewise(){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='1'?'activetaxstatewise':'inactivetaxstatewise';
  $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'taxvalue'=>'','countryid'=>'','stateid'=>'','baseprice'=>'','localvat'=>'','vatcat'=>'','vatnoncat'=>'','octorytax'=>'');
  $response      = $this->supper_admin->call_procedure('proc_tax',$parameter);

  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect("admin/tax/viewtaxstatewise");
}


//-----------------bulk data insert on tax----------------------------

public function jjaddtaxmas(){
//-----------------bulk data insert on tax----------------------------
  /*$parameter =array('act_mode'=>'brandid','id'=>'');
  $record['bbb']=$this->supper_admin->call_procedure('proc_categorydiscount',$parameter);*/
//p($record['bbb']);
  $parameter1 =array('act_mode'=>'satid','id'=>'');
  $record['stid']=$this->supper_admin->call_procedure('proc_categorydiscount',$parameter1);
   //$var = mysqli_connect('192.168.1.30','saa','123456');aaatqqaxid
  //mysqli_select_db('bizzgain',$var);
  //p($record['stid']);
  $parameter2 =array('act_mode'=>'aaatqqaxid','id'=>'');
  $record['stiqqd']=$this->supper_admin->call_procedure('proc_categorydiscount',$parameter2);

  //p($record['stiqqd']);exit;
foreach ($record['stid'] as $key => $value) {
  //p($value->stateid);exit();
 //$val =$value->id;
 //for($i=0;$i<33;$i++)
//{
for($j=0;$j<9;$j++)
{
  
  $paramet =array('act_mode' =>'inttax',
                          'row_id'   =>$record['stiqqd'][$j]->childid,
                          'taxvalue' =>'',
                          'countryid'=>6,
                          'stateid'  =>$value->stateid,
                          'baseprice'=>0,
                          'localvat' =>'',
                          'vatcat'   =>'',
                          'vatnoncat'=>'',
                          'octorytax'=>''
                          );
        //p($paramet);
  $record['prvieww']=$this->supper_admin->call_procedure('proc_tax',$paramet);
}
  /*$dda=date('Y-m-d h:m:i');
 $a=("insert into tbl_taxmaster (`countryid`,`stateid`,`brandid`,`catidd`,`statuss`,`createdon`) values('6','".$record['stid'][$i]->stateid."','$val','6','A','$dda')");
 $aa=mysql_query($a);
 
 if($aa>0)
 {
  echo "done";
 }*/
 //echo "</br>";


//}

}

echo "done";
//exit;


//-----------------bulk data insert on tax----------------------------


}



public function kkaddtaxbulk(){
  
  $parameter =array('act_mode'=>'taxid','id'=>'');
  $record['bbb']=$this->supper_admin->call_procedure('proc_categorydiscount',$parameter);
//p($record['bbb']);
  $parameter1 =array('act_mode'=>'tqqaxid','id'=>'');
  $record['ttt']=$this->supper_admin->call_procedure('proc_categorydiscount',$parameter1);
  //p($record['ttt']);
  /*for($i=0;$i<5;$i++)
{
  p($record['bbb'][$i]->id);
}*/
//exit;
  foreach ($record['ttt'] as $key => $value) {
    for($i=0;$i<5;$i++)
{
    $paramet =array('act_mode' =>'intstax',
                          'row_id'   =>$value->id,
                          'taxvalue' =>'',
                          'countryid'=>'',
                          'stateid'  =>$record['bbb'][$i]->id,
                          'baseprice'=>5,
                          'localvat' =>'',
                          'vatcat'   =>'',
                          'vatnoncat'=>'',
                          'octorytax'=>''
                          );
        //p($paramet);
                $record['prvieww']=$this->supper_admin->call_procedure('proc_tax',$paramet);
}
//exit;
  }
echo "done";
}


//-----------------bulk data insert on tax----------------------------


}
?>