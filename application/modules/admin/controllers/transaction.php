<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Transaction extends MX_Controller {

  public function __construct() {    
      $this->load->model("supper_admin");
      //$this->load->model("transaction_model");
      
      $this->load->helper('my_helper');
      $this->load->library('PHPExcel');
      $this->load->library('PHPExcel_IOFactory');
      $this->userfunction->loginAdminvalidation();    
  } 

  public function update_payment_status()
{
  //p($this->session->userdata('updatestatus'));
  $en=$this->session->userdata('updatestatus')[all].'_'.$this->session->userdata('updatestatus')[intamt].'_'.date('d-m-Y h:i:s');
  //p($en);
  $this->db->query("UPDATE  tbl_transaction_map SET  
    arears =".$this->session->userdata('updatestatus')[arrears].",pay_updation =  '".$en."' WHERE  t_id ='".$this->session->userdata('updatestatus')[tid]."' LIMIT 1");
}

 public function update_payment_status1()
{
  //p($this->session->userdata('updatestatus'));
  $en=$this->session->userdata('updatestatus')[all].'_'.$this->session->userdata('updatestatus')[intamt].'_'.date('d-m-Y h:i:s');
  //p($en);
  $this->db->query("UPDATE  tbl_transaction_map SET  
    arears =".$this->session->userdata('updatestatus')[arrears].",pay_updation =  '".$en."' WHERE  t_id ='".$this->session->userdata('updatestatus')[tid]."' LIMIT 1");

   $this->db->query("UPDATE  tbl_transaction_storewise SET  
    arears =".$this->session->userdata('updatestatus')[arrears].",pay_updation =  '".$en."' WHERE  t_sid ='".$this->session->userdata('updatestatus')[sid]."' LIMIT 1");
}


public function hdfcpaymentprocess()
{
  // p($this->input->post('payment'));
 //p($this->session->userdata('updatestatus'));

  $this->db->query("UPDATE  tbl_transaction_map SET  
    remain_intamt =".$this->input->post('payment').",mode_of_pay =  'Online' WHERE  t_id ='".$this->session->userdata('updatestatus')[tid]."' LIMIT 1");

 //exit;
 $data['price']=$this->input->post('payment');
 $this->load->view('transaction/process_payment',$data);
}

public function hdfcpaymentprocess1()
{
 // p($this->input->post('amount'));
 //p($this->session->userdata('updatestatus'));

  $en=$this->session->userdata('updatestatus')[all].'_'.$this->session->userdata('updatestatus')[intamt].'_'.date('d-m-Y h:i:s');

  if($this->input->post('productinfo')=='Store Payment')
  {
       $this->db->query("UPDATE  tbl_transaction_storewise SET  
    remain_intamt =".$this->input->post('amount').",pay_updation =  '".$en."',mode_of_pay =  'Online',transaction_id='".$this->input->post('txnid')."' WHERE  t_sid ='".$this->session->userdata('updatestatus')[sid]."' LIMIT 1");
  }

  $this->db->query("UPDATE  tbl_transaction_map SET  
    remain_intamt =".$this->input->post('amount').",pay_updation =  '".$en."',mode_of_pay =  'Online',transaction_id='".$this->input->post('txnid')."' WHERE  t_id ='".$this->session->userdata('updatestatus')[tid]."' LIMIT 1");
 //exit;
 $data['price']=$this->input->post('amount');


$PAYU_BASE_URL=$this->config->item('PAYU_BASE_URL'); 
  

$posted = array();

if(!empty($_POST)) {
   
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
  
  }
}


$hash = '';
// Hash Sequence
 $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
 
  $hashVarsSeq = explode('|', $hashSequence);

    $hash_string = '';  
  foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $this->config->item('SALT');


    $hash = strtolower(hash('sha512', $hash_string)); 
    $action = $PAYU_BASE_URL . '/_payment';
  }

$key=$this->config->item('MERCHANT_KEY');

    $data['payu'] = array(

        'action'=>$action,     
        'key' => $this->input->post('key'),  
        'hash' => $hash,
        'txnid' =>  $this->input->post('txnid'),
        'amount' => $this->input->post('amount'),
        'firstname' => $this->input->post('firstname'),
        'email' => $this->input->post('email'),
        'phone' => $this->input->post('phone'),
        'productinfo' => $this->input->post('productinfo'),
        'surl' => $this->input->post('surl'),
        'furl' => $this->input->post('furl'),
        'service_provider' => $this->input->post('service_provider'),
        'lastname' => $this->input->post('lastname'),
        'curl' => $this->input->post('curl'),
        'address1' => $this->input->post('address1'),
        'address2' => $this->input->post('address2'),
        'city' => $this->input->post('city'),
        'state' => $this->input->post('state'),
        'country' => $this->input->post('country'),
        'zipcode' => $this->input->post('zipcode'),
        'zipcode' => $this->input->post('zipcode'),

    );
  
  //p($data);die();
     //$payu['action'];
  

      $this->load->view('transaction/payumoney',$data);

}

public function payment_response_fail()
  {


    //p($_POST);
    //p($_REQUEST["address1"]);
   // p($_REQUEST["status"]);//exit;
  if($_REQUEST['productinfo']=='Store Payment')
  {
       $this->db->query("UPDATE  tbl_transaction_storewise SET pg_response ='".json_encode($_POST)."',payment_status =  '".$_REQUEST["status"]."' WHERE  transaction_id ='".$_REQUEST['txnid']."' LIMIT 1");
  }

     $this->db->query("UPDATE  tbl_transaction_map SET pg_response ='".json_encode($_POST)."',payment_status =  '".$_REQUEST["status"]."' WHERE  transaction_id ='".$_REQUEST['txnid']."' LIMIT 1");

     echo '<div style="text-align: center;">
        <h1>SORRY!</h1>
        <h2>We were unable to process your payment</h2>
    <p style="text-align: center;font-size:13px;"><span style="font-size:20px;color:#FF0000">Error Reason</span><br>'.$_REQUEST["field9"].'</p>';
    
    if($_REQUEST['productinfo']=='Store Payment')
    {
      echo '<a href="'.base_url().'admin/transaction/store_payment" style="text-align: center;">Redirect Payment Process</a></div>';
    }
    else
    {
      echo '<a href="'.base_url().'admin/transaction/retailer_paymentprocess" style="text-align: center;">Redirect Payment Process</a></div>';
    }  
  }
public function payment_response_check()
  {
     

   
        $t_id=$_REQUEST["address1"];
       
         $this->db->query("UPDATE  tbl_transaction_map SET  pg_response ='".json_encode($_POST)."',`payment_status` =  '".$_REQUEST["status"]."' WHERE  t_id ='".$t_id."' LIMIT 1");
         
         $param = array(
                      'act_mode' => 'get_transection', 
                      'row_id' => $t_id,
                      
                    );             
         $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
         
        

         $viewdata['from_date']=$row->from_date;
         $viewdata['to_date']=$row->to_date;
         $viewdata['pay_amt']=$_REQUEST['amount'];//$row->pay_amt;
         $viewdata['transaction_id']=$_REQUEST['txnid'];
         
         $usertype=$this->session->userdata('popcoin_login')->s_usertype;
         $viewdata['usertype']=$usertype;
         
         
         
         if ($usertype==2)//retailer 
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_retailer', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
            
            $viewdata['rs_id']=$row->retailer_codeid;
            $viewdata['rs_name']=$row->s_username;
            //pend($data);
          }
          elseif ($usertype==4)//store
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_store', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);

           
            $viewdata['rs_id']=$row->s_storeunid;
            $viewdata['rs_name']=$row->s_name;
          }
          else {

          }

         // p($_POST);exit;
          /* ----------------- Process Start --------------------- */

          if($this->session->userdata('updatestatus')[rem_bal_id]=='0')
          {
              $rem_bal=0;
          }
          else
          {
              $rem_bal=$this->session->userdata('updatestatus')[rem_bal];

            $this->db->query("UPDATE tbl_transaction_map SET remaining_bal_status=2 WHERE t_id =".$this->session->userdata('updatestatus')[rem_bal_id]." LIMIT 1");
          }
  
          $dis=0;
          $normalamt=0;
          $mode='Online';
          $refid=$_REQUEST['txnid'];
          $tradate=date('d-m-Y');
          $pay_amt=$_REQUEST['amount'];
          $total_amt=$this->session->userdata('updatestatus')[total_amt];
          $intamt=$this->session->userdata('updatestatus')[tinterest];
          $arears=$this->session->userdata('updatestatus')[arrears];

         // $tamt=$total_amt+$intamt+$arears;
          $tamt=$total_amt;
          $pay_amt1=$pay_amt+$rem_bal+$dis;
          $intamt1=$this->session->userdata('updatestatus')[intamt];
          $alldata=$this->session->userdata('updatestatus')[all];
          $ab=explode('#', $alldata);
          $inte=explode('#', $intamt1);
//p($alldata);
            
      /* ----------------------------------- Start ----------------------------------------------------*/

          $rec='0';
          $pay='0';
          $len=0;
          foreach ($ab as $key => $value1) {
             //p($value1);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[3]=='Receivables')
            {
                $rec=$rec+$ab1[2]+$payamt;
                $len++;
            }

            if($ab1[3]=='Payables')
            {
                $pay=$pay+$ab1[2];
            }

          }


         $samt=0;
         $inter=0;
         $arr=0;
         $arr1=0;
         $disc=0;
          foreach ($ab as $key => $value) 
          { # start for each
             //p($value);
               $ab1=explode(',', $value);
      if($ab1[3]=='Receivables') #main
      {

          $inter=$inter+$inte[$key];
          
          $pay_type=$ab1[3];
          if($samt==0)
          {
              $arr=0;
              $arr1=$ab1[2];
              $t_paid=($pay_amt-$intamt)+$rem_bal+$pay+$dis;
            
              if($pay_amt1 > $tamt)
              {
                $queue=1;
                $samt=$samt+($t_paid-$ab1[2]);
                $remaining_balance=$samt;
                $amount_paid=$ab1[2];
                $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                if($len==1)
                {
                  $amt=$pay_amt;
                }
                else
                {
                  $amt=0;
                }
              }
              else
              {
                  if($t_paid > $ab1[2])
                  {
                    $queue=2;
                    $samt=$samt+($t_paid-$ab1[2]);
                    $remaining_balance=0;
                    $amount_paid=$ab1[2];
                    $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                    if($len==1)
                    {
                      $amt=$pay_amt;
                    }
                    else
                    {
                      $amt=0;
                    }
                  }
                  else
                  {
                      $queue=3;
                      $samt=$samt+$t_paid;
                      $remaining_balance=0;
                      $amount_paid=$samt;
                      $paidper=100-(($ab1[2]-$amount_paid)*100/$ab1[2]);
                      if($len==1)
                      {
                        $amt=$pay_amt;
                      }
                      else
                      {
                        $amt=0;
                      }
                  }
                  
              }
          }
          else
          {
              $arr=$arr+$arr1;
              $arr1=$arr1+$ab1[2];
              if($samt > $total_amt)
              {
                $queue=4;
                $samt=$samt-$ab1[2];
                $remaining_balance=$samt;
                $amount_paid=$ab1[2];
                $paidper=100-(($ab1[2]-$amount_paid)*100/$ab1[2]);
                for($i=1; $i <= $len; $i++) 
                { 
                   if($i==$len)
                   {
                      $amt=$pay_amt;
                   } 
                   else
                   {
                      $amt=0;
                   }
                }
              }
              else
              {
                  $queue=5;
                  $remaining_balance=0;
                  $amount_paid=$samt;
                  $paidper=100-(($ab1[2]-$amount_paid)*100/$ab1[2]);
                  $amt=$pay_amt;
              }
          }

          if ($disc == 0) 
          {
            if($len==1)
            {
              $discount=$dis; 
              $remain_balance=$samt;   
            } 
            else 
            {
              $discount=0;
              $remain_balance=0;  
            }
          } 
          else if ($disc == $len -1) 
          {
            $discount=$dis;
            $remain_balance=$samt;  
          }
          else
          {
            $discount=0;
            $remain_balance=0;  
          }
          $disc++;

        }
        else #main
        {
            $inter=0;
            $disc=0;
            $queue=6;
            $discount=0;
            $pay_type=$ab1[3];
            $remaining_balance=0;
            $amount_paid=$ab1[2];
            $paidper=100;
            $t_paid=$ab1[2];
        }
         
        $t_idd=$ab1[7];
        $store_idd=$ab1[11];
        $txn_idd=$ab1[5];

        $en=$alldata.'_'.$intamt1.'_'.date('d-m-Y h:i:s');
         $this->db->query("UPDATE tbl_transaction_map SET arears =".$arr.",pay_updation ='".$en."', remain_intamt =".$pay_amt.",mode_of_pay ='".$mode."' WHERE t_id ='".$t_idd."' LIMIT 1");

     
              $testparam = array('store_idd' => $store_idd,
                    'txn_idd' => $txn_idd,
                    'row_id' => $t_idd,
                    'amount' => $ab1[2],
                    'rem_bal' => $samt,
                    'arears' => $arr,#$arears,
                    'remaining_balance' => $remaining_balance,
                    'amount_paid' => $amount_paid,
                    'pay_amt' => $pay_amt,
                    'paidper' => $paidper,
                    'pay_type' => $pay_type,
                    'queue' => $queue,
                    'discount' =>$discount,
                    'dis_count' =>$disc,
                    'interest'=>$inter,
                    'actual_interest' => $inte[$key],
                    'aamt' =>$amt
                  );  
       // p($testparam);    

    //} exit;{
 /* ----------------------------------- end ----------------------------------------------------*/





          $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_idd,//$_REQUEST['t_id'],
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
                            
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
//p($data);
          if($data->remaining_amt=="") {
            
            $remaining=number_format($data->pay_amt)-number_format($amount_paid) ;
          } else {
            
            $remaining=number_format($data->remaining_amt)-number_format($amount_paid);
          }          
          /*$param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_idd,//$_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $remaining,
                    'param10' => $txn_idd,//$_REQUEST['txn_id'],
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $intamt,//$intamt1,
                    'param15' => $arears,//$intamt_paid,
                    'param16' => $store_idd,//$_REQUEST['str_id'],
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest,
                    'param21' => $refid,
                    'param22' => $tradate,
                    'param23' => $dis,
                    'param24' => $total_amt,
                    'param25' => $mode,
                    'param26' => $normalamt,
                    'param27' => $pay_amt,
                    'param28' => '',
                    'param29' => '',
                    'param30' => ''
                  );  */

           $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_idd,//$_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $remaining,
                    'param10' => $txn_idd,//$_REQUEST['txn_id'],
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $inter,#$intamt,//$intamt1,
                    'param15' => $arr,#$arears,//$intamt_paid,
                    'param16' => $store_idd,//$_REQUEST['str_id'],
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest,
                    'param21' => $refid,
                    'param22' => $tradate,
                    'param23' => $discount,
                    'param24' => $total_amt,
                    'param25' => $mode,
                    'param26' => $amt,#$pay_amt,#$normalamt,
                    'param27' => $pay_amt,
                    'param28' => $remaining_balance,
                    'param29' => $tamt,
                    'param30' => ''
                  );  


            //p($param2);//exit;
            $res2 = $this->supper_admin->call_procedure('proc_manualpayment', $param2);                  
            //$res2 = $this->supper_admin->call_procedure('proc_procedure4', $param2);
            //p($res2);
      //} exit;{
          foreach($res2 as $val) { //4
            if($val->process_amt=='')
            {
                $process_amt=($val->pay_amt*$paidper)/100;
            }
            else
            {
                $a=$val->pay_amt-$val->process_amt;
                $process_amt=($a*$paidper)/100;
            }
            
            $param = array(
                  'act_mode' => 'updatetranssuccessdata', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt+$val->process_amt,
                  'param17' => 'success',
                  'param18' => $paidper,
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);           
            //$res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            if($val->user_type=='C') { //5
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc',//'insertcustomercashback', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => '',
                  'param18' => '',
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);   
                           
          //$res = $this->supper_admin->call_procedure('proc_procedure4', $param);
                
      $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$process_amt;
      #file_get_contents($no_url);
            } //5
          } //4   


        } 
        #for each first end
       $this->load->view('transaction/payment_success',$viewdata);

        /* ----------------- Process End --------------------- */


      
  }

public function check1()
{
   $this->load->view('transaction/payment_success',$viewdata);
}

  public function payment_response()
  {
     
    if (isset($_REQUEST['ResponseCode']) && $_REQUEST['ResponseCode'] == 0) {
         $viewdata=array();
        $t_id=$_REQUEST['MerchantRefNo'];
         
       $trans_id=$_REQUEST['TransactionID'];
       //echo "____";
//p($_REQUEST['ResponseCode']);
         //saving paymentgetway response
          $serlisedata=serialize($_REQUEST);
          
          //$update_data = array('pg_response' =>$serlisedata);  
          //$this->db->where('t_id', $t_id);  
          //$this->db->update('tbl_transaction_map', $update_data);
         $this->db->query("UPDATE  tbl_transaction_map SET  pg_response ='".$serlisedata."',`transaction_id` =  '".$trans_id."' WHERE  t_id ='".$t_id."' LIMIT 1");
         //end here  //saving paymentgetway response
         $param = array(
                      'act_mode' => 'get_transection', 
                      'row_id' => $t_id,
                      
                    );             
         $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
         
        

         $viewdata['from_date']=$row->from_date;
         $viewdata['to_date']=$row->to_date;
         $viewdata['pay_amt']=$_REQUEST['Amount'];//$row->pay_amt;
         $viewdata['transaction_id']=$trans_id;
         
         $usertype=$this->session->userdata('popcoin_login')->s_usertype;
         $viewdata['usertype']=$usertype;
         
         
         
         if ($usertype==2)//retailer 
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_retailer', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
            
            $viewdata['rs_id']=$row->s_admin_id;
            $viewdata['rs_name']=$row->company_name;
            //pend($data);
          }
          elseif ($usertype==4)//store
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_store', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);

           
            $viewdata['rs_id']=$row->store_id;
            $viewdata['rs_name']=$row->s_companyname;
          }
          else {

          }


       /**pavan calculation*/
          $alldata=$this->session->userdata('updatestatus')[all];
          $int=$this->session->userdata('updatestatus')[intamt];
          $arears=$this->session->userdata('updatestatus')[arrears]; 
       // p($alldata);exit;

          $ab=explode('#', $alldata);
          $inte=explode('#', $int);

          $rec='0';
          $pay='0';

          foreach ($ab as $key => $value1) {
              //p($value);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[3]=='Receivables')
            {
                $rec=$rec+$ab1[2]+$payamt;
            }

            if($ab1[3]=='Payables')
            {
                $pay=$pay+$ab1[2];
            }

          }
         $pa=$pay-$rec;

          foreach ($ab as $key => $value) {
              //p($value);
               $ab1=explode(',', $value);
       
      
         //pawan calculation end here 
         $param2 = array(
                      'act_mode' => 'fetchtransdata', 
                      'row_id' => $t_id,
                      'param1' => '',
                      'param2' => '',
                      'param3' => '',
                      'param4' => '',
                      'param5' => '',
                      'param6' => '',
                      'param7' => '',
                      'param8' => '',
                      'param9' => '',
                      'param10' => '',
                      'param11' => '',
                      'param12' => '',
                      'param13' => '',
                      'param14' => '',
                      'param15' => '',
                      'param16' => '',
                      'param17' => '',
                      'param18' => ''
                    );             
            $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
             //pend($data);

            if($data->pay_type=='Receivables')
            {
                $updateamt=0;
            }
            else
            {
              $updateamt=0;
                
            }
        $param22 = array(
                      'act_mode' => 'updatetransmapdata', 
                      'row_id' => $t_id,
                      'param1' => $data->txn_id,
                      'param2' => $data->cycle_id,
                      'param3' => $data->retailer_id,
                      'param4' => $data->store_count,//$data->store_id,
                      'param5' => $data->from_date,
                      'param6' => $data->to_date,
                      'param7' => $data->pay_date,
                      'param8' => $data->pay_type,
                      'param9' => $updateamt,
                      'param10' => $data->txn_id,
                      'param11' => 'success',
                      'param12' => $data->pay_amt,
                      'param13' => $data->cycle_day,
                      'param14' => $inte[$key],
                      'param15' => $arears,
                      'param16' => $ab1[11],
                      'param17' => $data->pay_permission,
                      'param18' => $data->mpc_interest
                    );  

      $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param22);
      foreach($res2 as $val) {  

             $param = array(
                    'act_mode' => 'updatetranssuccessdata_mpc', 
                    'row_id' => $val->t_id,
                    'param1' => $val->txn_id,
                    'param2' => $val->tran_map_id,
                    'param3' => $val->cycle_id,
                    'param4' => $val->receipts_id,
                    'param5' => $val->order_id,
                    'param6' => $val->retailer_id,
                    'param7' => $val->store_id,
                    'param8' => $val->deal_id,
                    'param9' => $val->user_id,
                    'param10' => $val->user_type,
                    'param11' => $val->pay_type,
                    'param12' => $val->tax_type,
                    'param13' => $val->tax_amt,
                    'param14' => $val->total_amt,
                    'param15' => $val->pay_amt,
                    'param16' => $val->pay_amt,
                    'param17' => $val->txn_id_point,
                    'param18' => $val->update_amt
                  );
                  //p($param);  
             $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            
                 
              if($val->user_type=='C') {
                $param = array(
                    'act_mode' => 'insertcustomercashback_mpc', 
                    'row_id' => '',
                    'param1' => $val->txn_id,
                    'param2' => $val->tran_map_id,
                    'param3' => $val->cycle_id,
                    'param4' => $val->receipts_id,
                    'param5' => $val->order_id,
                    'param6' => $val->retailer_id,
                    'param7' => $val->store_id,
                    'param8' => $val->deal_id,
                    'param9' => $val->user_id,
                    'param10' => $val->user_type,
                    'param11' => $val->pay_type,
                    'param12' => $val->tax_type,
                    'param13' => $val->tax_amt,
                    'param14' => $val->total_amt,
                    'param15' => $val->pay_amt,
                    'param16' => $val->update_amt,
                    'param17' => '',
                    'param18' => ''
                  );             
                $res1 = $this->supper_admin->call_procedure('proc_procedure4', $param);
              }
            } 
          }//foreach ($ab as $key => $value) {
            //pend($viewdata);
        //$this->load->view('helper/header');      
       $this->load->view('transaction/payment_success',$viewdata);
      }/// end if (isset($_REQUEST['ResponseCode']) && $_REQUEST['ResponseCode'] == 0) 
     else{

        echo 'payment Failed';

        $serlisedata=serialize($_REQUEST);
         $this->db->query("UPDATE  tbl_transaction_map SET  pg_response =".$serlisedata.",payment_status =  'Failed' WHERE  t_id ='".$t_id."' LIMIT 1");
     }  
      
  }

  public function payment_response_status()
  {
     $t_id1=$this->session->userdata('updatestatus')[tid];//$_REQUEST['MerchantRefNo'];
      $s_id=$this->session->userdata('updatestatus')[sid];

    if (isset($_REQUEST['ResponseCode']) && $_REQUEST['ResponseCode'] == 0) {
         $viewdata=array();
      //$t_id=$this->session->userdata('updatestatus')[tid];//$_REQUEST['MerchantRefNo'];
     // $s_id=$this->session->userdata('updatestatus')[sid];
      $trans_id=$_REQUEST['TransactionID'];
       //echo "____";
//p($_REQUEST['ResponseCode']);
         //saving paymentgetway response
          $serlisedata=serialize($_REQUEST);
          //p($serlisedata);
          //$update_data = array('pg_response' =>$serlisedata);  
          //$this->db->where('t_id', $t_id);  
          //$this->db->update('tbl_transaction_map', $update_data);
         $this->db->query("UPDATE  tbl_transaction_map SET  pg_response ='".$serlisedata."',`transaction_id` =  '".$trans_id."' WHERE  t_id ='".$t_id1."' LIMIT 1");
         $this->db->query("UPDATE  tbl_transaction_storewise SET  pg_response ='".$serlisedata."',`transaction_id` =  '".$trans_id."' WHERE  t_sid ='".$s_id."' LIMIT 1");
         //end here  //saving paymentgetway response
         $param = array(
                      'act_mode' => 'get_transection', 
                      'row_id' => $t_id1,
                      
                    );             
         $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
         
        

         $viewdata['from_date']=$row->from_date;
         $viewdata['to_date']=$row->to_date;
         $viewdata['pay_amt']=$_REQUEST['Amount'];//$row->pay_amt;
         $viewdata['transaction_id']=$trans_id;
         
         $usertype=$this->session->userdata('popcoin_login')->s_usertype;
         $viewdata['usertype']=$usertype;
         
         
         
         if ($usertype==2)//retailer 
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_retailer', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
            
            $viewdata['rs_id']=$row->s_admin_id;
            $viewdata['rs_name']=$row->company_name;
            //pend($data);
          }
          elseif ($usertype==4)//store
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_store', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);

           
            $viewdata['rs_id']=$row->store_id;
            $viewdata['rs_name']=$row->s_companyname;
          }
          else {

          }


       /**pavan calculation*/
          $alldata=$this->session->userdata('updatestatus')[all];
          $int=$this->session->userdata('updatestatus')[intamt];
          $arears=$this->session->userdata('updatestatus')[arrears]; 
         //p($alldata);
        // exit;
          /*$alldata=$_REQUEST['all'];
          $int=$_REQUEST['intamt'];
          $arears=$_REQUEST['arears'];
*/
          $ab=explode('#', $alldata);
          $inte=explode('#', $int);

          //p($ab);exit;

          $rec='0';
          $pay='0';

          foreach ($ab as $key => $value1) {
             // p($value);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[2]=='Receivables')
            {
                $rec=$rec+$ab1[1]+$payamt;
            }

            if($ab1[2]=='Payables')
            {
                $pay=$pay+$ab1[1];
            }

          }
         $pa=$pay-$rec;

          foreach ($ab as $key => $value) {
              //p($value);
               $ab1=explode(',', $value);
         /* p($ab1[0]);
          p($ab1[1]);
          p($ab1[2]);
          p($ab1[3]);
          p($ab1[4]);
          p($ab1[5]);
          p($ab1[6]);*/
          // p($ab1[2]);
           //echo $rec.'___'.$pay;

          

           $t_id=$ab1[6];

           $param2 = array(
                    'act_mode' => 'fetchtransdata_store', 
                    'row_id' => $t_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
       //p($data);

          $updateamt=0;

                $param22 = array(
                    'act_mode' => 'updatetransmapdata_store', 
                    'row_id' => $t_id,
                    'param1' => $data->txn_id,
                    'param2' => '',
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_id,
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => $data->pay_type,
                    'param9' => $updateamt,
                    'param10' => $arears,
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => '',
                    'param14' => $inte[$key],
                    'param15' => '0',
                    'param16' => '1',
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest
                  );  

//p($param22);
           $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param22);
           //p($res2); 

       foreach($res2 as $val) {  

           $param = array(
                  'act_mode' => 'updatetranssuccessdata_store', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->pay_amt,
                  'param17' => $val->txn_id_point,
                  'param18' => $val->update_amt
                );
                //p($param);  
           $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
          
               
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->update_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res1 = $this->supper_admin->call_procedure('proc_procedure4', $param);
      $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$val->update_amt;
      #file_get_contents($no_url);
            }
          }      
          
      
        }


             
       $this->load->view('transaction/payment_success',$viewdata);
      }
     else{

        echo 'payment Failed';

        $serlisedata=serialize($_REQUEST);
         $this->db->query("UPDATE  tbl_transaction_map SET  pg_response =".$serlisedata.",payment_status =  'Failed' WHERE  t_id ='".$t_id1."' LIMIT 1");

         $this->db->query("UPDATE  tbl_transaction_storewise SET  pg_response ='".$serlisedata.",payment_status =  'Failed' WHERE  t_sid ='".$s_id."' LIMIT 1");
     }  
      
  }


  public function payment_response_status_check()
{
    //$t_id=$_REQUEST["address1"];
    $t_id1=$this->session->userdata('updatestatus')[tid];
    $s_id=$this->session->userdata('updatestatus')[sid];
       
    $this->db->query("UPDATE  tbl_transaction_map SET  pg_response ='".json_encode($_POST)."',`payment_status` =  '".$_REQUEST["status"]."' WHERE  t_id ='".$t_id1."' LIMIT 1");
    $this->db->query("UPDATE  tbl_transaction_storewise SET  pg_response ='".json_encode($_POST)."',`payment_status` =  '".$_REQUEST["status"]."' WHERE  t_sid ='".$s_id."' LIMIT 1");
         
         $param = array(
                      'act_mode' => 'get_transection', 
                      'row_id' => $t_id1,
                      
                    );             
         $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
         
        

         $viewdata['from_date']=$row->from_date;
         $viewdata['to_date']=$row->to_date;
         $viewdata['pay_amt']=$_REQUEST['amount'];//$row->pay_amt;
         $viewdata['transaction_id']=$_REQUEST['txnid'];
         
         $usertype=$this->session->userdata('popcoin_login')->s_usertype;
         $viewdata['usertype']=$usertype;
         
         
         
         if ($usertype==2)//retailer 
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_retailer', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
            
            $viewdata['rs_id']=$row->retailer_codeid;
            $viewdata['rs_name']=$row->s_username;
            //pend($data);
          }
          elseif ($usertype==4)//store
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_store', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);

           
            $viewdata['rs_id']=$row->s_storeunid;
            $viewdata['rs_name']=$row->s_name;
          }
          else {

          }

        // p($_POST);exit;
          /* ----------------- Process Start --------------------- */


          if($this->session->userdata('updatestatus')[rem_bal_id]=='0')
          {
              $rem_bal=0;
          }
          else
          {
              $rem_bal=$this->session->userdata('updatestatus')[rem_bal];

            $this->db->query("UPDATE tbl_transaction_storewise SET remaining_bal_status=2 WHERE t_sid =".$this->session->userdata('updatestatus')[rem_bal_id]." LIMIT 1");
          }
  
          $dis=0;
          $normalamt=0;
          $mode='Online';
          $refid=$_REQUEST['txnid'];
          $tradate=date('d-m-Y');
          $pay_amt=$_REQUEST['amount'];
          //$pay_amt=910.65;
          $total_amt=number_format($this->session->userdata('updatestatus')[total_amt],2);
          $intamt=number_format($this->session->userdata('updatestatus')[tinterest],2);
          $intamt1=$this->session->userdata('updatestatus')[intamt];
          $arears=number_format($this->session->userdata('updatestatus')[arrears],2);

          //$tamt=$total_amt+$intamt+$arears;
          $tamt=$total_amt;
          $pay_amt1=$pay_amt+$rem_bal+$dis;
          
         
          $alldata=$this->session->userdata('updatestatus')[all];
          $ab=explode('#', $alldata);
          $inte=explode('#', $intamt1);


         
           /* ----------------------------------- Start ----------------------------------------------------*/


          $rec='0';
          $pay='0';
          $len=0;
          foreach ($ab as $key => $value1) {
             // p($value);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[2]=='Receivables')
            {
                $rec=$rec+$ab1[1]+$payamt;
                $len++;
            }

            if($ab1[2]=='Payables')
            {
                $pay=$pay+$ab1[1];
            }

          }
        

        $samt=0;
        $inter=0;
        $arr=0;
        $arr1=0;
        $disc=0;
          foreach ($ab as $key => $value) 
          { # start for each
             //p($value);
               $ab1=explode(',', $value);
       if($ab1[2]=='Receivables') #main
       {
          $inter=$inter+$inte[$key];
          $pay_type=$ab1[2];
          if($samt==0)
          {
              
              $t_paid=($pay_amt-$intamt)+$rem_bal+$pay+$dis;
              $tamt1=$ab1[1]+$intamt+$arears;
              $arr=0;
              $arr1=$ab1[1];
              if($pay_amt1 > $tamt1)
              {
                $queue=1;
                $samt=$samt+($t_paid-$ab1[1]);
                $remaining_balance=$samt;
                $amount_paid=$ab1[1];
                $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                if($len==1)
                {
                  $amt=$pay_amt;
                }
                else
                {
                  $amt=0;
                }
                
              }
              else
              {
                  if($t_paid > $ab1[1])
                  {
                    $queue=2;
                    $samt=$samt+($t_paid-$ab1[1]);
                    $remaining_balance=$samt;
                    $amount_paid=$ab1[1];
                    $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                    if($len==1)
                    {
                      $amt=$pay_amt;
                    }
                    else
                    {
                      $amt=0;
                    }
                  }
                  else
                  {
                      $queue=3;
                      $samt=$samt+$t_paid;
                      $remaining_balance=0;
                      $amount_paid=$samt;
                      $paidper=100-(($ab1[1]-$amount_paid)*100/$ab1[1]);
                      if($len==1)
                      {
                        $amt=$pay_amt;
                      }
                      else
                      {
                        $amt=0;
                      }
                  }
                  
              }
          }
          else
          {
              $arr=$arr+$arr1;
              $arr1=$arr1+$ab1[1];
              if($samt > $total_amt)
              {
                $queue=4;
                $samt=$samt-$ab1[1];
                $remaining_balance=$samt;
                $amount_paid=$ab1[1];
                $paidper=100-(($ab1[1]-$amount_paid)*100/$ab1[1]);
                for($i=1; $i <= $len; $i++) 
                { 
                   if($i==$len)
                   {
                      $amt=$pay_amt;
                   } 
                   else
                   {
                      $amt=0;
                   }
                }
              }
              else
              {
                  //$samt=0;
                  $queue=5;
                  $remaining_balance=0;
                  $amount_paid=$samt;
                  $paidper=100-(($ab1[1]-$amount_paid)*100/$ab1[1]);
                  $amt=$pay_amt;
              }
          }

          if ($disc == 0) 
          {
            if($len==1)
            {
              $discount=$dis; 
              $remain_balance=$samt;   
            } 
            else 
            {
              $discount=0;
              $remain_balance=0;  
            }
          } 
          else if ($disc == $len -1) 
          {
            $discount=$dis;
            $remain_balance=$samt;  
          }
          else
          {
            $discount=0;
            $remain_balance=0;  
          }
          $disc++;

        }
        else #main
        {
            $inter=0;
            $queue=6;
            $discount=0;
            $pay_type=$ab1[2];
            $remaining_balance=0;
            $amount_paid=$ab1[1];
            $paidper=100;
            $t_paid=$ab1[1];
        }
      
        $t_id=$ab1[9];
        $store_idd=$this->session->userdata('updatestatus')[storeid];
        $txn_idd=$ab1[4];

         $en=$alldata.'_'.$intamt1.'_'.date('d-m-Y h:i:s');
          $this->db->query("UPDATE  tbl_transaction_map SET arears =".$arears.",pay_updation ='".$en."', remain_intamt =".$pay_amt.",mode_of_pay ='".$mode."' WHERE t_id ='".$t_id."' LIMIT 1");

          $this->db->query("UPDATE  tbl_transaction_storewise SET arears =".$arr.",pay_updation ='".$en."', remain_intamt =".$pay_amt.",mode_of_pay ='".$mode."' WHERE txn_id ='".$txn_idd."' LIMIT 1");

         $testparam = array('store_idd' => $store_idd,
                    'txn_idd' => $txn_idd,
                    'row_id' => $t_id,
                    'amount' => $ab1[2],
                    'rem_bal' => $samt,
                    'arears' => $arears,
                    'tamt' => $tamt1,
                    'remaining_balance' => $remaining_balance,
                    'amount_paid' => $amount_paid,
                    'pay_amt' => $pay_amt,
                    'paidper' => $paidper,
                    'pay_type' => $pay_type,
                    'queue' => $queue,
                    'discount' =>$dis,
                    'aamt' =>$amt
                  );  
       // p($testparam);    

    //} exit;{
 /* ----------------------------------- end ----------------------------------------------------*/

          $param22 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,//$_REQUEST['t_id'],
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
                            
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param22);

          $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,//$_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $data->update_amt-$amount_paid,
                    'param10' => $txn_idd,//$_REQUEST['txn_id'],
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $intamt,//$intamt1,
                    'param15' => $arears,//$intamt_paid,
                    'param16' => $store_idd,//$_REQUEST['str_id'],
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest,
                    'param21' => $refid,
                    'param22' => $tradate,
                    'param23' => $discount,
                    'param24' => $total_amt,
                    'param25' => $mode,
                    'param26' => $amt,#$pay_amt,#$normalamt,
                    'param27' => $pay_amt,
                    'param28' => $remaining_balance,
                    'param29' => $tamt,
                    'param30' => ''
                  );  


         // p($param2);
        $res2 = $this->supper_admin->call_procedure('proc_manualpayment', $param2);  

           $param22 = array(
                    'act_mode' => 'fetchtransdata_store', 
                    'row_id' => $store_idd,
                    'param1' => $txn_idd,
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param21' => '',
                    'param22' => '',
                    'param23' => '',
                    'param24' => '',
                    'param25' => '',
                    'param26' => '',
                    'param27' => '',
                    'param28' => '',
                    'param29' => '',
                    'param30' => ''
                  );  
                            
          $data1 = $this->supper_admin->call_procedureRow('proc_manualpayment', $param22);
//p($data1);
          if($data1->remaining_amt=="") {
            
            //$remaining=number_format($data1->pay_amt)-number_format($amount_paid);
            $remaining=$data1->pay_amt-$amount_paid;
            
          } else {
            
            $remaining=$data1->remaining_amt-$amount_paid;

          }   

         

            $paramstore = array(
                    'act_mode' => 'updatetransmapdata_store', 
                    'row_id' => $data1->t_sid,
                    'param1' => $data1->txn_id,
                    'param2' => '',
                    'param3' => $data1->retailer_id,
                    'param4' => $data1->store_id,
                    'param5' => $data1->pay_type,
                    'param6' => $data1->pay_amt,
                    'param7' => '',
                    'param8' => '',
                    'param9' => $remaining,
                    'param10' => '',
                    'param11' => 'success',
                    'param12' => '',
                    'param13' => '',
                    'param14' => $inter,#$intamt,
                    'param15' => $arr,#$arears,
                    'param16' => '',
                    'param17' => $data1->pay_permission,
                    'param18' => $data1->mpc_interest,
                    'param21' => $refid,
                    'param22' => $tradate,
                    'param23' => $discount,
                    'param24' => $tamt1,
                    'param25' => $mode,
                    'param26' => $pay_amt,#$normalamt,
                    'param27' => $pay_amt,
                    'param28' => $remaining_balance,
                    'param29' => $pay_amt+$remaining,
                    'param30' => ''
                  );  
//p($paramstore);
//} exit;{

          $res_store = $this->supper_admin->call_procedure('proc_manualpayment', $paramstore);  

            
          foreach($res2 as $val) { //4 for each
            if($val->process_amt=='')
            {
                $process_amt=($val->pay_amt*$paidper)/100;
            }
            else
            {
                $a=$val->pay_amt-$val->process_amt;
                $process_amt=($a*$paidper)/100;
            }
            
            $param = array(
                  'act_mode' => 'updatetranssuccessdata_store', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt+$val->process_amt,
                  'param17' => 'success',
                  'param18' => $paidper,
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
           $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);           
            
            if($val->user_type=='C') { //5
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => '',
                  'param18' => '',
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);   
                           
       $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$val->update_amt;
      #file_get_contents($no_url);
            } //5
          } //4 foreach    
          
        

       } #foreach loop

     /* ------------------------ End -----------------------*/  

       $this->load->view('transaction/payment_success',$viewdata);

     
      
  }

  public function payment_response_status_check_17june()
  {
     $t_id1=$this->session->userdata('updatestatus')[tid];//$_REQUEST['MerchantRefNo'];
      $s_id=$this->session->userdata('updatestatus')[sid];

    if (isset($_REQUEST['ResponseCode']) && $_REQUEST['ResponseCode'] == 0) {
         $viewdata=array();
      //$t_id=$this->session->userdata('updatestatus')[tid];//$_REQUEST['MerchantRefNo'];
     // $s_id=$this->session->userdata('updatestatus')[sid];
      $trans_id=$_REQUEST['TransactionID'];
       //echo "____";
//p($_REQUEST['ResponseCode']);
         //saving paymentgetway response
          $serlisedata=serialize($_REQUEST);
          //p($serlisedata);
          //$update_data = array('pg_response' =>$serlisedata);  
          //$this->db->where('t_id', $t_id);  
          //$this->db->update('tbl_transaction_map', $update_data);
         $this->db->query("UPDATE  tbl_transaction_map SET  pg_response ='".$serlisedata."',`transaction_id` =  '".$trans_id."' WHERE  t_id ='".$t_id1."' LIMIT 1");
         $this->db->query("UPDATE  tbl_transaction_storewise SET  pg_response ='".$serlisedata."',`transaction_id` =  '".$trans_id."' WHERE  t_sid ='".$s_id."' LIMIT 1");
         //end here  //saving paymentgetway response
         $param = array(
                      'act_mode' => 'get_transection', 
                      'row_id' => $t_id1,
                      
                    );             
         $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
         
        

         $viewdata['from_date']=$row->from_date;
         $viewdata['to_date']=$row->to_date;
         $viewdata['pay_amt']=$_REQUEST['Amount'];//$row->pay_amt;
         $viewdata['transaction_id']=$trans_id;
         
         $usertype=$this->session->userdata('popcoin_login')->s_usertype;
         $viewdata['usertype']=$usertype;
         
         
         
         if ($usertype==2)//retailer 
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_retailer', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
            
            $viewdata['rs_id']=$row->retailer_codeid;
            $viewdata['rs_name']=$row->s_username;
            //pend($data);
          }
          elseif ($usertype==4)//store
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_store', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);

           
            $viewdata['rs_id']=$row->s_storeunid;
            $viewdata['rs_name']=$row->s_name;
          }
          else {

          }


          if($this->session->userdata('updatestatus')[rem_bal_id]=='0')
          {
              $rem_bal=0;
          }
          else
          {
              $rem_bal=$this->session->userdata('updatestatus')[rem_bal];

            $this->db->query("UPDATE tbl_transaction_storewise SET remaining_bal_status=2 WHERE t_sid =".$this->session->userdata('updatestatus')[rem_bal_id]." LIMIT 1");
          }
  
          $dis=0;
          $normalamt=0;
          $mode='Online';
          $refid=$_REQUEST['TransactionID'];
          $tradate=date('d-m-Y');
          $pay_amt=$_REQUEST['Amount'];
          $total_amt=$this->session->userdata('updatestatus')[total_amt];
          $intamt=$this->session->userdata('updatestatus')[tinterest];
          $intamt1=$this->session->userdata('updatestatus')[intamt];
          $arears=$this->session->userdata('updatestatus')[arrears];

          $tamt=$total_amt+$intamt+$arears;
          $pay_amt1=$pay_amt+$rem_bal;
          



          $alldata=$this->session->userdata('updatestatus')[all];
          $ab=explode('#', $alldata);

         
         /* ----------------------------------- Start ----------------------------------------------------*/


          $rec='0';
          $pay='0';

          foreach ($ab as $key => $value1) {
             // p($value);
               $ab1=explode(',', $value1);

             
        
            if($ab1[2]=='Receivables')
            {
               // $rec=$rec+$ab1[2]+$payamt;
            }

            if($ab1[2]=='Payables')
            {
                $pay=$pay+$ab1[1];
            }

          }
         $pa=$pay-$rec;

        $samt=0;
          foreach ($ab as $key => $value) 
          { # start for each
             //p($value);
               $ab1=explode(',', $value);
       if($ab1[2]=='Receivables') #main
       {
          $pay_type=$ab1[2];
          if($samt==0)
          {
              
              $t_paid=($pay_amt-$intamt)+$rem_bal+$pay;
              $tamt1=$ab1[1]+$intamt+$arears;
              if($pay_amt1 > $tamt1)
              {
                $queue=1;
                $samt=$samt+($t_paid-$ab1[2]);
                $remaining_balance=$samt;
                $amount_paid=$ab1[1];
                $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                
              }
              else
              {
                  if($t_paid > $ab1[1])
                  {
                    $queue=2;
                    $samt=$samt+($t_paid-$ab1[1]);
                    $remaining_balance=$samt;
                    $amount_paid=$ab1[1];
                    $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                  }
                  else
                  {
                      $queue=3;
                      $samt=$samt+$t_paid;
                      $remaining_balance=0;
                      $amount_paid=$samt;
                      $paidper=100-(($ab1[1]-$amount_paid)*100/$ab1[1]);
                  }
                  
              }
          }
          else
          {
              if($samt > $total_amt)
              {
                $queue=4;
                $samt=$samt-$ab1[1];
                $remaining_balance=$samt;
                $amount_paid=$ab1[1];
                $paidper=100-(($ab1[1]-$amount_paid)*100/$ab1[1]);
              }
              else
              {
                  //$samt=0;
                  $queue=5;
                  $remaining_balance=0;
                  $amount_paid=$samt;
                  $paidper=100-(($ab1[1]-$amount_paid)*100/$ab1[1]);
              }
          }

        }
        else #main
        {
            $queue=6;
            $pay_type=$ab1[2];
            $remaining_balance=0;
            $amount_paid=$ab1[1];
            $paidper=100;
            $t_paid=$ab1[1];
        }
         
        $t_id=$ab1[9];
        $store_idd=$this->session->userdata('updatestatus')[storeid];
        $txn_idd=$ab1[4];

         $en=$alldata.'_'.$intamt1.'_'.date('d-m-Y h:i:s');
          $this->db->query("UPDATE  tbl_transaction_map SET arears =".$arears.",pay_updation ='".$en."', remain_intamt =".$pay_amt.",mode_of_pay ='Manual_".$mode."' WHERE t_id ='".$t_id."' LIMIT 1");

          $en=$alldata.'_'.$intamt1.'_'.date('d-m-Y h:i:s');
          $this->db->query("UPDATE  tbl_transaction_storewise SET arears =".$arears.",pay_updation ='".$en."', remain_intamt =".$pay_amt.",mode_of_pay ='Manual_".$mode."' WHERE txn_id ='".$txn_idd."' LIMIT 1");

         $testparam = array('store_idd' => $store_idd,
                    'txn_idd' => $txn_idd,
                    'row_id' => $t_id,
                    'amount' => $ab1[1],
                    'rem_bal' => $samt,
                    'arears' => $arears,
                    'tamt' => $tamt1,
                    'remaining_balance' => $remaining_balance,
                    'amount_paid' => $amount_paid,
                    'pay_amt' => $pay_amt,
                    'paidper' => $paidper,
                    'pay_type' => $pay_type,
                    'queue' => $queue
                  );  
        //p($testparam);    

    //} exit;{
 /* ----------------------------------- end ----------------------------------------------------*/
          $param22 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,//$_REQUEST['t_id'],
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
                            
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param22);

          $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,//$_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $data->update_amt-$amount_paid,
                    'param10' => $txn_idd,//$_REQUEST['txn_id'],
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $intamt,//$intamt1,
                    'param15' => $arears,//$intamt_paid,
                    'param16' => $store_idd,//$_REQUEST['str_id'],
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest,
                    'param21' => $refid,
                    'param22' => $tradate,
                    'param23' => $dis,
                    'param24' => $total_amt,
                    'param25' => 'manual_'.$mode,
                    'param26' => $normalamt,
                    'param27' => $pay_amt,
                    'param28' => $remaining_balance,
                    'param29' => $tamt,
                    'param30' => ''
                  );  


          //p($param2);
        $res2 = $this->supper_admin->call_procedure('proc_manualpayment', $param2);  

           $param22 = array(
                    'act_mode' => 'fetchtransdata_store', 
                    'row_id' => $store_idd,
                    'param1' => $txn_idd,
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param21' => '',
                    'param22' => '',
                    'param23' => '',
                    'param24' => '',
                    'param25' => '',
                    'param26' => '',
                    'param27' => '',
                    'param28' => '',
                    'param29' => '',
                    'param30' => ''
                  );  
                            
          $data1 = $this->supper_admin->call_procedureRow('proc_manualpayment', $param22);
//p($data1);
          if($data1->remaining_amt=="") {
            
            //$remaining=number_format($data1->pay_amt)-number_format($amount_paid);
            $remaining=$data1->pay_amt-$amount_paid;
            
          } else {
            
            $remaining=$data1->remaining_amt-$amount_paid;

          }   

         

            $paramstore = array(
                    'act_mode' => 'updatetransmapdata_store', 
                    'row_id' => $data1->t_sid,
                    'param1' => $data1->txn_id,
                    'param2' => '',
                    'param3' => $data1->retailer_id,
                    'param4' => $data1->store_id,
                    'param5' => $data1->pay_type,
                    'param6' => $data1->pay_amt,
                    'param7' => '',
                    'param8' => '',
                    'param9' => $remaining,
                    'param10' => '',
                    'param11' => 'success',
                    'param12' => '',
                    'param13' => '',
                    'param14' => $intamt,
                    'param15' => $arears,
                    'param16' => '',
                    'param17' => $data1->pay_permission,
                    'param18' => $data1->mpc_interest,
                    'param21' => $refid,
                    'param22' => $tradate,
                    'param23' => $dis,
                    'param24' => $tamt1,
                    'param25' => 'manual_'.$mode,
                    'param26' => $normalamt,
                    'param27' => $pay_amt,
                    'param28' => $remaining_balance,
                    'param29' => $pay_amt+$remaining,
                    'param30' => ''
                  );  
//p($paramstore);
//} exit;{

          $res_store = $this->supper_admin->call_procedure('proc_manualpayment', $paramstore);  

            
          foreach($res2 as $val) { //4 for each
            if($val->process_amt=='')
            {
                $process_amt=($val->pay_amt*$paidper)/100;
            }
            else
            {
                $a=$val->pay_amt-$val->process_amt;
                $process_amt=($a*$paidper)/100;
            }
            
            $param = array(
                  'act_mode' => 'updatetranssuccessdata_store', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt+$val->process_amt,
                  'param17' => 'success',
                  'param18' => $paidper,
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
           $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);           
            
            if($val->user_type=='C') { //5
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => '',
                  'param18' => '',
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);   
                           
       $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$val->update_amt;
      #file_get_contents($no_url);
            } //5
          } //4 foreach    
          
        

       } #foreach loop

     /* ------------------------ End -----------------------*/  

       $this->load->view('transaction/payment_success',$viewdata);

      }
     else
     {

        echo 'payment Failed';

        $serlisedata=serialize($_REQUEST);
         $this->db->query("UPDATE  tbl_transaction_map SET  pg_response =".$serlisedata.",payment_status =  'Failed' WHERE  t_id ='".$t_id1."' LIMIT 1");

         $this->db->query("UPDATE  tbl_transaction_storewise SET  pg_response ='".$serlisedata.",payment_status =  'Failed' WHERE  t_sid ='".$s_id."' LIMIT 1");
     }  
      
  }


  public function payment_response_satyendra()
  {
     
    if (isset($_REQUEST['ResponseCode']) && $_REQUEST['ResponseCode'] == 0) {
         $viewdata=array();
         $t_id=$_REQUEST['MerchantRefNo'];
         
         $trans_id=$_REQUEST['TransactionID'];

         //saving paymentgetway response
          $serlisedata=serialize($_REQUEST);
          
          //$update_data = array('pg_response' =>$serlisedata);  
          //$this->db->where('t_id', $t_id);  
          //$this->db->update('tbl_transaction_map', $update_data);
         #$this->db->query("UPDATE  tbl_transaction_map SET  `transaction_id` =  '".$serlisedata."' WHERE  t_id ='".$t_id."' LIMIT 1");
          $this->db->query("UPDATE  tbl_transaction_map SET  pg_response ='".$serlisedata."',`transaction_id` =  '".$trans_id."' WHERE  t_id ='".$t_id."' LIMIT 1");
         //end here  //saving paymentgetway response
         $param = array(
                      'act_mode' => 'get_transection', 
                      'row_id' => $t_id,
                      
                    );             
         $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
         
        

         $viewdata['from_date']=$row->from_date;
         $viewdata['to_date']=$row->to_date;
         $viewdata['pay_amt']=$row->pay_amt;
         $viewdata['transaction_id']=$trans_id;
         
         $usertype=$this->session->userdata('popcoin_login')->s_usertype;
         $viewdata['usertype']=$usertype;
         
         
         
         if ($usertype==2)//retailer 
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_retailer', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);
            
            $viewdata['rs_id']=$row->s_admin_id;
            $viewdata['rs_name']=$row->company_name;
            //pend($data);
          }
          elseif ($usertype==4)//store
          {
            $id=$this->session->userdata('popcoin_login')->s_admin_id;
            $param=array(
                      'act_mode' =>'get_store', 
                      'row_id' => $id,
                      );
           
            $row= $this->supper_admin->call_procedureRow('proc_payment', $param);

           
            $viewdata['rs_id']=$row->store_id;
            $viewdata['rs_name']=$row->s_companyname;
          }
          else {

          }
       /**pavan calculation*/
                               $alldata='';
                              $int='';
                              $arears=''; 
         $i=0;
                                foreach ($amountdetails as $amount) { $i++; //p($amount->retailer_id);
                                  if($amount->curr_status==1){   
                                    

                                      $ab=explode('#', $amount->cc);
                                      //p($ab);
                                      $rec=0;
                                      $pay=0;
                                      $rec_in=0;
                                      $pay_in=0;
                                      $str=0;
                                      $interest1='';

                                      foreach ($ab as $key => $value) {
                                          //p($value);
                                           $ab1=explode(',', $value);
                                      /*p($ab1[0]);
                                      p($ab1[1]);
                                      p($ab1[2]);
                                      p($ab1[3]);
                                      p($ab1[4]);
                                      p($ab1[5]);
                                      p($ab1[6]);*/
                                      $str=$ab1[0];
                                      $cycle_id=$ab1[6];
                                      //$str=$str+$ab1[0];
                                       $mpc_interset=$ab1[8];
                                      $pay_permission=$ab1[9];
                                       $store_idd=$ab1[11];

                                        $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[2];
                                        $pdate=$ab1[4];
                                        $date1 = new DateTime($pdate);
                                        $date2 = new DateTime($curdate);
                                        $diff = $date2->diff($date1)->format("%a");
                             //echo $diff;
                                        if($curdate<=$pdate)
                                        {
                                            $intamt='0';
                                            $interest1 =$interest1.$intamt.'#';
                                           
                                        }
                                        else
                                        {
                                            $intamt1=($ab1[2]*$mpc_interset)/100;
                                            $intamt=round((($intamt1*$diff)/365),4);
                                            $interest1 =$interest1.$intamt.'#';
                                           
                                        }



                                      if($ab1[3]=='Receivables')
                                      {
                                          /*p($ab1[0]);
                                          p($ab1[1]);
                                          p($ab1[2]);
                                          p($ab1[3]);
                                          p($ab1[4]);
                                          p($ab1[5]);
                                          p($ab1[6]);*/
                                          $newamtrec =$ab1[2];
                                          //$newamtrec =$ab1[10];
                                          $newrec_ref =$ab1[5];
                                          $mpc_intersetr=$ab1[8];
                                          

                                $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[2];
                                $pdate=$ab1[1];
                                $date1 = new DateTime($pdate);
                                $date2 = new DateTime($curdate);
                                $diff = $date2->diff($date1)->format("%a");
                     // echo $diff;
                                if($curdate<=$pdate)
                                {
                                    $intamt='0';
                                    $rec_in=$rec_in+$intamt;
                                    $rec =$rec+$ab1[2];
                                }
                                else
                                {
                                    $intamt1=($ab1[2]*$mpc_intersetr)/100;
                                    $intamt=round((($intamt1*$diff)/365),4);

                                    //echo $intamt1;
                                    $rec_in=$rec_in+$intamt;
                                    $rec =$rec+$ab1[2];
                                }
                                      }
                                      if($ab1[3]=='Payables')
                                      {
                                          /*p($ab1[0]);
                                          p($ab1[1]);
                                          p($ab1[2]);
                                          p($ab1[3]);
                                          p($ab1[4]);
                                          p($ab1[5]);
                                          p($ab1[6]);*/
                                           $newamtpay =$ab1[2];
                                           //$newamtpay =$ab1[10];
                                           $newpay_ref =$ab1[5];
                                           $mpc_intersetp=$ab1[8];
                                         

                                $curdate=date('Y-m-d h:i:s');
                                //$pdate=$ab1[2];
                                $pdate=$ab1[1];
                                $date1 = new DateTime($pdate);
                                $date2 = new DateTime($curdate);
                                $diff = $date2->diff($date1)->format("%a");
                     
                                if($curdate<=$pdate)
                                {
                                    $intamt='0';
                                    $pay_in=$pay_in+$intamt;
                                    $pay =$pay+$ab1[2];
                                }
                                else
                                {
                                    $intamt1=($ab1[2]*$mpc_intersetp)/100;
                                    $intamt=round((($intamt1*$diff)/365),4);
                                    //echo $intamt;
                                    $pay_in=$pay_in+$intamt;
                                    $pay =$pay+$ab1[2];
                                }


                                      }
                                      }   
 $str;  

$m=date("m", strtotime($amount->t_createdon));
$y=date("Y", strtotime($amount->t_createdon));

                                      if(empty($newamtpay)) $newamtpay='0'; else $newamtpay=$newamtpay;
                                       
                                            
                                            
                                     if(empty($newamtrec)) $newamtrec='0'; else $newamtrec=$newamtrec;
                                      
                                      
                                           
                                            $ret=$rec+$rec_in;
                                                        $pet=$pay+$pay_in;

                                                        $apa=round($pay-$newamtpay,4);
                                                        //$are=round($rec-$newamtrec,4)-$apa;
                                                        $are=$apa-round($rec-$newamtrec,4);
                                                      
                          
                                            $are; 
                      
                   $int=$pay_in-$rec_in+$interest1;  
                      
                    
                         
                                            $amt=($newamtpay-$newamtrec)+$are+$int; 
                                            
                                             
                                          
                                               $amount->txn_id;  
                 

                            echo $are;  
                            echo '<br>';      
                             echo $amount->cc;  
                              echo '<br>';     
                             echo $interest1; 

          $alldata=$amount->cc;
          $int=$interest1;
          $arears=$are;        

                                          
                                          
   } } //foreach ($amountdetails as $amount) { $i++; //p($amount->retailer_id);
         

          $ab=explode('#', $alldata);
          $inte=explode('#', $int);

          $rec='0';
          $pay='0';

          foreach ($ab as $key => $value1) {
              //p($value);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[3]=='Receivables')
            {
                $rec=$rec+$ab1[2]+$payamt;
            }

            if($ab1[3]=='Payables')
            {
                $pay=$pay+$ab1[2];
            }

          }
         $pa=$pay-$rec;

          foreach ($ab as $key => $value) {
              //p($value);
               $ab1=explode(',', $value);
       
      
         /** pawan calculation end here*/  
         $param2 = array(
                      'act_mode' => 'fetchtransdata', 
                      'row_id' => $t_id,
                      'param1' => '',
                      'param2' => '',
                      'param3' => '',
                      'param4' => '',
                      'param5' => '',
                      'param6' => '',
                      'param7' => '',
                      'param8' => '',
                      'param9' => '',
                      'param10' => '',
                      'param11' => '',
                      'param12' => '',
                      'param13' => '',
                      'param14' => '',
                      'param15' => '',
                      'param16' => '',
                      'param17' => '',
                      'param18' => ''
                    );             
            $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
             //pend($data);

            if($data->pay_type=='Receivables')
            {
                $updateamt=0;
            }
            else
            {
              $updateamt=0;
                
            }
        $param22 = array(
                      'act_mode' => 'updatetransmapdata', 
                      'row_id' => $t_id,
                      'param1' => $data->txn_id,
                      'param2' => $data->cycle_id,
                      'param3' => $data->retailer_id,
                      'param4' => $data->store_count,//$data->store_id,
                      'param5' => $data->from_date,
                      'param6' => $data->to_date,
                      'param7' => $data->pay_date,
                      'param8' => $data->pay_type,
                      'param9' => $updateamt,
                      'param10' => $data->txn_id,
                      'param11' => 'success',
                      'param12' => $data->pay_amt,
                      'param13' => $data->cycle_day,
                      'param14' => $inte[$key],
                      'param15' => $arears,
                      'param16' => $ab1[11],
                      'param17' => $data->pay_permission,
                      'param18' => $data->mpc_interest
                    );  

      $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param22);
      foreach($res2 as $val) {  

             $param = array(
                    'act_mode' => 'updatetranssuccessdata_mpc', 
                    'row_id' => $val->t_id,
                    'param1' => $val->txn_id,
                    'param2' => $val->tran_map_id,
                    'param3' => $val->cycle_id,
                    'param4' => $val->receipts_id,
                    'param5' => $val->order_id,
                    'param6' => $val->retailer_id,
                    'param7' => $val->store_id,
                    'param8' => $val->deal_id,
                    'param9' => $val->user_id,
                    'param10' => $val->user_type,
                    'param11' => $val->pay_type,
                    'param12' => $val->tax_type,
                    'param13' => $val->tax_amt,
                    'param14' => $val->total_amt,
                    'param15' => $val->pay_amt,
                    'param16' => $val->pay_amt,
                    'param17' => $val->txn_id_point,
                    'param18' => $val->update_amt
                  );
                  //p($param);  
             $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            
                 
              if($val->user_type=='C') {
                $param = array(
                    'act_mode' => 'insertcustomercashback_mpc', 
                    'row_id' => '',
                    'param1' => $val->txn_id,
                    'param2' => $val->tran_map_id,
                    'param3' => $val->cycle_id,
                    'param4' => $val->receipts_id,
                    'param5' => $val->order_id,
                    'param6' => $val->retailer_id,
                    'param7' => $val->store_id,
                    'param8' => $val->deal_id,
                    'param9' => $val->user_id,
                    'param10' => $val->user_type,
                    'param11' => $val->pay_type,
                    'param12' => $val->tax_type,
                    'param13' => $val->tax_amt,
                    'param14' => $val->total_amt,
                    'param15' => $val->pay_amt,
                    'param16' => $val->update_amt,
                    'param17' => '',
                    'param18' => ''
                  );             
                $res1 = $this->supper_admin->call_procedure('proc_procedure4', $param);
              }
            } 
          }//foreach ($ab as $key => $value) {
            //pend($viewdata);
        //$this->load->view('helper/header');      
       $this->load->view('transaction/payment_success',$viewdata);
      }/// end if (isset($_REQUEST['ResponseCode']) && $_REQUEST['ResponseCode'] == 0) 
     else{

        echo 'payment Failed';
     }  
      
  }
  public function paymentprocess() {
     
         $parameter = array(
                    'act_mode' => 'retailerpaymentprocess121', 
                    'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
        $mailsms = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);

        $parameter = array(
                    'act_mode' => 'getinterest', 
                    'row_id' => '',
                    'counname' => '',
                    'coucode' => '',
                    'commid' => ''
                  );
        $response['interest'] = $this->supper_admin->call_procedureRow('proc_master', $parameter);
//p($response['interest']);

        if(isset($_POST['save_data'])) {
          //pend($_REQUEST);

          $curdate=date('Y-m-d');
          $pdate=$_REQUEST['paydate'];
          $date1 = new DateTime($pdate);
          $date2 = new DateTime($curdate);
          $diff = $date2->diff($date1)->format("%a");


          $intamt=$_REQUEST['intamt'];
          $intamtbefore=$_REQUEST['intamtbefore'];
          $intval=$response['interest']->i_value*$diff;
          if($_REQUEST['pay_amt']>=$_REQUEST['payment'])
          {
              if($intamt==0)
              {
                 $check=0;
                 $amount_paid=$_REQUEST['payment'];
                 $intamt1='';
                 $intamt_paid='';
                 $amount=$_REQUEST['pay_amt'];
                 $paidper=100-(($amount-$amount_paid)*100/$amount);
              }
              else
              {
                 $check=0;
                 $amount_paid=$_REQUEST['payment'];

                  $paidint=$_REQUEST['payment'];
                  $int=$intamt+$intamtbefore;
                  if($int==$paidint)
                  {
                      $intamt1=round(((($amount_paid*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='0';
                  }
                  else
                  {
                     //$intamt1=(($amount_paid*$intval)/100)+$intamtbefore;
                     $intamt1=round(((($amount_paid*$intval)/100)/365),4)+$intamtbefore;
                     $intamt_paid='';
                  }  
                 $amount=$_REQUEST['pay_amt'];
                 $paidper=100-(($amount-$amount_paid)*100/$amount);
              }

          }
          else 
            //if($_REQUEST['pay_amt']<$_REQUEST['payment'])
          { //echo 'hi';
              if($_REQUEST['pay_amt']==0)
              {
                  $check=1;
                  $amount_paid=0;
                  //$intamt_paid=$_REQUEST['payment'];//102-100
                  $paidint=$_REQUEST['payment'];
                  $int=$intamt+$intamtbefore;
                  if($int==$paidint)
                  {
                      $check=1;
                      $amount_paid=0;
                      $intamt1=$int;
                      $intamt_paid='0';
                  }
                  else
                  {
                      $check=1;
                      $amount_paid=0;
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                  }
              }
              else
              {
                  $paidint=round(($_REQUEST['payment']-$_REQUEST['pay_amt']),4);
                  $int=$intamt+$intamtbefore;
                  //echo $int;
                  if($paidint=$int)
                  {  //echo 'hi1';
                      $check=0;
                      $amount_paid=$_REQUEST['pay_amt'];
                      $intamt1=$int;
                      $intamt_paid='0';
                      //$intamt_paid=$_REQUEST['payment']-$_REQUEST['pay_amt'];//102-100
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount_paid)*100/$amount);
                  }
                  else
                  { //echo 'hi11';
                      $check=0;
                      $amount_paid=$_REQUEST['pay_amt'];
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                      //$intamt_paid=$_REQUEST['payment']-$_REQUEST['pay_amt'];//102-100
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount_paid)*100/$amount);
                  }
              }
          }
          
          //echo $check;
          //echo $amount_paid;
         // echo $intamt_paid;
          
        
          $mapstatus = 'failed';
          //exit;
          $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $_REQUEST['t_id'],
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
                            
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);

          if($data->remaining_amt=="") {
            //$remaining=$data->pay_amt-$_REQUEST['payment'];
            $remaining=$data->pay_amt-$amount_paid;
          } else {
            //$remaining=$data->remaining_amt-$_REQUEST['payment'];
            $remaining=$data->remaining_amt-$amount_paid;
          }          
          $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $remaining,
                    'param10' => $_REQUEST['txn_id'],
                    'param11' => $mapstatus,
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $intamt1,
                    'param15' => $intamt_paid,
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
            //p($param2);exit;                  
            $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param2);
            //p($res2);
     if($check==0)//  Interest Amt   
     {
          foreach($res2 as $val) {
            if($val->process_amt=='')
            {
                $process_amt=($val->pay_amt*$paidper)/100;
            }
            else
            {
                $a=$val->pay_amt-$val->process_amt;
                $process_amt=($a*$paidper)/100;
            }
            
            $param = array(
                  'act_mode' => 'updatetranssuccessdata', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt+$val->process_amt,
                  'param17' => $mapstatus,
                  'param18' => $paidper//
                );  
                 //p($param);          
            $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            }
          }


          }//  Interest Amt    

          /*--------------------------SMS/EMAIL------------------------*/
          $email=$mailsms->s_loginemail;
          $mobile=$mailsms->contact_no;
          $amt=($mailsms->pay_amt)-$amount_paid;
           $msg=urlencode('DEAR MPC Partner, Pay for '.$mailsms->s_name.'-'.$mailsms->company_name.' is Pay Rs '.$amount_paid.' .RefID- '.$mailsms->txn_id.'. Pymt due Rs '.$amt);

         // $url='http://sms.tattler.co/app/smsapi/index.php?key=558709407EE8FE&routeid=288&type=text&contacts='.$mobile.'&senderid=WEBPRO&msg='.$msg; 
                $data11=SMSURL;
           $url=$data11.$mobile.'&message='.$msg;  
          #file_get_contents($url);
              //print_r(file_get_contents($url));

                  $content='<html>
                   <body bgcolor="#DCEEFC">
                          <br><div> 
                        <table><tr><td>DEAR MPC Partner, Pay for '.$mailsms->s_name.' ('.$mailsms->company_name.') is Pay Rs '.$amount_paid.' .RefID- '.$mailsms->txn_id.'. Pymt due Rs '.$amt.'.</td> </tr>
                          
                          <tr><td>Regards,</br>
                          MPC
                        
                          </table>
                         </div>
                   </body>
              </html>';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('support@mindzshop.com', 'Mypopcoins');
                  $this->email->to($email);
                  //$this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Pay Bill');
                  $this->email->message($content);
                  #$this->email->send();

          /*--------------------------SMS/EMAIL------------------------*/

          //exit;  
          //$this->session->set_flashdata('message', 'Your transaction has been successful - '.$_REQUEST['txn_id'].' (TXN ID)!');
          //redirect(base_url().'admin/tax/paymentprocess');
          redirect(base_url().'admin/receipts/paymentgateway/'.$_REQUEST['txn_id']);
      }


      $this->load->view('helper/header');      
      $this->load->view('pay_transaction',$response);
  }

 

  public function paymentprocessall() {
      
      
        $parameter = array(
                    'act_mode' => 'retailerpaymentprocess12', 
                    'v_id' => '',
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
     
      //p($response['amountdetails']);exit; 
      $this->load->view('helper/header');      
      $this->load->view('pay_transaction',$response);
  }


   public function mpc_paymentprocess() {
      
      
        $parameter = array(
                    'act_mode' => 'mpcpayment',//'retailerpaymentprocess12', 
                    'v_id' => '',
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
        //p($response['amountdetails']);

        

           if(!empty($this->input->post('pay_payment')))
        {


          #$finalExcelArr = array('S.No.','Settlement Cycle','Retailer/Store ID','Company/Store Name','No of Store','Receivables Amt.','Payables Amt.','Arears','Interest Amt.','Net Amt.','Beneficiary Account Number','Instrument Amount','Bank Transaction ID','Beneficiary Name','IFSC Code','Beneficiary Bank Name','Email ID','Contact No.');
          $finalExcelArr = array('S.No.','Pay Date','Retailer/Store ID','Company/Store Name','Mode of Payment','Beneficiary Account Number','Instrument Amount','Update Amt.','Bank Transaction ID','Beneficiary Name','IFSC Code','Beneficiary Bank Name','Email ID','Contact No.','Process I','Process II','Arrears','Interest');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Payment Details');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                      
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
            $ii=0;
            $ij=-1;
            foreach ($response['amountdetails'] as $key => $value) {  //$ii++; 


              /*----------------------------------------------------*/
              $ab=explode('#', $value->cc);
              //p($ab);
              $rec=0;
              $pay=0;
              $rec_in=0;
              $pay_in=0;
              $str=0;
              $interest1='';
              $first='';
              $second='';

              foreach ($ab as $va) {
                  //p($value);
                   $ab1=explode(',', $va);
             
              $str=$ab1[0];
              $first=$first.$ab1[7].',';
              $second=$second.$ab1[12].',';
              

              $mpc_interset=$ab1[9];
              $pay_permission=$ab1[10];

                $curdate=date('Y-m-d h:i:s');
        //$pdate=$ab1[2];
                $pdate=$ab1[4];
                $date1 = new DateTime($pdate);
                $date2 = new DateTime($curdate);
                $diff = $date2->diff($date1)->format("%a");
     //echo $diff;
                if($curdate<=$pdate)
                {
                    $intamt='0';
                    $interest1 =$interest1.$intamt.'#';
                   
                }
                else
                {
                    $intamt1=($ab1[2]*$mpc_interset)/100;
                    $intamt=round((($intamt1*$diff)/365),4);
                    $interest1 =$interest1.$intamt.'#';
                   
                }



              if($ab1[3]=='Receivables')
              {
                  
                  $newamtrec =$ab1[2];
                  $newrec_ref =$ab1[5];
                  
                  $mpc_intersetr=$ab1[9];

        $curdate=date('Y-m-d h:i:s');
        //$pdate=$ab1[2];
        $pdate=$ab1[1];
        $date1 = new DateTime($pdate);
        $date2 = new DateTime($curdate);
        $diff = $date2->diff($date1)->format("%a");
// echo $diff;
        if($curdate<=$pdate)
        {
            $intamt='0';
            $rec_in=$rec_in+$intamt;
            $rec =$rec+$ab1[2];
        }
        else
        {
            $intamt1=($ab1[2]*$mpc_intersetr)/100;
            $intamt=round((($intamt1*$diff)/365),4);

            //echo $intamt1;
            $rec_in=$rec_in+$intamt;
            $rec =$rec+$ab1[2];
        }
              }
              /* else
              {
                $newamtrec=0;
              }*/
              if($ab1[3]=='Payables')
              {
                  
                   $newamtpay =$ab1[2];
                   $newpay_ref =$ab1[5];
                   $mpc_intersetp=$ab1[9];
                 

        $curdate=date('Y-m-d h:i:s');
        //$pdate=$ab1[2];
        $pdate=$ab1[1];
        $date1 = new DateTime($pdate);
        $date2 = new DateTime($curdate);
        $diff = $date2->diff($date1)->format("%a");

        if($curdate<=$pdate)
        {
            $intamt='0';
            $pay_in=$pay_in+$intamt;
            $pay =$pay+$ab1[2];
        }
        else
        {
            $intamt1=($ab1[2]*$mpc_intersetp)/100;
            $intamt=round((($intamt1*$diff)/365),4);
            //echo $intamt;
            $pay_in=$pay_in+$intamt;
            $pay =$pay+$ab1[2];
        }


              }
               /*else
              {
                $newamtpay=0;
              }*/
              }  //echo $interest1.'___';
//echo $str;
              $apa=round($pay-$newamtpay,4);
              $are=round($rec-$newamtrec,4)-$apa;

              $int=$rec_in-$pay_in+$interest1;
              $amt=($newamtrec-$newamtpay)+$are+$int;
              $ret=$rec+$rec_in;
              $pet=$pay+$pay_in;
              if($ret < $pet)
              {
                  $amt1=$amt;
                  $continue=1;
                  $ii++; 
                  $ij++; 
              }
              else
              {
                  $amt1=0;
                  $continue=0;
              }
              /*----------------------------------------------------*/
           
            if($continue==1)
            {
               //$newvar = $j+$key;
             $newvar = $j+$ij;
            

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $ii);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, date("d-m-Y", strtotime($value->pay_date)));
            //$objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, date("d-m-Y", strtotime($value->from_date)).' To '.date("d-m-Y", strtotime($value->to_date)).' Pay Date '.date("d-m-Y", strtotime($value->pay_date)));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->s_name);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->company_name);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, 'NEFT/RTGS');
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->ahnumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, substr($amt1,1));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, substr($amt1,1));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, '');
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->ahname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->ifsc);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->bname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->email);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->contact);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, trim($first,','));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, trim($second,','));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[16].$newvar, '0');
            $objPHPExcel->getActiveSheet()->setCellValue($cols[17].$newvar, '0');
            
            }
                 
            }
          }
      
          $dt=date('d-m-Y h:i:s');
          $filename='PaymentDetails'.$dt.'.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
        }

       

        if(isset($_POST['settled_data'])) {
         // pend($_REQUEST);

          $alldata=$_REQUEST['all'];
          $int=$_REQUEST['intamt'];
          $arears=$_REQUEST['arears'];

          $ab=explode('#', $alldata);
          $inte=explode('#', $int);

          foreach ($ab as $key => $value) {
              //p($value);
               $ab1=explode(',', $value);
         /* p($ab1[0]);
          p($ab1[1]);
          p($ab1[2]);
          p($ab1[3]);
          p($ab1[4]);
          p($ab1[5]);
          p($ab1[6]);*/

           $t_id=$ab1[7];

           $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
        // p($data);

           if($ab1[11]!=0)
          {
              $updateamt=$data->update_amt-$payamt;
          }
          else
          {
              $updateamt=0;
          }

                $param22 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,//$data->store_id,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $updateamt,
                    'param10' => $data->txn_id,
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $inte[$key],
                    'param15' => $arears,
                    'param16' => $ab1[11],
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest
                  );  


           $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param22);
           //p($res2); 

         foreach($res2 as $val) {  

           $param = array(
                  'act_mode' => 'updatetranssuccessdata_mpc', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->pay_amt,
                  'param17' => $val->txn_id_point,
                  'param18' => $val->update_amt
                );
                //p($param);  
           $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
          
               
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->update_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res1 = $this->supper_admin->call_procedure('proc_procedure4', $param);

      $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$val->update_amt;
      #file_get_contents($no_url);


            }
          }       
          
      
        }
        //exit;
            redirect(base_url().'admin/transaction/mpc_paymentprocess');
        }


         if(isset($_POST['save_data'])) {
          //pend($_REQUEST);
          $alldata=$_REQUEST['all'];
          $int=$_REQUEST['intamt'];
          $arears=$_REQUEST['arears'];

          $ab=explode('#', $alldata);
          $inte=explode('#', $int);

          $rec='0';
          $pay='0';

          foreach ($ab as $key => $value1) {
              //p($value);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[3]=='Receivables')
            {
                $rec=$rec+$ab1[2]+$payamt;
            }

            if($ab1[3]=='Payables')
            {
                $pay=$pay+$ab1[2];
            }

          }
         $pa=$pay-$rec;

          foreach ($ab as $key => $value) {
              //p($value);
               $ab1=explode(',', $value);
         /* p($ab1[0]);
          p($ab1[1]);
          p($ab1[2]);
          p($ab1[3]);
          p($ab1[4]);
          p($ab1[5]);
          p($ab1[6]);*/
          // p($ab1[2]);
           //echo $rec.'___'.$pay;

          

           $t_id=$ab1[7];

           $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
        // p($data);
          if($ab1[11]!=0)
          {
              $updateamt=$data->update_amt-$payamt;
          }
          else
          {
              if($data->pay_type=='Receivables')
              {
                  $updateamt=0;
              }
              else
              {
                $updateamt=0;
               /* if(!empty($pa)){
                 if($data->update_amt>$pa){
                  $updateamt=$pa;
                  $pa=0;
                 }elseif ($data->update_amt<$pa) {
                   $updateamt=$data->update_amt;
                   $pa=$pa-$data->update_amt;
                 }else{
                   $updateamt=$data->update_amt;
                   $pa=0;
                 }
               }*/
                  
              }
        }

                $param22 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,//$data->store_id,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $updateamt,
                    'param10' => $data->txn_id,
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $inte[$key],
                    'param15' => $arears,
                    'param16' => $ab1[11],
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest
                  );  

//p($param22);
           $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param22);
           //p($res2); 

        foreach($res2 as $val) {  

           $param = array(
                  'act_mode' => 'updatetranssuccessdata_mpc', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->pay_amt,
                  'param17' => $val->txn_id_point,
                  'param18' => $val->update_amt
                );
                //p($param);  
           $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
          
               
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->update_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res1 = $this->supper_admin->call_procedure('proc_procedure4', $param);

      $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$val->update_amt;
      #file_get_contents($no_url);
            }
          }      
          
      
        }
        //exit;
       redirect(base_url().'admin/transaction/mpc_paymentprocess');
      }
      $this->load->view('helper/header');      
      $this->load->view('transaction/mpc_transaction',$response);
  }


     public function retailer_paymentprocess() {
      
      


        $parameter = array(
                    'act_mode' => 'retailerpayment',//'retailerpaymentprocess12', 
                    'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
     //p($response['amountdetails'][0]->payment_permission);
     //exit;

                      $ab=explode('#', $response['amountdetails'][0]->cc);
                      foreach ($ab as $key => $value) 
                      {
                           $ab1=explode(',', $value);
                           $store_idd=$ab1[11];
                     }
                    // p($store_idd);

        if($response['amountdetails'][0]->payment_permission==1)
        {
            $act_mode='remaining_balance';
            $idd=$this->session->userdata('popcoin_login')->s_admin_id;
        }
        else
        {
            $act_mode='remaining_balance_store';
            $idd=$store_idd;
        }

        $param = array(
                  'act_mode' => $act_mode,#'remaining_balance',
                  'row_id' => $idd,#$this->session->userdata('popcoin_login')->s_admin_id,
                  'param1' => '',
                  'param2' => '',
                  'param3' => '',
                  'param4' => '',
                  'param5' => '',
                  'param6' => '',
                  'param7' => '',
                  'param8' => '',
                  'param9' => '',
                  'param10' => '',
                  'param11' => '',
                  'param12' => '',
                  'param13' => '',
                  'param14' => '',
                  'param15' => '',
                  'param16' => '',
                  'param17' => '',
                  'param18' => '',
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $response['remaining_balance'] = $this->supper_admin->call_procedureRow('proc_manualpayment', $param); 
//p($response['remaining_balance']);


         if(isset($_POST['save_data'])) {
          //pend($_REQUEST);
          $alldata=$_REQUEST['all'];
          $int=$_REQUEST['intamt'];
          $arears=$_REQUEST['arears'];

          $ab=explode('#', $alldata);
          $inte=explode('#', $int);

          $rec='0';
          $pay='0';

          foreach ($ab as $key => $value1) {
              //p($value);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[3]=='Receivables')
            {
                $rec=$rec+$ab1[2]+$payamt;
            }

            if($ab1[3]=='Payables')
            {
                $pay=$pay+$ab1[2];
            }

          }
         $pa=$pay-$rec;

          foreach ($ab as $key => $value) {
              //p($value);
               $ab1=explode(',', $value);
         /* p($ab1[0]);
          p($ab1[1]);
          p($ab1[2]);
          p($ab1[3]);
          p($ab1[4]);
          p($ab1[5]);
          p($ab1[6]);*/
          // p($ab1[2]);
           //echo $rec.'___'.$pay;

          

           $t_id=$ab1[7];

           $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
        // p($data);

          if($data->pay_type=='Receivables')
          {
              $updateamt=0;
          }
          else
          {
            $updateamt=0;
            /*if(!empty($pa)){
             if($data->update_amt>$pa){
              $updateamt=$pa;
              $pa=0;
             }elseif ($data->update_amt<$pa) {
               $updateamt=$data->update_amt;
               $pa=$pa-$data->update_amt;
             }else{
               $updateamt=$data->update_amt;
               $pa=0;
             }
           }*/
              
          }

                $param22 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,//$data->store_id,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $updateamt,
                    'param10' => $data->txn_id,
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $inte[$key],
                    'param15' => $arears,
                    'param16' => $ab1[11],
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest
                  );  

//p($param22);
           $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param22);
           //p($res2); 

         foreach($res2 as $val) {  

           $param = array(
                  'act_mode' => 'updatetranssuccessdata_mpc', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->pay_amt,
                  'param17' => $val->txn_id_point,
                  'param18' => $val->update_amt
                );
                //p($param);  
           $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
          
               
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->update_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res1 = $this->supper_admin->call_procedure('proc_procedure4', $param);

      $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$val->update_amt;
      #file_get_contents($no_url);
            }
          }       
          
      
        }

         /*$email=$response1->email;
       $mobile=$response1->contact;
        //$mobile='9997785061';
          $gatewaytraid='POP'.rand(1000000,9999999);
          
           $msg=urlencode('Dear MPC Partner, Rs. '.$payamt.'/- has been debited to your account Store ID: '.$response1->s_storeunid.' on '.
            date('d/m/Y').', Transaction ID: '.$gatewaytraid.'. Payment Subject to Realisation. Thank You');

          $url='http://sms.tattler.co/app/smsapi/index.php?key=558709407EE8FE&routeid=288&type=text&contacts='.$mobile.'&senderid=WEBPRO&msg='.$msg; 
                //echo $url; 
          file_get_contents($url);
              //print_r(file_get_contents($url));

                   $content='<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="font-awesome.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin:0px; padding:0px; font-family:Arial, Helvetica, sans-serif; font-size:14px; background:#fff;">
<div style="width:530px; height:435px; margin:6% auto; padding:0px; border:1px solid rgba(128, 128, 128, 0.32);">
<div style="width:100%; height:auto; float:left; padding: 25px 0px;">
<div style="width:40%; height:auto; float:left; padding:5px;">
<a href=""><img src="'.base_url().'assets/img/logoNew.png" alt="" width="158"/></a>
</div>
<div style="float:right; width:50%; height:auto; padding: 5px;">
<ul style="width:auto; height:auto; margin:0px; padding:0px; float:right;">
<li style="float:left; list-style:none;"><a href="" style="  text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social1.png" alt=""  style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>

<li style="float:left; list-style:none;"><a href="" style=" text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social2.png" alt="" style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>

<li style="float:left; list-style:none;"><a href="" style="  text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social3.png" alt="" style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>

<li style="float:left; list-style:none;"><a href="" style="  text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social4.png" alt="" style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>
</ul>
</div>
</div>
<div style="width:100%; height:288px; float:left; background:#ffe5e6;">
<div style="width:100%; height:auto; float:left;">
<div style="width:45%; height:auto; float:left; padding:5px;">
<p style="line-height: 4px; font-weight: 600;;">Store ID:'.$response1->s_storeunid.'</p>
<p style="line-height: 4px; font-weight: 600;">Store Name:'.$response1->s_name.'</p>
</div>
<div style="width:45%; height:auto; float:right; padding:5px;">
<p style=" line-height: 4px; font-weight: 600; text-align:right;">Paid Date:'.date('d/m/Y').'</p>
<p style=" line-height: 4px; font-weight: 600; text-align:right;">Transaction ID:'.$gatewaytraid.'</p>
</div>
</div>

<div style=" width: 100%; height: auto; float: left; padding-top: 30px;">
<div style="width:30%; height:auto; float:left;">
<img src="'.base_url().'assets/img/coins.jpg" alt="" />
</div>
<div style="width:65%; height:auto; float:right;">
<h2 style="font-size: 16px; font-weight: 600; color: #ff0004;">Dear MPC Partner,</h2>
<h3 style=" font-size: 18px; padding-top: 5px;line-height: 2px;">Total Amount debited <span style="color:red;"><i class="fa fa-inr" aria-hidden="true"></i>  '.$payamt.'/-</span></h3>

</div>
</div>
</div>

<div style="width:100%; height:auto; float:left;">
<p align="left" style="font-weight: 600; float: left; padding: 8px; font-size: 13px; padding-top: 1px;">Looking Forword to do business With you..!!</p>
<p align="right" style=" float: right; padding: 8px; font-weight: 600; font-size: 13px; padding-top: 1px;">MyPopCoins Team.</p>
</div>
</div>
</body>
</html>
';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('info@MindzShop.com', 'MPC');
                  $this->email->to($email);
                  $this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Pay Bill');
                  $this->email->message($content);
                  $this->email->send();*/

        //exit;
       redirect(base_url().'admin/transaction/retailer_paymentprocess');
      }
      $this->load->view('helper/header');      
      $this->load->view('transaction/retailer_transaction',$response);
  }


function details_rec()
{
    $rid=$this->uri->segment(4);
  $ref_id=$this->uri->segment(5);
  $type=$this->uri->segment(6);
  $cid=$this->uri->segment(7);
  $month=$this->uri->segment(8);
  $year=$this->uri->segment(9);
  $sid=$this->uri->segment(10);

  if($sid=='0')
  {
      $type='R';
      $id=$this->uri->segment(4);
  }
  else
  {
      $type='';
      $id=$this->uri->segment(10);
  }


   $parameter1 = array(
                    'act_mode' => 'details_rec', 
                    'row_id' => $cid,//$this->uri->segment(4),
                    'param1' => $ref_id,//$month,//$this->uri->segment(5),
                    'param2' => $year,
                    'param3' => $type,
                    'param4' => $id,
                    'param5' => $month,
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

        //p($parameter1);exit;
        $response['view'] = $this->supper_admin->call_procedure('proc_transaction2', $parameter1);
//p($response['view']);

        if(!empty($this->input->post('receipts')))
        {
          $finalExcelArr = array('S.No.','Settlement Cycle','Transaction ID','Bill UID','Store ID','Store Name','Location','User ID','Date & Time of Upload','Date of Purchase','Bill No.','Bill Amt.','Paid By Cash/Card','Cashback %','Cashback Amt.','Approved Date & Time');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Upload Receipts Details');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                      
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
            $ii=0;
            foreach ($response['view'] as $key => $value) {  $ii++; 
           
             
            $newvar = $j+$key;
            

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $ii);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, date("d-m-Y", strtotime($value->from_date)).' To '.date("d-m-Y", strtotime($value->to_date)).' Pay Date '.date("d-m-Y", strtotime($value->pay_date)));
            //$objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->txn_id);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->txn_id);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->bill_uid);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->s_storeunid);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->s_name);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->location);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->t_FName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, date("d-m-Y h:i:s", strtotime($value->re_createdon)));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, date("d-m-Y h:i:s", strtotime($value->date_of_purchase)));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->bill_no);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->bill_amt);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->bill_update_amt);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->s_cashbackper. ' %');
            $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, ($value->bill_update_amt*$value->s_cashbackper)/100);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, date("d-m-Y h:i:s", strtotime($value->re_modified)));
            //$objPHPExcel->getActiveSheet()->setCellValue($cols[16].$newvar, $value->location);
            
                 
            }
          }
      

          $filename='UploadReceiptsDetails.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
        }
       

      $this->load->view('helper/header');      
      $this->load->view('transaction/tra_receipts',$response);
}


function details_pay()
{
  $rid=$this->uri->segment(4);
  $ref_id=$this->uri->segment(5);
  $type=$this->uri->segment(6);
  $cid=$this->uri->segment(7);
  $month=$this->uri->segment(8);
  $year=$this->uri->segment(9);
  $sid=$this->uri->segment(10);

  if($sid=='0')
  {
      $type='R';
      $id=$this->uri->segment(4);
  }
  else
  {
      $type='';
      $id=$this->uri->segment(10);
  }

   $parameter1 = array(
                    'act_mode' => 'details_pay', 
                    'row_id' => $cid,
                    'param1' => $month,
                    'param2' => $year,
                    'param3' => $type,
                    'param4' => $id,
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

        //p($parameter1);exit;
        $response['view'] = $this->supper_admin->call_procedure('proc_transaction2', $parameter1);
       // p($response['view']);


        if(!empty($this->input->post('receipts')))
        {
          $finalExcelArr = array('S.No.','Settlement Cycle','Deal Coupon Code / Transaction ID','Store ID','Store Name','Location','User Name','Date & Time','Deal ID','Redeemed Popcoins / Transferred Popcoins','MPC Margin %','Net Popcoins Redeemed');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Deal Transfer PopCoins Details');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                      
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
            $ii=0;
            foreach ($response['view'] as $key => $value) {  $ii++; 
           
            if($value->deal_id=='NA'){
            $deal= $value->deal_id; }
            else {
            $deal= 'DEA('.$value->deal_id.')POP';}
            if($value->tax_amt==''){
            $tax= 'NA'; }
            else{
            $tax= $value->tax_amt.'%';}
            $newvar = $j+$key;
            

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $ii);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, date("d-m-Y", strtotime($value->from_date)).' To '.date("d-m-Y", strtotime($value->to_date)).' Pay Date '.date("d-m-Y", strtotime($value->pay_date)));
            //$objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->txn_id);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->dealcoupan_code);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->storeuid);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->storename);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->storeaddress);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->customername);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, date("d-m-Y h:i:s", strtotime($value->date_of_purchase)));
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $deal);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->amount);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $tax);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->update_amt);
         
            
                 
            }
          }
      

          $filename='DealTransferDetails.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
        }
       

      $this->load->view('helper/header');      
      $this->load->view('transaction/tra_deal',$response);
}
  

  function details_areas()
{


  $string = $this->uri->segment(5);
if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_¬-]/', $string))
{
    $isPositive='Payables';
}
else
{
  $isPositive='Receivables';
}

   $parameter1 = array(
                    'act_mode' => 'details_areas', 
                    'row_id' => $this->uri->segment(4),
                    'param1' => $this->uri->segment(5),
                    'param2' => $isPositive,
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

       // p($parameter1);exit;
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_transaction2', $parameter1);
//p($response['amountdetails']);

         $parameter = array(
                    'act_mode' => 'getinterest', 
                    'row_id' => '',
                    'counname' => '',
                    'coucode' => '',
                    'commid' => ''
                  );
        $response['interest'] = $this->supper_admin->call_procedureRow('proc_master', $parameter);
       

      $this->load->view('helper/header');      
      $this->load->view('transaction/areas_transaction',$response);
}


 public function store_payment() {
     
         $parameter = array(
                    'act_mode' => 'storepaymentprocess', 
                    'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response1 = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);
//p($response1);
        $parameter1 = array(
                    'act_mode' => 'storepayment1', 
                    'v_id' => $response1->retailer_id,
                    'v_status' => $response1->txn_id,
                    'v_mobile' => $response1->cycle_id,
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );

        //p($parameter1);
        $response2 = $this->supper_admin->call_procedureRow('proc_vendor', $parameter1);
//p($response2);
        $param = array(
                        'act_mode' => 'remaining_balance_store',
                        'row_id' => $response1->store_id,
                        'param1' => '',
                        'param2' => '',
                        'param3' => '',
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => ''
                      );  
                      // p($param);    
            $response['remaining_balance'] = $this->supper_admin->call_procedureRow('proc_manualpayment', $param); 
      //p($response['remaining_balance']);
       
@$response['amount']=array('t_id'=>$response2->t_id,'curr_status'=>$response2->curr_status,'from_date'=>$response2->from_date,'to_date'=>$response2->to_date,'pay_date'=>$response2->pay_date,'s_name'=>$response1->rid,'store_count'=>$response1->s_name,'company_name'=>$response1->company_name,'pay_type'=>$response2->pay_type,'pay_amt'=>$response1->pay_amt,'remaining_amt'=>$response1->remaining_amt,'t_createdon'=>$response2->t_createdon,'txn_id'=>$response2->txn_id,'tamt'=>$response2->pay_amt,'interest_amt'=>$response2->interest_amt,'remain_intamt'=>$response2->remain_intamt,'active'=>$response1->payment_status,'cc'=>$response1->cc,'retailer_id'=>$response1->retailer_id,'store_id'=>$response1->store_id,'sid'=>$response1->s_storeunid,'email'=>$response1->email,'contact'=>$response1->contact);

        if(isset($_POST['save_data'])) {
          //pend($_REQUEST);
          $alldata=$_REQUEST['all'];
          $int=$_REQUEST['intamt'];
          $arears=$_REQUEST['arears'];

          $ab=explode('#', $alldata);
          $inte=explode('#', $int);

          //p($ab);exit;

          $rec='0';
          $pay='0';

          foreach ($ab as $key => $value1) {
             // p($value);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[2]=='Receivables')
            {
                $rec=$rec+$ab1[1]+$payamt;
            }

            if($ab1[2]=='Payables')
            {
                $pay=$pay+$ab1[1];
            }

          }
         $pa=$pay-$rec;

          foreach ($ab as $key => $value) {
              //p($value);
               $ab1=explode(',', $value);
         /* p($ab1[0]);
          p($ab1[1]);
          p($ab1[2]);
          p($ab1[3]);
          p($ab1[4]);
          p($ab1[5]);
          p($ab1[6]);*/
          // p($ab1[2]);
           //echo $rec.'___'.$pay;

          

           $t_id=$ab1[6];

           $param2 = array(
                    'act_mode' => 'fetchtransdata_store', 
                    'row_id' => $t_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
       //p($data);

          $updateamt=0;

                $param22 = array(
                    'act_mode' => 'updatetransmapdata_store', 
                    'row_id' => $t_id,
                    'param1' => $data->txn_id,
                    'param2' => '',
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_id,
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => $data->pay_type,
                    'param9' => $updateamt,
                    'param10' => $arears,
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => '',
                    'param14' => $inte[$key],
                    'param15' => '0',
                    'param16' => '1',
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest
                  );  

//p($param22);
           $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param22);
           //p($res2); 

       foreach($res2 as $val) {  

           $param = array(
                  'act_mode' => 'updatetranssuccessdata_store', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->pay_amt,
                  'param17' => $val->txn_id_point,
                  'param18' => $val->update_amt
                );
                //p($param);  
           $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
          
               
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->update_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res1 = $this->supper_admin->call_procedure('proc_procedure4', $param);
      $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$val->update_amt;
      #file_get_contents($no_url);
            }
          }      
          
      
        }


        $email=$response1->email;
       $mobile=$response1->contact;
        //$mobile='9997785061';
          $gatewaytraid='POP'.rand(1000000,9999999);
          
           $msg=urlencode('Dear MPC Partner, Rs. '.$payamt.'/- has been debited to your account Store ID: '.$response1->s_storeunid.' on '.
            date('d/m/Y').', Transaction ID: '.$gatewaytraid.'. Payment Subject to Realisation. Thank You');

         // $url='http://sms.tattler.co/app/smsapi/index.php?key=558709407EE8FE&routeid=288&type=text&contacts='.$mobile.'&senderid=WEBPRO&msg='.$msg; 
                $data11=SMSURL;
           $url=$data11.$mobile.'&message='.$msg;  
          #file_get_contents($url);
              //print_r(file_get_contents($url));

                   $content='<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="font-awesome.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin:0px; padding:0px; font-family:Arial, Helvetica, sans-serif; font-size:14px; background:#fff;">
<div style="width:530px; height:435px; margin:6% auto; padding:0px; border:1px solid rgba(128, 128, 128, 0.32);">
<div style="width:100%; height:auto; float:left; padding: 25px 0px;">
<div style="width:40%; height:auto; float:left; padding:5px;">
<a href=""><img src="'.base_url().'assets/img/logoNew.png" alt="" width="158"/></a>
</div>
<div style="float:right; width:50%; height:auto; padding: 5px;">
<ul style="width:auto; height:auto; margin:0px; padding:0px; float:right;">
<li style="float:left; list-style:none;"><a href="" style="  text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social1.png" alt=""  style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>

<li style="float:left; list-style:none;"><a href="" style=" text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social2.png" alt="" style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>

<li style="float:left; list-style:none;"><a href="" style="  text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social3.png" alt="" style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>

<li style="float:left; list-style:none;"><a href="" style="  text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social4.png" alt="" style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>
</ul>
</div>
</div>
<div style="width:100%; height:288px; float:left; background:#ffe5e6;">
<div style="width:100%; height:auto; float:left;">
<div style="width:45%; height:auto; float:left; padding:5px;">
<p style="line-height: 4px; font-weight: 600;;">Store ID:'.$response1->s_storeunid.'</p>
<p style="line-height: 4px; font-weight: 600;">Store Name:'.$response1->s_name.'</p>
</div>
<div style="width:45%; height:auto; float:right; padding:5px;">
<p style=" line-height: 4px; font-weight: 600; text-align:right;">Paid Date:'.date('d/m/Y').'</p>
<p style=" line-height: 4px; font-weight: 600; text-align:right;">Transaction ID:'.$gatewaytraid.'</p>
</div>
</div>

<div style=" width: 100%; height: auto; float: left; padding-top: 30px;">
<div style="width:30%; height:auto; float:left;">
<img src="'.base_url().'assets/img/coins.jpg" alt="" />
</div>
<div style="width:65%; height:auto; float:right;">
<h2 style="font-size: 16px; font-weight: 600; color: #ff0004;">Dear MPC Partner,</h2>
<h3 style=" font-size: 18px; padding-top: 5px;line-height: 2px;">Total Amount debited <span style="color:red;"><i class="fa fa-inr" aria-hidden="true"></i>  '.$payamt.'/-</span></h3>

</div>
</div>
</div>

<div style="width:100%; height:auto; float:left;">
<p align="left" style="font-weight: 600; float: left; padding: 8px; font-size: 13px; padding-top: 1px;">Looking Forword to do business With you..!!</p>
<p align="right" style=" float: right; padding: 8px; font-weight: 600; font-size: 13px; padding-top: 1px;">MyPopCoins Team.</p>
</div>
</div>
</body>
</html>
';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('support@mindzshop.com', 'Mypopcoins');
                  $this->email->to($email);
                  //$this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Pay Bill');
                  $this->email->message($content);
                  #$this->email->send();

        //exit;
       redirect(base_url().'admin/transaction/store_payment');
      }

      $this->load->view('helper/header');      
      $this->load->view('transaction/storepay_transaction',$response);
  }

public function storedetails() {
     
         $parameter = array(
                    'act_mode' => 'store_transaction_details', 
                    'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);

        //p($response['amountdetails']);

        $this->load->view('helper/header');      
      $this->load->view('transaction/store_transaction_details',$response);

}

function uploadfile()
{
//p("gfdfgdf");exit;
$filename=str_replace(' ', '', $_FILES['file']['name']);
  //p($filename);
//$this->load->library('PHPExcel');
//$this->load->library('PHPExcel_IOFactory');

    $file_name = str_replace(' ', '', $_FILES['file']['name']);
    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
    //Checking the file extension
    if($ext == "xls"){
     
//p("gfdhgqqqq");p($ext);p($file_name);exit;

            $file_name = str_replace(' ', '', $_FILES['file']['tmp_name']);
            $inputFileName = $file_name;

    /**********************PHPExcel Script to Read Excel File**********************/

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName); //Identify the file
            $objReader = PHPExcel_IOFactory::createReader($inputFileType); //Creating the reader
            $objPHPExcel = $objReader->load($inputFileName); //Loading the file
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
            . '": ' . $e->getMessage());
        }

        //Table used to display the contents of the file
        //echo '<center><table style="width:50%;" border=1>';

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);     //Selecting sheet 0
        $highestRow = $sheet->getHighestRow();     //Getting number of rows
        $highestColumn = $sheet->getHighestColumn();     //Getting number of columns

            $pro1="";
            $pro2="";
            $bankid="";
            $mode="";
            $arrears="";
            $intamt="";
            $upamt="";

            //p($highestRow);
        //  Loop through each row of the worksheet in turn
        for ($row = 1; $row <= $highestRow; $row++) {

            //  Read a row of data into an array

            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, 
            NULL, TRUE, FALSE);
            // This line works as $sheet->rangeToArray('A1:E1') that is selecting all the cells in that row from cell A to highest column cell

            //echo "<tr>";
//p($rowData[0]);
            //echoing every cell in the selected row for simplicity. You can save the data in database too.
            
            foreach($rowData[0] as $k=>$v)
            {
                if($k==14)
                {
                    $pro1 = $pro1.$v.'#'; 
                }
                else if($k==15)
                {
                    $pro2 = $pro2.$v.'#'; 
                }
                else if($k==8)
                {
                    $bankid = $bankid.$v.'#'; 
                }
                else if($k==4)
                {
                    $mode = $mode.$v.'#'; 
                }
                else if($k==16)
                {
                    $arrears = $arrears.$v.'#'; 
                }
                else if($k==17)
                {
                    $intamt = $intamt.$v.'#'; 
                }
                else if($k==7)
                {
                    $upamt = $upamt.$v.'#'; 
                }
            }
       //echo "</tr>";
        }

      
         //p($intamt);
         // p($pro2);
         //p($bankid);
          $banktid=explode('#', trim($bankid,'#'));
          $procc1=explode('#', trim($pro1,'#'));
          $procc2=explode('#', trim($pro2,'#'));
          $mode1=explode('#', trim($mode,'#'));
          $arr=explode('#', trim($arrears,'#'));
          $intr=explode('#', trim($intamt,'#'));
          $upamt1=explode('#', trim($upamt,'#'));
          
          //p($procc2);
          $che1=count($banktid)-1;
          
          $che=$highestRow-1;

          //p($che);
          //p($che1);
          if($che!=0){
          if($che==$che1){ // if bank
          $fdf=array();
          $obj=array();
      for ($i=1; $i < count($banktid) ; $i++) //main for
      {  
           //echo $banktid[$i].'*__*'; 
           //echo $procc1[$i].'**'; 
       
          $pr1=explode(',', trim($procc1[$i]));
          $pr2=explode(',', trim($procc2[$i]));
          //echo $procc2[$i];
         
          //p($pr1);
        $j=0;
        $fetchid='';

          foreach ($pr1 as $key => $value)  # each1
          {  
         // echo $value.'#'.$banktid[$i].'__'.$mode1[$i].'__'.$arr[$i].'__'.$intr[$i].'__'.$upamt1[$i].'__'.$procc2[$i].'__'.$pr2[$j].'##';
      


if(in_array($value, $fdf))
{
  $obj[$value]=$this->getID($obj[$value],$banktid[$i],$mode1[$i],$arr[$i],$intr[$i],$upamt1[$i],$pr2[$j]);
 
}
else
{
  array_push($fdf, $value);

$obj[$value]=$this->getID($value,$banktid[$i],$mode1[$i],$arr[$i],$intr[$i],$upamt1[$i],$pr2[$j]);

}



             /*------------------------------------------*/
             
             /*if($fetchid==$value){
              $t_id=$insertid;
             }
             else
             {
              $t_id=$value;
             }
              $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);

      //p($data);exit;
          if($pr2[$j]!=0)
          {
              $updateamt=$data->update_amt-$upamt1[$i];
          }
          else
          {
              $updateamt=0;
          }

         // $updateamt=$data->update_amt-$upamt1[$i];

          $param22 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $updateamt,
                    'param10' => '',
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $intr[$i],
                    'param15' => $arr[$i],
                    'param16' => $pr2[$j], //storeid
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest,
                    'Param21' => $banktid[$i],
                    'Param22' => $mode1[$i],
                    'Param23' => '',
                    'Param24' => '',
                    'Param25' => ''
                  );  

           //p($param22); exit;
           $res2 = $this->supper_admin->call_procedure('proc_neft', $param22);
          //p($res2[0]->lid);
           $insertid=$res2[0]->lid;
           $fetchid=$value; 
//p($insertid);
//p($fetchid);
         foreach($res2 as $val) {  

           $param = array(
                  'act_mode' => 'updatetranssuccessdata_mpc', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->pay_amt,
                  'param17' => $val->txn_id_point,
                  'param18' => $val->update_amt,
                  'Param21' => $banktid[$i],
                  'Param22' => $mode1[$i],
                  'Param23' => '',
                  'Param24' => '',
                  'Param25' => ''
                );
                //p($param);  
           $res = $this->supper_admin->call_procedure('proc_neft', $param);
          
               
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->update_amt,
                  'param17' => '',
                  'param18' => '',
                  'Param21' => '',
                  'Param22' => '',
                  'Param23' => '',
                  'Param24' => '',
                  'Param25' => ''
                );             
              $res1 = $this->supper_admin->call_procedure('proc_neft', $param);
            }
          }     */ 

             /*------------------------------------------*/
          $j++;
          } #each1 end
         
      } #main for end
      echo 'Uploading successful!'; 
    } // if bank
    else
    {
      echo '<p style="color:red;">Invalid Bank Transaction ID!</p>'; 
    }

    }
  else
  {
      echo '<p style="color:red;">Empty excel sheet!</p>'; 
  }

    }

    else{
        echo '<p style="color:red;">Please upload file with xls extension only!</p>'; 
    }   






}



function getID($t_id,$banktid,$mode1,$arr,$intr,$upamt1,$pr2)
{
  //p($t_id);
   $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );      
                    
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
  
if(empty($data)){
  return;
}
          if($pr2[$j]!=0)
          {
              $updateamt=$data->update_amt-$upamt1;
          }
          else
          {
              $updateamt=0;
          }

         // $updateamt=$data->update_amt-$upamt1[$i];

          $param22 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $updateamt,
                    'param10' => '',
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $intr,
                    'param15' => $arr,
                    'param16' => $pr2, //storeid
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest,
                    'Param21' => $banktid,
                    'Param22' => $mode1,
                    'Param23' => '',
                    'Param24' => '',
                    'Param25' => ''
                  );  

           //p($param22);return 0;
           $res2 = $this->supper_admin->call_procedure('proc_neft', $param22);
          //p($res2[0]->lid);
            //p($res2); //return 0;
           $insertid=$res2[0]->lid;
//p($insertid);
//p($fetchid);
        foreach($res2 as $val) {  

           $param = array(
                  'act_mode' => 'updatetranssuccessdata_mpc', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->pay_amt,
                  'param17' => $val->txn_id_point,
                  'param18' => $val->update_amt,
                  'Param21' => $banktid[$i],
                  'Param22' => $mode1[$i],
                  'Param23' => '',
                  'Param24' => '',
                  'Param25' => ''
                );
                //p($param);  
           $res = $this->supper_admin->call_procedure('proc_neft', $param);
          
               
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->update_amt,
                  'param17' => '',
                  'param18' => '',
                  'Param21' => '',
                  'Param22' => '',
                  'Param23' => '',
                  'Param24' => '',
                  'Param25' => ''
                );             
              $res1 = $this->supper_admin->call_procedure('proc_neft', $param);
      #$no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$val->update_amt;
      #file_get_contents($no_url);
            }
          }

          return   $insertid;
}

public function manual_retailer_paymentprocess() {
      
      
        $parameter = array(
                    'act_mode' => 'retailerpayment',//'retailerpaymentprocess12', 
                    'v_id' => $this->uri->segment(4),//$this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        //p($parameter);
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
     //  p($response['amountdetails']);

          $param = array(
                  'act_mode' => 'remaining_balance',
                  'row_id' => $this->uri->segment(4),
                  'param1' => '',
                  'param2' => '',
                  'param3' => '',
                  'param4' => '',
                  'param5' => '',
                  'param6' => '',
                  'param7' => '',
                  'param8' => '',
                  'param9' => '',
                  'param10' => '',
                  'param11' => '',
                  'param12' => '',
                  'param13' => '',
                  'param14' => '',
                  'param15' => '',
                  'param16' => '',
                  'param17' => '',
                  'param18' => '',
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $response['remaining_balance'] = $this->supper_admin->call_procedureRow('proc_manualpayment', $param); 
//p($response['remaining_balance']);

         if(isset($_POST['save_data'])) { //1
          
          if($response['remaining_balance']->t_id=='')
          {
              $rem_bal=0;
          }
          else
          {
              $rem_bal=$response['remaining_balance']->rem_bal;

            $this->db->query("UPDATE tbl_transaction_map SET remaining_bal_status=2 WHERE t_id =".$response['remaining_balance']->t_id." LIMIT 1");
          }

          $dis=$_REQUEST['discount'];
          $normalamt=$_REQUEST['paid'];
          $mode=$_REQUEST['mode'];
          $refid=$_REQUEST['refeid'];
          $tradate=$_REQUEST['date'];
          $pay_amt=$_REQUEST['payment'];
          $total_amt=$_REQUEST['pay_amt'];
          $intamt=$_REQUEST['intamt'];
          $arears=$_REQUEST['arears'];
          $tamt=$total_amt+$intamt+$arears;
          $pay_amt1=$pay_amt+$rem_bal+$dis;
          $intamt1=$_REQUEST['intamt1'];

          $alldata=$_REQUEST['all'];
          $ab=explode('#', $alldata);
          $inte=explode('#', $intamt1);

       
                      
     /* ----------------------------------- Start ----------------------------------------------------*/

          $rec='0';
          $pay='0';
          $len=0;
          foreach ($ab as $key => $value1) {
             //p($value1);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[3]=='Receivables')
            {
                $rec=$rec+$ab1[2]+$payamt;
                $len++;
            }

            if($ab1[3]=='Payables')
            {
                $pay=$pay+$ab1[2];
            }

          }


         $samt=0;
         $inter=0;
         $arr=0;
         $arr1=0;
         $disc=0;
          foreach ($ab as $key => $value) 
          { # start for each
             //p($value);
               $ab1=explode(',', $value);
      if($ab1[3]=='Receivables') #main
      {

          $inter=$inter+$inte[$key];
          
          $pay_type=$ab1[3];
          if($samt==0)
          {
              $arr=0;
              $arr1=$ab1[2];
              $t_paid=($pay_amt-$intamt)+$rem_bal+$pay+$dis;
            
              if($pay_amt1 > $tamt)
              {
                $queue=1;
                $samt=$samt+($t_paid-$ab1[2]);
                $remaining_balance=$samt;
                $amount_paid=$ab1[2];
                $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                if($len==1)
                {
                  $amt=$pay_amt;
                }
                else
                {
                  $amt=0;
                }
              }
              else
              {
                  if($t_paid > $ab1[2])
                  {
                    $queue=2;
                    $samt=$samt+($t_paid-$ab1[2]);
                    $remaining_balance=0;
                    $amount_paid=$ab1[2];
                    $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                    if($len==1)
                    {
                      $amt=$pay_amt;
                    }
                    else
                    {
                      $amt=0;
                    }
                  }
                  else
                  {
                      $queue=3;
                      $samt=$samt+$t_paid;
                      $remaining_balance=0;
                      $amount_paid=$samt;
                      $paidper=100-(($ab1[2]-$amount_paid)*100/$ab1[2]);
                      if($len==1)
                      {
                        $amt=$pay_amt;
                      }
                      else
                      {
                        $amt=0;
                      }
                  }
                  
              }
          }
          else
          {
              $arr=$arr+$arr1;
              $arr1=$arr1+$ab1[2];
              if($samt > $total_amt)
              {
                $queue=4;
                $samt=$samt-$ab1[2];
                $remaining_balance=$samt;
                $amount_paid=$ab1[2];
                $paidper=100-(($ab1[2]-$amount_paid)*100/$ab1[2]);

                for($i=1; $i <= $len; $i++) 
                { 
                   if($i==$len)
                   {
                      $amt=$pay_amt;
                   } 
                   else
                   {
                      $amt=0;
                   }
                }
              }
              else
              {
                  $queue=5;
                  $remaining_balance=0;
                  $amount_paid=$samt;
                  $paidper=100-(($ab1[2]-$amount_paid)*100/$ab1[2]);
                  $amt=$pay_amt;
              }
          }

          if ($disc == 0) 
          {
            if($len==1)
            {
              $discount=$dis; 
              $remain_balance=$samt;   
            } 
            else 
            {
              $discount=0;
              $remain_balance=0;  
            }
          } 
          else if ($disc == $len -1) 
          {
            $discount=$dis;
            $remain_balance=$samt;  
          }
          else
          {
            $discount=0;
            $remain_balance=0;  
          }
          $disc++;

        }
        else #main
        {
            $inter=0;
            $disc=0;
            $queue=6;
            $discount=0;
            $pay_type=$ab1[3];
            $remaining_balance=0;
            $amount_paid=$ab1[2];
            $paidper=100;
            $t_paid=$ab1[2];
        }
         




        $t_id=$ab1[7];
        $store_idd=$ab1[11];
        $txn_idd=$ab1[5];

         $en=$alldata.'_'.$intamt1.'_'.date('d-m-Y h:i:s');
         $this->db->query("UPDATE tbl_transaction_map SET arears =".$arr.",pay_updation ='".$en."', remain_intamt =".$pay_amt.",mode_of_pay ='Manual_".$mode."' WHERE t_id ='".$t_id."' LIMIT 1");


         $testparam = array('store_idd' => $store_idd,
                    'txn_idd' => $txn_idd,
                    'row_id' => $t_id,
                    'amount' => $ab1[2],
                    'rem_bal' => $samt,
                    'arears' => $arr,#$arears,
                    'remaining_balance' => $remaining_balance,
                    'amount_paid' => $amount_paid,
                    'pay_amt' => $pay_amt,
                    'paidper' => $paidper,
                    'pay_type' => $pay_type,
                    'queue' => $queue,
                    'discount' =>$discount,
                    'dis_count' =>$disc,
                    'interest'=>$inter,
                    'actual_interest' => $inte[$key],
                    'aamt' => $amt
                  );  
       
    // p($testparam);    

  // } exit;{
 /* ----------------------------------- end ----------------------------------------------------*/
          $param22 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,//$_REQUEST['t_id'],
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
                            
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param22);

          if($data->remaining_amt=="") {
            
            $remaining=number_format($data->pay_amt)-number_format($amount_paid) ;
          } else {
            
            $remaining=number_format($data->remaining_amt)-number_format($amount_paid);
          }   


          $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,//$_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $remaining,
                    'param10' => $txn_idd,//$_REQUEST['txn_id'],
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $inter,#$intamt,//$intamt1,
                    'param15' => $arr,#$arears,//$intamt_paid,
                    'param16' => $store_idd,//$_REQUEST['str_id'],
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest,
                    'param21' => $refid,
                    'param22' => $tradate,
                    'param23' => $discount,
                    'param24' => $total_amt,
                    'param25' => 'manual_'.$mode,
                    'param26' => $amt,#$pay_amt,#$normalamt,
                    'param27' => $pay_amt,
                    'param28' => $remaining_balance,
                    'param29' => $tamt,
                    'param30' => ''
                  );  


         // p($param2);


            $res2 = $this->supper_admin->call_procedure('proc_manualpayment', $param2);  

            //$res2 = $this->supper_admin->call_procedure('proc_procedure4', $param2);
            //p($res2);

 //   } exit;{
          foreach($res2 as $val) { //4 for each
            if($val->process_amt=='')
            {
                $process_amt=($val->pay_amt*$paidper)/100;
            }
            else
            {
                $a=$val->pay_amt-$val->process_amt;
                $process_amt=($a*$paidper)/100;
            }
            
            $param = array(
                  'act_mode' => 'updatetranssuccessdata', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt+$val->process_amt,
                  'param17' => 'success',
                  'param18' => $paidper,
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);           
            //$res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            if($val->user_type=='C') { //5
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc',//'insertcustomercashback', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => '',
                  'param18' => '',
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);   
                           
          //$res = $this->supper_admin->call_procedure('proc_procedure4', $param);
                
      $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$process_amt;
      #file_get_contents($no_url);
            } //5
          } //4 foreach    
          
      

      

       } #foreach loop

        /* ----------------- Process End --------------------- */  


 redirect(base_url().'admin/transaction/mpc_paymentprocess');
      } //1 form save data

  

      $this->load->view('helper/header');      
      $this->load->view('transaction/manual_retailer_transaction',$response);
  }

  public function manual_retailer_paymentprocess_22may() {
      
      
       

         /*$email=$response1->email;
       $mobile=$response1->contact;
        //$mobile='9997785061';
          $gatewaytraid='POP'.rand(1000000,9999999);
          
           $msg=urlencode('Dear MPC Partner, Rs. '.$payamt.'/- has been debited to your account Store ID: '.$response1->s_storeunid.' on '.
            date('d/m/Y').', Transaction ID: '.$gatewaytraid.'. Payment Subject to Realisation. Thank You');

          $url='http://sms.tattler.co/app/smsapi/index.php?key=558709407EE8FE&routeid=288&type=text&contacts='.$mobile.'&senderid=WEBPRO&msg='.$msg; 
                //echo $url; 
          file_get_contents($url);
              //print_r(file_get_contents($url));

                   $content='<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="font-awesome.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin:0px; padding:0px; font-family:Arial, Helvetica, sans-serif; font-size:14px; background:#fff;">
<div style="width:530px; height:435px; margin:6% auto; padding:0px; border:1px solid rgba(128, 128, 128, 0.32);">
<div style="width:100%; height:auto; float:left; padding: 25px 0px;">
<div style="width:40%; height:auto; float:left; padding:5px;">
<a href=""><img src="'.base_url().'assets/img/logoNew.png" alt="" width="158"/></a>
</div>
<div style="float:right; width:50%; height:auto; padding: 5px;">
<ul style="width:auto; height:auto; margin:0px; padding:0px; float:right;">
<li style="float:left; list-style:none;"><a href="" style="  text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social1.png" alt=""  style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>

<li style="float:left; list-style:none;"><a href="" style=" text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social2.png" alt="" style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>

<li style="float:left; list-style:none;"><a href="" style="  text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social3.png" alt="" style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>

<li style="float:left; list-style:none;"><a href="" style="  text-decoration: none; margin: 0px; display: block;"><img src="'.base_url().'assets/img/social4.png" alt="" style=" width: 39px; height: 39px; text-decoration: none; margin: 5px; border-radius: 5px;" /></a></li>
</ul>
</div>
</div>
<div style="width:100%; height:288px; float:left; background:#ffe5e6;">
<div style="width:100%; height:auto; float:left;">
<div style="width:45%; height:auto; float:left; padding:5px;">
<p style="line-height: 4px; font-weight: 600;;">Store ID:'.$response1->s_storeunid.'</p>
<p style="line-height: 4px; font-weight: 600;">Store Name:'.$response1->s_name.'</p>
</div>
<div style="width:45%; height:auto; float:right; padding:5px;">
<p style=" line-height: 4px; font-weight: 600; text-align:right;">Paid Date:'.date('d/m/Y').'</p>
<p style=" line-height: 4px; font-weight: 600; text-align:right;">Transaction ID:'.$gatewaytraid.'</p>
</div>
</div>

<div style=" width: 100%; height: auto; float: left; padding-top: 30px;">
<div style="width:30%; height:auto; float:left;">
<img src="'.base_url().'assets/img/coins.jpg" alt="" />
</div>
<div style="width:65%; height:auto; float:right;">
<h2 style="font-size: 16px; font-weight: 600; color: #ff0004;">Dear MPC Partner,</h2>
<h3 style=" font-size: 18px; padding-top: 5px;line-height: 2px;">Total Amount debited <span style="color:red;"><i class="fa fa-inr" aria-hidden="true"></i>  '.$payamt.'/-</span></h3>

</div>
</div>
</div>

<div style="width:100%; height:auto; float:left;">
<p align="left" style="font-weight: 600; float: left; padding: 8px; font-size: 13px; padding-top: 1px;">Looking Forword to do business With you..!!</p>
<p align="right" style=" float: right; padding: 8px; font-weight: 600; font-size: 13px; padding-top: 1px;">MyPopCoins Team.</p>
</div>
</div>
</body>
</html>
';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('info@MindzShop.com', 'MPC');
                  $this->email->to($email);
                  $this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Pay Bill');
                  $this->email->message($content);
                  $this->email->send();*/

       
  }

public function manual_store_paymentprocess() {
      
      
        $parameter = array(
                    'act_mode' => 'retailerpayment',//'retailerpaymentprocess12', 
                    'v_id' => $this->uri->segment(4),//$this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        //p($parameter);
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
     //  p($response['amountdetails']);

          $param = array(
                  'act_mode' => 'remaining_balance_store',
                  'row_id' => $this->uri->segment(5),
                  'param1' => '',
                  'param2' => '',
                  'param3' => '',
                  'param4' => '',
                  'param5' => '',
                  'param6' => '',
                  'param7' => '',
                  'param8' => '',
                  'param9' => '',
                  'param10' => '',
                  'param11' => '',
                  'param12' => '',
                  'param13' => '',
                  'param14' => '',
                  'param15' => '',
                  'param16' => '',
                  'param17' => '',
                  'param18' => '',
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                // p($param);    
            $response['remaining_balance'] = $this->supper_admin->call_procedureRow('proc_manualpayment', $param); 
//p($response['remaining_balance']);

         if(isset($_POST['save_data'])) { //1
          
          if($response['remaining_balance']->t_sid=='')
          {
              $rem_bal=0;
          }
          else
          {
              $rem_bal=$response['remaining_balance']->rem_bal;

            $this->db->query("UPDATE tbl_transaction_storewise SET remaining_bal_status=2 WHERE t_sid =".$response['remaining_balance']->t_sid." LIMIT 1");
          }

          $dis=$_REQUEST['discount'];
          $normalamt=$_REQUEST['paid'];
          $mode=$_REQUEST['mode'];
          $refid=$_REQUEST['refeid'];
          $tradate=$_REQUEST['date'];
          $pay_amt=$_REQUEST['payment'];
          $total_amt=$_REQUEST['pay_amt'];
          $intamt=$_REQUEST['intamt'];
          $arears=$_REQUEST['arears'];
          $tamt=$total_amt+$intamt+$arears;
          $pay_amt1=$pay_amt+$rem_bal+$dis;
          $intamt1=$_REQUEST['intamt1'];

          $alldata=$_REQUEST['all'];
          $ab=explode('#', $alldata);
          $inte=explode('#', $intamt1);
                      
     /* ----------------------------------- Start ----------------------------------------------------*/


          $rec='0';
          $pay='0';
          $len=0;
          foreach ($ab as $key => $value1) {
             // p($value);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[3]=='Receivables')
            {
                $rec=$rec+$ab1[2]+$payamt;
                $len++;
            }

            if($ab1[3]=='Payables')
            {
                $pay=$pay+$ab1[2];
            }

          }
        

        $samt=0;
        $inter=0;
        $arr=0;
        $arr1=0;
        $disc=0;
          foreach ($ab as $key => $value) 
          { # start for each
             //p($value);
               $ab1=explode(',', $value);
       if($ab1[3]=='Receivables') #main
       {
          $inter=$inter+$inte[$key];
          $pay_type=$ab1[3];
          if($samt==0)
          {
              
              $t_paid=($pay_amt-$intamt)+$rem_bal+$pay+$dis;
              $tamt1=$ab1[2]+$intamt+$arears;
              $arr=0;
              $arr1=$ab1[2];
              if($pay_amt1 > $tamt1)
              {
                $queue=1;
                $samt=$samt+($t_paid-$ab1[2]);
                $remaining_balance=$samt;
                $amount_paid=$ab1[2];
                $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                if($len==1)
               {
                  $amt=$pay_amt;
               } 
               else
               {
                  $amt=0;
               }
                
              }
              else
              {
                  if($t_paid > $ab1[2])
                  {
                    $queue=2;
                    $samt=$samt+($t_paid-$ab1[2]);
                    $remaining_balance=$samt;
                    $amount_paid=$ab1[2];
                    $paidper=100-(($amount_paid-$amount_paid)*100/$amount_paid);
                    if($len==1)
                   {
                      $amt=$pay_amt;
                   } 
                   else
                   {
                      $amt=0;
                   }
                  }
                  else
                  {
                      $queue=3;
                      $samt=$samt+$t_paid;
                      $remaining_balance=0;
                      $amount_paid=$samt;
                      $paidper=100-(($ab1[2]-$amount_paid)*100/$ab1[2]);
                       if($len==1)
                       {
                          $amt=$pay_amt;
                       } 
                       else
                       {
                          $amt=0;
                       }
                  }
                  
              }
          }
          else
          {
              $arr=$arr+$arr1;
              $arr1=$arr1+$ab1[2];
              if($samt > $total_amt)
              {
                $queue=4;
                $samt=$samt-$ab1[2];
                $remaining_balance=$samt;
                $amount_paid=$ab1[2];
                $paidper=100-(($ab1[2]-$amount_paid)*100/$ab1[2]);
                for($i=1; $i <= $len; $i++) 
                { 
                   if($i==$len)
                   {
                      $amt=$pay_amt;
                   } 
                   else
                   {
                      $amt=0;
                   }
                }
              }
              else
              {
                  //$samt=0;
                  $queue=5;
                  $remaining_balance=0;
                  $amount_paid=$samt;
                  $paidper=100-(($ab1[2]-$amount_paid)*100/$ab1[2]);
                  $amt=$pay_amt;
              }
          }

          if ($disc == 0) 
          {
            if($len==1)
            {
              $discount=$dis; 
              $remain_balance=$samt;   
            } 
            else 
            {
              $discount=0;
              $remain_balance=0;  
            }
          } 
          else if ($disc == $len -1) 
          {
            $discount=$dis;
            $remain_balance=$samt;  
          }
          else
          {
            $discount=0;
            $remain_balance=0;  
          }
          $disc++;

        }
        else #main
        {
            $inter=0;
            $queue=6;
            $discount=0;
            $pay_type=$ab1[3];
            $remaining_balance=0;
            $amount_paid=$ab1[2];
            $paidper=100;
            $t_paid=$ab1[2];
        }
         
        $t_id=$ab1[7];
        $store_idd=$ab1[11];
        $txn_idd=$ab1[5];

         $en=$alldata.'_'.$intamt1.'_'.date('d-m-Y h:i:s');
          $this->db->query("UPDATE  tbl_transaction_map SET arears ='".$arears."',pay_updation ='".$en."', remain_intamt ='".$pay_amt."',mode_of_pay ='Manual_".$mode."' WHERE t_id ='".$t_id."' LIMIT 1");

          $this->db->query("UPDATE  tbl_transaction_storewise SET arears ='".$arr."',pay_updation ='".$en."', remain_intamt ='".$pay_amt."',mode_of_pay ='Manual_".$mode."' WHERE txn_id ='".$txn_idd."' LIMIT 1");

         $testparam = array('store_idd' => $store_idd,
                    'txn_idd' => $txn_idd,
                    'row_id' => $t_id,
                    'amount' => $ab1[2],
                    'rem_bal' => $samt,
                    'arears' => $arears,
                    'tamt' => $tamt1,
                    'remaining_balance' => $remaining_balance,
                    'amount_paid' => $amount_paid,
                    'pay_amt' => $pay_amt,
                    'paidper' => $paidper,
                    'pay_type' => $pay_type,
                    'queue' => $queue,
                    'discount' =>$dis,
                    'aamt' =>$amt
                  );  
       // p($testparam);    

    //} exit;{
 /* ----------------------------------- end ----------------------------------------------------*/
          $param22 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,//$_REQUEST['t_id'],
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
                            
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param22);

          $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,//$_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $data->update_amt-$amount_paid,
                    'param10' => $txn_idd,//$_REQUEST['txn_id'],
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $intamt,//$intamt1,
                    'param15' => $arears,//$intamt_paid,
                    'param16' => $store_idd,//$_REQUEST['str_id'],
                    'param17' => $data->pay_permission,
                    'param18' => $data->mpc_interest,
                    'param21' => $refid,
                    'param22' => $tradate,
                    'param23' => $discount,
                    'param24' => $total_amt,
                    'param25' => 'manual_'.$mode,
                    'param26' => $amt,#$pay_amt,#$normalamt,
                    'param27' => $pay_amt,
                    'param28' => $remaining_balance,
                    'param29' => $tamt,
                    'param30' => ''
                  );  


          //p($param2);
        $res2 = $this->supper_admin->call_procedure('proc_manualpayment', $param2);  

           $param22 = array(
                    'act_mode' => 'fetchtransdata_store', 
                    'row_id' => $store_idd,
                    'param1' => $txn_idd,
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param21' => '',
                    'param22' => '',
                    'param23' => '',
                    'param24' => '',
                    'param25' => '',
                    'param26' => '',
                    'param27' => '',
                    'param28' => '',
                    'param29' => '',
                    'param30' => ''
                  );  
                            
          $data1 = $this->supper_admin->call_procedureRow('proc_manualpayment', $param22);
//p($data1);
          if($data1->remaining_amt=="") {
            
            //$remaining=number_format($data1->pay_amt)-number_format($amount_paid);
            $remaining=$data1->pay_amt-$amount_paid;
            
          } else {
            
            $remaining=$data1->remaining_amt-$amount_paid;

          }   

         

            $paramstore = array(
                    'act_mode' => 'updatetransmapdata_store', 
                    'row_id' => $data1->t_sid,
                    'param1' => $data1->txn_id,
                    'param2' => '',
                    'param3' => $data1->retailer_id,
                    'param4' => $data1->store_id,
                    'param5' => $data1->pay_type,
                    'param6' => $data1->pay_amt,
                    'param7' => '',
                    'param8' => '',
                    'param9' => $remaining,
                    'param10' => '',
                    'param11' => 'success',
                    'param12' => '',
                    'param13' => '',
                    'param14' => $inter,#$intamt,
                    'param15' => $arr,#$arears,
                    'param16' => '',
                    'param17' => $data1->pay_permission,
                    'param18' => $data1->mpc_interest,
                    'param21' => $refid,
                    'param22' => $tradate,
                    'param23' => $discount,
                    'param24' => $tamt1,
                    'param25' => 'manual_'.$mode,
                    'param26' => $amt,#$pay_amt,#$normalamt,
                    'param27' => $pay_amt,
                    'param28' => $remaining_balance,
                    'param29' => $pay_amt+$remaining,
                    'param30' => ''
                  );  
//p($paramstore);
//} exit;{

          $res_store = $this->supper_admin->call_procedure('proc_manualpayment', $paramstore);  

            
          foreach($res2 as $val) { //4 for each
            if($val->process_amt=='')
            {
                $process_amt=($val->pay_amt*$paidper)/100;
            }
            else
            {
                $a=$val->pay_amt-$val->process_amt;
                $process_amt=($a*$paidper)/100;
            }
            
            $param = array(
                  'act_mode' => 'updatetranssuccessdata_store', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt+$val->process_amt,
                  'param17' => 'success',
                  'param18' => $paidper,
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
           $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);           
            
            if($val->user_type=='C') { //5
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => '',
                  'param18' => '',
                  'param21' => '',
                  'param22' => '',
                  'param23' => '',
                  'param24' => '',
                  'param25' => '',
                  'param26' => '',
                  'param27' => '',
                  'param28' => '',
                  'param29' => '',
                  'param30' => ''
                );  
                 //p($param);    
            $res = $this->supper_admin->call_procedure('proc_manualpayment', $param);   
                           
       $no_url=base_url().'api/mainapi/activetepopcoins/format/json?custid='.$val->user_id.'&pop='.$val->update_amt;
      #file_get_contents($no_url);
            } //5
          } //4 foreach    
          
      

      

       } #foreach loop

       /*---------------- End Process ---------------------------*/
       
 redirect(base_url().'admin/transaction/mpc_paymentprocess');
      } //1 form save data

  

      $this->load->view('helper/header');      
      $this->load->view('transaction/manual_store_transaction',$response);
  }





}
?>
