<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report extends MX_Controller{
  public function __construct(){
    $this->load->model("supper_admin");
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }


  public function wastage()
{
   
    $prameter = array('act_mode' => 'listwastageproductadmin',
                      'param2' => '',
                      'param3' => '',
                      'param4' => '',
                      'param5' => '',
                      'param6' => '',
                      'param7' => '',
                      'param8' => '',
                      'param9' => '',
                      'param10'=> '',
                      'param11'=> '',
                      'param12'=> ''
                          );
        //p($prameter);exit;
        $record['list'] = $this->supper_admin->call_procedure('proc_wastage',$prameter);

         //-------- Download Excel --------//

      if(!empty($this->input->post('excel')))
          {
           
           $finalExcelArr = array('Product Name','Product SKU','Wastage QTY','Order Number','Wastage Type','Wastage Description','Created BY','Created Date');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Wastage Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($record['list'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->w_productname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->w_skuno);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->w_qty);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->w_orderno);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->w_type);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->w_desc);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->name.' ('.$value->email.')');
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->w_createdon);
           
            }
          }

          $filename='Wastage.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //------------End Download Excel --------// 


    $this->load->view('helper/header');   
    $this->load->view('report/wastagelist',$record);
}

    public function purchasereport(){

        if(isset($_REQUEST['submit'])) {
      $parameterpo    = array('act_mode'=>'viewporec',
                              'row_id'=>'',
                              'por_number'=>$_REQUEST['start_date'],
                              'por_challan'=>$_REQUEST['end_date'],
                              'por_venid'=>'',
                              'por_veninvoice'=>'',
                              'por_totqty'=>'',
                              'por_billrate'=>'',
                              'por_billamt'=>'',
                              'por_billdiscount'=>'',
                              'por_netamt'=>'',
                              'por_tax'=>'',
                              'por_totamt'=>'',
                              'por_invoiceimg'=>''
                              );
      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo);

      if(!empty($this->input->post('newsexcel')))
          {


           $finalExcelArr = array('Purchase Order number','Supplier Name','Supplier Invoice number','Discount (If any)','Net Amount Exclusive of VAT/CST','VAT/CST @ 2%/5%/12.5%','Total Amount Inclusive of VAT/CST','Date of Purchase');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Purchase Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($records['viewporeport'] as $key => $value) {
             
            $newvar = $j+$key;

              //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->pono);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->firstname.' '.$value->lastname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->poveninvoiceno);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->podiscount);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->ponetamt);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->potax);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->pototalamt);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->porecreatedon);
                                  
            }
          }

          $filename='PurchaseReport.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }

      //----------------  start pagination setting ------------------------// 
     $config['base_url']         = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
     $config['total_rows']       = count($records['viewporeport']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $records["links"]  = explode('&nbsp;',$str_links );
     $parameterpo2    = array('act_mode'=>'viewporec',
                                'row_id'=>$page,
                                'por_number'=>$_REQUEST['start_date'],
                                'por_challan'=>$_REQUEST['end_date'],
                                'por_venid'=>$second,
                                'por_veninvoice'=>'',
                                'por_totqty'=>'',
                                'por_billrate'=>'',
                                'por_billamt'=>'',
                                'por_billdiscount'=>'',
                                'por_netamt'=>'',
                                'por_tax'=>'',
                                'por_totamt'=>'',
                                'por_invoiceimg'=>''
                                );

      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo2);    
     //----------------  end pagination setting ------------------------// 
    } else {
      $parameterpo    = array('act_mode'=>'viewporec',
                              'row_id'=>'',
                              'por_number'=>'',
                              'por_challan'=>'',
                              'por_venid'=>'',
                              'por_veninvoice'=>'',
                              'por_totqty'=>'',
                              'por_billrate'=>'',
                              'por_billamt'=>'',
                              'por_billdiscount'=>'',
                              'por_netamt'=>'',
                              'por_tax'=>'',
                              'por_totamt'=>'',
                              'por_invoiceimg'=>''
                              );
      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo);

      if(!empty($this->input->post('newsexcel')))
          {


           $finalExcelArr = array('Purchase Order number','Supplier Name','Supplier Invoice number','Discount (If any)','Net Amount Exclusive of VAT/CST','VAT/CST @ 2%/5%/12.5%','Total Amount Inclusive of VAT/CST','Date of Purchase');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Purchase Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($records['viewporeport'] as $key => $value) {
             
            $newvar = $j+$key;

              //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->pono);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->firstname.' '.$value->lastname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->poveninvoiceno);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->podiscount);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->ponetamt);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->potax);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->pototalamt);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->porecreatedon);
                                  
            }
          }

          $filename='PurchaseReport.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }

          //----------------  start pagination setting ------------------------// 
     $config['base_url']         = base_url()."admin/report/purchasereport?";
     $config['total_rows']       = count($records['viewporeport']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $records["links"]  = explode('&nbsp;',$str_links );
     $parameterpo2    = array('act_mode'=>'viewporec',
                                'row_id'=>$page,
                                'por_number'=>'',
                                'por_challan'=>'',
                                'por_venid'=>$second,
                                'por_veninvoice'=>'',
                                'por_totqty'=>'',
                                'por_billrate'=>'',
                                'por_billamt'=>'',
                                'por_billdiscount'=>'',
                                'por_netamt'=>'',
                                'por_tax'=>'',
                                'por_totamt'=>'',
                                'por_invoiceimg'=>''
                                );

      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo2);   
     //----------------  end pagination setting ------------------------// 
    }

    $this->load->view('helper/header');
    $this->load->view('report/purchasereportdata',$records);
    
  }


  public function salesreport(){

    if(isset($_REQUEST['submit'])) {
      $parameterpo    = array('act_mode'=>'salesreportdata',
                              'row_id'=>'',
                              'por_number'=>$_REQUEST['start_date'],
                              'por_challan'=>$_REQUEST['end_date'],
                              'por_venid'=>'',
                              'por_veninvoice'=>'',
                              'por_totqty'=>'',
                              'por_billrate'=>'',
                              'por_billamt'=>'',
                              'por_billdiscount'=>'',
                              'por_netamt'=>'',
                              'por_tax'=>'',
                              'por_totamt'=>'',
                              'por_invoiceimg'=>''
                              );
      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo);

      if(!empty($this->input->post('newsexcel')))
    {
           $finalExcelArr = array('Order No.','Customer name','Company name','Customer ID','Order Date','Invoice No.','Invoice Date','Sub Total','State /Sales tax / VAT Amount','Total','Mode of Delivery');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Sales Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($records['viewporeport'] as $key => $value) {
              $newvar = $j+$key;
              $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);   
              $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->OrderNumber);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->firstname.' '.$value->lastname);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->companyname);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->vendorcode);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->OrderDate);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->invoice_no);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->created);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, number_format($value->subtotal,1,".",""));
              $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, number_format($value->Tax,1,".",""));
              $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, number_format((($value->TotalAmt+$value->ShippingAmt)-$value->CouponAmt),1,".",""));    
              $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->PaymentMode);       
            }
          }

          $filename='SalesReport.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
         
    }

      //----------------  start pagination setting ------------------------// 
     $config['base_url']         = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
     $config['total_rows']       = count($records['viewporeport']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $records["links"]  = explode('&nbsp;',$str_links );
     $parameterpo2    = array('act_mode'=>'salesreportdata',
                                'row_id'=>$page,
                                'por_number'=>$_REQUEST['start_date'],
                                'por_challan'=>$_REQUEST['end_date'],
                                'por_venid'=>$second,
                                'por_veninvoice'=>'',
                                'por_totqty'=>'',
                                'por_billrate'=>'',
                                'por_billamt'=>'',
                                'por_billdiscount'=>'',
                                'por_netamt'=>'',
                                'por_tax'=>'',
                                'por_totamt'=>'',
                                'por_invoiceimg'=>''
                                );

      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo2);    
     //----------------  end pagination setting ------------------------// 

    } else {
      $parameterpo    = array('act_mode'=>'salesreportdata',
                              'row_id'=>'',
                              'por_number'=>'',
                              'por_challan'=>'',
                              'por_venid'=>'',
                              'por_veninvoice'=>'',
                              'por_totqty'=>'',
                              'por_billrate'=>'',
                              'por_billamt'=>'',
                              'por_billdiscount'=>'',
                              'por_netamt'=>'',
                              'por_tax'=>'',
                              'por_totamt'=>'',
                              'por_invoiceimg'=>''
                              );
      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo);

      if(!empty($this->input->post('newsexcel')))
    {
           $finalExcelArr = array('Order No.','Customer name','Company name','Customer ID','Order Date','Invoice No.','Invoice Date','Sub Total','State /Sales tax / VAT Amount','Total','Mode of Delivery');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Sales Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($records['viewporeport'] as $key => $value) {
              $newvar = $j+$key;
              $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);   
              $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->OrderNumber);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->firstname.' '.$value->lastname);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->companyname);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->vendorcode);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->OrderDate);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->invoice_no);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->created);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, number_format($value->subtotal,1,".",""));
              $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, number_format($value->Tax,1,".",""));
              $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, number_format((($value->TotalAmt+$value->ShippingAmt)-$value->CouponAmt),1,".",""));    
              $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->PaymentMode);       
            }
          }

          $filename='SalesReport.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
         
    }

     //----------------  start pagination setting ------------------------// 
     $config['base_url']         = base_url()."admin/report/salesreport?";
     $config['total_rows']       = count($records['viewporeport']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $records["links"]  = explode('&nbsp;',$str_links );
     $parameterpo2    = array('act_mode'=>'salesreportdata',
                                'row_id'=>$page,
                                'por_number'=>'',
                                'por_challan'=>'',
                                'por_venid'=>$second,
                                'por_veninvoice'=>'',
                                'por_totqty'=>'',
                                'por_billrate'=>'',
                                'por_billamt'=>'',
                                'por_billdiscount'=>'',
                                'por_netamt'=>'',
                                'por_tax'=>'',
                                'por_totamt'=>'',
                                'por_invoiceimg'=>''
                                );

      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo2);    
     //----------------  end pagination setting ------------------------// 


    }
    
    $this->load->view('helper/header');
    $this->load->view('report/salesreportdata',$records);   
  }

  public function inventoryreport(){

    if(!empty($_GET['filter_by']))
    { 
         $parameter         = array('act_mode'=>'viewmappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>'%'.$_GET['searchname'].'%','atttextname'=>$_GET['filter_by'],'catidd'=>'');
         $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);


         //----------------  Download  Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('SKU Number','Product Name','Category','Brand','QTY','MRP','Selling Price');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Inventory Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->SKUNO);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->ProductName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->CatName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->BrandName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->proQnty);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->productMRP);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->SellingPrice);
           
            }
          }

          $filename='InventoryReport.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------// 



      //----------------  start pagination configuration ------------------------// 

     $config['base_url']         = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
     $config['total_rows']       = count($responce['vieww']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

      //----------------  end pagination configuration ------------------------// 
     
    
        $parameter         = array('viewmappro',$page,'','%'.$_GET['searchname'].'%',$_GET['filter_by'],$second);
        $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
    }else{

      $parameter            =array('act_mode'=>'apprview','row_id'=>'','catid'=>'');
      $responce['vieww']  = $this->supper_admin->call_procedure('proc_product',$parameter);
      //----------------  start pagination ------------------------// 



      //----------------  Download  Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('SKU Number','Product Name','Category','Brand','QTY','MRP','Selling Price');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Inventory Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->SKUNO);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->ProductName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->CatName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->BrandName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->proQnty);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->productMRP);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->SellingPrice);
           
            }
          }

          $filename='InventoryReport.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------// 

      $config['base_url']         = base_url()."admin/report/inventoryreport?";
      $config['total_rows']       = count($responce['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

    $parameter            =array('act_mode'=>'apprview','row_id'=>$page,'catid'=>$second);
    $responce['vieww']  = $this->supper_admin->call_procedure('proc_product',$parameter); 
    //----------------  end pagination ------------------------// 
  }

    $this->load->view('helper/header');
    $this->load->view('report/inventoryreportdata',$responce);
    
  }



  public function sales()
  { 
 	 $parameter=array('act_mode'=>'sales','row_id'=>'','pro_id'=>'');
 	 $data['record'] = $this->supper_admin->call_procedure('proc_report',$parameter);

 	//----------------  Download Newsletter Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Product Name','Retailer','Retailer ID','Invoice No','Invoice Date','Item Code','Qty','Unit Price','Amount','Order Date','Discount','Tax/Vat','Tax/CST','Tax/Entry','Total Tax','Mode Of Delivery');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Sales Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($data['record'] as $key => $value) {
             
            $newvar = $j+$key;

  

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->ProName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->ShippingName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->Email);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->OrderNumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->OrderDate);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->SkuNumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->OrdQty);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->FinalPrice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->amount);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->CreatedOn);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->TotalDiscount);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->protaxvat_p);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->protaxcst_p);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->protaxentry_p);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $value->taxtotal);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->PaymentMode);
            
                       
            }
          }

          $filename='Sales Report.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Newsletter Excel ------------------------// 

 	//----------------  start pagination setting ------------------------// 

     $config['base_url']         = base_url()."admin/report/sales?";
     $config['total_rows']       = count($data['record']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $data["links"]  = explode('&nbsp;',$str_links );

     $parameter=array('act_mode'=>'sales','row_id'=>$page,'pro_id'=>$second);
 	   $data['record'] = $this->supper_admin->call_procedure('proc_report',$parameter); 
     
     //----------------  end pagination setting ------------------------// 
     //p($data);exit();
  	$this->load->view('helper/header');
 	  $this->load->view('report/sales_report',$data);
 }
 /*function po()
 {
    $this->load->view('helper/header');
    $this->load->view('report/purchase_report',$data);
 }*/

 function po()
 {
    $this->load->view('helper/header');

    $parameter=array('act_mode'=>'return','row_id'=>'','pro_id'=>'');
 	  $data['record'] = $this->supper_admin->call_procedure('proc_report',$parameter); 
   	 
     if(!empty($this->input->post('newsexcel')))
          {
           

           $finalExcelArr = array('Order no','Product Description','Qty','Item Code','Unit Price','Item Code','Amount','Vat/Vat','Vat/CST','Vat/entry','Tax Total','Return Date','Mod Of Return','Retun Reason');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Return Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($data['record'] as $key => $value) {
             
            $newvar = $j+$key;

  

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->OrderNumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->OrderDate);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->description);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->ProQty);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->SkuNumber);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->base_prize);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->amount);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->protaxvat_p);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->protaxentry_p);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->protaxentry_p);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->taxtotals);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->CreatedOn);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->PaymentMode);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->Remark);
          
                       
            }
          }

          $filename='Purchase Return Report.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }

   	 $this->load->view('report/purchase_report',$data);
 }

 public function returnreport(){

        $parameterpo    = array('act_mode'=>'salesreportdata',
                              'row_id'=>'',
                              'por_number'=>$_REQUEST['start_date'],
                              'por_challan'=>$_REQUEST['end_date'],
                              'por_venid'=>'',
                              'por_veninvoice'=>'',
                              'por_totqty'=>'',
                              'por_billrate'=>'',
                              'por_billamt'=>'',
                              'por_billdiscount'=>'',
                              'por_netamt'=>'',
                              'por_tax'=>'',
                              'por_totamt'=>'',
                              'por_invoiceimg'=>''
                              );
      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo);


    if(!empty($this->input->post('newsexcel')))
          {


           $finalExcelArr = array('Purchase Order number','Supplier Name','Supplier Invoice number','Discount (If any)','Net Amount Exclusive of VAT/CST','VAT/CST @ 2%/5%/12.5%','Total Amount Inclusive of VAT/CST','Date of Purchase');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Return Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($records['viewporeport'] as $key => $value) {
             
            $newvar = $j+$key;

              //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->pono);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->firstname.' '.$value->lastname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->poveninvoiceno);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->podiscount);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->ponetamt);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->potax);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->pototalamt);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->porecreatedon);
                                  
            }
          }

          $filename='Return order.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }

    $this->load->view('helper/header');
    $this->load->view('report/returnreportdata',$records);
    
  }

  public function purchasereportbc(){

    if(isset($_REQUEST['submit'])) {
      $parameterpo    = array('act_mode'=>'viewporec',
                              'row_id'=>'',
                              'por_number'=>$_REQUEST['start_date'],
                              'por_challan'=>$_REQUEST['end_date'],
                              'por_venid'=>'',
                              'por_veninvoice'=>'',
                              'por_totqty'=>'',
                              'por_billrate'=>'',
                              'por_billamt'=>'',
                              'por_billdiscount'=>'',
                              'por_netamt'=>'',
                              'por_tax'=>'',
                              'por_totamt'=>'',
                              'por_invoiceimg'=>''
                              );
      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo);
    } else {
      $parameterpo    = array('act_mode'=>'viewporec',
                              'row_id'=>'',
                              'por_number'=>'',
                              'por_challan'=>'',
                              'por_venid'=>'',
                              'por_veninvoice'=>'',
                              'por_totqty'=>'',
                              'por_billrate'=>'',
                              'por_billamt'=>'',
                              'por_billdiscount'=>'',
                              'por_netamt'=>'',
                              'por_tax'=>'',
                              'por_totamt'=>'',
                              'por_invoiceimg'=>''
                              );
      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo);
    }
    if(!empty($this->input->post('newsexcel')))
    {
           $finalExcelArr = array('Item Received (SKU) code','Item Name','Date of Purchase','Purchase Order number','Challan Number','Supplier Name','Supplier Invoice No.','Quantity Received','Bill Rate','Bill Amount','Discount (If any)','Net Amount Exclusive of VAT/CST','VAT/CST @ 2%/5%/12.5%','Total Amount Inclusive of VAT/CST');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('purchase Report');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($records['viewporeport'] as $key => $value) {
              $newvar = $j+$key;
              $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);   
              $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->SKUNO);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->ProductName);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->porecreatedon);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->pono);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->pochallanno);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->pochallanno);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->firstname.' '.$value->lastname);           
              $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->poveninvoiceno);            
              $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->pototalqty);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->pototalqty);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->pototalqty);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->podiscount);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->ponetamt);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->potax);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $value->pototalamt);                     
            }
          }

          $filename='Purchase order.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
         
    }
    //----------------  start pagination setting ------------------------// 
     /*$config['base_url']         = base_url()."admin/report/purchasereport?";
     $config['total_rows']       = count($data['viewporeport']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $data["links"]  = explode('&nbsp;',$str_links );
     $parameterpo2    = array('act_mode'=>'viewporec',
                                'row_id'=>$page,
                                'por_number'=>'',
                                'por_challan'=>'',
                                'por_venid'=>$second,
                                'por_veninvoice'=>'',
                                'por_totqty'=>'',
                                'por_billrate'=>'',
                                'por_billamt'=>'',
                                'por_billdiscount'=>'',
                                'por_netamt'=>'',
                                'por_tax'=>'',
                                'por_totamt'=>'',
                                'por_invoiceimg'=>''
                                );

      $records['viewporeport'] = $this->supper_admin->call_procedure('proc_orderporeceive',$parameterpo2);  */   
     //----------------  end pagination setting ------------------------// 
    $this->load->view('helper/header');
    $this->load->view('report/purchasereportdata',$records);    
  }

}
?>