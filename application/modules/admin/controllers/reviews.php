<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reviews extends MX_Controller {

   public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper("my_helper");
   // $this->load->model('rating_model');
    $this->load->library('upload');
  }

  public function index()
  {


  	    if($this->input->post('submit')){
     foreach ($this->input->post( 'attdelete') as $key => $value) {
     // $parameter         = array('act_mode'=>'delete','row_id'=>$value,'brandname'=>'','brandimage'=>'','');
    //  $response['vieww'] = $this->supper_admin->call_procedure('proc_brand',$parameter); 
	 $review=array(
            'act_mode'=>'reviewdelete',
            'row_id'=>$value,
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>''
           
          );
         $response['reviewdata'] = $this->supper_admin->call_procedure('proc_ten', $review); 
 }
      $this->session->set_flashdata("message", "Your information was successfully delete.");
      redirect("admin/reviews/index");
     }



  	 $review=array(
            'act_mode'=>'reviewdetails',
            'row_id'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>''
           
          );
         // pend($images);
         $response['reviewdata'] = $this->supper_admin->call_procedure('proc_ten', $review); 
        // p( $response['reviewdata']);
         $this->load->view('helper/header');
  	$this->load->view('reviews/index',$response);

  }

//............. Change  Status ............... //
public function reviewsstatus (){
  $rowid             = $this->uri->segment(4);
  $status            = $this->uri->segment(5);
  $act_mode          = $status=='2'?'activereview':'inactivereview';
  $userid       = $this->session->userdata('bizzadmin')->LoginID;
  $parameter         = array('act_mode'=>$act_mode,'row_id'=>$rowid,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>$userid);
  $response['vieww'] = $this->supper_admin->call_procedure('proc_category',$parameter); 

  	 $review=array(
            'act_mode'=>$act_mode,
            'row_id'=>$rowid,
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>''
           
          );
         
         $response['reviewdata'] = $this->supper_admin->call_procedure('proc_ten', $review); 
//p( $review); exit;
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/reviews/index');

} //End function.

public function reviewsupdate (){
$rowid=$this->uri->segment(4);
 $review=array(
            'act_mode'=>'displayreview',
            'row_id'=>$rowid,
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>''
           
          );
         
         $response['reviewdata'] = $this->supper_admin->call_procedurerow('proc_ten', $review); 
        // p($response['reviewdata']);
  $this->load->view('helper/header');
  	$this->load->view('reviews/reviewupdate',$response);
	}


}