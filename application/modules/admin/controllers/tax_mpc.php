<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Tax_mpc extends MX_Controller {

  public function __construct() {    
      $this->load->model("supper_admin");
      $this->load->helper('my_helper');
      $this->userfunction->loginAdminvalidation();    
  } 

  public function addtax() {

      if($this->session->userdata('popcoin_login')->s_usertype!=1) {
        redirect(base_url().'admin/login');
      }
      if($this->input->post('submit')) 
      {
        $this->form_validation->set_rules('tax_type','Tax type','trim|required');
        //$this->form_validation->set_rules('tax_name','Tax name','trim|required');
        //$this->form_validation->set_rules('tax_value','Tax value','trim|required');
        if($this->form_validation->run() != false) {
            $parameter = array(
                        'act_mode' => 'checktax', 
                        'v_id' => '',
                        'v_status' => $this->input->post('tax_type'),
                        'v_mobile' => '',
                        'v_email' => '',
                        'v_pwd' => '',
                        'tin_no' => '',
                        'tax_no' => '',
                        'bank_name' => '',
                        'bank_addr' => '',
                        'ifsc_code' => '',
                        'bene_accno' => '',
                        'param1' => '',
                        'param2' => ''
                      );
            
            //p($parameter); exit;

            $res = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);
            


            if($res->cou_rec>0) {
              $this->session->set_flashdata('message', 'This type of tax already exists!');
              redirect("admin/tax_mpc/addtax");
            } else { 
              $parameter = array(
                        'act_mode' => 'inserttax', 
                        'v_id' => '',
                        'v_status' => $this->input->post('tax_type'),
                        'v_mobile' => $this->input->post('tax_value'),
                        'v_email' => '',
                        'v_pwd' => '',
                        'tin_no' => '',
                        'tax_no' => '',
                        'bank_name' => '',
                        'bank_addr' => '',
                        'ifsc_code' => '',
                        'bene_accno' => '',
                        'param1' => '',
                        'param2' => ''                      
                      );
              $response = $this->supper_admin->call_procedure('proc_vendor', $parameter);
              $this->session->set_flashdata('message', 'Your information has been saved successfully!');
              redirect("admin/tax/viewtaxes");
          }
        }
      }
      $this->load->view('helper/header');
      $this->load->view('tax/addtax');
  } 

  public function viewtaxes() {

      if($this->session->userdata('popcoin_login')->s_usertype!=1) {
        redirect(base_url().'admin/login');
      }
      $parameter = array(
                    'act_mode' => 'viewtaxes', 
                    'v_id' => '',
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );    
      $response['vieww'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);   
      $this->load->view('helper/header');      
      $this->load->view('tax/viewtaxes',$response);
  }

  public function updatetax($id) {

      if($this->session->userdata('popcoin_login')->s_usertype!=1) {
        redirect(base_url().'admin/login');
      }
      if($this->input->post('submit')) 
      {
        //$this->form_validation->set_rules('tax_type','Tax type','trim|required');
        //$this->form_validation->set_rules('tax_name','Tax name','trim|required');
        $this->form_validation->set_rules('tax_value','Tax value','trim|required');
        if($this->form_validation->run() != false) {
            /*$parameter = array(
                        'act_mode' => 'checktaxupdate', 
                        'v_id' => $id,
                        'v_status' => $this->input->post('tax_type'),
                        'v_mobile' => '',
                        'v_email' => '',
                        'v_pwd' => '',
                        'tin_no' => '',
                        'tax_no' => '',
                        'bank_name' => '',
                        'bank_addr' => '',
                        'ifsc_code' => '',
                        'bene_accno' => '',
                        'param1' => '',
                        'param2' => ''
                      );
            $res = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);
            if($res->cou_rec>0) {
              $this->session->set_flashdata('message', 'This type of tax already exists!');
              redirect("admin/tax/updatetax/".$id);
            } else {  */
              $parameter = array(
                        'act_mode' => 'updatetax', 
                        'v_id' => $id,
                        'v_status' => $this->input->post('tax_type'),
                        'v_mobile' => $this->input->post('tax_value'),
                        'v_email' => '',
                        'v_pwd' => '',
                        'tin_no' => '',
                        'tax_no' => '',
                        'bank_name' => '',
                        'bank_addr' => '',
                        'ifsc_code' => '',
                        'bene_accno' => '',
                        'param1' => '',
                        'param2' => ''                      
                      );
              $response = $this->supper_admin->call_procedure('proc_vendor', $parameter);
              $this->session->set_flashdata('message', 'Your information has been updated successfully!');
              redirect("admin/tax/viewtaxes");
          //}
        }
      }
      $parameter = array(
                        'act_mode' => 'fetchtaxdetail', 
                        'v_id' => $id,
                        'v_status' => '',
                        'v_mobile' => '',
                        'v_email' => '',
                        'v_pwd' => '',
                        'tin_no' => '',
                        'tax_no' => '',
                        'bank_name' => '',
                        'bank_addr' => '',
                        'ifsc_code' => '',
                        'bene_accno' => '',
                        'param1' => '',
                        'param2' => ''
                      );
      $response['vieww'] = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);
     
      //p($response['vieww']); exit;

      $this->load->view('helper/header');
      $this->load->view('tax/updatetax',$response);
  } 

public function storelistdetails() {
     
         $parameter = array(
                    'act_mode' => 'billgeneratedetails', 
                    'v_id' => $this->uri->segment(6),
                    'v_status' => $this->uri->segment(4),
                    'v_mobile' => $this->uri->segment(5),
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
//p($response['amountdetails']);
        $parameter1 = array(
                    'act_mode' => 'storepayment1', 
                    'v_id' => $this->uri->segment(6),
                    'v_status' => $this->uri->segment(4),
                    'v_mobile' => $this->uri->segment(5),
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );

        //p($parameter1);
        $response['single'] = $this->supper_admin->call_procedureRow('proc_vendor', $parameter1);
//p($response['single']);


$this->load->view('helper/header');      
$this->load->view('retailer_pay_transaction_details',$response);
}

public function storewise_check_transaction() {
     
      
        $param=array('act_mode' => 'store_wise_totalamt',
                    'Param'     => $this->uri->segment(8),
                    'Param1'    => $this->uri->segment(5),
                    'Param2'    => $this->uri->segment(6),
                    'Param3'    => '',
                    'Param4'    => '',
                    'Param5'    => '',
                    'Param6'    => '',
                    'Param7'    => '',
                    'Param8'    => $this->uri->segment(4),
                    'Param9'    => '',
                    'Param10'   => '');
        //p($param);exit;
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_transaction', $param);
//p($response['amountdetails']);
       


$this->load->view('helper/header');      
$this->load->view('storewise_check_transaction',$response);
}


  public function statustax($id,$status) {

      if($this->session->userdata('popcoin_login')->s_usertype!=1) {
        redirect(base_url().'admin/login');
      }
      $act_mode = $status=='1'?'taxdeactive':'taxactive'; 
      $parameter = array(
                        'act_mode' => $act_mode, 
                        'v_id' => $id,
                        'v_status' => '',
                        'v_mobile' => '',
                        'v_email' => '',
                        'v_pwd' => '',
                        'tin_no' => '',
                        'tax_no' => '',
                        'bank_name' => '',
                        'bank_addr' => '',
                        'ifsc_code' => '',
                        'bene_accno' => '',
                        'param1' => '',
                        'param2' => ''
                      );
      $response = $this->supper_admin->call_procedure('proc_vendor', $parameter);
      $this->session->set_flashdata('message', 'Status has been changed successfully!');
      redirect("admin/tax/viewtaxes");
  } 

  public function deletetax($id) {

      if($this->session->userdata('popcoin_login')->s_usertype!=1) {
        redirect(base_url().'admin/login');
      }
      $parameter = array(
                        'act_mode' => 'deletetax', 
                        'v_id' => $id,
                        'v_status' => '',
                        'v_mobile' => '',
                        'v_email' => '',
                        'v_pwd' => '',
                        'tin_no' => '',
                        'tax_no' => '',
                        'bank_name' => '',
                        'bank_addr' => '',
                        'ifsc_code' => '',
                        'bene_accno' => '',
                        'param1' => '',
                        'param2' => ''
                      );
      $response = $this->supper_admin->call_procedure('proc_vendor', $parameter);
      $this->session->set_flashdata('message', 'Record has been deleted successfully!');
      redirect("admin/tax/viewtaxes");
  } 

public function paymentcycle() {

      if($this->input->post('paysubmit'))
      {
          $this->form_validation->set_rules('payname','Payment Cycle Name','trim|required');
          $this->form_validation->set_rules('paydate','Payment Cycle Day ','trim|required');
          $this->form_validation->set_rules('todate','To Day 7','trim|required');
          
          if($this->form_validation->run() != false) {

        $parameter = array(
                    'act_mode' => 'addpaymentcycle', 
                    'v_id' => '',
                    'v_status' => $this->input->post('payname'),
                    'v_mobile' => $this->input->post('paydate'),
                    'v_email' => $this->input->post('todate'),
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );   

          $response = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);  

          $this->session->set_flashdata('message', 'Your information has been saved successfully!');
          redirect("admin/tax_mpc/paymentcycle");
        }
      }

      if($this->input->post('interestsubmit'))
      {
          $this->form_validation->set_rules('interestvalue','Enter Interest','trim|required');
          
          if($this->form_validation->run() != false) {

        $parameter = array(
                    'act_mode' => 'addinterest', 
                    'v_id' => '',
                    'v_status' => $this->input->post('interestvalue'),
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );   

          $response = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);  

          $this->session->set_flashdata('message', 'Your information has been saved successfully!');
          redirect("admin/tax_mpc/paymentcycle");
        }
      }

      $parameter = array(
                    'act_mode' => 'viewpaymentcycle', 
                    'v_id' => '',
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );   

      $response['vieww'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);  
     // p($response['vieww']);exit; 
      $this->load->view('helper/header');      
      $this->load->view('paymentcycle',$response);
  }

  public function paymentcycledelete() {

      $parameter = array(
                    'act_mode' => 'deletepaymentcycle', 
                    'v_id' => $this->uri->segment(4),
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );  
//p($parameter);exit;
          $response = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);  

          $this->session->set_flashdata('message', 'Your paymentcycle has been remove successfully!');
          redirect("admin/tax/paymentcycle");
       
  }

  public function paymentprocessstore() {
     
         $parameter = array(
                    'act_mode' => 'storepayment', 
                    'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response1 = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);
//p($response1);
        $parameter1 = array(
                    'act_mode' => 'storepayment1', 
                    'v_id' => $response1->retailer_id,
                    'v_status' => $response1->txn_id,
                    'v_mobile' => $response1->cycle_id,
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );

        //p($parameter1);
        $response2 = $this->supper_admin->call_procedureRow('proc_vendor', $parameter1);
//p($response2);

        $parameter = array(
                    'act_mode' => 'getinterest', 
                    'row_id' => '',
                    'counname' => '',
                    'coucode' => '',
                    'commid' => ''
                  );
        $interest = $this->supper_admin->call_procedureRow('proc_master', $parameter);
//p($interest->i_value);
@$response['amount']=array('t_id'=>$response2->t_id,'curr_status'=>$response2->curr_status,'from_date'=>$response2->from_date,'to_date'=>$response2->to_date,'pay_date'=>$response2->pay_date,'s_name'=>$response1->rid,'store_count'=>$response1->s_name,'company_name'=>$response1->company_name,'pay_type'=>$response2->pay_type,'pay_amt'=>$response1->pay_amt,'remaining_amt'=>$response1->remaining_amt,'t_createdon'=>$response2->t_createdon,'txn_id'=>$response2->txn_id,'tamt'=>$response2->pay_amt,'interest_amt'=>$response2->interest_amt,'remain_intamt'=>$response2->remain_intamt,'intvalue'=>$interest->i_value,'active'=>$response1->payment_status);

        if(isset($_POST['save_data'])) {
          //pend($_REQUEST);


          $curdate=date('Y-m-d');
          $pdate=$_REQUEST['paydate'];
          $date1 = new DateTime($pdate);
          $date2 = new DateTime($curdate);
          $diff = $date2->diff($date1)->format("%a");


          $intamt=$_REQUEST['intamt'];
          $intamtbefore=$_REQUEST['intamtbefore'];
          $intval=$interest->i_value*$diff;

      if($intamt!=0)
      {    
          if($_REQUEST['pay_amt']>=$_REQUEST['payment'])
          {
            
                   if($_REQUEST['pay_amt']==0)
                    { 
                        $check='1';
                        $paidint=round(($_REQUEST['payment']),4);
                        $int=$intamt+$intamtbefore;
                        //echo $paidint;
                        if($int==$paidint)
                        {
                            
                            $amount_paid=0;
                            $intamt1=$int;
                            $intamt_paid='00';
                           
                            
                        }
                        else
                        {
                            
                            $amount_paid=0;
                            $amount_paid1=$_REQUEST['payment'];
                           
                            $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                            $intamt_paid='';
                            
                        }

                    }
                    else
                    {

                            $paidint=$_REQUEST['pay_amt']-$_REQUEST['payment'];
                            
                                $check='0';
                                $amount_paid=0;
                                $amount_paid1=$_REQUEST['payment'];
                                //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                                $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                                $intamt_paid='';
                                $amount=$_REQUEST['pay_amt'];
                                $paidper=100-(($paidint)*100/$amount);
                            

                            $paidamt=$_REQUEST['payment'];
                            
                    }      
          }
          else 
          {  
             
              if($_REQUEST['pay_amt']==$response1->pay_amt)
              { 
                  $check='0';
                  $amount_paid=0;
                 
                  $paidint=round(($_REQUEST['payment']-$_REQUEST['pay_amt']),4);
                  $int=$intamt+$intamtbefore;
                  //echo $paidint;
                  if($int==$paidint)
                  {
                      //$check='-0';
                      $amount_paid=0;
                      $intamt1=$int;
                      $intamt_paid='00';
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount)*100/$amount);
                      
                  }
                  else
                  {
                      //$check='00';
                      $amount_paid=0;
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount)*100/$amount);
                  }

                  $paidamt=$_REQUEST['payment']-$paidint;
              }
              else
              {
                  $check='0';
                  $amount_paid=0;
                  
                  $paidint=round(($_REQUEST['payment']-$_REQUEST['pay_amt']),4);
                  $int=$intamt+$intamtbefore;
                  //echo $paidint;
                  if($int==$paidint)
                  {
                      //$check='-0';
                      $amount_paid=0;
                      $intamt1=$int;
                      $intamt_paid='00';
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount)*100/$amount);
                      
                  }
                  else
                  {
                      //$check='00';
                      $amount_paid=0;
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount)*100/$amount);
                  }

                  $paidamt=$_REQUEST['payment']-$paidint;
              }
             
          }


         /* $amount_paid=$_REQUEST['payment'];
          $amount=$_REQUEST['pay_amt'];
          $paidper=100-(($amount-$amount_paid)*100/$amount);*/
          $mapstatus = 'failed';       
          if($response1->remaining_amt=="0") 
          {
              if($response2->remaining_amt=='')
              {
                  //$remaining=$response2->pay_amt-$_REQUEST['payment'];
                  $remaining=$response2->pay_amt-$paidamt;
              }
              else
              {
                  //$remaining=$response2->remaining_amt-$_REQUEST['payment'];
                   $remaining=$response2->remaining_amt-$paidamt; 
              }
          } 
          else 
          {
              //$remaining=$response2->remaining_amt-$_REQUEST['payment']; 
              $remaining=$response2->remaining_amt-$paidamt;         
          }    
}
else
{
    $check=0;
    $amount_paid=$_REQUEST['payment'];
    $amount=$_REQUEST['pay_amt'];
    $paidper=100-(($amount-$amount_paid)*100/$amount);
    $mapstatus = 'failed'; 
    $intamt1='';
    $intamt_paid='';      
    if($response1->remaining_amt=="0") 
    {
        if($response2->remaining_amt=='')
        {
            $remaining=$response2->pay_amt-$_REQUEST['payment'];
            
        }
        else
        {
            $remaining=$response2->remaining_amt-$_REQUEST['payment'];
           
        }
    } 
    else 
    {
        $remaining=$response2->remaining_amt-$_REQUEST['payment']; 
             
    }    


}



          $response1->store_id; 
          $response2->cycle_id;
          $response2->txn_id;
          $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $_REQUEST['t_id'],
                    'param1' => $response2->txn_id,
                    'param2' => $response2->cycle_id,
                    'param3' => $response2->retailer_id,
                    'param4' => $response2->store_count,
                    'param5' => $response2->from_date,
                    'param6' => $response2->to_date,
                    'param7' => $response2->pay_date,
                    'param8' => $response2->pay_type,
                    'param9' => $remaining,
                    'param10' => $_REQUEST['txn_id'],
                    'param11' => $mapstatus,
                    'param12' => $response2->pay_amt,
                    'param13' => $response2->cycle_day,
                    'param14' => $intamt1,
                    'param15' => $intamt_paid,
                    'param16' => '',//
                    'param17' => $paidper,//
                    'param18' => $response1->remaining_amt//
                  );     

                  //p($param2);exit;                
            $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param2);
            //p($res2);exit;
         if($check==0) //check
         {     
            foreach($res2 as $val) {
            if($val->store_id==$response1->store_id) {
                //$process_amt=($val->pay_amt*$paidper)/100;

                if($val->process_amt=='')
                {
                    $process_amt=($val->pay_amt*$paidper)/100;
                }
                else
                {
                    $a=$val->pay_amt-$val->process_amt;
                    $process_amt=($a*$paidper)/100;
                }

                $param = array(
                      'act_mode' => 'updatetranssuccessdata',
                      'row_id' => $val->t_id,
                      'param1' => $val->txn_id,
                      'param2' => $val->tran_map_id,
                      'param3' => $val->cycle_id,
                      'param4' => $val->receipts_id,
                      'param5' => $val->order_id,
                      'param6' => $val->retailer_id,
                      'param7' => $val->store_id,
                      'param8' => $val->deal_id,
                      'param9' => $val->user_id,
                      'param10' => $val->user_type,
                      'param11' => $val->pay_type,
                      'param12' => $val->tax_type,
                      'param13' => $val->tax_amt,
                      'param14' => $val->total_amt,
                      'param15' => $val->pay_amt,
                      'param16' => $process_amt+$val->process_amt,
                      'param17' => $mapstatus,
                      'param18' => $response1->store_id
                    );      
                    //p($param);       
                $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
                if($val->user_type=='C') {
                  $param = array(
                      'act_mode' => 'insertcustomercashback', 
                      'row_id' => '',
                      'param1' => $val->txn_id,
                      'param2' => $val->tran_map_id,
                      'param3' => $val->cycle_id,
                      'param4' => $val->receipts_id,
                      'param5' => $val->order_id,
                      'param6' => $val->retailer_id,
                      'param7' => $val->store_id,
                      'param8' => $val->deal_id,
                      'param9' => $val->user_id,
                      'param10' => $val->user_type,
                      'param11' => $val->pay_type,
                      'param12' => $val->tax_type,
                      'param13' => $val->tax_amt,
                      'param14' => $val->total_amt,
                      'param15' => $val->pay_amt,
                      'param16' => $process_amt,
                      'param17' => $val->tax_type,
                      'param18' => ''
                    ); 
                        //p($param);         
                  $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
                }
              }         
              //$this->session->set_flashdata('message', 'Your transaction has been successful - '.$_REQUEST['txn_id'].' (TXN ID)!');
              //redirect(base_url().'admin/tax/paymentprocess');
              //redirect(base_url().'admin/receipts/paymentgateway2/'.$_REQUEST['txn_id']);
          } 
        } //check
        //exit;
          redirect(base_url().'admin/receipts/paymentgateway/'.$_REQUEST['txn_id']);
        }


      $this->load->view('helper/header');      
      $this->load->view('pay_transaction_store',$response);
  }

  public function paymentprocess() {
     
         $parameter = array(
                    'act_mode' => 'retailerpaymentprocess121', 
                    'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
        $mailsms = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);

        $parameter = array(
                    'act_mode' => 'getinterest', 
                    'row_id' => '',
                    'counname' => '',
                    'coucode' => '',
                    'commid' => ''
                  );
        $response['interest'] = $this->supper_admin->call_procedureRow('proc_master', $parameter);
//p($response['interest']);

        if(isset($_POST['save_data'])) {
          //pend($_REQUEST);

          $curdate=date('Y-m-d');
          $pdate=$_REQUEST['paydate'];
          $date1 = new DateTime($pdate);
          $date2 = new DateTime($curdate);
          $diff = $date2->diff($date1)->format("%a");


          $intamt=$_REQUEST['intamt'];
          $intamtbefore=$_REQUEST['intamtbefore'];
          $intval=$response['interest']->i_value*$diff;
          if($_REQUEST['pay_amt']>=$_REQUEST['payment'])
          {
              if($intamt==0)
              {
                 $check=0;
                 $amount_paid=$_REQUEST['payment'];
                 $intamt1='';
                 $intamt_paid='';
                 $amount=$_REQUEST['pay_amt'];
                 $paidper=100-(($amount-$amount_paid)*100/$amount);
              }
              else
              {
                 $check=0;
                 $amount_paid=$_REQUEST['payment'];

                  $paidint=$_REQUEST['payment'];
                  $int=$intamt+$intamtbefore;
                  if($int==$paidint)
                  {
                      $intamt1=round(((($amount_paid*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='0';
                  }
                  else
                  {
                     //$intamt1=(($amount_paid*$intval)/100)+$intamtbefore;
                     $intamt1=round(((($amount_paid*$intval)/100)/365),4)+$intamtbefore;
                     $intamt_paid='';
                  }  
                 $amount=$_REQUEST['pay_amt'];
                 $paidper=100-(($amount-$amount_paid)*100/$amount);
              }

          }
          else 
            //if($_REQUEST['pay_amt']<$_REQUEST['payment'])
          { //echo 'hi';
              if($_REQUEST['pay_amt']==0)
              {
                  $check=1;
                  $amount_paid=0;
                  //$intamt_paid=$_REQUEST['payment'];//102-100
                  $paidint=$_REQUEST['payment'];
                  $int=$intamt+$intamtbefore;
                  if($int==$paidint)
                  {
                      $check=1;
                      $amount_paid=0;
                      $intamt1=$int;
                      $intamt_paid='0';
                  }
                  else
                  {
                      $check=1;
                      $amount_paid=0;
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                  }
              }
              else
              {
                  $paidint=round(($_REQUEST['payment']-$_REQUEST['pay_amt']),4);
                  $int=$intamt+$intamtbefore;
                  //echo $int;
                  if($paidint=$int)
                  {  //echo 'hi1';
                      $check=0;
                      $amount_paid=$_REQUEST['pay_amt'];
                      $intamt1=$int;
                      $intamt_paid='0';
                      //$intamt_paid=$_REQUEST['payment']-$_REQUEST['pay_amt'];//102-100
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount_paid)*100/$amount);
                  }
                  else
                  { //echo 'hi11';
                      $check=0;
                      $amount_paid=$_REQUEST['pay_amt'];
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                      //$intamt_paid=$_REQUEST['payment']-$_REQUEST['pay_amt'];//102-100
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount_paid)*100/$amount);
                  }
              }
          }
          
          //echo $check;
          //echo $amount_paid;
         // echo $intamt_paid;
          
        
          $mapstatus = 'failed';
          //exit;
          $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $_REQUEST['t_id'],
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
                            
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);

          if($data->remaining_amt=="") {
            //$remaining=$data->pay_amt-$_REQUEST['payment'];
            $remaining=$data->pay_amt-$amount_paid;
          } else {
            //$remaining=$data->remaining_amt-$_REQUEST['payment'];
            $remaining=$data->remaining_amt-$amount_paid;
          }          
          $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $remaining,
                    'param10' => $_REQUEST['txn_id'],
                    'param11' => $mapstatus,
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $intamt1,
                    'param15' => $intamt_paid,
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
            //p($param2);exit;                  
            $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param2);
            //p($res2);
     if($check==0)//  Interest Amt   
     {
          foreach($res2 as $val) {
            if($val->process_amt=='')
            {
                $process_amt=($val->pay_amt*$paidper)/100;
            }
            else
            {
                $a=$val->pay_amt-$val->process_amt;
                $process_amt=($a*$paidper)/100;
            }
            
            $param = array(
                  'act_mode' => 'updatetranssuccessdata', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt+$val->process_amt,
                  'param17' => $mapstatus,
                  'param18' => $paidper//
                );  
                 //p($param);          
            $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            }
          }


          }//  Interest Amt    

          /*--------------------------SMS/EMAIL------------------------*/
          $email=$mailsms->s_loginemail;
          $mobile=$mailsms->contact_no;
          $amt=($mailsms->pay_amt)-$amount_paid;
           $msg=urlencode('DEAR MPCMEMBER, Pay for '.$mailsms->s_name.'-'.$mailsms->company_name.' is Pay Rs '.$amount_paid.' .RefID- '.$mailsms->txn_id.'. Pymt due Rs '.$amt);

          $url='http://sms.tattler.co/app/smsapi/index.php?key=558709407EE8FE&routeid=288&type=text&contacts='.$mobile.'&senderid=WEBPRO&msg='.$msg; 
                //echo $url; 
          #file_get_contents($url);
              //print_r(file_get_contents($url));

                  $content='<html>
                   <body bgcolor="#DCEEFC">
                          <br><div> 
                        <table><tr><td>DEAR MPCMEMBER, Pay for '.$mailsms->s_name.' ('.$mailsms->company_name.') is Pay Rs '.$amount_paid.' .RefID- '.$mailsms->txn_id.'. Pymt due Rs '.$amt.'.</td> </tr>
                          
                          <tr><td>Regards,</br>
                          MPC
                        
                          </table>
                         </div>
                   </body>
              </html>';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('support@mindzshop.com', 'Mypopcoins');
                  $this->email->to($email);
                  //$this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Pay Bill');
                  $this->email->message($content);
                  #$this->email->send();

          /*--------------------------SMS/EMAIL------------------------*/

          //exit;  
          //$this->session->set_flashdata('message', 'Your transaction has been successful - '.$_REQUEST['txn_id'].' (TXN ID)!');
          //redirect(base_url().'admin/tax/paymentprocess');
          redirect(base_url().'admin/receipts/paymentgateway/'.$_REQUEST['txn_id']);
      }


      $this->load->view('helper/header');      
      //$this->load->view('pay_transaction',$response);
      $this->load->view('transaction/retailerpay_transaction',$response);
      
  }

  public function paymentprocess_old() {
      if($this->input->get('store_id')!="") {
        if($this->input->get('month')!="") {
          $array=explode("-",$this->input->get('month'));
          $month=$array[0];
          $year=$array[1];
        } else {
          $month="";
          $year="";
        }    
        $dateObj   = DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F');
        $start = date("d",strtotime('first day of '.$monthName, time()));
        $dateObj   = DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F');
        $end = date("d",strtotime('last day of '.$monthName, time()));
        $sdate = $year.'-'.$month.'-'.$start.' 00:00:00';
        $edate = $year.'-'.$month.'-'.$end.' 00:00:00';
        $parameter = array(
                      'act_mode' => 'retailerpaymentprocess', 
                      'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                      'v_status' => $this->input->get('store_id'),
                      'v_mobile' => $sdate,
                      'v_email' => $edate,
                      'v_pwd' => '',
                      'tin_no' => '',
                      'tax_no' => '',
                      'bank_name' => '',
                      'bank_addr' => '',
                      'ifsc_code' => '',
                      'bene_accno' => '',
                      'param1' => '',
                      'param2' => ''
                    );
        $response['view'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
        $parameter = array(
                    'act_mode' => 'retailerpaymentprocess1', 
                    'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => $this->input->get('store_id'),
                    'v_mobile' => $sdate,
                    'v_email' => $edate,
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter); 
      } else {
        $parameter = array(
                      'act_mode' => 'retailerpaymentprocess', 
                      'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                      'v_status' => '',
                      'v_mobile' => '',
                      'v_email' => '',
                      'v_pwd' => '',
                      'tin_no' => '',
                      'tax_no' => '',
                      'bank_name' => '',
                      'bank_addr' => '',
                      'ifsc_code' => '',
                      'bene_accno' => '',
                      'param1' => '',
                      'param2' => ''
                    );
        $response['view'] = $this->supper_admin->call_procedure('proc_vendor', $parameter); 
        $parameter = array(
                    'act_mode' => 'retailerpaymentprocess1', 
                    'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
      }

      if(isset($_POST['save_data'])) {
          //pend($_REQUEST);
          $amount_paid=$_REQUEST['payment'];
          $amount=$_REQUEST['pay_amt'];
          $paidper=100-(($amount-$amount_paid)*100/$amount);
          $mapstatus = 'failed';
          /*if($paidper==100) {
            $mapstatus = 'success';
          } else {
            $mapstatus = 'partial';
          }*/
          $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $_REQUEST['t_id'],
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
          if($data->remaining_amt=="") {
            $remaining=$data->pay_amt-$_REQUEST['payment'];
          } else {
            $remaining=$data->remaining_amt-$_REQUEST['payment'];
          }          
          $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_id,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $remaining,
                    'param10' => $_REQUEST['txn_id'],
                    'param11' => $mapstatus,
                    'param12' => $data->pay_amt,
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
            $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param2);
          foreach($res2 as $val) {
            $process_amt=($val->pay_amt*$paidper)/100;
            $param = array(
                  'act_mode' => 'updatetranssuccessdata', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => $mapstatus,
                  'param18' => ''
                );             
            $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            }
          }         
          //$this->session->set_flashdata('message', 'Your transaction has been successful - '.$_REQUEST['txn_id'].' (TXN ID)!');
          //redirect(base_url().'admin/tax/paymentprocess');
          redirect(base_url().'admin/receipts/paymentgateway/'.$_REQUEST['txn_id']);
      }
      $param=array('country_id' => '',
                  'state_name' => '',
                  'countrycode'=> '',
                  'row_id'     => $this->session->userdata('popcoin_login')->s_admin_id,
                  'act_mode'   => 'getretailerstore');
      $response['stores'] = $this->supper_admin->call_procedure('proc_state', $param); 
      //p($response['vieww']);exit; 
      $this->load->view('helper/header');      
      $this->load->view('pay_transaction',$response);
  }

  public function viewpaymentdetails() {
      $txn_id=$this->uri->segment(4);
      $param = array(
                    'act_mode' => 'fetchpaymentdetails', 
                    'row_id' => $txn_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
      $response['details'] = $this->supper_admin->call_procedure('proc_procedure4', $param); 
      $this->load->view('helper/header');      
      $this->load->view('viewpaymentdetails',$response);
  }

  public function paymentprocessall() {
      
      
        $parameter = array(
                    'act_mode' => 'retailerpaymentprocess12', 
                    'v_id' => '',
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
     
     //p($response['amountdetails']);//exit; 
      $this->load->view('helper/header');      
      //$this->load->view('pay_transaction',$response);
      $this->load->view('transaction/allmpc_transaction',$response);
  }


   public function paymentprocessall1() {
      
      
        $parameter = array(
                    'act_mode' => 'mpcpayment',//'retailerpaymentprocess12', 
                    'v_id' => '',
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
        //p($response['amountdetails']);

        $parameter = array(
                    'act_mode' => 'getinterest', 
                    'row_id' => '',
                    'counname' => '',
                    'coucode' => '',
                    'commid' => ''
                  );
        $response['interest'] = $this->supper_admin->call_procedureRow('proc_master', $parameter);
        //p($response['interest']);
       

        if(isset($_POST['settled_data'])) {
         // pend($_REQUEST);

          $alldata=$_REQUEST['all'];
          $int=$_REQUEST['intamt'];

          $ab=explode('#', $alldata);
          $inte=explode('#', $int);

          foreach ($ab as $key => $value) {
              //p($value);
               $ab1=explode(',', $value);
         /* p($ab1[0]);
          p($ab1[1]);
          p($ab1[2]);
          p($ab1[3]);
          p($ab1[4]);
          p($ab1[5]);
          p($ab1[6]);*/

           $t_id=$ab1[7];

           $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
        // p($data);

                $param22 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,//$data->store_id,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => '0',
                    'param10' => $data->txn_id,
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => '',
                    'param14' => $inte[$key],
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  


           $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param22);
           //p($res2); 

         foreach($res2 as $val) {  

           $param = array(
                  'act_mode' => 'updatetranssuccessdata_mpc', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->pay_amt,
                  'param17' => 'success',
                  'param18' => $val->update_amt
                );
                //p($param);  
           $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
          
               
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->update_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res1 = $this->supper_admin->call_procedure('proc_procedure4', $param);
            }
          }       
          
      
        }
        //exit;
redirect(base_url().'admin/tax/paymentprocessall1');
        }


         if(isset($_POST['save_data'])) {
          //pend($_REQUEST);
          $alldata=$_REQUEST['all'];
          $int=$_REQUEST['intamt'];

          $ab=explode('#', $alldata);
          $inte=explode('#', $int);

          $rec='0';
          $pay='0';

          foreach ($ab as $key => $value1) {
              //p($value);
               $ab1=explode(',', $value1);

              $payamt=$_REQUEST['payment']-$inte[$key];
        
            if($ab1[3]=='Receivables')
            {
                $rec=$rec+$ab1[2]+$payamt;
            }

            if($ab1[3]=='Payables')
            {
                $pay=$pay+$ab1[2];
            }

          }
         $pa=$pay-$rec;

          foreach ($ab as $key => $value) {
              //p($value);
               $ab1=explode(',', $value);
         /* p($ab1[0]);
          p($ab1[1]);
          p($ab1[2]);
          p($ab1[3]);
          p($ab1[4]);
          p($ab1[5]);
          p($ab1[6]);*/
          // p($ab1[2]);
           //echo $rec.'___'.$pay;

          

           $t_id=$ab1[7];

           $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $t_id,
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );             
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);
        // p($data);

          if($data->pay_type=='Receivables')
          {
              $updateamt=0;
          }
          else
          {
            $updateamt=0;
            if(!empty($pa)){
             if($data->update_amt>$pa){
              $updateamt=$pa;
              $pa=0;
             }elseif ($data->update_amt<$pa) {
               $updateamt=$data->update_amt;
               $pa=$pa-$data->update_amt;
             }else{
               $updateamt=$data->update_amt;
               $pa=0;
             }
           }
              
          }

                $param22 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $t_id,
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,//$data->store_id,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $updateamt,
                    'param10' => $data->txn_id,
                    'param11' => 'success',
                    'param12' => $data->pay_amt,
                    'param13' => '',
                    'param14' => $inte[$key],
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  

//p($param22);
           $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param22);
           //p($res2); 

         foreach($res2 as $val) {  

           $param = array(
                  'act_mode' => 'updatetranssuccessdata_mpc', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->pay_amt,
                  'param17' => 'success',
                  'param18' => $val->update_amt
                );
                //p($param);  
           $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
          
               
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback_mpc', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $val->update_amt,
                  'param17' => '',
                  'param18' => ''
                );             
              $res1 = $this->supper_admin->call_procedure('proc_procedure4', $param);
            }
          }       
          
      
        }
        //exit;
redirect(base_url().'admin/tax/paymentprocessall1');
      }
      $this->load->view('helper/header');      
      $this->load->view('pay_transaction1',$response);
  }

  public function paymentprocessall_payables() {
      
      
        $parameter = array(
                    'act_mode' => 'retailerpaymentprocess_payables', 
                    'v_id' => '',
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);



          if(isset($_POST['save_data'])) {
          //pend($_REQUEST);

          $curdate=date('Y-m-d');
          $pdate=$_REQUEST['paydate'];
          $date1 = new DateTime($pdate);
          $date2 = new DateTime($curdate);
          $diff = $date2->diff($date1)->format("%a");


          $intamt=$_REQUEST['intamt'];
          $intamtbefore=$_REQUEST['intamtbefore'];
          $intval=$response['interest']->i_value*$diff;
          if($_REQUEST['pay_amt']>=$_REQUEST['payment'])
          {
              if($intamt==0)
              {
                 $check=0;
                 $amount_paid=$_REQUEST['payment'];
                 $intamt1='';
                 $intamt_paid='';
                 $amount=$_REQUEST['pay_amt'];
                 $paidper=100-(($amount-$amount_paid)*100/$amount);
              }
              else
              {
                 $check=0;
                 $amount_paid=$_REQUEST['payment'];

                  $paidint=$_REQUEST['payment'];
                  $int=$intamt+$intamtbefore;
                  if($int==$paidint)
                  {
                      $intamt1=round(((($amount_paid*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='0';
                  }
                  else
                  {
                     //$intamt1=(($amount_paid*$intval)/100)+$intamtbefore;
                     $intamt1=round(((($amount_paid*$intval)/100)/365),4)+$intamtbefore;
                     $intamt_paid='';
                  }  
                 $amount=$_REQUEST['pay_amt'];
                 $paidper=100-(($amount-$amount_paid)*100/$amount);
              }

          }
          else 
            //if($_REQUEST['pay_amt']<$_REQUEST['payment'])
          { //echo 'hi';
              if($_REQUEST['pay_amt']==0)
              {
                  $check=1;
                  $amount_paid=0;
                  //$intamt_paid=$_REQUEST['payment'];//102-100
                  $paidint=$_REQUEST['payment'];
                  $int=$intamt+$intamtbefore;
                  if($int==$paidint)
                  {
                      $check=1;
                      $amount_paid=0;
                      $intamt1=$int;
                      $intamt_paid='0';
                  }
                  else
                  {
                      $check=1;
                      $amount_paid=0;
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                  }
              }
              else
              {
                  $paidint=round(($_REQUEST['payment']-$_REQUEST['pay_amt']),4);
                  $int=$intamt+$intamtbefore;
                  //echo $int;
                  if($paidint=$int)
                  {  //echo 'hi1';
                      $check=0;
                      $amount_paid=$_REQUEST['pay_amt'];
                      $intamt1=$int;
                      $intamt_paid='0';
                      //$intamt_paid=$_REQUEST['payment']-$_REQUEST['pay_amt'];//102-100
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount_paid)*100/$amount);
                  }
                  else
                  { //echo 'hi11';
                      $check=0;
                      $amount_paid=$_REQUEST['pay_amt'];
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                      //$intamt_paid=$_REQUEST['payment']-$_REQUEST['pay_amt'];//102-100
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount_paid)*100/$amount);
                  }
              }
          }
          
          //echo $check;
          //echo $amount_paid;
         // echo $intamt_paid;
          
        
          $mapstatus = 'failed';
          //exit;
          $param2 = array(
                    'act_mode' => 'fetchtransdata', 
                    'row_id' => $_REQUEST['t_id'],
                    'param1' => '',
                    'param2' => '',
                    'param3' => '',
                    'param4' => '',
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
                            
          $data = $this->supper_admin->call_procedureRow('proc_procedure4', $param2);

          if($data->remaining_amt=="") {
            //$remaining=$data->pay_amt-$_REQUEST['payment'];
            $remaining=$data->pay_amt-$amount_paid;
          } else {
            //$remaining=$data->remaining_amt-$_REQUEST['payment'];
            $remaining=$data->remaining_amt-$amount_paid;
          }          
          $param2 = array(
                    'act_mode' => 'updatetransmapdata', 
                    'row_id' => $_REQUEST['t_id'],
                    'param1' => $data->txn_id,
                    'param2' => $data->cycle_id,
                    'param3' => $data->retailer_id,
                    'param4' => $data->store_count,
                    'param5' => $data->from_date,
                    'param6' => $data->to_date,
                    'param7' => $data->pay_date,
                    'param8' => $data->pay_type,
                    'param9' => $remaining,
                    'param10' => $_REQUEST['txn_id'],
                    'param11' => $mapstatus,
                    'param12' => $data->pay_amt,
                    'param13' => $data->cycle_day,
                    'param14' => $intamt1,
                    'param15' => $intamt_paid,
                    'param16' => '',
                    'param17' => '',
                    'param18' => ''
                  );  
            //p($param2);exit;                  
            $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param2);
            //p($res2);
     if($check==0)//  Interest Amt   
     {
          foreach($res2 as $val) {
            if($val->process_amt=='')
            {
                $process_amt=($val->pay_amt*$paidper)/100;
            }
            else
            {
                $a=$val->pay_amt-$val->process_amt;
                $process_amt=($a*$paidper)/100;
            }
            
            $param = array(
                  'act_mode' => 'updatetranssuccessdata', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt+$val->process_amt,
                  'param17' => $mapstatus,
                  'param18' => $paidper,
                  'Param19' => $val->update_amt,
                  'Param20' => '',
                  'Param21' => '',
                  'Param22' => ''  
                  );  
                 //p($param);          
           // $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            $res = $this->supper_admin->call_procedure('proc_transaction2', $param);
            if($val->user_type=='C') {
              $param = array(
                  'act_mode' => 'insertcustomercashback', 
                  'row_id' => '',
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt,
                  'param17' => $val->tax_type,
                  'param18' => ''
                );             
              $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            }
          }


          }//  Interest Amt    

          /*--------------------------SMS/EMAIL------------------------*/
          $email=$mailsms->s_loginemail;
          $mobile=$mailsms->contact_no;
          $amt=($mailsms->pay_amt)-$amount_paid;
           $msg=urlencode('DEAR MPCMEMBER, Pay for '.$mailsms->s_name.'-'.$mailsms->company_name.' is Pay Rs '.$amount_paid.' .RefID- '.$mailsms->txn_id.'. Pymt due Rs '.$amt);

          $url='http://sms.tattler.co/app/smsapi/index.php?key=558709407EE8FE&routeid=288&type=text&contacts='.$mobile.'&senderid=WEBPRO&msg='.$msg; 
                //echo $url; 
          #file_get_contents($url);
              //print_r(file_get_contents($url));

                  $content='<html>
                   <body bgcolor="#DCEEFC">
                          <br><div> 
                        <table><tr><td>DEAR MPCMEMBER, Pay for '.$mailsms->s_name.' ('.$mailsms->company_name.') is Pay Rs '.$amount_paid.' .RefID- '.$mailsms->txn_id.'. Pymt due Rs '.$amt.'.</td> </tr>
                          
                          <tr><td>Regards,</br>
                          MPC
                        
                          </table>
                         </div>
                   </body>
              </html>';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from('support@mindzshop.com', 'Mypopcoins');
                  $this->email->to($email);
                  //$this->email->cc('pavan@mindztechnology.com');
                  $this->email->subject('Pay Bill');
                  $this->email->message($content);
                  #$this->email->send();

          /*--------------------------SMS/EMAIL------------------------*/

          //exit;  
          //$this->session->set_flashdata('message', 'Your transaction has been successful - '.$_REQUEST['txn_id'].' (TXN ID)!');
          //redirect(base_url().'admin/tax/paymentprocess');
          redirect(base_url().'admin/receipts/paymentgateway/'.$_REQUEST['txn_id']);
      }


     
      //p($response['amountdetails']);exit; 
      $this->load->view('helper/header');      
      $this->load->view('payables_transaction',$response);
  }

  public function paymentprocessall_old() {
      if($this->input->get('store_id')!="") {
        if($this->input->get('month')!="") {
          $array=explode("-",$this->input->get('month'));
          $month=$array[0];
          $year=$array[1];
        } else {
          $month="";
          $year="";
        }    
        $dateObj   = DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F');
        $start = date("d",strtotime('first day of '.$monthName, time()));
        $dateObj   = DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F');
        $end = date("d",strtotime('last day of '.$monthName, time()));
        $sdate = $year.'-'.$month.'-'.$start.' 00:00:00';
        $edate = $year.'-'.$month.'-'.$end.' 00:00:00';
        $parameter = array(
                      'act_mode' => 'retailerpaymentprocess', 
                      'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                      'v_status' => $this->input->get('store_id'),
                      'v_mobile' => $sdate,
                      'v_email' => $edate,
                      'v_pwd' => '',
                      'tin_no' => '',
                      'tax_no' => '',
                      'bank_name' => '',
                      'bank_addr' => '',
                      'ifsc_code' => '',
                      'bene_accno' => '',
                      'param1' => '',
                      'param2' => ''
                    );
        $response['view'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
        $parameter = array(
                    'act_mode' => 'retailerpaymentprocess1', 
                    'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                    'v_status' => $this->input->get('store_id'),
                    'v_mobile' => $sdate,
                    'v_email' => $edate,
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter); 
      } else {
        $parameter = array(
                      'act_mode' => 'retailerpaymentprocess', 
                      'v_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                      'v_status' => '',
                      'v_mobile' => '',
                      'v_email' => '',
                      'v_pwd' => '',
                      'tin_no' => '',
                      'tax_no' => '',
                      'bank_name' => '',
                      'bank_addr' => '',
                      'ifsc_code' => '',
                      'bene_accno' => '',
                      'param1' => '',
                      'param2' => ''
                    );
        $response['view'] = $this->supper_admin->call_procedure('proc_vendor', $parameter); 
        $parameter = array(
                    'act_mode' => 'retailerpaymentprocess12', 
                    'v_id' => '',
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
      }
      $param=array('country_id' => '',
                  'state_name' => '',
                  'countrycode'=> '',
                  'row_id'     => $this->session->userdata('popcoin_login')->s_admin_id,
                  'act_mode'   => 'getretailerstore');
      $response['stores'] = $this->supper_admin->call_procedure('proc_state', $param); 
      //p($response['vieww']);exit; 
      $this->load->view('helper/header');      
      $this->load->view('pay_transaction',$response);
  }

  public function paymentprocessallview() {
      $txnid=$this->uri->segment(4);
        $parameter = array(
                      'act_mode' => 'retailerpaymentprocessview', 
                      'v_id' => '',
                      'v_status' => $txnid,
                      'v_mobile' => '',
                      'v_email' => '',
                      'v_pwd' => '',
                      'tin_no' => '',
                      'tax_no' => '',
                      'bank_name' => '',
                      'bank_addr' => '',
                      'ifsc_code' => '',
                      'bene_accno' => '',
                      'param1' => '',
                      'param2' => ''
                    );
        $response['view'] = $this->supper_admin->call_procedure('proc_vendor', $parameter); 
        
      //p($response['vieww']);exit; 
      $this->load->view('helper/header');      
      $this->load->view('pay_transactionview',$response);
  }

public function storelist() {
     
         $parameter = array(
                    'act_mode' => 'storepaymentlist', 
                    'v_id' => $this->uri->segment(6),
                    'v_status' => $this->uri->segment(4),
                    'v_mobile' => $this->uri->segment(5),
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
//p($response['amountdetails']);
        $parameter1 = array(
                    'act_mode' => 'storepayment1', 
                    'v_id' => $this->uri->segment(6),
                    'v_status' => $this->uri->segment(4),
                    'v_mobile' => $this->uri->segment(5),
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );

        //p($parameter1);
        $response['single'] = $this->supper_admin->call_procedureRow('proc_vendor', $parameter1);
//p($response['single']);


$this->load->view('helper/header');      
$this->load->view('store_pay_transaction',$response);
}


public function storepayment() {
     
         $parameter = array(
                    'act_mode' => 'storepaymentlistpayables', 
                    'v_id' => $this->uri->segment(6),
                    'v_status' => $this->uri->segment(4),
                    'v_mobile' => $this->uri->segment(5),
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);
//p($response['amountdetails']);
        $parameter1 = array(
                    'act_mode' => 'storepayment1', 
                    'v_id' => $this->uri->segment(6),
                    'v_status' => $this->uri->segment(4),
                    'v_mobile' => $this->uri->segment(5),
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );

        //p($parameter1);
        $response['single'] = $this->supper_admin->call_procedureRow('proc_vendor', $parameter1);
//p($response['single']);
$parameter = array(
                    'act_mode' => 'getinterest', 
                    'row_id' => '',
                    'counname' => '',
                    'coucode' => '',
                    'commid' => ''
                  );
        $interest = $this->supper_admin->call_procedureRow('proc_master', $parameter);
       
@$response['interest']=array('intvalue'=>$interest->i_value);
/* ------------------------  */
         if(isset($_POST['save_data'])) {


          //pend($_REQUEST);

           $parameter1 = array(
                    'act_mode' => 'storepaymentget', 
                    'v_id' => $_REQUEST['t_id'],
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );

        //p($parameter1);
        $response2 = $this->supper_admin->call_procedureRow('proc_vendor', $parameter1);
//p($response2); exit;

          $curdate=date('Y-m-d');
          $pdate=$_REQUEST['paydate'];
          $date1 = new DateTime($pdate);
          $date2 = new DateTime($curdate);
          $diff = $date2->diff($date1)->format("%a");


          $intamt=$_REQUEST['intamt'];
          $intamtbefore=$_REQUEST['intamtbefore'];
          $intval=$interest->i_value*$diff;

      if($intamt!=0)
      {    //echo 'll';
          if($_REQUEST['pay_amt']>=$response2->pay_amt)
          {
            
                   if($_REQUEST['pay_amt']==0)
                    { 
                        $check='1';
                        $paidint=round(($_REQUEST['payment']),4);
                        $int=$intamt+$intamtbefore;
                        //echo $paidint;
                        if($int==$paidint)
                        {
                            
                            $amount_paid=0;
                            $intamt1=$int;
                            $intamt_paid='00';
                           
                            
                        }
                        else
                        {
                            
                            $amount_paid=0;
                            $amount_paid1=$_REQUEST['payment'];
                           
                            $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                            $intamt_paid='';
                            
                        }

                    }
                    else
                    {

                            $paidint=$_REQUEST['pay_amt']-$_REQUEST['payment'];
                            
                                $check='0';
                                $amount_paid=0;
                                $amount_paid1=$_REQUEST['payment'];
                                //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                                $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                                $intamt_paid='';
                                $amount=$_REQUEST['pay_amt'];
                                $paidper=100-(($paidint)*100/$amount);
                            

                            $paidamt=$_REQUEST['payment'];
                            
                    }      
          }
          else 
          {  
             
              if($_REQUEST['pay_amt']==$response2->pay_amt)
              { 
                  $check='0';
                  $amount_paid=0;
                 
                  $paidint=round(($_REQUEST['payment']-$_REQUEST['pay_amt']),4);
                  $int=$intamt+$intamtbefore;
                  //echo $paidint;
                  if($int==$paidint)
                  {
                      //$check='-0';
                      $amount_paid=0;
                      $intamt1=$int;
                      $intamt_paid='00';
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount)*100/$amount);
                      
                  }
                  else
                  {
                      //$check='00';
                      $amount_paid=0;
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount)*100/$amount);
                  }

                  $paidamt=$_REQUEST['payment']-$paidint;
              }
              else
              {
                  $check='0';
                  $amount_paid=0;
                  
                  $paidint=round(($_REQUEST['payment']-$_REQUEST['pay_amt']),4);
                  $int=$intamt+$intamtbefore;
                  //echo $paidint;
                  if($int==$paidint)
                  {
                      //$check='-0';
                      $amount_paid=0;
                      $intamt1=$int;
                      $intamt_paid='00';
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount)*100/$amount);
                      
                  }
                  else
                  {
                      //$check='00';
                      $amount_paid=0;
                      $amount_paid1=$_REQUEST['payment'];
                      //$intamt1=(($amount_paid1*$intval)/100)+$intamtbefore;
                      $intamt1=round(((($amount_paid1*$intval)/100)/365),4)+$intamtbefore;
                      $intamt_paid='';
                      $amount=$_REQUEST['pay_amt'];
                      $paidper=100-(($amount-$amount)*100/$amount);
                  }

                  $paidamt=$_REQUEST['payment']-$paidint;
              }
             
          }


         /* $amount_paid=$_REQUEST['payment'];
          $amount=$_REQUEST['pay_amt'];
          $paidper=100-(($amount-$amount_paid)*100/$amount);*/
          $mapstatus = 'failed';       
          
              if($response2->remaining_amt=='')
              {
                  //$remaining=$response2->pay_amt-$_REQUEST['payment'];
                  $remaining=$response2->pay_amt-$paidamt;
              }
              else
              {
                  //$remaining=$response2->remaining_amt-$_REQUEST['payment'];
                   $remaining=$response2->remaining_amt-$paidamt; 
              }
           
}
else
{
    $check=0;
    $amount_paid=$_REQUEST['payment'];
    $amount=$_REQUEST['pay_amt'];
    $paidper=100-(($amount-$amount_paid)*100/$amount);
    $mapstatus = 'failed'; 
    $intamt1='';
    $intamt_paid='';    

    if($_REQUEST['pay_amt'] >= $response2->pay_amt)
    {
      echo $response2->pay_amt;
    }  
    else
    {
      echo 'k';
    }
      if($response2->remaining_amt=='')
        {
           echo $remaining=$response2->pay_amt-$_REQUEST['payment'];
            
        }
        else
        {
            $remaining=$response2->remaining_amt-$_REQUEST['payment'];
           
        }
     


}


       $param2 = array(
                    'act_mode' => 'updatetransmapdataMPC', 
                    'row_id' => $response['single']->t_id,
                    'param1' => $response['single']->txn_id,
                    'param2' => $response['single']->cycle_id,
                    'param3' => $response['single']->retailer_id,
                    'param4' => $response['single']->store_count,
                    'param5' => $response['single']->from_date,
                    'param6' => $response['single']->to_date,
                    'param7' => $response['single']->pay_date,
                    'param8' => $response['single']->pay_type,
                    'param9' => $remaining,
                    'param10' => $_REQUEST['txn_id'],
                    'param11' => $mapstatus,
                    'param12' => $response['single']->pay_amt,
                    'param13' => $response['single']->cycle_day,
                    'param14' => $intamt1,
                    'param15' => $intamt_paid,
                    'param16' => $response['single']->update_amt,//
                    'param17' => $paidper,//
                    'param18' => ''//$response1->remaining_amt//
                  );     

                 //p($param2);exit;                
            $res2 = $this->supper_admin->call_procedure('proc_procedure4', $param2);
            //p($res2);exit;
         if($check==0) //check
         {     
            foreach($res2 as $val) {
            if($val->store_id==$_REQUEST['store_id']) {
                //$process_amt=($val->pay_amt*$paidper)/100;

                if($val->process_amt=='')
                {
                    $process_amt=($val->pay_amt*$paidper)/100;
                }
                else
                {
                    $a=$val->pay_amt-$val->process_amt;
                    $process_amt=($a*$paidper)/100;
                }

         $param = array(
                  'act_mode' => 'updatetranssuccessdata', 
                  'row_id' => $val->t_id,
                  'param1' => $val->txn_id,
                  'param2' => $val->tran_map_id,
                  'param3' => $val->cycle_id,
                  'param4' => $val->receipts_id,
                  'param5' => $val->order_id,
                  'param6' => $val->retailer_id,
                  'param7' => $val->store_id,
                  'param8' => $val->deal_id,
                  'param9' => $val->user_id,
                  'param10' => $val->user_type,
                  'param11' => $val->pay_type,
                  'param12' => $val->tax_type,
                  'param13' => $val->tax_amt,
                  'param14' => $val->total_amt,
                  'param15' => $val->pay_amt,
                  'param16' => $process_amt+$val->process_amt,
                  'param17' => $mapstatus,
                  'param18' => $paidper,
                  'Param19' => $val->update_amt,
                  'Param20' => '',
                  'Param21' => '',
                  'Param22' => ''  
                  );  
                    p($param);   exit;    
               // $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
            $res = $this->supper_admin->call_procedure('proc_transaction2', $param);

                if($val->user_type=='C') {
                  $param = array(
                      'act_mode' => 'insertcustomercashback', 
                      'row_id' => '',
                      'param1' => $val->txn_id,
                      'param2' => $val->tran_map_id,
                      'param3' => $val->cycle_id,
                      'param4' => $val->receipts_id,
                      'param5' => $val->order_id,
                      'param6' => $val->retailer_id,
                      'param7' => $val->store_id,
                      'param8' => $val->deal_id,
                      'param9' => $val->user_id,
                      'param10' => $val->user_type,
                      'param11' => $val->pay_type,
                      'param12' => $val->tax_type,
                      'param13' => $val->tax_amt,
                      'param14' => $val->total_amt,
                      'param15' => $val->pay_amt,
                      'param16' => $process_amt,
                      'param17' => $val->tax_type,
                      'param18' => ''
                    ); 
                        //p($param);         
                  $res = $this->supper_admin->call_procedure('proc_procedure4', $param);
                }
              }         
              //$this->session->set_flashdata('message', 'Your transaction has been successful - '.$_REQUEST['txn_id'].' (TXN ID)!');
              //redirect(base_url().'admin/tax/paymentprocess');
              //redirect(base_url().'admin/receipts/paymentgateway2/'.$_REQUEST['txn_id']);
          } 
        } //check
        //exit;
          redirect(base_url().'admin/receipts/paymentgateway/'.$_REQUEST['txn_id']);
        }
/* ------------------------  */

$this->load->view('helper/header');      
$this->load->view('store_payment_transaction',$response);
}

public function interestvalue() {


      if($this->input->post('interestsubmit'))
      {
          $this->form_validation->set_rules('interestvalue','Enter Interest','trim|required');
          
          if($this->form_validation->run() != false) {

        $parameter = array(
                    'act_mode' => 'addinterest', 
                    'v_id' => '',
                    'v_status' => $this->input->post('interestvalue'),
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );   

          $response = $this->supper_admin->call_procedureRow('proc_vendor', $parameter);  

          $this->session->set_flashdata('message', 'Your information has been saved successfully!');
          redirect("admin/tax/interestvalue");
        }
      }

      $parameter = array(
                    'act_mode' => 'viewinterest', 
                    'v_id' => '',
                    'v_status' => '',
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );   

      $response['vieww'] = $this->supper_admin->call_procedure('proc_vendor', $parameter);  
     // p($response['vieww']);exit; 
      $this->load->view('helper/header');      
      $this->load->view('interestvalue',$response);
  }

  public function transactionpermissionstore()
  {
      $parameter = array(
                        'act_mode' => 'viewretailerstore1', 
                        'row_id' => $this->session->userdata('popcoin_login')->s_admin_id,
                        'p_name' => '',
                        's_storecountryid' => '',
                        's_statenameid' => '',
                        's_storecity' => '',//$this->input->post('storecityname'),
                        's_storeaddress' => '',//$this->input->post('storeaddress'),
                        's_managerid' => '',//$this->input->post('managerid'),
                        'p_tinno' => '',//$this->input->post('stinno'),
                        'p_servicetaxno' => '',//$this->input->post('sservicetaxno'),
                        'p_pancardno' => '',//$this->input->post('spancardno'),
                        'p_visitingcard' =>'',// $this->input->post('visitingcard'),
                        'p_samplecopy' => '',//$this->input->post('samplebillcopy'),
                        'p_email' => '',//$this->input->post('emailid'),
                        'p_concatno' => '',//$this->input->post('contactno'),
                        'p_aggrementdate' => '',//$this->input->post('dateofagrement'),
                        'param0' => '',
                        'param1' => '',//$this->input->post('categoryid'),
                        'p_firststarttime' => '',
                        'p_firstendtime' => '',
                        'p_sefirststarttime' => '',
                        'p_seendttime' => '',
                        's_traninggivename' => '',
                        's_traninggivenno' => '',
                        's_cashbackper' => '',
                        's_dealcommision' => '',
                        's_storeimg' => '',
                        'param11' => '',
                        'param2' => '',
                        'param3' => '',
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => ''
                       
                      );

      $response['vieww'] = $this->supper_admin->call_procedure('proc_store ', $parameter);
     // p($response['vieww']);exit; 
      $this->load->view('helper/header');      
      $this->load->view('transactionpermissionstore',$response);
  }

  public function paymentstatusupdate(){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='1'?'paymentpermissionoff':'paymentpermissionon';
  $parameter     = array(
                          'act_mode'=>$act_mode,
                          'v_id'=>$rowid,
                          'v_status' => '',
                          'v_mobile' => '',
                          'v_email' => '',
                          'v_pwd' => '',
                          'tin_no' => '',
                          'tax_no' => '',
                          'bank_name' => '',
                          'bank_addr' => '',
                          'ifsc_code' => '',
                          'bene_accno' => '',
                          'param1' => '',
                          'param2' => ''
                        );
    
//p($parameter); exit;
  $response      = $this->supper_admin->call_procedure('proc_vendor',$parameter);
  $this->session->set_flashdata('message', 'Your Permission was successfully Updated.');
  redirect(base_url().'admin/tax/transactionpermissionstore');
}



public function generateinvoice() {

  $rid=$this->uri->segment(4);
  $cid=$this->uri->segment(5);
  $month=$this->uri->segment(6);
  $year=$this->uri->segment(7);
  $sid=$this->uri->segment(8);
  $ref_id=$this->uri->segment(9);
  $storecount=$this->uri->segment(10);
  $pay_rec=$this->uri->segment(11);
  if($sid=='0')
  {
      $type='R';
      $id=$this->uri->segment(4);
      $ptype='Retailer';
  }
  else
  {
      $type='';
      $id=$this->uri->segment(8);
      $ptype='Store';
  }

  if($pay_rec=='M')
  {
      $pay='Payable';
      $rec='Receivable';
      //$mlm='Payable';
      $mlm='Earnings';
  }
  else
  {
     $rec='Payable';
     $pay='Receivable';
     //$mlm='Earnings';
     $mlm='Payable';
  }

  $invoiceno=$rid.$cid.$month.$year.$sid;

      $parameter1 = array(
                    'act_mode' => 'retailer_details', 
                    'p_t_imgname' => '',
                    'p_t_path' => '',
                    'imgtype' => $type,
                    'row_id' => $id,
                    'imagename' => ''
                  );

        //p($parameter1);
        $redetails = $this->supper_admin->call_procedureRow('proc_img', $parameter1);



$paramet = array(
                    'act_mode' => 'details_invoice', 
                    'row_id' => $cid,
                    'param1' => $ref_id,
                    'param2' => '',
                    'param3' => $type,
                    'param4' => $id,
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

       //p($paramet);
        $details_invoice = $this->supper_admin->call_procedureRow('proc_transaction2', $paramet);
        //p($details_invoice); exit;
         //p(round($details_invoice->arears,2));
         if(empty($details_invoice->interest))
         {
            $interest=0;
         }
         else
         {
            $interest=$details_invoice->interest;
         }

         if(empty($details_invoice->discount))
         {
            $discount=0;
         }
         else
         {
            $discount=$details_invoice->discount;
         }

         if(empty($details_invoice->paidamt))
         {
            $paidamt=0;
         }
         else
         {
            $paidamt=$details_invoice->paidamt;
         }

$paramet = array(
                    'act_mode' => 'details_receipts_invoice', 
                    'row_id' => $cid,//$this->uri->segment(4),
                    'param1' => $ref_id,//$month,//$this->uri->segment(5),
                    'param2' => $year,
                    'param3' => $type,
                    'param4' => $id,
                    'param5' => $month,
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

       //p($paramet);
        $receipts1 = $this->supper_admin->call_procedure('proc_transaction2', $paramet);
        //p($receipts1);


        $cbamt=0;
        $mlmamt=0;
        $stamt=0;
        $marginamt=0;
        $outstandingpop=0;
foreach ($receipts1 as $value) { 

  //p($value);
  if($value->tt=='CB')
  {
    $cbamt +=$value->pa;
  }

  if($value->tt=='MLM')
  {
    $mlmamt +=$value->pa;
  }
  if($value->tt=='ST')
  {
    $stamt +=$value->pa;
  }
  if($value->tt=='Margin')
  {
    $marginamt +=$value->pa;
  }

  if($value->tt=='CB')
  {
    $outstandingpop +=$value->ua;
  }



}
//echo $paid=$value->paid_amt;


   $parameter1 = array(
                    'act_mode' => 'details_pay', 
                    'row_id' => $cid,
                    'param1' => $month,
                    'param2' => $year,
                    'param3' => $type,
                    'param4' => $id,
                    'param5' => '',
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

        //p($parameter1);#exit;
        $amountdetails = $this->supper_admin->call_procedure('proc_transaction2', $parameter1);
     //p($amountdetails);exit;

     
foreach ($amountdetails as $val) { 


}

if($value->t_createdon=='')
{
  $indate=$val->t_createdon;
}
else
{
  $indate=$value->t_createdon;
}

if($value->pay_date=='')
{
  $paydate=date('d-m-Y',strtotime($val->pay_date));
}
else
{
  $paydate=date('d-m-Y',strtotime($value->pay_date));
}

$indate=date('d-m-Y',strtotime('2018-01-25'));

$parameter1 = array(
                    'act_mode' => 'details_rec', 
                    'row_id' => $cid,//$this->uri->segment(4),
                    'param1' => $ref_id,//$month,//$this->uri->segment(5),
                    'param2' => $year,
                    'param3' => $type,
                    'param4' => $id,
                    'param5' => $month,
                    'param6' => '',
                    'param7' => '',
                    'param8' => '',
                    'param9' => '',
                    'param10' => '',
                    'param11' => '',
                    'param12' => '',
                    'param13' => '',
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

        //p($parameter1);exit;
        $receipts = $this->supper_admin->call_procedure('proc_transaction2', $parameter1);
    //p($receipts);

$parameter1 = array(
                    'act_mode' => 'check_insert_invoice', 
                    'row_id' => $rid,
                    'param1' => date('Y-m-d h:i:S'),#$indate,
                    'param2' => $paydate,
                    'param3' => $invoiceno,
                    'param4' => $ptype,
                    'param5' => $redetails->id,
                    'param6' => $redetails->name,
                    'param7' => $redetails->address.', '.$redetails->cityname.', '.$redetails->statename,
                    'param8' => $interest,
                    'param9' => round($details_invoice->arears,2),
                    'param10' => $mlmamt,
                    'param11' => $cbamt,
                    'param12' => $stamt,
                    'param13' => $marginamt,
                    'param14' => $outstandingpop,
                    'param15' => $storecount,
                    'param16' => $discount,
                    'param17' => $details_invoice->delay_int,
                    'param18' => $paidamt,
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

        //p($parameter1);exit;
      $invoiceid = $this->supper_admin->call_procedureRow('proc_transaction2', $parameter1);
//p($invoiceid->lid);

if($invoiceid->lid!='E')
{

foreach ($receipts as $key => $value) {


  $parameter1 = array(
                    'act_mode' => 'receipts_insert_invoice', 
                    'row_id' => $invoiceid->lid,
                    'param1' => $rid,
                    'param2' => $value->store_id,
                    'param3' => date('d-m-Y',strtotime($value->from_date)).' To '.date('d-m-Y',strtotime($value->to_date)).' Pay Date '.date('d-m-Y',strtotime($value->pay_date)),
                    'param4' => $value->bill_uid,
                    'param5' => $value->s_storeunid,
                    'param6' => $value->s_name,
                    'param7' => $value->location,
                    'param8' => $value->t_FName,
                    'param9' => $value->date_of_purchase,
                    'param10' => $value->bill_no,
                    'param11' => $value->bill_update_amt,
                    'param12' => $value->mpc_margin.' %',
                    'param13' => (($value->bill_update_amt*$value->mpc_margin)/100),
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

        //p($parameter1);//exit;
      $insertreceipts = $this->supper_admin->call_procedureRow('proc_transaction2', $parameter1);
  
}


foreach ($amountdetails as $key => $value) {

if($value->deal_id=='NA')
{
  $di=$value->deal_id;
}
else
{
  $di='DEL('.$value->deal_id.')POP';
}

if($value->tax_amt=='')
{
  $ta='NA';
}
else
{
  $ta=$value->tax_amt.' %';
}

  $parameter1 = array(
                    'act_mode' => 'deal_insert_invoice', 
                    'row_id' => $invoiceid->lid,
                    'param1' => $rid,
                    'param2' => $value->store_id,
                    'param3' => date('d-m-Y',strtotime($value->from_date)).' To '.date('d-m-Y',strtotime($value->to_date)).' Pay Date '.date('d-m-Y',strtotime($value->pay_date)),
                    'param4' => $value->dealcoupan_code,
                    'param5' => $value->storeuid,
                    'param6' => $value->storename,
                    'param7' => $value->storeaddress,
                    'param8' => $value->customername,
                    'param9' => $value->date_of_purchase,
                    'param10' => $di,
                    'param11' => $value->total_amt,
                    'param12' => $ta,
                    'param13' => $value->pay_amt,
                    'param14' => '',
                    'param15' => '',
                    'param16' => '',
                    'param17' => '',
                    'param18' => '',
                    'param19' => '',
                    'param20' => '',
                    'param21' => '',
                    'param22' => ''
                  );

        //p($parameter1);//exit;
      $insertreceipts = $this->supper_admin->call_procedureRow('proc_transaction2', $parameter1);
  
}


}


$parameter1 = array(
                    'act_mode' => 'getinvoice', 
                    'p_t_imgname' => $invoiceno,
                    'p_t_path' => '',
                    'imgtype' => '',
                    'row_id' => '',
                    'imagename' => ''
                  );

        //p($parameter1);
$getinvoice = $this->supper_admin->call_procedureRow('proc_img', $parameter1);
//p($getinvoice);

$parameter1 = array(
                    'act_mode' => 'getinvoicereceipts', 
                    'p_t_imgname' => $getinvoice->id,
                    'p_t_path' => '',
                    'imgtype' => '',
                    'row_id' => '',
                    'imagename' => ''
                  );

        //p($parameter1);
$getreceipts = $this->supper_admin->call_procedure('proc_img', $parameter1);

$parameter1 = array(
                    'act_mode' => 'getinvoicedeal', 
                    'p_t_imgname' => $getinvoice->id,
                    'p_t_path' => '',
                    'imgtype' => '',
                    'row_id' => '',
                    'imagename' => ''
                  );

        //p($parameter1);
$getdeal = $this->supper_admin->call_procedure('proc_img', $parameter1);
	 

$invoice ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SNN'.$invoiceno.'</title>

</head>


<body style="margin:0px;
  padding:0px;
  font-family:Arial, Helvetica, sans-serif;
  font-size:14px;
  background:#fff;">
<div style="width:1150px;
    height:auto;
    margin:0 auto;">
<div style="width:100%;
    height:auto;
    float:left;">
<div style="width:100%;
    height:auto;
    float:left;
    padding:20px 0px;
    text-align:right;">
<img src="'.base_url().'assets/admin/images/logo3.png" width="158" alt="" />

<span style="
    position: absolute;
    top: 50px;
    left: 39%;
    font-size: x-large;
">Settlement Note</span>

</div>

<div style="width:100%;
    height:auto;
    float:left;
        padding: 7px 0px;">
<div style="width:40%;
    height:auto;
    float:left;">
<p style="text-align:left;">Date: '.date('d-m-Y h:i:s',strtotime($getinvoice->invoicedate)).'</p>



</div>


<div style="width:40%;
    height:auto;
    float:right;">

<p style="text-align:left;">Settlement Note Number: SNN'.$invoiceno.'</p>

</div>

</div>
<div style="width:100%;
  height:auto;
  float:left;margin-bottom: 10px;">
    <div style="float:left; width:60%; height:auto;">
    <p align="left" style="font-weight:bold; line-height:22px; margin:0px;">To,</p>
    <p align="left" style="line-height:22px; margin:0px;">Retailer/Store ID:- '.$getinvoice->ret_sto_id.' </p>
    <p align="left" style="line-height:22px; margin:0px;">Company Name:- '.$getinvoice->ret_sto_name.' </p>
    <p align="left" style="line-height:22px; margin:0px;">Address:- '.$getinvoice->ret_sto_address.' </p>
   
     </div>
      <div style="float:left; width:40%; height:auto;">
      <p align="left" style="font-weight:bold;margin:0px; line-height:22px;">From,</p>
      <p align="left" style="line-height:22px; margin:0px;">Mindz Loyalty Pvt. Ltd.</p>
        <p align="left" style="line-height:22px; margin:0px;">Phone:- +91 11 25250025 </p>
         <p align="left" style="line-height:22px; margin:0px;">Email:- info@mindztechnology.com </p>
      </div>
    
   </div>

</div>';


$invoice .= '<p style="font-weight: bold;text-decoration: underline;">Order History</p>
<div style="width:100%; height:auto; float:left;">
<table width="100%"  bgcolor="#f3f3f3" cellpadding="0" cellspacing="0">
<tr style="">
<th bgcolor="#184058" style="color:#fff; padding:10px; border:1px solid #fff;">S.No</th>
<th bgcolor="#184058" style="color:#fff; padding:10px; border:1px solid #fff;">Settlement Cycle</th>
<th bgcolor="#184058" style="color:#fff; padding:5px; border:1px solid #fff;">Order ID</th>
<th bgcolor="#184058" style="color:#fff; padding:5px; border:1px solid #fff;">User Name</th>
<th bgcolor="#184058" style="color:#fff; padding:5px; border:1px solid #fff;">Date & Time</th>

<th bgcolor="#184058" style="color:#fff; padding:5px; border:1px solid #fff;">Total Amt</th></tr>';
$i=0;
$tm=0;
foreach ($getdeal as $value) { $i++;
 

$invoice .='
<tr>
<td style="padding:10px; border:1px solid #fff;">'.$i.'</td>
<td style="padding:10px; border:1px solid #fff;" >'.$value->settlement_cycle.'</td>
<td style="padding:10px; border:1px solid #fff;" >'.$value->bill_uid.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->user_name.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->dateofpurchase.'</td>

<td style="padding:10px; border:1px solid #fff;">'.$amt11=$value->cbamt.'</td>


</tr>';
$tm +=$amt11;
}
#.$getinvoice->mlm_amt.
$invoice .='<tr><td colspan="4"></td><td colspan="1" style="padding:10px; border:1px solid #fff;background-color:yellow;">Total Amount '.$pay.'</td><td style="background-color:yellow;padding: 10px;">'.number_format($tm,2).'</td></tr>
</table>

</div>';

$adjustbal=$getinvoice->adjustbal;
$pay_amt=$tm;
$rec_amt=$tm1;
$arr=round($getinvoice->arears_amt,2);
$mlmamt=round(0,2);
$int=round($getinvoice->interest_amt,2);
$dis=round($getinvoice->discount,2);
#$totalamt=number_format($getinvoice->cbamnt-$tm,2);
$totalamt=round(($tm1+$arr+$int)-($tm+$mlmamt+$dis),2);
$paytotal=$pay_amt+$mlmamt+$dis+$adjustbal;
$rectotal=$rec_amt+$arr+$int;
$paid_amt=$getinvoice->paid_amt;
$rec_pay=$rectotal-$paytotal;
//echo $pay_amt.'***'.$mlmamt.'***'.$dis.'***'.$adjustbal.'***'.$paytotal;
$usertype=$this->session->userdata('popcoin_login')->s_usertype;
if($usertype==2 || $usertype==4) 
{
    $adjust=round($paid_amt-$rec_pay,2);
}
else
{
    $adjust1=round($paid_amt-$rec_pay,2);
    if($adjust1=='0.00')
    {
      $adjust=str_replace('-', '', round($paid_amt-$rec_pay,2)) ;
    }
    else
    {
      if($paid_amt > $rec_pay)
        $adjust='-'.str_replace('-', '', round($paid_amt-$rec_pay,2)) ;
      else
        $adjust=str_replace('-', '', round($paid_amt-$rec_pay,2)) ;
      //$adjust='-'.round($paid_amt-$rec_pay,2);
    }
    
}
$adjust=0;
//echo $paid_amt.'__'.$rec_pay;
$invoice .='<div style="width:100%; height:auto; float:left;margin-bottom: 50px;"><div style="width:40%; height:auto; float:left; margin-top: 25px;"><table width="100%"  bgcolor="#f3f3f3" cellpadding="0" cellspacing="0"><tr style=""><th bgcolor="#d40078" style="color:#fff; padding:10px; border:1px solid #fff;">'.$pay.'s</th><th bgcolor="#d40078" style="color:#fff; padding:10px; border:1px solid #fff;">Amount</th></tr><tr><td style="padding:10px; border:1px solid #fff;">Order Amt</td><td style="padding:10px; border:1px solid #fff;" >'.$pay_amt.'</td></tr>';
  
  $invoice .='<tr><td style="padding:10px; border:1px solid #fff;background-color:#71af0e; color:#fff;">Total Amount Settled (Paid Amount)</td><td style="padding:10px; border:1px solid #fff;background-color:#71af0e; color:#fff;" >'.$paid_amt.'</td></tr></table></div>';

$point=explode('.', $totalamt);

$invoice .='<div style="width:58%; height:auto; float:right; margin-top:12px;"><ul style="width: 97%;float: left;padding: 10px;list-style-type: none;border: 1px solid #ddd;">

    <li style="padding:5px 0;">Company`s PAN No: AAKCM3233G</li>
    <li style="padding:5px 0;">Terms and Conditions:
      <ul class="sub" style="list-style-type:none;">
          <li style="padding:5px 0;">1. All Disputes Subject to Delhi Jurisdiction.</li>
            <li style="padding:5px 0;">2. 100% Assured.</li>
        </ul>
    </li>
</ul></div>
</div>

<div class="bottom">
  <table style="margin:0 0 15px 0;width: 100%; border:1px solid #71af0e;">
    <tr>
      <th style="color:#71af0e;">ADDRESS</th>
        <th style="color:#71af0e;">PHONE</th>
        <th style="color:#71af0e;">EMAIL</th>
        <th style="color:#71af0e;">WEB</th>
        </tr>
        <tr style=" background:#71af0e;">
          <td style="padding:10px; background:#71af0e; color:#fff;">A – 12, Delhi, New Delhi - 110020</td>
            <td style="padding:10px; background:#71af0e; color:#fff;">+91 11 46563837</td>
            <td style="padding:10px; background:#71af0e; color:#fff;">info@mindztechnology.com</td>
            <td style="padding:10px; background:#71af0e; color:#fff;">www.mindztechnology.com</td>
        </tr>
    </table>
</div>

 
</div></body>
</html>';

echo $invoice;

}




public function generateinvoice1() {

  $invoiceno=$this->uri->segment(4);
  $pay_rec=$this->uri->segment(5);
  $rid=$this->uri->segment(6);
  $sid=$this->uri->segment(7);
  

 if($pay_rec=='M')
  {
      $pay='Payable';
      $rec='Receivable';
      //$mlm='Payable';
      $mlm='Earnings';
  }
  else
  {
     $rec='Payable';
     $pay='Receivable';
    // $mlm='Earnings';
     $mlm='Payable';
  }

  if($sid=='0')
  {
      $type='R';
      $id=$rid;
  }
  else
  {
      $type='';
      $id=$sid;
  }

      $parameter1 = array(
                    'act_mode' => 'retailer_details', 
                    'p_t_imgname' => '',
                    'p_t_path' => '',
                    'imgtype' => $type,
                    'row_id' => $id,
                    'imagename' => ''
                  );

        //p($parameter1);
        $redetails = $this->supper_admin->call_procedureRow('proc_img', $parameter1);
//p($redetails);


$parameter1 = array(
                    'act_mode' => 'getinvoice', 
                    'p_t_imgname' => $invoiceno,
                    'p_t_path' => '',
                    'imgtype' => '',
                    'row_id' => '',
                    'imagename' => ''
                  );

        //p($parameter1);
$getinvoice = $this->supper_admin->call_procedureRow('proc_img', $parameter1);
//p($getinvoice);

$parameter1 = array(
                    'act_mode' => 'getinvoicereceipts', 
                    'p_t_imgname' => $getinvoice->id,
                    'p_t_path' => '',
                    'imgtype' => '',
                    'row_id' => '',
                    'imagename' => ''
                  );

        //p($parameter1);
$getreceipts = $this->supper_admin->call_procedure('proc_img', $parameter1);

$parameter1 = array(
                    'act_mode' => 'getinvoicedeal', 
                    'p_t_imgname' => $getinvoice->id,
                    'p_t_path' => '',
                    'imgtype' => '',
                    'row_id' => '',
                    'imagename' => ''
                  );

        //p($parameter1);
$getdeal = $this->supper_admin->call_procedure('proc_img', $parameter1);

   

$invoice ='<body style="margin:0px;
  padding:0px;
  font-family:Arial, Helvetica, sans-serif;
  font-size:14px;
  background:#fff;">
<div style="width:1150px;
    height:auto;
    margin:0 auto;">
<div style="width:100%;
    height:auto;
    float:left;">
<div>
<img src="'.base_url().'assets/admin/images/logo3.png" width="158" alt="" />

<span style="font-size: x-large;width: 100%;
    margin-left: 60%;">Settlement Note</span>

</div>

<div style="width:100%;
    height:auto;
    float:left;
        padding: 7px 0px;">
<div style="width:40%;
    height:auto;
    float:left;">
<p style="text-align:left;">Date: '.date('d-m-Y h:i:s',strtotime($getinvoice->invoicedate)).'</p>



</div>


<div style="width:40%;
    height:auto;
    float:right;">

<p style="text-align:left;">Settlement Note Number: SNN'.$invoiceno.'</p>

</div>

</div>
<div style="width:100%;
  height:auto;
  float:left;">
    <div style="float:left; width:60%; height:auto;">
    <p align="left" style="font-weight:bold; line-height:22px; margin:0px;">To,</p>
    <p align="left" style="line-height:22px; margin:0px;">Retailer/Store ID:- '.$getinvoice->ret_sto_id.' </p>
    <p align="left" style="line-height:22px; margin:0px;">Retailer/Store Name:- '.$getinvoice->ret_sto_name.' </p>
    <p align="left" style="line-height:22px; margin:0px;">Address:- '.$getinvoice->ret_sto_address.' </p>
   
     </div>
      <div style="float:left; width:40%; height:auto;">
      <p align="left" style="font-weight:bold;margin:0px; line-height:22px;">From,</p>
       <p align="left" style="line-height:22px; margin:0px;">Mindz Loyalty Pvt. Ltd.</p>
       <p align="left" style="line-height:22px; margin:0px;">A – 12, Delhi, New Delhi - 110020</p>
        <p align="left" style="line-height:22px; margin:0px;">Phone:- +91 11 46563837 </p>
         <p align="left" style="line-height:22px; margin:0px;">Email:- info@mindztechnology.com </p>
        <p align="left" style="line-height:22px; margin:0px;">Web:- www.mindztechnology.com</p>
      
        
      </div>
    
   </div>

</div>

<p style="font-weight: bold;text-decoration: underline;">Bill Uploads</p>
<div style="height:auto; float:left; margin-bottom: 15px;">
<table width="100%"  bgcolor="#f3f3f3" cellpadding="0" cellspacing="0">
<tr style="">
<th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">S.No</th>
<th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">Settlement Cycle</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">Bill UID</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">Store ID</th>
<th bgcolor="#ff0000" style="color:#fff;padding:5px;border:1px solid #fff;">Store Name</th>
<th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">Location</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">User Name</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">Date of Purchase</th>
<th bgcolor="#ff0000" style="color:#fff;padding:5px;border:1px solid #fff;">Bill No.</th>
<th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">Bill Amt.</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">Cashback %</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">Cashback Amt.</th></tr>';
$i1=0;
$tm1=0;
foreach ($getreceipts as $value) { $i1++;
 

$invoice .='
<tr>
<td style="padding:10px; border:1px solid #fff;">'.$i1.'</td>
<td style="padding:10px; border:1px solid #fff;" >'.$value->settlement_cycle.'</td>
<td style="padding:10px; border:1px solid #fff;" >'.$value->bill_uid.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->store_uid.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->store_name.'</td>
<td style="padding:10px; border:1px solid #fff;" >'.$value->location.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->user_name.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->dateofpurchase.'</td>
<td style="padding:10px; border:1px solid #fff;" >'.$value->billno.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->billamt.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->cbper.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$amt=$value->cbamt.'</td>


</tr>';
$tm1 +=$amt;
}

$invoice .='<tr><td colspan="9"></td><td colspan="2" style="padding:10px; border:1px solid #fff;background-color:yellow;">Total Amount '.$rec.'</td><td style="background-color:yellow;padding: 10px;">'.$tm1.'</td></tr>
</table>
</div></br>


<p style="font-weight: bold;text-decoration: underline;">Redemptions</p>
<div style="width:100%; height:auto; float:left;">
<table width="100%"  bgcolor="#f3f3f3" cellpadding="0" cellspacing="0">
<tr style="">
<th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">S.No</th>
<th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">Settlement Cycle</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">Order ID</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">Store ID</th>
<th bgcolor="#ff0000" style="color:#fff;padding:5px;border:1px solid #fff;">Store Name</th>
<th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">Location</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">User Name</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">Date & Time</th>
<th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">Sub-Total</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;"Tax %</th>
<th bgcolor="#ff0000" style="color:#fff; padding:5px; border:1px solid #fff;">Grand Total</th></tr>';
$i=0;
$tm=0;
foreach ($getdeal as $value) { $i++;
 

$invoice .='
<tr>
<td style="padding:10px; border:1px solid #fff;">'.$i.'</td>
<td style="padding:10px; border:1px solid #fff;" >'.$value->settlement_cycle.'</td>
<td style="padding:10px; border:1px solid #fff;" >'.$value->bill_uid.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->store_uid.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->store_name.'</td>
<td style="padding:10px; border:1px solid #fff;" >'.$value->location.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->user_name.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->dateofpurchase.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->billamt.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$value->cbper.'</td>
<td style="padding:10px; border:1px solid #fff;">'.$amt11=$value->cbamt.'</td>


</tr>';
$tm +=$amt11;
}
#.$getinvoice->mlm_amt.
$invoice .='<tr><td colspan="8"></td><td colspan="2" style="padding:10px; border:1px solid #fff;background-color:yellow;">Total Amount '.$pay.'</td><td style="background-color:yellow;padding: 10px;">'.$tm.'</td></tr>
</table>

</div>';
$adjustbal=$getinvoice->adjustbal;
$pay_amt=$tm;
$rec_amt=$tm1;
$arr=round($getinvoice->arears_amt,2);
$mlmamt=round(0,2);
$int=round($getinvoice->interest_amt,2);
$dis=round($getinvoice->discount,2);
#$totalamt=number_format($getinvoice->cbamnt-$tm,2);
$totalamt=round(($tm1+$arr+$int)-($tm+$mlmamt+$dis),2);
$paytotal=$pay_amt+$mlmamt+$dis+$adjustbal;
$rectotal=$rec_amt+$arr+$int;
$paid_amt=$getinvoice->paid_amt;
$rec_pay=$rectotal-$paytotal;
$usertype=$this->session->userdata('popcoin_login')->s_usertype;
if($usertype==2 || $usertype==4) 
{
    $adjust=round($paid_amt-$rec_pay,2);
}
else
{
    //$adjust='-'.number_format($paid_amt-$rec_pay,2);
    $adjust1=round($paid_amt-$rec_pay,2);
    if($adjust1=='0.00')
    {
      $adjust=str_replace('-', '', round($paid_amt-$rec_pay,2)) ;
    }
    else
    {
      
      if($paid_amt > $rec_pay)
        $adjust='-'.str_replace('-', '', round($paid_amt-$rec_pay,2)) ;
      else
        $adjust=str_replace('-', '', round($paid_amt-$rec_pay,2)) ;
      //$adjust='-'.round($paid_amt-$rec_pay,2);
    }
}



$invoice .='<div style="width:100%; height:auto; float:left;margin-bottom: 50px;"><div style="width:40%; height:auto; float:right; margin-top: 25px;"><table width="100%"  bgcolor="#f3f3f3" cellpadding="0" cellspacing="0"><tr style=""><th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">'.$pay.'s</th><th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">Amount</th></tr><tr><td style="padding:10px; border:1px solid #fff;">Redemptions</td><td style="padding:10px; border:1px solid #fff;" >'.$pay_amt.'</td></tr><tr><td style="padding:10px; border:1px solid #fff;">Referral Earning</td><td style="padding:10px; border:1px solid #fff;" >'.$mlmamt.'</td></tr>';
  $invoice .='<tr><td style="padding:10px; border:1px solid #fff;">Adjust Balance</td><td style="padding:10px; border:1px solid #fff;" >'.$adjustbal.'</td></tr>';
$invoice .='<tr><td style="padding:10px; border:1px solid #fff;">Discount</td><td style="padding:10px; border:1px solid #fff;" >'.$dis.'</td></tr><tr><td style="padding:10px; border:1px solid #fff;background-color:blue; color:#fff;">Total '.$pay.'s</td><td style="padding:10px; border:1px solid #fff;background-color:blue; color:#fff;" >'.$paytotal.'</td></tr></table></div>
<div style="width:40%; height:auto;"><table width="100%"  bgcolor="#f3f3f3" cellpadding="0" cellspacing="0"><tr style=""><th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">'.$rec.'s</th><th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">Amount</th></tr><tr><td style="padding:10px; border:1px solid #fff;">Bill Uploads</td><td style="padding:10px; border:1px solid #fff;" >'.$rec_amt.'</td></tr><tr><td style="padding:10px; border:1px solid #fff;">Arears</td><td style="padding:10px; border:1px solid #fff;" >'.$arr.'</td></tr><tr><td style="padding:10px; border:1px solid #fff;">Interest</td><td style="padding:10px; border:1px solid #fff;" >'.$int.'</td></tr><tr><td style="padding:10px; border:1px solid #fff;background-color:blue; color:#fff;">Total '.$rec.'s</td><td style="padding:10px; border:1px solid #fff;background-color:blue; color:#fff;" >'.$rectotal.'</td></tr></table></div></div>';


$invoice .='<div style="width:100%; height:auto; float:left;margin-bottom: 50px;"><div style="width:100%; height:auto; margin-top: 25px;"><table width="100%"  bgcolor="#f3f3f3" cellpadding="0" cellspacing="0"><tr style=""><th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">Net Settlement Amount (Total Receivables - Total Payables)</th><th bgcolor="#ff0000" style="color:#fff; padding:10px; border:1px solid #fff;">'.$rec_pay.'</th></tr><tr><td style="padding:10px; border:1px solid #fff;">Total Amount Settled (Paid Amount)</td><td style="padding:10px; border:1px solid #fff;" >'.$paid_amt.'</td></tr><tr><td style="padding:10px; border:1px solid #fff;">Balance to be adjusted in next Settlement Cycle</td><td style="padding:10px; border:1px solid #fff;" >'.$adjust.'</td></tr></table></div>
</div>';


$point=explode('.', $totalamt);
#if($point[1]==00) $po='Zero'; else $po=convert_digit_to_words($point[1]);
#$a=' <li style="padding:5px 0;">Amount (In Words): Rupees '.convert_digit_to_words($point[0]).' And Paisa '.$po.' Only</li>';
$invoice .='<ul style="width: 65%;float: left;padding: 10px;list-style-type: none;border: 1px solid #ddd;">
    <li style="padding:5px 0;">Company`s PAN No: AAKCM3233G</li>
    <li style="padding:5px 0;">Terms and Conditions:
      <ul class="sub" style="list-style-type:none;">
          <li style="padding:5px 0;">1. All Disputes Subject to Delhi Jurisdiction.</li>
            <li style="padding:5px 0;">2. Interest @ '.$getinvoice->delay_int.'% shall be levied on delayed payments.</li>
        </ul>
    </li>
</ul>
</div>
<div class="bottom">
  <table style="margin:0 0 15px 0;width: 100%; border:1px solid #9bbb59;">
    <tr>
      <th style="color:#9bbb59;">ADDRESS</th>
        <th style="color:#9bbb59;">PHONE</th>
        <th style="color:#9bbb59;">EMAIL</th>
        <th style="color:#9bbb59;">WEB</th>
        </tr>
        <tr style=" background:#9bbb59;">
          <td style="padding:10px; background:#9bbb59; color:#fff;">A – 12, Delhi, New Delhi - 110020</td>
            <td style="padding:10px; background:#9bbb59; color:#fff;">+91 11 46563837</td>
            <td style="padding:10px; background:#9bbb59; color:#fff;">info@mindztechnology.com</td>
            <td style="padding:10px; background:#9bbb59; color:#fff;">www.mindztechnology.com</td>
        </tr>
    </table>
</div>
 
</div></body>';

//echo $invoice;


$pdfFilePath = 'SNN'.$invoiceno.".pdf";
//$pdfFilePath = "hggh.pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($invoice);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");

}




function details()
{
   $parameter1 = array(
                    'act_mode' => 'sepdetails', 
                    'v_id' => $this->uri->segment(4),
                    'v_status' => $this->uri->segment(5),
                    'v_mobile' => '',
                    'v_email' => '',
                    'v_pwd' => '',
                    'tin_no' => '',
                    'tax_no' => '',
                    'bank_name' => '',
                    'bank_addr' => '',
                    'ifsc_code' => '',
                    'bene_accno' => '',
                    'param1' => '',
                    'param2' => ''
                  );

        //p($parameter1);
        $response['amountdetails'] = $this->supper_admin->call_procedure('proc_vendor', $parameter1);

        $parameter = array(
                    'act_mode' => 'getinterest', 
                    'row_id' => '',
                    'counname' => '',
                    'coucode' => '',
                    'commid' => ''
                  );
        $response['interest'] = $this->supper_admin->call_procedureRow('proc_master', $parameter);

        //p($response['amountdetails']);

      $this->load->view('helper/header');      
      $this->load->view('pay_transaction2',$response);
}


}
?>
