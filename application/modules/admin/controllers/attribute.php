<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Attribute extends MX_Controller{

  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
   

  }//End function.

//............. Add new Attribute ............... //
  public function add_attribute(){
    $this->userfunction->loginAdminvalidation();
    if($this->input->post('submit')){

     // Validation form fields.
      //pend($_POST);
     $this->form_validation->set_rules('attname','Attribute Name','trim|required|xss_clean');

     if($this->form_validation->run() != false){
      $attname         = $this->input->post('attname');
      $userid          = $this->session->userdata('bizzadmin')->LoginID;
      $parameter       = array('act_mode'=>'attexit','row_id'=>'','attid'=>'','attrname'=>$attname,'atttextname'=>'','');
      $record['record']= $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
    
      if($record['record']->attcount>0){
        $this->session->set_flashdata("message", "Attribute Already Exists");
        redirect("admin/attribute/add_attribute");
      }
      else{
          $parameter        = array('act_mode'=>'attinsert','row_id'=>$userid,'attid'=>'','attrname'=>$attname,'atttextname'=>'','');

          $record['record'] = $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
          $this->session->set_flashdata("message", "Your information was successfully Saved.");
          redirect("admin/attribute/viewattribute");
      }
     }
    }//if submit
  
    $this->load->view('helper/header');
    $this->load->view('manageattribute/addattribute');

  }//End function.

//............. View Attribute List ............... //
  public function viewattribute(){
    $this->userfunction->loginAdminvalidation();
    //----------------------multiple delete -------------------------------//
    if($this->input->post('submit')){
     foreach ($this->input->post( 'attdelete') as $key => $value) {
        $parameter         = array('act_mode'=>'deleteatt','row_id'=>$value,'attid'=>'','attrname'=>'','atttextname'=>'','');
        $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
     }
        $this->session->set_flashdata("message", "Your information was successfully delete.");
        redirect("admin/attribute/viewattribute");
    }
    //----------------------------end delete ---------------------------------//

    //--------------------------multiple ststus ------------------------------//
    if($this->input->post('submitstatus')){
     foreach ($this->input->post( 'attdelete') as $key => $value) {
        $status            = $this->input->post('attstatu')[$value];
        $act_mode          = $status == 'A'?'activeatt':'inactiveatt';
        $userid       = $this->session->userdata('bizzadmin')->LoginID;
        $parameter2        = array('act_mode'=>$act_mode,'row_id'=>$value,'attid'=>$userid,'attrname'=>'','atttextname'=>'','');
        $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter2);
      }
      
     $this->session->set_flashdata("message", "Your Status was successfully Updated.");
     redirect("admin/attribute/viewattribute");
    }

    //-------------------------------end ----------------------------------------//
    $parameter         = array('act_mode'=>'viewatt','row_id'=>'','attid'=>'','attrname'=>'','atttextname'=>'','');
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);


    //----------------  Download Newsletter Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Attribute Name');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Attribute Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->attname);
            
            }
          }

          $filename='Attribute.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Newsletter Excel ------------------------// 



    $this->load->view('helper/header');
    $this->load->view('manageattribute/viewattribute',$responce);

  }//End function.
  
//............. Update Attribute List ............... //
  public function attupdate($id){
    if($this->input->post('submit')){

     // Validation form fields.
     $this->form_validation->set_rules('attrname','Attribute Name','trim|required|xss_clean');
     
     if($this->form_validation->run() != false){
      $attrname         = $this->input->post('attrname');
      $userid       = $this->session->userdata('bizzadmin')->LoginID;
      $parameter        = array('act_mode'=>'attupdate','row_id'=>$id,'attid'=>$userid,'attrname'=>$attrname,'atttextname'=>'','');
      $record['record'] = $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
      $this->session->set_flashdata("message", "Your information was successfully Update.");
      redirect("admin/attribute/viewattribute");
     }
    
    }//if submit.

    $parameter         = array('act_mode'=>'viewattid','row_id'=>$id,'attid'=>'','attrname'=>'','atttextname'=>'','');
    $responce['vieww'] = $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
    $this->load->view('helper/header');
    $this->load->view('manageattribute/editatt',$responce);

  }//End function.

//............. Delete Attribute ............... //
  public function attdelete($id){
    $parameter           = array('act_mode'=>'deleteatt','row_id'=>$id,'attid'=>'','attrname'=>'','atttextname'=>'','');
    $responce['vieww']   = $this->supper_admin->call_procedure('proc_attribute',$parameter);
    $this->session->set_flashdata("message", "Your information was successfully delete.");
    redirect("admin/attribute/viewattribute");  
 
  }//End function.

//............. Change Attribute Status ............... //
 public function attstatus (){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='A'?'activeatt':'inactiveatt';
  $userid       = $this->session->userdata('bizzadmin')->LoginID;
  $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'attid'=>$userid,'attrname'=>'','atttextname'=>'','');
  $response      = $this->supper_admin->call_procedure('proc_attribute',$parameter);
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/attribute/viewattribute');

}//End function.

//............. Add new Attribute Map ............... //
public function addattributemap(){
$this->userfunction->loginAdminvalidation();
if($this->input->post('submit')){

$this->form_validation->set_rules('attnameid','Attribute ','trim|required|xss_clean');

if($this->form_validation->run() != false){

 foreach($this->input->post('attname') as $key => $value){
   $atttext          = $this->input->post('attnametext');
   $userid           = $this->session->userdata('bizzadmin')->LoginID;
   $parameter        = array('act_mode'=>'attmexit','row_id'=>'','attid'=>$this->input->post('attnameid'),'attrname'=>$value,'atttextname'=>$atttext[$key],'');
   $record['record'] = $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
   if($record['record']->attcount>0){
     $this->session->set_flashdata("message", "Attribute Already Exists");
     redirect("admin/attribute/addattributemap");
   }
   else{
     $parameter        = array('act_mode'=>'attmapinsert','row_id'=>$userid,'attid'=>$this->input->post('attnameid'),'attrname'=>$value,'atttextname'=>$atttext[$key],'');
     $record['record'] = $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
   }
}
  $this->session->set_flashdata("message", "Your information was successfully Saved.");
  redirect("admin/attribute/viewattributemap");
 }
}
  $parameter         = array('act_mode'=>'viewattT','row_id'=>'','attid'=>'','attrname'=>'','atttextname'=>'','');
  $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
  $this->load->view('helper/header');
  $this->load->view('manageattribute/addattributemap',$responce);

}//End function.

//............. View Attribute Map ............... //
public function viewattributemap(){
    $this->userfunction->loginAdminvalidation();
    //----------------------multiple delete -------------------------------//
    if($this->input->post('submit')){
     foreach($this->input->post( 'attdelete') as $key => $value) {
        $parameter         = array('act_mode'=>'deleteattm','row_id'=>$value,'attid'=>'','attrname'=>'','atttextname'=>'','');
        $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
     }
        $this->session->set_flashdata("message", "Your information was successfully delete.");
        redirect("admin/attribute/viewattributemap");
    }
    //----------------------------end delete ---------------------------------//

    //--------------------------multiple ststus ------------------------------//
    if($this->input->post('submitstatus')){
     foreach ($this->input->post( 'attdelete') as $key => $value) {
        $status            = $this->input->post('attstatu')[$value];
        $act_mode          = $status=='A'?'activeattm':'inactiveattm';
        $userid       = $this->session->userdata('bizzadmin')->LoginID;
        $parameter2        = array('act_mode'=>$act_mode,'row_id'=>$value,'attid'=>$userid,'attrname'=>'','atttextname'=>'','');
        $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter2);
     }
      
     $this->session->set_flashdata("message", "Your Status was successfully Updated.");
     redirect("admin/attribute/viewattributemap");
    }

    $parameter         = array('act_mode'=>'viewattmap','row_id'=>'','attid'=>'','attrname'=>'','atttextname'=>'','');
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);

     //----------------  Download Newsletter Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Attribute Name','Attribute Value','Attribute Text');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Attribute Map Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->attname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->attvalue);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->attmapname);
            
            }
          }

          $filename='Attribute Map.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Newsletter Excel ------------------------// 

      //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/attribute/viewattributemap?";
      $config['total_rows']       = count($responce['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

     $parameter         = array('act_mode'=>'viewattmap_page','row_id'=>$page,'attid'=>$second,'attrname'=>'','atttextname'=>'','');
    #$responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);

    //----------------  end pagination ------------------------// 

    $this->load->view('helper/header');
    $this->load->view('manageattribute/viewattmap',$responce);
  
  } //end function.

//............. Attribute Map Delete ............... //
  public function attmapdelete($id){
    $parameter         = array('act_mode'=>'deleteattm','row_id'=>$id,'attid'=>'','attrname'=>'','atttextname'=>'','');
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
    $this->session->set_flashdata("message", "Your information was successfully delete.");
    redirect("admin/attribute/viewattributemap");    
  
  }//End function.

//............. Attribute Map Status ............... //
  public function attmapstatus (){
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='A'?'activeattm':'inactiveattm';
    $userid       = $this->session->userdata('bizzadmin')->LoginID;
    $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'attid'=>$userid,'attrname'=>'','atttextname'=>'','');
    $response      = $this->supper_admin->call_procedure('proc_attribute',$parameter);
    $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
    redirect(base_url().'admin/attribute/viewattributemap');

  }//End function.

//............. Attribute Map Update ............... //
public function attmapupdate($id){
  if($this->input->post('submit')){
 
  $this->form_validation->set_rules('attrname','Attribute Name','trim|required|xss_clean');
  $this->form_validation->set_rules('attrtext','Attribute Text','trim|required|xss_clean');

  if($this->form_validation->run() != false){
      $attrname         = $this->input->post('attrname');
      $attrtext         = $this->input->post('attrtext');
      $userid       = $this->session->userdata('bizzadmin')->LoginID;
      $parameter        = array('act_mode'=>'attmapupdate','row_id'=>$id,'attid'=>$userid,'attrname'=>$attrname,'atttextname'=>$attrtext,'');
      $record['record'] = $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
      $this->session->set_flashdata("message", "Your information was successfully Update.");
      redirect("admin/attribute/viewattributemap");
    }
  }
  $parameter         = array('act_mode'=>'viewattmapid','row_id'=>$id,'attid'=>'','attrname'=>'','atttextname'=>'','');
  $responce['vieww'] = $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
  $this->load->view('helper/header');
  $this->load->view('manageattribute/editattmap',$responce);

}//End function.


//............. Attribute Master Add ............... //
public function addattmaster(){
$this->userfunction->loginAdminvalidation();  
if($this->input->post('submit')){
 // pend($_POST);

  $this->form_validation->set_rules('attrid','Attribute ','trim|required|xss_clean');
  $this->form_validation->set_rules('mcatidd','Category','trim|required|xss_clean');

  if($this->form_validation->run() != false){
  foreach ($this->input->post('attvalueid') as $key => $value){
    $parameter        = array('act_mode'=>'atchec','row_id'=>$value,'attid'=>$this->input->post('attrid'),'attrname'=>$attname,'atttextname'=>'','catidd'=>$this->input->post('mcatidd'));
    //pend($parameter);
    $record['record'] = $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
  
    if($record['record']->attcount>0){
      $this->session->set_flashdata("message", "Attribute Already Exists");
      redirect("admin/attribute/addattmaster");
    }
    else{
      $userid       = $this->session->userdata('bizzadmin')->LoginID;
      $parameter         = array('act_mode'=>'hmaatminsert','row_id'=>$value,'attid'=>$this->input->post('attrid'),'attrname'=>$userid,'atttextname'=>'','catidd'=>$this->input->post('mcatidd'));
      //pend($parameter);
      $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
    }
  } // end foreach.
 } // end if.

    $this->session->set_flashdata("message", "Your information was successfully Saved.");
    redirect("admin/attribute/viewattrimaster");
}
    $parameter                 = array('act_mode'=>'viewattT','row_id'=>'','attid'=>'','attrname'=>'','atttextname'=>'','');
    $responce['vieww']         = $this->supper_admin->call_procedure('proc_attribute',$parameter); 
    $parameterr                = array('act_mode'=>'parecatview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
    $responce['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 
   
    $this->load->view('helper/header');
    $this->load->view('manageattribute/addattributemaster',$responce);

}//End function.

public function catvalue(){
  $catidd                    = $this->input->post('catid');
  $parameterr                = array('act_mode'=>'childcatview','row_id'=>$catidd,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $responce['childCatdata']  = $this->supper_admin->call_procedure('proc_category',$parameterr);  
  

  $str               = '';

  foreach($responce['childCatdata'] as $k=>$v){   
   # $str .= "<option value=".$v->id.">".$v->attvalue."</option>";
   $str .= '<option value="'.$v->catid.'">'.$v->catname.'</option>';
  }
  echo $str;

}

public function subbranddcatvalue(){
 
  $catidd = $this->input->post('catid');
  $parameterr                = array('act_mode'=>'subbranddcatview','row_id'=>'','catparentid'=>'','cname'=>$catidd,'cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  //p($parameterr);exit();
  $responce['childCatdata']  = $this->supper_admin->call_procedure('proc_category',$parameterr); 
  //p($responce['childCatdata']);exit(); 
  $str = '';
  foreach($responce['childCatdata'] as $k=>$v){   
   # $str .= "<option value=".$v->id.">".$v->attvalue."</option>";
   $str .= '<option value="'.$v->catid.'">'.$v->catname.'</option>';
  }
  echo $str;

}


//............. Attribute Value ............... //
public function attributevalue(){
  $attriid           = $this->input->post('attrid');
  $parameter         = array('act_mode'=>'attvalu','row_id'=>$attriid,'attid'=>'','attrname'=>'','atttextname'=>'','');
  $response['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter); 

  $str               = '';

  foreach($response['vieww'] as $k=>$v){   
   # $str .= "<option value=".$v->id.">".$v->attvalue."</option>";
   $str .= '<option value="'.$v->id.'"  style="background-color:#'.$v->attvalue.';">'.$v->attvalue.' - '.$v->attmapname.'</option>';
  }
  echo $str;

}//End function.

//............. Attribute Master View ............... //
public function viewattrimaster(){
    $this->userfunction->loginAdminvalidation();

    //----------------------multiple delete -------------------------------//
    if($this->input->post('submit')){
     foreach ($this->input->post( 'attdelete') as $key => $value) {
        $parameter         = array('act_mode'=>'deletemastatt','row_id'=>$value,'attid'=>'','attrname'=>'','atttextname'=>'','');
        $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
     }
     $this->session->set_flashdata("message", "Your information was successfully delete.");
     redirect("admin/attribute/viewattrimaster");
    }    

    $parameter         = array('act_mode'=>'atmasv','row_id'=>'','attid'=>'','attrname'=>'','atttextname'=>'','');
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);

     //----------------  Download Newsletter Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('Attribute Name','Attribute Value','Category Name');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Attribute Master Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->attname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->attmapname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->catname);
            
            }
          }

          $filename='Attribute Master.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Newsletter Excel ------------------------// 
          
      //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/attribute/viewattrimaster?";
      $config['total_rows']       = count($responce['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

     $parameter         = array('act_mode'=>'atmasv','row_id'=>$page,'attid'=>$second,'attrname'=>'','atttextname'=>'','');
    #$responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);

    //----------------  end pagination ------------------------// 


    $this->load->view('helper/header');
    $this->load->view('manageattribute/viewattmaster',$responce);
  }//End function.

//............. Attribute Delete ............... //
 public function attmasterdelete($id){
    $parameter           = array('act_mode'=>'deletemastatt','row_id'=>$id,'attid'=>'','attrname'=>'','atttextname'=>'','');
    $responce['vieww']   = $this->supper_admin->call_procedure('proc_attribute',$parameter);
    $this->session->set_flashdata("message", "Your information was successfully delete.");
    redirect("admin/attribute/viewattrimaster");  

 }//End function.

public function addcombo(){
    


 /*   $parameterr                = array('act_mode'=>'parecatview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');*/

 $parameterr  = array('act_mode'=>'brandlisting',
                      'row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
$responce['branddata'] = $this->supper_admin->call_procedure('proc_category',$parameterr); 
   

    $this->load->view('helper/header');
    $this->load->view('manageattribute/addcombo',$responce);

 }


public function combolisting(){
    


         $parameter1 = array(

        'act_mode'  =>  'listing',
        'p_name'  =>  '',
        'p_qty'  =>  '',
        'p_start'  =>  '',
        'p_end'  =>  '',
        'p_ingridients'  => '' ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  '',
        'p_comboid'   => '',
        'p_cat_id'    =>  '',
        'p_sub_catid' => '',
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
     
      );


    $responce['vieww'] = $this->supper_admin->call_procedure('proc_savecombo',$parameter1);
  
    

    $this->load->view('helper/header');
    $this->load->view('manageattribute/combolisting',$responce);

 }










 public function catproduct(){
    
    $catid = $_POST['catid'];
    $parameter         = array('act_mode'=>'catproduct', 'row_id'=>$catid, 'attid'=>'', 'attrname'=>'','atttextname'=>'','catidd'=>'');
   
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
   
    foreach($responce['vieww'] as $k=>$v){   
   # $str .= "<option value=".$v->id.">".$v->attvalue."</option>";
   $str .= '<option value="'.$v->ProId.'">'.$v->ProductName.'</option>';
  }
  echo $str;

 }



 public function productprice(){
    
    $productid = $_POST['productid'];
    $parameter         = array('act_mode'=>'productprice', 'row_id'=>$productid, 'attid'=>'', 'attrname'=>'','atttextname'=>'','catidd'=>'');
   
    $responce = $this->supper_admin->call_procedureRow('proc_attribute',$parameter);
    echo json_encode($responce);
    die;
   //p($responce);


 }

 public function formsubmit()
 {
   
    $rangearr = array();
   
 
    foreach ($_POST['firstrange'] as $key => $value) {
        
        array_push($rangearr, $value);
    }

     $rangearr2 = array();
    foreach ($_POST['firstrange2'] as $key => $value2) {
        
        $rangearr2[] = $value2;
    }

   

     $rangearr3 = array();
    foreach ($_POST['firstrange3'] as $key => $value3) {
        
        array_push($rangearr3, $value3);
    }
   

     $parameter = array(

        'act_mode'  =>  trim('insert'),
        'p_name'  =>  $_POST['name'],
        'p_qty'  =>  $_POST['cquanity'],
        'p_start'  =>  $_POST['from'],
        'p_end'  =>  $_POST['to'],
        'p_ingridients'  =>  $_POST['ingri'],
        'p_about'  =>  $_POST['about'],
        'p_totalqty'  =>  $_POST['totalqty'],
        'p_comboActPrice'  =>  $_POST['totocomboprice'],
        'p_disPer'  =>  $_POST['discount'],
        'p_comb_sellingprice'  =>  $_POST['totalseleing'],
        'p_totalsaving'  =>  $_POST['totalsaving'],
        'p_combo_img'   =>  $_POST['img'],
       
        'p_comboid'   =>  $rangearr[0]['rangevalue'].'to'.$rangearr[1]['rangevalue'],
        'p_cat_id'    =>  $rangearr2[0]['rangevalue2'].'to'.$rangearr2[1]['rangevalue2'],
        'p_sub_catid' => $rangearr3[0]['rangevalue3'].'to'.'more',
       
        'p_product_id'    =>  $_POST['price1'],
        'p_product_qty'    => $_POST['price2'],
        'p_pro_mrp'    =>     $_POST['price3'],
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
       
      );

//p($parameter); exit;
 

 $responce = $this->supper_admin->call_procedureRow('proc_saveCombo',$parameter);
 
 //echo ($responce); exit;

 if($responce->lastinsert!='')
 {

       foreach ($_POST['fordata'] as $val) {
            

         $parameter1 = array(

        'act_mode'  =>  trim('insertmap'),
        'p_name'  =>  '',
        'p_qty'  =>  $val['brandname'],
        'p_start'  =>  '',
        'p_end'  =>  '',
        'p_ingridients'  => '' ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  $_POST['city1'],
        'p_comb_sellingprice'  =>  $_POST['city2'],
        'p_totalsaving'  =>  $_POST['city3'],
        'p_combo_img'   =>  '',
        'p_comboid'   =>  $responce->lastinsert,
        'p_cat_id'    =>  $val['cat'],
        'p_sub_catid' => $val['subcat'],
        'p_product_id'    =>  $val['product'],
        'p_product_qty'    =>  $val['quantity'],
        'p_pro_mrp'    =>  $val['mrp'],
        'p_pro_seling'    =>  $val['sellingprice'],
        'p_pro_combo_price'    =>  $val['price']
     
      );
        

        //p( $parameter1); exit;
       $result = $this->supper_admin->call_procedureRow('proc_saveCombo',$parameter1); 
        }
        echo "successfull";
        $this->session->set_flashdata("message", "Combo saved successfully");
     

 

}     

 }


public function comboimage(){

    $parameter1 = array(

        'act_mode'  =>  'comboname',
        'p_name'  =>  '',
        'p_qty'  =>  '',
        'p_start'  =>  '',
        'p_end'  =>  '',
        'p_ingridients'  => '' ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  '',
        'p_comboid'   => '',
        'p_cat_id'    =>  '',
        'p_sub_catid' => '',
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
     
      );


 $responce['imagesss'] = $this->supper_admin->call_procedure('proc_saveCombo',$parameter1);
 
 
  
if($this->input->post('submit')!='')
{


$filename = $_FILES['image']['name']; 
move_uploaded_file($_FILES["image"]["tmp_name"],FCPATH."assets/comboimage/".$filename);

 $imagepath = $filename;
$save = FCPATH.'assets/comboimage/thumnail/' . $imagepath;
$file = FCPATH.'assets/comboimage/' . $imagepath; 
if($filename!="")
           {

            list($width, $height) = getimagesize($file) ; 
            $modwidth = 130; 
            $diff = $width / $modwidth;
            $modheight = 185; 
            $tn = imagecreatetruecolor($modwidth, $modheight) ; 
          
                $ext = pathinfo($filename, PATHINFO_EXTENSION); 
             

                    if($ext=='jpg' || $ext=='jpeg'){
                       $image = imagecreatefromjpeg($file) ; 
                          imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ; 
                          imagejpeg($tn, $save, 70) ; 
                          
                   
                    }
                    else if($ext=='png'){
                      
                       $image = imagecreatefrompng($file) ;  
                          imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ; 
                          imagepng($tn, $save) ;   
                     }     
         
          }



$filename1 = $_FILES['img1']['name']; 
move_uploaded_file($_FILES["img1"]["tmp_name"],FCPATH."assets/comboimage/".$filename1);

 $imagepath1 = $filename1;
$save1 = FCPATH.'assets/comboimage/thumnail/' . $imagepath1;
$file1 = FCPATH.'assets/comboimage/' . $imagepath1; 
if($filename1!="")
           {

            list($width1, $height1) = getimagesize($file1) ; 
            $modwidth1 = 130; 
            $diff1 = $width1 / $modwidth1;
            $modheight1 = 185; 
            $tn1 = imagecreatetruecolor($modwidth1, $modheight1) ; 
          
                $ext1 = pathinfo($filename1, PATHINFO_EXTENSION); 
             

                    if($ext1=='jpg' || $ext1=='jpeg'){
                       $image1 = imagecreatefromjpeg($file1) ; 
                          imagecopyresampled($tn1, $image1, 0, 0, 0, 0, $modwidth1, $modheight1, $width1, $height1) ; 
                          imagejpeg($tn1, $save1, 70) ; 
                          
                   
                    }
                    else if($ext1=='png'){
                      
                       $image1 = imagecreatefrompng($file1) ;  
                          imagecopyresampled($tn1, $image1, 0, 0, 0, 0, $modwidth1, $modheight1, $width1, $height1) ; 
                          imagepng($tn1, $save1) ;   
                     }     
         
          }


 $filename2 = $_FILES['img2']['name']; 


       move_uploaded_file($_FILES["img2"]["tmp_name"],FCPATH."assets/comboimage/".$filename2);

         $imagepath2 = $filename2;
        

            
            $save2= FCPATH.'assets/comboimage/thumnail/' . $imagepath2;
            $file2 = FCPATH.'assets/comboimage/' . $imagepath2; 
            

           if($filename2!="")
           {

            list($width2, $height2) = getimagesize($file2) ; 
            $modwidth2 = 130; 
            $diff2 = $width2 / $modwidth2;
            $modheight2 = 185; 
            $tn2 = imagecreatetruecolor($modwidth2, $modheight2) ; 
          
                $ext2 = pathinfo($filename2, PATHINFO_EXTENSION); 
             

                    if($ext2=='jpg' || $ext2=='jpeg'){
                       $image2 = imagecreatefromjpeg($file2) ; 
                          imagecopyresampled($tn2, $image2, 0, 0, 0, 0, $modwidth2, $modheight2, $width2, $height2) ; 
                          imagejpeg($tn2, $save2, 70) ; 
                          
                   
                    }
                    else if($ext2=='png'){
                      
                       $image2 = imagecreatefrompng($file2) ;  
                          imagecopyresampled($tn2, $image2, 0, 0, 0, 0, $modwidth2, $modheight2, $width2, $height2) ; 
                          imagepng($tn2, $save2) ;   
                     }     
         
          }


  $filename3 = $_FILES['img3']['name']; 


       move_uploaded_file($_FILES["img3"]["tmp_name"],FCPATH."assets/comboimage/".$filename3);

         $imagepath3 = $filename3;
         

           $save3= FCPATH.'assets/comboimage/thumnail/' . $imagepath3;
            $file3 = FCPATH.'assets/comboimage/' . $imagepath3; 
            

           if($filename3!="")
           {

            list($width3, $height3) = getimagesize($file3) ; 
            $modwidth3 = 130; 
            $diff3 = $width3 / $modwidth3;
            $modheight3 = 185; 
            $tn3 = imagecreatetruecolor($modwidth3, $modheight3) ; 
          
                $ext3 = pathinfo($filename3, PATHINFO_EXTENSION); 
             

                    if($ext3=='jpg' || $ext3=='jpeg'){
                       $image3 = imagecreatefromjpeg($file3) ; 
                          imagecopyresampled($tn3, $image3, 0, 0, 0, 0, $modwidth3, $modheight3, $width3, $height3) ; 
                          imagejpeg($tn3, $save3, 70) ; 
                          
                   
                    }
                    else if($ext3=='png'){
                      
                       $image3 = imagecreatefrompng($file3) ;  
                          imagecopyresampled($tn3, $image3, 0, 0, 0, 0, $modwidth3, $modheight3, $width3, $height3) ; 
                          imagepng($tn3, $save3) ;   
                     }     
         
          }        





   /* if (!empty($_FILES['image']['name'])){
    
           $config['upload_path']   = './assets/comboimage';
           $config['allowed_types'] = 'jpeg|jpg|png|';
           $config['remove_space']  = TRUE;
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
        if(!$this->upload->do_upload('image')) {
          $image = NULL;
        } else {
          $data  = $this->upload->data();
         
          $image = $data['file_name'];
          $param['CreatedBy'] = $image;
         
       }
     
 }   */



        $comboid = $this->input->post('comboname');
  $parameter1 = array(

        'act_mode'  =>  'imgupdate',
        'p_name'  =>  $filename3,
        'p_qty'  =>  '',
        'p_start'  =>  $filename1,
        'p_end'  =>    $filename2,
        'p_ingridients'  => '' ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  $filename,
        'p_comboid'   => $comboid,
        'p_cat_id'    =>  '',
        'p_sub_catid' => '',
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
     
      );

  
//p($parameter1);; exit;
 //redirect(base_url().'admin/attribute/combolisting');
 $responce['imagesss'] = $this->supper_admin->call_procedure('proc_saveCombo',$parameter1);

 $this->session->set_flashdata("message", "Combo Images saved successfully");
 redirect(base_url().'admin/attribute/comboimage');
  
}

  $this->load->view('helper/header');
    $this->load->view('manageattribute/comboimage',$responce);
  }





public function combodelete($id)
{

$parameter1 = array(

        'act_mode'  =>  'combostatus',
        'p_name'  =>  '',
        'p_qty'  =>  '',
        'p_start'  =>  '',
        'p_end'  =>  '',
        'p_ingridients'  => '' ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  '',
        'p_comboid'   => $id,
        'p_cat_id'    =>  '',
        'p_sub_catid' => '',
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
     
      );


  $responce['imagesss'] = $this->supper_admin->call_procedure('proc_saveCombo',$parameter1);
  redirect(base_url().'admin/attribute/combolisting');

}

public function editcombo($id)
{

    if($this->input->post('updatecombo'))
    {
      $parameter = array(

        'act_mode'  =>  'updatecomboinfo',
        'p_name'  =>  $this->input->post('combo_name'),
        'p_qty'  =>  $this->input->post('cQuantity'),
        'p_start'  =>  $this->input->post('from'),
        'p_end'  =>  $this->input->post('to'),
        'p_ingridients'  =>  $this->input->post('ingridients'),
        'p_about'  =>  $this->input->post('about'),
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  '',
        'p_comboid'   =>  $id,
        'p_cat_id'    =>  '',
        'p_sub_catid' => '',
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
       
      );
     
      $responce['comboinfo'] = $this->supper_admin->call_procedure('proc_saveCombo',$parameter);

      $parameterc = array(

        'act_mode'  =>  'updatecomboqtyandcityinfo',
        'p_name'  =>  $this->input->post('startrange1').'to'.$this->input->post('endrange1'),
        'p_qty'  =>  '',
        'p_start'  =>  $this->input->post('startrange2').'to'.$this->input->post('endrange2'),
        'p_end'  =>  $this->input->post('startrange3').'to'.$this->input->post('endrange3'),
        'p_ingridients'  =>  $this->input->post('price1'),
        'p_about'  =>  $this->input->post('price2'),
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  $this->input->post('price3'),
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  $this->input->post('city1'),
        'p_comboid'   =>  $id,
        'p_cat_id'    =>  $this->input->post('city2'),
        'p_sub_catid' => $this->input->post('city3'),
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
       
      );
      //p($parameterc);exit;
     
      $responce['comboqtyandcityinfoupdate'] = $this->supper_admin->call_procedure('proc_saveCombo',$parameterc);
      $this->session->set_flashdata("message", "Your information was successfully Saved.");
      redirect(base_url().'admin/attribute/combolisting');
    }

   
   $parameter1 = array(

        'act_mode'  =>  'singlescominfo',
        'p_name'  =>  '',
        'p_qty'  =>  '',
        'p_start'  =>  '',
        'p_end'  =>  '',
        'p_ingridients'  => '' ,
        'p_about'  =>  '',
        'p_totalqty'  =>  '',
        'p_comboActPrice'  =>  '',
        'p_disPer'  =>  '',
        'p_comb_sellingprice'  =>  '',
        'p_totalsaving'  =>  '',
        'p_combo_img'   =>  '',
        'p_comboid'   => $id,
        'p_cat_id'    =>  '',
        'p_sub_catid' => '',
        'p_product_id'    =>  '',
        'p_product_qty'    =>  '',
        'p_pro_mrp'    =>  '',
        'p_pro_seling'    =>  '',
        'p_pro_combo_price'    =>  ''
     
      );

    $responce['comboinfo'] = $this->supper_admin->call_procedure('proc_saveCombo',$parameter1);
    
    $this->load->view('helper/header');
    $this->load->view('manageattribute/editcombo',$responce);


}

public function get_maincategory() {
       $brandids                    = $this->input->post('brandids');
      //$parameterr                = array('act_mode'=>'brandcategory','row_id'=>$brandids,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
      $parameterr                = array('act_mode'=>'brandcategorycombo','row_id'=>$brandids,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
       $responce['brandcategory']  = $this->supper_admin->call_procedure('proc_category',$parameterr);  
        
        $str = "<select name='brandcategory[]' id='catidd' data-href='".$brandids."' onchange='childcategory($(this));' class='required barndcatid' field='Sub-category'>";
        $str .= "<option value=''>Select category</option>";
        foreach($responce['brandcategory'] as $k=>$v){   
              $str .= '<option value="'.$v->catid.'">'.$v->catname.'</option>';
      }
        $str .= "</select>";
        echo $str;
    }

public function get_subcat() {
       $catidd                    = $this->input->post('catid');
       $brandid = $this->input->post('brandid');
      //$parameterr                = array('act_mode'=>'childcatview','row_id'=>$catidd,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
       $parameterr                = array('act_mode'=>'childcatviewcombo','row_id'=>$catidd,'catparentid'=>$brandid,'cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $responce['childCatdata']  = $this->supper_admin->call_procedure('proc_category',$parameterr);  
        
        $str = "<select name='subcategory[]' id='catidd' data-href='".$brandid."' onchange='get_products($(this))' class='required catidd' field='Sub-category'>";
        $str .= "<option value=''>Select category</option>";
        foreach($responce['childCatdata'] as $k=>$v){   
              $str .= '<option value="'.$v->catid.'">'.$v->catname.'</option>';
      }
        $str .= "</select>";
        echo $str;
    }



public function get_products() {
       
 $catid = $_POST['catid'];
 $brandid = $_POST['brandid'];
    //$parameter         = array('act_mode'=>'catproduct', 'row_id'=>$catid, 'attid'=>'', 'attrname'=>'','atttextname'=>'','catidd'=>'');

     $parameter         = array('act_mode'=>'catproductcombo', 'row_id'=>$catid, 'attid'=>$brandid, 'attrname'=>'','atttextname'=>'','catidd'=>'');
   
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
   
    


$str = "<select name='products[]' onchange='productprice($(this))' class='required products' field='Product'>";
        $str .= "<option value=''>Select Product</option>";
        foreach($responce['vieww'] as $k=>$v){   
   
          $str .= '<option value="'.$v->ProId.'">'.$v->ProductName.'/'.$v->SKUNO.'</option>';
      }
        $str .= "</select>";
        echo $str;
    }

public function viewcombodetail($id){


  $parameter = array('act_mode'    =>'getcomboadmin_dtls',
                      'row_id'      =>$id,
                      'orderstatus' =>'',
                      'order_proid' =>'',
                      'order_id'    =>'',
                      'order_venid' =>'',
                      'order_qty'   =>'',
                      'oldstatus'   =>''
                    );
  $responce['vieww'] = $this->supper_admin->call_procedure('proc_orderflow',$parameter);

  if($this->input->post('updatecombo')=='Update Combo'){
      //p($_POST);exit;

      foreach ($this->input->post('proqty') as $key => $value) {
      
        $parameterpp = array('act_mode'   =>'updatecomboprodata',
                          'row_id'      =>$this->input->post('combomapid')[$key],
                          'orderstatus' =>'',
                          'order_proid' =>'',
                          'order_id'    =>$this->input->post('procomboprice')[$key],
                          'order_venid' =>'',
                          'order_qty'   =>$value,
                          'oldstatus'   =>''
                        );
        $recordpp = $this->supper_admin->call_procedure('proc_orderflow',$parameterpp);
        //p($parameter);

      }

      $parametersum = array('act_mode'    =>'getcombosumdata',
                          'row_id'      =>$id,
                          'orderstatus' =>'',
                          'order_proid' =>'',
                          'order_id'    =>'',
                          'order_venid' =>'',
                          'order_qty'   =>'',
                          'oldstatus'   =>''
                        );
      $record = $this->supper_admin->call_procedureRow('proc_orderflow',$parametersum);
      //p($record);

      $parameterup = array(
                    'act_mode'  =>  'getcomboupdate',
                    'p_name'  =>  '',
                    'p_qty'  =>  '',
                    'p_start'  =>  '',
                    'p_end'  =>  '',
                    'p_ingridients'  => '' ,
                    'p_about'  =>  '',
                    'p_totalqty'  =>  '',
                    'p_comboActPrice'  =>  $record->totalcomboprice,
                    'p_disPer'  =>  ((($record->totalcombo_mrp-$record->totalcomboprice)/$record->totalcombo_mrp)*100),
                    'p_comb_sellingprice'  =>  $record->totalcombo_mrp,
                    'p_totalsaving'  =>  ($record->totalcombo_mrp-$record->totalcomboprice),
                    'p_combo_img'   =>  '',
                    'p_comboid'   => $id,
                    'p_cat_id'    =>  '',
                    'p_sub_catid' => '',
                    'p_product_id'    =>  '',
                    'p_product_qty'    =>  '',
                    'p_pro_mrp'    =>  '',
                    'p_pro_seling'    =>  '',
                    'p_pro_combo_price'    =>  ''     
                    );
     // p($parameterup);
  $recordupdate = $this->supper_admin->call_procedureRow('proc_saveCombo',$parameterup);

      //  exit;
      $this->session->set_flashdata("message", "Your information was successfully Saved.");
      redirect("admin/attribute/viewcombodetail/".$id);
  }






  if($this->input->post('deleteprocombo')=='Delete Combo Products'){

      foreach ($this->input->post('comboproApp') as $key => $value) {
      
        $parameterpp = array('act_mode'   =>'delcomboprodata',
                          'row_id'      =>$value,
                          'orderstatus' =>'',
                          'order_proid' =>'',
                          'order_id'    =>'',
                          'order_venid' =>'',
                          'order_qty'   =>'',
                          'oldstatus'   =>''
                        );
        //exit;
        $recordpp = $this->supper_admin->call_procedure('proc_orderflow',$parameterpp);
        //p($parameter);

      }

      $parametersum = array('act_mode'    =>'getcombosumdata',
                          'row_id'      =>$id,
                          'orderstatus' =>'',
                          'order_proid' =>'',
                          'order_id'    =>'',
                          'order_venid' =>'',
                          'order_qty'   =>'',
                          'oldstatus'   =>''
                        );
      $record = $this->supper_admin->call_procedureRow('proc_orderflow',$parametersum);
      //p($record);

      $parameterup = array(
                    'act_mode'  =>  'getcomboupdate',
                    'p_name'  =>  '',
                    'p_qty'  =>  '',
                    'p_start'  =>  '',
                    'p_end'  =>  '',
                    'p_ingridients'  => '' ,
                    'p_about'  =>  '',
                    'p_totalqty'  =>  '',
                    'p_comboActPrice'  =>  $record->totalcomboprice,
                    'p_disPer'  =>  ((($record->totalcombo_mrp-$record->totalcomboprice)/$record->totalcombo_mrp)*100),
                    'p_comb_sellingprice'  =>  $record->totalcombo_mrp,
                    'p_totalsaving'  =>  ($record->totalcombo_mrp-$record->totalcomboprice),
                    'p_combo_img'   =>  '',
                    'p_comboid'   => $id,
                    'p_cat_id'    =>  '',
                    'p_sub_catid' => '',
                    'p_product_id'    =>  '',
                    'p_product_qty'    =>  '',
                    'p_pro_mrp'    =>  '',
                    'p_pro_seling'    =>  '',
                    'p_pro_combo_price'    =>  ''     
                    );
      // p($parameterup);
      $recordupdate = $this->supper_admin->call_procedureRow('proc_saveCombo',$parameterup);
      //exit;
      
      $this->session->set_flashdata("message", "Your information was successfully Deleted.");
      redirect("admin/attribute/viewcombodetail/".$id);
  }

$this->load->view('helper/header');
$this->load->view('manageattribute/combodetail',$responce);

}


public function existcomboproduct($cdid){
  $parameterr  = array('act_mode'=>'brandlisting','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $responce['branddata'] = $this->supper_admin->call_procedure('proc_category',$parameterr);

if($this->input->post('submit')){

      $parameter1 = array('act_mode'  =>  trim('insertmap'),
                          'p_name'  =>  '',
                          'p_qty'  =>  $this->input->post('brandid'),
                          'p_start'  =>  '',
                          'p_end'  =>  '',
                          'p_ingridients'  => '' ,
                          'p_about'  =>  '',
                          'p_totalqty'  =>  '',
                          'p_comboActPrice'  =>  '',
                          'p_disPer'  =>  0,
                          'p_comb_sellingprice'  =>  0,
                          'p_totalsaving'  =>  0,
                          'p_combo_img'   =>  '',
                          'p_comboid'   =>  $cdid,
                          'p_cat_id'    =>  $this->input->post('mcatidd'),
                          'p_sub_catid' => $this->input->post('catidd'),
                          'p_product_id'    =>  $this->input->post('catproidd'),
                          'p_product_qty'    =>  $this->input->post('comboqty'),
                          'p_pro_mrp'    =>  $this->input->post('combopromrp'),
                          'p_pro_seling'    =>  $this->input->post('combosellpri'),
                          'p_pro_combo_price'    =>  $this->input->post('combopri')
                        );
        
      $result = $this->supper_admin->call_procedureRow('proc_saveCombo',$parameter1);


      $parametersum = array('act_mode'    =>'getcombosumdata',
                          'row_id'      =>$cdid,
                          'orderstatus' =>'',
                          'order_proid' =>'',
                          'order_id'    =>'',
                          'order_venid' =>'',
                          'order_qty'   =>'',
                          'oldstatus'   =>''
                        );
      $record = $this->supper_admin->call_procedureRow('proc_orderflow',$parametersum);
      

      $parameterup = array(
                    'act_mode'  =>  'getcomboupdate',
                    'p_name'  =>  '',
                    'p_qty'  =>  '',
                    'p_start'  =>  '',
                    'p_end'  =>  '',
                    'p_ingridients'  => '' ,
                    'p_about'  =>  '',
                    'p_totalqty'  =>  '',
                    'p_comboActPrice'  =>  $record->totalcomboprice,
                    'p_disPer'  =>  ((($record->totalcombo_mrp-$record->totalcomboprice)/$record->totalcombo_mrp)*100),
                    'p_comb_sellingprice'  =>  $record->totalcombo_mrp,
                    'p_totalsaving'  =>  ($record->totalcombo_mrp-$record->totalcomboprice),
                    'p_combo_img'   =>  '',
                    'p_comboid'   => $cdid,
                    'p_cat_id'    =>  '',
                    'p_sub_catid' => '',
                    'p_product_id'    =>  '',
                    'p_product_qty'    =>  '',
                    'p_pro_mrp'    =>  '',
                    'p_pro_seling'    =>  '',
                    'p_pro_combo_price'    =>  ''     
                    );
      // p($parameterup);
      $recordupdate = $this->supper_admin->call_procedureRow('proc_saveCombo',$parameterup);
      

      $this->session->set_flashdata("message", "Your information was successfully Saved.");
      redirect("admin/attribute/existcomboproduct/".$cdid);
}


  $this->load->view('helper/header');
  $this->load->view('manageattribute/editexistcombo',$responce);

}


public function get_maincategoryedit() {
       $brandids                    = $this->input->post('brandids');
      $parameterr                = array('act_mode'=>'brandcategorycombo','row_id'=>$brandids,'catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
       $responce['brandcategory']  = $this->supper_admin->call_procedure('proc_category',$parameterr);  
        
        foreach($responce['brandcategory'] as $k=>$v){   
              $str .= '<option value="'.$v->catid.'">'.$v->catname.'</option>';
      }
        echo $str;
    }

public function get_subcatedit() {
       $catidd                    = $this->input->post('catid');
       $brandid = $this->input->post('brandid');
       $parameterr                = array('act_mode'=>'childcatviewcombo','row_id'=>$catidd,'catparentid'=>$brandid,'cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $responce['childCatdata']  = $this->supper_admin->call_procedure('proc_category',$parameterr);  
        
      foreach($responce['childCatdata'] as $k=>$v){   
          $str .= '<option value="'.$v->catid.'">'.$v->catname.'</option>';
      }
        echo $str;
    }

public function get_productsedit() {
       
  $catid = $_POST['catid'];
  $brandid = $_POST['brandid'];
  $parameter         = array('act_mode'=>'catproductcombo', 'row_id'=>$catid, 'attid'=>$brandid, 'attrname'=>'','atttextname'=>'','catidd'=>'');
  $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
      foreach($responce['vieww'] as $k=>$v){   
        $str .= '<option value="'.$v->ProId.'">'.$v->ProductName.'/'.$v->SKUNO.'</option>';
      }
        echo $str;
    }


}//end class.
?>