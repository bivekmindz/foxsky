<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends MX_Controller{

  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->userfunction->loginAdminvalidation();

  }
 public function topbanner(){
  if($this->input->post('submit')){
   $url         =$this->input->post('imgurl');
   $allowedExts = array("gif", "jpeg", "jpg", "png");
   $temp        = explode(".", $_FILES["file"]["name"]);
   $extension   = end($temp);
   if ((($_FILES["file"]["type"] == "image/gif")
      || ($_FILES["file"]["type"] == "image/jpeg")
      || ($_FILES["file"]["type"] == "image/jpg")
      || ($_FILES["file"]["type"] == "image/pjpeg")
      || ($_FILES["file"]["type"] == "image/x-png")
      || ($_FILES["file"]["type"] == "image/png"))
      && ($_FILES["file"]["size"] < 10000000)
      && in_array($extension, $allowedExts)) {
      if ($_FILES["file"]["error"] > 0) {
       echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
      } else {
       $label=time();
       $filename = $label.$_FILES["file"]["name"];
      if (file_exists("imagefiles/HomeBanner/" . $filename)) {
       $this->session->set_flashdata('message', 'Image already exists');
       } else {
       move_uploaded_file($_FILES["file"]["tmp_name"],"imagefiles/HomeBanner/" . $filename);
       $type     =$this->input->post('type');
       $parameter=array('act_mode'=>'homeimg','row_id'=>'','imagename'=>$filename,'imageurl'=>$url,'imgtype'=>$type );
       $data['bannerz'] = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
       $this->session->set_flashdata('message', 'Image saved successfully!');
       redirect("admin/home/topbanner");
        }
    }
}//end if
  }
  $parameter=array('act_mode'=>'banner','row_id'=>'','imagename'=>'','imageurl'=>'','imgtype'=>'' );
  $data['banner'] = $this->supper_admin->call_procedure('proc_homebanner',$parameter);

  $this->load->view('helper/header');
  $this->load->view('homepage/topbanner',$data);
 } 

 public function topbannerupdate($id){
  if($this->input->post('submit')){
   $type        =$this->input->post('type');
   $imgurl      =$this->input->post('imgurl');
   $prevideo_img=$this->input->post('oldimagename');
   $allowedExts = array("gif", "jpeg", "jpg", "png");
   $temp        = explode(".",$_FILES["file"]["name"]);
   $extension   = end($temp);
     if ((($_FILES["file"]["type"] == "image/gif")
      || ($_FILES["file"]["type"] == "image/jpeg")
      || ($_FILES["file"]["type"] == "image/jpg")
      || ($_FILES["file"]["type"] == "image/pjpeg")
      || ($_FILES["file"]["type"] == "image/x-png")
      || ($_FILES["file"]["type"] == "image/png"))
      && ($_FILES["file"]["type"] < 90000000)
      && in_array($extension, $allowedExts)) {
       if ($_FILES["video_img"]["error"] > 0) {
         echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
        } else {
       $label=time();
       $filename = $label.$_FILES["file"]["name"];
       $filename = preg_replace("/[^a-zA-Z0-9.]/", "", $filename);
       if (file_exists("imagefiles/HomeBanner/".$filename)) {
        $this->session->set_flashdata('message', 'Image already exists');
        } else {
       move_uploaded_file($_FILES["file"]["tmp_name"],"imagefiles/HomeBanner/".$filename);
       $parameter       =array('act_mode'=>'topupdate','row_id'=>$id,'imagename'=>$filename,'imageurl'=>$imgurl,'imgtype'=>'');
       $data['bannerz'] = $this->supper_admin->call_procedure('proc_homebanner',$parameter);           
       $delImg1         ="imagefiles/HomeBanner/".$prevideo_img;
       unlink($delImg1);
       $this->session->set_flashdata('message', 'Image saved successfully!');
       redirect("admin/home/topbanner");
        }     
      }
  } else {

      $parameter       =array('act_mode'=>'topupdate','row_id'=>$id,'imagename'=>$prevideo_img,'imageurl'=>$imgurl,'imgtype'=>'');
      $data['bannerz'] = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
      $this->session->set_flashdata('message', 'Your information was successfully Updated.');
      redirect('admin/home/topbanner');
        
      }

  }//submit
  $parameter=array('act_mode'=>'viewtop','row_id'=>$id,'imagename'=>'','imageurl'=>'','imgtype'=>'' );
  $data['value'] = $this->supper_admin->call_procedureROW('proc_homebanner',$parameter);
  $this->load->view('helper/header');
  $this->load->view('homepage/edittop',$data);
 }
 

 public function topstatus ($id){
  $rowid             = $this->uri->segment(4);
  $status            = $this->uri->segment(5);
  $act_mode          = $status=='A'?'activetop':'inactivetop';
  $parameter=array('act_mode'=>$act_mode,'row_id'=>$id,'imagename'=>'','imageurl'=>'','imgtype'=>'' );
  $data['value'] = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect('admin/home/topbanner');

}
public function topdelete ($id){
 $parameter         =array('act_mode'=>'viewtop','row_id'=>$id,'imagename'=>'','imageurl'=>'','imgtype'=>'' );
 $data['value']     = $this->supper_admin->call_procedureROW('proc_homebanner',$parameter);
 $delImg1           ="imagefiles/HomeBanner/".$data['value']->t_imgname;
 unlink($delImg1); 
 $parameter         =array('act_mode'=>'topdelete','row_id'=>$id,'imagename'=>'','imageurl'=>'','imgtype'=>'' );
 $data['value']     = $this->supper_admin->call_procedure('proc_homebanner',$parameter);
 $this->session->set_flashdata('message', 'Your Banner was successfully Delete.');
 redirect('admin/home/topbanner');

}
}
?>