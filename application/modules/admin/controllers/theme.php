<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Theme extends MX_Controller{
  public function __construct(){
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }

public function color_theme (){
  $this->load->view('helper/header');
  $this->load->view('theme/colortheme');
}
 
//---------------------- Weight Status  -------------------------//
public function weightstatus (){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='A'?'activeandinactive':'inactiveandinactive';
  $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'title'=>'','unit'=>'', 'value'=>'');
  $response      = $this->supper_admin->call_procedure('proc_weight_des',$parameter);
  $this->session->set_flashdata('message1', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/weight/viewweight');
}

}// class

?>