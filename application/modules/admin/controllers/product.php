<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Product extends MX_Controller{

 public function __construct() {
  $this->load->model("supper_admin");
  $this->load->helper('my_helper');
  $this->load->library('PHPExcel');
  $this->load->library('PHPExcel_IOFactory');
  $this->userfunction->loginAdminvalidation();

 }

public function excel_index(){
  $this->userfunction->loginAdminvalidation();
  if($this->input->post('download')){

   $brandid                  = $this->input->post('brandid');
   $parameter                = array('act_mode'=>'bcategory','row_id'=>$brandid,'');
   $responce['category']     = $this->supper_admin->call_procedure('proc_product',$parameter); 

   $parameter                = array('act_mode'=>'bmcategory','row_id'=>$brandid,'');
   $responce['Maincategory'] = $this->supper_admin->call_procedure('proc_product',$parameter);
   
   $allArr               = array();
   $templevel            = 0;  
   $newkey               = 0;
   $grouparr[$templevel] = "";
   foreach ($responce['category'] as $key => $value) {
      $catid[]    = $value->catid;
      $catname[]  = $value->catname;
   }//foreach
   $finalcatid   = implode($catid, ',');
   $finalcatname = implode($catname, ',');
  //-----------------------------------end category --------------------------------------//

  //---------------------------------main category ---------------------------------------//
    foreach ($responce['Maincategory'] as $key => $val) {
      $mcatid[]     = $val->catid;
      $mcatname1[]  = $val->catname;
    }//foreach

    $mcatname       = array_unique($mcatname1);
    $finalmcatid    = implode(array_unique($mcatid), ',');
    $finalmcatname  = implode(array_unique($mcatname1), ',');
  //--------------------------------end category ------------------------------------------//

   // p($finalcatname);

   //-----------------------------MANUFACTURECODE----------------------------------//
  $parameter            =array('act_mode'=>'vendcode','row_id'=>'','catid'=>'');
  $responce['vendcodee']= $this->supper_admin->call_procedure('proc_product',$parameter); 
  foreach ($responce['vendcodee'] as $key => $value) {
   $manufvalue[]=$value->vendorcode;
   }
   $vendorname=implode($manufvalue, ',');
   //------------------------------END CODE-----------------------------------------//
  
  //------------------------------FEATURE-------------------------------------------//
 $featurevaluee1[]    = 'ManufactureCode';
 $featurevaluee1[]    = 'categories'; 
 $featurevaluee1[]    = 'maincategories'; 
 //$parameter           =array('act_mode'=>'feature','row_id'=>'','catid'=>$finalcatid);
 //$responce['fetname'] = $this->supper_admin->call_procedure('proc_product',$parameter);
 $parameter           =array('act_mode'=>'feature','row_id'=>'','pvalue'=>$finalcatid);
 $responce['fetname'] = $this->supper_admin->call_procedure('proc_productmasterdata',$parameter); 

 foreach ($responce['fetname'] as $key => $value) {
   $featurevaluee[]   = $value->LeftValue;
   $featurevaluee1[]  = $value->LeftValue;
   $featureleftid[]   = $value->FeatureLeftID;
   $groupfeature[$key]= $value->LeftValue;
 }//foreach
 $featurevalue        = array_unique($featurevaluee);
 $newfeatureleftid    = array_unique($featureleftid);

 foreach ($newfeatureleftid as $ke => $value) {
    $parameter        = array('act_mode'=>'getleftfromright','row_id'=>$value,'feaid'=>'','','');
    $data['featureData'][$groupfeature[$ke]]  = $this->supper_admin->call_procedure('proc_feature',$parameter); 
    $j = 0;
 }

  foreach ($data['featureData'] as $k=>$v){
    foreach ($v as $kk=>$vv) {
      if($k == $vv->LeftValue){
        $newfeatureData[$k]   .= $vv->RightValue.', ';
        $newfeatureData2[$k][] = $vv->RightValue;
      }//if
    }//foreach

    $finalfetureData[$k]  = substr($newfeatureData[$k],0,-2);  
    $finalfetureData2[$k] = $newfeatureData2[$k];  
  }//foreach
//-------------------------------END ---------------------------------------------//  

//----------------------------------city group ------------------------------------//
  $parameter            = array('act_mode'=>'citymasterview','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
  $responce['cityview'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  foreach ($responce['cityview'] as $key => $cityval) {  
    $citygroup[]        = $cityval->mastname;
  }//foreach
//-----------------------------------end -------------------------------------------//   

//------------------------------ ATTRIBUTE------------------------------------------//   
 $parameter             = array('act_mode'=>'catatt','row_id'=>'','catid'=>$finalcatid);
 $responce['attname']   = $this->supper_admin->call_procedure('proc_product',$parameter); 

foreach($responce['attname'] as $k=>$v){
    $oldexcelHeadArr[]  = $v->attname;
    $oldexcelHeadArr1[] = $v->attname;
    $excelHeadArrcvc[]  = $v->id;
    $allArr[]           = $v->attname;
    $grouparr[$k]       = $v->attname;
}
$excelHeadArr  = array_merge($featurevalue,$oldexcelHeadArr);
$excelHeadArr1 = array_merge($featurevaluee1,$oldexcelHeadArr1);

//-------------------------------------- END -----------------------------------------//
//------------------------------------ATTRIBUTE VALUE----------------------------------//
foreach ($excelHeadArrcvc as $key => $value) {
    $parameter    = array('act_mode'=>'attvalue','row_id'=>$value,'catid'=>'');
    $data['attData'][$grouparr[$key]] = $this->supper_admin->call_procedure('proc_product',$parameter);
    $j = 0;
 }

foreach ($data['attData'] as $k=>$v) {
  foreach ($v as $kk=>$vv) {
    if($k == $vv->attname){
      $attributeData[$k]    .= $vv->attmapname.', ';
      $attributeData2[$k][] = $vv->attmapname;
    }
  }//foreach

  $oldfinalAttrData[$k]  = substr($attributeData[$k],0,-2);
  $oldfinalAttrData2[$k] = $attributeData2[$k];

}
  
  $oldfinalAttrData['categories']       = $finalcatname;
  $oldfinalAttrData2['categories']      = $catname;
  $oldfinalAttrData['maincategories']   = $finalmcatname;  
  $oldfinalAttrData2['maincategories']  = $mcatname;
  $oldfinalAttrData2['ManufactureCode'] = $manufvalue;
  $oldfinalAttrData['ManufactureCode']  = $vendorname;
  $finalAttrData  = array_merge($oldfinalAttrData,$finalfetureData);
  $finalAttrData2 = array_merge($oldfinalAttrData2,$finalfetureData2);
  // p($test);
  // p( $finalAttrData);
  

//--------------------------------------------END ATTRIBUTE VALUE -------------------------------//
$attCount = array();
foreach($finalAttrData as $k=>$v){
 $valueCount   = count(explode(",", $v));
 $attCount[$k] = $valueCount+1;
} 
$objPHPExcel   = new PHPExcel();
//-------------------------------- First defult Sheet -------------------------------------------------//
$objWorkSheet  = $objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(0);
$finalExcelArr1 = array_merge($excelHeadArr1);

$cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
$j      = 2;
for($i=0;$i<count($finalExcelArr1);$i++){
 $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
 foreach ($finalAttrData2 as $key => $value) {
  foreach ($value as $k => $v) {
    if($key == $finalExcelArr1[$i]){
      $newvar = $j+$k;
      $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
   }
  }
 }  
}
$objPHPExcel->getSheetByName('Worksheet')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
//----------------------------------end first defult sheet ---------------------------------------------// 

//---------------------------------state first product sheet--------------------------------------------//
$arrPart2   = array('ProductName','ArticleNumber','ProductSku','BarcodeNumber','ProductDescription','Stock_QTY','PurchasePrice','Mrp','SellingPrice');
$arrPart22  = array('ProfitMargin_percent','Discount_percent','Range1','Range2','Range3','Price1_percent','Price2_percent','Price3_percent','Height','MeasuringUnit1','Breadth','MeasuringUnit2','Length','MeasuringUnit3','Weight','MeasuringUnit4','VolumetricWeight_KG','Productwarranty','MetaTag1','MetaTag2','MetaTag3','MainImage','Image1','Image2','Image3','Image4','maincategories','categories','ManufactureCode');
$finalExcelArr = array_merge($arrPart2,$citygroup,$arrPart22,$excelHeadArr);
$objPHPExcel->setActiveSheetIndex(1);
$totalfinal = count($finalExcelArr);
$totalatt   = count($excelHeadArr);
$total      = $totalfinal-$totalatt;
$objPHPExcel->getActiveSheet()->setTitle('VendorProductUploadWorksheet#'.$brandid.'');
$cols       = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
 
//Set border style for active worksheet
$styleArray = array(
      'borders' => array(
          'allborders' => array(
            'style'  => PHPExcel_Style_Border::BORDER_THIN
          )
      )
);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArray);
//Set border style for active worksheet

 for($i=0;$i<count($finalExcelArr);$i++){
  // Add heading column to worksheet.
  $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]); 

  //For freezing top heading row.
  $objPHPExcel->getActiveSheet()->freezePane('A2');

  //Set height for column head.
  $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
  
  //Set width for column head.
  $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

  //protect whole sheet.
  $objPHPExcel->getActiveSheet()->getProtection()->setPassword('bizgain');
  $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
  $objPHPExcel->getActiveSheet()->getStyle('A2:'.$cols[$totalfinal].'500')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);

  //Set background color for heading column.
  $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
      array(
          'fill' => array(
              'type'  => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => '71B8FF')
          ),
          'font'  => array(
              'bold'  => false,
              'size'  => 15,
          )
      )
  );

for($k=2;$k <500;$k++){
// Set background color for heading column.
/*  $objPHPExcel->getActiveSheet()->getStyle('J'.$k)->applyFromArray(
      array(
          'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => 'FDFBD7')
          ),
            'font'  => array(
            'bold'  => false,
            'size'  => 15,
            )
      )
  );
  $objPHPExcel->getActiveSheet()->getStyle('K'.$k)->applyFromArray(
      array(
          'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => 'FDFBD7')
          ),
            'font'  => array(
            'bold'  => false,
            'size'  => 15,
            )
      )
  );
   $objPHPExcel->getActiveSheet()->getStyle('T'.$k)->applyFromArray(
      array(
          'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => 'FDFBD7')
          ),
            'font'  => array(
            'bold'  => false,
            'size'  => 15,
            )
      )
  );*/
  
    //Set height for every single row.
    $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);

    // Formula for calculating discount.
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$k.'', '= ((1-(I'.$k.')/(H'.$k.'))*100)');
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$k.'', '= (((I'.$k.')/(G'.$k.')-1)*100)');
    $objPHPExcel->getActiveSheet()->setCellValue('AC'.$k.'', '= ((U'.$k.'*W'.$k.'*Y'.$k.')/5000)');
   
    //Create select box for Manufacturer.
    $objValidation22 = $objPHPExcel->getActiveSheet()->getCell('AO2')->getDataValidation();
    $objValidation22->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation22->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation22->setAllowBlank(false);
    $objValidation22->setShowInputMessage(true);
    $objValidation22->setShowErrorMessage(true);
    $objValidation22->setShowDropDown(true);
    $objValidation22->setErrorTitle('Input error');
    $objValidation22->setError('Value is not in list.');
    $objValidation22->setPromptTitle('Pick from list');
    $objValidation22->setPrompt('Please pick a value from the drop-down list.');
    $objValidation22->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['ManufactureCode']));
    $objPHPExcel->getActiveSheet()->getCell('AO'.$k)->setDataValidation($objValidation22);

    //Create select box for categories.
    $objValidation2 = $objPHPExcel->getActiveSheet()->getCell('AM2')->getDataValidation();
    $objValidation2->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation2->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation2->setAllowBlank(false);
    $objValidation2->setShowInputMessage(true);
    $objValidation2->setShowErrorMessage(true);
    $objValidation2->setShowDropDown(true);
    $objValidation2->setErrorTitle('Input error');
    $objValidation2->setError('Value is not in list.');
    $objValidation2->setPromptTitle('Pick from list');
    $objValidation2->setPrompt('Please pick a value from the drop-down list.');
    $objValidation2->setFormula1('Worksheet!$'.'C$2:$'.'C$'.($attCount['maincategories']));
    $objPHPExcel->getActiveSheet()->getCell('AM'.$k)->setDataValidation($objValidation2);


    $objValidation2 = $objPHPExcel->getActiveSheet()->getCell('AN2')->getDataValidation();
    $objValidation2->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation2->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation2->setAllowBlank(false);
    $objValidation2->setShowInputMessage(true);
    $objValidation2->setShowErrorMessage(true);
    $objValidation2->setShowDropDown(true);
    $objValidation2->setErrorTitle('Input error');
    $objValidation2->setError('Value is not in list.');
    $objValidation2->setPromptTitle('Pick from list');
    $objValidation2->setPrompt('Please pick a value from the drop-down list.');
    $objValidation2->setFormula1('Worksheet!$'.'B$2:$'.'B$'.($attCount['categories']));
    $objPHPExcel->getActiveSheet()->getCell('AN'.$k)->setDataValidation($objValidation2);


     if($i>=$total){
      for($h=0; $h <count($finalExcelArr1); $h++) { 
       if($finalExcelArr1[$h] == $finalExcelArr[$i]){
        #p($finalExcelArr[$i]);
        #p($attCount[$finalExcelArr[$i]]);
       
          $objValidation = $objPHPExcel->getActiveSheet()->getCell($cols[$i].'2')->getDataValidation();
          $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
          $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
          $objValidation->setAllowBlank(false);
          $objValidation->setShowInputMessage(true);
          $objValidation->setShowErrorMessage(true);
          $objValidation->setShowDropDown(true);
          $objValidation->setErrorTitle('Input error');
          $objValidation->setError('Value is not in list.');
          $objValidation->setPromptTitle('Pick from list');
          $objValidation->setPrompt('Please pick a value from the drop-down list.');
          $objValidation->setFormula1('Worksheet!$'.$cols[$h].'$2:$'.$cols[$h].'$'.$attCount[$finalExcelArr[$i]].'');
          $objPHPExcel->getActiveSheet()->getCell($cols[$i].$k)->setDataValidation($objValidation);
        }
      }//forfro
    } //thfor
  }//secfor
}//first main for
#exit;

//----------------------------------end product sheet --------------------------------------------------//
$filename  = 'vendorexcel.xls';
header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
ob_end_clean();
ob_start();
$objWriter->save('php://output');
exit;

}// end final if; 
// --------------------- End Download Excel condition ----------------------


// --------------------- Upload Excel condition ----------------------
if($this->input->post('submit')){
 $barndvalueid= $this->input->post('product_brandid');
 $fileName    = $_FILES['userfile']['tmp_name'];
 $objPHPExcel = PHPExcel_IOFactory::load($fileName);
 $rowsold     = $objPHPExcel->getActiveSheet()->toArray(); 
 $sheetName   = $objPHPExcel->getActiveSheet()->getTitle();
//--------------------------------- attribute value feature value --------------------------------//
 $brandid              = explode('#', $sheetName);
 $parameter            = array('act_mode'=>'bcategory','row_id'=>$brandid[1],'');
 $responce['category'] = $this->supper_admin->call_procedure('proc_product',$parameter); 

 foreach ($responce['category'] as $key => $value) {
  $catid[]   = $value->catid;
  $catname[] = $value->catname;
 }
 $finalcatid           = implode($catid, ',');
 $parameter            = array('act_mode'=>'catatt','row_id'=>'','catid'=>$finalcatid);
 $responce['attname']  = $this->supper_admin->call_procedure('proc_product',$parameter); 

 foreach ($responce['attname'] as $key => $value) {
  $attname[]  = $value->attname.' varchar(250)';
  $attname2[] = $value->attname;
  $attIds[$value->attname] = $value->id;
}
// --------------- final attribute value --------------------- //
$finalattname  = implode($attname, ',');
$finalattname2 = implode($attname2, ',');

$attsql        = 'CREATE temporary TABLE Attributetable ( id INT NOT NULL AUTO_INCREMENT Primary key ,Dynamic_Attribute VARCHAR(250) NOT NULL , Dynamic_Attributevalue VARCHAR(250) NOT NULL, Attid VARCHAR(250) NOT NULL);';   
$attinsert     = 'insert into Attributetable(Dynamic_Attribute,Dynamic_Attributevalue,Attid)';
// --------------------------- end --------------------------//

//----------------- feature value -----------------------//
$parameter            = array('act_mode'=>'feature','row_id'=>'','catid'=>$finalcatid);
$responce['fetname']  = $this->supper_admin->call_procedure('proc_product',$parameter); 

foreach ($responce['fetname'] as $key => $value) {
   $featurevalue[]  = $value->LeftValue.' varchar(250)';
   $featurevalue2[] = $value->LeftValue;
   $fcatIds[$value->LeftValue] = $value->FeatureID;
 }

$featurevalue_unique        = array_unique($featurevalue2);

$finalfeature  = implode($featurevalue, ',');
$finalfeature2 = implode($featurevalue2, ',');
$featuresql    = 'CREATE temporary TABLE tblfeature ( id INT NOT NULL AUTO_INCREMENT Primary key ,feature_left VARCHAR(250) NOT NULL , feture_rightvalue VARCHAR(250) NOT NULL,featuid VARCHAR(250) NOT NULL);';   
$fearureinsert = 'insert into tblfeature(feature_left,feture_rightvalue,featuid)';
//--------------------------------end feature --------------------------------------//

//-----------------------------city value ---------------------------------------//
$parameter            = array('act_mode'=>'citymasterview','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
$responce['cityview'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
foreach ($responce['cityview'] as $key => $cityval) {
  $citygroup[] = $cityval->mastname;
  $cityid[]    = $cityval->cmasterid;
  $citycatIds[$cityval->mastname] = $cityval->cmasterid;
} 

$citysql    = 'CREATE temporary TABLE tblcitynew ( id INT NOT NULL AUTO_INCREMENT Primary key ,citygid VARCHAR(250) NOT NULL , cityvalues VARCHAR(250) NOT NULL);';   
$cityinsert = 'insert into tblcitynew(citygid,cityvalues)';
//--------------------------------end feature --------------------------------------//

//-----------------------------------end attribute value --------------------------//
$headArr = array();
//-------------- remove empty array ------------------------//
#$data    = array_map('array_filter', $rowsold);
#$rows    = array_filter($data);

#$data    = array_map('array_filter', $rowsold);
#$rows    = array_filter($data);

$countAttribute   = count($attname);//Total dynamic attribute count.
$countFeature     = count($featurevalue_unique);//Total dynamic feature count.

$commattfeatu     = $countAttribute + $countFeature;
$countColumn      = (int) 40 + $commattfeatu; //Total dynamic excel columns.

$staticvaluetotal = $countColumn - $commattfeatu;
$totalfeasture    = $staticvaluetotal + $countFeature;
$totalattvalue    = $totalfeasture + $countAttribute;

//---- new logic for removing extra columns.
$rowsold_upd = array();
$rowsold_empty = array();

/*foreach($rowsold as $k=>$v){
  #print ' <br>- top - '.gettype($k);
  if($v[0] == ""){
    unset($v);
  }
  $rowsold_upd[$k] = array_splice($v, $countColumn+1);
  #print '<br> - bottom - '.gettype($k);
  $rowsold[$k] = $v;
  if($k == NULL){
    unset($k);
  }
}*/
for($i=0;$i<count($rowsold);$i++){
  if($rowsold[$i][0] == ""){
    $rowsold_empty[$i] = $i;
  }
  if(is_array($rowsold[$i])){
    $rowsold_upd[$i]   = array_splice($rowsold[$i], $countColumn+1);
  }
}
$rowsold_final         = array_diff_key($rowsold, $rowsold_empty);

//---- new logic for removing extra columns.
$data = $rowsold;
$rows = $data;
#p($rowsold_final);
#exit;
//  -----------------end --------------------------------------//
for($i = 0; $i<count($rows); $i++){
 for($j = 0; $j<count($rows[$i]); $j++){}
  $headArr[] = $rows[$i];
}

for($k=0;$k<count($headArr);$k++){
 if(count($headArr[$k])>2){
 if(gettype($headArr[$k]) == 'array'){
  if($k == 0){
  // ----------------------- create table------------------ //
  $sql = 'CREATE temporary TABLE ExcelTable (';
  for($c=0;$c<count($headArr[0]); $c++){
    if($headArr[0][$c]!=''){
      if($c == 4){
        $sql .= $headArr[0][$c].' text, ';      
      }else{
        $sql .= $headArr[0][$c].' VARCHAR(250),';
      }
    }
  }//foreach

  $sql   .= ')';
  $newsql = substr_replace($sql ,"",-2);
  $newsql.= ')';

    //------------------------end----------------------------//
   
    //------------------------insert table value -------------//
  $sql3  = 'insert into ExcelTable(';
  for($t=0;$t<count($headArr[0]); $t++) {
    if($headArr[0][$t] != ''){
      $sql3    .= $headArr[0][$t].',';
      $sqldata .= $headArr[0][$t].',';
    }
  }
  $sql3    .= ')';
  $newsql3  = substr_replace($sql3 ,"",-2);
  $newsql3 .= ')';

  //---------------------------end value----------------------//

}//end $k==0;

#if($k > 0){
#p(count($rowsold_final));
#exit;
if(($k > 0) && ($k < count($rowsold_final))){

//-------------------feature value ------------------//
$citysql4 = 'values ';
$uur      = 0;

for($ir=9;$ir<12; $ir++){
 $cityVals[$citygroup[$uur]] = $headArr[$k][$ir];//create attribute values array
 $uur++;
}

$citysql4_4     = '';
foreach($cityVals as $c=>$d){
  $citysql4_4[] = "('".$citycatIds[$c]."','".$d."')";
}
$allcitys     = implode(",", $citysql4_4);
$finalcity    = $citysql4.$allcitys;
$citynewsql5  = $cityinsert;
$citynewsql5 .= $finalcity;
$citynewsql5 .= ";";
//---------------------end ------------------------------//

//-------------------feature value ------------------//
$featuresql4 = 'values ';
$uu          = 0;

for($ir=$staticvaluetotal;$ir<$totalfeasture; $ir++){
 $fetaVals[$featurevalue2[$uu]] = $headArr[$k][$ir];//create attribute values array
 $uu++;
}

$featusql4_4 = '';
foreach($fetaVals as $c=>$d){
  $featusql4_4[] = "('".$c."','".$d."','".$fcatIds[$c]."')";
}
$allfeaturVals  = implode(",", $featusql4_4);
$finalfetur     = $featuresql4.$allfeaturVals;
$featurnewsql5  = $fearureinsert;
$featurnewsql5 .= $finalfetur;
$featurnewsql5 .= ";";
//---------------------end ------------------------------//

//--------------attribute value -------------------------//

$attsql4 = 'values ';
$u       = 0;
for($irt = $totalfeasture;$irt<$totalattvalue; $irt++){
 $attVals[$attname2[$u]] = $headArr[$k][$irt];//create attribute values array
 $u++;
}
$attsql4_4 = '';
foreach($attVals as $c=>$d){
  $attsql4_4[] = "('".$c."','".$d."','".$attIds[$c]."')";
}

$allAttVals = implode(",", $attsql4_4);
$finalAtt   = $attsql4.$allAttVals;
$attnewsql5 = $attinsert;
$attnewsql5.= $finalAtt;
$attnewsql5.= ";";

//----------------end attribute ----------------------------//
  $sql4 = 'values(';
  for($i=0;$i<count($headArr[$k]); $i++) {
    #if(trim($headArr[$k][$i])!=''){
      if($headArr[$k][0] !=""){
        //if(($i == 2) OR ($i == 4)){// changed : 13 apr 16
        if(($i == 0) OR ($i == 4)){
           $sql4 .= "'".addslashes($headArr[$k][$i])."'".',';
        }
        else if(($i == 14) OR ($i == 15) OR ($i == 16)){
           $sql4 .= "'".str_replace(" ","",$headArr[$k][$i])."'".',';
        }
        else{
           $sql4 .= "'".$headArr[$k][$i]."'".',';
        }
      }// if($headArr[$k] !="")
    #}
  }
  $sql4    .= ')';

  $newsql4  = substr_replace($sql4 ,"",-2);
  $newsql4 .= ')';
  $newsql5  = $newsql3;
  $newsql5 .= $newsql4;
  $newsql5 .= ";";
  
  $parameter = array('sqlquery'=>$newsql,'sqlquery2'=>$newsql5,'sqlattquery'=>$attsql,'sqlattquery2'=>$attnewsql5,'sqlfeatquery'=>$featuresql,'sqlfeatquery2'=>$featurnewsql5 ,'sqlcity'=>$citysql,'sqlcity2'=>$citynewsql5,'brandid'=>$barndvalueid);
  //p($parameter);exit;
  $responce['upload'] = $this->supper_admin->call_procedure('proc_bulkuploadtest',$parameter);
  $responce['excelErr'] = 'Product excel successfully uploaded.';
 
 }
} 
}// divif end
}//for end
  //exit();
move_uploaded_file($_FILES['userfile']['tmp_name'], FCPATH.'newproductexcels/'.time().'-'.$barndvalueid.'-'.$_FILES['userfile']['name']);
}//firstsubmit if;

  $parameter           = array('act_mode'=>'brmview','row_id'=>'','brandname'=>'','brandimage'=>'','');
  $responce['vieww']   = $this->supper_admin->call_procedure('proc_brand',$parameter); 
  $this->load->view('helper/header');
  $this->load->view('product/productexcel',$responce);

}
/////// -------------  End Function  ------------ ////////

public function unproduct(){
 // pend('hgghg');
  $this->userfunction->loginAdminvalidation();
  //------------------------- approve product----------------------------------------//
  if ($this->input->post('submit')){
    $procatid   = $this->input->post('proApp');
    //pend($procatid);
    $arraycount = count($procatid);
    for($i = 0;$i<$arraycount;$i++){

      $parameter        = array('psku'=>str_replace(';', '', $procatid[$i]));
      //pend($parameter);
      $data['proviewr'] = $this->supper_admin->call_procedure('foo', $parameter);
    }
// Start push notification for app 
    /*$parameterapp = array('act_mode'=>'getallappdeviceid','row_id'=>'','catid'=>'');
    $appdeviceids  = $this->supper_admin->call_procedure('proc_product',$parameterapp);
    $appdeviceiddata = array();
    $appmsgdevice=array('message'=>$arraycount.' new products are launched in bizzgain.','title'=>'New Arrival Products');
    $appnotification='1';
    foreach ($appdeviceids as $key => $value) {
      array_push($appdeviceiddata,$value->appdeviceid);
    }
    send_andriod_notification($appdeviceiddata,$appmsgdevice,$appnotification);*/
// End push notification for app 
    //exit;
    //redirect('admin/product/unproduct');
    header('Location:'.base_url().'admin/product/unproduct?'.$_SERVER['QUERY_STRING']);exit;
   }
  //-------------------------end ----------------------------------------------------//  

//-----------------------delete product--------------------------------------------//
  if($this->input->post('submitdelete')){
    foreach ($this->input->post('proApp') as $key => $value) {
      $parameter          = array('act_mode'=>'venimg','row_id'=>'','catid'=>$value);
      //$responce['vieww']  = $this->supper_admin->call_procedure('proc_product',$parameter);
      foreach ($data['vieww'] as $key => $value) {
        $delImg1 = "images/hoverimg/".$value->venproimgpath;
        $delImg2 = "images/thumimg/". $value->venproimgpath;
        $delImg3 = "images/zoomimg/". $value->venproimgpath;
        unlink($delImg1);
        unlink($delImg2);
        unlink($delImg3);
      }
        $parameter          = array('psku'=>$value);
        //$responce['viewwg'] = $this->supper_admin->call_procedure('proc_unapprovedelete',$parameter);
    }
      //$this->session->set_flashdata("message", "Your information was successfully delete.");
      //header('Location:'.base_url().'admin/product/unproduct?'.$_SERVER['QUERY_STRING']);exit;
  }
//-------------------------- End Delete ----------------------------// 

  $parameter         = array('act_mode'=>'unapproveproduct','row_id'=>'','catid'=>'');
  $responce['vieww'] = $this->supper_admin->call_procedure('proc_product',$parameter); 
 //p(count($responce['vieww']));exit();

  //----------------  Download  Excel ----------------------------//
  if(!empty($this->input->post('newsexcel'))){           
           $paramxls         = array('act_mode'=>'xlsunapprview','row_id'=>'','catid'=>'');
          $xls_unapppro = $this->supper_admin->call_procedure('proc_product',$paramxls); 

           $finalExcelArr = array('Product Name','Artical Number','SKU No','Barcode No','Product Description','Stock Quantity','Purchase Price','MRP','Selling Price','CityGroup1_percent','CityGroup2_percent','CityGroup3_percent','Profit Margin Percent','Discount Percent','FromRange One','ToRange One','RangeOne Percent','FromRange Two','ToRange Two','RangeTwo Percent','FromRange Three','ToRange Three','RangeThree Percent','Height','Height Measuring Unit','Breadth','Breadth Measuring Unit','Length','Length Measuring Unit','Volumetric Weight_Kg','Brand Name','Category Name','Store Name','MetaTag1','MetaTag2','MetaTag3','Landing Price','Product Tax','Tax Applied Status','Minimum Purchase Quantity','Maximum Purchase Quantity','Product Status');
           $objPHPExcel   = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Unapprove Product Worksheet');
           $cols          = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
           $j             = 2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');
            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($xls_unapppro as $key => $value) {             
              $newvar = $j+$key;
              if($value->RStatus=='A'){
                $pro_status='Active';
              } 
              if($value->RStatus=='D'){
                $pro_status='Inactive';
              }
              //Set height for all rows.
              $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);            
              $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->productname);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->skuno);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->Uni_Pro_Sku);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->barcodenumber);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->description);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->ProductQty);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->purchaseprice);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->prodmrp);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->prodsellingprice);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->city_per1);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->city_per2);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->city_per3);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->profitmargin);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->ProDiscount);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $value->qty);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->qty_to);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[16].$newvar, $value->price);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[17].$newvar, $value->qty1);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[18].$newvar, $value->qty1_to);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[19].$newvar, $value->price1);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[20].$newvar, $value->qty2);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[21].$newvar, $value->qty2_to);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[22].$newvar, $value->price2);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[23].$newvar, $value->height);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[24].$newvar, $value->heighttype);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[25].$newvar, $value->width);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[26].$newvar, $value->widthtype);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[27].$newvar, $value->length);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[28].$newvar, $value->lenghttype);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[29].$newvar, $value->volumetricweight);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[30].$newvar, $value->brandname);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[31].$newvar, $value->catname);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[32].$newvar, $value->st_store_name);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[33].$newvar, $value->meta);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[34].$newvar, $value->metakeyword);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[35].$newvar, $value->metadesc);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[36].$newvar, $value->landingPrice);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[37].$newvar, $value->productTax);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[38].$newvar, $value->productStatus);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[39].$newvar, $value->min);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[40].$newvar, $value->max);
              $objPHPExcel->getActiveSheet()->setCellValue($cols[41].$newvar, $pro_status);
            }//foreach.
          }

          $filename  = 'Unapprove Product.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');
      }
  //----------------  End Download Excel ------------------------//

if(!empty($_GET['filter_by']))
    { 
       if($_GET['filter_by']=='sku_no'){
          $parameter         = array('act_mode'=>'viewmunappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>$_GET['searchname'],'atttextname'=>$_GET['filter_by'],'catidd'=>'');
        } else{
          $parameter         = array('act_mode'=>'viewmunappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>'%'.$_GET['searchname'].'%','atttextname'=>$_GET['filter_by'],'catidd'=>'');
        }
       //$parameter         = array('act_mode'=>'viewmappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>$_GET['searchname'],'atttextname'=>$_GET['filter_by'],'catidd'=>'');
       //p($parameter);exit;
       $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
      //----------------  start pagination configuration ------------------------// 

     $config['base_url']         = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
     $config['total_rows']       = count($responce['vieww']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

      //----------------  end pagination configuration ------------------------// 
     
     if($_GET['filter_by']=='sku_no'){
        $parameter         = array('viewmunappro',$page,'',$_GET['searchname'],$_GET['filter_by'],$second);
     } else {
        $parameter         = array('viewmunappro',$page,'','%'.$_GET['searchname'].'%',$_GET['filter_by'],$second);
     }   
    
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
    }else{
  //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/product/unproduct?";
      $config['total_rows']       = count($responce['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

    $parameter         = array('act_mode'=>'unapproveproduct','row_id'=>$page,'catid'=>$second);
    #$responce['vieww'] = $this->supper_admin->call_procedure('proc_product',$parameter); 
    //p(count($responce['vieww']));exit();
    //----------------  end pagination ------------------------//  
}

  $this->load->view('helper/header');
  $this->load->view('product/unapproveprod',$responce); 
}
/////// -------------  End Function  ------------ ////////


public function rejectproductlist(){
  $this->userfunction->loginAdminvalidation();
  $parameter         = array('act_mode'=>'rejectprolist','row_id'=>'','catid'=>'');
  $responce['vieww'] = $this->supper_admin->call_procedure('proc_product',$parameter); 
 //p(count($responce['vieww']));exit();

  //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/product/rejectproductlist?";
      $config['total_rows']       = count($responce['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

    $parameter         = array('act_mode'=>'rejectprolist','row_id'=>$page,'catid'=>$second);
    #$responce['vieww'] = $this->supper_admin->call_procedure('proc_product',$parameter); 
    //p(count($responce['vieww']));exit();
    //----------------  end pagination ------------------------//  


  $this->load->view('helper/header');
  $this->load->view('product/rejectprod',$responce); 
}
/////// -------------  End Function  ------------ ////////


public function productdelete($id){
    $parameter            = array('act_mode'=>'venimg','row_id'=>'','catid'=>$id);
    $responce['vieww']    = $this->supper_admin->call_procedure('proc_product',$parameter); 
    foreach ($data['vieww'] as $key => $value) {
      $delImg1 = "images/hoverimg/".$value->venproimgpath;
      $delImg2 = "images/thumimg/".$value->venproimgpath;
      $delImg3 = "images/zoomimg/".$value->venproimgpath;
      unlink($delImg1);
      unlink($delImg2);
      unlink($delImg3);
    } //end foreach
    $parameter          = array('psku'=>$id);
    $responce['viewwg'] = $this->supper_admin->call_procedure('proc_unapprovedelete',$parameter);
    $this->session->set_flashdata("message", "Your information was successfully delete.");
    redirect('admin/product/unproduct');
}
/////// -------------  End Function  ------------ ////////

public function starproduct(){

 if($this->input->post('submitstatus')){

foreach ($this->input->post( 'attdelete') as $key => $value) {
 $status     = $this->input->post('attstatu')[$value];
 $act_mode   ='starstatusremove';
 $userid = $this->session->userdata('bizzadmin')->LoginID;
 $parameter  = array('act_mode'=>$act_mode,'row_id'=>$value,'catid'=>$userid);
 $response   = $this->supper_admin->call_procedure('proc_product', $parameter );
}
$this->session->set_flashdata("message", "Your Star Status was successfully Updated.");
//redirect("admin/product/approveproduct?".$_SERVER['QUERY_STRING']);
 header('Location:'.base_url().'admin/product/starproduct'.$_SERVER['QUERY_STRING']);exit;

 }


  $param = array('act_mode'=>'star_prdt',
                'blimit'=>'',
                'btype'=>'');
  $data['vieww']= $this->supper_admin->call_procedure('proc_image',$param);
   // echo '<pre>';
   // print_r($data['vieww']);
   // die('/');
  $this->load->view('helper/header');  
  $this->load->view('product/starproduct',$data);  
 

}




public function approveproduct(){
 $this->userfunction->loginAdminvalidation(); 
 //p($_POST);
 //--------------- product status-------------------------// 

 if($this->input->post('submitstar')){

foreach ($this->input->post( 'attdelete') as $key => $value) {
 $status     = $this->input->post('attstatu')[$value];
 $act_mode   ='starstatus';
 $userid = $this->session->userdata('bizzadmin')->LoginID;
 $parameter  = array('act_mode'=>$act_mode,'row_id'=>$value,'catid'=>$userid);
 $response   = $this->supper_admin->call_procedure('proc_product', $parameter );
}
$this->session->set_flashdata("message", "Your Star Status was successfully Updated.");
//redirect("admin/product/approveproduct?".$_SERVER['QUERY_STRING']);
 header('Location:'.base_url().'admin/product/approveproduct?'.$_SERVER['QUERY_STRING']);exit;

 }
if($this->input->post('submitstatus')){
foreach ($this->input->post( 'attdelete') as $key => $value) {
 $status     = $this->input->post('attstatu')[$value];
 $act_mode   = $status=='A'?'activepro':'inactivepro';
 $userid = $this->session->userdata('bizzadmin')->LoginID;
 $parameter  = array('act_mode'=>$act_mode,'row_id'=>$value,'catid'=>$userid);
 $response   = $this->supper_admin->call_procedure('proc_product', $parameter );
}
$this->session->set_flashdata("message", "Your Status was successfully Updated.");
//redirect("admin/product/approveproduct?".$_SERVER['QUERY_STRING']);
 header('Location:'.base_url().'admin/product/approveproduct?'.$_SERVER['QUERY_STRING']);exit;
} 
//------------------end product---------------------------------//
//---------------------delete product----------------------------//
if($this->input->post('submit')){
foreach ($this->input->post( 'attdelete') as $key => $value) {
 $param2         = array('act_mode'=>'imgveiw','row_id'=>$value,'catid'=>'');
 $result['data'] = $this->supper_admin->call_procedure('proc_product', $param2);
foreach ($result['data'] as $key => $v) {
  $delImg1="images/hoverimg/".$v->imgpath;
  $delImg2="images/thumimg/".$v->imgpath;
  $delImg3="images/zoomimg/".$v->imgpath;
  unlink($delImg1);
  unlink($delImg2);
  unlink($delImg3);
}
$param = array('act_mode'=>'deletea','row_id'=>$value,'catid'=>'');
$result['datac'] = $this->supper_admin->call_procedure('proc_product', $param);
}
$this->session->set_flashdata("message", "Your information was successfully delete.");
//redirect("admin/product/approveproduct?".$_SERVER['QUERY_STRING']);
 header('Location:'.base_url().'admin/product/approveproduct?'.$_SERVER['QUERY_STRING']);
exit;
}
//--------------------end product --------------------------------// 
$parameter            =array('act_mode'=>'apprview','row_id'=>'','catid'=>'');
$responce['vieww']  = $this->supper_admin->call_procedure('proc_product',$parameter); 
//pend($responce['vieww']);

//----------------  Download  Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $paramxls =array('act_mode'=>'xlsapprview','row_id'=>'','catid'=>'');
           $app_proxls  = $this->supper_admin->call_procedure('proc_product',$paramxls); 
           
           $finalExcelArr = array('Product Name','Artical Number','SKU No','Barcode No','Product Description','Stock Quantity','Purchase Price','MRP','Selling Price','CityGroup1_percent','CityGroup2_percent','CityGroup3_percent','Profit Margin Percent','Discount Percent','FromRange One','ToRange One','RangeOne Percent','FromRange Two','ToRange Two','RangeTwo Percent','FromRange Three','ToRange Three','RangeThree Percent','Height','Height Measuring Unit','Breadth','Breadth Measuring Unit','Length','Length Measuring Unit','Volumetric Weight_Kg','Product Image Name','Brand Name','Category Name','Store Name','MetaTag1','MetaTag2','MetaTag3','Landing Price','Product Tax','Tax Applied Status','Minimum Purchase Quantity','Maximum Purchase Quantity','Product Status');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Approve Product Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($app_proxls as $key => $value) {
             
            $newvar = $j+$key;

            if($value->ProStatus=='A'){
              $pro_status='Active';
            } else {
              $pro_status='Inactive';
            }

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->ProductName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->SKUNO);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->SellerSku);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->barcode);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->ProDesc);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->proQnty);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->purchaseprice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->productMRP);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->FinalPrice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->city_per1);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->city_per2);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->city_per3);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->profitmargin);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->ProDiscount);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $value->qty1);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->qty1_to);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[16].$newvar, $value->price);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[17].$newvar, $value->qty2);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[18].$newvar, $value->qty2_to);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[19].$newvar, $value->price2);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[20].$newvar, $value->qty3);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[21].$newvar, $value->qty3_to);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[22].$newvar, $value->price3);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[23].$newvar, $value->height);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[24].$newvar, $value->heighttype);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[25].$newvar, $value->width);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[26].$newvar, $value->widthtype);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[27].$newvar, $value->length);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[28].$newvar, $value->lengthtype);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[29].$newvar, $value->volumetricweight);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[30].$newvar, $value->Image);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[31].$newvar, $value->BrandName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[32].$newvar, $value->CatName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[33].$newvar, $value->st_store_name);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[34].$newvar, $value->metatitle);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[35].$newvar, $value->metakeywords);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[36].$newvar, $value->metadescription);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[37].$newvar, $value->landingPrice);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[38].$newvar, $value->productTax);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[39].$newvar, $value->productStatus);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[40].$newvar, $value->min);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[41].$newvar, $value->max);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[42].$newvar, $pro_status);
           
            }
          }

          $filename='Approve Product.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------// 

      if(!empty($_GET['filter_by']))
    { 
       if($_GET['filter_by']=='sku_no'){
          $parameter         = array('act_mode'=>'viewmappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>$_GET['searchname'],'atttextname'=>$_GET['filter_by'],'catidd'=>'');
        } else{
          $parameter         = array('act_mode'=>'viewmappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>'%'.$_GET['searchname'].'%','atttextname'=>$_GET['filter_by'],'catidd'=>'');
          
        }
       //$parameter         = array('act_mode'=>'viewmappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>$_GET['searchname'],'atttextname'=>$_GET['filter_by'],'catidd'=>'');
       //p($parameter);exit;
       $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
      //----------------  start pagination configuration ------------------------// 

     $config['base_url']         = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
     $config['total_rows']       = count($responce['vieww']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

      //----------------  end pagination configuration ------------------------// 
     
     if($_GET['filter_by']=='sku_no'){
        $parameter         = array('viewmappro',$page,'',$_GET['searchname'],$_GET['filter_by'],$second);
     } else {
        $parameter         = array('viewmappro',$page,'','%'.$_GET['searchname'].'%',$_GET['filter_by'],$second);
     }   
    
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
    }else{
      //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/product/approveproduct?";
      $config['total_rows']       = count($responce['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

    $parameter            =array('act_mode'=>'apprview','row_id'=>$page,'catid'=>$second);
    #$responce['vieww']  = $this->supper_admin->call_procedure('proc_product',$parameter); 
    //p($responce['vieww']); exit;
    //----------------  end pagination ------------------------// 
  }

$this->load->view('helper/header');
$this->load->view('product/approductview',$responce);  
}
/////// -------------  End Function  ------------ ////////



public function inactiveapproveproduct(){
 $this->userfunction->loginAdminvalidation(); 
 //--------------- product status-------------------------// 
if($this->input->post('submitstatus')){
foreach ($this->input->post( 'attdelete') as $key => $value) {
 $status     = $this->input->post('attstatu')[$value];
 $act_mode   = $status=='A'?'activepro':'inactivepro';
 $parameter  = array('act_mode'=>$act_mode,'row_id'=>$value,'catid'=>'');
 $response   = $this->supper_admin->call_procedure('proc_product', $parameter );
}
$this->session->set_flashdata("message", "Your Status was successfully Updated.");
redirect("admin/product/approveproduct");
} 
//------------------end product---------------------------------//
//---------------------delete product----------------------------//
if($this->input->post('submit')){
foreach ($this->input->post( 'attdelete') as $key => $value) {
 $param2         = array('act_mode'=>'imgveiw','row_id'=>$value,'catid'=>'');
 $result['data'] = $this->supper_admin->call_procedure('proc_product', $param2);
foreach ($result['data'] as $key => $v) {
  $delImg1="images/hoverimg/".$v->imgpath;
  $delImg2="images/thumimg/".$v->imgpath;
  $delImg3="images/zoomimg/".$v->imgpath;
  unlink($delImg1);
  unlink($delImg2);
  unlink($delImg3);
}
$param = array('act_mode'=>'deletea','row_id'=>$value,'catid'=>'');
$result['datac'] = $this->supper_admin->call_procedure('proc_product', $param);
}
$this->session->set_flashdata("message", "Your information was successfully delete.");
redirect("admin/product/approveproduct");
}
//--------------------end product --------------------------------// 
$parameter            =array('act_mode'=>'apprview','row_id'=>'','catid'=>'');
$responce['vieww']  = $this->supper_admin->call_procedure('proc_product',$parameter); 

//----------------  Download  Excel ----------------------------//

      if(!empty($this->input->post('newsexcel')))
          {
           
           $finalExcelArr = array('SKU Number','Product Name','Category','Brand','QTY','MRP','Selling Price');
           $objPHPExcel = new PHPExcel();
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('Approve Product Worksheet');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

            foreach ($responce['vieww'] as $key => $value) {
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->SKUNO);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->ProductName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->CatName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->BrandName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->proQnty);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->productMRP);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->SellingPrice);
           
            }
          }

          $filename='Approve Product.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }
      //----------------  End Download Excel ------------------------// 

      if(!empty($_GET['filter_by']))
    { 
       if($_GET['filter_by']=='sku_no'){
          $parameter         = array('act_mode'=>'viewmappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>$_GET['searchname'],'atttextname'=>$_GET['filter_by'],'catidd'=>'');
        } else{
          $parameter         = array('act_mode'=>'viewmappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>'%'.$_GET['searchname'].'%','atttextname'=>$_GET['filter_by'],'catidd'=>'');
          
        }
       //$parameter         = array('act_mode'=>'viewmappro', 'row_id'=>'', 'attid'=>'', 'attrname'=>$_GET['searchname'],'atttextname'=>$_GET['filter_by'],'catidd'=>'');
       //p($parameter);exit;
       $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
      //----------------  start pagination configuration ------------------------// 

     $config['base_url']         = base_url(uri_string()).'?'.(isset($_GET['page'])?str_replace('&'.end(explode('&', $_SERVER['QUERY_STRING'])),'',$_SERVER['QUERY_STRING']):str_replace('&page=','',$_SERVER['QUERY_STRING']));
     $config['total_rows']       = count($responce['vieww']);
     $config['per_page']         = 50;
     $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

      //----------------  end pagination configuration ------------------------// 
     
     if($_GET['filter_by']=='sku_no'){
        $parameter         = array('viewmappro',$page,'',$_GET['searchname'],$_GET['filter_by'],$second);
     } else {
        $parameter         = array('viewmappro',$page,'','%'.$_GET['searchname'].'%',$_GET['filter_by'],$second);
     }   
    
    $responce['vieww'] = $this->supper_admin->call_procedure('proc_attribute',$parameter);
    }else{
      //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/product/approveproduct?";
      $config['total_rows']       = count($responce['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $responce["links"]  = explode('&nbsp;',$str_links );

    $parameter            =array('act_mode'=>'apprview','row_id'=>$page,'catid'=>$second);
    $responce['vieww']  = $this->supper_admin->call_procedure('proc_product',$parameter); 
    //p($responce['vieww']); exit;
    //----------------  end pagination ------------------------// 
  }

$this->load->view('helper/header');
$this->load->view('product/inactiveapproductview',$responce);  
}
/////// -------------  End Function  ------------ ////////






public function deleteappproduct(){
  $rowid          =  $this->uri->segment(4);
  $param2         = array('act_mode'=>'imgveiw','row_id'=>$rowid,'catid'=>'');
  $result['data'] = $this->supper_admin->call_procedure('proc_product', $param2);
  foreach ($result['data'] as $key => $v) {
    $delImg1      = "images/hoverimg/". $v->imgpath;
    $delImg2      = "images/thumimg/" . $v->imgpath;
    $delImg3      = "images/zoomimg/" . $v->imgpath;
    unlink($delImg1);
    unlink($delImg2);
    unlink($delImg3);
  }
  $param           = array('act_mode'=>'deletea','row_id'=>$rowid,'catid'=>'');
  $result['datac'] = $this->supper_admin->call_procedure('proc_product', $param);
  redirect("admin/product/approveproduct");
}


public function editstarproduct(){

  $img_id = $this->uri->segment('4');
if($this->input->post('submit')){
  $param = array('act_mode'=>'edit_star_prdt',
                'blimit'=>$img_id,
                'btype'=>''
                );
  $data['vieww']= $this->supper_admin->call_procedureRow('proc_image',$param);

}
  $this->load->view('helper/header');  
  $this->load->view('product/editstarproduct',$data);  

}




public function editproduct(){
   $rowid =  $this->uri->segment(4);
   $mapid  = $this->uri->segment(5); //exit();
 if($this->input->post('proupdate'))
 {

  $r_userid = $this->session->userdata('bizzadmin')->LoginID;

  //pend($_POST);
  $parameter_data = array( 'act_mode'    =>'updateproduct',
  'row_id'      =>$rowid,  
'brandid'     =>'',  
'catid'       =>'', 
'mapid'       =>$mapid,
'colorname'   =>'',  
'agename'     =>'',
'manufacture' =>$r_userid,   
'pname'       =>$this->input->post('pname'),  
'anumber '    =>$this->input->post('anumber'),
'psku '       =>$this->input->post('psku'),
'barcode '    =>$this->input->post('barcode'),  
'pdes'        =>$this->input->post('pdes'), 
'qty'         =>$this->input->post('qty'),
'pprice'      =>$this->input->post('pprice'), 
'sprice'      =>$this->input->post('sprice'), 
'pmrp'        =>$this->input->post('pmrp'), 
'cg1p'        =>'',
'cg2p'        =>'',
'cg3p'        =>'', 
'pmargin'     =>'',
'discount'    =>'',
'range1'      =>'',  
'range2'      =>'', 
'range3'      =>'',  
'price1'      =>'', 
'price2'      =>'',  
'price3'      =>'',  
'hight'       =>'', 
'munit1'      =>'',  
'breadth'     =>'', 
'munit2'      =>'',  
'p_length'    =>'',
'munit3'      =>'',  
'weight'      =>'', 
'munit4'      =>'',  
'volweight'   =>'', 
'pwarranty'   =>'',  
'metatag1'    =>'', 
'metatag2'    =>'',  
'metatag3'    =>'',
'mainimg'     =>'',  
'img1'        =>'', 
'img2'        =>'',  
'img3'        =>'', 
'img4'        =>'',
'featurename'            =>''
                                               
                            );


 $result['data'] = $this->supper_admin->call_procedure('proc_singleproduct', $parameter_data);
 /******************attribute update*************/

$dkid=$this->input->post('attribute'); 
$attr= explode(',', $this->input->post('colorname').','.$this->input->post('agename'));
foreach ($attr as $key => $value) {
  $dsfsd=explode('#', $value);

   $parameter_attrb = array( 'act_mode'    =>'updateattribute',
  'row_id'      =>$dkid[$key],  
'brandid'     =>'',  
'catid'       =>'', 
'mapid'       =>$mapid,
'colorname'   =>$dsfsd[0],  
'agename'     =>$dsfsd[1],
'manufacture' =>'',   
'pname'       =>'',  
'anumber '    =>'',
'psku '       =>'',
'barcode '    =>'',  
'pdes'        =>'', 
'qty'         =>'',
'pprice'      =>'', 
'sprice'      =>'', 
'pmrp'        =>'',  
'cg1p'        =>'',
'cg2p'        =>'',
'cg3p'        =>'', 
'pmargin'     =>'',
'discount'    =>'',
'range1'      =>'',  
'range2'      =>'', 
'range3'      =>'',  
'price1'      =>'', 
'price2'      =>'',  
'price3'      =>'',  
'hight'       =>'', 
'munit1'      =>'',  
'breadth'     =>'', 
'munit2'      =>'',  
'p_length'    =>'',
'munit3'      =>'',  
'weight'      =>'', 
'munit4'      =>'',  
'volweight'   =>'', 
'pwarranty'   =>'',  
'metatag1'    =>'', 
'metatag2'    =>'',  
'metatag3'    =>'',
'mainimg'     =>'',  
'img1'        =>'', 
'img2'        =>'',  
'img3'        =>'', 
'img4'        =>'',
'featurename'            =>''
                                               
                            );
  

 $result['data'] = $this->supper_admin->call_procedure('proc_singleproduct', $parameter_attrb);
}

/******************citymap update*************/
// $citymap=array('1' =>$this->input->post('cg1p') ,'2' =>$this->input->post('cg2p'),'3' =>$this->input->post('cg3p') );
// foreach ($citymap as $key => $value) {
  

//    $parameter_city= array( 'act_mode'    =>'updatecity',
//   'row_id'      =>$key,  
// 'brandid'     =>'',  
// 'catid'       =>'', 
// 'mapid'       =>$mapid,
// 'colorname'   =>'',  
// 'agename'     =>'',
// 'manufacture' =>'',   
// 'pname'       =>'',  
// 'anumber '    =>'',
// 'psku '       =>'',
// 'barcode '    =>'',  
// 'pdes'        =>'', 
// 'qty'         =>'',
// 'pprice'      =>'', 
// 'sprice'      =>'', 
// 'pmrp'        =>'',  
// 'cg1p'        =>$value,
// 'cg2p'        =>'',
// 'cg3p'        =>'', 
// 'pmargin'     =>'',
// 'discount'    =>'',
// 'range1'      =>'',  
// 'range2'      =>'', 
// 'range3'      =>'',  
// 'price1'      =>'', 
// 'price2'      =>'',  
// 'price3'      =>'',  
// 'hight'       =>'', 
// 'munit1'      =>'',  
// 'breadth'     =>'', 
// 'munit2'      =>'',  
// 'p_length'    =>'',
// 'munit3'      =>'',  
// 'weight'      =>'', 
// 'munit4'      =>'',  
// 'volweight'   =>'', 
// 'pwarranty'   =>'',  
// 'metatag1'    =>'', 
// 'metatag2'    =>'',  
// 'metatag3'    =>'',
// 'mainimg'     =>'',  
// 'img1'        =>'', 
// 'img2'        =>'',  
// 'img3'        =>'', 
// 'img4'        =>'',
// 'featurename'            =>''                                             
//                             );
  

//  $result['data'] = $this->supper_admin->call_procedure('proc_singleproduct', $parameter_city);
// }
/******************image update*************/
$userdir = $this->session->userdata('bizzadmin')->LoginID;
 
 
 FCPATH.'uploads/'.$userdir;
 if($userdir==''){
    print '<br>No vendor available.';
    exit;
 }
  else{
    chmod(FCPATH.'uploads/', 0777);
    mkdir(FCPATH.'uploads/'.$userdir);
    chmod(FCPATH.'uploads/'.$userdir, 0777);
  }
 

if(!empty($_FILES['mainimg']['name']))
{

   $main=$_FILES['mainimg']['name'];
   /*$mainext = pathinfo(FCPATH."images/zoomimg/DAS-0079.jpg", PATHINFO_EXTENSION);
   p($mainext);
   p(getimagesize(FCPATH."images/zoomimg/DAS-0079.jpg"));
   p(getimagesize(FCPATH."images/zoomimg/a.jpg"));
   p(getimagesize(FCPATH."images/zoomimg/check.png"));
   exit; */
   $mainext = pathinfo($main, PATHINFO_EXTENSION);
   //p($main);
   //p($mainext);exit;
   $main_id=$this->input->post('mainimg_id');
   
   //$main_id=$this->input->post('mainimg_id');
  $filedir                 = FCPATH.'uploads/'.$userdir.'/';
  $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
  $config['allowed_types'] = 'gif|jpg|png|jpeg';
  $config['max_size'] = '2048';
       //$this->upload->initialize($config);
  $this->load->library('upload', $config);
  if ( ! $this->upload->do_upload('mainimg')){
    //print $error = array('error' => $this->upload->display_errors());
    print $error = $this->upload->display_errors();
    //exit;
  }
  else{
    $data   = array('upload_data' => $this->upload->data());
    $file   = $data['upload_data']['full_path'];
    $nofile = $data['upload_data']['file_name'];
    chmod($file,0777);
    
         $readdir = scandir($filedir,0);

      $newwidth  = 450;
                list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
                 
                $ratio_orig = $width_orig/$height_orig;
                $height     = $newwidth/$ratio_orig;
                
                $newheight = $height;
                $actulan   = FCPATH."images/hoverimg/".$main;
                  
                $pt = FCPATH."uploads/".$userdir.'/'.$main;
                chmod($pt,0777);
                //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                if($mainext=="jpg" || $mainext=="jpeg")
                {
                 compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                elseif($mainext=="png")
                {
                 compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                else
                {
                  compressImageany(".".$mainext,$pt,$path,$actulan,$newwidth,$newheight);
                }

                // --------------------------------
                $newwidth1        = 300;
                list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
                 
                $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
                $height_thumb     = $newwidth1/$ratio_orig_thumb;
                
                $newheight1       = $height_thumb;

                $actulan1 = FCPATH."images/thumimg/".$main;
                $pt1      = FCPATH."uploads/".$userdir.'/'.$main;
                chmod($pt1,0777);
                //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                if($mainext=="jpg" || $mainext=="jpeg")
                {
                  compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                } 
                elseif($mainext=="png")
                {
                  compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }
                else
                {
                  compressImageany(".".$mainext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }  
                 //---------------------------------------
                #print './uploads/'.$userdir."/".$readdir[$i];
                #print '<br>';
                #print "./images/zoomimg/".$readdir[$i];
                @rename(FCPATH.'uploads/'.$userdir."/".$main, FCPATH."images/zoomimg/".$main); 

}

}
else if(!empty($this->input->post('old_mainimg')))
{
  $main=$this->input->post('old_mainimg');
  $main_id=$this->input->post('mainimg_id');
}
else{
   $main='na';
   $main_id=$this->input->post('mainimg_id');
}

if(!empty($_FILES['img1']['name']))
{
    $img1=$_FILES['img1']['name'];
    $img1ext = pathinfo($img1, PATHINFO_EXTENSION);
    $img1_id=$this->input->post('img1_id');
    $filedir                 = FCPATH.'uploads/'.$userdir.'/';
    $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '2048';
       //$this->upload->initialize($config);
  $this->load->library('upload', $config);
  if ( ! $this->upload->do_upload('img1')){
    #print $error = array('error' => $this->upload->display_errors());
    print $error = $this->upload->display_errors();
  }
  else{
    $data   = array('upload_data' => $this->upload->data());
     $file   = $data['upload_data']['full_path'];
    $nofile = $data['upload_data']['file_name'];
    chmod($file,0777);
    
         $readdir = scandir($filedir,0);

      $newwidth  = 450;
                list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$img1);
                 
                $ratio_orig = $width_orig/$height_orig;
                $height     = $newwidth/$ratio_orig;
                
                $newheight = $height;
                $actulan   = FCPATH."images/hoverimg/".$img1;
                  
                $pt = FCPATH."uploads/".$userdir.'/'.$img1;
                chmod($pt,0777);
                //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                if($img1ext=="jpg" || $img1ext=="jpeg")
                {
                 compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                elseif($img1ext=="png")
                {
                 compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                else
                {
                  compressImageany(".".$img1ext,$pt,$path,$actulan,$newwidth,$newheight);
                }

                // --------------------------------
                $newwidth1        = 300;
                list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$img1);
                 
                $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
                $height_thumb     = $newwidth1/$ratio_orig_thumb;
                
                $newheight1       = $height_thumb;

                $actulan1 = FCPATH."images/thumimg/".$img1;
                $pt1      = FCPATH."uploads/".$userdir.'/'.$img1;
                chmod($pt1,0777);
                //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                if($img1ext=="jpg" || $img1ext=="jpeg")
                {
                  compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                } 
                elseif($img1ext=="png")
                {
                  compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }
                else
                {
                  compressImageany(".".$img1ext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }  
                 //---------------------------------------
                #print './uploads/'.$userdir."/".$readdir[$i];
                #print '<br>';
                #print "./images/zoomimg/".$readdir[$i];
                @rename(FCPATH.'uploads/'.$userdir."/".$img1, FCPATH."images/zoomimg/".$img1); 
}


}
else if(!empty($this->input->post('old_img1')))
{
  $img1=$this->input->post('old_img1');
  $img1_id=$this->input->post('img1_id');
}
else{
   $img1='na';
   $img1_id=$this->input->post('img1_id');
}
if(!empty($_FILES['img2']['name']))
{
  $img2=$_FILES['img2']['name'];
   $img2ext = pathinfo($img2, PATHINFO_EXTENSION);
$img2_id=$this->input->post('img2_id');
   $filedir                 = FCPATH.'uploads/'.$userdir.'/';
  $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '2048';
       //$this->upload->initialize($config);
  $this->load->library('upload', $config);
  if ( ! $this->upload->do_upload('img2')){
    #print $error = array('error' => $this->upload->display_errors());
    print $error = $this->upload->display_errors();
  }
  else{
    $data   = array('upload_data' => $this->upload->data());
$file   = $data['upload_data']['full_path'];
    $nofile = $data['upload_data']['file_name'];
    chmod($file,0777);
    
         $readdir = scandir($filedir,0);

      $newwidth  = 450;
                list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$img2);
                 
                $ratio_orig = $width_orig/$height_orig;
                $height     = $newwidth/$ratio_orig;
                
                $newheight = $height;
                $actulan   = FCPATH."images/hoverimg/".$img2;
                  
                $pt = FCPATH."uploads/".$userdir.'/'.$img2;
                chmod($pt,0777);
                //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                if($img2ext=="jpg" || $img2ext=="jpeg")
                {
                 compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                elseif($img2ext=="png")
                {
                 compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                else
                {
                  compressImageany(".".$img2ext,$pt,$path,$actulan,$newwidth,$newheight);
                }

                // --------------------------------
                $newwidth1        = 300;
                list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$img2);
                 
                $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
                $height_thumb     = $newwidth1/$ratio_orig_thumb;
                
                $newheight1       = $height_thumb;

                $actulan1 = FCPATH."images/thumimg/".$img2;
                $pt1      = FCPATH."uploads/".$userdir.'/'.$img2;
                chmod($pt1,0777);
                //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                if($img2ext=="jpg" || $img2ext=="jpeg")
                {
                  compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                } 
                elseif($img2ext=="png")
                {
                  compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }
                else
                {
                  compressImageany(".".$img2ext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }  
                 //---------------------------------------
                #print './uploads/'.$userdir."/".$readdir[$i];
                #print '<br>';
                #print "./images/zoomimg/".$readdir[$i];
                @rename(FCPATH.'uploads/'.$userdir."/".$img2, FCPATH."images/zoomimg/".$img2); 
}


}
else if(!empty($this->input->post('old_img2')))
{
  $img2=$this->input->post('old_img2');
  $img2_id=$this->input->post('img2_id');
}
else{
   $img2='na';
   $img2_id=$this->input->post('img2_id');
}

if(!empty($_FILES['img3']['name']))
{
  $img3=$_FILES['img3']['name'];
  $img3ext = pathinfo($img3, PATHINFO_EXTENSION);
$img3_id=$this->input->post('img3_id');
   $filedir                 = FCPATH.'uploads/'.$userdir.'/';
  $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '2048';
       //$this->upload->initialize($config);
  $this->load->library('upload', $config);
  if ( ! $this->upload->do_upload('img3')){
    #print $error = array('error' => $this->upload->display_errors());
    print $error = $this->upload->display_errors();
  }
  else{
    $data   = array('upload_data' => $this->upload->data());
$file   = $data['upload_data']['full_path'];
    $nofile = $data['upload_data']['file_name'];
    chmod($file,0777);
    
         $readdir = scandir($filedir,0);

      $newwidth  = 450;
                list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$img3);
                 
                $ratio_orig = $width_orig/$height_orig;
                $height     = $newwidth/$ratio_orig;
                
                $newheight = $height;
                $actulan   = FCPATH."images/hoverimg/".$img3;
                  
                $pt = FCPATH."uploads/".$userdir.'/'.$img3;
                chmod($pt,0777);
                //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                if($img3ext=="jpg" || $img3ext=="jpeg")
                {
                 compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                elseif($img3ext=="png")
                {
                 compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                else
                {
                  compressImageany(".".$img3ext,$pt,$path,$actulan,$newwidth,$newheight);
                }

                // --------------------------------
                $newwidth1        = 300;
                list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$img3);
                 
                $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
                $height_thumb     = $newwidth1/$ratio_orig_thumb;
                
                $newheight1       = $height_thumb;

                $actulan1 = FCPATH."images/thumimg/".$img3;
                $pt1      = FCPATH."uploads/".$userdir.'/'.$img3;
                chmod($pt1,0777);
                //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                if($img3ext=="jpg" || $img3ext=="jpeg")
                {
                  compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                } 
                elseif($img3ext=="png")
                {
                  compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }
                else
                {
                  compressImageany(".".$img3ext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }  
                 //---------------------------------------
                #print './uploads/'.$userdir."/".$readdir[$i];
                #print '<br>';
                #print "./images/zoomimg/".$readdir[$i];
                @rename(FCPATH.'uploads/'.$userdir."/".$img3, FCPATH."images/zoomimg/".$img3); 
}


}
else if(!empty($this->input->post('old_img3')))
{
  $img3=$this->input->post('old_img3');
  $img3_id=$this->input->post('img3_id');
}
else{
   $img3='na';
   $img3_id=$this->input->post('img3_id');
}


if(!empty($_FILES['img4']['name']))
{
  $img4=$_FILES['img4']['name'];
  $img4ext = pathinfo($img4, PATHINFO_EXTENSION);
$img4_id=$this->input->post('img4_id');
   $filedir                 = FCPATH.'uploads/'.$userdir.'/';
  $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '2048';
       //$this->upload->initialize($config);
  $this->load->library('upload', $config);
  if ( ! $this->upload->do_upload('img4')){
    #print $error = array('error' => $this->upload->display_errors());
    print $error = $this->upload->display_errors();
  }
  else{
    $data   = array('upload_data' => $this->upload->data());
$file   = $data['upload_data']['full_path'];
    $nofile = $data['upload_data']['file_name'];
    chmod($file,0777);
    
         $readdir = scandir($filedir,0);

      $newwidth  = 450;
                list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$img4);
                 
                $ratio_orig = $width_orig/$height_orig;
                $height     = $newwidth/$ratio_orig;
                
                $newheight = $height;
                $actulan   = FCPATH."images/hoverimg/".$img4;
                  
                $pt = FCPATH."uploads/".$userdir.'/'.$img4;
                chmod($pt,0777);
                //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                if($img4ext=="jpg" || $img4ext=="jpeg")
                {
                 compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                elseif($img4ext=="png")
                {
                 compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                else
                {
                  compressImageany(".".$img4ext,$pt,$path,$actulan,$newwidth,$newheight);
                }

                // --------------------------------
                $newwidth1        = 300;
                list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$img4);
                 
                $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
                $height_thumb     = $newwidth1/$ratio_orig_thumb;
                
                $newheight1       = $height_thumb;

                $actulan1 = FCPATH."images/thumimg/".$img4;
                $pt1      = FCPATH."uploads/".$userdir.'/'.$img4;
                chmod($pt1,0777);
                //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                if($img4ext=="jpg" || $img4ext=="jpeg")
                {
                  compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                } 
                elseif($img4ext=="png")
                {
                  compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }
                else
                {
                  compressImageany(".".$img4ext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
                } 
                 //---------------------------------------
                #print './uploads/'.$userdir."/".$readdir[$i];
                #print '<br>';
                #print "./images/zoomimg/".$readdir[$i];
                @rename(FCPATH.'uploads/'.$userdir."/".$img4, FCPATH."images/zoomimg/".$img4); 
}


}
else if(!empty($this->input->post('old_img4')))
{
  $img4=$this->input->post('old_img4');
  $img4_id=$this->input->post('img4_id');
}
else{
   $img4='na';
   $img4_id=$this->input->post('img4_id');
} 


$image_in=array($main,$img1,$img2,$img3,$img4);
$image_id=array($main_id,$img1_id,$img2_id,$img3_id,$img4_id);
foreach ($image_in as $key => $value) {
  $imagesd[]= array($image_id[$key] => $image_in[$key]);
}
//$imagesd=array_combine($image_id, $mage_in);
//p($image_in); p($image_id);
//p($imagesd); exit();
 

foreach ($imagesd as $key => $value) {
  foreach ($value as $key2 => $value2) {
    
  
  

   $parameter_image= array( 'act_mode'    =>'updateimage',
  'row_id'      =>$key2,  
'brandid'     =>'',  
'catid'       =>'', 
'mapid'       =>$mapid,
'colorname'   =>'',  
'agename'     =>'',
'manufacture' =>'',   
'pname'       =>'',  
'anumber '    =>'',
'psku '       =>'',
'barcode '    =>'',  
'pdes'        =>'', 
'qty'         =>'',
'pprice'      =>'', 
'sprice'      =>'', 
'pmrp'        =>'',  
'cg1p'        =>'',
'cg2p'        =>'',
'cg3p'        =>'', 
'pmargin'     =>'',
'discount'    =>'',
'range1'      =>'',  
'range2'      =>'', 
'range3'      =>'',  
'price1'      =>'', 
'price2'      =>'',  
'price3'      =>'',  
'hight'       =>'', 
'munit1'      =>'',  
'breadth'     =>'', 
'munit2'      =>'',  
'p_length'    =>'',
'munit3'      =>'',  
'weight'      =>'', 
'munit4'      =>'',  
'volweight'   =>'', 
'pwarranty'   =>'',  
'metatag1'    =>'', 
'metatag2'    =>'',  
'metatag3'    =>'',
'mainimg'     =>$value2,  
'img1'        =>'', 
'img2'        =>'',  
'img3'        =>'', 
'img4'        =>'',
'featurename'            =>''                                             
                            );
   

 $result['data'] = $this->supper_admin->call_procedure('proc_singleproduct', $parameter_image);


    $parameter_image2= array( 'act_mode'    =>'mainimgupdate',
  'row_id'      =>'',  
'brandid'     =>'',  
'catid'       =>'', 
'mapid'       =>$mapid,
'colorname'   =>'',  
'agename'     =>'',
'manufacture' =>'',   
'pname'       =>'',  
'anumber '    =>'',
'psku '       =>'',
'barcode '    =>'',  
'pdes'        =>'', 
'qty'         =>'',
'pprice'      =>'', 
'sprice'      =>'', 
'pmrp'        =>'',  
'cg1p'        =>'',
'cg2p'        =>'',
'cg3p'        =>'', 
'pmargin'     =>'',
'discount'    =>'',
'range1'      =>'',  
'range2'      =>'', 
'range3'      =>'',  
'price1'      =>'', 
'price2'      =>'',  
'price3'      =>'',  
'hight'       =>'', 
'munit1'      =>'',  
'breadth'     =>'', 
'munit2'      =>'',  
'p_length'    =>'',
'munit3'      =>'',  
'weight'      =>'', 
'munit4'      =>'',  
'volweight'   =>'', 
'pwarranty'   =>'',  
'metatag1'    =>'', 
'metatag2'    =>'',  
'metatag3'    =>'',
'mainimg'     =>$main,  
'img1'        =>'', 
'img2'        =>'',  
'img3'        =>'', 
'img4'        =>'',
'featurename'            =>''                                             
                            );
   

$result['data'] = $this->supper_admin->call_procedure('proc_singleproduct', $parameter_image2);
$directory = FCPATH."uploads/".$userdir;
          recursiveRemoveDirectory($directory);
         
}
}
$this->session->set_flashdata('message','Product was successfully Updated');
  
  //redirect('/admin/product/approveproduct?'.$_SERVER['QUERY_STRING'], 'location', 301);
  header('Location:'.base_url().'admin/product/approveproduct?'.$_SERVER['QUERY_STRING']);
exit;
 }
  //  $parametercity = array( 'act_mode'    =>'citydata',  
  //                           'p_brandId'     =>'',  
  //                           'p_proid'       =>$rowid,  
  //                           'p_promapid'       =>'',
  //                           'p_cityid' =>'',  
  //                           'p_proname'    =>'',
  //                           'p_prosku' => '', 
  //                           'p_barcode'    =>'',  
  //                           'p_description'   =>'', 
  //                           'p_proqty'       =>'',  
  //                           'p_prodpurchaseprice'   =>'', 
  //                           'p_prodmrp'     =>'',  
  //                           'p_prodsellingprice'  =>'',
  //                           'p_finalprice'=>'',  
  //                           'p_cityvalue'      =>'',  
  //                           'p_profitmargin'      =>'',
  //                           'p_discount'      =>'',  
  //                           'p_qty1'      =>'', 
  //                           'p_qty2'      =>'',  
  //                           'p_qty3'   =>'', 
  //                           'p_price'   =>'',  
  //                           'p_price2'    =>'', 
  //                           'p_price3'    =>''  
  //                     );
 


 
  // $result['city'] = $this->supper_admin->call_procedure('proc_bulkproductupdate',$parametercity);
 
  $parameter_data = array('act_mode'    =>'editproduct',  
                            'row_id'      =>$rowid,
                            'brandid'     =>'',  
                            'catid'       =>'', 
                            'mapid'       =>$mapid,
                            'colorname'   =>'',  
                            'agename'     =>'',
                            'manufacture' =>'',   
                            'pname'       =>'',  
                            'anumber '    =>'',
                             'psku '       =>'',
                            'barcode '    =>'',  
                            'pdes'        =>'', 
                            'qty'         =>'',
                            'pprice'      =>'', 
                            'sprice'      =>'', 
                            'pmrp'        =>'',  
                            'cg2p'        =>'',
                            'cg1p'        =>'',
                            'cg3p'        =>'', 
                            'pmargin'     =>'',
                            'discount'    =>'',
                            'range1'      =>'',  
                            'range2'      =>'', 
                            'range3'      =>'',  
                            'price1'      =>'', 
                            'price2'      =>'',  
                            'price3'      =>'',  
                            'hight'       =>'', 
                            'munit1'      =>'',  
                            'breadth'     =>'', 
                            'munit2'      =>'',  
                            'p_length'      =>'',
                            'munit3'      =>'',  
                            'weight'      =>'', 
                            'munit4'      =>'',  
                            'volweight'   =>'', 
                            'pwarranty'   =>'',  
                            'metatag1'    =>'', 
                            'metatag2'    =>'',  
                            'metatag3'    =>'',
                            'mainimg'     =>'',  
                            'img1'        =>'', 
                            'img2'        =>'',  
                            'img3'        =>'', 
                            'img4'        =>'',
                            'featurename' =>''
                                               
                            );

 $result['data'] = $this->supper_admin->call_procedure('proc_singleproduct', $parameter_data);

 $parameter_image = array( 'act_mode'    =>'editimage',  
                            'row_id'      =>$rowid,
                            'brandid'     =>'',  
                            'catid'       =>'',
                            'mapid'       =>$mapid, 
                            'colorname'   =>'',  
                            'agename'     =>'',
                            'manufacture' =>'',   
                            'pname'       =>'',  
                            'anumber '    =>'',
                             'psku '       =>'',
                            'barcode '    =>'',  
                            'pdes'        =>'', 
                            'qty'         =>'',
                            'pprice'      =>'', 
                            'sprice'      =>'', 
                            'pmrp'        =>'',  
                            'cg2p'        =>'',
                            'cg1p'        =>'',
                            'cg3p'        =>'', 
                            'pmargin'     =>'',
                            'discount'    =>'',
                            'range1'      =>'',  
                            'range2'      =>'', 
                            'range3'      =>'',  
                            'price1'      =>'', 
                            'price2'      =>'',  
                            'price3'      =>'',  
                            'hight'       =>'', 
                            'munit1'      =>'',  
                            'breadth'     =>'', 
                            'munit2'      =>'',  
                            'p_length'      =>'',
                            'munit3'      =>'',  
                            'weight'      =>'', 
                            'munit4'      =>'',  
                            'volweight'   =>'', 
                            'pwarranty'   =>'',  
                            'metatag1'    =>'', 
                            'metatag2'    =>'',  
                            'metatag3'    =>'',
                            'mainimg'     =>'',  
                            'img1'        =>'', 
                            'img2'        =>'',  
                            'img3'        =>'', 
                            'img4'        =>'',
                            'featurename' =>''
                                             
                            );

 $result['image'] = $this->supper_admin->call_procedure('proc_singleproduct', $parameter_image);

 $parameter_att = array( 'act_mode'    =>'editattribute',  
                            'row_id'      =>$rowid,
                            'brandid'     =>'',  
                            'catid'       =>'',
                             'mapid'       =>$mapid, 
                            'colorname'   =>'',  
                            'agename'     =>'',
                            'manufacture' =>'',   
                            'pname'       =>'',  
                            'anumber '    =>'',
                             'psku '       =>'',
                            'barcode '    =>'',  
                            'pdes'        =>'', 
                            'qty'         =>'',
                            'pprice'      =>'', 
                            'sprice'      =>'', 
                            'pmrp'        =>'',  
                            'cg2p'        =>'',
                            'cg1p'        =>'',
                            'cg3p'        =>'', 
                            'pmargin'     =>'',
                            'discount'    =>'',
                            'range1'      =>'',  
                            'range2'      =>'', 
                            'range3'      =>'',  
                            'price1'      =>'', 
                            'price2'      =>'',  
                            'price3'      =>'',  
                            'hight'       =>'', 
                            'munit1'      =>'',  
                            'breadth'     =>'', 
                            'munit2'      =>'',  
                            'p_length'      =>'',
                            'munit3'      =>'',  
                            'weight'      =>'', 
                            'munit4'      =>'',  
                            'volweight'   =>'', 
                            'pwarranty'   =>'',  
                            'metatag1'    =>'', 
                            'metatag2'    =>'',  
                            'metatag3'    =>'',
                            'mainimg'     =>'',  
                            'img1'        =>'', 
                            'img2'        =>'',  
                            'img3'        =>'', 
                            'img4'        =>'',
                            'featurename' =>''
                            );

 $result['attr'] = $this->supper_admin->call_procedure('proc_singleproduct', $parameter_att);


 $parameter_feature = array( 'act_mode'    =>'editfeatures',  
                            'row_id'      =>$rowid,
                            'brandid'     =>'',  
                            'catid'       =>'',
                            'mapid'       =>$mapid, 
                            'colorname'   =>'',  
                            'agename'     =>'',
                            'manufacture' =>'',   
                            'pname'       =>'',  
                            'anumber '    =>'',
                             'psku '       =>'',
                            'barcode '    =>'',  
                            'pdes'        =>'', 
                            'qty'         =>'',
                            'pprice'      =>'', 
                            'sprice'      =>'', 
                            'pmrp'        =>'',  
                            'cg2p'        =>'',
                            'cg1p'        =>'',
                            'cg3p'        =>'', 
                            'pmargin'     =>'',
                            'discount'    =>'',
                            'range1'      =>'',  
                            'range2'      =>'', 
                            'range3'      =>'',  
                            'price1'      =>'', 
                            'price2'      =>'',  
                            'price3'      =>'',  
                            'hight'       =>'', 
                            'munit1'      =>'',  
                            'breadth'     =>'', 
                            'munit2'      =>'',  
                            'p_length'    =>'',
                            'munit3'      =>'',  
                            'weight'      =>'', 
                            'munit4'      =>'',  
                            'volweight'   =>'', 
                            'pwarranty'   =>'',  
                            'metatag1'    =>'', 
                            'metatag2'    =>'',  
                            'metatag3'    =>'',
                            'mainimg'     =>'',  
                            'img1'        =>'', 
                            'img2'        =>'',  
                            'img3'        =>'', 
                            'img4'        =>'',
                            'featurename' =>''
                            );

 $result['feature'] = $this->supper_admin->call_procedure('proc_singleproduct', $parameter_feature);

 $parameter               = array('act_mode'=>'catatt','row_id'=>'','catid'=>$result['data'][0]->CatId);
 $response['vieww']       = $this->supper_admin->call_procedure('proc_product',$parameter);

    $colorid                 = $response['vieww'][0]->id;
  $parameter1              = array('act_mode'=>'attvalue','row_id'=>$colorid,'catid'=>'');
  $result['viewcolor']   = $this->supper_admin->call_procedure('proc_product',$parameter1);
  $parameter2              = array('act_mode'=>'attvalue','row_id'=>2,'catid'=>'');
  $result['viewage']     = $this->supper_admin->call_procedure('proc_product',$parameter2);
  
  $parameter3              = array('act_mode'=>'feature','row_id'=>'','catid'=>$result['data'][0]->CatId);
  $result['viewfeature'] = $this->supper_admin->call_procedure('proc_product',$parameter3);
  foreach ($result['viewfeature'] as $key => $value) {
    $leftfeaid=$value->FeatureLeftID;
    $parameter4 = array('act_mode'=>'getleftfromright','row_id'=>$leftfeaid,'feaid'=>'','feavalue'=>'','catid'=>'');
    $result['viewrightfeature'][] =$this->supper_admin->call_procedure('proc_feature',$parameter4);
  }

  // p($result['data']);
  // exit();
    $this->load->view('helper/header');
    $this->load->view('product/editproduct',$result);
  
}







/////// ------------- End Edit product Function  ------------ ////////

public function approve_status (){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='A'?'activepro':'inactivepro';
  $userid = $this->session->userdata('bizzadmin')->LoginID;
  $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'catid'=>$userid);
  $response      = $this->supper_admin->call_procedure('proc_product', $parameter );
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect("admin/product/approveproduct");

}
/////// -------------  End Function  ------------ ////////

public function img_resixe(){
//p(FCPATH."images/hoverimg/");exit;
 $userdir = $this->session->userdata('bizzadmin')->LoginID;
 FCPATH.'uploads/'.$userdir;
 if($userdir==''){
    print '<br>No vendor available.';
    exit;
 }
  else{
    chmod(FCPATH.'uploads/', 0777);
    mkdir(FCPATH.'uploads/'.$userdir);
    chmod(FCPATH.'uploads/'.$userdir, 0777);
  }
 // exit;

  $field_name = 'my_file';     
  //$zipdata  = $this->img_resixe($field_name);

  $filedir                 = FCPATH.'uploads/'.$userdir.'/';
  $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
  $config['allowed_types'] = 'zip|rar';
  $config['max_size']      = ' 5000000';
  $config['max_width']     = '102400';
  $config['max_height']    = '768000';
  //$this->upload->initialize($config);
  $this->load->library('upload', $config);
  if ( ! $this->upload->do_upload($field_name)){
    #print $error = array('error' => $this->upload->display_errors());
    print $error = $this->upload->display_errors();
  }
  else{
    $data   = array('upload_data' => $this->upload->data());
    $zip    = new ZipArchive();
    if(!$zip) die('<br>Unable to Load Zip Archive.');

    $file   = $data['upload_data']['full_path'];
    $nofile = $data['upload_data']['file_name'];
    chmod($file,0777);
    $arc    = $zip->open($file);
   
    if(!$arc) die('<br>Unable to Open Zip Archive.');
    
    if ($zip->open($file) === TRUE) {
         $zip->extractTo(FCPATH.'uploads/'.$userdir);
         $zip->close();
         
         $readdir = scandir($filedir,0);
         if(!$readdir) die('<br>Unable to read image directory.');

         $fileArr = [];
         for($i=2;$i<count($readdir); $i++){
          if($readdir[$i] !='.' && $readdir[$i] !='..' && $readdir[$i] !='Thumbs.db' && $readdir[$i] !=$nofile){
                $fileArr[] = $readdir[$i];
            ////////////////////////////////////////////////
$mainext = pathinfo($readdir[$i], PATHINFO_EXTENSION);
                $newwidth  = 450;
                list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$readdir[$i]);
$checkupfile=getimagesize(FCPATH.'uploads/'.$userdir.'/'.$readdir[$i]);
                // p(FCPATH.'uploads/'.$userdir.'/'.$readdir[$i]); exit;
                $ratio_orig = $width_orig/$height_orig;
                $height     = $newwidth/$ratio_orig;
                
                $newheight = $height;
                $actulan   = FCPATH."images/hoverimg/".$readdir[$i];
                  //p($actulan); exit;
                $pt        = FCPATH."uploads/".$userdir.'/'.$readdir[$i];
                chmod($pt,0777);
                //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                if($mainext=="jpg" || $mainext=="jpeg")
                {
                   if($checkupfile["mime"]=='image/png'){
                    compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
                  } else{
                    compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                  }
                 //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                 //compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
                } 
                else if($mainext=="png")
                {
                   if($checkupfile["mime"]=='image/png'){
                    compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
                  } else{
                    compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
                  }
                  //compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
                  
                } 
                else
                {
                  compressImageany(".".$mainext,$pt,$path,$actulan,$newwidth,$newheight);
                  
                }

                // --------------------------------
                $newwidth1        = 300;
                list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$readdir[$i]);
                 
                $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
                $height_thumb     = $newwidth1/$ratio_orig_thumb;
                
                $newheight1       = $height_thumb;

                $actulan1 = FCPATH."images/thumimg/".$readdir[$i];
                $pt1      = FCPATH."uploads/".$userdir.'/'.$readdir[$i];
                chmod($pt1,0777);
                //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                if($mainext=="jpg" || $mainext=="jpeg")
                {
                  if($checkupfile["mime"]=='image/png'){
                    compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                  } else{
                    compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                  }

                  //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                  //compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                } 
                elseif($mainext=="png")
                {
                  if($checkupfile["mime"]=='image/png'){
                    compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                  } else{
                    compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                  }
                  //compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
                   
                } 
                else
                {
                  compressImageany(".".$mainext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
                }

                //---------------------------------------
                #print './uploads/'.$userdir."/".$readdir[$i];
                #print '<br>';
                #print "./images/zoomimg/".$readdir[$i];
                @rename(FCPATH.'uploads/'.$userdir."/".$readdir[$i], FCPATH."images/zoomimg/".$readdir[$i]);                  
            ////////////////////////////////////////////////
          }// if loop
         }//for loop
         
          $directory = FCPATH."uploads/".$userdir;
          recursiveRemoveDirectory($directory);
          print "Image successfully Uploaded";
      }else{
          echo 'Failed to open zip archive.';
    }
  }
  
}
/////// -------------  End Function  ------------ ////////

public function addproduct(){
   //pend($_POST);
    if(!empty($_POST))
    {  
   //pend($_POST); 
           $main_image=$img1=$img2=$img3=$img4='';
          $userdir = $this->session->userdata('bizzadmin')->LoginID;
          FCPATH.'uploads/'.$userdir;
          if($userdir==''){
          print '<br>No vendor available.';
          exit;
          }
          else{
          chmod(FCPATH.'uploads/', 0777);
          mkdir(FCPATH.'uploads/'.$userdir);
          chmod(FCPATH.'uploads/'.$userdir, 0777);
          }

        if(!empty($_FILES['mainimg']['name']))
        {

          $main=$_FILES['mainimg']['name'];
          $mainext = pathinfo($main, PATHINFO_EXTENSION);
          $filedir                 = FCPATH.'uploads/'.$userdir.'/';
          $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['max_size'] = '2048';
          $config['encrypt_name']= TRUE;
          $config['remove_spaces']= TRUE;
          $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('mainimg')){
            print $error = $this->upload->display_errors();exit();
        }else{
            $data   = array('upload_data' => $this->upload->data());
            $file   = $data['upload_data']['full_path'];
            $nofile = $data['upload_data']['file_name'];
            $main=$main_image=$data['upload_data']['file_name'];
            chmod($file,0777);
            $readdir = scandir($filedir,0);
            $newwidth  = 450;
            list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig = $width_orig/$height_orig;
            $height     = $newwidth/$ratio_orig;
            
            $newheight = $height;
            $actulan   = FCPATH."images/hoverimg/".$main;
              
            $pt = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt,0777);
            //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
             compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            elseif($mainext=="png")
            {
             compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            else
            {
              compressImageany(".".$mainext,$pt,$path,$actulan,$newwidth,$newheight);
            }

            // --------------------------------
            $newwidth1        = 300;
            list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
            $height_thumb     = $newwidth1/$ratio_orig_thumb;
            
            $newheight1       = $height_thumb;

            $actulan1 = FCPATH."images/thumimg/".$main;
            $pt1      = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt1,0777);
            //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
              compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            } 
            elseif($mainext=="png")
            {
              compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }
            else
            {
              compressImageany(".".$mainext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }  
             //---------------------------------------
            #print './uploads/'.$userdir."/".$readdir[$i];
            #print '<br>';
            #print "./images/zoomimg/".$readdir[$i];
            @rename(FCPATH.'uploads/'.$userdir."/".$main, FCPATH."images/zoomimg/".$main); 

        }

        }

        if(!empty($_FILES['img1']['name']))
        {

          $main=$_FILES['img1']['name'];
          $mainext = pathinfo($main, PATHINFO_EXTENSION);
          $filedir                 = FCPATH.'uploads/'.$userdir.'/';
          $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['max_size'] = '2048';
          $config['encrypt_name']= TRUE;
          $config['remove_spaces']= TRUE;
          $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('img1')){
            print $error = $this->upload->display_errors();exit();
        }else{
            $data   = array('upload_data' => $this->upload->data());
            $file   = $data['upload_data']['full_path'];
            $nofile = $data['upload_data']['file_name'];
            $main=$img1=$data['upload_data']['file_name'];
            chmod($file,0777);
            $readdir = scandir($filedir,0);
            $newwidth  = 450;
            list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig = $width_orig/$height_orig;
            $height     = $newwidth/$ratio_orig;
            
            $newheight = $height;
            $actulan   = FCPATH."images/hoverimg/".$main;
              
            $pt = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt,0777);
            //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
             compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            elseif($mainext=="png")
            {
             compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            else
            {
              compressImageany(".".$mainext,$pt,$path,$actulan,$newwidth,$newheight);
            }

            // --------------------------------
            $newwidth1        = 300;
            list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
            $height_thumb     = $newwidth1/$ratio_orig_thumb;
            
            $newheight1       = $height_thumb;

            $actulan1 = FCPATH."images/thumimg/".$main;
            $pt1      = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt1,0777);
            //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
              compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            } 
            elseif($mainext=="png")
            {
              compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }
            else
            {
              compressImageany(".".$mainext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }  
             //---------------------------------------
            #print './uploads/'.$userdir."/".$readdir[$i];
            #print '<br>';
            #print "./images/zoomimg/".$readdir[$i];
            @rename(FCPATH.'uploads/'.$userdir."/".$main, FCPATH."images/zoomimg/".$main); 

        }

        }

        if(!empty($_FILES['img2']['name']))
        {

          $main=$_FILES['img2']['name'];
          $mainext = pathinfo($main, PATHINFO_EXTENSION);
          $filedir                 = FCPATH.'uploads/'.$userdir.'/';
          $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['max_size'] = '2048';
          $config['encrypt_name']= TRUE;
          $config['remove_spaces']= TRUE;
          $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('img2')){
            print $error = $this->upload->display_errors();exit();
        }else{
            $data   = array('upload_data' => $this->upload->data());
            $file   = $data['upload_data']['full_path'];
            $nofile = $data['upload_data']['file_name'];
            $main=$img2=$data['upload_data']['file_name'];
            chmod($file,0777);
            $readdir = scandir($filedir,0);
            $newwidth  = 450;
            list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig = $width_orig/$height_orig;
            $height     = $newwidth/$ratio_orig;
            
            $newheight = $height;
            $actulan   = FCPATH."images/hoverimg/".$main;
              
            $pt = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt,0777);
            //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
             compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            elseif($mainext=="png")
            {
             compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            else
            {
              compressImageany(".".$mainext,$pt,$path,$actulan,$newwidth,$newheight);
            }

            // --------------------------------
            $newwidth1        = 300;
            list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
            $height_thumb     = $newwidth1/$ratio_orig_thumb;
            
            $newheight1       = $height_thumb;

            $actulan1 = FCPATH."images/thumimg/".$main;
            $pt1      = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt1,0777);
            //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
              compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            } 
            elseif($mainext=="png")
            {
              compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }
            else
            {
              compressImageany(".".$mainext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }  
             //---------------------------------------
            #print './uploads/'.$userdir."/".$readdir[$i];
            #print '<br>';
            #print "./images/zoomimg/".$readdir[$i];
            @rename(FCPATH.'uploads/'.$userdir."/".$main, FCPATH."images/zoomimg/".$main); 

        }

        }

        if(!empty($_FILES['img3']['name']))
        {

          $main=$_FILES['img3']['name'];
          $mainext = pathinfo($main, PATHINFO_EXTENSION);
          $filedir                 = FCPATH.'uploads/'.$userdir.'/';
          $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['max_size'] = '2048';
          $config['encrypt_name']= TRUE;
          $config['remove_spaces']= TRUE;
          $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('img3')){
            print $error = $this->upload->display_errors();exit();
        }else{
            $data   = array('upload_data' => $this->upload->data());
            $file   = $data['upload_data']['full_path'];
            $nofile = $data['upload_data']['file_name'];
            $main=$img3=$data['upload_data']['file_name'];
            chmod($file,0777);
            $readdir = scandir($filedir,0);
            $newwidth  = 450;
            list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig = $width_orig/$height_orig;
            $height     = $newwidth/$ratio_orig;
            
            $newheight = $height;
            $actulan   = FCPATH."images/hoverimg/".$main;
              
            $pt = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt,0777);
            //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
             compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            elseif($mainext=="png")
            {
             compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            else
            {
              compressImageany(".".$mainext,$pt,$path,$actulan,$newwidth,$newheight);
            }

            // --------------------------------
            $newwidth1        = 300;
            list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
            $height_thumb     = $newwidth1/$ratio_orig_thumb;
            
            $newheight1       = $height_thumb;

            $actulan1 = FCPATH."images/thumimg/".$main;
            $pt1      = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt1,0777);
            //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
              compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            } 
            elseif($mainext=="png")
            {
              compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }
            else
            {
              compressImageany(".".$mainext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }  
             //---------------------------------------
            #print './uploads/'.$userdir."/".$readdir[$i];
            #print '<br>';
            #print "./images/zoomimg/".$readdir[$i];
            @rename(FCPATH.'uploads/'.$userdir."/".$main, FCPATH."images/zoomimg/".$main); 

        }

        }

        if(!empty($_FILES['img4']['name']))
        {

          $main=$_FILES['img4']['name'];
          $mainext = pathinfo($main, PATHINFO_EXTENSION);
          $filedir                 = FCPATH.'uploads/'.$userdir.'/';
          $config['upload_path']   = FCPATH.'uploads/'.$userdir.'/';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['max_size'] = '2048';
          $config['encrypt_name']= TRUE;
          $config['remove_spaces']= TRUE;
          $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('img4')){
            print $error = $this->upload->display_errors();exit();
        }else{
            $data   = array('upload_data' => $this->upload->data());
            $file   = $data['upload_data']['full_path'];
            $nofile = $data['upload_data']['file_name'];
            $main=$img4=$data['upload_data']['file_name'];
            chmod($file,0777);
            $readdir = scandir($filedir,0);
            $newwidth  = 450;
            list($width_orig, $height_orig) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig = $width_orig/$height_orig;
            $height     = $newwidth/$ratio_orig;
            
            $newheight = $height;
            $actulan   = FCPATH."images/hoverimg/".$main;
              
            $pt = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt,0777);
            //compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
             compressImage(".jpeg",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            elseif($mainext=="png")
            {
             compressImagepng(".png",$pt,$path,$actulan,$newwidth,$newheight);
            } 
            else
            {
              compressImageany(".".$mainext,$pt,$path,$actulan,$newwidth,$newheight);
            }

            // --------------------------------
            $newwidth1        = 300;
            list($width_orig_thumb, $height_orig_thumb) = getimagesize(FCPATH.'uploads/'.$userdir.'/'.$main);
             
            $ratio_orig_thumb = $width_orig_thumb/$height_orig_thumb;
            $height_thumb     = $newwidth1/$ratio_orig_thumb;
            
            $newheight1       = $height_thumb;

            $actulan1 = FCPATH."images/thumimg/".$main;
            $pt1      = FCPATH."uploads/".$userdir.'/'.$main;
            chmod($pt1,0777);
            //compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            if($mainext=="jpg" || $mainext=="jpeg")
            {
              compressImage(".jpeg",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            } 
            elseif($mainext=="png")
            {
              compressImagepng(".png",$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }
            else
            {
              compressImageany(".".$mainext,$pt1,$path,$actulan1,$newwidth1,$newheight1);
            }  
             //---------------------------------------
            #print './uploads/'.$userdir."/".$readdir[$i];
            #print '<br>';
            #print "./images/zoomimg/".$readdir[$i];
            @rename(FCPATH.'uploads/'.$userdir."/".$main, FCPATH."images/zoomimg/".$main); 

        }

        }
  /******************insert images end******************/     
        $catname=array(
            'act_mode'=>'get_categorynamebyid',
            'row_id'=>$this->input->post('catid'),
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>''
          );
         $response['cat'] = $this->supper_admin->call_procedureRow('proc_five', $catname);
         $categoryname= $response['cat']->catname;


        
         $product_param=array(
            'act_mode'=>'add_productmanufacture1',
            'row_id'=>'',
            'Param3'=>$this->input->post('pname'),
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>$this->input->post('catid'), 
            'Param7'=>'',
            'Param8'=>$this->input->post('psku'), 
            'Param9'=>$this->input->post('barcode'),
            'Param10'=>'',//$this->input->post('hight'),
            'Param11'=>'',//$this->input->post('munit1'),
            'Param12'=>'',//$this->input->post('breadth'),
            'Param13'=>'',//$this->input->post('munit2'),
            'Param14'=>'',//$this->input->post('length'),
            'Param15'=>'',//$this->input->post('munit3'),
            'Param16'=>'',//$this->input->post('volweight'),
            'Param17'=>'',//$this->input->post('weight'), 
            'Param18'=>'',//$this->input->post('munit4'),
            'Param19'=>'',//$this->input->post('meta_ttl'),
            'Param20'=>'',//$this->input->post('meta_key'),
            'Param21'=>'',//$this->input->post('meta_disc'),
            'Param22'=>$this->input->post('pwarranty'),
            'Param23'=>$this->input->post('stock'),
            'Param24'=>$this->input->post('pprice'),
            'Param25'=>$this->input->post('pmrp'),
            'Param26'=>$this->input->post('sprice'), 
            'Param27'=>'',//$this->input->post('pmargin'),
            'Param28'=>$this->input->post('pdes'),
            'Param29'=>$this->input->post('anumber'),
            'Param30'=>'',//$this->input->post('rangefrom1'), 
            'Param31'=>'',//$this->input->post('rangefrom2'),
            'Param32'=>'',//$this->input->post('rangefrom3'),
            'Param33'=>'',//$this->input->post('price1'), 
            'Param34'=>'',//$this->input->post('price2'), 
            'Param35'=>'',//$this->input->post('price3'),
            'Param36'=>$this->input->post('colorname'),  
            'Param37'=>$this->input->post('agename'),
            'Param38'=>'',//$_POST['leftfeaturename'],
            'Param39'=>'',//$_POST['rightfeaturename'],
            'Param40'=>'',//$this->input->post('rangeto1'),
            'Param41'=>'',//$this->input->post('rangeto2'),
            'Param42'=>'',//$this->input->post('rangeto3'),
            'Param43'=>'',//$userid,
            'Param44'=>$this->input->post('catid'),
            'Param45'=>$categoryname,
            'Param46'=>'',//$this->input->post('storeid'),
            'Param47'=>'',//$fid,
            'Param48'=>'',
            'Param49'=>'',
            'Param50'=>'',
            'Param51'=>'',
            'Param52'=>'',
            'Param53'=>'',
            'Param54'=>'',
            'Param55'=>''
           
          );
        
          $response['saveproduct'] = $this->supper_admin->call_procedureRow('proc_addbulkdata', $product_param);  


         /*****************************saveimages*************/
          $images=array(
            'act_mode'=>'save_images',
            'row_id'=>'',
            'Param3'=>$response['saveproduct']->prodid,
            'Param4'=>$main_image,
            'Param5'=>$img1,
            'Param6'=>$img2,  
            'Param7'=>$img3, 
            'Param8'=>$img4 ,
            'Param9'=>'',
            'Param10'=>''
           
          );
         // pend($images);
         $response['saveattributor'] = $this->supper_admin->call_procedureRow('proc_ten', $images); 



/*******************insert product Features ********************/

$leftfeature=$this->input->post('leftattribute');
$rightfeature=$this->input->post('rightattribute');


if(!empty($leftfeature))
{
foreach ($leftfeature as $key => $value) {
   $insert_feature   =array(
    'act_mode'=>'insert_product_attributesadminnew',
    'row_id'     =>'',
    'Param3'     =>$response['saveproduct']->prodid,
    'Param4'     =>$key,
    'Param5'     =>$value,
    'Param6'     =>'',
    'Param7'     =>$value,
    'Param8'     =>'',
    'Param9'     =>'',
    'Param10'     =>''
    
    );
   //pend($insert_feature);
   $add_features= $this->supper_admin->call_procedure('proc_ten',$insert_feature);

//p( $insert_feature);
  
}
}


 $pro=array(
            'act_mode'=>'get_product_sku',
            'row_id'=>'',
            'Param3'=>$response['saveproduct']->prodid,
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',  
            'Param7'=>'', 
            'Param8'=>'' ,
            'Param9'=>'',
            'Param10'=>''
           
          );
         // pend($pro);
         $response['prosku'] = $this->supper_admin->call_procedureRow('proc_ten', $pro); 
        // pend(  $response['prosku']);


 $procatid   = $response['prosku']->skuno;
   // pend($procatid);
    
   

     $parameter        = array('psku'=>str_replace(';', '', $procatid));
      //pend($parameter);
      $data['proviewr'] = $this->supper_admin->call_procedure('foo', $parameter);
//pend($_POST); 
  // foreach ($view_feature as $key => $valfeature) {
  //   $insert_feature   =array(
  //   'act_mode'=>'insert_product_attributesadmin',
  //   'row_id'     =>'',
  //   'Param3'     =>$response['saveproduct']->prodid,
  //   'Param4'     =>$valfeature->featuremap_id,
  //   'Param5'     =>$this->input->post('featureid_'.$valfeature->featuremap_id),
  //   'Param6'     =>$userid,
  //   'Param7'     =>$valfeature->LeftValue,
  //   'Param8'     =>'',
  //   'Param9'     =>'',
  //   'Param10'     =>''
    
  //   );
  //  $add_features = $this->supper_admin->call_procedureRow('proc_ten',$insert_feature);
  // }

/*******************insert product Features end ********************/  
 
   $this->session->set_flashdata("message", "Your information was successfully Saved.");
 redirect("admin/product/approveproduct");

    }


     

    
    $parameter         = array('act_mode'=>'view','row_id'=>'','brandname'=>'','brandimage'=>'','');
    $response['vieww'] = $this->supper_admin->call_procedure('proc_brand',$parameter);
    $parameter         = array('act_mode'=>'get_manufature','','');
    $response['code'] = $this->supper_admin->call_procedure('proc_product',$parameter);
     $parameter         = array('act_mode'=>'get_main_cat','row_id'=>$barid,'catid'=>'');
  $response['maincat'] = $this->supper_admin->call_procedure('proc_product',$parameter);

 

   
    $this->load->view('helper/header');
    $this->load->view('product/addproduct',$response);
}
/////// -------------  End Function  ------------ ////////

public function brandcategory(){
  $barid             = $this->input->post('barid');
  $parameter         = array('act_mode'=>'get_main_cat','row_id'=>$barid,'catid'=>'');
  $response['vieww'] = $this->supper_admin->call_procedure('proc_product',$parameter);
  
  $str               = '';
  foreach($response['vieww'] as $k=>$v){
      $str .= "<option value=".$v->catid.">".$v->catname."</option>";
  }
  echo $str;
 } 
 

/////// -------------  End Function  ------------ ////////
public function selectatt()
{
  $catid  = $this->input->post('catid');
  $parameter               = array('act_mode'=>'catatt','row_id'=>'','catid'=>$catid);
  $response['vieww']       = $this->supper_admin->call_procedure('proc_product',$parameter);
  $att=json_encode(  $response['vieww']  );
  echo $att;

}



 public function selectattribute(){
  $catid                   = $this->input->post('catid');
  $parameter               = array('act_mode'=>'catatt','row_id'=>'','catid'=>$catid);
  $response['vieww']       = $this->supper_admin->call_procedure('proc_product',$parameter);
  $colorid                 = $response['vieww'][0]->id;
  $parameter1              = array('act_mode'=>'attvalue','row_id'=>$colorid,'catid'=>'');
  $colors                  = $this->supper_admin->call_procedure('proc_product',$parameter1);
  $parameter2              = array('act_mode'=>'attvalue','row_id'=>2,'catid'=>'');
  $ages                    = $this->supper_admin->call_procedure('proc_product',$parameter2);
  $parameter3              = array('act_mode'=>'feature_typ','row_id'=>'','catid'=>$catid);
  $feature                 = $this->supper_admin->call_procedure('proc_product',$parameter3);


  $feat=$color=$age="";
  if(!empty($response['vieww']))
  {
    if(!empty($colors))
    { 
      $color.='<option value="">Select </option>';
      foreach ($colors as $key => $value) {
      $color.='<option value="'.$value->attmapname.'">'.$value->attmapname.'</option>';
     }
    }

    if(!empty($colors))
    { 
      $age.='<option value="">Select </option>';
      foreach ($ages as $key => $value) {
      $age.='<option value="'.$value->attmapname.'">'.$value->attmapname.'</option>';
     }
    }

    if(!empty($feature))
    { 
      $feat_attr=['leftfeaturename','rightfeaturename'];
      
      $i=0;
      foreach ($feature as $key => $value) {
       $parameter4 = array('act_mode'=>'getleftfromright','row_id'=>$value->FeatureLeftID,'feaid'=>'','feavalue'=>'','catid'=>'');
                  $res =$this->supper_admin->call_procedure('proc_feature',$parameter4); 
                 // echo $res;die;
       
            $feat.="<div class='col-lg-6'>
                <div class='row'>
                <div class='col-lg-4'>
                <div class='tbl_text'>".$value->LeftValue."</div>
                </div><div class='col-lg-8' style='margin-bottom:2px;'>
                <div class='tbl_input' >
                  <span><input type='text' name='leftattribute[".$value->FeatureLeftID."]' id='leftattribute[".$value->FeatureLeftID."]' ></span>

                  </div>";
                  // foreach ($res as $key => $val) {
                  //   $feat.="<table ><tr><td><label value=".$val->RightValue.">".$val->RightValue."</label></td><td><input type='text' name='rightattribute[".$val->RightFeatureID."]' id='rightattribute[".$val->RightFeatureID."]' ></td></tr></table>";

                  // }

                //   <tr><td></td></tr></table>'
                //   '<select name="'.$feat_attr[$i].'" id="'.$feat_attr[$i++].'" >
                //    <option value="">Select feature</option>';
                // foreach ($res as $key => $value) {
                //   $feat.='<option value="'.$value->RightValue.'">'.$value->RightValue.'</option></select>';
                // }
                    
        $feat.='</span></div></div></div></div>';
        $i++;
      }
    }
    
  }
  echo $color.'**'.$age.'**'.$feat;
  }





  public function selectfeature()
  {
    $catid  = $this->input->post('catid');
  // echo $catid;die;
    $parameter3 = array('act_mode'=>'feature_typ','row_id'=>'','catid'=>$catid);
  $feature = $this->supper_admin->call_procedure('proc_product',$parameter3);
  //echo $parameter3;die;
 
                $feat=json_encode($feature);
                echo $feat;
  }


public function bulkproductupdate(){

  $parameterr  = array('act_mode'=>'parecatview','row_id'=>'','catparentid'=>'','cname'=>'','cdesc'=>'','caturl'=>'','catmeta'=>'','metadesc'=>'','metakeyword'=>'','catsort'=>'','catdisplay'=>'','catlevel'=>'');
  $responce['parentCatdata'] = $this->supper_admin->call_procedure('proc_category',$parameterr);
  $r_userid = $this->session->userdata('bizzadmin')->LoginID;
    if($this->input->post('submit'))
    {
      //echo $this->input->post('subcatidd');
      //p($_POST);exit;
      $catvalueid= $this->input->post('subcatidd');
      $fileName    = str_replace(' ', '', $_FILES['userfile']['tmp_name']);
      $objPHPExcel = PHPExcel_IOFactory::load($fileName);
      $rowsold     = $objPHPExcel->getActiveSheet()->toArray(); 
      $sheetName   = $objPHPExcel->getActiveSheet()->getTitle();

      $sheet_catid=explode('#', $sheetName);
      if($sheet_catid[1]==$catvalueid){
      $bxls=0;
      foreach ($rowsold as $key => $value) {
        if(!empty($value[1])){
          $bxls++;
        }
      }  
      //p($bxls);
      //exit; 
     // p($rowsold); 
      for($k=1;$k<$bxls;$k++){
       if(is_numeric($rowsold[$k][11])){
          $profitmarginupdate=$rowsold[$k][11];
       } else {
          $profitmarginupdate=0;
       }

       if(is_numeric($rowsold[$k][12])){
          $discountupdate=$rowsold[$k][12];
       } else {
          $discountupdate=0;
       }
  
        $parameter2 = array('act_mode'    =>'productupdate_catalog',  
                            'p_brandId'     =>$catvalueid,  
                            'p_proid'       =>$rowsold[$k][0],  
                            'p_promapid'       =>$rowsold[$k][1],
                            'p_cityid' =>$r_userid,  
                            'p_proname'    =>$rowsold[$k][2],
                            'p_prosku' => '',
                            'p_barcode'    =>'',
                            'p_description'   =>$rowsold[$k][3], 
                            'p_proqty'       =>$rowsold[$k][4],  
                            'p_prodpurchaseprice'   =>$rowsold[$k][5], 
                            'p_prodmrp'     =>$rowsold[$k][6],  
                            'p_prodsellingprice'  =>$rowsold[$k][7],
                            'p_finalprice'=>$rowsold[$k][7],  
                            'p_cityvalue'      =>'',                             
                            'p_profitmargin'      =>$profitmarginupdate,
                            'p_discount'      =>$discountupdate,  
                            'p_qty1'      =>str_replace(" ", "", $rowsold[$k][13]), 
                            'p_qty1_to'      =>str_replace(" ", "", $rowsold[$k][14]),
                            'p_qty2'      =>str_replace(" ", "", $rowsold[$k][16]),
                            'p_qty2_to'      =>str_replace(" ", "", $rowsold[$k][17]),  
                            'p_qty3'   =>str_replace(" ", "", $rowsold[$k][19]), 
                            'p_qty3_to'      =>str_replace(" ", "", $rowsold[$k][20]),
                            'p_price'   =>$rowsold[$k][15],  
                            'p_price2'    =>$rowsold[$k][18], 
                            'p_price3'    =>$rowsold[$k][21],
                            'Param27'    =>'',
                            'Param28'    =>'',
                            'Param29'    =>'',
                            'Param30'    =>'',
                            'Param31'    =>'',
                            'Param32'    =>'',
                            'Param33'    =>'',
                            'Param34'    =>'',
                            'Param35'    =>'',
                            'Param36'    =>'',
                            'Param37'    =>'',
                            'Param38'    =>'',
                            'Param39'    =>'',
                            'Param40'    =>'',
                            'Param41'    =>'',
                            'Param42'    =>'',
                            'Param43'    =>'',
                            'Param44'    =>'',
                            'Param45'    =>''   
                          );
                            /*'Param27'    =>$rowsold[$k][22],
                            'Param28'    =>$rowsold[$k][23],
                            'Param29'    =>$rowsold[$k][24],
                            'Param30'    =>$rowsold[$k][25],
                            'Param31'    =>$rowsold[$k][26],
                            'Param32'    =>$rowsold[$k][27],
                            'Param33'    =>$rowsold[$k][28],
                            'Param34'    =>$rowsold[$k][34],
                            'Param35'    =>$rowsold[$k][35],
                            'Param36'    =>$rowsold[$k][36],
                            'Param37'    =>$rowsold[$k][37],
                            'Param38'    =>$rowsold[$k][38],
                            'Param39'    =>$rowsold[$k][39],
                            'Param40'    =>$rowsold[$k][40],
                            'Param41'    =>$rowsold[$k][41],
                            'Param42'    =>$rowsold[$k][42],
                            'Param43'    =>$rowsold[$k][43],
                            'Param44'    =>'',
                            'Param45'    =>''   
                      );*/

        $responce['vieww']   = $this->supper_admin->call_procedure('proc_hm_bulkuploadproductcatalog',$parameter2);


        //----------------------- start image update -----------------------------
          $parameterimg = array('act_mode'    =>'allproimageupdate_catelog',  
                                'p_brandId'     =>'',  
                                'p_proid'       =>$rowsold[$k][0],  
                                'p_promapid'       =>$rowsold[$k][1],
                                'p_cityid' =>'',  
                                'p_proname'    =>$rowsold[$k][22],
                                'p_prosku' => $rowsold[$k][23], 
                                'p_barcode'    =>$rowsold[$k][24],  
                                'p_description'   =>$rowsold[$k][25], 
                                'p_proqty'       =>'',  
                                'p_prodpurchaseprice'   =>'', 
                                'p_prodmrp'     =>'',  
                                'p_prodsellingprice'  =>'',
                                'p_finalprice'=>'',  
                                'p_cityvalue'      =>'',  
                                'p_profitmargin'      =>'',
                                'p_discount'      =>'',  
                                'p_qty1'      =>$rowsold[$k][26],
                                'p_qty1_to'   =>'',
                                'p_qty2'      =>'',  
                                'p_qty2_to'   =>'',
                                'p_qty3'   =>'',
                                'p_qty3_to'   =>'', 
                                'p_price'   =>'',  
                                'p_price2'    =>'', 
                                'p_price3'    =>'',
                                'Param27'    =>'',
                                'Param28'    =>'',
                                'Param29'    =>'',
                                'Param30'    =>'',
                                'Param31'    =>'',
                                'Param32'    =>'',
                                'Param33'    =>'',
                                'Param34'    =>'',
                                'Param35'    =>'',
                                'Param36'    =>'',
                                'Param37'    =>'',
                                'Param38'    =>'',
                                'Param39'    =>'',
                                'Param40'    =>'',
                                'Param41'    =>'',
                                'Param42'    =>'',
                                'Param43'    =>'',
                                'Param44'    =>'',
                                'Param45'    =>''   
                              );
          //p($parameterimg);exit;
        $responce['viewwimg']   = $this->supper_admin->call_procedure('proc_hm_bulkuploadproductcatalog',$parameterimg); 
        //----------------------- end image update -------------------------------
        $citydata=array(1 =>$rowsold[$k][8] , 2     =>$rowsold[$k][9], 3      =>$rowsold[$k][10]);

          foreach ($citydata as $key => $value) {
            $parameter3 = array( 'act_mode'    =>'cityupdate_catelog',  
                                  'p_brandId'     =>'',  
                                  'p_proid'       =>$rowsold[$k][0],  
                                  'p_promapid'       =>$rowsold[$k][1],
                                  'p_cityid' => $key,  
                                  'p_proname'    =>'',
                                  'p_prosku' => '', 
                                  'p_barcode'    =>'',  
                                  'p_description'   =>'', 
                                  'p_proqty'       =>'',  
                                  'p_prodpurchaseprice'   =>'', 
                                  'p_prodmrp'     =>'',  
                                  'p_prodsellingprice'  =>'', 
                                  'p_finalprice'=>'',  
                                  'p_cityvalue'      =>$value,  
                                  'p_profitmargin'      =>'',
                                  'p_discount'      =>'',  
                                  'p_qty1'     =>'',
                                  'p_qty1_to'  =>'',
                                  'p_qty2'     =>'',  
                                  'p_qty2_to'  =>'',
                                  'p_qty3'     =>'',
                                  'p_qty3_to'  =>'', 
                                  'p_price'    =>'',  
                                  'p_price2'   =>'', 
                                  'p_price3'   =>'',
                                  'Param27'    =>'',
                                  'Param28'    =>'',
                                  'Param29'    =>'',
                                  'Param30'    =>'',
                                  'Param31'    =>'',
                                  'Param32'    =>'',
                                  'Param33'    =>'',
                                  'Param34'    =>'',
                                  'Param35'    =>'',
                                  'Param36'    =>'',
                                  'Param37'    =>'',
                                  'Param38'    =>'',
                                  'Param39'    =>'',
                                  'Param40'    =>'',
                                  'Param41'    =>'',
                                  'Param42'    =>'',
                                  'Param43'    =>'',
                                  'Param44'    =>'',
                                  'Param45'    =>''   
                           );
            $responce['cityupdate'] = $this->supper_admin->call_procedure('proc_hm_bulkuploadproductcatalog',$parameter3);
          }
        $responce['updatesuccess'] = 'Product was successfully Updated';
      }
      //p($parameter2);exit();
      move_uploaded_file(str_replace(' ', '', $_FILES['userfile']['tmp_name']), FCPATH.'bulkproductexcels/'.time().'-'.$catvalueid.'-'.str_replace(' ', '', $_FILES['userfile']['name']));
    } else {
      $responce['updatefail'] = 'You have selected Wrong Category for uploading Product excel.';
    }

    }
  $this->load->view('helper/header');
  $this->load->view('product/updateproduct',$responce);
 } 


/////// -------------  End Function  ------------ ////////

function get_child_cat($catid,$brandid)
{
  $parameter = array('act_mode'=>'get_sub_cat','row_id'=>$brandid,'catid'=>$catid);
  $response = $this->supper_admin->call_procedure('proc_product',$parameter);
  $str = '';
  foreach($response as $k=>$v){
      $str .= "<option value=".$v->catid.">".$v->catname."</option>";
  }
  echo $str;
} 

public function reject_product(){
  //p($_POST);
  $userid = $this->session->userdata('bizzadmin')->LoginID;
  $reject_proid = $this->input->post('reject_proid');
  $reject_proreason = $this->input->post('reject_proreason');
  $parameter = array('act_mode'=>'reject_pro','row_id'=>$reject_proid,'brandname'=>'','brandimage'=>$reject_proreason,'catid'=>$userid);
  $reject_pro_view = $this->supper_admin->call_procedureRow('proc_brand',$parameter); 
  echo "success";
}

public function testpush(){
  
  $parameterapp = array('act_mode'=>'getallappdeviceid','row_id'=>'','catid'=>'');
  $appdeviceids  = $this->supper_admin->call_procedure('proc_product',$parameterapp);
  $appdeviceiddata = array();
  $appmsgdevice=array('message'=>'this is new products.','title'=>'New Arrival Products');
  $appnotification='1';
  foreach ($appdeviceids as $key => $value) {
    array_push($appdeviceiddata,$value->appdeviceid);
  }
  send_andriod_notification($appdeviceiddata,$appmsgdevice,$appnotification);
  /*p($appdeviceiddata); 
  p($appmsgdevice);
  p($appnotification);
  exit;*/
}

public function testorderdelpush(){
  
  $parameterapp = array('act_mode'=>'getappdelorderdeviceid','row_id'=>'81828','catid'=>'');
  $appdeviceids  = $this->supper_admin->call_procedureRow('proc_product',$parameterapp);
  $appdeviceiddata = array($appdeviceids->appdeviceid);
  $appmsgdevice=array('message'=>'Some Products are Remove in your existing order. OrderId is '.$appdeviceids->OrderNumber,'title'=>'Remove Products');
  $appnotification='2';
  //send_andriod_notification($appdeviceiddata,$appmsgdevice,$appnotification);
  p($appdeviceiddata); 
  p($appmsgdevice);
  p($appnotification);
  exit;
}

public function testorderaddpush(){
  
  $parameterapp = array('act_mode'=>'getappaddorderdeviceid','row_id'=>'1086','catid'=>'');
  $appdeviceids  = $this->supper_admin->call_procedureRow('proc_product',$parameterapp);
  $appdeviceiddata = array($appdeviceids->appdeviceid);
  $appmsgdevice=array('message'=>'Some Products are Added in your existing order. OrderId is '.$appdeviceids->OrderNumber,'title'=>'Add Products');
  $appnotification='2';
  //send_andriod_notification($appdeviceiddata,$appmsgdevice,$appnotification);
  p($appdeviceiddata); 
  p($appmsgdevice);
  p($appnotification);
  exit;
}

  public function deleteoverview(){
    $this->load->view('overview/updatesection');
    // redirect('admin/overview/index');
    $del_id = $this->uri->segment('4');
    $updateid = $this->uri->segment('5');
    $paramm = array(
                'act_mode' =>'delete_top',
                'Param1' =>$del_id,
                'Param2' =>'',
                'Param3' =>'',
                'Param4' =>'',
                'Param5' =>'',
                'Param6' =>'',
                'Param7' =>'',
                'Param8' =>'',
                'Param9' =>'',
                'Param10' =>''
            
                );
           
  $record  = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
    $this->session->set_flashdata('message', 'overview image successfully deleted');
    redirect(base_url().'admin/overview/updatesection/'.$updateid);
 
  }

   public function deletebottomoverview(){
    $this->load->view('overview/updatesection');
    // redirect('admin/overview/index');
    $del_id = $this->uri->segment('4');
    $updateid = $this->uri->segment('5');
    $paramm = array(
                'act_mode' =>'delete_bottom',
                'Param1' =>$del_id,
                'Param2' =>'',
                'Param3' =>'',
                'Param4' =>'',
                'Param5' =>'',
                'Param6' =>'',
                'Param7' =>'',
                'Param8' =>'',
                'Param9' =>'',
                'Param10' =>''
            
                );
           
  $record  = $this->supper_admin->call_procedureRow('proc_tvoverviewslider',$paramm);
    $this->session->set_flashdata('message', 'bottom overview image successfully deleted');
    redirect(base_url().'admin/overview/updatesection/'.$updateid);
 
  }
}
?>