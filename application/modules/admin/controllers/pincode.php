<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Pincode extends MX_Controller{
  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }

//---------------------Pincode Section---------------------//
 public function listPincode()
 {

   $pinList=array(
     'act_mode'=>'list_pincode',
     'row_id'=>'',
     'Param3'=>'',
     'Param4'=>'',
     'Param5'=>''
    

    );
  //p($pincode); exit;
  $response['pincode_list1']  = $this->supper_admin->call_procedure('proc_five',$pinList);
//p($response['pincode_list']); exit;
    //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/pincode/listPincode?";
      $config['total_rows']       = count($response['pincode_list']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;
//p( $config['total_rows'] ); exit;
     $this->pagination->initialize($config);
     if($_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );
  $pinpage=array(
     'act_mode'=>'list_pincode_limit',
     'row_id'=>$page,
     'Param3'=>'',
     'Param4'=>'',
     'Param5'=>$second
    

    );
  //p($pinpage); exit;
   $response['pincode_list']  = $this->supper_admin->call_procedure('proc_five',$pinpage);
   //p($response['pincode_list']); exit;
    //----------------  end pagination ------------------------//  






  //p($pincode_list); exit;
  $this->load->view('helper/header');
  $this->load->view('pincode/listpincode_view',$response);
 }
 public function addPincode()
 {
  $userid = $this->session->userdata('bizzadmin')->LoginID;
  if($this->input->post('submit'))
  {
        

    $pincode=array(
     'act_mode'=>'add_pincode_check',
     'row_id'=>'',
     'Param3'=>$this->input->post('pincodename'),
     'Param4'=>'',
     'Param5'=>''
    

    );
  //p($pincode); exit;
  $response['checking']  = $this->supper_admin->call_procedureRow('proc_five',$pincode);

  if($response['checking']->pincheck >0)
  {
     //echo "hii";exit;
     $this->session->set_flashdata('message', 'Pincode already exist!!.');
     redirect(base_url().'admin/pincode/addPincode');
    
  }
  else
  {
   // echo "hello";exit;
  $pincode=array(
     'act_mode'=>'add_pincode',
     'row_id'=>'',
     'Param3'=>$this->input->post('pincodename'),
     'Param4'=>'',
     'Param5'=>$userid
    

    );
  //p($pincode); exit;
   $response  = $this->supper_admin->call_procedure('proc_five',$pincode);
   $this->session->set_flashdata('message', 'Your information was successfully Saved.');
   redirect(base_url().'admin/pincode/listPincode');
  }
 
  //redirect('admin/listPincode');
}
  $this->load->view('helper/header');
  $this->load->view('pincode/addpincode_view');
 }


 public function pinupdate($id)
 {
  $userid = $this->session->userdata('bizzadmin')->LoginID;
  if($this->input->post('submit'))
  {


   $pinupdate=array(
     'act_mode'=>'update_pincode',
     'row_id'=>$id,
     'Param3'=>$this->input->post('pincodename'),
     'Param4'=>'',
     'Param5'=>$userid
    

  );
  //p($pinupdate); exit;
   $response  = $this->supper_admin->call_procedureRow('proc_five',$pinupdate);
   $this->session->set_flashdata('message', 'Your information was successfully Updated.');
   redirect(base_url().'admin/pincode/listPincode');
 }
 $getpincode = array(
     'act_mode'=>'get_pincode_row',
     'row_id'=>$id,
     'Param3'=>'',
     'Param4'=>'',
     'Param5'=>''
    

  );
  //p($getpincode); exit;
  $response['pinrow']  = $this->supper_admin->call_procedureRow('proc_five',$getpincode);
  $this->load->view('helper/header');
  $this->load->view('pincode/updatepincode_view',$response);
 }

 public function pincodedelete($id)
 {
     $userid = $this->session->userdata('bizzadmin')->LoginID;
     $getpincode = array(
     'act_mode'=>'delete_pincode_row',
     'row_id'=>$id,
     'Param3'=>'',
     'Param4'=>'',
     'Param5'=>$userid
    

  );
  //p($getpincode); exit;
  $response['pinrow']  = $this->supper_admin->call_procedureRow('proc_five',$getpincode);
  $this->session->set_flashdata('message', 'Your information was successfully Deleted.');
   redirect(base_url().'admin/pincode/listPincode');
 }

public function pincodestatus()
{
  $id = $this->uri->segment(4);
  $status = $this->uri->segment(5);
  $userid = $this->session->userdata('bizzadmin')->LoginID;
  $act_mode      = $status=='A'?'active_status':'inactive_status';
  $getpincode = array(
     'act_mode'=>$act_mode,
     'row_id'=>$id,
     'Param3'=>'',
     'Param4'=>'',
     'Param5'=>$userid
    

  );

  //p($getpincode); exit;
   $response['pinrow']  = $this->supper_admin->call_procedureRow('proc_five',$getpincode);
   $this->session->set_flashdata('message', 'Your status was successfully Updated.');
   redirect(base_url().'admin/pincode/listPincode');
}

//---------------------Pincode Section End ---------------------//

//---------------------Excel Upload Pincode---------------------//



public function addpincodeexcel(){
  if($this->input->post('download')){

      

       $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
    $objWorkSheet  = $objPHPExcel->createSheet();
    $objPHPExcel->setActiveSheetIndex(0);

    $finalExcelArr1 = array_merge($excelHeadArr1);
//p($finalAttrData2);exit;
     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        $j      = 2;
       
        for($i=0;$i<count($finalExcelArr1);$i++){
         $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
         foreach ($finalAttrData2 as $key => $value) {
          foreach ($value as $k => $v) {

            if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
      }  
    }
     $objPHPExcel->getSheetByName('Worksheet')->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
        $arrPart2 = array('Pincode');
          if(!empty($excelHeadArr)){
             $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
         } else {
            $finalExcelArr = array_merge($arrPart2);
         }
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#'.$cateid.'');
      
    $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
 
 //Set border style for active worksheet
 $styleArray = array(
      'borders' => array(
          'allborders' => array(
            'style'  => PHPExcel_Style_Border::BORDER_THIN
          )
      )
);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->applyFromArray($styleArray);

for($i=0;$i<count($finalExcelArr);$i++){
            
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );
            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

          for($k=2;$k <1000;$k++){
    
    //Set height for every single row.
    $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);

    
  
  }//secfor
}

         $filename  = 'pincode_excel_upload.xls';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        ob_end_clean();
        ob_start();
        $objWriter->save('php://output');
        exit;
          }

    //----------End Download Excel File ---------------------//

          // --------------------- Upload Excel condition ----------------------
  if($this->input->post('submit')){
   $fileName    = $_FILES['userfile']['tmp_name'];
   $userid = $this->session->userdata('bizzadmin')->LoginID;
   $objPHPExcel = PHPExcel_IOFactory::load($fileName);
   $maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
   $rowsold     = $objPHPExcel->getActiveSheet()->rangeToArray('A2:' . $maxCell['column'] . $maxCell['row']); 
   $rowsoldHeader     = $objPHPExcel->getActiveSheet()->toArray(); 
   $sheetName   = $objPHPExcel->getActiveSheet()->getTitle();

  
    $m =0;
  //--------Insret Data Form Excel File --------//
  foreach($rowsold as $firstrow){
    


 
 $modelArrayListcheck           =array(
    'act_mode'=>'add_pincode_multipile_check',
    'row_id'     =>'',
    'Param3'     =>$firstrow[0],
    'Param4'     =>'',
    'Param5'     =>''
    
    );

  $getmodellist = $this->supper_admin->call_procedure('proc_five',$modelArrayListcheck);
  // p($getmodellist); 

   if($getmodellist[0]->pincheck < 1)
    {
      
     $pincodelists           =array(
    'act_mode'=>'add_pincode_multipile',
    'row_id'     =>'',
    'Param3'     =>$firstrow[0],
    'Param4'     =>'',
    'Param5'     =>$userid
   
    );

  $getmodellist = $this->supper_admin->call_procedure('proc_five',$pincodelists);
    //p($modelArrayList);
   }



     $m++;
} 
   $responce['excelErr'] = 'Pincode excel successfully uploaded.';
 } 
//------------End Upload Excel FIle----------------------------//
     
     $this->load->view('helper/header');
     $this->load->view('pincode/pincodeexcel_view',$responce);
 }
//---------------------Excel Upload Pincode End ---------------------//



}// end class
?>